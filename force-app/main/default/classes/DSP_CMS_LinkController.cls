global virtual with sharing class DSP_CMS_LinkController extends cms.ContentTemplateController{
    global DSP_CMS_LinkController(cms.CreateContentController cc){
        super(cc);
    }
    
    global DSP_CMS_LinkController(cms.GenerateContent cc){
        super(cc);
    }
    
    global DSP_CMS_LinkController(){}

    global virtual override String getHTML(){return '';}

    public String name{
        get{
            return String.isNotBlank(getProperty('name')) ?  getProperty('name') : '';
        }
        set;
    }
    
    public String linkDate {
        get{
            if(String.isNotBlank(getProperty('linkDate')))
            {
                return getProperty('linkDate');
            }
            else
            {
                return '';
            }
            
        }
        set;
    }
    
    public String website {
        get{
            return getProperty('website');
        }
        set;
    }

    public cms.Link parsedWebsite{
        get{
            return new cms.Link(this.website, this.page_mode, this.site_name);
        }
    }

    public String linkHTML(){
        String html='';
        
        html+='<a href="';
        
        if(content != null && this.parsedWebsite.targetPage != null)
            html+=parsedWebsite.targetPage;
        html+='"';
        if(content != null && this.parsedWebsite.target != null && this.parsedWebsite.target != '')
            html += 'target="' + this.parsedWebsite.target + '"';
        if(content != null && this.parsedWebsite.javascript != null && this.parsedWebsite.javascript != '')
            html += ' onlick=\''+this.parsedWebsite.javascript+'\'';
        html+=' >';
        html+= name+'</a>';
        
        return html;
    }
    
    public String newsLinkHTML(){
        String html='';
        
        html += '<div class="col s12 m12 l12">';
        html+='<a href="';
        if(content != null && this.parsedWebsite.targetPage != null)
            html+=parsedWebsite.targetPage;
        html+='"';
        if(content != null && this.parsedWebsite.target != null && this.parsedWebsite.target != '')
            html += 'target="' + this.parsedWebsite.target + '"';
        if(content != null && this.parsedWebsite.javascript != null && this.parsedWebsite.javascript != '')
            html += ' onlick=\''+this.parsedWebsite.javascript+'\'';
        html+=' >';
        html+= name+'</a>';
        html += '</div>';
        html += '<div class="small-text  col s12 m12 l12">';
        
        if(linkDate.indexOf('/') != -1)
        {
            try
            {
                String[] month_day_year = linkDate.split('/');
                Integer day = integer.valueOf(month_day_year[1]);
                Integer month = integer.valueOf(month_day_year[0]);
                Integer year = integer.valueOf(month_day_year[2]);
                Datetime d = Datetime.newInstance(year, month, day);
                html += d.format('MMMMMM d, YYYY');
            }
            catch(Exception e)
            {
                
            }
        }
        else
        {
            html += linkDate;
        }
        
        html += '</div>';
        
        return html;
    }
}