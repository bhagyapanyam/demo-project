/********************************
    CreatedBy : Subhashini Katta
    Organisation: Accenture
    Purpose  : This is the utility class to declare common constant variables.
    Version : 1.0.0.1
    **********************************/
    public class RaM_ConstantUtility
    {
        //Notification work order constant variable
         public static boolean firstRun = true;
         
        public static final string EM_TEXT ='EM';
        public static final string SPACE_TEXT =' ';
        public static final string P1_TEXT ='P1';
        public static final string P2_TEXT ='P2';
        public static final string P3_TEXT ='P3';
        public static final string NOTIFICATION_ID ='ZIPM';
        public static final string NOTIFICATION_NUMBER = '100';
        public static final string WORKORDER_ID = 'IW';
        public static final string WORKORDER_NYMBER = '080';
        public static final string Y_TEXT = 'Y';
        public static final string N_TEXT = 'N';
        public static final string DRM_TEXT = 'DRM';
        public static final string CERTIFICATE_NAME_TEXT = 'HuskyCertificate';
        public static final string ONE_TEXT = '1';
        public static final string TWO_TEXT = '2';
        public static final string THREE_TEXT = '3';
        public static final string RAM_SALESFORCE_TEXT = 'RaM_Salesforce';
        public static final string REP_TEXT = 'REP';
        public static final string PONE_TEXT = 'P100';
        public static final string PTWO_TEXT = 'P200';
        public static final string PTHREE_TEXT = 'P300';

        public static final string RETAIL_LOCATION_TRIGGER_HANDLER_CLASSNAME = 'RaM_RetailLocationHandler';
        public static final string RETAIL_LOCATION_TRIGGER_HANDLER_METHODNAME = 'insertRetailLocationRecord';
        public static final string BLANK =''; //Blank value
        public static final string COMA =','; //Coma value
        public static final string NEWLINE ='\n'; //new line value
        public static final string DASHES='-'; //Dash value
        public static final string P1_EMEGENCY_TEXT =  'P1-Emergency'; //Case Priority value
        public static final string P2_BREAK_FIX_TEXT = 'P2-Break/Fix';
        public static final string P3_PROGRAMMED_WORK_TEXT = 'P3-Programmed Work';
        public static final string SLA_HUSKY_ENERGY_TEXT = 'SLA Husky Energy';
        public static final string MAIN_TENANCETECH_TEXT = 'Maintenance Tech';
        public static final string RETAILER_TEXT = 'Retailer';
        public static final string RETAILER_ROLE = 'RaM_Retailer';
        public static final string TECHNICAL_SUPPORT_TEXT = 'Technical Support';
        public static final string HELP_DESK_ROLE = 'Help desk';
        public static final string RTAILER_USER_ROLE = 'Retailer';
        public static final string PORTAL_USER = 'Partner User';
        public static final string RETAILER_PROFILE_NAME = 'RaM Retailer Community';
        public static final string YES_TEXT = 'YES';
        public static final string NEXT_CASE_COUNTER = 'Case Next Counter';
        
        public static final string UNDERREVIEW_TEXT = 'Under Review';
        public static final string POSTPONED_TEXT = 'Postponed'; 
        public static final string ASSIGNED_TEXT = 'Assigned';
        public static final string NEW_TEXT = 'New';
        public static final Boolean NO_TEXT = False;
        // To get RecordType id's
        public static final Id CHILD_TICKET_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Child Case').getRecordTypeId();
        public static final Id TICKET_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        // public static final Id VenorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        public static final String VENDOR_ERROR_MESSAGE = Label.Ram_Ticket_Error_Message;
        Public Static final String RESTRICT_ERROR_MESSAGE = Label.RaM_Restrict_Insertion_Error_Message;
        //Milestone complete related labels
        public static final string VENDOR_COMPLETED_TEXT= 'Vendor Completed';
        public static final string WORKINPROGREE_TEXT= 'Work in Progress';

        //Constant class name for error log Added by Akanksha on 23 Jan 2018
        public static final string CLASS_NAME_CASE_TRIGGER =  'RaM_CaseTriggerHandler'; 
        //Constant method name for error log
        public static final string METHOD_NAME_CASE_TRIGGER_MILESTONE =  'completeCaseMilestone'; 
        public static final string METHOD_NAME_CASE_TRIGGER_UPDATE_MILESTONE =  'UpdateCaseMilestone';
        public static final string METHOD_NAME_CASE_TRIGGER_MILESTONE_ENTITLEMENT =  'entitlementMethod'; 
        public static final string METHOD_NAME_CASE_TRIGGER_UPDATE_ASSIGN_PROCESS =  'CaseAssignmentProcess';
        public static final string METHOD_NAME_CASE_TRIGGER_ORIGIN =  'ChangeCaseOrgin'; 
        public static final string CASE_OBJECT=  'Case'; 
        public static final string VENDOR_ASSIGNMENT_OBJECT=  'RaM_VendorAssignment__c'; 
        public static final string FATAL=  'FATAL';  
        public static final string BATCH_CLASS_RETAIL_LOCATION_CLASSNAME =  'RaM_moveRetailLocationRecordToLocation'; 
        public static final string Retail_Location_OBJECT=  'Retail_Location__c'; 
        // Constant name for Equipment Trigger Handler Added by Akanksha on 9 Feb 2018
        public static final string CUSTOMFIELD= 'CustomField';
        public static final string PICKLIST_TEXT=  'Picklist'; 
        public static final string EQUIPMENT_OBJECT=  'RaM_Equipment__c'; 
        public static final string EQUIPMENT_TYPE=  'Equipment_Type__c'; 
        public static final string EQUIPMENT_CLASS=  'Equipment_Class__c'; 
        public static final string EQUIPMENT_TYPE_LABEL=  'Equipment Type'; 
        public static final string EQUIPMENT_CLASS_LABEL=  'Equipment Class'; 
        public static final string METADATA_API_EQUIPMENT_CLASS= 'RaM_Equipment__c.Equipment_Class__c';
        public static final string METADATA_API_EQUIPMENT_TYPE= 'RaM_Equipment__c.Equipment_Type__c';

        public static final string EQUIPMENT_TYPE_VA=  'Equipment_Type_VA__c'; 
        public static final string EQUIPMENT_CLASS_VA=  'Equipment_Class_VA__c';
        public static final string METADATA_API_EQUIPMENT_CLASS_VA= 'RaM_VendorAssignment__c.Equipment_Class_VA__c';
        public static final string METADATA_API_EQUIPMENT_TYPE_VA= 'RaM_VendorAssignment__c.Equipment_Type_VA__c';

        public static final string CLASS_NAME_EQUIPMENT_TRIGGER =  'RaM_EquipmentTriggerHandler'; 
        public static final string METHOD_NAME_EQUIPMENT_CHECK_NEW_VAL='equipmentTypeClassNewValueCheck';
        public static final String LOCATION_ERROR_MESSAGE = Label.Ram_Location_Error_Message;
        public static final string METHOD_NAME_UPDATE_LOCATION_NUM_LOOKUP='updateLocationNumberLookup';
    }