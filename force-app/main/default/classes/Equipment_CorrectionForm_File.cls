/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_CorrectionForm_File {
	public Boolean insertAttachment;
	public String originalAttachmentFileId;
	public String attachmentFileName;
	public String attachmentBase64Data;
	public String originalAttachmentId;
}