/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = True)
private class ATSSMSReportGeneratorControllerOppTest {

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.ATSSMSReportGenerator;
            Test.setCurrentPageReference(pageRef);        
        
            // Create a Test Account with the name Asphalt
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
            
            insert account;
            System.AssertNotEquals(account.Id, Null);
        
            Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
            insert cont; 
            
            Opportunity  tender =  ATSParentOpportunityTestData.createTender(account.Id);
			insert tender;
            
            Cpm_Customer_Opportunity__c customer = ATSTestData.createOpportunityCustomer(account.Id, tender.id, cont.id);
            customer.cpm_Won__c = true;
            insert customer; 
            
            // Create Products        
            List<Product2> productList = ProductTestData.createProducts(101);
            insert productList;
            
            // Create Price Book Entries
            /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
            insert priceBookEntryList;*/
            
            Map<String, Id> recordTypeMap = new Map<String, Id>();
    
            for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                      WHERE DeveloperName in ('ATS_Asphalt_Product_Category', 'ATS_Emulsion_Product_Category', 'ATS_Residual_Product_Category', 'Asphalt') AND 
                                      SObjectType = 'Opportunity']) {
                recordTypeMap.put(recType.DeveloperName, recType.Id);            
            }
            
            List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
            
            Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
            Opportunity opp = new Opportunity();//OpportunityTestData.createOpportunity(recordTypeMap.get('Asphalt'));
            //opp.Opportunity_ATS_Product_Category__c = tender.Id;
            opp.Name = 'Test Opportunity';
            opp.AccountId = account.Id;
            opp.CloseDate = Date.today();
            opp.StageName = 'Won'; 
            opp.Confirmation_Method__c = 'PO';
            opp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            opp.PO__c = 'PO Test';
            opp.recordTypeId = recordTypeId;
            opp.cpm_Tax_Exempt_picklist__c = null; 
            opp.Pay_out_Handling_Fees__c = null;
            opp.cpm_Opportunity_Results_Category__c = 'Asphalt';
            opp.cpm_Tax_Exempt_picklist2__c = null; 
            opp.Pay_out_Handling_Fees2__c = null;
            opp.cpm_Opportunity_Results_Category2__c = 'Emulsion';
            insert opp;
            opp.Pricebook2Id = standardPriceBook[0].Id;
            opp.priceBook2 = standardPriceBook[0];
            update opp;
            
            List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
            priceBookEntryList[0].IsActive = true;
            update priceBookEntryList[0];
            System.debug('opp tom: '+opp);
            System.debug('priceBookEntryList[0] tom: '+priceBookEntryList[0]);
            OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
            System.debug('oli tom: '+oli);
            insert oli;
            
            Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
            System.assertEquals(theOpp.Pricebook2Id, standardPriceBook[0].Id);
            System.assertNotEquals(theOpp.Pricebook2, Null);
            System.assertNotEquals(theOpp.PriceBook2.Name, Null);

            //create standard controller
            ApexPages.StandardController sc = new ApexPages.standardController(tender);          

            //Create Controller
            ATSSMSReportGeneratorControllerOpp controller = new ATSSMSReportGeneratorControllerOpp(sc);
            System.AssertNotEquals(controller.getCustomers().size(), 0);

			// Set Categories
            String[] dataSetCategories = new String[3];
            dataSetCategories[0] = 'Residual';
            dataSetCategories[1] = 'Emulsion';
            datasetCategories[2] = 'Asphalt';
           
            controller.setSelectedCategories(dataSetCategories);
            System.AssertEquals(3,controller.getSelectedCategories().size());

            System.assertEquals(controller.getCategoryItems().size(), 3); 
            //System.AssertEquals(controller.getOpportunityItems().size(), 1);            
            //System.AssertEquals(controller.getSelectedCategories().size(), 1);   
            
            PageReference pageBackRef = controller.BackMethod();
            System.AssertEquals('/'+tender.id,pageBackRef.getUrl());
            
            controller.GenerateSMSReports();
            List<Attachment> attachments = [select id from Attachment where ParentId =:tender.id]; 
            System.AssertNotEquals(0, attachments.size());
            
            controller.doSearch();
            
        }
    }
}