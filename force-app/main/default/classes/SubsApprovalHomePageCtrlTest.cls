@isTest
public class SubsApprovalHomePageCtrlTest 
{
    @isTest
    public static void unitTest()
    {
        Profile p = [select id from Profile where profile.name='System Administrator' limit 1];

        User u = new User(ProfileId = p.id,LastName = 'Test', FirstName = 'Test', Username ='test7778@gmail.com', Alias='test',Email='test7778@gmail.com', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US');
        Insert u;
        system.debug(u.id);
        System.runAs(u) 
        {
            List<jbrvrm__Vendor_Subscription_Title__c> TitleList= new List<jbrvrm__Vendor_Subscription_Title__c>();
            List<jbrvrm__Subscription_Order__c> soLst = new List<jbrvrm__Subscription_Order__c>();
            account a=new account();
            a.name='test';
            a.type='Vendor';
            insert a;
            system.assertnotequals(a.id,null);
            jbrvrm__Vendor_Subscription_Title__c Title= new jbrvrm__Vendor_Subscription_Title__c( Name='testtitle');
            Title.jbrvrm__Publisher__c= a.id;
            insert Title;
            System.assertNotEquals(Title.id, null);
            TitleList.add(Title);
            jbrvrm__Currency__c curr= new jbrvrm__Currency__c(name='testcur');
            insert curr;
            System.assertNotEquals(curr.id, null);
            jbrvrm__Subscription_Order__c subOrder= new  jbrvrm__Subscription_Order__c();
            subOrder.name='testorder';
            subOrder.jbrvrm__Subscription_Title__c= Title.id;
            subOrder.jbrvrm__Currency__c=curr.id;
            subOrder.jbrvrm__End_Date__c = Date.Today();
            subOrder.jbrvrm__Start_Date__c=Date.Today();
            subOrder.jbrvrm__Category_Specialist__c = u.id;
            subOrder.jbrvrm__Ops_Reviewer__c = u.id;
            subOrder.jbrvrm__Renewal_Owner__c = u.id;
            subOrder.jbrvrm__IS_Business_Partner__c = u.id;
            subOrder.jbrvrm__Cost_Collector_Owner__c = u.id;
            subOrder.jbrvrm__Out_of_Budget_Spend_Approver__c = u.id;
            subOrder.jbrvrm__IS_Analyst__c = u.id;
            
            insert subOrder;
            
            //system.debug('suborder- '+subOrder);
            system.debug([select count() from ProcessInstance where targetobjectid=:subOrder.id]);
            if([select count() from ProcessInstance where targetobjectid=:subOrder.id] < 1)
            {       
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Approve.');
                req.setObjectId(subOrder.Id);
    
                //Submit the approval request
                Approval.ProcessResult result = Approval.process(req);
    
            }
            
            Test.startTest();
            SubsApprovalHomePageCtrl.getUserPendingProcess('',true);
            SubsApprovalHomePageCtrl.getUserPendingProcess('Name',true);
            SubsApprovalHomePageCtrl.getUserPendingProcess('Amount',true);
            SubsApprovalHomePageCtrl.getUserPendingProcess('Currency',true);
            SubsApprovalHomePageCtrl.getUserPendingProcess('StartDate',false);
            PendingApprovalsHomePageCtrl.getUserPendingProcess();
            Test.stopTest();
        }   
    }
}