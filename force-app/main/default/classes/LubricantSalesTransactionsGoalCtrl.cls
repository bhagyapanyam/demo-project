public with sharing class LubricantSalesTransactionsGoalCtrl  
{
    public String currentUser {get;set;}
    public Id currentUserId {get;set;}
    public String selectedYear {get;set;}
    public String selectedQuarter {get;set;}
    public String selectedSector {get;set;}
    public String selectedSegment {get;set;}
    public String selectedAccount {get;set;}
    public String editableQuarter {get;set;}
    public String SortFieldSave;
    private final Integer RESULT_PER_PAGE = 20;
    private integer currentPage = 1;
    public String view {get;set;}
    public Boolean showErrors {get;set;}
   
    public String blockHeader {get;set;}
    private Map<Id, User> subordinateMap;
    public Lubricant_Sales_Transaction_By_Quarter__c hiddenField {get;set;}
    /*public List<Lubricant_Sales_Transaction_By_Quarter__c> results {get;set;}*/
    
    public LubricantSalesTransactionsGoalCtrl ()
    {
        currentUser = UserInfo.getName();
        currentUserId = UserInfo.getUserId();
        
        selectedYear = String.valueOf(date.today().year());
        editableQuarter = getEditableQuarter();
        subordinateMap = new Map<Id, User>();
        if (Subordinates != null && Subordinates.size() > 0)
            view = '';
        else
        {   
            view = currentUserId;
            blockHeader = 'My Sales Transactions';
        }
        hiddenField = new Lubricant_Sales_Transaction_By_Quarter__c();
        showErrors = false;
     //   doSearch();
     //   view ='005G0000001Dnpb';
    }
    public List<SelectOption> Subordinates
    {
        get{
            if (Subordinates == null)
            {
                Subordinates = new List<SelectOption>();
                
                List<UserRole> role = [select Id, Name from UserRole where Id =: UserInfo.getUserRoleId()];
                System.debug('role =' + role);
                if (role != null && role[0].Name.contains('Sales Manager'))
                { 
                    if(view == null) Subordinates.add(new SelectOption('', '-- Select a Sales Rep --'));
                   
                    subordinateMap = new Map<Id, User>([select Id, Name from User where UserRole.ParentRoleId =: role[0].Id]);
                    
                    // KK: 2015-01-22 Use a List instead of a Map so that I can sort the sales rep names.  Currently they are not sorted.
                    List<User> salesRepNames = [SELECT Name FROM User WHERE UserRole.ParentRoleId = : role[0].Id ORDER BY Name];
                    /*
                    for(User u : subordinateMap.values())
                    {
                        Subordinates.add(new SelectOption(u.Id, u.Name));
                    }
                    */
                    for (User u : salesRepNames)
                    {
                        Subordinates.add(new SelectOption(u.Id, u.Name));
                    }
                }
              
            }
            
            return Subordinates;
        }
        set;
    }

    public List<SelectOption> YearList {
        get {
            if(YearList == null) {
                YearList = new List<SelectOption>();

                Integer startYear = 2015;
                Integer currentYear = Date.today().year();
                for(Integer i=startYear; i <= currentYear + 1; i++) {
                    YearList.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }
            }

            return YearList;
        }
        set;
    }

    public void resetSegmentandAccount()
    {
        selectedSegment = '';
        selectedAccount = '';
        blockHeader = view;
        if (subordinateMap.containsKey(view))
           blockHeader = subordinateMap.get(view).Name + '\'s Transactions';
        Subordinates = null;
    }
    private String getEditableQuarter()
    {
        String quarter = 'Q1';
        Date today = date.today();
        integer year = today.year();
        integer thisMonth = today.month();
        if (thisMonth >= 1 && thisMonth <= 3)
           return year + '-Q1' + ',' + year + '-Q2' + ','+ year + '-Q3' + ',' + year + '-Q4';
        else if (thisMonth > 3 && thisMonth <= 6)
           return year + '-Q2' + ','+ year + '-Q3' + ',' + year + '-Q4';
        else if (thisMonth > 6 && thisMonth <= 9)
           return year + '-Q3' + ',' + year + '-Q4';      
        
         return year + '-Q4';   
    }
    public List<SelectOption> getProductSegmentList()
    {
        List<SelectOption> products = new List<SelectOption>();
        products.add(new SelectOption('','--- All ---'));   
        
        //KK 2015-01-22 should only pick product segments that are applicable to the sales rep.
        /*
        List<Lubricant_Product_Segment__c> segments = [SELECT Name FROM Lubricant_Product_Segment__c ORDER BY Name];
        for (Lubricant_Product_Segment__c segment : segments)
        {
            products.add(new SelectOption(segment.Name, segment.Name));
        }
        */
        
        List<AggregateResult> queryResult = 
               [select Product_Segment__r.Name, count(Id) from Lubricant_Sales_Transaction_By_Quarter__c 
                   where Sales_Rep__c =: view 
                   group by Product_Segment__r.Name];
        
        for (AggregateResult item: queryResult)
        {
            String segment = String.valueOf(item.get('Name'));
            products.add(new SelectOption(segment, segment));           
        }
        
        return products;
    }
    public List<SelectOption> getAccountList()
    {
        List<SelectOption> accounts = new List<SelectOption>();
        accounts.add(new SelectOption('','--- All ---'));    
        List<AggregateResult> queryResult = 
               [select Account__c, Account__r.Name, Account__r.SMS_Number__c, count(Id) from Lubricant_Sales_Transaction_By_Quarter__c 
                    where Sales_Rep__c =: view 
                   group by Account__c, Account__r.Name, Account__r.SMS_Number__c order by Account__r.Name, Account__r.SMS_Number__c];

        for(AggregateResult item : queryResult)
        {
            String acct = String.valueOf(item.get('Name'));
            String smsNumber = String.valueOf(item.get('SMS_Number__c'));
            accounts.add(new SelectOption(smsNumber, acct + ' (' + smsNumber + ')'));       
        }
        return accounts;
    } 
  
    public PageReference refreshData()
    {
        if(String.isEmpty(view)) {
            showErrors = true;
            ApexPages.Message errorViewNotSelected = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Sales Rep.');
            ApexPages.addMessage(errorViewNotSelected);
            return null;
        }
        
        showErrors = false;
        System.debug('refesh data');
        standardSetCtrl = null;
        //doSearch();
        return null;
    }

    /*public void doSearch()
    {
        results = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
        if (standardSetCtrl != null)
        {
            System.debug('getResults = ' + standardSetCtrl.getRecords());
            results = (List<Lubricant_Sales_Transaction_By_Quarter__c>) standardSetCtrl.getRecords();
            
        }
      
    }*/

    public List<Lubricant_Sales_Transaction_By_Quarter__c> getResults()
    {
        if (standardSetCtrl != null)
        {
            return (List<Lubricant_Sales_Transaction_By_Quarter__c>) standardSetCtrl.getRecords();
        }
        return null;
    }
    
    public ApexPages.StandardSetController standardSetCtrl 
    {
        get 
        {
            if (standardSetCtrl == null && view != '')
            {
                standardSetCtrl = new ApexPages.StandardSetController(Database.query(buildSearchQuery())); 
                standardSetCtrl.setpagesize(RESULT_PER_PAGE);
                standardSetCtrl.setPageNumber(currentPage);
                CurrentPageNumber = String.valueOf(currentPage);
            }
            return standardSetCtrl;
        }
        set;
    } 

    public String CurrentPageNumber
    {
        get;
        set
        {
            CurrentPageNumber = value;
        }
    }
    
    private String buildSearchQuery()
    {
        String strQuery = 'select Account__r.Name, Account__r.SMS_Number__c, Volume_Goal_L__c, Gross_Profit_Goal__c, Id, ' +
                       'Latest_Transaction_Date__c, Product_Sector__c, Product_Segment__r.Name, Quarter__c, ' +
                       'Sales_Rep__c,  SOW_Gross_Profit_Goal__c, SOW_Gross_Profit_Actual__c, SOW_Volume_Actual_L__c, ' + 
                       'SOW_Volume_Goal_L__c,  Year__c ' +
                   'from Lubricant_Sales_Transaction_By_Quarter__c' + 
                   ' where Year__c =: selectedYear ';

        if (view != '')
            strQuery += ' and Sales_Rep__c =: view';
             
        if (selectedSector != '' && selectedSector != null)
            strQuery += ' and Product_Sector__c =: selectedSector';
            
        if (selectedSegment != '' && selectedSegment != null)
            strQuery += ' and Product_Segment__r.Name =: selectedSegment';
            
        if (selectedQuarter != '' && selectedQuarter != null)
            strQuery += ' and Quarter_1_to_4__c =: selectedQuarter';
            
        if (selectedAccount != '' && selectedAccount != null)
           strQuery += ' and Account__r.SMS_Number__c = : selectedAccount';
       
        
        strQuery += ' ORDER BY ' + String.escapeSingleQuotes(SortField) + ' ' + String.escapeSingleQuotes(SortDirection) ;
    
        strQuery += ' limit 10000';
        System.debug(strQuery);
        return strQuery;
    }
    
    /***
    * SortDirection - return sort direction. Default ascending(asc)
    ***/
    public String SortDirection {
        get { if (SortDirection == null) {  SortDirection = 'asc'; } return SortDirection;  }
        set;
    }
 
    /***
    * SortField - return sort by field. Default to Name
    ***/
    public String SortField {
        get { 
            if (SortField == null) 
            {
                SortField = 'Quarter_1_to_4__c, Account__r.Name, Account__r.SMS_Number__c, Product_Sector__c, Product_Segment__r.Name'; 
            } 
            return SortField;  
        }
        set; 
    }
 
    /***
    * SortToggle - toggles the sorting of query from asc<-->desc
    ***/
    public PageReference SortToggle() {
        System.debug('SortDirection =' + SortDirection);
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        //SortField = ApexPages.currentPage().getParameters().get('SortField');
        System.debug('SortField: ' + SortField + 'ApexPages.currentPage().getParameters().get(\'SortField\'): ' + ApexPages.currentPage().getParameters().get('SortField'));
        System.debug('SortDirection =' + SortDirection);
        // reset alpha filter and sort sequence when sorted field is changed
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
            SortFieldSave = SortField;
        }

        System.debug('--------------call sorttoggle');
        // run the query again
        Save();
        refreshData();
        return null;
    }
    public void doNothing(){
    
       //Do absolutely nothing
    
    }
    public void Save()
    {
        System.debug('---------------save the record');
        standardSetCtrl.save();
        //update results;
    }
    
    public void Cancel()
    {
        standardSetCtrl.cancel();
        //KK 2015-01-28 set the set controller back to null, so on rerender, it will try to refresh the data. 
        standardSetCtrl = null;
    }
    
    // Go to the first page
    public void first() {
        
        Save();
        standardSetCtrl.first();
        //doSearch();
    }
    
    // --------------------------------------------------------------------------
    // Go to the last page
    public void last() {
        Save();
        standardSetCtrl.last();
        //doSearch(); 
    }
    
    // --------------------------------------------------------------------------
    // Go to the previous page
    public void previous() {
        Save();
        standardSetCtrl.previous();
        //doSearch();
    }
    
    // --------------------------------------------------------------------------
    // Go to the next page
    public void next() {
        Save();
        standardSetCtrl.next();
        //doSearch();
    }
    
    public void gotoPage() {
        Save();
        //standardSetCtrl.setPageNumber(Integer.valueOf(CurrentPageNumber));
        //doSearch();
    }
}