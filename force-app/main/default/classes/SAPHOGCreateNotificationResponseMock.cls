@isTest
global with sharing class SAPHOGCreateNotificationResponseMock implements WebServiceMock {
	
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGNotificationServices.CreateNotificationResponse responseElement = new SAPHOGNotificationServices.CreateNotificationResponse();
   		responseElement.Message = 'SAPHOGNotificationServices.CreateNotificationResponse Test Message';
   		responseElement.Notification = new SAPHOGNotificationServices.Notification();
   		responseElement.Notification.NotificationNumber = '7521256';
   		responseElement.type_x = True;
   		
   		response.put('response_x', responseElement);
	}
}