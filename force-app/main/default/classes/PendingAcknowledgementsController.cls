public with sharing class PendingAcknowledgementsController {
    public Map<String, List<PendingStandingOrderAcknowledgements>>  pendingStandingOrderAcknowledgementsMap{get; set;}

    public PendingAcknowledgementsController() {
        Map<String, List<Standing_Order_Acknowledgement__c>> stdOrderAcknowledgements = getAcknowledgedUserMap();
        Map<String, User> opsLogUsers = getAllOpsLogUsers();
        List<Standing_Order__c> stdOrders = getAllStandingOrders();
        Map<String, Standing_Order__c> standingOrderMap = getStandingOrderListAsMap(stdOrders);

        Map<String, Map<String, Boolean>> acknowledgementUsers = matchAcknowledgedUsers(stdOrderAcknowledgements, opsLogUsers, stdOrders);

        this.pendingStandingOrderAcknowledgementsMap = getPendingAcknowledgementsDetails(acknowledgementUsers, opsLogUsers, standingOrderMap);
    }

    private Map<String, List<PendingStandingOrderAcknowledgements>> getPendingAcknowledgementsDetails(Map<String, Map<String, Boolean>> acknowledgementUsers, 
                Map<String, User> opsLogUsers, Map<String, Standing_Order__c> standingOrderMap) {

        Map<String, List<PendingStandingOrderAcknowledgements>>  pendingStandingOrderAcknowledgementsMap = new Map<String, List<PendingStandingOrderAcknowledgements>>();

        for(String stdOrder : acknowledgementUsers.keySet()) {
            Map<String, Boolean> userAcknowledgement = acknowledgementUsers.get(stdOrder);
            List<PendingStandingOrderAcknowledgements> pendingAcknowledgements = new List<PendingStandingOrderAcknowledgements>();

            for(String key : userAcknowledgement.keySet()) {
                if(opsLogUsers.get(key) != null && !userAcknowledgement.get(key)) {
                    PendingStandingOrderAcknowledgements pendingAcknowledgement = new PendingStandingOrderAcknowledgements(standingOrderMap.get(stdOrder), opsLogUsers.get(key));
                    pendingAcknowledgements.add(pendingAcknowledgement);
                }

            }

            pendingStandingOrderAcknowledgementsMap.put(getStandingOrderTitle(standingOrderMap.get(stdOrder), pendingAcknowledgements.size()), pendingAcknowledgements);
        }

        return pendingStandingOrderAcknowledgementsMap;
    }    

    /**This matches who has not acknowledged to given standing orders */
    private Map<String, Map<String, Boolean>> matchAcknowledgedUsers(Map<String, List<Standing_Order_Acknowledgement__c>> stdOrderAcknowledgements, 
                            Map<String, User> opsLogUsers, List<Standing_Order__c> stdOrders) {

        Map<String, Map<String, Boolean>> acknowledgementUsers = buildAcknowledgementsMap(opsLogUsers, stdOrders);

        for(String stdOrder : acknowledgementUsers.keySet()) {
            List<Standing_Order_Acknowledgement__c> acknowledgements = stdOrderAcknowledgements.get(stdOrder);
            Map<String, Boolean> userAcknowledgement = acknowledgementUsers.get(stdOrder);

            if(acknowledgements != null) {
                for(Standing_Order_Acknowledgement__c acknowledgement : acknowledgements){
                    if(userAcknowledgement.containsKey(acknowledgement.Acknowledged_User__c)) {
                        userAcknowledgement.put(acknowledgement.Acknowledged_User__c, true);
    
                    } else {
                        userAcknowledgement.put(acknowledgement.Acknowledged_User__c, false);
                    }
                }
    
                acknowledgementUsers.put(stdOrder, userAcknowledgement);
            }

        }

        return acknowledgementUsers;
    }

    /**This returns a map of users based on Operator Contacts */
    private Map<String, Map<String, Boolean>> buildAcknowledgementsMap(Map<String, User> opsLogUsers, List<Standing_Order__c> stdOrders) {
        Map<String, Map<String, Boolean>> acknowledgements = new Map<String, Map<String, Boolean>>();

        for(Standing_Order__c stdOrder : stdOrders) {
            Map<String, Boolean> acknowledgement = new Map<String, Boolean>();

            for(String opsLogUser : opsLogUsers.keySet()) {
                acknowledgement.put(opsLogUser, false);
            }

            acknowledgements.put(stdOrder.Id, acknowledgement);
        }

        return acknowledgements;
    }

    /**This returns a map of acknowledged standing orders by users */
    private Map<String, List<Standing_Order_Acknowledgement__c>> getAcknowledgedUserMap() {
        List<Standing_Order_Acknowledgement__c> stdOrderAcknowledgements = getAllAcknowledgements();
        Map<String, List<Standing_Order_Acknowledgement__c>> acknowledgements = new Map<String, List<Standing_Order_Acknowledgement__c>>();

        for(Standing_Order_Acknowledgement__c stdOrderAcknowledgement : stdOrderAcknowledgements) {
            if(acknowledgements.containsKey(stdOrderAcknowledgement.Standing_Order__c)) {
                List<Standing_Order_Acknowledgement__c> userAcknowledgements = acknowledgements.get(stdOrderAcknowledgement.Standing_Order__c);
                userAcknowledgements.add(stdOrderAcknowledgement);
                acknowledgements.put(stdOrderAcknowledgement.Standing_Order__c, userAcknowledgements);

            } else {
                List<Standing_Order_Acknowledgement__c> userAcknowledgements = new List<Standing_Order_Acknowledgement__c>();
                userAcknowledgements.add(stdOrderAcknowledgement);
                acknowledgements.put(stdOrderAcknowledgement.Standing_Order__c, userAcknowledgements);                
            }
        }

        return acknowledgements;
    }    

    private Map<String, User> getAllOpsLogUsers() {
        List<String> fedIds = new List<String>();

        AggregateResult[] groupedResults = [SELECT Operator_Contact__r.Husky_AD_Username__c huskyAdName
                                            FROM Shift_Operator__c 
                                            WHERE Operator_Contact__r.Account.Name = 'Sunrise' 
                                                AND Team__r.RecordType.Name NOT IN ('Maintenance')
                                                AND CreatedDate = LAST_N_DAYS:365 
                                            GROUP BY Operator_Contact__r.Husky_AD_Username__c];

        for(AggregateResult groupedResult : groupedResults) {
            fedIds.add((String)groupedResult.get('huskyAdName'));
        }

        return getOpsLogUsersByContact(fedIds);
    }

    private Map<String, User> getOpsLogUsersByContact(List<String> fedIds) {
        Map<String, User> userMap = new Map<String, User>();

        List<User> users = [SELECT Id, Name, FederationIdentifier 
                            FROM User 
                            WHERE FederationIdentifier In :fedIds
                                AND IsActive = true];

        for(User usr : users) {
            if(!userMap.containsKey(usr.Id)) {
                userMap.put(usr.Id, usr);
            }
        }

        return userMap;
    }

    private List<Standing_Order__c> getAllStandingOrders() {

        return [SELECT Id, Name, Plant__c, Unit__r.Name, Equipment__r.Name
                FROM Standing_Order__c
                WHERE Completion_Date__c = null OR Completion_Date__c >= TODAY];
    }

    private List<Standing_Order_Acknowledgement__c> getAllAcknowledgements() {

        return [SELECT Id, Acknowledged_User__c, Acknowledged_User__r.Name, Acknowledged_User__r.Email, Read_Acknowledged__c, 
                        Standing_Order__c, Standing_Order__r.Name, Standing_Order__r.Plant__c
                FROM Standing_Order_Acknowledgement__c
                WHERE Read_Acknowledged__c = true];
    } 
    
    private Map<String, Standing_Order__c> getStandingOrderListAsMap(List<Standing_Order__c> standingOrders) {
        Map<String, Standing_Order__c> standingOrderMap = new Map<String, Standing_Order__c>();

        for(Standing_Order__c standingOrder : standingOrders) {
            standingOrderMap.put(standingOrder.Id, standingOrder);
        }

        return standingOrderMap;
    }  
    
    private String getStandingOrderTitle(Standing_Order__c standingOrder, Integer listSize) {
        String title = '';

        title = 'Standing Order Name : ' + standingOrder.Name;

        if(standingOrder.Plant__c != null) {
            title = title + ', Plant Area : ' + standingOrder.Plant__c;
        }

        if(standingOrder.Unit__r.Name != null) {
            title = title + ', Unit : ' + standingOrder.Unit__r.Name;
        }   
        
        if(standingOrder.Equipment__r.Name != null) {
            title = title + ', Equipment : ' + standingOrder.Equipment__r.Name;
        }  
        
        listSize = listSize == null ? 0 : listSize;
        title = title + ' (' +  listSize + ' Users)';                     

        return title;
    }      
    
    public class PendingStandingOrderAcknowledgements {
        public String standingOrderName {get; set;}
        public String standingOrderId {get; set;}
        public String platAreaName {get; set;}
        public String userId {get; set;}
        public String userName {get; set;}

        public PendingStandingOrderAcknowledgements(Standing_Order__c standingOrder, User usr) {
            this.standingOrderName = standingOrder.Name;
            this.standingOrderId = standingOrder.Id;
            this.platAreaName = standingOrder.Plant__c;
            this.userId = usr.Id;
            this.userName = usr.Name;
        } 
             
    }    

}