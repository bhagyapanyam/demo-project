@isTest
global with sharing class SAPHOGIsAliveResponseMock implements WebServiceMock {

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGNotificationServices.IsAliveResponse responseElement = new SAPHOGNotificationServices.IsAliveResponse();
   		responseElement.ErrorMessage = 'SAPHOGNotificationServices.IsAliveResponse Test Message';
   		responseElement.Alive = True;
   		
   		response.put('response_x', responseElement);
	}
}