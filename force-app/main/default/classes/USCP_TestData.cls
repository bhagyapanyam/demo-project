@isTest
public class USCP_TestData {

  public static USCP_Terminal__c createTerminal(String TerminalName, String address, String city, String state, String opismarket,  Boolean save) {
  
    USCP_Terminal__c testData = new USCP_Terminal__c();
    testData.Name = TerminalName ;
    testData.Address__c = address;
    testData.City__c = city;
    testData.State__c = state;
    testData.OPIS_Market__c = opismarket;
    testData.IsActive__c = true;
    testData.LocationType__c = 'Terminal';    
    if(save)    
    {
        insert testData;
    }
        
    return testData;
  }

  public static Product2 createProduct(String ProductName, Boolean save) {
  
    Product2  testData = new Product2();
    testData.Name = ProductName;
    if(save)    
    {
        insert testData;
    }
    return testData;
  } 
  public static USCP_TerminalProductAssociation__c createTerminalProductAssociation(String TerminalID, String ProductID, Boolean save) {
  
    USCP_TerminalProductAssociation__c testData = new USCP_TerminalProductAssociation__c();
    testData.Product__c = ProductID;
    testData.Terminal__c = TerminalID;
    if(save)    
    {
        insert testData;
    }
    return testData;
  }   


  public static USCP_Rack_Price__c createRackPrice(String TerminalID, String ProductID, Date dt, Decimal price,Decimal change,   Boolean save) {
  
    USCP_Rack_Price__c testData = new USCP_Rack_Price__c();
    testData.Terminal__c = Terminalid ;
    testData.Product__c = ProductID;
    testData.Price__c = price;
    testData.Change__c = change;
    testData.Date__c = dt;    

    if(save)    
    {
        insert testData;
    }
        
    return testData;
  }   
  
  public static USCP_EFT__c createEFT(String AccountID, Date draftdate, Decimal amount,  Boolean save) {
  
    USCP_EFT__c testData = new USCP_EFT__c();
    testData.Account__c = AccountID;
    testData.Draft_Date__c = draftdate;
    testData.Amount__c = amount;

    if(save)    
    {
        insert testData;
    }
        
    return testData;
  }

  public static USCP_Invoice__c createInvoice(String AccountID, String InvoiceNumber, String EFTID,  Date duedate, Date invoicedate, Date shipdate, Decimal amount,  Boolean save) {
  
    USCP_Invoice__c testData = new USCP_Invoice__c();
    testData.Name = InvoiceNumber;
    testData.Account__c = AccountID;
    testData.Due_Date__c = duedate;
    testData.Invoice_Date__c = invoicedate;
    testData.Ship_Date__c = shipdate;     
    testData.Amount__c = amount;
    testData.EFT__c =  EFTID;  

    if(save)    
    {
        insert testData;
    }
        
    return testData;
  }

//USCP_BOL_AccTransaction__c
public static USCP_BOL_AccTransaction__c createBOLAccTransaction(String AccountID, String BOLNumber, String InvoiceID,  String Carrier, String TerminalID,   Date movementdate,  String ProductID,  Boolean save) {
    USCP_BOL_AccTransaction__c testData = new USCP_BOL_AccTransaction__c();
    testData.Account__c = AccountID;
    testData.BOL_Number__c = BOLNumber;
    testData.Carrier__c = Carrier;
    testData.Invoice__c = InvoiceID;    
    //testData.Destination_Location__c
    //testData.Gross_Quantity__c
    //testData.Lifting_Number__c
    testData.Location__c = TerminalID;
    testData.Movement_Date__c = movementdate;
    //testData.Net_Quantity__c
    testData.Product__c = ProductID;
//    testData.SOURCE_SYSTEM_ID__c
//    testData.Type__c   
    if(save)    
    {
        insert testData;
     }
    return testData;    
    
}

    public static User createTestCustomerPortalUser(Id contactId) {
        ID ProfileID = [ Select id from Profile where name = 'US Portal User Profile'].id;
                
        User user = new User();
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
        user.ContactId = contactId;
        user.ProfileId = ProfileID;
        user.UserName = 'test-user' + random + '@fakeemail.com';
        user.alias = 'tu' + String.ValueOf(contactId).right(6);
        user.CommunityNickName = 'tuser1' + random;
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1'; 
        user.LanguageLocaleKey='en_US';
        user.FirstName = 'Test';
        user.LastName = 'User';
        insert user;
        
        return user;
    }
  

}