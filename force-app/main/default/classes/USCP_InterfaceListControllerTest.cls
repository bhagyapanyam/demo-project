/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_InterfaceListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        PageReference pageRef = Page.USCP_InterfaceList;
        Test.setCurrentPageReference(pageRef);
        
        System.runAs(runningUser) {
            upsert new USCP_Interfaces__c (
                Name = 'INT1',
                Interface_Name__c = 'INT1',
                Max_Days__c = 5,
                Date_Parameter__c = false,
                NumDays_Parameter__c = true,
                Invoke_URL__c = 'google.ca'
            );
            upsert new USCP_Interfaces__c (
                Name = 'INT2',
                Interface_Name__c = 'INT2',
                Max_Days__c = 5,
                Date_Parameter__c = true,
                NumDays_Parameter__c = false,
                Invoke_URL__c = 'google.ca'
            );
                
 
                
            Test.startTest();
                Boolean flag = false;
                USCP_InterfaceListController sc = new USCP_InterfaceListController();              
                System.AssertEquals(2,sc.InterfaceWrapperList.size()); 
                            
                //get the wrapper object from the map
                USCP_InterfaceListController.InterfaceWrapper intWraper1 = sc.InterfaceMap.get('INT1');    
                USCP_InterfaceListController.InterfaceWrapper intWraper2 = sc.InterfaceMap.get('INT2'); 


                //Number of Days based interface Tests
                pageRef.getParameters().put('interfacename','wrong interface');
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages());
                flag = MessageContains('Unknown interface');
                System.Assert(flag);
                
                         
                pageRef.getParameters().put('interfacename','INT1');
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages());
                flag = MessageContains('Please selecte date');
                System.Assert(flag);


                //set the date too far in past, should fail
                intWraper1.dateplaceholder.birthdate = Date.Today() - 10;                
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages());
                flag = MessageContains('you can only go back');
                System.Assert(flag);                

                intWraper1.dateplaceholder.birthdate = Date.Today();                
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages());
                flag = MessageContains('interface successfully started');
                System.Assert(flag);                 
                
                
                //Date based interface Tests   
                
                pageRef.getParameters().put('interfacename','INT2');
                sc.InvokeInterface();
                //System.Asser(ApexPages.hasMessages()); 
                flag = MessageContains('Please selecte date');
                System.Assert(flag);                                 

                //set the date in the future, should fail
                intWraper2.dateplaceholder.birthdate = Date.Today() + 1 ;                
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages());
                flag = MessageContains('you can only run this interface for past dates');
                System.Assert(flag);                 
                             
                intWraper2.dateplaceholder.birthdate = Date.Today()-1;                
                sc.InvokeInterface();
                //System.Assert(ApexPages.hasMessages()); 
                flag = MessageContains('interface successfully started');
                System.Assert(flag);                
                                
            Test.stopTest(); 
        }
    }
    
    static Boolean MessageContains(String messagePart)
    {
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(messagePart)) b = true;
            System.debug(msg.getDetail());
        }
        return b;
    }    
}