global class TicketEmailResponseHandler implements Messaging.InboundEmailHandler 
{
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
      {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          System.debug('Inside TicketEmailResponseHandler ');
          System.debug('Gmail? '+email.fromAddress);
          System.debug('Email Subject is '+email.Subject);
          String str = email.Subject;
          System.debug('Is this a reply email  '+email.inReplyTo);
          String  s = str.substring(str.lastIndexOf('Ticket:')+7, str.length());
          System.debug(' is this Ticket #? '+s);
          if( (email.Subject).contains('RE: ') || (email.Subject).contains('Re: '))
          {
            Ticket__c ticketRecord = [Select Id, Name from Ticket__c where Name=:s];
            
            //newEmailcon.add(new EmailMessage(
            //EmailMessage emailMessage = new EmailMessage(FromAddress = email.fromAddress,
            //FromName = email.fromName,
            //ToAddress = email.toAddresses[0],
            //Subject = email.subject,
            //TextBody = email.plainTextBody,
            //HtmlBody = email.htmlBody,
            //RelatedToId = ticketRecord.Id); 
            //insert emailMessage;
            Inbound_Email__c inboundEmail = new Inbound_Email__c();
            inboundEmail.Name=email.Subject;             
            inboundEmail.FromAddress__c=email.FromAddress;
            //inboundEmail.ToAddress__c=email.ToAddress;
            String CcAddress='';
            for(String ccAddr: email.CcAddresses)
            {
                CcAddress = CcAddress + ccAddr;
            }
            String ToAddress='';
            for(String toAddr: email.ToAddresses)
            {
                ToAddress = ToAddress + toAddr;
            }
            inboundEmail.ToAddress__c=ToAddress; 
            inboundEmail.CcAddress__c=CcAddress;
            System.debug('1--ccAddresses---- '+email.ccAddresses);
            System.debug('1--headers---- '+email.headers);              
            System.debug('1--inReplyTo---- '+email.inReplyTo);
            System.debug('1--messageId---- '+email.messageId);
            System.debug('1--references---- '+email.references);
            System.debug('1--replyTo---- '+email.replyTo);
            System.debug('1--toAddresses---- '+email.toAddresses);
            //inboundEmail.BccAddress__c=email.BccAddress;
            inboundEmail.FromName__c=email.FromName;            
            //inboundEmail.HasAttachment__c=email.HasAttachment; //Headers__c=emailMsg.Headers__c,
            inboundEmail.HtmlBody__c=email.plaintextBody; //MessageDate__c=emailMsg.MessageDate__c, MessageSize__c=emailMsg.MessageSize, 
            inboundEmail.RelatedTo__c=ticketRecord.Id;
            inboundEmail.Status__c = 'Replied';
            inboundEmail.MessageDate__c=System.today();
            Database.SaveResult sr = Database.insert(inboundEmail);
            if( sr.isSuccess() )
            {
                if( email.textAttachments !=null)
                {
                    for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) 
                    {
                      Attachment attachment = new Attachment();
                     
                      attachment.Name = tAttachment.fileName;
                      attachment.Body = Blob.valueOf(tAttachment.body);
                      attachment.ParentId = sr.getId();
                      insert attachment;
                    }
                }
                if( email.binaryAttachments !=null)
                {
                    for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) 
                    {
                      Attachment attachment = new Attachment();
                     
                      attachment.Name = bAttachment.fileName;
                      attachment.Body = bAttachment.body;
                      attachment.ParentId = sr.getId();
                      insert attachment;
                    }
                }                
            }
            
          }
          return result;
      }
}