/**
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:    Test class for creating Exemptions on Alerts on Approval and Rejection
 *  History:        Created on 9/30/2018   
 */

@isTest
public class HOG_VG_Alert_Exemption_TriggerHelperTest {

	@isTest
	static  void HOG_VG_Alert_Exemption_Trigger_Approved_Test() {

		Test.startTest();
		HOG_Vent_Gas_Alert_Exemption_Request__c exemption = [SELECT Status__c FROM HOG_Vent_Gas_Alert_Exemption_Request__c];
		exemption.Status__c = 'Approved';
		update exemption;
		HOG_Vent_Gas_Alert__c alertQueryForAssert = [SELECT Status__c, Exemption__c FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(true, alertQueryForAssert.Exemption__c, 'Alert field Exemption checkbox haven\t been set to True');
		System.assertEquals(HOG_VentGas_Utilities.ALERT_STATUS_EXEMPTION, alertQueryForAssert.Status__c, 'Alert status haven\t changed to Exemption status');
	}

	@isTest
	static  void HOG_VG_Alert_Exemption_Trigger_Rejected_Test() {

		Test.startTest();
		HOG_Vent_Gas_Alert_Exemption_Request__c exemption = [SELECT Status__c FROM HOG_Vent_Gas_Alert_Exemption_Request__c];
		exemption.Status__c = 'Rejected';
		update exemption;
		HOG_Vent_Gas_Alert__c alertQueryForAssert = [SELECT Status__c, Exemption__c FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(false, alertQueryForAssert.Exemption__c, 'Alert field Exemption checkbox haven\t been set back to False');
		System.assertEquals(HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, alertQueryForAssert.Status__c, 'Alert status haven\t changed to back Not Started');
	}

	@testSetup
	static void setData() {
		HOG_Vent_Gas_Alert_Configuration__c customSettings= new HOG_Vent_Gas_Alert_Configuration__c();
		insert customSettings;

		Date expiredExemptionDate = Date.Today()+365;

		User usr = HOG_VentGas_TestData.createUser('Mr', 'Mychal', 'TheMys');
		User usr2 = HOG_VentGas_TestData.createUser('Lucifer', 'Morningstar', 'Devil');
		Location__C location = HOG_VentGas_TestData.createLocation();
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(location.Id, usr.Id, true);
		alert.Exemption_Approver__c = usr2.Id;
		update alert;

		HOG_Vent_Gas_Alert_Exemption_Request__c exemption = new HOG_Vent_Gas_Alert_Exemption_Request__c();
		exemption.Vent_Gas_Alert__c = alert.Id;
		exemption.Well_Location__c = location.Id;
		exemption.Operations_Engineer__c = usr2.Id;
		exemption.Expiration_Date__c = expiredExemptionDate;
		exemption.Reason__c = 'Approve pls';
		insert exemption;
	}


}