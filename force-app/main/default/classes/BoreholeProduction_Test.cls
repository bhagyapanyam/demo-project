@isTest
public class BoreholeProduction_Test
{
    static testMethod void testBoreholeProduction()
    {
        // Create an operating district for the opField objects
        Business_Department__c busDep1 = new Business_Department__c();
        busDep1.Name = 'Business Department Test';
        
        insert busDep1;
        
        // Create an operating district for the opField objects
        Operating_District__c opDis1 = new Operating_District__c();
        opDis1.Name = 'Operating District Test';
        opDis1.Business_Department__c = busDep1.id;
        opDis1.GRD_ID__c = '120905';
        
        insert opDis1;
        
        // Create operating field objects for use in location object
        Field__c opField1 = new Field__c();
        opField1.Name = 'Ansell';
        opField1.Operating_District__c = opDis1.id;
        opField1.GRD_ID__c = '4981';
        
        insert opField1;
        
        // Create a Well Location
        Location__c uwi = new Location__c(Well_Orientation__c = 'HZ',
                                          Operating_Field_AMU__c = opField1.Id);
        Database.SaveResult uwiResult = Database.insert(uwi);
        
        // Create a borehole
        GRD_Borehole__c borehole = new GRD_Borehole__c(UWI_Location__c     = uwiResult.getId(),
                                                       Pool__c             = 'Bakken',
                                                       Target_Formation__c = 'Bakken',
                                                       Risked__c           = .5,
                                                       Strategy__c         = 'Oil',
                                                       Production_Start__c = Date.newInstance(2015,1,1),
                                                       GRD_GUID__c         = '12345678901234567890');
        Database.SaveResult boreholeResult = Database.insert(borehole);
        
        // Create a Well Production
        Well_Production__c production = new Well_Production__c(YYYYMM__c = '201501',
                                                               Oil_Actual_bbl__c = 10,
                                                               NGL_Actual_bbl__c = 10,
                                                               Gas_Actual_mcf__c = 10,
                                                               Gas_Override_mcf__c = 10,
                                                               External_Id__c = '12345678901234567890-201501',
                                                               Borehole__c = boreholeResult.getId());
        Database.SaveResult productionResult = Database.insert(production);
        
        
        // Create a Type Curve
        Type_Curve__c typeCurve = new Type_Curve__c(Month__c   = 1,
                                                    Oil_bbl__c = 5,
                                                    NGL_bbl__c = 5,
                                                    Gas_mcf__c = 5,
                                                    Target_Formation__c = 'Bakken',
                                                    Well_Type__c = 'HZ',
                                                    Pool__c = 'Bakken');
        Database.SaveResult typeCurveResult = Database.insert(typeCurve);
        
        PageReference pageRef = Page.BoreholeProduction;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',boreholeResult.getId());
        BoreholeProductionController ctrllr = new BoreholeProductionController();

        BoreholeProductionXML productionXMLController = new BoreholeProductionXML(ctrllr);
        
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Feb-15','OIL',8.0);
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Mar-15','NGL',8.0);
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Apr-15','GAS',8.0);
        
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Jan-15','OIL',8.0);
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Jan-15','NGL',8.0);
        BoreholeProductionXML.createWellProduction(boreholeResult.getId(),'Jan-15','GAS',8.0);
        
        BoreholeProductionXML.updateWellProduction(boreholeResult.getId(),productionResult.getId(),'OIL',8.0);
        BoreholeProductionXML.updateWellProduction(boreholeResult.getId(),productionResult.getId(),'NGL',8.0);
        BoreholeProductionXML.updateWellProduction(boreholeResult.getId(),productionResult.getId(),'GAS',8.0);
        
        BoreholeProductionXML.XMLWellProduction(boreholeResult.getId(),'UPDATE','Jan-15','Oil','7.0','12345678901234567890','test@test.com','GRD');
        
    }
}