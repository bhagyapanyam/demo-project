/*
Created Date - 22 Nov 2019
Description - WebService class to pull data from ServiceNow and store it in Salesforce
*/
global class VrmServiceHuskyIntegrationCls
{
    global class VSTInfo
    {
        webservice String name;    
        webservice String u_service_delivery_specialist;
        webservice String sys_id;
    }
    
    global class PushSNowInfo
    {
        webservice String u_vrm_id;   
        webservice String u_license_expiration;
        webservice String u_mss_owner;
        webservice String u_mss_url;
        webservice String sys_id;
    }
    
    global class ResponseCls
    {
        webservice String sysId;
        webservice String msg;
    }
    
    webservice static List<ResponseCls> SNowToVrm(List<VSTInfo> vstObj)
    {
        Map<String,Id> userNameMap = new Map<String,Id>();
        
        List<jbrvrm__Vendor_Subscription_Title__c> vstList = new List<jbrvrm__Vendor_Subscription_Title__c>();
        Set<String> sysIdSet = new Set<String>();
        
        List<User> userList = [Select id, name, Alias from User LIMIT 50000];
        
        if(userList != null && userList.size()>0)
        {
            for(User u : userList)
            {
                userNameMap.put(u.name.toLowerCase()+' ('+u.alias.toLowerCase()+')',u.id);
            }
        }
        
        if(vstObj != null && vstObj.size()>0)
        {
            for(VSTInfo obj : vstObj)
            {
                sysIdSet.add(obj.sys_id);
            }
        }
        

        Map<Id,jbrvrm__Vendor_Subscription_Title__c> vstMap = new Map<Id,jbrvrm__Vendor_Subscription_Title__c>([Select id, jbrvrm__ServiceNow_CI_New__c, jbrvrm__ServiceNow_Sys_Id__c , jbrvrm__Ops_Reviewer__c from jbrvrm__Vendor_Subscription_Title__c where jbrvrm__ServiceNow_Sys_Id__c =: sysIdSet]);
        List<ResponseCls> respClsList = new List<ResponseCls>();
        if(vstMap != null && vstMap.size()>0)
        {
            for(Id vstId : vstMap.keySet())
            {
                for(VSTInfo obj : vstObj)
                {
                    if(obj.sys_id == vstMap.get(vstId).jbrvrm__ServiceNow_Sys_Id__c)
                    {
                        jbrvrm__Vendor_Subscription_Title__c vObj = new jbrvrm__Vendor_Subscription_Title__c ();
                        vObj.Id = vstId;
                        vObj.jbrvrm__ServiceNow_CI_New__c = obj.name;
                        vObj.jbrvrm__ServiceNow_Sys_Id__c = vstMap.get(vstId).jbrvrm__ServiceNow_Sys_Id__c;
                        String[] deliverySpecialists = obj.u_service_delivery_specialist.split(',');
                        if(userNameMap.containsKey(deliverySpecialists[0].toLowerCase()))
                        {
                            vObj.jbrvrm__Ops_Reviewer__c = userNameMap.get(deliverySpecialists[0].toLowerCase());
                        }
                        
                        vstList.add(vObj);
                    }
                }
                
            }
            
            Database.SaveResult[] srList = Database.update(vstList, false);
            if(srList != null)
            {
                for (Database.SaveResult sr : srList) 
                {
                    ResponseCls respObj = new ResponseCls();
                    if (sr.isSuccess()) 
                    {
                        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                        respObj.sysId = vstMap.get(sr.getId()).jbrvrm__ServiceNow_Sys_Id__c;
                        respObj.msg = 'Updated Successfully';
                        
                        respClsList.add(respObj);
                    }
                    else 
                    {
                        for(Database.Error err : sr.getErrors()) 
                        {
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            respObj.sysId = vstMap.get(sr.getId()).jbrvrm__ServiceNow_Sys_Id__c;
                            respObj.msg = 'Error - ' +err.getMessage();
                            respClsList.add(respObj);
                            
                        }
                    }
                    
                    system.debug('#### respClsList: ' + respClsList);
                }   
            }
        }
        
           
        return respClsList;
    
    }
    
    webservice static List<PushSNowInfo> VrmToSNow()
    {
        Map<Id,String> userNameMap = new Map<Id,String>();
        
        List<PushSNowInfo> PushSNowInfoList = new List<PushSNowInfo>();
        List<jbrvrm__Vendor_Subscription_Title__c> existingVSTList = new List<jbrvrm__Vendor_Subscription_Title__c>();
        existingVSTList = [Select id, name, jbrvrm__ServiceNow_SYS_ID__c, jbrvrm__Contract_End_Date__c, jbrvrm__Vendor_Analyst__c from jbrvrm__Vendor_Subscription_Title__c where jbrvrm__ServiceNow_SYS_ID__c != '' AND jbrvrm__ServiceNow_SYS_ID__c != null AND jbrvrm__Status__c = 'Active'];
        
        List<User> userList = [Select id, name, Alias from User LIMIT 50000];
        
        if(userList != null && userList.size()>0)
        {
            for(User u : userList)
            {
                userNameMap.put(u.id,u.name.toLowerCase()+' ('+u.alias.toLowerCase()+')');
            }
        }
        
        if(existingVSTList != null && existingVSTList.size()>0)
        {
            for(jbrvrm__Vendor_Subscription_Title__c vst : existingVSTList)
            {
                PushSNowInfo psObj = new PushSNowInfo();
                psObj.sys_id = vst.jbrvrm__ServiceNow_SYS_ID__c;
                psObj.u_vrm_id = vst.Name;
                psObj.u_mss_url = System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+vst.Id;
                if(userNameMap.containsKey(vst.jbrvrm__Vendor_Analyst__c))
                {
                    psObj.u_mss_owner = userNameMap.get(vst.jbrvrm__Vendor_Analyst__c);
                }    
                if(vst.jbrvrm__Contract_End_Date__c != null)
                {
                    String month = '';
                    String day = '';
                    if(vst.jbrvrm__Contract_End_Date__c.month() >= 1 && vst.jbrvrm__Contract_End_Date__c.month() <=9)
                    {
                        month = '0'+vst.jbrvrm__Contract_End_Date__c.month();
                    }
                    else
                    {
                        month = ''+vst.jbrvrm__Contract_End_Date__c.month();
                    }
                    if(vst.jbrvrm__Contract_End_Date__c.day() >=1 && vst.jbrvrm__Contract_End_Date__c.day() <=9)
                    {
                        day = '0'+vst.jbrvrm__Contract_End_Date__c.day();
                    }
                    else
                    {
                        day = ''+vst.jbrvrm__Contract_End_Date__c.day();
                    }
                    psObj.u_license_expiration = vst.jbrvrm__Contract_End_Date__c.year()+'-'+ month + '-' + day;
                }
                
                PushSNowInfoList.add(psObj);
                
            }
        }
        System.debug('#### PushSNowInfoList - ' + PushSNowInfoList);
        return PushSNowInfoList;
    }
    
}