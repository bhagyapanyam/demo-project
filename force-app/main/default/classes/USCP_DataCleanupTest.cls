/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_DataCleanupTest
{
    static testmethod void test() {

        Integer recordcount;
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {


            USCP_Settings__c  mc = new USCP_Settings__c(); //USCP_Settings__c.getOrgDefaults();
            mc.Delete_Data_After_N_Days__c  = 15; 
            insert mc;

            //lets prepare some data
            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);
    
            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);
            
            Date cutoffday =  Date.today()-30;
            
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);            
            
            
            
            USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100',  null,  cutoffday , cutoffday , cutoffday , 1000.0,  true);
            USCP_TestData.createInvoice(account.Id, 'INV200', null, cutoffday , cutoffday , cutoffday , 2000.0,  true);
            
            recordcount = [SELECT COUNT() FROM USCP_Invoice__c];
            System.assertEquals(recordcount, 2);            
            
            
            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL1', invoice.id,  'Carrier 1', terminal.id,   cutoffday , product.id ,  true);
            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL2', invoice.id,  'Carrier 1', terminal.id,   cutoffday , product.id ,  true);
            recordcount = [SELECT COUNT() FROM USCP_BOL_AccTransaction__c];
            System.assertEquals(recordcount, 2);                
            
          
            
            Test.startTest();
            
            //and now lets delete this data 
            USCP_DataCleanup bol_b = new USCP_DataCleanup('BOL');
            Database.executeBatch(bol_b); 
            USCP_DataCleanup inv_b = new USCP_DataCleanup('INV');
            Database.executeBatch(inv_b);
            USCP_DataCleanup eft_b = new USCP_DataCleanup('EFT');
            Database.executeBatch(eft_b); 
            
            Test.stopTest();
            
            // Verify records got deleted 
            recordcount = [SELECT COUNT() FROM USCP_Invoice__c];
            System.assertEquals(recordcount, 0);
            recordcount = [SELECT COUNT() FROM USCP_BOL_AccTransaction__c];
            System.assertEquals(recordcount, 0); 
            recordcount = [SELECT COUNT() FROM USCP_EFT__c];
            System.assertEquals(recordcount, 0);                            
        }            
            
    }

}