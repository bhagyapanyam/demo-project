/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Work Order Activity SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface VTT_WorkOrderActivityDAO {

    List<Work_Order_Activity__c> getRecord(Id workOrderActivityId);

}