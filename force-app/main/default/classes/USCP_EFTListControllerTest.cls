/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_EFTListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_EFT;
            Test.setCurrentPageReference(pageRef);


            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);
            //lets create another test terminal
            terminal =   USCP_TestData.createTerminal('trm2', 'addr2', 'city2', 'state2', 'opis2', true);
            System.AssertNotEquals(terminal.Id, Null);
            
/*            
            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
            //create another test product
            product = USCP_TestData.createProduct('product2',true);
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
*/
        
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);



            Date draftdate =  Date.today();
            //create test eft
            USCP_EFT__c eft = USCP_TestData.createEFT(account.Id, draftdate, 1000.0, true);
            //create another test eft            
            eft = USCP_TestData.createEFT(account.Id, draftdate, 500.0,  true);

            //create test invoice and link it to the second eft
            USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100', eft.id,  draftdate , draftdate , draftdate , 1000.0,  true);

            Date movementdate =  Date.today();
            Product2 product = USCP_TestData.createProduct('product1',true);
            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL1', invoice.id,  'Carrier 1', terminal.id,   movementdate , product.id ,  true);

            //now lets create controller
            USCP_EFTListController controller = new USCP_EFTListController();
            //with lazy load we should have 0 records           
            System.AssertEquals(0, controller.getEFTRecords().size());             
            //we should have 0 records   
            
            System.AssertEquals(0, controller.getWEFTRecords().size());             
            //we should have 0 records  
                    
            System.AssertEquals(0, controller.getAllEFTRecords().size());              

            //test account search method for autocomplete
            System.AssertEquals(1,USCP_EFTListController.searchAccount('100').size());



            //set search parameters for the controller
            controller.AccountID = account.Id;            
            controller.InvoiceNumber  = '100';
            controller.DateRangeType = '1';
            controller.BOLNumber= 'BOL1';
            controller.dateFrom = Date.Today()-1;
            controller.dateTo = Date.Today(); 
            System.AssertNotEquals(null,controller.dateFromStr);      
            System.AssertNotEquals(null,controller.dateToStr);            

            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getEFTRecords().size());   
            
            List<USCP_EFT__c> eftlist = controller.getEFTRecords();
            for(USCP_EFT__c obj: eftlist)
            {//should be just one record with invoice with amount of 1000
                for(USCP_Invoice__c invobj: obj.Invoices__r)
                {
                    System.AssertEquals('INV100', invobj.Name);  
                    System.AssertEquals(1000.0, invobj.Amount__c);  
                }    
            } 

            System.AssertNotEquals(null, controller.SaveToExcel());  

            controller.ClearFilter();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getEFTRecords().size());    
            
            // no records means no export to excel
            System.AssertEquals(null, controller.SaveToExcel());  
            
        }
    }

}