/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding EPD_Engine_Performance_Data__c SObject
Test Class:     EPD_EPDServiceTest
History:        jschn 2019-06-14 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EPDService {

    private static final String CLASS_NAME = String.valueOf(EPD_EPDService.class);

    /**
     * Builds EPD Form Structure based on EPD Records.
     * Works in bulk mode.
     * First it loads EPD records based on their IDs, then it creates Map of Block Information grouped by EPD Ids.
     * Last step creates structures.
     *
     * @param records
     *
     * @return List<EPD_FormStructure>
     */
    public List<EPD_FormStructure> getFormStructuresForEPDs(List<EPD_Engine_Performance_Data__c> records) {

        Set<Id> ids = HOG_GeneralUtilities.getSetOfIds(records);

        List<EPD_Engine_Performance_Data__c> queriedRecords = EPD_DAOProvider.EPDDAO.getEPDRecords(ids);

        Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId = new EPD_BlockInformationService().getBlockInformationByEPDId(ids);

        return new EPD_FormService().generateFormStructures(queriedRecords, blockInformationByEPDId);
    }

    /**
     * Checks if EPD Form is required for current Work Order.
     * Only PM work order which is done on Engine for which there is a configuration created.
     *
     * @param equipmentId
     * @param workOrder
     *
     * @return Boolean
     */
    public Boolean isEPDRequired(Id equipmentId, HOG_Maintenance_Servicing_Form__c workOrder) {
        System.debug(CLASS_NAME + ' -> isEPDRequired. Params: (ID) ' + equipmentId + ', (WO Type) ' + workOrder.Order_Type__c);

        Equipment_Engine__c engine = new EquipmentEngineService().getEngineByEquipmentId(equipmentId);

        System.debug(CLASS_NAME + ' -> isEPDRequired. Engine: ' + JSON.serialize(engine));

        EPD_Engine__mdt config;

        try{
            config = new EPD_EngineConfigService().getEngineConfig(engine.Manufacturer__c, engine.Model__c);
        } catch(HOG_Exception ex) {
            //This exception occurs when no config is found. Even if that happens we don't want this code to break code
            //flow. We are going to check if content was found with null check.
            System.debug(CLASS_NAME + ' -> isEPDRequired. Exception: ' + ex.getMessage());
        }

        System.debug(CLASS_NAME + ' -> isEPDRequired. Config: ' + JSON.serialize(config));
        System.debug(CLASS_NAME + ' -> isEPDRequired. Supported WO Types: ' + JSON.serialize(EPD_Constants.SUPPORTED_WO_ORDER_TYPES));
        System.debug(CLASS_NAME + ' -> isEPDRequired. Is Supported: ' + EPD_Constants.SUPPORTED_WO_ORDER_TYPES.contains(workOrder.Order_Type__c));

        return config != null && EPD_Constants.SUPPORTED_WO_ORDER_TYPES.contains(workOrder.Order_Type__c) && !workOrder.EPD_Suppressed__c;
    }

    /**
     * Sends emails with Threshold alert when there is EPD record that reached threshold
     *
     * @param workOrderIDs
     */
    public void sendThresholdAlertsForWOs(Set<Id> workOrderIDs) {
        Database.executeBatch(
                new EPD_EmailBatch(
                        new EPD_EmailThresholdAlert(),
                        EPD_DAOProvider.EPDDAO.getEPDRecordsByWorkOrderIds(workOrderIDs)
                ),
                10
        );
    }

    /**
     * Retrieves Id of latest EPD record on Engine.
     *
     * @param equipmentId
     *
     * @return Id
     */
    public Id getIdForLastEPDOnEngine(Id equipmentId) {
        List<EPD_Engine_Performance_Data__c> previousEPDs = EPD_DAOProvider.EPDDAO.getPreviousEPDByEngineId(equipmentId);
        Id previousEPDId;

        if(previousEPDs != null && previousEPDs.size() > 0) {
            previousEPDId = previousEPDs.get(0).Id;
        }

        return previousEPDId;
    }

}