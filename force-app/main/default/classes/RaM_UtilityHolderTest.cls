/****
*Class       -----     RaM_UtilityHolder
*Author      -----     Accenture
*Date        -----     23/1/2018
*Description -----     This test class will check the utility functions that will be reusable in any
*                      application across salesforce.
***/
@isTest
// test class for RaM_UtilityHolder
private class RaM_UtilityHolderTest{
    
    static testmethod void testUtilityHolder_01(){
        RaM_UtilityHolder.errorMethod('Candidate__c', 'TestClass', 'TestMethod', 'FATAL', 'NullPointerException' );
    } 
    
}