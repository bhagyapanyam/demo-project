public class VrmSNowSyncBatchScheduler implements Schedulable
{
    public void execute(SchedulableContext SC) 
    {
        VrmSNowSyncBatch bObj = new VrmSNowSyncBatch();
        
        Database.executeBatch(bObj);
    }
}