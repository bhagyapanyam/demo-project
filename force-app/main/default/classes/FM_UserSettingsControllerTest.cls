@isTest
private class FM_UserSettingsControllerTest {

	//
	private static Business_Unit__c testBusinessUnit;
	private static Business_Department__c testBusinessDepartment;
	private static Operating_District__c testOperatingDistrict;
	private static Field__c testField;
	private static Route__c testRoute;
	private static List<Location__c> testWellLocations;
	
	@isTest static void testEntireController() {
		User testUser = UserTestData.createTestsysAdminUser();
		insert testUser;
		assignPermissionSet(testUser, 'HOG_Read_Only');
		assignPermissionSet(testUser, 'HOG_Field_Operator');

		System.runAs(testUser) {
			SetupData();

			//Setup Page and Parameters
			Test.setCurrentPage(Page.FM_CustomizeFluid);
			ApexPages.currentPage().getParameters().put('route', testRoute.Name);

			//Initialize controller
			FM_UserSettingsController userSettingsController = new FM_UserSettingsController();

			//Test initialized controller variables
			System.assertEquals(userSettingsController.sRouteNum, testRoute.Name);
			System.assertEquals(userSettingsController.oFoundRoute.Id, testRoute.Id);
			System.assertEquals(userSettingsController.aAllWells.size(), testWellLocations.size());
			System.assertEquals(userSettingsController.aSelectedWells.size(), 0);

			//Select couple locations
			userSettingsController.aSelectedWells.add(userSettingsController.aAllWells[0]);
			userSettingsController.aSelectedWells.add(userSettingsController.aAllWells[1]);
			userSettingsController.UpdateWellOrder();

			//Test Settings
			FM_CustomWellOrder__c testCustomWellOrder = [Select Id, Route__c, User__c, WellIDList__c
														 From FM_CustomWellOrder__c
														 Where Route__c =: testRoute.Id
														 And User__c =: UserInfo.getUserId()];
			System.assertNotEquals(testCustomWellOrder, null);
			String testWellIDList = userSettingsController.aSelectedWells[0].getValue() + ',' + userSettingsController.aSelectedWells[1].getValue();
			System.assertEquals(testWellIDList, testCustomWellOrder.WellIDList__c);
		}
	}
	
	private static void SetupData() {
		//create test BU
        testBusinessUnit = FM_RunSheetTestData.createBusinessUnit('Test BU',  true);

        //create test BD            
        testBusinessDepartment = FM_RunSheetTestData.createBusinessDepartment('Test BD',  true);
        
        //create test OD
        testOperatingDistrict = FM_RunSheetTestData.createOperatingDistrict('Test OD', testBusinessDepartment.Id, testBusinessUnit.Id,  true) ;
        
        //create test field
        testField = FM_RunSheetTestData.createField('Test Field', testOperatingDistrict.Id, true) ;
        
        //create test route
        testRoute = FM_RunSheetTestData.createRoute('999', true);

        //create test well Locations
        testWellLocations = new List<Location__c>();
        testWellLocations.add(SetupLocation('Test Well 1', testField.Id, testRoute.Id, 'OIL', 'PROD'));
        testWellLocations.add(SetupLocation('Test Well 2', testField.Id, testRoute.Id, 'OIL', 'SHUT'));
        testWellLocations.add(SetupLocation('Test Well 2', testField.Id, testRoute.Id, 'OIL', 'STND'));
        testWellLocations.add(SetupLocation('Test Well 2', testField.Id, testRoute.Id, 'OIL', 'SUSP'));
        upsert testWellLocations;
	}

	private static Location__c SetupLocation(String pName, String pFieldId, String pRouteId, String pWellType, String pStatus) {
		Location__c testLocation = FM_RunSheetTestData.createLocation(pName, pFieldId, pRouteId, false, true);
        //we need location with category = 4
        testLocation.Functional_Location_Category__c =4;
        testLocation.Well_Type__c = pWellType;
        //and setup unit configuration
        testLocation.Unit_Configuration__c = 'T5X';
        testLocation.Status__c = pStatus;

        //Setup Tank
        SetupEquipmentTank(testLocation);

  		return testLocation;
	}

	private static void SetupEquipmentTank(Location__c pWellLocation) {
		//Equipment_Tank__c + Equipment__c
        Equipment__c testEquipment =  EquipmentTestData.createEquipment(pWellLocation);
        testEquipment.Equipment_Category__c = 'D';
        testEquipment.Object_Type__c = 'VESS_ATMOS';
        update testEquipment;

        Equipment_Tank__c testEquipmentTank = new Equipment_Tank__c();
        testEquipmentTank.Equipment__c = testEquipment.Id;
        testEquipmentTank.Low_Level__c = 10;
        testEquipmentTank.Tank_Size_m3__c = 30;
        insert testEquipmentTank;
	}

	private static void assignPermissionSet(User runningUser, String pPermissionSetName) {
		PermissionSet ps = [Select Id, Name From PermissionSet Where Name = :pPermissionSetName Limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = runningUser.id,PermissionSetId = ps.id);
        insert psa;
	}
}