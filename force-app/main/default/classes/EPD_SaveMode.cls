/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Modes for save actions for EPD Form
History:        jschn 2019-06-12 - Created. - EPD R1
*************************************************************************************************/
public enum EPD_SaveMode {

    SAVE,
    SUBMIT

}