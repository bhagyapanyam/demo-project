/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Maintenance_Servicing_Notification__c
-------------------------------------------------------------------------------------------------*/

public class MaintenanceServicingNotificationTestData
{
    public static List<HOG_Maintenance_Servicing_Notification__c> createMaintenanceServicingNotification
        (List<String> notificationNumbers)
    {        
        List<HOG_Maintenance_Servicing_Notification__c> results = new List<HOG_Maintenance_Servicing_Notification__c>();
            
        for (String notificationNumber : notificationNumbers)
            results.add(new HOG_Maintenance_Servicing_Notification__c(Notification_Number__c = notificationNumber));
        
        return results;
    }
}