/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Equipment SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface EquipmentDAO {

    List<Equipment__c> getEquipments(Set<Id> equipmentIds);

}