/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
Description:    E-Log SObject field defaults for Testing purposes.
History:        mbrimus 01.07.2019 - Created.
****************************************************************************************************
WARNING.. WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
****************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but dont change any values these, if you have to then rerun these tests

ELG_UtilitiesTest
**************************************************************************************************/
@IsTest
public with sharing class ELG_FieldDefaults {
    public static final String CLASS_NAME = 'ELG_FieldDefaults';

    public class ELG_Handover_CategoryDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_Handover_Category__c.RecordType_DeveloperName__c => 'Generic',
                    ELG_Handover_Category__c.Order__c => 1,
                    ELG_Handover_Category__c.Icon__c => 'action:something',
                    ELG_Handover_Category__c.External_ID__c => '1',
                    ELG_Handover_Category__c.Name => 'Testing Question Category'

            };
        }
    }

    public class ELG_Handover_Category_QuestionDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_Handover_Category_Question__c.Question__c => 'Are you not entertained?',
                    ELG_Handover_Category_Question__c.Order__c => 1,
                    ELG_Handover_Category_Question__c.Active__c => true
            };
        }
    }

    public class ELG_Log_AcknowledgmentDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
            };
        }
    }

    public class ELG_Log_EntryDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_Log_Entry__c.User_Log_Entry__c => 'Captains log, Stardate 4211.4. Keeping our presence here ' +
                            'secret... is an enormous tactical advantage. Therefore, I cannot risk contact with ' +
                            'Starfleet Command. I must take action on my own judgment. I ve elected to violate ' +
                            'orders... and make contact with planet inhabitants here.',
                    ELG_Log_Entry__c.User__c => UserInfo.getUserId()
            };
        }
    }

    public class ELG_PostDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_Post__c.Post_Name__c => 'Something'
            };
        }
    }

    public class ELG_Shift_AssignementDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_Shift_Assignement__c.Shift_Status__c => 'New'
            };
        }
    }

    public class ELG_Shift_HandoverDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{

            };
        }
    }

    public class ELG_User_SettingDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    ELG_User_Setting__c.User__c => UserInfo.getUserId()
            };
        }
    }

}