/**
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:    Test class for HOG_VG_Alert_Service on Alert Expired GOR Test from Location_Trigger that validates
 *  				if the Alert should be created or not and checks if new alert was created properly
 *  History:        Created on 9/27/2018   
 */

@isTest
public class HOG_VG_Alert_ServiceTest {

	@isTest
	static void singleAlertCreated6MonthsTest() {

		String todaysDate = String.valueOf(Date.today());
		Date expiredGorTestDate = Date.today()-1;
		Date addMonthsToDate = expiredGorTestDate.addMonths(-6);

		/*
		* 6 months has been added and new alert should be created based on Total Gas Productions field with
		* formula Measured_Vent_Rate__c + PVR_Fuel_Consumption_Rate__c on Location object that is 50 + 20,
		* the condition is more or equal that 1000 (based on Custom Setting that is GOR_Test_Upper_Threshold__c set to 1000)
		* to get 6 months
		* */
		Test.startTest();
		Location__c location = [SELECT Name FROM Location__c];
		location.Measured_Vent_Rate__c = 1000;
		location.GOR_Effective_Date__c = addMonthsToDate;
		location.recalculateFormulas();
		update location;
		String nameOfAlert = location.Name + ' - ' + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST + ' - ' + todaysDate;
		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, Status__c
											, Production_Engineer__c
											, Type__c
											, Priority__c
											, Start_Date__c
											, Well_Location__c
											, Exemption__c
											, Comments__c
									FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(nameOfAlert, alert.Name);
		System.assertEquals('Not Started',alert.Status__c);
		System.assertEquals('Expired GOR Test' ,alert.Type__c);
		System.assertEquals('Medium', alert.Priority__c);
		System.assertEquals(location.Id, alert.Well_Location__c);
	}

	@isTest
	static void singleAlertCreated12MonthsTest() {

		String todaysDate = String.valueOf(Date.today());
		Date expiredGorTestDate = Date.today()-1;
		Date addMonthsToDate = expiredGorTestDate.addMonths(-12);

		/*
		* Twelve months has been added and new alert should be created based on predifined value
		* in testSetup for Total Gas Productions field with  formula Measured_Vent_Rate__c + PVR_Fuel_Consumption_Rate__c
		* on Location object that is 100 + 20, the condition is more that 100 and lower than 1000
		* (based on Custom Setting GOR_Test_Lower_Threshold__c and GOR_Test_Upper_Threshold__c that is set to 100 and 1000)
		* to get 12 months
		* */
		Test.startTest();
		Location__c location = [SELECT Name FROM Location__c];
		location.GOR_Effective_Date__c = addMonthsToDate;
		update location;
		location.recalculateFormulas();
		String nameOfAlert = location.Name + ' - ' + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST + ' - ' + todaysDate;
		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, Status__c
											, Production_Engineer__c
											, Type__c
											, Priority__c
											, Start_Date__c
											, Well_Location__c
											, Exemption__c
											, Comments__c
									FROM HOG_Vent_Gas_Alert__c];

		Test.stopTest();

		System.assertEquals(nameOfAlert, alert.Name);
		System.assertEquals('Not Started',alert.Status__c);
		System.assertEquals('Expired GOR Test' ,alert.Type__c);
		System.assertEquals('Medium', alert.Priority__c);
		System.assertEquals(location.Id, alert.Well_Location__c);
	}

	@isTest
	static void singleAlertCreated36MonthsTest() {

		String todaysDate = String.valueOf(Date.today());
		Date expiredGorTestDate = Date.today()-1;
		Date addMonthsDate = expiredGorTestDate.addMonths(-36);

		/*
		* 36 months has been added and new alert should be created based on Total Gas Productions field with
		* formula Measured_Vent_Rate__c + PVR_Fuel_Consumption_Rate__c on Location object that is 50 + 20,
		* (based on Custom Setting GOR_Test_Lower_Threshold__c that is set to 100) the condition is less that 100 to get 36 months
		* */
		Test.startTest();
		Location__c location = [SELECT Name FROM Location__c];
		location.Measured_Vent_Rate__c = 50;
		location.GOR_Effective_Date__c = addMonthsDate;
		location.recalculateFormulas();
		update location;

		String nameOfAlert = location.Name + ' - ' + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST + ' - ' + todaysDate;
		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, Status__c
											, Production_Engineer__c
											, Type__c
											, Priority__c
											, Start_Date__c
											, Well_Location__c
											, Exemption__c
											, Comments__c
									FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(nameOfAlert, alert.Name);
		System.assertEquals('Not Started',alert.Status__c);
		System.assertEquals('Expired GOR Test' ,alert.Type__c);
		System.assertEquals('Medium', alert.Priority__c);
		System.assertEquals(location.Id, alert.Well_Location__c);
	}

	@isTest
	static void singleAlertNotCreatedBasedOnWrongValidation() {

		Date expiredGorTestDate = Date.today()-1100;

		Test.startTest();
		Location__c location = [SELECT Name FROM Location__c];
		location.Functional_Location_Category__c = 3;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		update location;
		location.recalculateFormulas();
		try {
			HOG_Vent_Gas_Alert__c alert = [SELECT Id FROM HOG_Vent_Gas_Alert__c];
		}
		catch (Exception e) {
			System.assert(e.getMessage().contains('List has no rows for assignment to SObject'));
		}
		Test.stopTest();
	}

	@testSetup
	static void setData() {
		HOG_Vent_Gas_Alert_Configuration__c customSettings= new HOG_Vent_Gas_Alert_Configuration__c();
		insert customSettings;
		HOG_VentGas_TestData.createLocationForAlert(true);
	}
}