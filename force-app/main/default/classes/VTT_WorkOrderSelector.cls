/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class that implements DAO interface(s) and provides implementation
                for methods that selects records.
Test Class:     VTT_WorkOrderSelectorTest
                VTT_WorkOrderSelectorTestNP
History:        jschn 22/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WorkOrderSelector implements VTT_WOA_ListViewDAO {

    private static final String CLASS_NAME = String.valueOf(VTT_WorkOrderSelector.class);

    private static final String QUERY_BASE_LIST_VIEW = 'SELECT ' +
                    'id, Name, Order_Type__c, Work_Order_Priority_Number__c, User_Status_Code__c, Plant_Section__c, ' +
                    'ALT_Confirmed__c, Service_Status__c, Functional_Location_Description_SAP__c, ' +
                    'Well_Event__c, Yard__c, Functional_Equipment_Level__c, Sub_System__c, System__c, Location__c, ' +
                    'Facility__c, Operating_Field_AMU_Lookup__c, Well_Event__r.Name, Yard__r.Name, ' +
                    'Functional_Equipment_Level__r.Name, Sub_System__r.Name, System__r.Name, Location__r.Name, ' +
                    'Facility__r.Name,Operating_Field_AMU_Lookup__r.Name, Main_Work_Centre__c, Order_Description__c, ' +
                    'RecordTypeId, Work_Order_Number__c, VTT_Activities_Count__c, ' +
                    '(' +
                            'SELECT Id, Name, Assigned_Vendor__c, Assigned_Vendor__r.Name, Description__c, ' +
                                    'Work_Center__c, Assigned_Text__c, Maintenance_Work_Order__r.Location__r.Name, ' +
                                    'Maintenance_Work_Order__r.Plant_Section__c, Scheduled_Start_Date__c, Status__c, ' +
                                    'Status_Reason__c, Maintenance_Work_Order__r.Order_Type__c, ' +
                                    'Maintenance_Work_Order__r.Work_Order_Priority_Number__c, ' +
                                    'Maintenance_Work_Order__r.User_Status_Code__c, Maintenance_Work_Order__c, ' +
                                    'Maintenance_Work_Order__r.Name, Functional_Location_Description_SAP__c, ' +
                                    'Location_Name__c, Maintenance_Work_Order__r.Location__r.Hazardous_H2S__c, ' +
                                    'Location__r.Hazardous_H2S__c, Priority__c ' +
                            'FROM Work_Order_Activities__r {0}{1}' + //{0} innerWhereClause; {1} sorting
                    ')' +
            ' FROM HOG_Maintenance_Servicing_Form__c' +
            ' WHERE Id IN (' +
                    'SELECT Maintenance_Work_Order__c ' +
                    'FROM Work_Order_Activity__c {2}' + //{2} innerWhereClause
            '){3}' + //{3} outerWhereClause
            ' ORDER BY {4} NULLS LAST' + //{4} Sorting
            ' LIMIT {5}' + //{5} queryLimit
            ' OFFSET {6}'; //{6} offset

    /**
     * This implementation will use BASE QUERY String and append all parts necessary for query proper records.
     *
     * @param filter
     * @param sorting
     * @param offset
     * @param queryLimit
     *
     * @return List<HOG_Maintenance_Servicing_Form__c>
     */
    public List<HOG_Maintenance_Servicing_Form__c> getRecordsByFilter(VTT_WOA_SearchCriteria filter, String sorting, Integer offset, Integer queryLimit) {
        System.debug(CLASS_NAME + ' -> getRecordsByFilter. sorting: ' + sorting);
        System.debug(CLASS_NAME + ' -> getRecordsByFilter. offset: ' + offset);
        System.debug(CLASS_NAME + ' -> getRecordsByFilter. queryLimit: ' + queryLimit);
        String innerWhereClause = filter.buildInnerWhereClause();
        String queryString = String.format(
                QUERY_BASE_LIST_VIEW,
                new List<String> {
                        innerWhereClause,
                        filter.getActivitySort(),
                        innerWhereClause,
                        filter.buildOuterWhereClause(),
                        sorting,
                        String.valueOf(queryLimit),
                        String.valueOf(offset)
                }
        );
        System.debug(CLASS_NAME + ' -> getRecordsByFilter. queryString: ' + queryString);
        return Database.query(queryString);
    }

}