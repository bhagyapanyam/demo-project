/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WOA_LogEntryDAOImpl
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_LogEntryDAOImplTest {

    @IsTest
    static void getLastLogEntryForTradesman_withoutId() {
        VTT_Utility_WOA_LogEntryDAO selector = new VTT_WOA_LogEntryDAOImpl();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedSize = 0;

        Test.startTest();
        result = selector.getLastLogEntryForTradesman(null);
        Test.stopTest();

        System.assertNotEquals(null, result, 'Query should never return null.');
        System.assertEquals(expectedSize, result.size(), 'Empty list is expected in this scenario');
    }

    @IsTest
    static void getLastLogEntryForTradesman_withDifferentId() {
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser){
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesmanContact = VTT_TestData.createTradesmanContact('Iron', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        VTT_Utility_WOA_LogEntryDAO selector = new VTT_WOA_LogEntryDAOImpl();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedSize = 0;

        Test.startTest();
        result = selector.getLastLogEntryForTradesman(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, result, 'Query should never return null.');
        System.assertEquals(expectedSize, result.size(), 'Empty list is expected in this scenario');
    }

    @IsTest
    static void getLastLogEntryForTradesman_withProperId() {
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser){
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesmanContact = VTT_TestData.createTradesmanContact('Iron', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        VTT_Utility_WOA_LogEntryDAO selector = new VTT_WOA_LogEntryDAOImpl();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedSize = 1;

        Test.startTest();
        result = selector.getLastLogEntryForTradesman(tradesmanContact.Id);
        Test.stopTest();

        System.assertNotEquals(null, result, 'Query should never return null.');
        System.assertEquals(expectedSize, result.size(), 'List with one record is expected in this scenario');
    }

}