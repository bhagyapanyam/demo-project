@isTest
private class VTT_MassCreateConfInSAPCtrlXTest {
	
	@isTest static void testSendDataToSAP() {
		User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;  
            VTT_TestData.SetupRelatedTestData(false);

            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);           

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;  
            update workOrder1;

            MaintenanceServicingUtilities.executeTriggerCode = true; 

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Operation_Number__c = '0010';
            update woActivity;

	        //Create Logs and Log Entries
	        VTT_Utilities.executeTriggerCode = false;
            List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
            	null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
            	null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_JOBONHOLD, null,
            	null, null, 'On Hold'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null,
            	null, null, 'Start Job Again'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
            	null, null, 'Start Job at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
            	null, null, 'Finish Job at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY, null,
                    null, null, 'Finish for the day'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
                    null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    null, null, 'Finish Job at Equipment'));
    	    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_JOBCOMPLETE, null,
            	null, null, 'Job Complete'));
    		insert logEntryList;

    		//Create confirmations
    		List<Work_Order_Activity_Confirmation__c> confirmationList = new List<Work_Order_Activity_Confirmation__c>();
    		confirmationList.add(new Work_Order_Activity_Confirmation__c(
                        End_Log_Entry__c = logEntryList[2].Id,
                        Work_Order_Activity_Log__c = logEntryList[2].Work_Order_Activity_Log_Lookup__c,
                        Work_Order_Activity__c = logEntryList[2].Work_Order_Activity__c,
                        Work_Finish__c = logEntryList[2].TimeStamp__c,
                        Confirmation_Long_Text__c = logEntryList[2].Comments__c,
                        Final_Confirmation__c = false,
                        Start_Log_Entry__c = logEntryList[1].Id,
                        Work_Start__c = logEntryList[1].TimeStamp__c,
                        Status__c = VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR
                    ));
    		confirmationList.add(new Work_Order_Activity_Confirmation__c(
                        End_Log_Entry__c = logEntryList[6].Id,
                        Work_Order_Activity_Log__c = logEntryList[6].Work_Order_Activity_Log_Lookup__c,
                        Work_Order_Activity__c = logEntryList[6].Work_Order_Activity__c,
                        Work_Finish__c = logEntryList[6].TimeStamp__c,
                        Confirmation_Long_Text__c = logEntryList[6].Comments__c,
                        Final_Confirmation__c = false,
                        Start_Log_Entry__c = logEntryList[4].Id,
                        Work_Start__c = logEntryList[4].TimeStamp__c,
                        Status__c = VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR
                    ));
    		confirmationList.add(new Work_Order_Activity_Confirmation__c(
                        End_Log_Entry__c = logEntryList[10].Id,
                        Work_Order_Activity_Log__c = logEntryList[10].Work_Order_Activity_Log_Lookup__c,
                        Work_Order_Activity__c = logEntryList[10].Work_Order_Activity__c,
                        Work_Finish__c = logEntryList[10].TimeStamp__c,
                        Confirmation_Long_Text__c = logEntryList[10].Comments__c,
                        Final_Confirmation__c = true,
                        Start_Log_Entry__c = logEntryList[8].Id,
                        Work_Start__c = logEntryList[8].TimeStamp__c,
                        Status__c = VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR
                    ));
    		insert confirmationList;

    		VTT_Utilities.executeTriggerCode = true;
        }

        //Test
        Test.startTest();
            Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
	        List<Work_Order_Activity_Confirmation__c> confirmationList = [Select Id, Confirmation_Text__c, Final_Confirmation__c, Start_Log_Entry__r.Status__c,
	                                                                             End_Log_Entry__r.Status__c, Status__c, Tradesman__c, Work_Order_Activity__c, 
	                                                                             Confirmation_Number__c, Confirmation_Log__c, Actual_Work__c, Work_Start__c, 
                                                                                 Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
                                                                                 Work_Order_Activity__r.Operation_Number__c, Work_Finish__c,
                                                                                 Work_Order_Activity__r.Description__c, Confirmation_Long_Text__c,
                                                                                 Work_Order_Activity__r.System_Status__c,
                                                                                 Work_Order_Activity__r.Work_Center__c,
                                                                                 Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c,
                                                                                 Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Maintenance_Plant__c
	                                                                      From Work_Order_Activity_Confirmation__c];
	        HOG_Maintenance_Servicing_Form__c workOrder = [Select Id, Work_Order_Number__c From HOG_Maintenance_Servicing_Form__c];
	        Test.setCurrentPage(Page.VTT_MassCreateConfirmationsInSAP);
	        ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(confirmationList);
            stdController.setSelected(confirmationList);
	        VTT_MassCreateConfirmationsInSAPCtrlX extController = new VTT_MassCreateConfirmationsInSAPCtrlX(stdController);

            //Test
	        extController.sendDataToSAP();

            //For Callout Exception coverage
            extController.sendDataToSAP();
	    Test.stopTest();

        confirmationList = [Select Id, Confirmation_Log__c
                            From Work_Order_Activity_Confirmation__c];
        System.assertEquals(confirmationList.size(), 3);
        String testString = 'E - ' + workOrder.Work_Order_Number__c + ' not found.';
        //for(Work_Order_Activity_Confirmation__c conf : confirmationList) {
        //	System.assert(conf.Confirmation_Log__c.contains(testString));
        //}
	}
	
}