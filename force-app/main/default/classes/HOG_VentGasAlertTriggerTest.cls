/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VentGasAlertTrigger.trigger
History:        jschn 04.26.2018 - Created.
				jschn 2020-01-23 - US 001742 & 001751 - updated coverage
**************************************************************************************************/
@isTest
private class HOG_VentGasAlertTriggerTest {
	
	@isTest static void singleAlert() {
		HOG_Vent_Gas_Alert__c alert = [SELECT Id, Status__c FROM HOG_Vent_Gas_Alert__c WHERE Name = 'lonelyAlert'].get(0);
		Test.startTest();
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_COMPLETED;
		try{
			update alert;
		} catch(Exception ex) {
			System.assert(ex.getMessage().contains('Status__c'));
			System.assert(ApexPages.hasMessages());
			System.assertEquals(1, ApexPages.getMessages().size());
		}

		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE;
		update alert;
		System.assertEquals(1, ApexPages.getMessages().size());
		Test.stopTest();
	}

	@isTest static void parentAlert() {
		HOG_Vent_Gas_Alert__c alert = [SELECT Id, Status__c FROM HOG_Vent_Gas_Alert__c WHERE Name = 'parentAlert'].get(0);
		Test.startTest();
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE;
		try{
			update alert;
		} catch(Exception ex) {
			System.assert(ex.getMessage().contains('Status__c'));
			System.assert(ApexPages.hasMessages());
			System.assertEquals(1, ApexPages.getMessages().size());
		}

		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_COMPLETED;
		try{
			update alert;
		} catch(Exception ex) {
			System.assert(ex.getMessage().contains('Status__c'));
			System.assert(ApexPages.hasMessages());
			System.assertEquals(2, ApexPages.getMessages().size());
		}

		List<HOG_Vent_Gas_Alert_Task__c> tasks = [SELECT Id, Status__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Vent_Gas_Alert__c =: alert.Id];
		for(HOG_Vent_Gas_Alert_Task__c t : tasks) t.Status__c = HOG_VentGas_Utilities.TASK_STATUS_COMPLETED;
		update tasks;

		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_COMPLETED;
		update alert;
		System.assert(ApexPages.hasMessages());
		System.assertEquals(2, ApexPages.getMessages().size());
		Test.stopTest();
	}

	//jschn 2020-01-23 - US 001742
	@IsTest
	static void insertEngineeringNotificationAlert() {
		Location__c location = [SELECT Id FROM Location__c LIMIT 1];
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		Integer originalCount = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];

		Test.startTest();
		HOG_VentGas_TestData.createAlert('',
				'TestAlertDescription',
				location.Id,
				usr.Id,
				HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
				HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF);
		Test.stopTest();

		HOG_Vent_Gas_Alert__c newAlert = [
				SELECT Name, Start_Date__c
				FROM HOG_Vent_Gas_Alert__c
				WHERE Type__c = :HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF
				LIMIT 1
		];
		System.assertEquals(originalCount + 1, [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c]);
		System.assertNotEquals(null, newAlert);
		System.assert(String.isNotBlank(newAlert.Name), 'Name should not be blank as trigger is handling name creation for this type');
		System.assertNotEquals(null, newAlert.Start_Date__c, 'Start Date should be populated as trigger is handling this logic');
	}

	//jschn 2020-01-23 - US 001742
	@IsTest
	static void insertEngineeringNotificationAlert_failOnNotificationMocked() {
		Location__c location = [SELECT Id FROM Location__c LIMIT 1];
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		Integer originalCount = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		HOG_VentGas_Utilities.alertTriggerDAO = new AlertDAOMockedToFail();

		Test.startTest();
		HOG_VentGas_TestData.createAlert('',
				'TestAlertDescription',
				location.Id,
				usr.Id,
				HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
				HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF);
		Test.stopTest();

		HOG_Vent_Gas_Alert__c newAlert = [
				SELECT Name, Start_Date__c
				FROM HOG_Vent_Gas_Alert__c
				WHERE Type__c = :HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF
				LIMIT 1
		];
		System.assertEquals(originalCount + 1, [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c]);
		System.assertNotEquals(null, newAlert);
		System.assert(String.isNotBlank(newAlert.Name), 'Name should not be blank as trigger is handling name creation for this type');
		System.assertNotEquals(null, newAlert.Start_Date__c, 'Start Date should be populated as trigger is handling this logic');
	}

	class AlertDAOMockedToFail implements VG_AlertTriggerDAO {
		public List<HOG_Vent_Gas_Alert__c> getAlertsForTriggerByType(Set<Id> ids, Set<String> types) {
			throw new HOG_Exception('Failing on purpose of testing catch part of test.');
		}
	}

	@testSetup static void prepareData() {
		HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c();
		insert config;

		Location__c location = HOG_VentGas_TestData.createLocation();
		User usr = HOG_VentGas_TestData.createUser();

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert('parentAlert', 
																	'TestAlertDescription', 
																	location.Id, 
																	usr.Id, 
																	HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																	HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																	HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_Vent_Gas_Alert_Task__c alertTask = HOG_VentGas_TestData.createTask(alert.Id, 
																			usr.Id, 
																			null, 
																			'TestTaskComments1', 
																			alert.Priority__c, 
																			true, 
																			HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																			HOG_VentGas_Utilities.ASSIGNEE_TYPE_OPERATIONS_ENGINEER);
		alertTask = HOG_VentGas_TestData.createTask(alert.Id, 
													usr.Id, 
													null, 
													'TestTaskComments2', 
													alert.Priority__c, 
													true, 
													HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
													HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);

		HOG_VentGas_TestData.createAlert('lonelyAlert', 
										'TestAlertDescription', 
										location.Id, 
										usr.Id, 
										HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
										HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
										HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);
	}
	
}