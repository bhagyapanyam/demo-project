@isTest
private class SM_ImportVendorTicketsControllerTest {
    
    /////////////////
    //* CONSTANTS *//
    /////////////////

    private static final Integer SAND_STING_COUNT = 3;

    // ********************************** //
    // ** TEST CREATING A SAND REQUEST ** //
    // ********************************** //

    @isTest static void test_method_one() {
        // Implement test code
    }
    
    @isTest static void FindCollumnIndexesTest() {
        // Implement test code
        String headerLine = 'Sand Request ID Number,Vendor Name,Vendor Invoice #,Vendor Ticket Date,Vendor Ticket #,Vendor Truck Type,Vendor Unit #,Vendor Ticket Hours,Vendor Load Volume (m3),Disposal Location,Disposal Location Invoice #,Disposal Location Invoiced Volume (m3)';
        SM_ImportVendorTicketsController.FindCollumnIndexes(headerLine);
        System.assertEquals(0, SM_ImportVendorTicketsController.COLLUMN_INDEX_SAND_REQUEST);
        System.assertEquals(1, SM_ImportVendorTicketsController.COLLUMN_INDEX_VENDOR);
        System.assertEquals(2, SM_ImportVendorTicketsController.COLLUMN_INDEX_INVOICE_NUMBER);
        System.assertEquals(3, SM_ImportVendorTicketsController.COLLUMN_INDEX_TICKET_DATE);
        System.assertEquals(4, SM_ImportVendorTicketsController.COLLUMN_INDEX_TICKET_NUMBER);
        System.assertEquals(5, SM_ImportVendorTicketsController.COLLUMN_INDEX_TRUCK_TYPE);
        System.assertEquals(6, SM_ImportVendorTicketsController.COLLUMN_INDEX_UNIT);
        System.assertEquals(7, SM_ImportVendorTicketsController.COLLUMN_INDEX_TICKET_HOURS);
        System.assertEquals(8, SM_ImportVendorTicketsController.COLLUMN_INDEX_TICKET_VOLUME);
        System.assertEquals(9, SM_ImportVendorTicketsController.COLLUMN_INDEX_DISPOSAL_LOCATION);
        System.assertEquals(10, SM_ImportVendorTicketsController.COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICE);      
        System.assertEquals(11, SM_ImportVendorTicketsController.COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICED_VOLUME);              
    }
    
    @isTest static void GetValueMethodsTest() {
        String[] csvRecordData = new String[]{'TEST1','08/01/2015','5.5'};
        System.assertEquals(csvRecordData[0], SM_ImportVendorTicketsController.GetStringValue(csvRecordData, 0));
        System.assertEquals(Date.newInstance(2015, 8, 1), SM_ImportVendorTicketsController.GetDateValue(csvRecordData, 1));         
        System.assertEquals(5.5, SM_ImportVendorTicketsController.GetDecimalValue(csvRecordData, 2));               
    }


    //test parseDate method test 12
    @isTest static void ParseDateTest() {
        System.assertEquals(null, SM_ImportVendorTicketsController.parseDate(null));
        System.assertEquals(null, SM_ImportVendorTicketsController.parseDate('15/1/2015'));                  
        System.assertEquals(Date.newInstance(2015, 8, 1), SM_ImportVendorTicketsController.parseDate('8/1/2015'));          
        System.assertEquals(Date.newInstance(2015, 8, 1), SM_ImportVendorTicketsController.parseDate('2015-8-1'));                  
    }

    @isTest static void ControllerTest() {

        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        /////////////////////////////////

        List<SM_Run_Sheet__c> lSandRunSheets = GetSandHeaderObjects();
        List<SM_Run_Sheet_Detail__c> lSandDetails = GetSandDetailObjects();
        List<Location__c> lLocations = GetLocationData();

        Location__c oLocation1 = lLocations[0];
        Location__c oLocation2 = lLocations[1];

        Contact oContact = [SELECT Id FROM Contact LIMIT 1];
        User oUser = GetTestUser();

        /////////////////////////////////////
        //* MISC VARIABLES FOR ASSIGNMENT *//
        /////////////////////////////////////

        SM_Run_Sheet__c oSandRunSheet = lSandRunSheets[0];
        List<SM_Run_Sheet_Detail__c> lTankData;
        List<SelectOption> lSelectTest;
        PageReference oPage;
        Facility__c facility1 = [select id, name from Facility__c where Sand_Destination_Ind__c = true limit 1][0];
        Date dToday = Date.today();


        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();


        System.runAs(oUser)
        {

            SM_ImportVendorTicketsController ctrl = new SM_ImportVendorTicketsController();

            String[] csvLines = new String[]{
            'Sand Request ID Number,Vendor Name,Vendor Invoice #,Vendor Ticket Date,Vendor Ticket #,Vendor Truck Type,Vendor Unit #,Vendor Ticket Hours,Vendor Load Volume (m3),Disposal Location,Disposal Location Invoice #,Disposal Location Invoiced Volume (m3)',
            'Request for Tank 1,Clean Harbors,439897,8/1/2015,420016,Semi Vac,4238,2.5,1.08,Facility 1,,',
            'Request for Tank 1,FieldTek,439897,8/1/2015,439425,Pressure Truck,4253,2.5,0,TEST Location,,',
            'Request for Tank 2,FieldTek,439927,8/1/2015,430460,Self Contained,4246,2.5,14,Facility 1,456852,13.75',
            'Request for Tank 2,FieldTek,439927,8/1/2015,430461,Self Contained,4246,2.5,12.27,Facility 1,456852,12.5',
            'Request for Tank 3,FieldTek,439927,8/1/2015,430462,Self Contained,4246,3,10.77,Facility 1,456852,10.7',
            'Bad Request,,440021,88/33/2015,,Self Contained,4265,33.5,999.5,Facility 2,440021,999.5'
            };
            string strCSV =  String.join( csvLines, '\n' );
            Blob fileBody = Blob.valueof(strCSV);

            ctrl.csvFileBody = fileBody;

            ctrl.importCSVFile();
            System.assertEquals(6, ctrl.ticketList.size());

            ctrl.importTicketList();
            //System.assertNotEquals(null, ctrl.ticketList[0].ticket.id);
            System.assertEquals(null, ctrl.ticketList[1].ticket.id);
            System.assert(ctrl.ticketList[1].isError);
            System.assert(ctrl.ticketList[1].errorMessage.contains('Unable to find Disposal Location:'));

            //System.assertNotEquals(null, ctrl.ticketList[2].ticket.id);
            //System.assertNotEquals(null, ctrl.ticketList[3].ticket.id);
            //System.assertNotEquals(null, ctrl.ticketList[4].ticket.id);

            //System.assertEquals(null, ctrl.ticketList[5].ticket.id);
            //System.assert(ctrl.ticketList[5].isError);
        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();        
    }
    //////////////////////////////////////////////
    //* DATA RETRIEVAL FUNCTIONS --> TEST DATA *//
    //////////////////////////////////////////////

    private static List<SM_Run_Sheet__c> GetSandHeaderObjects()
    {
        return [SELECT Id, Date__c, Well_Location__r.Route__r.Name FROM SM_Run_Sheet__c];
    }

    private static List<SM_Run_Sheet_Detail__c> GetSandDetailObjects()
    {
        return [SELECT Id, Name, Blow_off_location__c, Chem_and_Rate__c, 
            Comments__c, Flood_Up__c, Flowline_push__c, Location_for_Water_Tank__c, 
            Location_for_Water_Well__c, Priority__c, Sand_Level__c, Sand_Run_Sheet__c, Status__c, 
            Sting_Type__c, Tank__c, Tank_Enabled__c, Tank_Level__c, Trigger_Bypass__c, 
            Vendor_Date_given_out__c, Water_Level__c, Work_Order__c,
            Primary_Vendor__c,
            Primary_Disposal_Facility__c 
            FROM SM_Run_Sheet_Detail__c];
    }

    private static List<Location__c> GetLocationData()
    {
        return [SELECT Id, Route__c, Route__r.Name, Location_Name_For_Fluid__c FROM Location__c];
    }

    private static User GetTestUser()
    {
        return [SELECT Id FROM User WHERE Name = 'Guy Incognito' AND Alias = 'guyincog' AND Email = 'guy.incognito@testingsandtestclasses.com' LIMIT 1];
    }

    private static List<SM_Run_Sheet_Detail__c> BuildSampleTankData(SM_Run_Sheet__c oSandRequest, Location__c oLocation)
    {
        List<SM_Run_Sheet_Detail__c> lSampleTankData = new List<SM_Run_Sheet_Detail__c>();
        SM_Run_Sheet_Detail__c oTank1 = new SM_Run_Sheet_Detail__c();
        SM_Run_Sheet_Detail__c oTank2 = new SM_Run_Sheet_Detail__c();
        SM_Run_Sheet_Detail__c oTank3 = new SM_Run_Sheet_Detail__c();

        oTank1.Sand_Run_Sheet__c = oSandRequest.Id;
        oTank2.Sand_Run_Sheet__c = oSandRequest.Id;
        oTank3.Sand_Run_Sheet__c = oSandRequest.Id;

        oTank1.Tank_Enabled__c = true;
        oTank2.Tank_Enabled__c = true;
        oTank3.Tank_Enabled__c = true;

        oTank1.Tank__c = 'Tank 1';
        oTank2.Tank__c = 'Tank 2';
        oTank3.Tank__c = 'Tank 3';

        oTank1.Sting_Type__c = 'Sting-to-Air';
        oTank2.Sting_Type__c = 'Production';
        oTank3.Sting_Type__c = 'Door Pull';

        oTank1.Tank_Level__c = 501;
        oTank2.Tank_Level__c = 412;
        oTank3.Tank_Level__c = 306;

        oTank1.Sand_Level__c = 15;
        oTank2.Sand_Level__c = 9;
        oTank3.Sand_Level__c = 6;

        oTank1.Water_Level__c = 150;
        oTank2.Water_Level__c = 5;
        oTank3.Water_Level__c = 3;

        oTank1.Priority__c = '1';
        oTank2.Priority__c = '2';
        oTank3.Priority__c = '3';

        oTank1.Chem_and_rate__c = 'glaiven';
        oTank2.Chem_and_rate__c = 'floiven';
        oTank3.Chem_and_rate__c = 'maiiven';

        oTank1.Location_For_Water_Well__c = oLocation.Id;
        oTank2.Location_For_Water_Well__c = oLocation.Id;
        oTank3.Location_For_Water_Well__c = oLocation.Id;

        oTank1.Location_For_Water_Tank__c = 'Tank 1';
        oTank2.Location_For_Water_Tank__c = 'Tank 2';
        oTank3.Location_For_Water_Tank__c = 'Tank 3';

        oTank1.Comments__c = 'Sample Production Tank';
        oTank2.Comments__c = 'Sample Sting-to-air Tank';
        oTank3.Comments__c = 'Sample Door Pull Tank';

        oTank1.Blow_off_location__c = 'A Well';
        oTank2.Blow_off_location__c = 'Another Well';
        oTank3.Blow_off_location__c = 'Some Well';

        lSampleTankData.add(oTank1);
        lSampleTankData.add(oTank2);
        lSampleTankData.add(oTank3);

        return lSampleTankData;
    }

    // ******************************************* //
    // ** CREATE ALL TEST DATA FOR TEST METHODS ** //
    // ******************************************* //

    private static void CreateTestData()
    {
        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Husky Account';
        insert oAccount;

        // Test Contact
        Contact oContact = new Contact();
        oContact.Account = oAccount;
        oContact.FirstName = 'Senator';
        oContact.LastName = 'McTest';
        oContact.X2Z_Employee_ID__c = 'Octothorpe';
        insert oContact;

        // Test Business Department
        Business_Department__c oBusiness = new Business_Department__c();
        oBusiness.Name = 'Husky Business';
        insert oBusiness;

        // Test Operating District
        Operating_District__c oDistrict = new Operating_District__c();
        oDistrict.Name = 'District 1234';
        oDistrict.Business_Department__c = oBusiness.Id;
        insert oDistrict;

        // Test AMU Field
        Field__c oField = new Field__c();
        oField.Name = 'AMU Field';
        oField.Operating_District__c = oDistrict.Id;
        insert oField;


        Facility__c ofac1 = new Facility__c();
        ofac1.Name = 'Facility 1';
        ofac1.Sand_Destination_Ind__c = true;
        ofac1.Operating_Field_AMU__c = oField.id;
        insert ofac1;

        Facility__c ofac2 = new Facility__c();
        ofac2.Name = 'Facility 2';
        ofac2.Sand_Destination_Ind__c = false;
        ofac2.Operating_Field_AMU__c = oField.id;
        insert ofac2;

        // Test Route 1
        Route__c oRoute1 = new Route__c();
        oRoute1.Fluid_Management__c = true;
        oRoute1.Name = '777';
        oRoute1.Route_Number__c = '777';
        insert oRoute1;

        // Test Route 2
        Route__c oRoute2 = new Route__c();
        oRoute2.Fluid_Management__c = true;
        oRoute2.Name = '999';
        oRoute2.Route_Number__c = '999';
        insert oRoute2;

        // Test Location 1
        Location__c oLocation1 = new Location__c();
        oLocation1.Name = 'Husky Well';
        oLocation1.Fluid_Location_Ind__c = true;
        oLocation1.Location_Name_for_Fluid__c = 'Husky Well Fluid';
        oLocation1.Operating_Field_AMU__c = oField.Id;
        oLocation1.Hazardous_H2s__c = 'Y';
        oLocation1.Functional_Location_Category__c = 4;
        oLocation1.Well_Type__c = 'OIL';
        oLocation1.Route__c = oRoute2.Id;
        insert oLocation1;

        // Test Location 2
        Location__c oLocation2 = new Location__c();
        oLocation2.Name = 'Husky WIG Well';
        oLocation2.Fluid_Location_Ind__c = true;
        oLocation2.Location_Name_for_Fluid__c = 'Husky Well Fluid Stuff';
        oLocation2.Operating_Field_AMU__c = oField.Id;
        oLocation2.Hazardous_H2s__c = 'N';
        oLocation2.Functional_Location_Category__c = 4;
        oLocation2.Well_Type__c = 'OIL';
        oLocation2.Route__c = oRoute2.Id;
        insert oLocation2;

        // Well Event Type
        List<RecordType> lRecordTypes = [SELECT Id FROM RecordType WHERE SobjectType = 'Location__c' AND DeveloperName = 'Well_Event' LIMIT 1];
        RecordType oRecordType = lRecordTypes[0];

        // Test Well Event 1
        Location__c oEvent1 = new Location__c();
        oEvent1.Name = 'Husky Well Event';
        oEvent1.RecordTypeId = oRecordType.Id;
        oEvent1.Operating_Field_AMU__c = oField.Id;
        oEvent1.Route__c = oRoute2.Id;
        oEvent1.Well_ID__c = oLocation1.Id;
        oEvent1.PVR_AVGVOL_30D_OIL__c = 111;
        oEvent1.PVR_AVGVOL_30D_WATER__c = 222;
        insert oEvent1;

        // Test Well Event 2
        Location__c oEvent2 = new Location__c();
        oEvent2.Name = 'Another Husky Well Event';
        oEvent2.RecordTypeId = oRecordType.Id;
        oEvent2.Operating_Field_AMU__c = oField.Id;
        oEvent2.Route__c = oRoute2.Id;
        oEvent2.Well_ID__c = oLocation2.Id;
        oEvent2.PVR_AVGVOL_30D_OIL__c = 123;
        oEvent2.PVR_AVGVOL_30D_WATER__c = 321;
        insert oEvent2;

        // Test Favorite Object
        SM_CustomWellOrder__c oFavorite = new SM_CustomWellOrder__c();
        oFavorite.Route__c = oRoute2.Id;
        oFavorite.User__c = UserInfo.getUserId();
        oFavorite.WellIDList__c = oLocation1.Id;
        insert oFavorite;

        // Test Sand Run Sheet
        SM_Run_Sheet__c oSandRunSheet = new SM_Run_Sheet__c();
        oSandRunSheet.Date__c = Date.today();
        oSandRunSheet.Well_Location__c = oLocation1.Id;
        oSandRunSheet.Sand_Lead__c = oContact.Id;
        insert oSandRunSheet;

        // Test Sand Run Sheet Details

        SM_Run_Sheet_Detail__c oSandRunSheetDetail;

        for(Integer iCount = 0; iCount < SAND_STING_COUNT; iCount++)
        {
            oSandRunSheetDetail = new SM_Run_Sheet_Detail__c();
            oSandRunSheetDetail.Priority__c = '2';
            oSandRunSheetDetail.Sand_Level__c = 5;
            oSandRunSheetDetail.Sand_Run_Sheet__c = oSandRunSheet.Id;
            oSandRunSheetDetail.Status__c = 'New';
            oSandRunSheetDetail.Sting_Type__c = 'Production';
            oSandRunSheetDetail.Tank__c = 'Tank ' + (iCount + 1);
            oSandRunSheetDetail.Tank_Enabled__c = true;
            oSandRunSheetDetail.Tank_Level__c = 5;
            oSandRunSheetDetail.Water_Level__c = 7;

            oSandRunSheetDetail.Name = 'Request for Tank ' + (iCount + 1);
            insert oSandRunSheetDetail;
        }

        // Test FLUID Run Sheet
        FM_Run_Sheet__c oFluidRunSheet = new FM_Run_Sheet__c();
        oFluidRunSheet.Date__c = Date.today();
        oFluidRunSheet.Well__c = oLocation2.Id;
        oFluidRunSheet.Act_Flow_Rate__c = 5;
        oFluidRunSheet.Act_Tank_Level__c = 3;
        oFluidRunSheet.Axle__c = '6';
        oFluidRunSheet.Flowline_volume__c = 4;
        oFluidRunSheet.Sour__c = true;
        oFluidRunSheet.Tank__c = 'Tank 2';
        oFluidRunSheet.Tomorrow_Oil__c = 1;
        insert oFluidRunSheet;

        List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];

        // Test USER
        User oUser = new User();
        oUser.FirstName = 'Guy';
        oUser.LastName = 'Incognito';
        oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testingsandtestclasses.com';
        oUser.UserName = oUser.Email;
        oUser.Alias = 'guyincog';
        oUser.TimeZoneSidKey = 'America/New_York';
        oUser.LocaleSidKey = 'en_US';
        oUser.EmailEncodingKey = 'ISO-8859-1'; 
        oUser.LanguageLocaleKey='en_US';
        oUser.ProfileId = lProfiles[0].Id;
        oUser.IsActive = true;
        insert oUser;
		
        // Test Picklist Manager data
        HOG_Picklist_Manager__c oPicklistManager = new HOG_Picklist_Manager__c();
        oPicklistManager.Picklist_Name__c = 'Unit Type';
        oPicklistManager.Picklist_Value__c = 'Pressure Truck';
        insert oPicklistManager;

        HOG_Picklist_Manager__c oPicklistManager2 = new HOG_Picklist_Manager__c();
        oPicklistManager2.Picklist_Name__c = 'Vendor';
        oPicklistManager2.Picklist_Value__c = 'Clean Harbors';
        insert oPicklistManager2;
        
        HOG_Picklist_Manager__c oPicklistManager3 = new HOG_Picklist_Manager__c();
        oPicklistManager3.Picklist_Name__c = 'Disposal';
        oPicklistManager3.Picklist_Value__c = 'Facility 1';
        insert oPicklistManager3;
        
        HOG_Picklist_Manager__c oPicklistManager4 = new HOG_Picklist_Manager__c();
        oPicklistManager4.Picklist_Name__c = 'Disposal';
        oPicklistManager4.Picklist_Value__c = 'Facility 2';
        insert oPicklistManager4;
        
        System.runAs(oUser)
        {
            // Permission Set Assignment - Operator
            PermissionSet oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = 'HOG_Field_Operator' LIMIT 1];
            PermissionSetAssignment oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
            insert oPermissionAssign;
        }
    }



}