/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_Rule_AssignedButNotWorking
Test Class:     VTT_LTNG_WorkFlowEngineTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public with sharing class VTT_Rule_AssignedButNotWorking implements VTT_LTNG_EngineRule {
    Work_Order_Activity__c activity;
    Contact tradesman;
    public Boolean hasEPDSubmitted;
    public Boolean isEPDFormRequired;
    public Boolean isEPDFormAvailable = false;

    Set<VTT_LTNG_EngineMessage> messages = new Set<VTT_LTNG_EngineMessage>();
    Set<VTT_LTNG_EngineAction> availableActions = new Set<VTT_LTNG_EngineAction>();
    Boolean passed = false;

    public VTT_Rule_AssignedButNotWorking(
            Work_Order_Activity__c activity,
            Contact tradesman,
            Boolean hasEPDSubmitted,
            Boolean isEPDFormRequired) {
        this.activity = activity;
        this.tradesman = tradesman;
        this.hasEPDSubmitted = hasEPDSubmitted;
        this.isEPDFormRequired = isEPDFormRequired;
    }

    public Boolean passedRule() {
        return this.passed;
    }

    public void processRule() {
        //this.availableActions.add(new VTT_LTNG_EngineAction('CanExecuteAction'));
        Boolean isAssigned = activity.Work_Order_Activity_Assignments__r.size() > 0;
        if (tradesman.Current_Work_Order_Activity__c == activity.Id
                && isAssigned) {
            // TODO check if we can ignore this function and go straight to else..
//            // TODO Change this...
//            this.messages.add('You are currently working on a different Activity.');
//            this.availableActions.add('CanRejectActivity');
//            this.passed = false;
//            CanRejectActivity = false;
//            CanCancelActivity = false;
        } else {
            // TODO CHANGE THIS
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.REJECT_ACTIVITY));
            //this.availableActions.add(new VTT_LTNG_EngineAction('CanCancelActivity'));
        }

        // Start Job Action
        this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB));

        // EPD
        if (!hasEPDSubmitted && isEPDFormRequired) {
            this.isEPDFormAvailable = true;
            this.availableActions.add(
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.EPD_LABEL)
            );
        }

        // Start Job At Equipment
        if (VTT_Utilities.IsThermalActivity(activity)) {
            this.availableActions.add(
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.START_JOB_AT_EQUIPMENT)
            );
        }
    }

    public Set<VTT_LTNG_EngineMessage> getMessages() {
        return messages;
    }

    public Set<VTT_LTNG_EngineAction> getAvailableActions() {
        return availableActions;
    }
}