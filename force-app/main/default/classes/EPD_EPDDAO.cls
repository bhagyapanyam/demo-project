/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Engine Performance Data SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface EPD_EPDDAO {

    List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId);
    List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds);
    List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds);
    Integer getCountOfSubmittedRecords(Id workOrderActivityId);
    List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId);

}