/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_PageTrackingControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_Invoices;
            Test.setCurrentPageReference(pageRef);
            
            //setup custom settings for tracking page visits
            USCP_Settings__c newUserVal =  new USCP_Settings__c();
            //newUserVal.setupOwnerId = runningUser.id;
            newUserVal.Track_page_visits_Internal_Users__c =  true;
            insert newUserVal;  
            

            USCP_PageTrackingController controller = new USCP_PageTrackingController();
            controller.LogPageVisit();
            List<USCP_PageTracking__c> pagevisits = [select id, name from USCP_PageTracking__c];
            System.AssertEquals(1, pagevisits.size());
                        
            controller.pagename = 'Test Page';
            controller.LogPageVisit2();
            pagevisits = [select id, name from USCP_PageTracking__c];
            System.AssertEquals(2, pagevisits.size());
            
            //we can log visits from USCP_PricingListController
            controller = new USCP_PageTrackingController(new USCP_PricingListController());
            System.AssertNotEquals(null, controller);
            
            //we can log visits from USCP_MSDS_Controller
            controller = new USCP_PageTrackingController(new USCP_MSDS_Controller());
            System.AssertNotEquals(null, controller);  
            
            //we can log visits from USCP_ALVController
            controller = new USCP_PageTrackingController(new USCP_ALVController());
            System.AssertNotEquals(null, controller); 
            
            //we can log visits from any class inherited from DynamicListController
            controller = new USCP_PageTrackingController(new USCP_BOLListController());
            System.AssertNotEquals(null, controller);                                   
        }
     }
}