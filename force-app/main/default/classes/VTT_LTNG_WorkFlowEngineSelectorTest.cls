/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_WorkFlowEngineSelector class
History:        mbrimus 14/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_WorkFlowEngineSelectorTest {

    @IsTest
    static void getActivitiesWithAssignment_withoutParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getActivitiesWithAssignment(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getActivitiesWithAssignment_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Work_Order_Activity__c activity;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
        }

        Test.startTest();
        result = selector.getActivitiesWithAssignment(activity.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmanContacts_withoutData() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Contact> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getTradesmanContacts();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmanContacts_withData() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Contact> result;
        Integer expectedCount = 1;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            result = selector.getTradesmanContacts();
        }
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getLastLogEntryForTradesmanAndActivity_withoutParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getLastLogEntryForTradesmanAndActivity(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getLastLogEntryForTradesmanAndActivity_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        Test.stopTest();
        result = selector.getLastLogEntryForTradesmanAndActivity(activity.Id, tradesmanContact.Id);

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getLatestVersionOfActivity_withoutParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        Work_Order_Activity__c result;

        Test.startTest();
        result = selector.getLatestVersionOfActivity(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getLatestVersionOfActivity_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        Work_Order_Activity__c result;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;

        Test.startTest();
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            result = VTT_TestData.createWorkOrderActivity(workOrder.Id);

        }
        Test.stopTest();
        result = selector.getLatestVersionOfActivity(result.Id);
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getCloseOutChecklistForToday_withoutParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Work_Execution_Close_Out_Checklist__c> result;
        Boolean throwException = false;

        Test.startTest();
        try {
            result = selector.getCloseOutChecklistForToday(null, null);
        } catch (Exception e) {
            throwException = true;
        }

        Test.stopTest();

        System.assertEquals(null, result);
        System.assertEquals(true, throwException);
    }

    @IsTest
    static void getCloseOutChecklistForToday_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Work_Execution_Close_Out_Checklist__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);

            HOG_Work_Execution_Close_Out_Checklist__c closeOutforToday = new HOG_Work_Execution_Close_Out_Checklist__c(
                    Permit_Holder_Name__c = tradesmanContact.Id,
                    Maintenance_Work_Order__c = workOrder.Id,
                    Company_Name_Lookup__c = vendor.Id,
                    Permit_Number__c = '314159265359'
            );
            insert closeOutforToday;
        }
        Test.stopTest();
        result = selector.getCloseOutChecklistForToday(activity, tradesmanContact);

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignedTradesman_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Contact> result;

        Integer expectedCount = 1;
        User tradesmanUser = VTT_TestData.createVTTUser();

        System.runAs(tradesmanUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Slovakia');
            Contact tradesman = VTT_TestData.createTradesmanContact('Is', 'Awesome', vendor.Id, tradesmanUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);

            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;

            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            result = selector.getAssignedTradesman(activity.Id, VTT_TestData.createVTTUser().Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignedTradesman_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Contact> result;

        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getAssignedTradesman(null, null);
        Test.stopTest();


        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignment_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Assignment__c> result;

        Integer expectedCount = 1;
        User tradesmanUser = VTT_TestData.createVTTUser();

        System.runAs(tradesmanUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Slovakia');
            Contact tradesman = VTT_TestData.createTradesmanContact('Is', 'Awesome', vendor.Id, tradesmanUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);

            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;

            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            result = selector.getNonRejectedAssignment(activity.Id, tradesman.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignment_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Assignment__c> result;

        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getNonRejectedAssignment(null, null);
        Test.stopTest();


        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOtherWorkingTradesman_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Contact> result;

        Boolean throwException = false;

        Test.startTest();
        try {
            result = selector.getOtherWorkingTradesman(null, null, null);
        } catch (Exception e) {
            throwException = true;
        }

        Test.stopTest();

        System.assertEquals(null, result);
        System.assertEquals(true, throwException);
    }

    @IsTest
    static void getAvailableActivitiesToComplete_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<VTT_LTNG_AvailableActivity> result;

        Boolean throwException = false;

        Test.startTest();
        try {
            result = selector.getAvailableActivitiesToComplete(null, null);
        } catch (Exception e) {
            throwException = true;
        }

        Test.stopTest();

        System.assertEquals(null, result);
        System.assertEquals(true, throwException);
    }

    @IsTest
    static void getAvailableActivitiesToComplete_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<VTT_LTNG_AvailableActivity> result;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            activity.User_Status__c = '2SCH FIX';
            update activity;
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Work_Order_Activity__c secondActivity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            secondActivity.User_Status__c = '2SCH FIX';
            update secondActivity;
            VTT_TestData.createWorkOrderActivityAssignment(secondActivity.Id, tradesman.Id);
            secondActivity = VTT_TestData.reloadWorkOrderActivity(secondActivity.Id);

            Test.startTest();
            result = selector.getAvailableActivitiesToComplete(tradesman, secondActivity);
            Test.stopTest();
        }
    }

    @IsTest
    static void getRunningTallyTime_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        Double result;


        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;
        List<Work_Order_Activity_Log__c> logs;

        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            Work_Order_Activity_Log_Entry__c entry = VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Start at Equipment');
            insert entry;

            logs = [
                    SELECT Id, WorkTime_New__c, (
                            SELECT Id, TimeStamp__c, StatusEnd__c
                            FROM Work_Order_Activity_Log_Entries2__r
                            WHERE Status__c = :'Start at Equipment'
                            AND StatusEnd__c = NULL
                            AND TimeStamp__c <> NULL
                            ORDER BY CreatedDate DESC
                            LIMIT 1
                    )
                    FROM Work_Order_Activity_Log__c
                    WHERE Id = : entry.Work_Order_Activity_Log_Lookup__c
                    AND Tradesman__r.User__c = :UserInfo.getUserId()
                    ORDER BY CreatedDate DESC
            ];
        }

        Test.startTest();
        result = selector.getRunningTallyTime(activity, logs);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getActivityLogs_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 1;

        User tradesman = VTT_TestData.createVTTUser();
        Work_Order_Activity__c activity;

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            Contact tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            Work_Order_Activity_Log_Entry__c entry = VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Start at Equipment');
            insert entry;
        }

        Test.startTest();
        result = selector.getActivityLogs(activity.Id, tradesman.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getActivityLogs_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getActivityLogs(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPartsByCatalogueCode_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Part__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getPartsByCatalogueCode(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPartsByCatalogueCode_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Part__c> result;
        Integer expectedCount = 1;

        HOG_Part__c equipmentPart = HOGPartTestData.createHOGPart('Test Part', 'H1', 'Test Part', 'H1', true);
        insert equipmentPart;

        Test.startTest();
        result = selector.getPartsByCatalogueCode('H1');
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getDamageByPartCode_withNoParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Damage__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getDamageByPartCode(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getDamageByPartCode_withParam() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Damage__c> result;
        Integer expectedCount = 1;

        HOG_Damage__c equipmentPartDamage = HOGDamageTestData.createHOGDamage('Test Damage', 'DMGECODE', 'Test Damage', 'H1', true);
        insert equipmentPartDamage;

        Test.startTest();
        result = selector.getDamageByPartCode('H1');
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getCause_withNoData() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Cause__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getCause();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getCause_withData() {
        VTT_LTNG_WorkFlowEngineDAO selector = new VTT_LTNG_WorkFlowEngineSelector();
        List<HOG_Cause__c> result;
        Integer expectedCount = 1;

        HOG_Cause__c equipmentPartDamageCause = HOGCauseTestData.createHOGCause('Test Cause', 'DMGECODE', 'Test Cause', 'CDEGROUP', true);
        insert equipmentPartDamageCause;

        Test.startTest();
        result = selector.getCause();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

}