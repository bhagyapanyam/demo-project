/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Parallel Unit Test for EPD_FormService
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormServiceTest {

    @IsTest
    static void handleLoadForExistingEPD_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void handleLoadForExistingEPD_withRecordParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(epd, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.success);
        System.assertNotEquals(null, result.errors);
        System.assertEquals(1, result.errors.size());
        System.assertEquals(Label.EPD_Response_Already_Submitted, result.errors.get(0));
    }

    @IsTest
    static void handleLoadForExistingEPD_withLoadModeParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(null, EPD_LoadMode.INDIRECT_LOAD);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void handleLoadForExistingEPD_alreadySubmitted() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(epd, EPD_LoadMode.INDIRECT_LOAD);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(false, result.success);
        System.assertNotEquals(null, result.errors);
        System.assertEquals(1, result.errors.size());
        System.assertEquals(Label.EPD_Response_Already_Submitted, result.errors.get(0));
    }

    @IsTest
    static void handleLoadForNewEPD_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForNewEPD(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void handleLoadForNewEPD_withWrongParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForNewEPD(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void handleLoadForNewEPD_mocked1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Response result;
        EPD_DAOProvider.workOrderActivityDAO = new WOAMock_for_handleLoadForNewEPD();
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMock_for_handleLoadForNewEPD();

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForNewEPD(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(true, result.success);
        System.assertNotEquals(null, result.formStructure);
    }

    @IsTest
    static void handleLoadForNewEPD_mocked2_fail() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Response result;
        EPD_DAOProvider.workOrderActivityDAO = new WOAMock_for_handleLoadForNewEPD();
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMock_for_handleLoadForNewEPD2();

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForNewEPD(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    public class WOAMock_for_handleLoadForNewEPD implements VTT_WorkOrderActivityDAO {
        public List<Work_Order_Activity__c> getRecord(Id workOrderActivityId) {
            Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName', null, null, false);
            woa.Maintenance_Work_Order__r = VTT_TestDataFactory.createWorkOrder(null, null, false);
            woa.Assigned_Vendor__r = (Account) HOG_SObjectFactory.createSObject(new Account(), false);
            System.debug(woa.Maintenance_Work_Order__r.Equipment__c);
            System.debug(woa.Equipment__c);
            return new List<Work_Order_Activity__c> {
                    woa
            };
        }
    }
    public class EquipmentEngineMock_for_handleLoadForNewEPD implements EquipmentEngineDAO {
        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(
                            new Equipment_Engine__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }
        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }
    public class EquipmentEngineMock_for_handleLoadForNewEPD2 implements EquipmentEngineDAO {
        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {};
        }
        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }

    @IsTest
    static void generateFormStructures_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_FormService().generateFormStructures(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void generateFormStructures_withEmptyMap() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId = new Map<Id, List<EPD_Block_Information__c>>();
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_FormService().generateFormStructures(null, blockInformationByEPDId);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void generateFormStructures_withEmptyList() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Engine_Performance_Data__c> epdRecords = new List<EPD_Engine_Performance_Data__c>();
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_FormService().generateFormStructures(epdRecords, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void generateFormStructures_withEmptyParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId = new Map<Id, List<EPD_Block_Information__c>>();
        List<EPD_Engine_Performance_Data__c> epdRecords = new List<EPD_Engine_Performance_Data__c>();
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_FormService().generateFormStructures(epdRecords, blockInformationByEPDId);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void generateFormStructures_withProperRecords() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId = new Map<Id, List<EPD_Block_Information__c>> {
                null => new List<EPD_Block_Information__c> {
                        (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                new EPD_Block_Information__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                }
        };
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        epd.Equipment_Engine__r = (Equipment_Engine__c) HOG_SObjectFactory.createSObject(
                new Equipment_Engine__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        List<EPD_Engine_Performance_Data__c> epdRecords = new List<EPD_Engine_Performance_Data__c> {
                epd
        };
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_FormService().generateFormStructures(epdRecords, blockInformationByEPDId);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
    }

    @IsTest
    static void getCOCStagesWrapped_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<EPD_Compressor_Operating_Condition_Stage__c> cocStages;
        List<EPD_FormStructureCOCStage> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getCOCStagesWrapped(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getCOCStagesWrapped_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Compressor_Operating_Condition_Stage__c> cocStages = new List<EPD_Compressor_Operating_Condition_Stage__c>();
        List<EPD_FormStructureCOCStage> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getCOCStagesWrapped(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStages.size(), result.size());
    }

    @IsTest
    static void getCOCStagesWrapped_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Compressor_Operating_Condition_Stage__c> cocStages = new List<EPD_Compressor_Operating_Condition_Stage__c>{
                (EPD_Compressor_Operating_Condition_Stage__c) HOG_SObjectFactory.createSObject(
                        new EPD_Compressor_Operating_Condition_Stage__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        };
        List<EPD_FormStructureCOCStage> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getCOCStagesWrapped(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStages.size(), result.size());
    }

    @IsTest
    static void getPartsWrapped_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<EPD_Part_Replacement__c> parts;
        List<EPD_FormStructurePart> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getPartsWrapped(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getPartsWrapped_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Part_Replacement__c> parts = new List<EPD_Part_Replacement__c>();
        List<EPD_FormStructurePart> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getPartsWrapped(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(parts.size(), result.size());
    }

    @IsTest
    static void getPartsWrapped_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Part_Replacement__c> parts = new List<EPD_Part_Replacement__c>{
                (EPD_Part_Replacement__c) HOG_SObjectFactory.createSObject(
                        new EPD_Part_Replacement__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        };
        List<EPD_FormStructurePart> result;

        Test.startTest();
        try {
            result = new EPD_FormService().getPartsWrapped(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(parts.size(), result.size());
    }

}