public with sharing class SPCC_ServiceEntrySheetCtrlX {
    /**************
    ** Variables **
    **************/
    //Controller
    private ApexPages.StandardController stdController;

    //Service Entry Sheet Record
	public SPCC_Service_Entry_Sheet__c serviceEntrySheet {get; private set;}

    //Purchase Order Information
    public String purchaseOrderNumber {get; set;}
    public String purchaseOrderId {get; set;}
    public SPCC_Purchase_Order__c purchaseOrder {get; private set;}

    //User Security
    public SPCC_Utilities.UserPermissions userPermissions;

    //Url Params
    private UrlParams params;
	
    /*****************
    ** Constructors **
    *****************/
    public SPCC_ServiceEntrySheetCtrlX(ApexPages.StandardController stdController) {
        //Set Record
        this.stdController = stdController;
    	this.serviceEntrySheet = (SPCC_Service_Entry_Sheet__c) stdController.getRecord();

        //Set User Security
        userPermissions = new SPCC_Utilities.UserPermissions();

        //Get Url Params
        getUrlParams();
    }

    /********************
    ** Getters/Setters **
    ********************/
    public List<SelectOption> purchaseOrderLineItemOptions {
        get{
            if(purchaseOrderLineItemOptions == null || purchaseOrderLineItemOptions.isEmpty()) {
                if(!String.isBlank(purchaseOrderId)) {
                    purchaseOrderLineItemOptions = new List<SelectOption>();
                    for(SPCC_Line_Item__c lineItem : [Select Id, Name, Cost_Element__c, Purchase_Order__c, 
                                                                     Short_Text__c, Item__c
                                                              From SPCC_Line_Item__c
                                                              Where Purchase_Order__c =: purchaseOrderId]) {
                        purchaseOrderLineItemOptions.add(new SelectOption(lineItem.Id, lineItem.Item__c));
                    }
                }
            }
            return purchaseOrderLineItemOptions;
        }
        private set;
    }
    
    /************
    ** Actions **
    *************/
    public PageReference submit() {
        try {
            serviceEntrySheet.Purchase_Order__c = purchaseOrderId;
            upsert serviceEntrySheet;
        } catch (DmlException ex) {
            SPCC_Utilities.logErrorOnPage(ex);
            return null;
        }

        return (!String.isBlank(params.retUrl)) ? new PageReference(params.retUrl) : Page.SPCC_ServiceEntrySheetList;
    }

    public PageReference cancel() {
        return (!String.isBlank(params.retUrl)) ? new PageReference(params.retUrl) : stdController.cancel();
    }

    @RemoteAction
    public static List<SPCC_Purchase_Order__c> searchPurchaseOrder(String purchaseOrderNumber) {
        String queryString = 'Select Id, Name, Authorization_for_Expenditure__c, Cost_Code__c,' +
                             '       Description__c, Engineering_Work_Request__c,' +
                             '       Purchase_Order_Number__c, Status__c, Vendor_Account__c ' +
                             'From SPCC_Purchase_Order__c ' +
                             'Where Purchase_Order_Number__c Like \'%' + String.escapeSingleQuotes(purchaseOrderNumber) +'%\'';
        List<SPCC_Purchase_Order__c> purchaseOrderList = Database.query(queryString);
        System.debug(purchaseOrderList);
        return purchaseOrderList;
    }

    public void refreshPurchaseOrder() {
        if(!String.isEmpty(purchaseOrderId)) {
            List<SPCC_Purchase_Order__c> purchaseOrderList = [Select Id, Name, Engineering_Work_Request__c, Engineering_Work_Request__r.Name,
                                                                     Authorization_for_Expenditure__c, Authorization_for_Expenditure__r.Name,
                                                                     Cost_Code__r.Cost_Code_Number__c, Purchase_Order_Number__c, Status__c, 
                                                                     Vendor_Account__c
                                                              From SPCC_Purchase_Order__c
                                                              Where Id =: purchaseOrderId];
            purchaseOrder = (purchaseOrderList != null && !purchaseOrderList.isEmpty()) ? purchaseOrderList[0] : null;
            System.debug('refreshPurchaseOrder: ' + purchaseOrder);
        }
    }

    /**********************
    ** Utility Functions **
    **********************/
    private void getUrlParams() {
        params = new UrlParams();
        params.retUrl = ApexPages.currentPage().getParameters().get('retUrl');
    }

    /********************
    ** Utility Classes **
    ********************/
    private Class UrlParams {
        String retUrl;
    }

}