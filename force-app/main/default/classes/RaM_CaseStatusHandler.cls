/**********************************************************************************
  CreatedBy : Rajendar Jadigam
  Organization:Accenture
  Purpose   :  Handler class For custom webservice interface to SAPPI
  Test Class:  RaM_CaseStatusHandlerTest
  Version:1.0.0.1
**********************************************************************************/
global with sharing class RaM_CaseStatusHandler {
 @TestVisible
 global class CaseStatus {
  webservice String ticketNumber_R_M;
  webservice String caseStatus;
 }

 global class CaseStatusResponce {
  webservice String caseStatusResult;
 }
     /**************************************
    CreatedBy:      Accenture
    Method name:    updateCaseStatus
    Purpose:        RaM_CaseStatusHandler class main method for upsert case status value through Webservice
    Param:          List < CaseStatus >
    Return:         List < CaseStatusResponce >
    *************************************/
 webservice static List < CaseStatusResponce > updateCaseStatus(List < CaseStatus > css) {
  System.debug('css***' + css);

  List < CaseStatusResponce > csrs = new List < CaseStatusResponce > ();
  List < Case > oldCaseStatus;
  for (CaseStatus cs: css) {
   CaseStatusResponce csr = new CaseStatusResponce();
   try {    
    oldCaseStatus = new List < Case > ([Select Ticket_Number_R_M__c, Status From Case Where Ticket_Number_R_M__c =: cs.ticketNumber_R_M limit 1]);           
    System.debug('oldCaseStatus***' + oldCaseStatus);

    if (!oldCaseStatus.isEmpty() && oldCaseStatus[0].Ticket_Number_R_M__c == cs.ticketNumber_R_M) {
     oldCaseStatus[0].Status = cs.caseStatus;
     upsert oldCaseStatus;
     csr.caseStatusResult = cs.ticketNumber_R_M + ' - Update Success';
     csrs.add(csr);
    } else {
     String errorDetails = 'Failed to update the Case with Ticket number: ' + cs.ticketNumber_R_M + ' with Status ' + cs.caseStatus + ', Since there is no Case with: ' + cs.ticketNumber_R_M + ' in Salesforce.';
     String sbjct = 'Update Failed: ' + cs.ticketNumber_R_M;
     sendEmail(sbjct, errorDetails);

     csr.caseStatusResult = errorDetails;
     csrs.add(csr);
     System.debug('csr in else2***' + csr);
     System.debug('csrs in else2***' + csrs);
    }
   } catch (Exception e) {
    String errorDetails = 'Failed to update the Case with Ticket number: ' + cs.ticketNumber_R_M + ' with Status ' + cs.caseStatus + ' . ***Exception: ' + e.getMessage(); String sbjct = 'Update Failed: ' + cs.ticketNumber_R_M;
    sendEmail(sbjct, errorDetails); csr.caseStatusResult = errorDetails; csrs.add(csr);
    System.debug('csr in catch***' + csr);
    System.debug('csrs in catch***' + csrs);
   }
  }
  return csrs;
 }// -- Webservice Method ends --
 
 global static void sendEmail(String sub, String msg) { //  -- metod for email triggering --
  Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
  message.toAddresses = new String[] {
   'rajendar.jadigam@huskyenergy.com'
  };
  message.optOutPolicy = 'FILTER';
  message.subject = sub;
  message.plainTextBody = msg;
  Messaging.SingleEmailMessage[] messages = new List < Messaging.SingleEmailMessage > {
   message
  };
  Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
  if (results[0].success) {
   System.debug('The email was sent successfully.');
  } else {
   System.debug('The email failed to send: ' + results[0].errors[0].message);
  }
 }

}