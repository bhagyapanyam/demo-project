/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/25/2019   
 */

public interface ELG_Task_DAO {

	List<ELG_Task__c> getActiveTasks(String postId);
	List<ELG_Post__c> getAMUPosts(String amuId);
	ELG_Task__c getTask(String taskId);
	ELG_Shift_Assignement__c getActiveShift(String postId);
}