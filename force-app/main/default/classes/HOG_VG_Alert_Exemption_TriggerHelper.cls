/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Helper for Exemption Trigger providing processes for locking, unlocking parent alert
                and updating statuses.
Test Class:     HOG_VG_Alert_Exemption_TriggerHelperTest
History:        jschn 18. 9. 2018 - Created.
**************************************************************************************************/
public with sharing class HOG_VG_Alert_Exemption_TriggerHelper {

    private List<HOG_Vent_Gas_Alert_Exemption_Request__c> newExemptions;
    private Map<Id, HOG_Vent_Gas_Alert_Exemption_Request__c> oldExemptionsMap;

    public HOG_VG_Alert_Exemption_TriggerHelper(List<HOG_Vent_Gas_Alert_Exemption_Request__c> newExemptions,
            Map<Id, HOG_Vent_Gas_Alert_Exemption_Request__c> oldExemptionsMap) {
        this.newExemptions = newExemptions;
        this.oldExemptionsMap = oldExemptionsMap;
    }

    public void handleAfterUpdate() {
        updateAlertsExemptionOrNotStartedStatus();
    }

    public void handleAfterInsert() {
        updateAlertsExemptionStatusAndApprover();
    }

    private void updateAlertsExemptionOrNotStartedStatus() {
        List<HOG_Vent_Gas_Alert__c> alertsToUpdate = new List<HOG_Vent_Gas_Alert__c>();
        for(HOG_Vent_Gas_Alert_Exemption_Request__c exemption : newExemptions) {
            HOG_Vent_Gas_Alert__c alert = new HOG_Vent_Gas_Alert__c(Id = exemption.Vent_Gas_Alert__c);
            if(exemption.Status__c == 'Approved') {
                alert.End_Date__c = Datetime.now();
            } else if (exemption.Status__c == 'Rejected'){
                alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED;
                alert.Exemption__c = false;
                alert.Exemption_Approver__c = null;
            }

            alertsToUpdate.add(alert);
        }
        update alertsToUpdate;
    }


    private void updateAlertsExemptionStatusAndApprover() {
        List<HOG_Vent_Gas_Alert__c> alertsToUpdate = new List<HOG_Vent_Gas_Alert__c>();
        for(HOG_Vent_Gas_Alert_Exemption_Request__c exemption : newExemptions) {
            HOG_Vent_Gas_Alert__c alert = new HOG_Vent_Gas_Alert__c(Id = exemption.Vent_Gas_Alert__c);
            alert.Exemption__c = true;
            alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_EXEMPTION;
            alert.Exemption_Approver__c = exemption.Operations_Engineer__c;
            alertsToUpdate.add(alert);
        }
        update alertsToUpdate;
    }

}