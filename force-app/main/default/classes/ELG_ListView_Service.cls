/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/4/2019   
 */

public class ELG_ListView_Service {


	private static Boolean checkShiftEngineerReview = false;
	private static Boolean checkSteamChiefReview = false;
	private static Boolean checkShiftLeadReview = false;
	private static Boolean checkSupervisorReview = false;

	private static String fieldsToQuery = 'Shift_Cycle__c, ' +
			'AMU__c, ' +
			'Post__c, ' +
			'Status__c, ' +
			'Name, ' +
			'Id, ' +
			'Outgoing_Operator__c, ' +
			'Shift_Assignement__r.Shift_Engineer__r.Name, ' +
			'Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c, ' +
			'Handover_Name__c, ' +
			'CreatedDate, ' +
			'Shift_Engineer__c, ' +
			'Steam_Chief__c, ' +
			'Shift_Lead__c, ' +
			'Sr_Supervisor__c';

	public static DataPayload getAllThermalAMUs(String userId) {
		List<Field__c> thermalAMUsList = [
				SELECT Id, Name
				FROM Field__c
				WHERE Is_Thermal__c = TRUE
				ORDER BY Name
		];

		List<TemporaryClassToBuildObject> thermalAmus = new List<TemporaryClassToBuildObject>();

		for (Field__c amu : thermalAMUsList) {
			thermalAmus.add(new TemporaryClassToBuildObject(amu.Id, amu.Name));
		}

		List<ELG_User_Setting__c> settings = new List<ELG_User_Setting__c>();
		settings = [SELECT Operating_Field_AMU__c FROM ELG_User_Setting__c WHERE User__c = :userId];

		DataPayload thermals = new DataPayload();
		thermals.amus = thermalAmus;
		if (!settings.isEmpty()) {
			thermals.defaultAMU = settings[0].Operating_Field_AMU__c;
		}

		return thermals;
	}

	public static List<TemporaryClassToBuildObject> getAvailablePosts(String currentAMU) {
		List<ELG_Post__c> availablePosts = [
				SELECT Id, Post_Name__c
				FROM ELG_Post__c
				WHERE Operating_Field_AMU__c = :currentAMU
		];

		List<TemporaryClassToBuildObject> postsList = new List<TemporaryClassToBuildObject>();

		for (ELG_Post__c post : availablePosts) {
			postsList.add(new TemporaryClassToBuildObject(post.Id, post.Post_Name__c));
		}

		return postsList;

	}

	//Whole search logic
	public static List<DataTableWrapper> searchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection,
			Boolean notReviewedBySE,
			Boolean notReviewedBySteamChief,
			Boolean notReviewedByShiftLead,
			Boolean notReviewedBySupervisor) {
		List<ELG_Shift_Handover__c> queriedHandovers = new List<ELG_Shift_Handover__c>();
		Integer numberOfHandovers = 0;

		checkShiftEngineerReview = notReviewedBySE;
		checkSteamChiefReview = notReviewedBySteamChief;
		checkShiftLeadReview = notReviewedByShiftLead;
		checkSupervisorReview = notReviewedBySupervisor;

		FilterWrapper filterCriteria = (FilterWrapper) JSON.deserialize(JSON.serialize(filterSelection), FilterWrapper.class);
		String startDate = filterCriteria.startDate + 'T00:00:00z';
		String endDate = filterCriteria.endDate + 'T23:59:59z';

		queriedHandovers = selectedFilter(fieldsToQuery,
				'ELG_Shift_Handover__c',
				stringBuilder(JSON.serialize(filterCriteria), startDate, endDate, offsetSize, true, fieldSort, sortDirection));

		if (!queriedHandovers.isEmpty()) {
			numberOfHandovers = handoverCounter(stringBuilder(JSON.serialize(filterCriteria), startDate, endDate, 0, false, '', ''));
		}

		return objectBuilder(queriedHandovers, numberOfHandovers);
	}

	@TestVisible
	private static List<DataTableWrapper> objectBuilder(List<ELG_Shift_Handover__c> queriedHandovers, Integer numberOfHandovers) {

		List<DataTableWrapper> response = new List<DataTableWrapper>();
		DataTableWrapper formattedHandover = new DataTableWrapper();
		if (queriedHandovers.isEmpty()) {
			formattedHandover.hasError = true;
			formattedHandover.errorMsg = ELG_Constants.NO_HANDOVER_FOUND;
			response.add(formattedHandover);
		} else if (numberOfHandovers > 2050) {
			response.clear();
			formattedHandover.hasError = true;
			formattedHandover.errorMsg = ELG_Constants.maximumQueryLimit('Handovers', numberOfHandovers);
			response.add(formattedHandover);
		} else {
			for (ELG_Shift_Handover__c handover : queriedHandovers) {
				DataTableWrapper preparedHandover = new DataTableWrapper();
				preparedHandover.handId = handover.Id;
				preparedHandover.handoverName = handover.Handover_Name__c;
				preparedHandover.nameUrl = '/' + handover.Id;
				preparedHandover.shiftEngineer = handover.Shift_Assignement__r.Shift_Engineer__r.Name;
				preparedHandover.outgoingOperator = handover.Outgoing_Operator__c;
				preparedHandover.amuName = handover.AMU__c;
				preparedHandover.postName = handover.Post__c;
				preparedHandover.handoverStatus = handover.Status__c;
				preparedHandover.shiftCycle = handover.Shift_Cycle__c;
				preparedHandover.hasError = false;
				if (checkShiftEngineerReview || checkSteamChiefReview || checkShiftLeadReview || checkSupervisorReview) {
					preparedHandover.awaitingReviewText = queryForReviewers(handover);
				}
				response.add(preparedHandover);
			}
		}


		response[0].numberOfRecords = numberOfHandovers;

		return response;
	}

	private static String queryForReviewers(ELG_Shift_Handover__c handoverList) {
		String fieldPopulateBy = '';

		if (checkShiftEngineerReview
				&& handoverList.Shift_Engineer__c == null
				&& handoverList.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c
				&& handoverList.Status__c != ELG_Constants.HANDOVER_NEW) {
			fieldPopulateBy = buildMultipleReviewers(fieldPopulateBy);
			fieldPopulateBy += ELG_Constants.SHIFT_ENGINEER_REVIEW;
		}

		if (checkSteamChiefReview
				&& handoverList.Steam_Chief__c == null
				&& handoverList.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c
				&& handoverList.Status__c != ELG_Constants.HANDOVER_NEW) {
			fieldPopulateBy = buildMultipleReviewers(fieldPopulateBy);
			fieldPopulateBy += ELG_Constants.STEAM_CHIEF_REVIEW;
		}

		if (checkShiftLeadReview
				&& handoverList.Shift_Lead__c == null
				&& handoverList.Status__c != ELG_Constants.HANDOVER_NEW) {
			fieldPopulateBy = buildMultipleReviewers(fieldPopulateBy);
			fieldPopulateBy += ELG_Constants.SHIFT_LEAD_REVIEW;
		}

		if (checkSupervisorReview
				&& handoverList.Sr_Supervisor__c == null
				&& handoverList.Status__c != ELG_Constants.HANDOVER_NEW) {
			fieldPopulateBy = buildMultipleReviewers(fieldPopulateBy);
			fieldPopulateBy += ELG_Constants.SUPERVISOR_REVIEW;
		}

		return fieldPopulateBy;
	}

	private static String buildMultipleReviewers(String queryCondition) {
		if (String.isNotBlank(queryCondition)) {
			queryCondition += ', ';
		}
		return queryCondition;
	}

	private static List<SObject> selectedFilter(String fields, String objectName, String filterSelection) {
		String preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				fields,
				objectName,
				filterSelection
		});

		return Database.query(preparedQuery);
	}


	private static Integer handoverCounter(String filteredQuery) {

		String numberOfRecordsBasedOnFilterCriteria = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				'COUNT()',
				'ELG_Shift_Handover__c',
				filteredQuery
		});

		return Database.countQuery(numberOfRecordsBasedOnFilterCriteria);
	}

	private static String stringBuilder(
			String filterSelection,
			String startDate,
			String endDate,
			Integer offsetSize,
			Boolean limitations,
			String fieldSort,
			String sortDirection) {

		List<String> whereBuilder = new List<String>();
		List<String> temporaryList = new List<String>();
		String tempoString;

		tempoString = filterSelection.replace('amuId', 'Shift_Assignement__r.Post__r.Operating_Field_AMU__c');
		tempoString = tempoString.replace('shiftCycle', 'Shift_Cycle__c');
		tempoString = tempoString.replace('handoverStatus', 'Status__c');
		tempoString = tempoString.replace('reviewerHandoverState', 'Status__c');
		tempoString = tempoString.replace('postId', 'Shift_Assignement__r.Post__c');
		tempoString = tempoString.replace('primaryId', 'Shift_Assignement__r.Primary__c');
		tempoString = tempoString.replace('shiftEngineerId', 'Shift_Assignement__r.Shift_Engineer__c');
		temporaryList = tempoString.remove('{').remove('}').remove('"').split(',');

		for (String str : temporaryList) {
			if (!str.contains('null')) {
				whereBuilder.add(str);
			}
		}

		String whereClause = '';

		for (String filterItem : whereBuilder) {
			List<String> filterPair = filterItem.split(':');
			if (String.isNotBlank(whereClause)) {
				whereClause += ' AND';
			}
			if (filterPair.contains('startDate')) {
				whereClause += ' CreatedDate >= ' + startDate;
			} else if (filterPair.contains('endDate')) {
				whereClause += ' CreatedDate <= ' + endDate;
			} else {
				whereClause += ' ' + filterPair.get(0) + ' = \'' + filterPair.get(1) + '\'';
			}
		}

		if (checkShiftEngineerReview || checkSteamChiefReview || checkShiftLeadReview || checkSupervisorReview) {
			whereClause += getReviewerQueryCondition(checkShiftEngineerReview, checkSteamChiefReview, checkShiftLeadReview, checkSupervisorReview);
			whereClause += ' AND Status__c != \'' + ELG_Constants.HANDOVER_NEW + '\'';
		}

		if (limitations) {
			whereClause += ' ORDER BY ' + fieldSort + ' ' + sortDirection + ' LIMIT 50';
			whereClause += ' OFFSET ' + offsetSize;
		}

		System.debug('BUILD QUERY ' + whereClause);

		return whereClause;
	}

	private static String getReviewerQueryCondition(Boolean checkShiftEngineerReview, Boolean checkSteamChiefReview, Boolean checkShiftLeadReview, Boolean checkSupervisorReview) {
		String queryCondition = '';

		if (checkShiftEngineerReview) {
			queryCondition = buildMultiplyCondition(queryCondition);
			queryCondition += '(Shift_Engineer__c = null AND Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c = true)';
		}

		if (checkSteamChiefReview) {
			queryCondition = buildMultiplyCondition(queryCondition);
			queryCondition += '(Steam_Chief__c = null  AND Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c = true)';
		}

		if (checkShiftLeadReview) {
			queryCondition = buildMultiplyCondition(queryCondition);
			queryCondition += '(Shift_Lead__c = null)';
		}

		if (checkSupervisorReview) {
			queryCondition = buildMultiplyCondition(queryCondition);
			queryCondition += '(Sr_Supervisor__c = null)';
		}

		if (String.isNotBlank(queryCondition)) {
			queryCondition = ' AND (' + queryCondition + ')';
		}

		return queryCondition;
	}

	private static String buildMultiplyCondition(String queryCondition) {
		if (String.isNotBlank(queryCondition)) {
			queryCondition += ' OR ';
		}
		return queryCondition;
	}


	public static String massHandoverReviewersUpdate(
			List<String> listOfIds,
			String currentUser,
			Boolean supervisorReviews,
			Boolean steamChiefReviews,
			Boolean shiftEngineerReviews,
			Boolean shiftLeadReviews) {

		String message = '';

		if (ELG_Utilities.hasEditAccessForReviewShiftEngineer()) {
			Datetime dateToday = Datetime.now();
			Boolean isFormChanged = false;

			checkSupervisorReview = supervisorReviews;
			checkSteamChiefReview = steamChiefReviews;
			checkShiftEngineerReview = shiftEngineerReviews;
			checkShiftLeadReview = shiftLeadReviews;

			List<ELG_Shift_Handover__c> selectedHandovers = [
					SELECT Id,
							Status__c,
							Sr_Supervisor__c,
							Reviewed_by_Sr_Supervisor_Timestamp__c,
							Steam_Chief__c,
							Reviewed_by_Steam_Chief_Timestamp__c,
							Shift_Lead__c,
							Reviewed_by_Shift_Lead_Timestamp__c,
							Shift_Engineer__c,
							Reviewed_by_Shift_Engineer_Timestamp__c,
							Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
							Shift_Assignement__r.Shift_Engineer__c
					FROM ELG_Shift_Handover__c
					WHERE Id IN :listOfIds
			];

			List<ELG_Shift_Handover__c> handoversToUpdate = new List<ELG_Shift_Handover__c>();


			for (ELG_Shift_Handover__c handover : selectedHandovers) {

				if (handover.Status__c == ELG_Constants.HANDOVER_ACCEPTED) {

					if (checkSupervisorReview && handover.Sr_Supervisor__c == null) {
						handover.Sr_Supervisor__c = currentUser;
						handover.Reviewed_by_Sr_Supervisor_Timestamp__c = dateToday;
						isFormChanged = true;
					}

					if (checkSteamChiefReview && handover.Steam_Chief__c == null) {
						handover.Steam_Chief__c = currentUser;
						handover.Reviewed_by_Steam_Chief_Timestamp__c = dateToday;
						isFormChanged = true;
					}

					if (checkShiftLeadReview && handover.Shift_Lead__c == null) {
						handover.Shift_Lead__c = currentUser;
						handover.Reviewed_by_Shift_Lead_Timestamp__c = dateToday;
						isFormChanged = true;
					}

					if (checkShiftEngineerReview && handover.Shift_Engineer__c == null && handover.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c) {
						handover.Shift_Engineer__c = currentUser;
						handover.Reviewed_by_Shift_Engineer_Timestamp__c = dateToday;
						isFormChanged = true;
					}

					if (isFormChanged) {
						handoversToUpdate.add(handover);
					}

				} else if (handover.Status__c == ELG_Constants.HANDOVER_IN_PROGRESS
						&& handover.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c
						&& handover.Shift_Assignement__r.Shift_Engineer__c == currentUser
						&& handover.Shift_Engineer__c == null) {

					handover.Shift_Engineer__c = currentUser;
					handover.Reviewed_by_Shift_Engineer_Timestamp__c = dateToday;

					handoversToUpdate.add(handover);
				}
			}

			update handoversToUpdate;

			if (handoversToUpdate.size() > 0) {
				message = handoversToUpdate.size() + ' Handovers has been successfully updated.';
			} else {
				message = 'There are no Handovers to updated based on the selections.';
			}
		} else {
			message = System.Label.ELG_Wrong_Permission_Set;
		}

		return message;

	}


	//From LWC process parameters
	public class FilterWrapper {
		@AuraEnabled
		public String amuId;
		@AuraEnabled
		public String postId;
		@AuraEnabled
		public String shiftCycle;
		@AuraEnabled
		public String handoverStatus;
		@AuraEnabled
		public String reviewerHandoverState;
		@AuraEnabled
		public String primaryId;
		@AuraEnabled
		public String shiftEngineerId;
		@AuraEnabled
		public String startDate;
		@AuraEnabled
		public String endDate;
	}


	//Class to prepare Object for LWC
	public class TemporaryClassToBuildObject {
		@AuraEnabled
		public String value;
		@AuraEnabled
		public String label;

		public TemporaryClassToBuildObject(String amuId, String amuName) {
			this.value = amuId;
			this.label = amuName;
		}
	}

	//Payload that bring data to LWC
	public class DataPayload {
		@AuraEnabled
		public List<TemporaryClassToBuildObject> amus;
		@AuraEnabled
		public String defaultAMU;

	}

	//Class that bring formatted data for LWC Data table
	public class DataTableWrapper {
		@AuraEnabled
		public Id handId;
		@AuraEnabled
		public String name;
		@AuraEnabled
		public String nameUrl;
		@AuraEnabled
		public String shiftEngineer;
		@AuraEnabled
		public String outgoingOperator;
		@AuraEnabled
		public String amuName;
		@AuraEnabled
		public String postName;
		@AuraEnabled
		public String handoverStatus;
		@AuraEnabled
		public String shiftCycle;
		@AuraEnabled
		public String handoverName;
		@AuraEnabled
		public Integer numberOfRecords;
		@AuraEnabled
		public String errorMsg;
		@AuraEnabled
		public String awaitingReviewText;
		@AuraEnabled
		public Boolean hasError = false;

		@AuraEnabled
		public String label;
		@AuraEnabled
		public String fieldName;
		@AuraEnabled
		public String type;

	}


}