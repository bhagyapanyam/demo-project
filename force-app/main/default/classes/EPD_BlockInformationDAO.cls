/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Block Information SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface EPD_BlockInformationDAO {

    List<EPD_Block_Information__c> getBlockInformationByEPDs(Set<Id> epdIds);

}