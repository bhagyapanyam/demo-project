/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Email notification class for purpose of on EPD Form submit notification with PDF.
Test Class:     EPD_EmailNotificationOnSubmitPDFTest
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EmailNotificationOnSubmitPDF extends EPD_EmailServiceBase {

    private static final String CLASS_NAME = String.valueOf(EPD_EmailNotificationOnSubmitPDF.class);

    /**
     * Builds recipient map for current payload.
     * It's mapped by ID, that we don't need to create (query) it for every iteration.
     */
    protected override void buildRecipientsMap() {
        recipientsMap = new Map<Id, List<Id>>();
        for(EPD_Engine_Performance_Data__c form : forms) {
            if(!recipientsMap.containsKey(form.Id)) {
                recipientsMap.put(form.Id, new List<Id> { form.Data_Collected_By__c });
            }
        }
    }

    /**
     * set Email template ID for rendering.
     */
    protected override void setEmailTemplate() {
        templateId = new HOG_EmailTemplateSelector().getTemplateByDevName(EPD_Constants.TEMPLATE_NAME_ON_SUBMIT_PDF).Id;
    }


    /**
     * Attachment is required for this type of email. So it is build for provided form.
     * It's a PDF version of EPD Form.
     *
     * @param form
     *
     * @return List<Messaging.EmailFileAttachment>
     */
    protected override List<Messaging.EmailFileAttachment> setAttachments(EPD_Engine_Performance_Data__c form) {
        if(!attachmentsByEPDId.containsKey(form.Id)) {
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();

            Messaging.EmailFileAttachment attachment = getEmailAttachment(form);
            if(attachment.getBody() != null) { attachments.add(attachment); }

            attachmentsByEPDId.put(form.Id, attachments);
        }
        return attachmentsByEPDId.get(form.Id);
    }

    /**
     * Set sender for email.
     *
     * @param emailMessage
     */
    protected override void setSender(Messaging.SingleEmailMessage emailMessage) {
        if(!Test.isRunningTest()) { emailMessage.setOrgWideEmailAddressId(EPD_Constants.SENDER_ID); }
    }

    /**
     * Build email attachment for the email.
     * Sets file name and content.
     *
     * @param epd
     *
     * @return Messaging.EmailFileAttachment
     */
    private Messaging.EmailFileAttachment getEmailAttachment(EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> getEmailAttachment. START.');

        Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
        fileAttachment.setFileName(
                getFileName(epd.LSD__c)
        );

        System.debug(CLASS_NAME + ' -> getEmailAttachment. PDF Name: ' + fileAttachment.getFileName());

        Blob body = getPDFContent(epd.Id);
        fileAttachment.setBody(body);

        System.debug(CLASS_NAME + ' -> getEmailAttachment. END.');
        return fileAttachment;
    }

    /**
     * Builds PDF Content from EPD record.
     * Uses Visualforce page to do that.
     *
     * @param epdId
     *
     * @return Blob
     */
    private Blob getPDFContent(Id epdId) {
        System.debug(CLASS_NAME + ' -> getPDFContent. START.');

        PageReference pdf = Page.EPD_PDFPage;
        pdf.getParameters().put('id', epdId);
        pdf.setRedirect(true);

        System.debug(CLASS_NAME + ' -> getPDFContent. BEFORE GET CONTENT.');
        Blob content;
        if(!Test.isRunningTest()) { content = pdf.getContent(); }

        System.debug(CLASS_NAME + ' -> getPDFContent. END.');
        return content;
    }

    /**
     * Builds file name for attachment based on constants.
     *
     * @param LSD
     *
     * @return String
     */
    private String getFileName(String LSD) {
        return String.format(
                EPD_Constants.FILE_NAME_BASE,
                new List<String> {
                        LSD,
                        Date.today().format(),
                        EPD_Constants.FILE_EXTENSION
                }
        );
    }

    /**
     * No custom content is required for this Email type
     *
     * @param emailMessage
     * @param form
     */
    protected override void setCustomContent(Messaging.SingleEmailMessage emailMessage, EPD_Engine_Performance_Data__c form) {
        Messaging.SingleEmailMessage templateMessage = Messaging.renderStoredEmailTemplate(templateId, null, form.Id);
        emailMessage.setSubject(templateMessage.getSubject());
        emailMessage.setHtmlBody(templateMessage.getHtmlBody());
    }

    /**
     * Checks if record is valid for email.
     *
     * @param form
     *
     * @return Boolean
     */
    protected override Boolean validRecordForEmail(EPD_Engine_Performance_Data__c form) {
        return true;
    }

    //not required for this type of email
    protected override void prepareImplementationSpecificVariables() {}
    protected override void setCCAddresses(Messaging.SingleEmailMessage emailMessage) {}

}