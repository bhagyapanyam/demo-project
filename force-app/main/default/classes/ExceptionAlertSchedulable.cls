global with sharing class ExceptionAlertSchedulable implements Schedulable {

    global void execute(SchedulableContext sc) {
        String query = 'SELECT Id, Name, Status__c, Apex_Class_Trigger__c, Description__c, Log_Date__c FROM Error_Log__c WHERE (Status__c = null OR Status__c = \'Created\') AND Severity__c = \'ERROR\' AND Apex_Class_Trigger__c != null ORDER  BY CreatedDate DESC';
        Database.executeBatch(new ExceptionAlertBatchable(query), 100);
    } 

}