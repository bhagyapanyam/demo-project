/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_EngineRule interface for VTT_LTNG_Engine..
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public interface VTT_LTNG_EngineRule {

    /**
     * Check if rule passed
     * @return
     */
    Boolean passedRule();

    /**
     * Process logic for rule
     */
    void processRule();

    /**
     * List of messages that came out when rule was processed
     * @return
     */
    Set<VTT_LTNG_EngineMessage> getMessages();

    /**
     * List of available action after rule was processed
     * @return
     */
    Set<VTT_LTNG_EngineAction> getAvailableActions();
}