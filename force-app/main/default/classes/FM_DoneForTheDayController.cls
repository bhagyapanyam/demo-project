public class FM_DoneForTheDayController
{
	// ******************************************************************************************************** //
	// ** THIS CONTROLLER IS USED TO ALLOW OPERATORS TO MARK THEIR WORK AS DONE FOR THE DAY FOR THEIR ROUTES ** //
	// ******************************************************************************************************** //

	///////////////////
	// * CONSTANTS * //
	///////////////////

	private static final String PERMISSION_SET_OPERATOR = 'HOG_Field_Operator';
	private static final String PERMISSION_SET_DISPATCH_STANDARD = 'HOG_FM_Dispatcher';
	private static final String PERMISSION_SET_DISPATCH_VENDOR = 'HOG_FM_Dispatcher_Vendor';
	private static final String PERMISSION_SET_FLUID_ADMIN = 'HOG_Fluid_Management_Admin';
    
    //HOG_Read_Only should have access to Route Status according to UserStory W-000265
    private static final String PERMISSION_SET_READ_ONLY = 'HOG_Read_Only';
    
	private static final String DATE_FORMAT = 'MMMMM dd yyyy h:mm a';

	////////////////////
	// * ROUTE DATA * //
	////////////////////

	private List<DoneForTheDayRoute> lFullRouteStatus;
	private List<DoneForTheDayRoute> lUserRouteStatus;
	private List<SelectOption> lDeskNames;

	private Map<String, DateTime> mapDoneData;
	private Map<String, Boolean> mapDoneOperator;
	private Map<String, Boolean> mapDeskFilter;

	public String[] sDeskSelection {public get; public set;}
    
    ////////////////////////////
	// * USER-SPECIFIC DATA * //
	////////////////////////////

	private FM_Done_For_The_Day__c oDoneForTheDay;
	private String sCurrentUserId;
	private String sCurrentUserName;

	//////////////////////////////////
	// * PERMISSION & ACCESS DATA * //
	//////////////////////////////////

	private Boolean bMarkCompletePermission;
	private Boolean bAlreadySubmittedForTheDay;
	private Boolean bViewPermission;
	private Boolean bIsAdmin;

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_DoneForTheDayController()
	{
		InitializeControllerData();
		CheckUserPermissions();
		CreateRouteInfo();
	}

	///////////////////////////////////////
	// * SET GLOBAL CONFIGURATION DATA * //
	///////////////////////////////////////

	private void InitializeControllerData()
	{
		this.lDeskNames = new List<SelectOption>();
		this.sDeskSelection = new String[]{}; 
		this.oDoneForTheDay = new FM_Done_For_The_Day__c();
		this.oDoneForTheDay.Done_Date__c = System.today();
		this.sCurrentUserId = UserInfo.getUserId();
		this.sCurrentUserName = UserInfo.getName();

		this.bMarkCompletePermission = false;
		this.bAlreadySubmittedForTheDay = false;
		this.bViewPermission = false;
		this.bIsAdmin = false;

		GenerateDeskNames();
	}

	//////////////////////////////////////
	// * GENERATE DISPATCH DESK NAMES * //
	//////////////////////////////////////

	private void GenerateDeskNames()
	{
		List<Dispatch_Desk__c> lDeskRecords = [SELECT Id, Name FROM Dispatch_Desk__c WHERE RecordType.DeveloperName = 'Fluid_Dispatch' ORDER BY Name];
		SelectOption oOneOption;

		this.sDeskSelection.add('none');

		// ** MAKE NON-VENDOR DESKS VISIBLE TO REGULAR DISPATCHERS, FIELD OPERATORS, FLUID ADMIN, HOG ADMIN, AND SYSTEM ADMIN ONLY ** //
//		if(VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_DISPATCH_STANDARD) || VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_OPERATOR) || VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_FLUID_ADMIN) || VerifyProfile(this.sCurrentUserId, 'Standard HOG - Administrator') || VerifyProfile(this.sCurrentUserId, 'System Administrator'))
//		{
			for(Dispatch_Desk__c oOneDesk : lDeskRecords)
			{
				oOneOption = new SelectOption(oOneDesk.Id, oOneDesk.Name);
	
				this.sDeskSelection.add(oOneDesk.Id);
				this.lDeskNames.add(oOneOption);
			}
//		}
	}

	////////////////////////////////////
	// * CHECK USER PERMISSION DATA * //
	////////////////////////////////////

	private void CheckUserPermissions()
	{
		this.bViewPermission = VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_READ_ONLY) ||VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_DISPATCH_STANDARD) || VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_DISPATCH_VENDOR) || VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_FLUID_ADMIN) || VerifyProfile(this.sCurrentUserId, 'Standard HOG - Administrator') || VerifyProfile(this.sCurrentUserId, 'System Administrator');
		this.bAlreadySubmittedForTheDay = CheckCurrentSubmittedRoutes();
		this.bMarkCompletePermission = VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_OPERATOR);
		this.bIsAdmin = (VerifyProfile(this.sCurrentUserId, 'Standard HOG - Administrator') || VerifyProfile(this.sCurrentUserId, 'System Administrator'));
	}

	//////////////////////////////////
	// * CREATE ROUTE INFORMATION * //
	//////////////////////////////////

	public PageReference CreateRouteInfo()
	{
		this.lFullRouteStatus = new List<DoneForTheDayRoute>();
		this.lUserRouteStatus = new List<DoneForTheDayRoute>();
		this.mapDoneData = new Map<String, DateTime>();
		this.mapDoneOperator = new Map<String, Boolean>();
		this.mapDeskFilter = new Map<String, Boolean>();

		BuildDoneForTheDayMaps();
		FilterDispatchDesks();

		if(this.bViewPermission == true)
		{
			this.lFullRouteStatus = BuildRouteInfo(null);
		}

		if(this.bMarkCompletePermission == true)
		{
			this.lUserRouteStatus = BuildRouteInfo(this.sCurrentUserId);
		}

		return null;
	}

	////////////////////////////////////////////////
	// * CREATE FILTER (MAP) FOR DISPATCH DESKS * //
	////////////////////////////////////////////////

	private void FilterDispatchDesks()
	{
		for(String sOneDeskId : this.sDeskSelection)
		{
			mapDeskFilter.put(sOneDeskId, true);
		}
	}

	//////////////////////////////////////////////
	// * BUILD DONE FOR THE DAY TIME MAPPINGS * //
	//////////////////////////////////////////////

	private void BuildDoneForTheDayMaps()
	{
		List<FM_Done_For_The_Day__c> lDoneData = [SELECT Operator__c, Operator_Route__c, Operator__r.Name, Operator_Route__r.Name, CreatedDate FROM FM_Done_For_The_Day__c WHERE Done_Date__c = :this.oDoneForTheDay.Done_Date__c];
		String sOneKey;

		for(FM_Done_For_The_Day__c oOneData : lDoneData)
		{
			sOneKey = oOneData.Operator__c + '-' + oOneData.Operator_Route__c;
			this.mapDoneData.put(oOneData.Operator__c, oOneData.CreatedDate);

			if(this.mapDoneOperator.get(oOneData.Operator__c) != true)
			{
				this.mapDoneOperator.put(oOneData.Operator__c, true);
			}
		}
	}

	///////////////////////////////////////
	// * BUILD USER ROUTE INFO (STATS) * //
	///////////////////////////////////////

	private List<DoneForTheDayRoute> BuildRouteInfo(String sUserId)
	{
		List<AggregateResult> lRoutes = (sUserId == null || sUserId == '') ? [SELECT Well__r.Route__r.Id RouteId, Well__r.Route__r.Name RouteName, Well__r.Route__r.Dispatch_Desk__r.Id DeskId, Well__r.Route__r.Dispatch_Desk__r.Name DeskName, CreatedBy.Id OperatorId, CreatedBy.Name OperatorName, COUNT(Id) RouteCount FROM FM_Run_Sheet__c WHERE Date__c = :this.oDoneForTheDay.Done_Date__c GROUP BY Well__r.Route__r.Id, Well__r.Route__r.Name, Well__r.Route__r.Dispatch_Desk__r.Id, Well__r.Route__r.Dispatch_Desk__r.Name, CreatedBy.Id, CreatedBy.Name ORDER BY Well__r.Route__r.Name NULLS LAST, CreatedBy.Name] : [SELECT Well__r.Route__r.Id RouteId, Well__r.Route__r.Name RouteName, Well__r.Route__r.Dispatch_Desk__r.Id DeskId, Well__r.Route__r.Dispatch_Desk__r.Name DeskName, CreatedBy.Id OperatorId, CreatedBy.Name OperatorName, COUNT(Id) RouteCount FROM FM_Run_Sheet__c WHERE Date__c = :this.oDoneForTheDay.Done_Date__c AND CreatedBy.Id = :sUserId GROUP BY Well__r.Route__r.Id, Well__r.Route__r.Name, Well__r.Route__r.Dispatch_Desk__r.Id, Well__r.Route__r.Dispatch_Desk__r.Name, CreatedBy.Id, CreatedBy.Name ORDER BY Well__r.Route__r.Name NULLS LAST, CreatedBy.Name];
		List<FM_Run_Sheet__c> lRunSheetLatest = [SELECT Id, CreatedDate, CreatedBy.Id, Route__c FROM FM_Run_Sheet__c WHERE Date__c = :this.oDoneForTheDay.Done_Date__c ORDER BY CreatedDate];

		Map<String, DoneForTheDayRoute> mapRoutes = new Map<String, DoneForTheDayRoute>();
		Map<String, Integer> mapRouteEntryOrder = new Map<String, Integer>();
		Map<String, Id> mapRunSheetCountId = new Map<String, Id>();
		Map<String, DateTime> mapRunSheetCountTime = new Map<String, DateTime>();

		DoneForTheDayRoute oRouteParent;
		DoneForTheDayRouteEntry oRouteEntry;
		DateTime dTimeStamp;

		String sOneRouteId;
		String sOneRouteName;
		String sOneDeskId;
		String sOneDeskName;
		String sCreatedById;
		String sCreatedByName;
		String sRunSheetKey;
		Integer iRunSheetCount;
		Boolean bWasDone;

		for(FM_Run_Sheet__c oOneRunSheet : lRunSheetLatest)
		{
			sRunSheetKey = oOneRunSheet.Route__c + '-' + oOneRunSheet.CreatedBy.Id;

			mapRunSheetCountId.put(sRunSheetKey, oOneRunSheet.Id);
			mapRunSheetCountTime.put(sRunSheetKey, oOneRunSheet.CreatedDate);
		}

		for(AggregateResult oOneRoute : lRoutes)
		{
			sOneRouteId = String.valueOf(oOneRoute.get('RouteId'));
			sOneRouteName = String.valueOf(oOneRoute.get('RouteName'));
			sOneDeskId = (String.valueOf(oOneRoute.get('DeskId')) == null) ? 'none' : String.valueOf(oOneRoute.get('DeskId'));
			sOneDeskName = String.valueOf(oOneRoute.get('DeskName'));
			sCreatedById = String.valueOf(oOneRoute.get('OperatorId'));
			sCreatedByName = String.valueOf(oOneRoute.get('OperatorName'));
			iRunSheetCount = Integer.valueOf(oOneRoute.get('RouteCount'));

			sRunSheetKey = sOneRouteName + '-' + sCreatedById;

			if(mapDeskFilter.get(sOneDeskId) == true)
			{
				if(mapRoutes.get(sOneRouteName) == null)
				{
					oRouteParent = new DoneForTheDayRoute(sOneRouteId, sOneRouteName, sOneDeskName);
					mapRoutes.put(sOneRouteName, oRouteParent);
					mapRouteEntryOrder.put(sOneRouteName, 1);
				}

				dTimeStamp = this.mapDoneData.get(sCreatedById);
				oRouteEntry = new DoneForTheDayRouteEntry(sOneRouteName, this.oDoneForTheDay.Done_Date__c, dTimeStamp, sCreatedById, sCreatedByName, iRunSheetCount, mapRouteEntryOrder.get(sOneRouteName), this.mapDoneOperator.get(sCreatedById), mapRunSheetCountId.get(sRunSheetKey), mapRunSheetCountTime.get(sRunSheetKey));

				mapRoutes.get(sOneRouteName).AddOperatorEntry(oRouteEntry);
				mapRouteEntryOrder.put(sOneRouteName, mapRouteEntryOrder.get(sOneRouteName) + 1);
			}
		}

		List<DoneForTheDayRoute> lFinalResult = new List<DoneForTheDayRoute>();

		for(String sOneKey : mapRoutes.keySet())
		{
			lFinalResult.add(mapRoutes.get(sOneKey));
		}

		return lFinalResult;
	}

	//////////////////////////////////////////////////////////
	// * CHECK IF USER ALREADY SUBMITTED DONE FOR THE DAY * //
	//////////////////////////////////////////////////////////

	private Boolean CheckCurrentSubmittedRoutes()
	{
		List<FM_Done_For_The_Day__c> lFoundDone = [SELECT Id FROM FM_Done_For_The_Day__c WHERE CreatedDate = TODAY AND CreatedBy.Id = :this.sCurrentUserId LIMIT 1];
		return (lFoundDone.size() == 1) ? true : false;
	}

	//////////////////////////////////////
	// * SUBMIT DONE FOR THE DAY DATA * //
	//////////////////////////////////////

	public PageReference SubmitDoneForTheDay()
	{
		List<FM_Done_For_The_Day__c> lAddEntries = new List<FM_Done_For_The_Day__c>(); 
		FM_Done_For_The_Day__c oOneEntry;
		Integer iTotalRouteCount = 0;

		// Check IF Already Submitted --> Reload Page
		if(CheckCurrentSubmittedRoutes() == true)
		{
			return RedirectDonePage();
		}

		for(DoneForTheDayRoute oOneRoute : lUserRouteStatus)
		{
			oOneEntry = new FM_Done_For_The_Day__c();
			oOneEntry.Operator__c = this.sCurrentUserId;
			oOneEntry.Operator_Route__c = oOneRoute.sRouteId;
			oOneEntry.Done_Date__c = System.today();
			iTotalRouteCount = 0;

			for(DoneForTheDayRouteEntry oOneLocation : oOneRoute.lEntries)
			{
				iTotalRouteCount += oOneLocation.iRunSheetCount;
			}

			oOneEntry.Runsheet_Count__c = iTotalRouteCount;
			oOneEntry.Unique_Key__c = oOneEntry.Done_Date__c + '-' + oOneRoute.sRouteNumber + '-' + this.sCurrentUserId; 

			lAddEntries.add(oOneEntry);
		}

		if(lAddEntries.size() > 0)
		{
			try
			{
				insert lAddEntries;
			}
			catch(DMLException e)
			{
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error creating done for the day: ' + e));
				return null;
			}
		}

		this.bAlreadySubmittedForTheDay = CheckCurrentSubmittedRoutes();
		return RedirectListView();
	}

	///////////////////////////////////////////////////////////
	// * CHECK USERS PERMISSION SET -> DETERMINE IF EXISTS * //
	///////////////////////////////////////////////////////////

	private Boolean VerifyPermissions(String sUserId, String sPermissionSet)
	{
		List<PermissionSetAssignment> lPermissions = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :sUserId AND PermissionSet.Name = :sPermissionSet];
		return (lPermissions.size() == 1) ? true : false;
	}

	/////////////////////////////////////////////////////////
	// * CHECK USERS PROFILE -> DETERMINE IF IS ASSIGNED * //
	/////////////////////////////////////////////////////////

	private Boolean VerifyProfile(String sUserId, String sProfileName)
	{
		List<User> lUsers = [SELECT Id FROM User WHERE Id = :sUserId AND Profile.Name = :sProfileName];
		return (lUsers.size() == 1) ? true : false;
	}

	/////////////////////////////////////////
	// * REDIRECT TO RUNSHEET LIST VIEW * ///
	/////////////////////////////////////////

	public PageReference RedirectListView()
	{
		Schema.DescribeSObjectResult oSchemaResult = FM_Run_Sheet__c.sObjectType.getDescribe();
		PageReference oPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + oSchemaResult.getKeyPrefix());

		oPage.setRedirect(true);
		return oPage;
	}

	/////////////////////////////////////////////////////////
	// * REDIRECT TO DONE FOR THE DAY CONFIRMATION PAGE * ///
	/////////////////////////////////////////////////////////

	private PageReference RedirectDonePage()
	{
		PageReference oPage = Page.FM_Done_for_the_day;
		oPage.setRedirect(true);

		return oPage;
	}

	/////////////////////////////////////////////////////
	// * CLASS FOR STORING ROUTE STATISTICS - PARENT * //
	/////////////////////////////////////////////////////

	public class DoneForTheDayRoute
	{
		private List<DoneForTheDayRouteEntry> lEntries;
		private Integer iDoneCount;

		public String sRouteId {get; private set;}
		public String sRouteNumber {get; private set;}
		public String sDeskName {get; private set;}

		public DoneForTheDayRoute(String sRouteId, String sRoute, String sDesk)
		{
			this.lEntries = new List<DoneForTheDayRouteEntry>();
			this.sRouteId = sRouteId;
			this.sRouteNumber = sRoute;
			this.sDeskName = sDesk;
			this.iDoneCount = 0;
		}

		public void AddOperatorEntry(DoneForTheDayRouteEntry oNewEntry)
		{
			this.lEntries.add(oNewEntry);
			this.iDoneCount = (oNewEntry.dDoneTimeStamp != null) ? this.iDoneCount + 1 : this.iDoneCount; 
		}

		public List<DoneForTheDayRouteEntry> getEntries()
		{
			return this.lEntries;
		}

		public Integer getEntriesSize()
		{
			return this.lEntries.size();
		}

		public Boolean getRouteComplete()
		{
			return (this.lEntries.size() == this.iDoneCount) ? true : false;
		}
	}

	////////////////////////////////////////////////////
	// * CLASS FOR STORING ROUTE STATISTICS - CHILD * //
	////////////////////////////////////////////////////

	public class DoneForTheDayRouteEntry
	{
		public DateTime dDoneTimeStamp {get; private set;}
		public DateTime dLatestRunSheet {get; private set;}
		public String sLatestRunSheetId {get; private set;}
		public String sCreatedById {get; private set;}
		public String sCreatedByName {get; private set;}
		public Integer iRunSheetCount {get; private set;}
		public Integer iSortOrder {get; private set;}
		public Boolean bPreviousDone {get; private set;}

		public DoneForTheDayRouteEntry(String sRouteNumber, Date dDoneTime, DateTime dTime, String sCreatedById, String sCreatedByName, Integer iCount, Integer iSort, Boolean bWasDone, Id sLastCreatedId, DateTime dLastCreatedTime)
		{
			this.dDoneTimeStamp = dTime;
			this.dLatestRunSheet = dTime;
			this.sLatestRunSheetId = null;
			this.sCreatedById = sCreatedById;
			this.sCreatedByName = sCreatedByName;
			this.iRunSheetCount = iCount;
			this.iSortOrder = iSort;
			this.bPreviousDone = bWasDone;

			this.sLatestRunSheetId = sLastCreatedId;
			this.dLatestRunSheet = dLastCreatedTime;
		}

		public DateTime getDoneTime()
		{
			return this.dDoneTimeStamp;
		}

		public String getDoneTimeFormatted()
		{
			return (this.dDoneTimeStamp != null) ? this.dDoneTimeStamp.format(DATE_FORMAT) : '';
		}

		public Integer getRunSheetCount()
		{
			return this.iRunSheetCount;
		}

		public Integer getSortOrder()
		{
			return this.iSortOrder;
		}

		public Boolean getIsDone()
		{
			return (this.bPreviousDone == true || this.dDoneTimeStamp != null) ? true : false;
		}

		public Boolean getAfterDone()
		{
			return (this.bPreviousDone == true && this.dDoneTimeStamp < dLatestRunSheet) ? true : false;
		}
	}

	/////////////////////////////////
	// * GETTER + SETTER METHODS * //
	/////////////////////////////////

	public List<DoneForTheDayRoute> getFullRouteStatus()
	{
		return this.lFullRouteStatus;
	}

	public List<DoneForTheDayRoute> getUserRouteStatus()
	{
		return this.lUserRouteStatus;
	}

	public List<SelectOption> getDeskNames()
	{
		return this.lDeskNames;
	}

	public void setDoneForTheDay(FM_Done_For_The_Day__c oNewDone)
	{
		this.oDoneForTheDay = oNewDone;
	}

	public FM_Done_For_The_Day__c getDoneForTheDay()
	{
		return this.oDoneForTheDay;
	}

	public String getCurrentUserName()
	{
		return this.sCurrentUserName;
	}

	public Integer getRouteCount()
	{
		return this.lFullRouteStatus.size();
	}

	public String getTimeZone()
	{
		DateTime dTimezone = this.oDoneForTheDay.Done_Date__c;
		return dTimezone.format('z');
	}

	public Boolean getMarkCompletePermission()
	{
		return this.bMarkCompletePermission;
	}

	public Boolean getAlreadySubmittedForTheDay()
	{
		return this.bAlreadySubmittedForTheDay;
	}

	public Boolean getFluidSubmittedForTheDay()
	{
		return (this.lUserRouteStatus.size() > 0);
	}

	public Boolean getViewPermission()
	{
		return this.bViewPermission;
	}

	public Boolean getIsDispatcherStandard()
	{
		return VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_DISPATCH_STANDARD) || VerifyPermissions(this.sCurrentUserId, PERMISSION_SET_DISPATCH_VENDOR);
	}
}