/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO layer interface surfacing API methods that handles data access to Permission
                related records.
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public interface HOG_PermissionDAO {

    List<PermissionSet> getPermissionSetsByName(Set<String> names);
    Integer getAssignmentCountForPermissions(Id userId, Set<Id> permissionIds);

}