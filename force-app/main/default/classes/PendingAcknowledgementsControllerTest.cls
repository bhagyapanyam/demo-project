@isTest
public with sharing class PendingAcknowledgementsControllerTest {

    @TestSetup
    static void setup(){
        Account acct = new Account(Name = 'Sunrise');
        insert acct;

        Contact ct = new Contact(
            AccountId = acct.Id,
            LastName = 'Testing',
            FirstName = 'Unit',
            Husky_AD_Username__c = 'unttst'
        );  
        
        insert ct;

        Standing_Order__c stdOrder1 = new Standing_Order__c();
        stdOrder1.Name = 'Standing Order 1';
        stdOrder1.Details__c = 'Standing Order 1 details here';
        stdOrder1.Plant__c = 'Oil Processing';
        stdOrder1.Start_Date__c = System.Today() - 365;
        stdOrder1.Completion_Date__c = System.Today() + 365;

        Standing_Order__c stdOrder2 = new Standing_Order__c();
        stdOrder2.Name = 'Standing Order 2';
        stdOrder2.Details__c = 'Standing Order 2 details here';
        stdOrder2.Plant__c = 'Field Facilities';
        stdOrder2.Start_Date__c = System.Today() - 365;
        stdOrder2.Completion_Date__c = System.Today();         
        
        insert stdOrder1;
        insert stdOrder2;
    }

    @isTest
    private static void testReportWhenAcknowledgeAllStdOrders() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard Oilsands User'];
        UserRole role = [select Id from UserRole where DeveloperName = 'Sunrise_Operations'];

        User usr =  TestUtilities.createTestUser(p);
        usr.FederationIdentifier = 'unttst';
        usr.UserRoleId = role.Id;

        insert usr;

        PermissionSet ps = [select Id from PermissionSet where Name ='Operations_Log_Admin'];
        PermissionSetAssignment assignUser = new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id);
        insert assignUser; 
        
        List<Standing_Order__c> standingOrders = [SELECT Id, Name, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c];
        OperationsStandingOrderController stdOrderController = new OperationsStandingOrderController();

        System.runAs(usr) {
            stdOrderController.selectedStandingOrderId = standingOrders[0].Id;
            stdOrderController.confirmReadAcknowledgement();

            stdOrderController.selectedStandingOrderId = standingOrders[1].Id;
            stdOrderController.confirmReadAcknowledgement();            
        }

        PendingAcknowledgementsController pendingController = new PendingAcknowledgementsController();
        System.assertEquals(2, pendingController.pendingStandingOrderAcknowledgementsMap.size());
        
        List<String> mapKeys = new List<String>(pendingController.pendingStandingOrderAcknowledgementsMap.keySet());

        List<PendingAcknowledgementsController.PendingStandingOrderAcknowledgements> pendingAcknowledgements = pendingController.pendingStandingOrderAcknowledgementsMap.get(mapKeys[0]);
        System.assertEquals(0, pendingAcknowledgements.size());

        pendingAcknowledgements = pendingController.pendingStandingOrderAcknowledgementsMap.get(mapKeys[1]);
        System.assertEquals(0, pendingAcknowledgements.size());
    }

    @isTest
    private static void testReportWhenAcknowledgeSomeStdOrders() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard Oilsands User'];
        UserRole role = [select Id from UserRole where DeveloperName = 'Sunrise_Operations'];

        User usr =  TestUtilities.createTestUser(p);
        usr.FederationIdentifier = 'unttst';
        usr.UserRoleId = role.Id;

        insert usr;

        PermissionSet ps = [select Id from PermissionSet where Name ='Operations_Log_Admin'];
        PermissionSetAssignment assignUser = new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id);
        insert assignUser; 
        
        List<Standing_Order__c> standingOrders = [SELECT Id, Name, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c ORDER BY Name];
        OperationsStandingOrderController stdOrderController = new OperationsStandingOrderController();

        System.runAs(usr) {
            stdOrderController.selectedStandingOrderId = standingOrders[0].Id;
            stdOrderController.confirmReadAcknowledgement();          
        }

        PendingAcknowledgementsController pendingController = new PendingAcknowledgementsController();
        System.assertEquals(2, pendingController.pendingStandingOrderAcknowledgementsMap.size());

        List<String> mapKeys = new List<String>(pendingController.pendingStandingOrderAcknowledgementsMap.keySet());

        List<PendingAcknowledgementsController.PendingStandingOrderAcknowledgements> pendingAcknowledgements = pendingController.pendingStandingOrderAcknowledgementsMap.get(mapKeys[0]);
        System.assertEquals(0, pendingAcknowledgements.size());

        pendingAcknowledgements = pendingController.pendingStandingOrderAcknowledgementsMap.get(mapKeys[1]);
        // System.assertEquals(1, pendingAcknowledgements.size());    
    }
    
    @isTest
    private static void testInnerClass() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard Oilsands User'];
        UserRole role = [select Id from UserRole where DeveloperName = 'Sunrise_Operations'];

        User usr =  TestUtilities.createTestUser(p);
        usr.FederationIdentifier = 'unttst';
        usr.UserRoleId = role.Id;

        insert usr;

        List<Standing_Order__c> standingOrders = [SELECT Id, Name, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c ORDER BY Name];
        
        PendingAcknowledgementsController.PendingStandingOrderAcknowledgements innerClass = new PendingAcknowledgementsController.PendingStandingOrderAcknowledgements(standingOrders[0], usr);
        System.assertEquals(standingOrders[0].Name, innerClass.standingOrderName);
        System.assertEquals(standingOrders[0].Id, innerClass.standingOrderId);
        System.assertEquals(standingOrders[0].Plant__c, innerClass.platAreaName);
        System.assertEquals(usr.Id, innerClass.userId);
        System.assertEquals(usr.Name, innerClass.userName);
    }    

}