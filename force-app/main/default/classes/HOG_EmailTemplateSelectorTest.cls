/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_EmailTemplateSelector
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_EmailTemplateSelectorTest {

    @IsTest
    static void getTemplateByDevName_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EmailTemplate result;

        Test.startTest();
        try {
            result = new HOG_EmailTemplateSelector().getTemplateByDevName(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getTemplateByDevName_withWrongParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EmailTemplate result;

        Test.startTest();
        try {
            result = new HOG_EmailTemplateSelector().getTemplateByDevName('Wrong Name');
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getTemplateByDevName_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EmailTemplate result;

        Test.startTest();
        try {
            result = new HOG_EmailTemplateSelector().getTemplateByDevName(EPD_Constants.TEMPLATE_NAME_THRESHOLD_ALERT);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

}