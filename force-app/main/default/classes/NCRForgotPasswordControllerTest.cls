@IsTest 
public with sharing class NCRForgotPasswordControllerTest {     
  
    @IsTest(SeeAllData=true) 
    public static void testForgotPasswordController() {
       
        // Instantiate a new controller with all parameters in the page
        NCRForgotPasswordController controller = new NCRForgotPasswordController();
        controller.username = 'test@salesforce.com';

        ApexPages.CurrentPage().getParameters().put('hasError','true');  

        System.assertEquals(controller.forgotPassword(),null); 
    }
}