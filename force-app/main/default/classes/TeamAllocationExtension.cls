public  class TeamAllocationExtension extends AbstractControllerExtension{
    
    private String dateAsString;
    
    public Date selectedDate {get; set;}
    
    public selectedDateLogViewWrapper wrapper{get; set;}
    public Id selectedRT {get;set;}
    
    public List<SelectOption> teamAllocationRT {get;set;}
    
    public TeamAllocationExtension(ApexPages.StandardController controller) {
        this.selectedDateAsString = Datetime.now().format(DateUtilities.DDMMYYYY_DATE_FORMAT);
        getTeamAllocationRT();
      
        refreshTeamAllocations();
    }
    private void getTeamAllocationRT()
    {
        teamAllocationRT = new List<SelectOption>();
        Map<String, Schema.Recordtypeinfo> recTypesByName = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        Id operationRT = recTypesByName.get('Operations').getRecordTypeId();
        Id maintenanceRT = recTypesByName.get('Maintenance').getRecordTypeId();
        Id OTSTechnicalRT = recTypesByName.get('Operations Technical Training').getRecordTypeId();
        teamAllocationRT.add(new SelectOption(operationRT, 'Operations'));
        teamAllocationRT.add(new SelectOption(maintenanceRT, 'Maintenance'));
        teamAllocationRT.add(new SelectOption(OTSTechnicalRT, 'Operations Technical Training'));
        
        UserRole role = [select DeveloperName from UserRole where Id =: UserInfo.getUserRoleId()];
       
        if (role.DeveloperName.contains('Maintenance'))   
            selectedRT = maintenanceRT;
        else
            selectedRT = operationRT;
            
     /*   Profile currentProfile = [Select Name from Profile where Id =: UserInfo.getProfileId() ];
        if (currentProfile.Name == 'System Administrator')
        {
            for(String name : recTypesByName.keySet())
            {
                System.debug(name);
                if (name != 'Master')
                    teamAllocationRT.add(new SelectOption(recTypesByName.get(name).getRecordTypeId(), name));
            }
        }
        else
        {
            
            List<PermissionSetAssignment> permissionSets = [SELECT PermissionSet.Name 
                             FROM PermissionSetAssignment 
                             WHERE AssigneeId =: UserInfo.getUserId() 
                                 and (PermissionSet.Name like 'Operations%' or PermissionSet.Name like 'Maintenance%') ];
            if (permissionSets != null)
            {
                for(PermissionSetAssignment ps : permissionSets)
                {
                    if (ps.PermissionSet.Name.contains('Operations'))
                      teamAllocationRT.add(new SelectOption(recTypesByName.get('Operations').getRecordTypeId(), 'Operations'));
                      
                    if (ps.PermissionSet.Name.contains('Maintenance'))
                      teamAllocationRT.add(new SelectOption(recTypesByName.get('Maintenance').getRecordTypeId(), 'Maintenance'));  
                }
             
            }
        }
        */
    }
    public String getSelectedRTName()
    {
        Map<Id, Schema.Recordtypeinfo> recTypesById = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosById();
        return recTypesById.get(selectedRt).getName();
        
    }
    public String selectedDateAsString {
        get { return this.dateAsString; }
        
        // Make sure the internal date is in synch with this update.         
        set {
            this.dateAsString = value;
            
            if (String.isEmpty(this.dateAsString)) {
                this.selectedDate = null;
            } else if (this.selectedDate == null) {
                // Update the date instance.
                this.selectedDate = DateUtilities.toDMYYYDate(this.dateAsString, '/', null);
            } else {
                // Make sure they are the same.
                String dateString = DateUtilities.toDateTime(this.selectedDate).format(DateUtilities.DDMMYYYY_DATE_FORMAT);
                if (dateString != this.dateAsString) {
                    // Update the date instance.
                    this.selectedDate = DateUtilities.toDMYYYDate(this.dateAsString, '/', null);
                }
            }           
        }
    }
    
    public PageReference refreshTeamAllocations() {
        if (this.selectedDate != null) {
            wrapper = new SelectedDateLogViewWrapper(selectedDate, selectedRT);
        }
        return null;
    }
    
    // Run on Friday 11th January 2013 at 7:30PM By Clifford Berepiki
    public String currentRunInfo {
        get {
            return 'Run on ' + Datetime.now().format('EEEE dd-MM-yyyy \' at \' h:mma') + ' by ' + UserInfo.getName();
        }
    }
}