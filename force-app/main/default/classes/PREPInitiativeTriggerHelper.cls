public class PREPInitiativeTriggerHelper
{
    public static Boolean runTrigger = false;
    public static List<PREP_Initiative__c> newInitiativeList = new List<PREP_Initiative__c>();
    public static List<PREP_Initiative__c> oldInitiativeList = new List<PREP_Initiative__c>();
    public static Map<Id, PREP_Initiative__c> newInitiativeMap = new Map<Id, PREP_Initiative__c>();
    public static Map<Id, PREP_Initiative__c> oldInitiativeMap = new Map<Id, PREP_Initiative__c>();
    
    public static void createAcquisition(){
        Id prj = Schema.getGlobalDescribe().get( 'PREP_Initiative__c' ).getDescribe().getRecordTypeInfosByName().get( 'Projects Initiative' ).getRecordTypeId();
        Id matACId = Schema.getGlobalDescribe().get( 'PREP_Acquisition_Cycle__c' ).getDescribe().getRecordTypeInfosByName().get( 'Mat Acquisition Cycle' ).getRecordTypeId();
        Id servACId = Schema.getGlobalDescribe().get( 'PREP_Acquisition_Cycle__c' ).getDescribe().getRecordTypeInfosByName().get( 'Serv Acquisition Cycle' ).getRecordTypeId();
        
        List<PREP_Acquisition_Cycle__c> acqlistToBeInserted = new List<PREP_Acquisition_Cycle__c>();
        
        for( PREP_Initiative__c init: newInitiativeList ){
            if( init.RecordTypeId == prj ){
                PREP_Acquisition_Cycle__c MatAcqCycle = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 3,
                                                        Initiative__c = init.Id, Bid_Period__c =  28,
                                                        Bid_Evaluation_1__c = 21, Bid_Evaluation_2__c = 21,
                                                        Bid_Evaluation_3__c = 2, Bid_Evaluation_4__c = 7,
                                                        Husky_Approval__c = 7, MRP_Prep__c = 7,
                                                        PO_Prep_Issue__c = 9, Shipping_Duration__c = 7,
                                                        Type__c = 'Standard 12', RecordTypeId = matACId );
                acqlistToBeInserted.add( MatAcqCycle );
        
                PREP_Acquisition_Cycle__c MatAcqCycle2 = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 3,
                                                         Initiative__c = init.Id, Bid_Period__c =  14,
                                                         Bid_Evaluation_1__c = 7, Bid_Evaluation_2__c = 7,
                                                         Bid_Evaluation_3__c = 1, Bid_Evaluation_4__c = 1,
                                                         Husky_Approval__c = 7, MRP_Prep__c = 7, 
                                                         PO_Prep_Issue__c = 2, Shipping_Duration__c = 7,
                                                         Type__c = 'Standard 6', RecordTypeId = matACId );
                acqlistToBeInserted.add( MatAcqCycle2 );
        
                PREP_Acquisition_Cycle__c MatAcqCycle3 = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 1,
                                                         Initiative__c = init.Id, Bid_Period__c =  5,
                                                         Bid_Evaluation_1__c = 2, Bid_Evaluation_2__c = 2,
                                                         Bid_Evaluation_3__c = 2, Bid_Evaluation_4__c = 1,
                                                         Husky_Approval__c = 1, MRP_Prep__c = 0,
                                                         PO_Prep_Issue__c = 2, Shipping_Duration__c = 7,
                                                         Type__c = 'Standard 2', RecordTypeId = matACId );
                acqlistToBeInserted.add( MatAcqCycle3 );
                
                PREP_Acquisition_Cycle__c ServAcqCycle = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 3,
                                                         Initiative__c = init.Id, Bid_Period__c =  28,
                                                         Bid_Evaluation_1__c = 21, Bid_Evaluation_2__c = 21,
                                                         Bid_Evaluation_3__c = 2, Bid_Evaluation_4__c = 7,
                                                         Husky_Approval__c = 7, CWP_Prep__c = 7,
                                                         CT_Prep_Issue__c = 9, Kick_off_Mobilization__c = 5,
                                                         Demob__c = 5, Type__c = 'Standard 12',
                                                         RecordTypeId = servACId );
                acqlistToBeInserted.add( ServAcqCycle );      
        
                PREP_Acquisition_Cycle__c ServAcqCycle2 = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 3,
                                                          Initiative__c = init.Id, Bid_Period__c =  14,
                                                          Bid_Evaluation_1__c = 7, Bid_Evaluation_2__c = 7,
                                                          Bid_Evaluation_3__c = 1, Bid_Evaluation_4__c = 1,
                                                          Husky_Approval__c = 7, CWP_Prep__c = 7,
                                                          CT_Prep_Issue__c = 2, Kick_off_Mobilization__c = 3,
                                                          Demob__c = 3, Type__c = 'Standard 6',
                                                          RecordTypeId = servACId );
                acqlistToBeInserted.add( ServAcqCycle2 );
            
                PREP_Acquisition_Cycle__c ServAcqCycle3 = new PREP_Acquisition_Cycle__c( RFQ_Prep__c = 1,
                                                          Initiative__c = init.Id, Bid_Period__c =  5,
                                                          Bid_Evaluation_1__c = 2, Bid_Evaluation_2__c = 2,
                                                          Bid_Evaluation_3__c = 2, Bid_Evaluation_4__c = 1,
                                                          Husky_Approval__c = 1, CWP_Prep__c = 0,
                                                          CT_Prep_Issue__c = 2, Kick_off_Mobilization__c = 2,
                                                          Demob__c = 2, Type__c = 'Standard 2',
                                                          RecordTypeId = servACId );
                acqlistToBeInserted.add( ServAcqCycle3 );
            }
        }
        
        if( acqlistToBeInserted.size() > 0 )
            insert acqlistToBeInserted;
    }
}