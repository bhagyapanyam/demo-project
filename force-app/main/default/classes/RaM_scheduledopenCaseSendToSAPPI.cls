global class RaM_scheduledopenCaseSendToSAPPI implements Schedulable {
   global void execute(SchedulableContext sc) {
       RaM_openCaseSendToSAPPI aula=new RaM_openCaseSendToSAPPI();

        aula.Query = 'Select Id,CaseNumber,Ticket_Number_R_M__c,Status,LocationNumber__r.name,RecordTypeId  from case where Status IN (\'Assigned\',\'Pending Parts\',\'Work in Progress\',\'Pending Approval\',\'Disputed\',\'Resolved\') AND (  RecordTypeId=\'' +RaM_ConstantUtility.CHILD_TICKET_RECORD_TYPE_ID+'\' OR RecordTypeId= \''+ RaM_ConstantUtility.TICKET_RECORD_TYPE_ID +'\')';
        //if(!Test.isRunningTest()) { ID batchprocessid = Database.executeBatch(aula,2000); }
       ID batchprocessid = Database.executeBatch(aula,2000); 
   }
}