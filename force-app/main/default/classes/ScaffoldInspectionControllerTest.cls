@isTest(SeeAllData=false)
private class ScaffoldInspectionControllerTest {
		
	 static testMethod void testScaffoldInspectionController() {
		Scaffold__c scaffold = ScaffoldTestData.createScaffold();
		Scaffold_Inspection__c scaffoldInspection = ScaffoldInspectionTestData.createScaffoldInspection(scaffold);
		
		ApexPages.Standardcontroller sc = New ApexPages.StandardController(scaffoldInspection);
        Test.SetCurrentPageReference(New PageReference('Page.Scaffold_Inspection'));
        ScaffoldInspectionController cont = new ScaffoldInspectionController(sc);
        cont.record.Toe_board_surface__c = 'Yes';
        cont.record.Toe_board_point_load__c = 'Yes';
        cont.record.Toe_board_above_platform__c = 'Yes';
        cont.record.Tags_updated__c = 'Yes';
        cont.record.Tags_accessess__c = 'Yes';
        cont.record.Stairs_wide__c = 'Yes';
        cont.record.Stairs_vertical__c = 'Yes';
        
        cont.save();
        
        scaffoldInspection = [SELECT Id, isCertified__c, isLocked__c FROM Scaffold_Inspection__c WHERE Id =: cont.record.Id];
        System.assertNotEquals(scaffoldInspection.Id, null);
        System.assertEquals(scaffoldInspection.isCertified__c, 'Violation or Incomplete');
        System.assertEquals(scaffoldInspection.isLocked__c, False);
        
        cont.record.Stairs_height__c = 'Yes';
        cont.record.Stairs_depth__c = 'Yes';
        cont.record.Stairs_angle__c = 'Yes';
        cont.record.Sills_Secured_from_movement__c = 'Yes';
        cont.record.Sills_Load_Distribution__c = 'Yes';
        cont.record.Sills_Hard_Surfaces__c = 'Yes'; 
        
        cont.submit();
        
        scaffoldInspection = [SELECT Id, isCertified__c, isLocked__c FROM Scaffold_Inspection__c WHERE Id =: cont.record.Id];
        System.assertNotEquals(scaffoldInspection.Id, null);
        System.assertEquals(scaffoldInspection.isCertified__c, 'Violation or Incomplete');
        System.assertEquals(scaffoldInspection.isLocked__c, True);
        
        scaffold = [SELECT Id, Days_Remaining__c, Inspection_Deadline__c, Last_Inspection__c FROM Scaffold__c WHERE Id =: scaffold.Id];
        System.assertNotEquals(scaffold.Id, Null);
        System.assertEquals(scaffold.Last_Inspection__c, Date.Today());
        System.assertEquals(scaffold.Inspection_Deadline__c, null);
        System.assertEquals(scaffold.Days_Remaining__c, 0);
	}
}