/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentEngineService
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EquipmentEngineServiceTest {

    @IsTest
    static void getEngineByEquipmentId_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Equipment_Engine__c result;
        Exception resultException;

        Test.startTest();
        try {
            result = new EquipmentEngineService().getEngineByEquipmentId(null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assertEquals(new HOG_Exception().getTypeName(), resultException.getTypeName());
    }

    @IsTest
    static void getEngineByEquipmentId_withWrongParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Equipment_Engine__c result;
        Exception resultException;

        Test.startTest();
        try {
            result = new EquipmentEngineService().getEngineByEquipmentId(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assertEquals(new HOG_Exception().getTypeName(), resultException.getTypeName());
    }

    @IsTest
    static void getEngineByEquipmentId_mockedFail() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Equipment_Engine__c result;
        Exception resultException;

        Test.startTest();
        try {
            EPD_DAOProvider.equipmentEngineDAO = new GetEngineByEquipmentIdMockMultiple();
            result = new EquipmentEngineService().getEngineByEquipmentId(null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assertEquals(new HOG_Exception().getTypeName(), resultException.getTypeName());
    }

    @IsTest
    static void getEngineByEquipmentId_mockedSuccess() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Equipment_Engine__c result;
        Exception resultException;

        Test.startTest();
        try {
            EPD_DAOProvider.equipmentEngineDAO = new GetEngineByEquipmentIdMockSingle();
            result = new EquipmentEngineService().getEngineByEquipmentId(null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertNotEquals(null, result);
    }

    public class GetEngineByEquipmentIdMockSingle implements EquipmentEngineDAO {
        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(new Equipment_Engine__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }
        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }

    public class GetEngineByEquipmentIdMockMultiple implements EquipmentEngineDAO {
        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(new Equipment_Engine__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false),
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(new Equipment_Engine__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }
        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }

    @IsTest
    static void updateEngines_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Exception resultException;

        Test.startTest();
        try {
            new EquipmentEngineService().updateEngines(null, null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, resultException);
    }

    @IsTest
    static void updateEngines_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Integer expectedNumberOfDMLs = 0;

        Test.startTest();
        try {
            new EquipmentEngineService().updateEngines(new List<Equipment__c>(), null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, Limits.getDmlStatements());
    }

    @IsTest
    static void createEngines_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Exception resultException;

        Test.startTest();
        try {
            new EquipmentEngineService().createEngines(null, null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, resultException);
    }

    @IsTest
    static void createEngines_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Integer expectedNumberOfDMLs = 0;

        Test.startTest();
        try {
            new EquipmentEngineService().createEngines(new List<Equipment__c>(), null);
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, Limits.getDmlStatements());
    }

}