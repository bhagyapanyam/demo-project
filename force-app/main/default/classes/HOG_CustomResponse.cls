public abstract class HOG_CustomResponse extends HOG_RequestResult{
	@AuraEnabled public List<Object> resultObjects {get;set;}

	abstract void addResult(Object result);

}