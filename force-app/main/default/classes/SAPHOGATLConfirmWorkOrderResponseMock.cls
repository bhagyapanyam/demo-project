@isTest
global with sharing class SAPHOGATLConfirmWorkOrderResponseMock  implements WebServiceMock {
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse responseElement = new SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse();
   		responseElement.Message = 'ATLConfirmWorkOrderResponse Test Message';
   		responseElement.WorkOrderNumber = 'WorkOrderNumber Test';
   		responseElement.type_x = True;
   		
   		response.put('response_x', responseElement);
	}
}