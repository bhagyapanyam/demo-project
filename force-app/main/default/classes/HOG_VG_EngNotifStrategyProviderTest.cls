/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for HOG_VG_EngNotifStrategyProvider
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_VG_EngNotifStrategyProviderTest {

    @IsTest
    static void missingLocation() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult;
        Boolean result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        try {
            result = strategyProvider.shouldCreate(null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void withoutValues() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Location__c loc = new Location__c();

        try {
            result = strategyProvider.shouldCreate(loc);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void wrongCategory() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Location__c loc = new Location__c(Functional_Location_Category__c = 1);

        try {
            result = strategyProvider.shouldCreate(loc);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void wrongWellType() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Location__c loc = new Location__c(
                Functional_Location_Category__c = 4,
                Well_Type__c = 'Wrong'
        );

        try {
            result = strategyProvider.shouldCreate(loc);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void wrongAMU() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Field__c amu = new Field__c(
                Planner_Group__c = '010'
        );
        amu.recalculateFormulas();
        Location__c loc = new Location__c(
                Functional_Location_Category__c = 4,
                Well_Type__c = 'OIL',
                Operating_Field_AMU__r = amu
        );

        try {
            result = strategyProvider.shouldCreate(loc);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void correct() {
        HOG_VG_AlertStrategyProvider strategyProvider = new HOG_VG_EngNotifStrategyProvider();
        Boolean expectedResult = true;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Field__c amu = new Field__c(
                Planner_Group__c = '000'
        );
        amu.recalculateFormulas();
        Location__c loc = new Location__c(
                Functional_Location_Category__c = 4,
                Well_Type__c = 'OIL',
                Operating_Field_AMU__r = amu
        );

        try {
            result = strategyProvider.shouldCreate(loc);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

}