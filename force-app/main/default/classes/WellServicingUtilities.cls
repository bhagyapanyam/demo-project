/*------------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class for Well Servicing
Test Class : WellServicingUtilitiesTest, TestNewHOGTriggers
History    :
            02.05.15    rbo Modified TriggerBeforeUpdateWellServicingForm - mainDescription
            02.18.15    Gangha Kaliyan  To change operator lookups from contact to user
            10.16.15    rbo modifications related to SNR and Tech Debt fixes
            01.08.16    rbo Modifications due to Refactoring
                            replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c               
                            replaced all Rigless_Servicing_Form__c with Work_Order__c
                            replaced all Production_Engineer2__c with Production_Engineer_User__c
                            bulkified the code              
-------------------------------------------------------------------------------------------------------------*/
public with sharing class WellServicingUtilities  
{
    /***********************************************************************************************
     * Utility used to execute the Trigger code, value defaults to true
     ***********************************************************************************************/    
    public static Boolean executeTriggerCode = true;  


    /**********************************************************************************************************
     * Code manager of "before update" trigger of HOG_Rigless_Servicing_Form
     **********************************************************************************************************/    
    public static void TriggerBeforeUpdateWellServicingForm(List<HOG_Maintenance_Servicing_Form__c> newRecords, 
    Map<Id, HOG_Maintenance_Servicing_Form__c> oldMap)
    {
        System.debug('\n*****************************************\n'
            + 'METHOD: HOG_Rigless_Servicing_Form_Trigger()'
            + '\nTrigger: executed'
            + '\n************************************************\n');                  
    
        DateTime dateTimeCompletion = DateTime.now();
        DateTime dateTimeWellTrackerCompletion;
        DateTime dateTimeWorkOrderCompletion;
        Boolean updateWellTrackerCompletion = false;
        Boolean updateWorkOrderCompletion = false;
        Boolean updateWellShutIn = false;

        //final Set<Id> equipmentIds = new Set<Id>();
        //final Set<Id> locationIds = new Set<Id>();
        //final Set<Id> facilityIds = new Set<Id>();
        final Set<Id> routeIds = new Set<Id>();
        final Set<Id> fieldIds = new Set<Id>();
        final Set<Id> workOrderIds = new Set<Id>();

        for (HOG_Maintenance_Servicing_Form__c riglessServicingForm : newRecords)
        {
            if (String.isBlank(riglessServicingForm.Equipment__c))                            
            {    
                if (String.isBlank(riglessServicingForm.Location__c))                                           
                {
                    //facilityIds.add(riglessServicingForm.Facility__c);
                    routeIds.add(riglessServicingForm.Facility__r.Plant_Section__c);
                    fieldIds.add(riglessServicingForm.Facility__r.Operating_Field_AMU__c);
                }
                else
                {    
                    //locationIds.add(riglessServicingForm.Location__c);
                    //facilityIds.add(riglessServicingForm.Location__r.Facility__c);
                    routeIds.add(riglessServicingForm.Location__r.Route__c);
                    fieldIds.add(riglessServicingForm.Location__r.Operating_Field_AMU__c);
                }
            }
            else
            {
                //equipmentIds.add(riglessServicingForm.Equipment__c);
                
                if (String.isBlank(riglessServicingForm.Equipment__r.Location__c))
                {
                    //facilityIds.add(riglessServicingForm.Equipment__r.Facility__c);
                    routeIds.add(riglessServicingForm.Equipment__r.Facility__r.Plant_Section__c);
                    fieldIds.add(riglessServicingForm.Equipment__r.Facility__r.Operating_Field_AMU__c);
                }
                else
                {
                    //locationIds.add(riglessServicingForm.Equipment__r.Location__c);
                    //facilityIds.add(riglessServicingForm.Equipment__r.Location__r.Facility__c);
                    routeIds.add(riglessServicingForm.Equipment__r.Location__r.Route__c);
                    fieldIds.add(riglessServicingForm.Equipment__r.Location__r.Operating_Field_AMU__c);
                }
            }
            
            workOrderIds.add(riglessServicingForm.Id);                            
        }
        
        final List<Route__c> routes =                 
            [Select 
                    Id,
                    Operator_1_User__c, 
                    Operator_2_User__c, 
                    Operator_3_User__c, 
                    Operator_4_User__c,
                    Operator_5_User__c,
                    Operator_6_User__c,
                    Field_Senior__c
                From Route__c
                Where Id In :routeIds];    

        final Map<String, Route__c> routeRecordMap = new Map<String, Route__c>(routes);
 
        final List<Field__c> operatingFields = 
            [Select
                    Id, 
                    Production_Engineer_User__c 
                From Field__c 
                Where Id In :fieldIds];        

        final Map<String, Field__c> fieldRecordMap = new Map<String, Field__c>(operatingFields);

        // get indicators for validating start and stop time, and pressure test fields                
        final List<HOG_Maintenance_Servicing_Form__c> servicesRequired =
            [Select
                Id, 
                HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Validate_Start_and_Stop_Time__c,
                HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Validate_Pressure_Test__c,
                HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Vendor_Company_Is_Not_Required__c
            From HOG_Maintenance_Servicing_Form__c Where Id In :workOrderIds];

        final Map<String, HOG_Maintenance_Servicing_Form__c> serviceRequiredRecordMap = new Map<String, HOG_Maintenance_Servicing_Form__c>(servicesRequired);
        
        // Get the lowest recorded value of Service Time Rig Off Location
        final Map<String, DateTime> serviceTimeRecordMap = new Map<String, DateTime>();
        
        final AggregateResult[] groupedServiceTime = 
            [Select 
                    Work_Order__c, 
                    MIN(Rig_Off_Location__c) Rig_Off_Location__c
                From HOG_Service_Time__c
                Where Work_Order__c In :workOrderIds
                Group By Work_Order__c];

        for (AggregateResult ar : groupedServiceTime)  
        {
            serviceTimeRecordMap.put
                (
                    (String)ar.get('Work_Order__c'),
                    (DateTime)ar.get('Rig_Off_Location__c')
                );
        }
                              
        // Get the lowest recorded value of Service Time Rig Off Location
        final Map<String, DateTime> vendorTimeRecordMap = new Map<String, DateTime>();
                
        final AggregateResult[] groupedVendorTime = 
            [Select 
                    Work_Order__c, 
                    MIN(Off_Location_Time__c) Off_Location_Time__c
                From HOG_Vendor_Personnel__c
                Where Work_Order__c In :workOrderIds
                Group By Work_Order__c];

        for (AggregateResult ar : groupedVendorTime)  
        {
            vendorTimeRecordMap.put
                (
                    (String)ar.get('Work_Order__c'),
                    (DateTime)ar.get('Off_Location_Time__c')
                );
        }                                    

        for (HOG_Maintenance_Servicing_Form__c riglessServicingForm : newRecords)
        {
            HOG_Maintenance_Servicing_Form__c oldriglessServicingForm = oldMap.get(riglessServicingForm.Id);

            if (oldriglessServicingForm.TECO__c)
                riglessServicingForm.addError('You can not modify this work order, it has already been "TECOed".');
                
            if (oldriglessServicingForm.TECO__c != riglessServicingForm.TECO__c)
                riglessServicingForm.addError('You can not do "TECO" from this page, '
                    + 'please use the "TECO" button located at "Well Servicing Work Order Detail" page.');

            if (oldriglessServicingForm.Service_Status__c == 'Cancelled' && riglessServicingForm.Service_Status__c == 'Cancelled')
                riglessServicingForm.addError('You can not modify this work order, it has already been "Cancelled".');

            if (riglessServicingForm.Name != oldriglessServicingForm.Name)
                riglessServicingForm.addError('You can not edit the Work Order Name.');

            HOG_Maintenance_Servicing_Form__c serviceRequired = serviceRequiredRecordMap.get(riglessServicingForm.Id);

            System.debug('riglessServicingForm: ' + riglessServicingForm);
            System.debug('oldriglessServicingForm: ' + oldriglessServicingForm);
                                                                
            // only execute if Service_Status__c or Well_On_Production__c or Stop_Time__c or Date_Completed__c has changed
            if (riglessServicingForm.Service_Status__c != oldriglessServicingForm.Service_Status__c 
            || riglessServicingForm.Well_On_Production__c != oldriglessServicingForm.Well_On_Production__c
            || riglessServicingForm.Stop_Time__c != oldriglessServicingForm.Stop_Time__c
            || riglessServicingForm.Date_Completed__c != oldriglessServicingForm.Date_Completed__c
            || riglessServicingForm.Action_Steps__c != oldriglessServicingForm.Action_Steps__c
            || riglessServicingForm.Service_Status_Reason__c != oldriglessServicingForm.Service_Status_Reason__c)            
            {
                updateWellShutIn =
                            riglessServicingForm.Action_Steps__c == 'Shut Well In' ||
                            oldriglessServicingForm.Action_Steps__c == 'Shut Well In' ||
                            riglessServicingForm.Service_Status_Reason__c == 'Shut Well In' ||
                            oldriglessServicingForm.Service_Status_Reason__c == 'Shut Well In';
                
                if (riglessServicingForm.Service_Status__c == 'Complete')
                {
                    if (riglessServicingForm.Service_Time_Is_Required__c)
                    {                                               
                        // Get the lowest recorded value of Service Time Rig Off Location
                        if (serviceTimeRecordMap.containskey(riglessServicingForm.Id))
                        {                        
                            DateTime serviceTime = serviceTimeRecordMap.get(riglessServicingForm.Id);
                            if (serviceTime == Null)
                            {
                                riglessServicingForm.addError('Service Time Log Rig Off Location must not be empty in order to set the Service Status to "Complete".');
                            }                                                       
                        }
                        else
                        {
                            riglessServicingForm.addError('Service Time Log must have at least one record in order to set the Service Status to "Complete".');
                        }                       
                    }
                    
                    if (riglessServicingForm.Vendor_Invoicing_Personnel_Is_Required__c)
                    {                       
                        // Get the lowest recorded value of the Vendor Personnel Time Off Location
                        if (vendorTimeRecordMap.containskey(riglessServicingForm.Id))
                        {
                            DateTime vendorTime = vendorTimeRecordMap.get(riglessServicingForm.Id);
                            if (vendorTime == Null)
                            {
                                riglessServicingForm.addError('Vendor Invoicing & Personnel Log Off Location must not be empty in order to set the Service Status to "Complete".');
                            }                           
                        }
                        else                        
                        {
                            riglessServicingForm.addError('Vendor Invoicing & Personnel Log must have at least one record in order to set the Service Status to "Complete".');
                        }
                    }
                    
                    if (!riglessServicingForm.Supervised__c)
                    {
                        if (serviceRequired != null)
                        { 
                            if (String.IsBlank(riglessServicingForm.Vendor_Company__c)
                            && !serviceRequired.HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Vendor_Company_Is_Not_Required__c)
                                riglessServicingForm.addError('Vendor Company must not be empty in order to set the Service Status to "Complete".');
                            
                            if (serviceRequired.HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Validate_Start_and_Stop_Time__c)
                            {
                                if (riglessServicingForm.Start_Time__c == null)
                                    riglessServicingForm.addError('Start Time must not be empty in order to set the Service Status to "Complete".');
        
                                if (riglessServicingForm.Stop_Time__c == null)
                                    riglessServicingForm.addError('Stop Time must not be empty in order to set the Service Status to "Complete".');
        
                                if (riglessServicingForm.Start_Time__c >= riglessServicingForm.Stop_Time__c)
                                    riglessServicingForm.addError('Stop Time must be greater than Start Time.');
                                                
                                if (riglessServicingForm.Stop_Time__c != null && riglessServicingForm.Stop_Time__c > DateTime.now())
                                    riglessServicingForm.addError('Stop Time must be less than or equal to now.');
                
                                if (riglessServicingForm.Well_Down_On__c != null && riglessServicingForm.Well_Down_On__c >= riglessServicingForm.Start_Time__c)
                                    riglessServicingForm.addError('Start Time must be greater than Well Down On.');                                    
                                
                                dateTimeCompletion = riglessServicingForm.Stop_Time__c;    
                            }
                            else
                            {
                                // validate Date_Completed__c field
                                if (riglessServicingForm.Date_Completed__c == null)
                                    riglessServicingForm.addError('Date Completed must not be empty in order to set the Service Status to "Complete".');

                                if (riglessServicingForm.Date_Completed__c != null && riglessServicingForm.Date_Completed__c > DateTime.now())
                                    riglessServicingForm.addError('Date Completed must be less than or equal to now.');
                
                                if (riglessServicingForm.Well_Down_On__c != null && riglessServicingForm.Well_Down_On__c >= riglessServicingForm.Date_Completed__c)
                                    riglessServicingForm.addError('Date Completed must be greater than Well Down On.');                                    

                                dateTimeCompletion = riglessServicingForm.Date_Completed__c;
                            }
                            
                            if (serviceRequired.HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Validate_Pressure_Test__c)
                            {
                                if (riglessServicingForm.Pressure_Test__c == null)    
                                    riglessServicingForm.addError('Pressure Test must not be empty in order to set the Service Status to "Complete".');
                            } 
                        }              
                    }
                    
                    if (riglessServicingForm.Well_On_Production__c == 'Yes')
                    {                    
                        updateWellTrackerCompletion = 
                            oldriglessServicingForm.Service_Status__c != 'Complete' || 
                            oldriglessServicingForm.Well_On_Production__c == 'No' ||
                            riglessServicingForm.Stop_Time__c != oldriglessServicingForm.Stop_Time__c ||
                            riglessServicingForm.Date_Completed__c != oldriglessServicingForm.Date_Completed__c ||
                            updateWellShutIn;
                                                                                                                                
                        dateTimeWellTrackerCompletion = dateTimeCompletion;                            
                    }
                    else
                    {                    
                        updateWellTrackerCompletion = 
                            (oldriglessServicingForm.Service_Status__c == 'Complete' && 
                            oldriglessServicingForm.Well_On_Production__c == 'Yes') ||
                            updateWellShutIn;
                                                    
                        dateTimeWellTrackerCompletion = null;
                    }
                                    
                    if (oldriglessServicingForm.Service_Status__c != 'Complete' || 
                    riglessServicingForm.Stop_Time__c != oldriglessServicingForm.Stop_Time__c ||
                    riglessServicingForm.Date_Completed__c != oldriglessServicingForm.Date_Completed__c)
                    {                        
                        dateTimeWorkOrderCompletion = dateTimeCompletion;
                            
                        updateWorkOrderCompletion = true;
                    }            
    
                }
                else
                {                
                    updateWellTrackerCompletion = 
                        (oldriglessServicingForm.Service_Status__c == 'Complete' && 
                        oldriglessServicingForm.Well_On_Production__c == 'Yes') ||
                        updateWellShutIn;
                                                
                    dateTimeWellTrackerCompletion = null;            
                    
                    if (oldriglessServicingForm.Service_Status__c == 'Complete' ||
                    riglessServicingForm.Stop_Time__c != oldriglessServicingForm.Stop_Time__c ||
                    riglessServicingForm.Date_Completed__c != oldriglessServicingForm.Date_Completed__c)                    
                    {
                        dateTimeWorkOrderCompletion = null;     
                        updateWorkOrderCompletion = true;
                    }                                   
                }
        
                if (updateWorkOrderCompletion)
                    riglessServicingForm.Service_Completed__c = dateTimeWorkOrderCompletion;
            }
            // only execute if Service_Status__c or Well_On_Production__c or Stop_Time__c or Date_Completed__c has changed
            else
            {
                // the following covers validations for both supervised and unsupervised services                
                if (serviceRequired != null) 
                {
                    if (serviceRequired.HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Service_Required__r.Validate_Start_and_Stop_Time__c)
                    {
                        if (riglessServicingForm.Stop_Time__c != null && riglessServicingForm.Start_Time__c == null)
                            riglessServicingForm.addError('Start Time cannot not be empty if Stop Time is not empty.');
            
                        if (riglessServicingForm.Start_Time__c >= riglessServicingForm.Stop_Time__c)
                            riglessServicingForm.addError('Stop Time must be greater than Start Time.');
                        
                        if (riglessServicingForm.Stop_Time__c != null && riglessServicingForm.Stop_Time__c > DateTime.now())
                            riglessServicingForm.addError('Stop Time must be less than or equal to now.');
                                
                        if (riglessServicingForm.Well_Down_On__c != null && riglessServicingForm.Well_Down_On__c >= riglessServicingForm.Start_Time__c)
                            riglessServicingForm.addError('Start Time must be greater than Well Down On.');    
                    }
                    else
                    {
                        if (riglessServicingForm.Date_Completed__c != null && riglessServicingForm.Date_Completed__c > DateTime.now())
                            riglessServicingForm.addError('Date Completed must be less than or equal to now.');
                                
                        if (riglessServicingForm.Well_Down_On__c != null && riglessServicingForm.Well_Down_On__c >= riglessServicingForm.Date_Completed__c)
                            riglessServicingForm.addError('Date Completed must be greater than Well Down On.');                        
                    }  
                }                                      
            }
                            
            If (String.IsBlank(oldriglessServicingForm.Work_Order_Number__c) && !String.IsBlank(riglessServicingForm.Work_Order_Number__c))
            {
                if (String.IsBlank(riglessServicingForm.Equipment__c))
                {   
                    riglessServicingForm.Name = (String.IsBlank(riglessServicingForm.Location__c) 
                        ? riglessServicingForm.Facility__r.Name
                        : riglessServicingForm.Location__r.Name) 
                        + ' - ' + riglessServicingForm.Work_Order_Number__c;
                }
                else
                {
                    riglessServicingForm.Name = riglessServicingForm.Equipment__r.Name + ' - ' + riglessServicingForm.Work_Order_Number__c;
                }
            }
                                         
            if (riglessServicingForm.Service_Status__c == 'Requires Reassignment') 
                riglessServicingForm.Service_Status__c = 'New Request';

            Route__c routeRecord = new Route__c();
            Field__c fieldRecord = new Field__c();

            if (String.IsBlank(riglessServicingForm.Equipment__c))
            {            
                routeRecord = String.IsBlank(riglessServicingForm.Location__c) 
                    ? routeRecordMap.get(riglessServicingForm.Facility__r.Plant_Section__c)
                    : routeRecordMap.get(riglessServicingForm.Location__r.Route__c);
                fieldRecord = String.IsBlank(riglessServicingForm.Location__c)
                    ? fieldRecordMap.get(riglessServicingForm.Facility__r.Operating_Field_AMU__c)
                    : fieldRecordMap.get(riglessServicingForm.Location__r.Operating_Field_AMU__c);                      
            }
            else
            {
                routeRecord = String.IsBlank(riglessServicingForm.Equipment__r.Location__c) 
                    ? routeRecordMap.get(riglessServicingForm.Equipment__r.Facility__r.Plant_Section__c)
                    : routeRecordMap.get(riglessServicingForm.Equipment__r.Location__r.Route__c);
                fieldRecord = String.IsBlank(riglessServicingForm.Equipment__r.Location__c)
                    ? fieldRecordMap.get(riglessServicingForm.Equipment__r.Facility__r.Operating_Field_AMU__c)
                    : fieldRecordMap.get(riglessServicingForm.Equipment__r.Location__r.Operating_Field_AMU__c);                     
            }

            if (routeRecord != null)
            {
                riglessServicingForm.Operator_1_User__c = routeRecord.Operator_1_User__c;
                riglessServicingForm.Operator_2_User__c = routeRecord.Operator_2_User__c;
                riglessServicingForm.Operator_3_User__c = routeRecord.Operator_3_User__c;
                riglessServicingForm.Operator_4_User__c = routeRecord.Operator_4_User__c;
                riglessServicingForm.Operator_5_User__c = routeRecord.Operator_5_User__c;
                riglessServicingForm.Operator_6_User__c = routeRecord.Operator_6_User__c;
                riglessServicingForm.Field_Senior__c = routeRecord.Field_Senior__c;            
            }

            if (fieldRecord != null)
            {
                riglessServicingForm.Production_Engineer_User__c = fieldRecord.Production_Engineer_User__c;
            }
                                
            if (riglessServicingForm.SAP_Generated_Work_Order_Number__c && !String.IsBlank(riglessServicingForm.Work_Order_Number__c))            
            {
                if (riglessServicingForm.Work_Details__c != oldriglessServicingForm.Work_Details__c || riglessServicingForm.Service_Status__c == 'Cancelled' || oldriglessServicingForm.Service_Status__c == 'Cancelled')
                {
                    // call the web service if both future calls and callouts limits have not been exceeded
                    if (Limits.getFutureCalls() != Limits.getLimitFutureCalls() && Limits.getCallouts() != Limits.getLimitCallouts())
                    {                                   

                        String userName = String.isEmpty(UserInfo.getFirstName()) ? '' : UserInfo.getFirstName().toUpperCase();                     
                        String workOrderNumber = riglessServicingForm.Work_Order_Number__c;                        
                        Integer lengthDescription = riglessServicingForm.Work_Details__c.length();
                        String mainDescription = riglessServicingForm.Work_Details__c.substring(0, (lengthDescription < 40 ? lengthDescription : 40));                                                                        
                        String mainLongDescription = riglessServicingForm.Work_Details__c + (riglessServicingForm.Service_Status__c == 'Cancelled'                                                     
                                ? ' - Cancelled By: ' + UserInfo.getLastName().toUpperCase() + ' ' + userName 
                                : '');
                        
                        String priority = null;
                        String mainWorkCentre = null;
                        String maintenanceActivityType = null;
                        String activityNumber = null;
                        String userStatus = null;                                   
                        Date lastChangedDate = riglessServicingForm.SAP_Changed_Date__c;
                        String lastChangedBy = riglessServicingForm.SAP_Changed_By__c;
                                        
                        updateServiceWorkOrderInSAP
                            (
                                riglessServicingForm.Id,
                                workOrderNumber,
                                mainDescription,
                                mainLongDescription,                            
                                priority,
                                mainWorkCentre,
                                maintenanceActivityType,
                                activityNumber,
                                userStatus,                                 
                                lastChangedDate,
                                lastChangedBy                       
                            );

                        If (riglessServicingForm.Service_Status__c == 'Cancelled')
                            CancelWorkOrderInSAP(riglessServicingForm.Id);                                                      
                    }               
                }
            }             
        }
    }            


    @future (callout=true)
    public static void CancelWorkOrderInSAP
    (
        Id workOrderId
    )
    {
        // create a 3-second delay for the updateServiceWorkOrderInSAP to do its job
        Long startingTime = System.now().getTime(); // Num milliseconds since Jan 1 1970
        Integer delayInMilliseconds = 3000; // three-second delay
        while (System.now().getTime() - startingTime < delayInMilliseconds)  
        {
            // Do nothing until desired delay has passed
        }

        // call TECO after delay has passed     
        String retunMessage = WellServicingGlobal.updateTECO(workOrderId, false);       
        System.debug('retunMessage: ' + retunMessage);      
    }
    
        
    @future (callout=true)
    public static void updateServiceWorkOrderInSAP
    (
        Id workOrderId,
        String workOrderNumber,
        String mainDescription,
        String mainLongDescription,                         
        String priority,
        String mainWorkCentre,
        String maintenanceActivityType,
        String activityNumber,      
        String userStatus,                          
        Date lastChangedDate,
        String lastChangedBy
    )
    {
        // Retrieve 1 record or nothing
        List<HOG_Maintenance_Servicing_Form__c> workOrderRecord =
            [Select
                Id,
                System_Status_Code__c,
                User_Status_Code__c,
                SAP_Changed_Date__c,
                SAP_Changed_By__c,
                Request_Generate_Number_Message__c                
            From HOG_Maintenance_Servicing_Form__c
            Where Id = :workOrderId];  

        //If (workOrderRecord != null && workOrderRecord.size() > 0)
        If (!workOrderRecord.isEmpty())
        {         
            SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();

            try
            {
                SAPHOGWorkOrderServices.UpdateWorkOrderResponse response = workOrder.UpdateWorkOrder
                    (
                        workOrderNumber,
                        mainDescription,            
                        mainLongDescription,
                        priority,
                        mainWorkCentre,
                        maintenanceActivityType,
                        activityNumber,
                        userStatus,
                        lastChangedDate,
                        lastChangedBy,
                        null
                    );
                
                if (response != null)
                {    
                    workOrderRecord[0].Request_Generate_Number_Message__c = response.Message;
                    
                    if (response.type_x)
                    {
                        workOrderRecord[0].System_Status_Code__c = response.WorkOrder.SystemStatus;
                        workOrderRecord[0].User_Status_Code__c = response.WorkOrder.UserStatus;
                        workOrderRecord[0].SAP_Changed_Date__c = response.WorkOrder.ChangedDate;
                        workOrderRecord[0].SAP_Changed_By__c = response.WorkOrder.ChangedBy;                                        
                    }
                }
            }
            catch(Exception e)
            {
                
                String exceptionMessage = 'METHOD: WellServicingUtilities.updateServiceWorkOrderInSAP() - UpdateWorkOrder'
                    + '\nException getMessage(): ' + e.getMessage();

                System.debug('\n****************************************************************\n'
                    + exceptionMessage
                    + '\n***********************************************************************\n');

                workOrderRecord[0].Request_Generate_Number_Message__c = exceptionMessage;                                                                           
            }

            // do not execute the Trigger
            executeTriggerCode = false;
            MaintenanceServicingUtilities.executeTriggerCode = false;
            
            update workOrderRecord;         
        }
        else
            System.debug('Work order record with an ID of ' + workOrderId + ' no longer exist!');        
    }        
}