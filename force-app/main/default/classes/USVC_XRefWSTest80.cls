@isTest(SeeAllData=true)

/*
 * Test class for web service USVC_XRefWS80 
 * This program is the test class for deployment of USVC_XRefWS60 to higher environments. 
 * /
 */

global class USVC_XRefWSTest80 {
    
      global class USVC_T4C {
          
      
          WebService String location;
          WebService String carrier_code;
          WebService String shipper_code;
          WebService String tankage;
          WebService String line;
          WebService String transtype;
          WebService String impexprtType;
          WebService String plantid;
          WebService String scheduler;
          Webservice String assetid;
          Webservice date   transdate;
          Webservice String message;
          Webservice String filtersts;
          WebService List<USVC_Material> material;
          
      
       public USVC_T4C(String location, String carrier_code,  String shipper_code, String tankage, String line, String transtype,
                       String impexprttype,
                       String plantid, String scheduler, String assetid, Date transdate, 
                       String message, String filtersts,  List<USVC_Material> material) {
           
            
               this.location = location;           
               this.carrier_code = carrier_code;
               this.shipper_code = shipper_code;
               this.tankage      = tankage;
               this.line         = line;
               this.transtype    = transtype;
               this.impexprttype = impexprttype;
                           
               this.plantid = plantid;
               this.scheduler = scheduler;
               this.assetid = assetid;
               this.transdate = transdate;
               this.message = message;
               this.filtersts = filtersts;
               this.material = material;

              
          }
   
   }

//   
// Definition of the USVC_Material Object
//   
    global class USVC_Material {
        WebService    String src_material_code;
        WebService    String src_material_name;
        WebService    String src_uom;
        WebService    String trgt_material_code;
        WebService    String trgt_material_name;
        WebService    String trgt_uom;
        
        
        public USVC_Material(String src_material_code, String src_material_name, String src_uom, 
                             String trgt_material_code, String trgt_material_name, String trgt_uom){
                                 
                 this.src_material_code = src_material_code;
                 this.src_material_name = src_material_name;
                 this.src_uom = src_uom;
                 this.trgt_material_code = trgt_material_code;
                 this.trgt_material_name = trgt_material_name;
                 this.trgt_uom  = trgt_uom;            
           }
  
    }       
     
    
    
   global class USVC_PIC {
          
          WebService String pi_tag;
          WebService String plantid;
          WebService String assetid;
          WebService String uom;

          WebService String meastype;
          Webservice String pi_message;
          
      
       public USVC_PIC(String pi_tag, String plantid,  String assetid, String uom,  String meastype, String pi_message) {
           
            
               this.pi_tag = pi_tag;   
               this.plantid = plantid;
               this.assetid = assetid;
               this.uom = uom;
               this.meastype = meastype;
               this.pi_message = pi_message;             
             
      
          }
   
   }  


    
   @isTest  static void testXRefWS80() { 
                                          
   /********************************************************************************** 
  * CALL AND TEST getUSVC_T4C .  
  ***********************************************************************************   
   */      
    USVC_XRefWS80.USVC_Material usvc_mtrl11 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl11.src_material_code = 'WTI';
       usvc_mtrl11.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material = new List<USVC_XRefWS80.USVC_Material>();
    source_material.add(usvc_mtrl11);
    
 //   USVC_XRefWS80.USVC_Material usvc_mtrl12 = new USVC_XRefWS80.USVC_Material();
 //      usvc_mtrl12.src_material_code = 'WTI';
 //      usvc_mtrl12.src_uom = 'BR';  
 //        source_material.add(usvc_mtrl12);
       
       
    String source_app = 'Transport4';
    String source_location = 'LA'; 
    String carrier_code = '';
    String shipper_code = '';
    String tankage = 'HUS';
    Date transdate = Date.valueOf('2019-08-29');
    String line = '';
    String transtype = 'T';
    String impexprttype = '';
 
     
 //  USVC_XRefWS80.USVC_T4C usvc_t4c2 = USVC_XRefWS80.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
 //                                                              transtype2, impexprttype2, transdate2, source_material);              
    USVC_XRefWS80.USVC_T4C usvc_t4c = USVC_XRefWS80.getUSVC_T4C(source_app, source_location, carrier_code, shipper_code, tankage, line, 
                                                               transtype,  impexprttype, transdate, source_material);
       
  
         System.Debug('Asset_Id1: ' + usvc_t4c.assetid);
         System.Debug('Plant ID1: ' + usvc_t4c.plantid);
         System.Debug('Message1: ' + usvc_t4c.message);  
         System.Debug('Filter Status1: ' + usvc_t4c.filtersts);  
       

   // 
   // Second Test Case For T4
   //         
       
    USVC_XRefWS80.USVC_Material usvc_mtrl2 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl2.src_material_code = '999';
       usvc_mtrl2.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material2 = new List<USVC_XRefWS80.USVC_Material>();
    source_material2.add(usvc_mtrl2);
    
    String source_app2 = 'Transport4';
    String source_location2 = 'XXXX'; 
    String carrier_code2 = '';
    String shipper_code2 = '';
    String tankage2 = ' SPM';
    String line2 = '';
    String transtype2 = '';
    Date transdate2 = Date.valueOf('2019-07-30');
    String impexprttype2 = '';
       
     
   USVC_XRefWS80.USVC_T4C usvc_t4c2 = USVC_XRefWS80.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
                                                               transtype2,  impexprttype2, transdate2, source_material2);    
   
       
       
         System.Debug('Asset_Id2: ' + usvc_t4c2.assetid);
         System.Debug('Plant ID2: ' + usvc_t4c2.plantid);
         System.Debug('Message2: ' + usvc_t4c2.message); 
         System.Debug('Filter Status2: ' + usvc_t4c2.filtersts);  
       
     //
     //  Third Test Care
     //    
 
     USVC_XRefWS80.USVC_Material usvc_mtrl3 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl3.src_material_code = '012';
       usvc_mtrl3.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material3 = new List<USVC_XRefWS80.USVC_Material>();
    source_material3.add(usvc_mtrl3);
    
    String source_app3 = 'Transport4';
    String source_location3 = 'LA'; 
    String carrier_code3 = '';
    String shipper_code3 = '';
    String tankage3 = 'HUS';
    Date transdate3 = Date.valueOf('2019-07-22');
    String line3 = 'CL8';
    String transtype3 = '';
    String impexprttype3 = '';
 
     
 //  USVC_XRefWS80.USVC_T4C usvc_t4c2 = USVC_XRefWS80.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
 //                                                              transtype2, impexprttype2, transdate2, source_material);              
        USVC_XRefWS80.USVC_T4C usvc_t4c3 = USVC_XRefWS80.getUSVC_T4C(source_app3, source_location3, carrier_code3, shipper_code3, tankage3, line3, 
                                                               transtype3,  impexprttype3, transdate3, source_material3);
       
  
         System.Debug('Asset_Id3: ' + usvc_t4c3.assetid);
         System.Debug('Plant ID3: ' + usvc_t4c3.plantid);
         System.Debug('Message3: ' + usvc_t4c3.message);  
         System.Debug('Filter Status3: ' + usvc_t4c3.filtersts);  
       
   //
     //  Fourth Test Care WIth Condition
     //    
 
     USVC_XRefWS80.USVC_Material usvc_mtrl4 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl4.src_material_code = 'X1';
       usvc_mtrl4.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material4 = new List<USVC_XRefWS80.USVC_Material>();
    source_material4.add(usvc_mtrl4);
    
    String source_app4 = 'Transport4';
    String source_location4 = 'LA'; 
    String carrier_code4 = '';
    String shipper_code4 = '';
    String tankage4 = 'SPM';
    Date transdate4 = Date.valueOf('2019-07-22');
    String line4 = '';
    String transtype4 = '';
    String impexprttype4 = '';
 
     
 //  USVC_XRefWS70.USVC_T4C usvc_t4c2 = USVC_XRefWS70.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
 //                                                              transtype2, impexprttype2, transdate2, source_material);              
  USVC_XRefWS80.USVC_T4C usvc_t4c4 = USVC_XRefWS80.getUSVC_T4C(source_app4, source_location4, carrier_code4, shipper_code4, tankage4, line4, 
                                                               transtype4,  impexprttype4, transdate4, source_material4);
       
  
         System.Debug('Asset_Id4: ' + usvc_t4c4.assetid);
         System.Debug('Plant ID4: ' + usvc_t4c4.plantid);
         System.Debug('Message4: ' + usvc_t4c4.message);        
         System.Debug('Filter Status4: ' + usvc_t4c4.filtersts);      
  
        
   //
     //  Fifth Test Care no overrides
     //    
 
     USVC_XRefWS80.USVC_Material usvc_mtrl5 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl5.src_material_code = 'X2';
       usvc_mtrl5.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material5 = new List<USVC_XRefWS80.USVC_Material>();
    source_material5.add(usvc_mtrl5);
    
    String source_app5 = 'Transport4';
    String source_location5 = 'LA'; 
    String carrier_code5 = '';
    String shipper_code5 = '';
    String tankage5 = 'HUS';
    Date transdate5 = Date.valueOf('2019-07-22');
    String line5 = '';
    String transtype5 = 'S';
    String impexprttype5 = '';
 
     
 //  USVC_XRefWS70.USVC_T4C usvc_t4c2 = USVC_XRefWS70.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
 //                                                              transtype2, impexprttype2, transdate2, source_material);              
  USVC_XRefWS80.USVC_T4C usvc_t4c5 = USVC_XRefWS80.getUSVC_T4C(source_app5, source_location5, carrier_code5, shipper_code5, tankage4, line4, 
                                                               transtype5,  impexprttype5, transdate5, source_material5);
       
  
         System.Debug('Asset_Id5: ' + usvc_t4c5.assetid);
         System.Debug('Plant ID5: ' + usvc_t4c5.plantid);
         System.Debug('Message5: ' + usvc_t4c5.message);        
            
         
   //
     //  Sixth Test Care no overrides
     //    
 
     USVC_XRefWS80.USVC_Material usvc_mtrl6 = new USVC_XRefWS80.USVC_Material();
       usvc_mtrl6.src_material_code = 'X3';
       usvc_mtrl6.src_uom = 'BR';
       
    List<USVC_XRefWS80.USVC_Material> source_material6 = new List<USVC_XRefWS80.USVC_Material>();
    source_material6.add(usvc_mtrl6);
    
    String source_app6 = 'Transport4';
    String source_location6 = 'ALLE'; 
    String carrier_code6 = '';
    String shipper_code6 = '';
    String tankage6 = 'SPM';
    Date transdate6 = Date.valueOf('2019-07-22');
    String line6 = 'CL8';
    String transtype6 = 'S';
    String impexprttype6 = '';
 
     
 //  USVC_XRefWS80.USVC_T4C usvc_t4c2 = USVC_XRefWS80.getUSVC_T4C(source_app2, source_location2, carrier_code2, shipper_code2, tankage2, line2, 
 //                                                              transtype2, impexprttype2, transdate2, source_material);              
  USVC_XRefWS80.USVC_T4C usvc_t4c6 = USVC_XRefWS80.getUSVC_T4C(source_app6, source_location6, carrier_code6, shipper_code6, tankage4, line4, 
                                                               transtype6,  impexprttype6, transdate6, source_material6);
       
  
         System.Debug('Asset_Id5: ' + usvc_t4c6.assetid);
         System.Debug('Plant ID5: ' + usvc_t4c6.plantid);
         System.Debug('Message5: ' + usvc_t4c6.message);        
            
        
       
           
       
       
       
       
 /********************************************************************************** 
  * CALL AND TEST getUSVC_PIC .  
  ***********************************************************************************   
   */     
       
         String pi_tag = 'TK005.VN';
         String plantid = '';
         String assetid = '';
         String meastype = '';
         

         USVC_XRefWS80.USVC_PIC usvc_pic = USVC_XRefWS80.getUSVC_PIC(pi_tag, plantid, assetid, meastype);
     
         System.Debug('PI Tag: ' + usvc_pic.pi_tag);
         System.Debug('Plant ID: ' + usvc_pic.plantid);
         System.Debug('Message: ' + usvc_pic.pi_message);
        
         pi_tag = 'TK005.VX';
         plantid = '';
         assetid = '';
         meastype = '';
         
// Second Conditions
  
         USVC_XRefWS80.USVC_PIC usvc_pic2 = USVC_XRefWS80.getUSVC_PIC(pi_tag, plantid, assetid, meastype);
            
         System.Debug('PI Tag: ' + usvc_pic2.pi_tag);
         System.Debug('Plant ID: ' + usvc_pic2.plantid);
         System.Debug('Message: ' + usvc_pic2.pi_message);
    }    
}