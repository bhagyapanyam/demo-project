/**
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:    Test class for Alert Expired GOR Test creation from Location_Trigger
 *  				and Location_TriggerHelper, also test if additional Alert are created when they SHOULD and tests
 *  				if no new Alert are created when they should NOT
 *  History:        Created on 9/26/2018   
 */

@isTest
public class Location_TriggerHelperTest {

	@isTest
	static void singleAlertTest() {

		String todaysDate = String.valueOf(Date.today());
		Date expiredGorTestDate = Date.today()-1100;

		Test.startTest();
		Location__c location = [SELECT Name FROM Location__c];
		location.GOR_Effective_Date__c = expiredGorTestDate;
		update location;
		location.recalculateFormulas();
		System.debug('Show me the date ' +location.GOR_Effective_Date__c);
		String nameOfAlert = location.Name + ' - ' + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST + ' - ' + todaysDate;
		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, Status__c
											, Production_Engineer__c
											, Type__c
											, Priority__c
											, Start_Date__c
											, Well_Location__c
											, Exemption__c
											, Description__c
										FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(nameOfAlert, alert.Name);
		System.assertEquals('Not Started',alert.Status__c);
		System.assertEquals('Expired GOR Test' ,alert.Type__c);
		System.assertEquals('Medium', alert.Priority__c);
		System.assertEquals(location.Id, alert.Well_Location__c);
	}

	@isTest
	static void tryToCreateMultipleAlertsWhenNotStartedStatusTest() {

		Date todaysDate = Date.today();
		Date expiredGorTestDate = Date.today()-1100;

		Test.startTest();
		Location__c location = [SELECT Name
										, Functional_Location_Category__c
										, Expired_GOR_Test__c
										, Well_Type__c
										, Current_Well_Status__c
								FROM Location__c];

		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;
		Integer singleAlertCreated = [SELECT Count() FROM HOG_Vent_Gas_Alert__c];
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = todaysDate;
		location.recalculateFormulas();
		update location;
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;

		Integer numberOfAlerts = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(1, numberOfAlerts, 'Anothe alert should not be created');
	}

	@isTest
	static void tryToCreateMultipleAlertWhenAcknowledgeStatusTest() {
		Date todaysDate = Date.today();
		Date expiredGorTestDate = Date.today()-1100;

		Test.startTest();
		Location__c location = [SELECT Name
									, Functional_Location_Category__c
									, Expired_GOR_Test__c
									, Well_Type__c
									, Current_Well_Status__c
							FROM Location__c];

		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;
		Integer singleAlertCreated = [SELECT Count() FROM HOG_Vent_Gas_Alert__c];

		HOG_Vent_Gas_Alert__c alert = [SELECT Status__c FROM HOG_Vent_Gas_Alert__c];
		alert.Status__c = 'Acknowledge';
		update alert;
		System.assertEquals('Acknowledge', alert.Status__c, 'Status should be changed');


		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = todaysDate;
		location.recalculateFormulas();
		update location;
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;

		Integer numberOfAlerts = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(1, singleAlertCreated, 'One alert should be created');
		System.assertEquals(2, numberOfAlerts, 'New alert should be created');
	}

	@isTest
	static void tryToCreateMultipleAlertWhenInProgressStatusTest() {
		Date todaysDate = Date.today();
		Date expiredGorTestDate = Date.today()-1100;

		Test.startTest();
		Location__c location = [SELECT Name
									, Functional_Location_Category__c
									, Expired_GOR_Test__c
									, Well_Type__c
									, Current_Well_Status__c
							FROM Location__c];

		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;
		Integer singleAlertCreated = [SELECT Count() FROM HOG_Vent_Gas_Alert__c];

		HOG_Vent_Gas_Alert__c alert = [SELECT Status__c FROM HOG_Vent_Gas_Alert__c];
		alert.Status__c = 'In Progress';
		update alert;
		System.assertEquals('In Progress', alert.Status__c, 'Status should be changed');


		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = todaysDate;
		location.recalculateFormulas();
		update location;
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;

		Integer numberOfAlerts = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(1, singleAlertCreated, 'One alert should be created');
		System.assertEquals(1, numberOfAlerts, 'No new alert should be created');
	}

	@isTest
	static void tryToCreateMultipleAlertWhenCompletedStatusTest() {
		Date todaysDate = Date.today();
		Date expiredGorTestDate = Date.today()-1100;

		User usr = HOG_TestDataFactory.createUser('Hebka', 'Plska', 'Mys');
		User usr2 = HOG_TestDataFactory.createUser('Mr', 'Mychal', 'TheMys');


		Test.startTest();
		Location__c location = [SELECT Name
										, Functional_Location_Category__c
										, Expired_GOR_Test__c
										, Well_Type__c
										, Current_Well_Status__c
								FROM Location__c];

		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;
		Integer singleAlertCreated = [SELECT Count() FROM HOG_Vent_Gas_Alert__c];

		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, CreatedById
											, AMU__c
											, Description__c
											, End_Date__c
											, My_Alert__c
											, Priority__c
											, Production_Engineer__c
											, Production_Engineer__r.Name
											, Start_Date__c
											, Status__c
											, Type__c
											, Well_Location__c
											, Well_Location__r.Name
											, Well_Location__r.Operating_Field_AMU__c
											, Well_Location__r.Operating_Field_AMU__r.Name
										FROM HOG_Vent_Gas_Alert__c];
		alert.Production_Engineer__c = usr.Id;
		update alert;

		HOG_Vent_Gas_Alert_Task__c newTask = HOG_VentGas_TestData.createTask(alert, usr2.Id, true);
		newTask.Status__c = 'Completed';
		update newTask;
		System.assertEquals('Completed', newTask.Status__c, 'Status should be updated');

		alert.Status__c = 'Completed';
		update alert;
		System.assertEquals('Completed', alert.Status__c, 'Status should be updated');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = todaysDate;
		location.recalculateFormulas();
		update location;
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;

		Integer numberOfAlerts = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(1, singleAlertCreated, 'One alert should be created');
		System.assertEquals(2, numberOfAlerts, 'New alert should be created');
	}

	@isTest
	static void tryToCreateMultipleAlertWhenExemptionStatusTest() {
		Date todaysDate = Date.today();
		Date expiredGorTestDate = Date.today()-1100;
		Date expiredExemptionDate = Date.today()+365;

		User usr = HOG_TestDataFactory.createUser('Hebka', 'Plska', 'Mys');
		User usr2 = HOG_TestDataFactory.createUser('Mr', 'Mychal', 'TheMys');


		Test.startTest();
		Location__c location = [SELECT Name
										, Functional_Location_Category__c
										, Expired_GOR_Test__c
										, Well_Type__c
										, Current_Well_Status__c
								FROM Location__c];

		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;
		Integer singleAlertCreated = [SELECT Count() FROM HOG_Vent_Gas_Alert__c];

		HOG_Vent_Gas_Alert__c alert = [SELECT Id
											, Name
											, CreatedById
											, AMU__c
											, Description__c
											, End_Date__c
											, My_Alert__c
											, Priority__c
											, Production_Engineer__c
											, Production_Engineer__r.Name
											, Start_Date__c
											, Status__c
											, Type__c
											, Well_Location__c
											, Well_Location__r.Name
											, Well_Location__r.Operating_Field_AMU__c
											, Well_Location__r.Operating_Field_AMU__r.Name
									FROM HOG_Vent_Gas_Alert__c];

		alert.Production_Engineer__c = usr.Id;
		update alert;

		HOG_Vent_Gas_Alert_Exemption_Request__c exemption = new HOG_Vent_Gas_Alert_Exemption_Request__c();
		exemption.Vent_Gas_Alert__c = alert.Id;
		exemption.Well_Location__c = location.Id;
		exemption.Operations_Engineer__c = usr2.Id;
		exemption.Expiration_Date__c = expiredExemptionDate;
		exemption.Reason__c = 'Approve pls';
		insert exemption;

		HOG_Vent_Gas_Alert_Exemption_Request__c exemptionForStatusUpdate = [SELECT Status__c
																			FROM HOG_Vent_Gas_Alert_Exemption_Request__c];
		exemption.Status__c = 'Approved';
		update exemption;

		HOG_Vent_Gas_Alert__c alertQueryForAssert = [SELECT Status__c, Exemption__c FROM HOG_Vent_Gas_Alert__c];
		System.assertEquals(true, alertQueryForAssert.Exemption__c, 'Alert field Exemption checkbox haven\t been set to True');
		System.assertEquals(HOG_VentGas_Utilities.ALERT_STATUS_EXEMPTION, alertQueryForAssert.Status__c, 'Alert status haven\t changed to Exemption status');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = todaysDate;
		location.recalculateFormulas();
		update location;
		System.assertEquals(1, singleAlertCreated, 'One alert should be created');

		HOG_TriggerRecursionPreventController.flag = true;
		location.GOR_Effective_Date__c = expiredGorTestDate;
		location.recalculateFormulas();
		update location;

		Integer numberOfAlerts = [SELECT COUNT() FROM HOG_Vent_Gas_Alert__c];
		Test.stopTest();

		System.assertEquals(1, singleAlertCreated, 'One alert should be created');
		System.assertEquals(1, numberOfAlerts, 'New alert was created and should not be');
	}



	@testSetup
	static void setData() {
		HOG_Vent_Gas_Alert_Configuration__c customSettings= new HOG_Vent_Gas_Alert_Configuration__c();
		insert customSettings;
		HOG_VentGas_TestData.createLocationForAlert(true);

	}
}