@isTest (SeeAllData = true)
private class ATSFormatGmtDateTimeToLocalTimeTest{
    static testMethod void myUnitTest() {
         
        //Date currentDate = Date.Today();        
        DateTime compareDateTime = DateTime.now();                      
        
        String convertedDateTime;
        ATSFormatGmtDateTimeToLocalTime testConversion = new ATSFormatGmtDateTimeToLocalTime();
        System.assertNotEquals(testConversion, null);
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            User testUser = ATSTestData.CreateUser();                    
            
            ATS_Parent_Opportunity__c  tender1 =  ATSTestData.createTender('Test Tender', compareDateTime, testUser);          
            
            insert tender1;
            
            //Query for tender1 again
            tender1 = [select id,Bid_Due_Date_Time__c from ATS_Parent_Opportunity__c where id=:tender1.id];
            
            testConversion.date_time = tender1.Bid_Due_Date_Time__c;
            
            //test the trigger to confirm date field has been converted. was inputted into date fields         
            convertedDateTime = testConversion.getFormattedDatetime();
                           
            
            System.assertNotEquals(compareDateTime, null);
            System.assertNotEquals(convertedDateTime, null);
            System.assertNotEquals(convertedDateTime, '');
            System.assertEquals(compareDateTime.format(), convertedDateTime);        
            
            testConversion.convertedDateTime = 'EEE MMM d kk:mm:ss z yyyy';
            testConversion.getFormattedDatetime();
                  
            //test to confirm if empty date returns empty string
            testConversion.date_time = null;
            convertedDateTime = testConversion.getFormattedDatetime();
            System.AssertEquals(convertedDateTime, '');
        }        
        
    }
}