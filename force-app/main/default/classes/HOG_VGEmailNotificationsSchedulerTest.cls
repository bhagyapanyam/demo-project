/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for HOG_VGEmailNotificationsScheduler
History:        jschn 17/10/2018 - Created.
*************************************************************************************************/
@IsTest
public class HOG_VGEmailNotificationsSchedulerTest {

	@IsTest
	public static void scheduleWithDefaultCron() {

		Test.startTest();
		Id jobId = HOG_VGEmailNotificationsScheduler.schedule();
		Test.stopTest();

		CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
							FROM CronTrigger
							WHERE Id = :jobId];
		System.assertEquals(HOG_VGEmailNotificationsScheduler.DEFAULT_CRON_STRING, job.CronExpression);
		System.assertEquals(HOG_VGEmailNotificationsScheduler.JOB_NAME, job.CronJobDetail.Name);
	}

	@IsTest
	public static void scheduleWithCustomCron() {
		String customCronString = '0 0 9 ? * MON';

		Test.startTest();
		Id jobId = HOG_VGEmailNotificationsScheduler.schedule(customCronString);
		Test.stopTest();

		CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
		FROM CronTrigger
		WHERE Id = :jobId];
		System.assertEquals(customCronString, job.CronExpression);
		System.assertEquals(HOG_VGEmailNotificationsScheduler.JOB_NAME, job.CronJobDetail.Name);
	}

}