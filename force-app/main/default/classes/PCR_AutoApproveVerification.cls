global class PCR_AutoApproveVerification{
    @InvocableMethod
    public static void submitApproval(List<ID> changeIds) {
        
        for(ID changeid : changeIds){
            // Create an approval request
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(changeid);
            req1.setProcessDefinitionNameOrId('Bypass_Assessor_Request');
            req1.setSkipEntryCriteria(true);
            
            Approval.ProcessResult result = Approval.process(req1);
            //List<Id> newWorkItemIds = result.getNewWorkitemIds();
            
            // Submit the request for approval
            //Approval.ProcessWorkitemRequest v = new Approval.ProcessWorkitemRequest();
            //v.setComments('Auto approving');
            //v.setAction('Approve');
            //v.setWorkitemId(newWorkItemIds.get(0));
            
            //Approval.ProcessResult r =  Approval.process(v);
        }
    }
}