public class FM_FacilitySummaryCapacityController
{
	// ********************************************************************************************** //
	// ** THIS CONTROLLER HANDLES LOGIC RELATED TO MODIFYING CAPACITY NUMBERS FOR FLUID FACILITIES ** //
	// ********************************************************************************************** //

	public List<SelectOption> lFacilityList {get; private set;}
	public Facility_Capacity__c oFacilityCapacity {get; set;}

	public String sSelectedFacility {get; set;}
	public String sLastModifiedDate {get; private set;}
	public Boolean bIsFacility {get; private set;}

	private String sSelectionPrefix;
	private String sFacilityPrefix;
	private String sLocationPrefix;

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_FacilitySummaryCapacityController()
	{
		InitializeData();
	}

	/////////////////////////////////////
	// * INITIALIZE CONTROLLER DATA * ///
	/////////////////////////////////////

	private void InitializeData()
	{
		BuildFacilityList();
		this.sSelectedFacility = (ApexPages.currentPage().getParameters().get('id') != '') ? ApexPages.currentPage().getParameters().get('id') : '';

		CreatePrefixData();
		GetFacilityCapacityRecord();
	}

	/////////////////////////////////////////////////
	// * GENERATE FULL LIST OF FLUID FACILITIES * ///
	/////////////////////////////////////////////////

	private void BuildFacilityList()
	{
		List<SelectOption> lFullFacilityList = new List<SelectOption>();
		String sFacilityName;

		List<Facility__c> lFacilities;
		List<Location__c> lLocations;

		////////////////////////////////////////////////
		// ** SEARCH FOR STANDARD FLUID FACILITIES ** //
		////////////////////////////////////////////////

		lFacilities = [SELECT Id, Name, Facility_Name_For_Fluid__c FROM Facility__c WHERE Fluid_Facility_Ind__c = true ORDER BY Facility_Name_For_Fluid__c, Name];

		for(Facility__c oOneFacility : lFacilities)
		{
			sFacilityName = (oOneFacility.Facility_Name_For_Fluid__c == '' || oOneFacility.Facility_Name_For_Fluid__c == null) ? oOneFacility.Name : oOneFacility.Facility_Name_For_Fluid__c;
			lFullFacilityList.add(new SelectOption(oOneFacility.Id, sFacilityName));
		}

		///////////////////////////////////////////////////////////////////////
		// ** SEARCH FOR "OTHER" FACILITIES (NON STANDARD WELL LOCATIONS) ** //
		///////////////////////////////////////////////////////////////////////

		lLocations = [SELECT Id, Name, Location_Name_For_Fluid__c FROM Location__c WHERE Fluid_Location_Ind__c = true ORDER BY Location_Name_For_Fluid__c, Name];

		for(Location__c oOneLocation : lLocations)
		{
			sFacilityName = (oOneLocation.Location_Name_For_Fluid__c == '' || oOneLocation.Location_Name_For_Fluid__c == null) ? oOneLocation.Name : oOneLocation.Location_Name_For_Fluid__c;
			lFullFacilityList.add(new SelectOption(oOneLocation.Id, sFacilityName));
		}

		// Sort AND Assign Final List
		this.lFacilityList = FM_Utilities.SortSelectOptions(lFullFacilityList, FM_Utilities.SortSelectOptionBy.Label);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// * GET FACILITY CAPACITY RECORD (IF IT EXISTS) OTHERWISE CREATE A BLANK OBJECT * ///
	//////////////////////////////////////////////////////////////////////////////////////

	private void CreatePrefixData()
	{
		Schema.DescribeSObjectResult oResult;
		this.sSelectionPrefix = (this.sSelectedFacility != '' && this.sSelectedFacility != null) ? this.sSelectedFacility.left(3) : '';

		if(this.sLocationPrefix == null || this.sFacilityPrefix == null)
		{
			oResult = Facility__c.sObjectType.getDescribe();
			this.sFacilityPrefix = oResult.getKeyPrefix();

			oResult = Location__c.sObjectType.getDescribe();
			this.sLocationPrefix = oResult.getKeyPrefix();
		}

		this.bIsFacility = (this.sSelectionPrefix != '' && this.sSelectionPrefix == this.sFacilityPrefix) ? true : false;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// * GET FACILITY CAPACITY RECORD (IF IT EXISTS) OTHERWISE CREATE A BLANK OBJECT * ///
	//////////////////////////////////////////////////////////////////////////////////////

	private void GetFacilityCapacityRecord()
	{
		List<Facility_Capacity__c> lFacilityCapacities = new List<Facility_Capacity__c>();

		if(this.sSelectedFacility != null && this.sSelectedFacility != '' && this.sSelectionPrefix != '')
		{
			lFacilityCapacities = (this.bIsFacility == true) ? [SELECT Id, Facility__c, Facility_Name__c, Facility_Type__c, Total_Capacity_Oil__c, Total_Capacity_Water__c, Unique_Facility__c, LastModifiedDate, LastModifiedBy.Id, LastModifiedBy.Name FROM Facility_Capacity__c WHERE Facility__c = :this.sSelectedFacility LIMIT 1] : [SELECT Id, Well_Location__c, Facility_Name__c, Facility_Type__c, Total_Capacity_Oil__c, Total_Capacity_Water__c, Unique_Facility__c, LastModifiedDate, LastModifiedBy.Id, LastModifiedBy.Name FROM Facility_Capacity__c WHERE Well_Location__c = :this.sSelectedFacility LIMIT 1];
		}

		this.oFacilityCapacity = (lFacilityCapacities.size() == 1) ? lFacilityCapacities[0] : new Facility_Capacity__c();
		this.sLastModifiedDate = (this.oFacilityCapacity.Id != null) ? this.oFacilityCapacity.LastModifiedDate.format('MMMMM dd yyyy') : '';

		this.oFacilityCapacity.Total_Capacity_Oil__c = (this.oFacilityCapacity.Total_Capacity_Oil__c == null) ? 0 : this.oFacilityCapacity.Total_Capacity_Oil__c;
		this.oFacilityCapacity.Total_Capacity_Water__c = (this.oFacilityCapacity.Total_Capacity_Water__c == null) ? 0 : this.oFacilityCapacity.Total_Capacity_Water__c;
	}

	//////////////////////////////////////////////
	// * UPDATE THE SELECTED FACILITY RECORD * ///
	//////////////////////////////////////////////

	public PageReference UpdateSelectedFacility()
	{
		CreatePrefixData();
		GetFacilityCapacityRecord();

		return null;
	}

	////////////////////////////////////////
	// * SAVE BUTTON --> UPSERT RECORD * ///
	////////////////////////////////////////

	public PageReference SaveRecordChanges()
	{
		try
		{
			// New Record --> Set Values
			if(this.oFacilityCapacity.Id == null || this.oFacilityCapacity.Unique_Facility__c == '')
			{
				this.oFacilityCapacity.Unique_Facility__c = this.sSelectedFacility;
				this.oFacilityCapacity.Facility__c = (this.sSelectionPrefix == this.sFacilityPrefix) ? this.sSelectedFacility : null;
				this.oFacilityCapacity.Well_Location__c = (this.sSelectionPrefix == this.sLocationPrefix) ? this.sSelectedFacility : null;
			}

			upsert oFacilityCapacity;
			GetFacilityCapacityRecord();

			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Facility Capacity saved'));
			return null;
		}
		catch(Exception ex)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error updating Facility Capacity: ' + ex.getMessage()));
			return null;
		}
	}

	/////////////////////////////////////////////////////////
	// * CLOSE BUTTON --> RE-DIRECT TO FACILITY SUMMARY * ///
	/////////////////////////////////////////////////////////

	public PageReference CloseButton()
	{
        PageReference oPage = Page.FM_FacilitySummary;
        oPage.setRedirect(true);

        return oPage;
	}
}