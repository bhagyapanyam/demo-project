/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureEPD
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureEPDTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        EPD_FormStructureEPD result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        EPD_FormStructureEPD result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(epd.Company__c, result.companyId);
        System.assertEquals(epd.Data_Collected_By__c, result.dataCollectedById);
        System.assertEquals(epd.Equipment_Engine__c, result.equipmentEngineId);
        System.assertEquals(epd.Work_Order__c, result.workOrderId);
        System.assertEquals(epd.Work_Order_Activity__c, result.workOrderActivityId);
        System.assertEquals(epd.Vendor_Work_Order_Number__c, result.vendorWorkOrderNumber);
        System.assertEquals(epd.Operation_Group__c, result.operationGroup);
        System.assertEquals(epd.Planner_Group__c, result.plannerGroup);
        System.assertEquals(epd.LSD__c, result.LSD);
        System.assertEquals(epd.Data_Collected__c, result.dataCollected);
        System.assertEquals(epd.Hours__c, result.hours);
        System.assertEquals(epd.COC_Oil_Pressure__c, result.COCOilPressure);
        System.assertEquals(epd.COC_Oil_Temperature__c, result.COCOilTemperature);
        System.assertEquals(epd.EI_Ignition_Timing__c, result.EIIgnitionTiming);
        System.assertEquals(epd.EI_RPM__c, result.EIRPM);
        System.assertEquals(epd.EJW_Temperature_IN__c, result.EJWTemperatureIN);
        System.assertEquals(epd.EJW_Temperature_OUT__c, result.EJWTemperatureOUT);
        System.assertEquals(epd.EOC_Crankcase_Pressure__c, result.EOCCrankcasePressure);
        System.assertEquals(epd.EOC_Oil_Pressure__c, result.EOCOilPressure);
        System.assertEquals(epd.EOC_Oil_Temperature__c, result.EOCOilTemperature);
        System.assertEquals(epd.Notes__c, result.notes);
        System.assertEquals(epd.Submitted__c, result.submitted);
        System.assertEquals(epd.Required_Exhaust_Measurements__c, result.requiredExhaustMeasurements);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureEPD result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(epd.Company__c, result.companyId);
        System.assertEquals(epd.Data_Collected_By__c, result.dataCollectedById);
        System.assertEquals(epd.Equipment_Engine__c, result.equipmentEngineId);
        System.assertEquals(epd.Work_Order__c, result.workOrderId);
        System.assertEquals(epd.Work_Order_Activity__c, result.workOrderActivityId);
        System.assertEquals(epd.Vendor_Work_Order_Number__c, result.vendorWorkOrderNumber);
        System.assertEquals(epd.Operation_Group__c, result.operationGroup);
        System.assertEquals(epd.Planner_Group__c, result.plannerGroup);
        System.assertEquals(epd.LSD__c, result.LSD);
        System.assertEquals(epd.Data_Collected__c, result.dataCollected);
        System.assertEquals(epd.Hours__c, result.hours);
        System.assertEquals(epd.COC_Oil_Pressure__c, result.COCOilPressure);
        System.assertEquals(epd.COC_Oil_Temperature__c, result.COCOilTemperature);
        System.assertEquals(epd.EI_Ignition_Timing__c, result.EIIgnitionTiming);
        System.assertEquals(epd.EI_RPM__c, result.EIRPM);
        System.assertEquals(epd.EJW_Temperature_IN__c, result.EJWTemperatureIN);
        System.assertEquals(epd.EJW_Temperature_OUT__c, result.EJWTemperatureOUT);
        System.assertEquals(epd.EOC_Crankcase_Pressure__c, result.EOCCrankcasePressure);
        System.assertEquals(epd.EOC_Oil_Pressure__c, result.EOCOilPressure);
        System.assertEquals(epd.EOC_Oil_Temperature__c, result.EOCOilTemperature);
        System.assertEquals(epd.Notes__c, result.notes);
        System.assertEquals(epd.Submitted__c, result.submitted);
        System.assertEquals(epd.Required_Exhaust_Measurements__c, result.requiredExhaustMeasurements);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        EPD_Engine_Performance_Data__c result;

        Test.startTest();
        try {
            result = (EPD_Engine_Performance_Data__c) new EPD_FormStructureEPD().setRecord(epd).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(epd.Company__c, result.Company__c);
        System.assertEquals(epd.Data_Collected_By__c, result.Data_Collected_By__c);
        System.assertEquals(epd.Equipment_Engine__c, result.Equipment_Engine__c);
        System.assertEquals(epd.Work_Order__c, result.Work_Order__c);
        System.assertEquals(epd.Work_Order_Activity__c, result.Work_Order_Activity__c);
        System.assertEquals(epd.Vendor_Work_Order_Number__c, result.Vendor_Work_Order_Number__c);
        System.assertEquals(epd.Operation_Group__c, result.Operation_Group__c);
        System.assertEquals(epd.Planner_Group__c, result.Planner_Group__c);
        System.assertEquals(epd.LSD__c, result.LSD__c);
        System.assertEquals(epd.Data_Collected__c, result.Data_Collected__c);
        System.assertEquals(epd.Hours__c, result.Hours__c);
        System.assertEquals(epd.COC_Oil_Pressure__c, result.COC_Oil_Pressure__c);
        System.assertEquals(epd.COC_Oil_Temperature__c, result.COC_Oil_Temperature__c);
        System.assertEquals(epd.EI_Ignition_Timing__c, result.EI_Ignition_Timing__c);
        System.assertEquals(epd.EI_RPM__c, result.EI_RPM__c);
        System.assertEquals(epd.EJW_Temperature_IN__c, result.EJW_Temperature_IN__c);
        System.assertEquals(epd.EJW_Temperature_OUT__c, result.EJW_Temperature_OUT__c);
        System.assertEquals(epd.EOC_Crankcase_Pressure__c, result.EOC_Crankcase_Pressure__c);
        System.assertEquals(epd.EOC_Oil_Pressure__c, result.EOC_Oil_Pressure__c);
        System.assertEquals(epd.EOC_Oil_Temperature__c, result.EOC_Oil_Temperature__c);
        System.assertEquals(epd.Notes__c, result.Notes__c);
        System.assertEquals(epd.Submitted__c, result.Submitted__c);
        System.assertEquals(epd.Required_Exhaust_Measurements__c, result.Required_Exhaust_Measurements__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine_Performance_Data__c result;

        Test.startTest();
        try {
            result = (EPD_Engine_Performance_Data__c) new EPD_FormStructureEPD().setRecord(epd).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(epd.Company__c, result.Company__c);
        System.assertEquals(epd.Data_Collected_By__c, result.Data_Collected_By__c);
        System.assertEquals(epd.Equipment_Engine__c, result.Equipment_Engine__c);
        System.assertEquals(epd.Work_Order__c, result.Work_Order__c);
        System.assertEquals(epd.Work_Order_Activity__c, result.Work_Order_Activity__c);
        System.assertEquals(epd.Vendor_Work_Order_Number__c, result.Vendor_Work_Order_Number__c);
        System.assertEquals(epd.Operation_Group__c, result.Operation_Group__c);
        System.assertEquals(epd.Planner_Group__c, result.Planner_Group__c);
        System.assertEquals(epd.LSD__c, result.LSD__c);
        System.assertEquals(epd.Data_Collected__c, result.Data_Collected__c);
        System.assertEquals(epd.Hours__c, result.Hours__c);
        System.assertEquals(epd.COC_Oil_Pressure__c, result.COC_Oil_Pressure__c);
        System.assertEquals(epd.COC_Oil_Temperature__c, result.COC_Oil_Temperature__c);
        System.assertEquals(epd.EI_Ignition_Timing__c, result.EI_Ignition_Timing__c);
        System.assertEquals(epd.EI_RPM__c, result.EI_RPM__c);
        System.assertEquals(epd.EJW_Temperature_IN__c, result.EJW_Temperature_IN__c);
        System.assertEquals(epd.EJW_Temperature_OUT__c, result.EJW_Temperature_OUT__c);
        System.assertEquals(epd.EOC_Crankcase_Pressure__c, result.EOC_Crankcase_Pressure__c);
        System.assertEquals(epd.EOC_Oil_Pressure__c, result.EOC_Oil_Pressure__c);
        System.assertEquals(epd.EOC_Oil_Temperature__c, result.EOC_Oil_Temperature__c);
        System.assertEquals(epd.Notes__c, result.Notes__c);
        System.assertEquals(epd.Submitted__c, result.Submitted__c);
        System.assertEquals(epd.Required_Exhaust_Measurements__c, result.Required_Exhaust_Measurements__c);
    }

    @IsTest
    static void getThresholdWarnings_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)).getThresholdWarnings(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getThresholdWarnings_noWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1500,
                Min_RPM__c = 1000
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals('', result);
    }

    @IsTest
    static void getThresholdWarnings_withWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1100,
                Min_RPM__c = 800
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
    }

    @IsTest
    static void testClassicGettersAndSetters_emptyValues() {
        EPD_FormStructureEPD structure = new EPD_FormStructureEPD();
        structure.hoursClassic = '';
        structure.COCOilPressureClassic = '';
        structure.COCOilTemperatureClassic = '';
        structure.EIIgnitionTimingClassic = '';
        structure.EIRPMClassic = '';
        structure.EJWTemperatureINClassic = '';
        structure.EJWTemperatureOUTClassic = '';
        structure.EOCCrankcasePressureClassic = '';
        structure.EOCOilPressureClassic = '';
        structure.EOCOilTemperatureClassic = '';

        System.assertEquals(null, structure.hoursClassic);
        System.assertEquals(null, structure.COCOilPressureClassic);
        System.assertEquals(null, structure.COCOilTemperatureClassic);
        System.assertEquals(null, structure.EIIgnitionTimingClassic);
        System.assertEquals(null, structure.EIRPMClassic);
        System.assertEquals(null, structure.EJWTemperatureINClassic);
        System.assertEquals(null, structure.EJWTemperatureOUTClassic);
        System.assertEquals(null, structure.EOCCrankcasePressureClassic);
        System.assertEquals(null, structure.EOCOilPressureClassic);
        System.assertEquals(null, structure.EOCOilTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_properValues() {
        EPD_FormStructureEPD structure = new EPD_FormStructureEPD();
        structure.hoursClassic = '1';
        structure.COCOilPressureClassic = '1';
        structure.COCOilTemperatureClassic = '1';
        structure.EIIgnitionTimingClassic = '1';
        structure.EIRPMClassic = '1';
        structure.EJWTemperatureINClassic = '1';
        structure.EJWTemperatureOUTClassic = '1';
        structure.EOCCrankcasePressureClassic = '1';
        structure.EOCOilPressureClassic = '1';
        structure.EOCOilTemperatureClassic = '1';

        System.assertEquals('1', structure.hoursClassic);
        System.assertEquals('1', structure.COCOilPressureClassic);
        System.assertEquals('1', structure.COCOilTemperatureClassic);
        System.assertEquals('1', structure.EIIgnitionTimingClassic);
        System.assertEquals('1', structure.EIRPMClassic);
        System.assertEquals('1', structure.EJWTemperatureINClassic);
        System.assertEquals('1', structure.EJWTemperatureOUTClassic);
        System.assertEquals('1', structure.EOCCrankcasePressureClassic);
        System.assertEquals('1', structure.EOCOilPressureClassic);
        System.assertEquals('1', structure.EOCOilTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_wrongValues() {
        EPD_FormStructureEPD structure = new EPD_FormStructureEPD();

        Boolean hoursClassicFail = false;
        try {
            structure.hoursClassic = 'abc';
        } catch(Exception ex) {
            hoursClassicFail = true;
        }

        Boolean COCOilPressureClassicFail = false;
        try {
            structure.COCOilPressureClassic = 'abc';
        } catch(Exception ex) {
            COCOilPressureClassicFail = true;
        }

        Boolean COCOilTemperatureClassicFail = false;
        try {
            structure.COCOilTemperatureClassic = 'abc';
        } catch(Exception ex) {
            COCOilTemperatureClassicFail = true;
        }

        Boolean EIIgnitionTimingClassicFail = false;
        try {
            structure.EIIgnitionTimingClassic = 'abc';
        } catch(Exception ex) {
            EIIgnitionTimingClassicFail = true;
        }

        Boolean EIRPMClassicFail = false;
        try {
            structure.EIRPMClassic = 'abc';
        } catch(Exception ex) {
            EIRPMClassicFail = true;
        }

        Boolean EJWTemperatureINClassicFail = false;
        try {
            structure.EJWTemperatureINClassic = 'abc';
        } catch(Exception ex) {
            EJWTemperatureINClassicFail = true;
        }

        Boolean EJWTemperatureOUTClassicFail = false;
        try {
            structure.EJWTemperatureOUTClassic = 'abc';
        } catch(Exception ex) {
            EJWTemperatureOUTClassicFail = true;
        }

        Boolean EOCCrankcasePressureClassicFail = false;
        try {
            structure.EOCCrankcasePressureClassic = 'abc';
        } catch(Exception ex) {
            EOCCrankcasePressureClassicFail = true;
        }

        Boolean EOCOilPressureClassicFail = false;
        try {
            structure.EOCOilPressureClassic = 'abc';
        } catch(Exception ex) {
            EOCOilPressureClassicFail = true;
        }

        Boolean EOCOilTemperatureClassicFail = false;
        try {
            structure.EOCOilTemperatureClassic = 'abc';
        } catch(Exception ex) {
            EOCOilTemperatureClassicFail = true;
        }

        System.assertEquals(true, hoursClassicFail);
        System.assertEquals(true, COCOilPressureClassicFail);
        System.assertEquals(true, COCOilTemperatureClassicFail);
        System.assertEquals(true, EIIgnitionTimingClassicFail);
        System.assertEquals(true, EIRPMClassicFail);
        System.assertEquals(true, EJWTemperatureINClassicFail);
        System.assertEquals(true, EJWTemperatureOUTClassicFail);
        System.assertEquals(true, EOCCrankcasePressureClassicFail);
        System.assertEquals(true, EOCOilPressureClassicFail);
        System.assertEquals(true, EOCOilTemperatureClassicFail);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureEPD structure = new EPD_FormStructureEPD();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}