/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructurePart
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructurePartTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Part_Replacement__c partReplacement;
        EPD_FormStructurePart result;

        Test.startTest();
        try {
            result = (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(partReplacement);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Part_Replacement__c partReplacement = new EPD_Part_Replacement__c();
        EPD_FormStructurePart result;

        Test.startTest();
        try {
            result = (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(partReplacement);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(partReplacement.Name, result.partName);
        System.assertEquals(partReplacement.Replaced__c, result.replaced);
        System.assertEquals(partReplacement.Inspected__c, result.inspected);
        System.assertEquals(partReplacement.Comments__c, result.comments);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Part_Replacement__c partReplacement = (EPD_Part_Replacement__c) HOG_SObjectFactory.createSObject(
                new EPD_Part_Replacement__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructurePart result;

        Test.startTest();
        try {
            result = (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(partReplacement);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(partReplacement.Name, result.partName);
        System.assertEquals(partReplacement.Replaced__c, result.replaced);
        System.assertEquals(partReplacement.Inspected__c, result.inspected);
        System.assertEquals(partReplacement.Comments__c, result.comments);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Part_Replacement__c partReplacement = new EPD_Part_Replacement__c();
        EPD_Part_Replacement__c result;

        Test.startTest();
        try {
            result = (EPD_Part_Replacement__c) new EPD_FormStructurePart().setRecord(partReplacement).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(partReplacement.Name, result.Name);
        System.assertEquals(partReplacement.Replaced__c, result.Replaced__c);
        System.assertEquals(partReplacement.Inspected__c, result.Inspected__c);
        System.assertEquals(partReplacement.Comments__c, result.Comments__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Part_Replacement__c partReplacement = (EPD_Part_Replacement__c) HOG_SObjectFactory.createSObject(
                new EPD_Part_Replacement__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Part_Replacement__c result;

        Test.startTest();
        try {
            result = (EPD_Part_Replacement__c) new EPD_FormStructurePart().setRecord(partReplacement).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(partReplacement.Name, result.Name);
        System.assertEquals(partReplacement.Replaced__c, result.Replaced__c);
        System.assertEquals(partReplacement.Inspected__c, result.Inspected__c);
        System.assertEquals(partReplacement.Comments__c, result.Comments__c);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructurePart structure = new EPD_FormStructurePart();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}