/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_BlockInformationTrigger
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
@IsTest
private class EPD_BlockInformationTriggerTest {

    @IsTest
    static void delete_allowed() {
        EPD_Block_Information__c blockInfo = EPD_TestData.createBlockInformation();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete blockInfo;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void delete_restricted() {
        EPD_Block_Information__c blockInfo = EPD_TestData.createBlockInformation();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete blockInfo;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}