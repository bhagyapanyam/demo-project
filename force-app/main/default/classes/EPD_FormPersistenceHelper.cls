/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    This class is designed to help with persistence logic for EPD Form.
                It takes care of persistence as well as cleaning necessary values during process
                and after fail.
Test Class:     EPD_FormPersistenceHelperTest
                EPD_FormPersistenceHelperTestNP
History:        jschn 2019-07-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormPersistenceHelper {

    private static final String CLASS_NAME = String.valueOf(EPD_FormPersistenceHelper.class);

    /**
     * This method clean related lists (it prevents persistence if not done) and upsert record.
     *
     * @param epd
     */
    public void persistEPD(EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> persistEPD. START.');

        epd.Equipment_Engine__r = null;
        epd.Work_Order__r = null;
        epd.Work_Order_Activity__r = null;
        epd.Company__r = null;
        epd.Data_Collected_By__r = null;

        upsert epd;

        System.debug(CLASS_NAME + ' -> persistEPD. END.');
    }

    /**
     * This method will parse records from Part Replacement structures into proper records in list. For parsing this method is utilizing
     * getRecord method on structure class to handle mapping correctly.
     *
     * @param wrappedPartReplacements
     * @param epd
     */
    public void persistPartReplacements(List<EPD_FormStructurePart> wrappedPartReplacements, EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> persistPartReplacements. START.');

        List<EPD_Part_Replacement__c> partReplacements = new List<EPD_Part_Replacement__c>();

        for(EPD_FormStructurePart wrappedPart : wrappedPartReplacements) {
            EPD_Part_Replacement__c partReplacement = (EPD_Part_Replacement__c) wrappedPart.getRecord();

            if(String.isBlank(partReplacement.Id)) {
                partReplacement.Engine_Performance_Data__c = epd.Id;
            }

            partReplacements.add(partReplacement);
        }

        upsert partReplacements;

        for(Integer i = 0; i < partReplacements.size(); i++) {
            if(String.isBlank(wrappedPartReplacements.get(i).recordId)) {
                wrappedPartReplacements.get(i).recordId = partReplacements.get(i).Id;
            }
        }

        System.debug(CLASS_NAME + ' -> persistPartReplacements. END.');
    }

    /**
     * This method will run parenting logic if necessary and persists EOC records.
     *
     * @param wrappedEOCs
     * @param epd
     */
    public void persistEOCs(List<EPD_FormStructureEOC> wrappedEOCs, EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> persistEOCs. START.');

        List<EPD_Engine_Operating_Condition__c> EOCs = new List<EPD_Engine_Operating_Condition__c>();

        for(EPD_FormStructureEOC wrappedEOC : wrappedEOCs) {
            EPD_Engine_Operating_Condition__c eoc = (EPD_Engine_Operating_Condition__c) wrappedEOC.getRecord();

            if(String.isBlank(eoc.Id)) {
                eoc.Engine_Performance_Data__c = epd.Id;
            }

            EOCs.add(eoc);
        }

        upsert EOCs;

        for(Integer i = 0; i < EOCs.size(); i++) {
            if(String.isBlank(wrappedEOCs.get(i).recordId)) {
                wrappedEOCs.get(i).recordId = EOCs.get(i).Id;
            }
        }

        System.debug(CLASS_NAME + ' -> persistEOCs. END.');
    }

    /**
     * This method will run parenting logic if necessary and persists COC records.
     *
     * @param stages
     * @param epd
     */
    public void persistCOCStages(List<EPD_FormStructureCOCStage> wrappedCOCStages, EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> persistCOCStages. START.');

        if(wrappedCOCStages != null) {
            List<EPD_Compressor_Operating_Condition_Stage__c> cocStages = new List<EPD_Compressor_Operating_Condition_Stage__c>();

            for(EPD_FormStructureCOCStage wrappedStage : wrappedCOCStages) {
                EPD_Compressor_Operating_Condition_Stage__c stage = (EPD_Compressor_Operating_Condition_Stage__c) wrappedStage.getRecord();

                if(String.isBlank(stage.Id)) {
                    stage.Engine_Performance_Data__c = epd.Id;
                }

                cocStages.add(stage);
            }

            upsert cocStages;

            for(Integer i = 0; i < cocStages.size(); i++) {
                if(String.isBlank(wrappedCOCStages.get(i).recordId)) {
                    wrappedCOCStages.get(i).recordId = cocStages.get(i).Id;
                }
            }
        }

        System.debug(CLASS_NAME + ' -> persistCOCStages. END.');
    }

    /**
     * This method will run parenting logic if necessary and persists Block Information records.
     * After successful upsert, it will run persistence logic for cylinders.
     *
     * @param wrappedBlocks
     * @param epd
     */
    public void persistBlockInfosWithChildRecs(List<EPD_FormStructureBlock> wrappedBlocks, EPD_Engine_Performance_Data__c epd) {
        System.debug(CLASS_NAME + ' -> persistBlockInfosWithChildRecs. START.');

        if(wrappedBlocks != null) {
            List<EPD_Block_Information__c> blockInfos = new List<EPD_Block_Information__c>();

            for(EPD_FormStructureBlock wrappedBlock : wrappedBlocks) {

                if(String.isBlank(wrappedBlock.recordId)) {
                    wrappedBlock.epdId = epd.Id;
                }

                blockInfos.add((EPD_Block_Information__c) wrappedBlock.getRecord());
            }

            upsert blockInfos;

            for(Integer i = 0; i < blockInfos.size(); i++) {
                if(String.isBlank(wrappedBlocks.get(i).recordId)) {
                    wrappedBlocks.get(i).recordId = blockInfos.get(i).Id;
                }
            }

            persistCylinderInfos(wrappedBlocks);
        }

        System.debug(CLASS_NAME + ' -> persistBlockInfosWithChildRecs. END.');
    }

    /**
     * This method will run parenting logic if necessary and persists Cylinder Information records.
     *
     * @param wrappedBlocks
     */
    public void persistCylinderInfos(List<EPD_FormStructureBlock> wrappedBlocks) {
        System.debug(CLASS_NAME + ' -> persistCylinderInfos. START.');

        if(wrappedBlocks != null) {
            List<EPD_Cylinder_Information__c> cylinderInfos = new List<EPD_Cylinder_Information__c>();

            for(EPD_FormStructureBlock wrappedBlock : wrappedBlocks) {

                for(EPD_FormStructureCylinder cylinderInfoWrapped : wrappedBlock.cylinders) {

                    if(String.isBlank(cylinderInfoWrapped.recordId)) {
                        cylinderInfoWrapped.blockInformationId = wrappedBlock.recordId;
                    }

                    cylinderInfos.add((EPD_Cylinder_Information__c) cylinderInfoWrapped.getRecord());
                }
            }

            upsert cylinderInfos;

            Integer counter = 0;
            for(EPD_FormStructureBlock wrappedBlock : wrappedBlocks) {

                for(Integer i = 0; i < wrappedBlock.cylinders.size(); i++) {

                    if(String.isBlank(wrappedBlock.cylinders.get(i).recordId)) {
                        wrappedBlock.cylinders.get(i).recordId = cylinderInfos.get(i).Id;
                    }

                    counter++;
                }
            }

        }

        System.debug(CLASS_NAME + ' -> persistCylinderInfos. END.');
    }

    /**
     * This method will clean all necessary recordIDs and relation IDs.
     *
     * @param formStructure
     */
    public void cleanIDsFromFormStructure(EPD_FormStructure formStructure) {
        System.debug(CLASS_NAME + ' -> cleanIDsFromFormStructure. START.');

        cleanEPDId(formStructure.epd);
        cleanPartsIds(formStructure.parts);
        cleanCOCStageIds(formStructure.cocStages);
        cleanBlockInfoIds(formStructure.engineBlocks);
        cleanEOCIds(formStructure.EOCs);

        System.debug(CLASS_NAME + ' -> cleanIDsFromFormStructure. END.');
    }

    /**
     * This method removes EPD Record Id.
     *
     * @param epd
     */
    private void cleanEPDId(EPD_FormStructureEPD epd) {
        if(epd != null) {
            epd.recordId = null;
        }
    }

    /**
     * This method removes Part record Id and relation ID to EPD
     *
     * @param wrappedPartReplacements
     */
    private void cleanPartsIds(List<EPD_FormStructurePart> wrappedPartReplacements) {
        if(wrappedPartReplacements != null) {
            for(EPD_FormStructurePart part : wrappedPartReplacements) {
                part.recordId = null;
                part.epdId = null;
            }
        }
    }

    /**
     * This method removes COC Stage record Id and relation ID to EPD
     *
     * @param wrappedCOCStages
     */
    private void cleanCOCStageIds(List<EPD_FormStructureCOCStage> wrappedCOCStages) {
        if(wrappedCOCStages != null) {
            for(EPD_FormStructureCOCStage cocStage : wrappedCOCStages) {
                cocStage.recordId = null;
                cocStage.enginePerformanceDataId = null;
            }
        }
    }

    /**
     * This method removes Block Information record Id and relation ID to EPD.
     * It runs method that cleans Cylinder Ids
     *
     * @param wrappedBlocks
     */
    private void cleanBlockInfoIds(List<EPD_FormStructureBlock> wrappedBlocks) {
        if(wrappedBlocks != null) {
            for(EPD_FormStructureBlock block : wrappedBlocks) {
                block.recordId = null;
                block.epdId = null;
                cleanCylinderIds(block.cylinders);
            }
        }
    }

    /**
     * This method removes Cylinder Information record Id and relation ID to Block Information
     *
     * @param wrappedCylinders
     */
    private void cleanCylinderIds(List<EPD_FormStructureCylinder> wrappedCylinders) {
        if(wrappedCylinders != null) {
            for(EPD_FormStructureCylinder cylinder : wrappedCylinders) {
                cylinder.recordId = null;
                cylinder.blockInformationId = null;
            }
        }
    }

    /**
     * This method removes EOC record Id and relation ID to EPD
     *
     * @param wrappedEOCs
     */
    private void cleanEOCIds(List<EPD_FormStructureEOC> wrappedEOCs) {
        if(wrappedEOCs != null) {
            for(EPD_FormStructureEOC EOCRecord : wrappedEOCs) {
                EOCRecord.recordId = null;
                EOCRecord.epdId = null;
            }
        }
    }

}