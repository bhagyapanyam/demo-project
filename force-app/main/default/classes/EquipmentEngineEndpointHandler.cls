/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class that handles request from endpoint
Test Class:     EquipmentEngineEndpointHandlerTest
                EquipmentEngineEndpointHandlerTestNP
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EquipmentEngineEndpointHandler {

    /**
     * First it will try to load Equipment record based on provided parameter (Equipment ID).
     * Then it will validate retrieved Equipment records. Only certain Object Types are supported.
     * Then it will run update and create engines logic through Equipment Engine Service class.
     *
     * @param equipmentId
     *
     * @return HOG_SimpleResponse
     */
    public HOG_SimpleResponse refreshEngineData(Id equipmentId) {
        HOG_SimpleResponse response;

        try {
            validateParam(equipmentId);
            List<Equipment__c> equipments = EPD_DAOProvider.equipmentDAO.getEquipments(
                    new Set<Id> { equipmentId }
            );
            validateEquipments(equipments);
            new EquipmentEngineService()
                    .updateEngines(equipments, EquipmentEngineSaveMode.ALL_OR_NOTHING_ENABLED)
                    .createEngines(equipments, EquipmentEngineSaveMode.ALL_OR_NOTHING_ENABLED);
            response = new HOG_SimpleResponse()
                    .addSuccessMessage(Label.EPD_Refresh_Engine_Data_Success_Message);
        } catch (Exception ex) {
            response = new HOG_SimpleResponse();
            response.addError(ex);
        }

        return response;
    }

    /**
     * This methods validates if correct equipments are entering the logic.
     * Only certain Object Types are supported.
     *
     * @param equipments
     */
    private void validateEquipments(List<Equipment__c> equipments){
        if(equipments != null) {
            for(Equipment__c eq : equipments) {
                if(!EPD_Constants.SUPPORTED_EQUIPMENT_OBJECT_TYPE.contains(eq.Object_Type__c)) {
                    HOG_ExceptionUtils.throwError(
                            Label.EPD_Refresh_Engine_Data_Not_Supported_Type,
                            new List<String> {
                                    eq.Id,
                                    eq.Object_Type__c
                            }
                    );
                }
            }
        }
    }

    /**
     * Validates if equipment Id param was provided.
     *
     * @param equipmentId
     */
    private void validateParam(Id equipmentId) {
        if(String.isBlank(equipmentId)) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Missing_Param,
                    new List<String> {
                            'Equipment ID'
                    }
            );
        }
    }

}