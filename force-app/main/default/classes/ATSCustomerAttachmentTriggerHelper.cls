public class ATSCustomerAttachmentTriggerHelper {
    public static Boolean runTrigger = true;
    public static List<ATS_Customer_Attachment_Junction__c> newAtsCustomerAtachmentList = new List<ATS_Customer_Attachment_Junction__c>();
    public static List<ATS_Customer_Attachment_Junction__c> oldAtsCustomerAtachmentList = new List<ATS_Customer_Attachment_Junction__c>();
    public static Map<Id, ATS_Customer_Attachment_Junction__c> newAtsCustomerAtachmentMap = new Map<Id, ATS_Customer_Attachment_Junction__c>();
    public static Map<Id, ATS_Customer_Attachment_Junction__c> oldAtsCustomerAtachmentMap = new Map<Id, ATS_Customer_Attachment_Junction__c>();
    
    public static void setAttachmentName() {
        Set<Id> attachmentIds = new Set<Id>();
        for( ATS_Customer_Attachment_Junction__c ats : newAtsCustomerAtachmentList ) {
            if( ats.Attachment_Id__c != null )
                attachmentIds.add( ats.Attachment_Id__c );
        }
        
        Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>([ Select Id, Name from Attachment where Id IN :attachmentIds ]);
        
        for( ATS_Customer_Attachment_Junction__c ats : newAtsCustomerAtachmentList ) {
            if( ats.Attachment_Id__c != null && attachmentMap.containsKey( ats.Attachment_Id__c ))
                ats.Attachment_Name__c = attachmentMap.get( ats.Attachment_Id__c ).Name;
        }
    }
}