/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for HOG_ContactDAOImpl class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class HOG_ContactDAOImplTest {

    @IsTest
    static void getActiveContactsByAccId_withoutParam() {
        HOG_ContactDAO selector = new HOG_ContactDAOImpl();
        List<Contact> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getActiveContactsByAccId(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getActiveContactsByAccId_withParam() {
        HOG_ContactDAO selector = new HOG_ContactDAOImpl();
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Contact con = VTT_TestData.createTradesmanContact('Natasha', 'Romanov', acc.Id, UserInfo.getUserId());
        List<Contact> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = selector.getActiveContactsByAccId(acc.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(con.Id, result.get(0).Id);
    }


    @IsTest
    static void getContactsByAccount_withoutParam() {
        VTT_SCContactDAO selector = new HOG_ContactDAOImpl();
        List<Contact> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getContactsByAccount(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getContactsByAccount_withParam() {
        VTT_SCContactDAO selector = new HOG_ContactDAOImpl();
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Contact con = VTT_TestData.createTradesmanContact('Natasha', 'Romanov', acc.Id, UserInfo.getUserId());
        List<Contact> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = selector.getContactsByAccount(acc.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(con.Id, result.get(0).Id);
    }

    @IsTest
    static void getTradesmanInfo_withParam() {
        VTT_ContactUtilityDAO selector = new HOG_ContactDAOImpl();
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Contact con = VTT_TestData.createTradesmanContact('Natasha', 'Romanov', acc.Id, UserInfo.getUserId());
        List<Contact> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = selector.getTradesmanInfo();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(con.Id, result.get(0).Id);
    }

}