/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Request_Notification_Form__c
-------------------------------------------------------------------------------------------------*/

public class ServiceRequestNotificationFormTestData
{

    /*---------------------------------------------------------------------------------
    Description: Function to create an item of HOG_Service_Request_Notification_Form__c
    Returns    : HOG_Service_Request_Notification_Form__c
    -----------------------------------------------------------------------------------*/        
    public static HOG_Service_Request_Notification_Form__c createServiceRequestNotificationForm
        (
            Id wellTrackerId,
            Id workOrderTypeId, 
            Id businessUnitId,
            Id operatingDistrictId,
            Id operatingFieldAMUId,
            Id facilityId,
            Id locationId,
            Id equipmentId,
            Id notificationTypePriorityId,
            Id workOrderPriorityId,
            Id vendorCompanyId,
            String notificationNumber,
            String workOrderNumber,
            String workDetails,
            String mainDescription,
            Date recurringStartDate,
            Date recurringEndDate              
        )
    {        

        HOG_Service_Request_Notification_Form__c results = new HOG_Service_Request_Notification_Form__c
            (
                Well_Tracker__c = wellTrackerId,
                HOG_Work_Order_Type__c = workOrderTypeId,
                Business_Unit_Lookup__c = businessUnitId,                                    
                Operating_District_Lookup__c = operatingDistrictId,
                Operating_Field_AMU_Lookup__c = operatingFieldAMUId,
                Facility_Lookup__c = facilityId,
                Location_Lookup__c = locationId,
                Equipment__c = equipmentId,
                HOG_Notification_Type_Priority__c = notificationTypePriorityId,
                HOG_Notification_Type_Priority_WO__c = workOrderPriorityId,
                Vendor_Company__c = vendorCompanyId,
                Notification_Number__c = notificationNumber,
                Work_Order_Number__c = workOrderNumber,
                Title__c = mainDescription,
                Work_Details__c = workDetails,
                Recurring_Start_Date__c = recurringStartDate,
                Recurring_End_Date__c = recurringStartDate                              
            );

        return results;
    }
}