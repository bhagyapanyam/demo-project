/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for ELG_Utilities
History:        mbrimus 2019-07-12 - Created.
*************************************************************************************************/
@IsTest
private class ELG_UtilitiesTest {

    @IsTest
    static void whenQuestionIsCreated_UUIDIsTaken() {

        // GIVEN
        ELG_Handover_Category__c category = ELG_TestDataFactory.createQuestionCategory('Generic', 'Test Cat', true);
        ELG_Handover_Category_Question__c question = ELG_TestDataFactory.createQuestion('Question', false);
        question.Handover_Category__c = category.Id;

        // WHEN
        insert question;

        // THEN
        ELG_Handover_Category_Question__c insertedQuestion = [
                SELECT
                        Question_UUID__c
                FROM ELG_Handover_Category_Question__c
                WHERE Id = :question.Id
                LIMIT 1
        ];

        System.assertNotEquals(null, insertedQuestion.Question_UUID__c,
                'Workflow is probably not active and UUID is not taken when Question is inserted');
    }

    @IsTest
    static void whenQuestionIsUpdated_UUIDIDoesNotChange() {

        // GIVEN
        ELG_Handover_Category__c category = ELG_TestDataFactory.createQuestionCategory('Generic', 'Test Cat', true);
        ELG_Handover_Category_Question__c question = ELG_TestDataFactory.createQuestion('Question', false);
        question.Handover_Category__c = category.Id;
        insert question;

        ELG_Handover_Category_Question__c insertedQuestion = [
                SELECT
                        Question_UUID__c
                FROM ELG_Handover_Category_Question__c
                WHERE Id = :question.Id
                LIMIT 1
        ];

        // WHEN
        update question;

        // THEN
        ELG_Handover_Category_Question__c updatedQuestion = [
                SELECT
                        Question_UUID__c
                FROM ELG_Handover_Category_Question__c
                WHERE Id = :question.Id
                LIMIT 1
        ];

        System.assertEquals(insertedQuestion.Question_UUID__c, updatedQuestion.Question_UUID__c,
                'Question UUID cannot change on update');

    }

    @IsTest
    static void whenPostIsCreated_onlyOneForAMUCanBeEnabledForEngineerReview() {

        // GIVEN
        List<ELG_Post__c> postsForAMU = ELG_TestDataFactory.createPosts('TestName', 5, false);
        postsForAMU.get(0).Shift_Engineer_Review_Required__c = true;
        insert postsForAMU;

        // WHEN
        ELG_Post__c newPost = ELG_TestDataFactory.createPost('Steam', false);
        newPost.Operating_Field_AMU__c = postsForAMU.get(0).Operating_Field_AMU__c;
        newPost.Shift_Engineer_Review_Required__c = true;

        // THEN
        try {
            insert newPost;
        } catch (Exception e) {
            System.debug('e.getMessage() ' + e.getMessage());
            System.assertEquals(
                    e.getMessage().contains('This Amu already has a Post that Requires Shift Engineer Review'), true);
        }
    }

    @IsTest
    static void whenPostIsUpdated_onlyOneForAMUCanBeEnabledForEngineerReview() {

        // GIVEN
        List<ELG_Post__c> postsForAMU = ELG_TestDataFactory.createPosts('TestName', 5, false);
        postsForAMU.get(0).Shift_Engineer_Review_Required__c = true;
        insert postsForAMU;

        // WHEN
        ELG_Post__c updatedPost = postsForAMU.get(1);
        updatedPost.Shift_Engineer_Review_Required__c = true;

        // THEN
        try {
            update updatedPost;
        } catch (Exception e) {
            System.debug('e.getMessage() ' + e.getMessage());
            System.assertEquals(
                    e.getMessage().contains('This Amu already has a Post that Requires Shift Engineer Review'), true);
        }
    }

    @IsTest
    static void whenShiftIsCreated_HandoverIsCreatedAlso() {

        // GIVEN
        List<ELG_Handover_Category_Question__c> questions =
                ELG_TestDataFactory.createQuestions('Question', 10, true);
        ELG_Post__c post = ELG_TestDataFactory.createPost('TestName', true);

        ELG_Shift_Assignement__c shiftAssignement = ELG_TestDataFactory.createShiftAssignement(false);
        shiftAssignement.Post__c = post.Id;

        // WHEN
        insert shiftAssignement;

        // THEN
        List<ELG_Shift_Handover__c> handoverDocument = [
                SELECT
                        Id
                FROM ELG_Shift_Handover__c
                WHERE Shift_Assignement__c = :shiftAssignement.Id
        ];

        System.assertEquals(1, handoverDocument.size(), 'Handover should be created with shift assignement');

    }

    @IsTest
    static void whenHandoverIsCreated_UUIDSnapShotOfActiveQuestionIsTaken() {

        // GIVEN
        ELG_TestDataFactory.createQuestions('Question', 10, true);
        ELG_Post__c post = ELG_TestDataFactory.createPost('TestName', true);

        ELG_Shift_Assignement__c shiftAssignement = ELG_TestDataFactory.createShiftAssignement(false);
        shiftAssignement.Post__c = post.Id;

        // WHEN
        insert shiftAssignement;

        // THEN
        ELG_Shift_Handover__c handover = [
                SELECT
                        Question_UUIDs__c
                FROM ELG_Shift_Handover__c
                WHERE Shift_Assignement__c = :shiftAssignement.Id
        ];

        String uuidOfQuestions = '';
        for (ELG_Handover_Category_Question__c q : [SELECT Question_UUID__c FROM ELG_Handover_Category_Question__c]) {
            System.assertNotEquals(null, q.Question_UUID__c, 'UUID should not be empty on question');
            uuidOfQuestions += q.Question_UUID__c + ';';
        }

        System.assertNotEquals(null, handover.Question_UUIDs__c, 'UUID snapshot should not be empty on handover');
        System.assertEquals(handover.Question_UUIDs__c, uuidOfQuestions, 'Handover should have taken the question UUID snapshot');
    }

    @IsTest
    static void whenHandoverIsUpdated_UUIDSnapShotShouldNotChange() {

        // GIVEN
        ELG_TestDataFactory.createQuestions('Question', 10, true);
        ELG_Post__c post = ELG_TestDataFactory.createPost('TestName', true);

        ELG_Shift_Assignement__c shiftAssignement = ELG_TestDataFactory.createShiftAssignement(false);
        shiftAssignement.Post__c = post.Id;
        insert shiftAssignement;


        String uuidOfQuestions = '';
        for (ELG_Handover_Category_Question__c q : [SELECT Question_UUID__c FROM ELG_Handover_Category_Question__c]) {
            System.assertNotEquals(null, q.Question_UUID__c, 'UUID should not be empty on question');
            uuidOfQuestions += q.Question_UUID__c + ';';
        }

        // WHEN
        ELG_Shift_Handover__c handover = [
                SELECT
                        Question_UUIDs__c
                FROM ELG_Shift_Handover__c
                WHERE Shift_Assignement__c = :shiftAssignement.Id
        ];

        update handover;

        // THEN
        String beforeUpdate = handover.Question_UUIDs__c;

        ELG_Shift_Handover__c afterUpdate = [
                SELECT
                        Question_UUIDs__c
                FROM ELG_Shift_Handover__c
                WHERE Shift_Assignement__c = :shiftAssignement.Id
        ];

        System.assertEquals(beforeUpdate, afterUpdate.Question_UUIDs__c, 'UUID on handover should not change when Handover is updated');
    }

    @IsTest
    static void whenUserIsShiftEngineer_heCanReviewAsShiftEngineer() {

        // GIVEN
        User reviewer = ELG_TestDataFactory.createReviewerUser();
        Boolean result;

        // WHEN
        Test.startTest();
        System.runAs(reviewer) {
            result = ELG_Utilities.hasEditAccessForReviewShiftEngineer();
        }
        Test.stopTest();

        System.assertEquals(true, result);

    }

    @IsTest
    static void whenUserIsNotShiftEngineer_heCannotReviewAsShiftEngineer() {

        // GIVEN
        User logUser = ELG_TestDataFactory.createLogEntryUser();
        Boolean result;

        // WHEN
        Test.startTest();
        System.runAs(logUser) {
            result = ELG_Utilities.hasEditAccessForReviewShiftEngineer();
        }
        Test.stopTest();

        System.assertEquals(false, result);

    }

    @IsTest
    static void whenUserIsReviewer_heCanDoReviews() {

        // GIVEN
        User reviewer = ELG_TestDataFactory.createReviewerUser();
        Boolean result;

        // WHEN
        Test.startTest();
        System.runAs(reviewer) {
            result = ELG_Utilities.hasEditAccessForReview();
        }
        Test.stopTest();

        System.assertEquals(true, result);

    }

    @IsTest
    static void whenUserIsNotReviewer_heCannotDoReviews() {

        // GIVEN
        User logUser = ELG_TestDataFactory.createLogEntryUser();
        Boolean result;

        // WHEN
        Test.startTest();
        System.runAs(logUser) {
            result = ELG_Utilities.hasEditAccessForReview();
        }
        Test.stopTest();

        System.assertEquals(false, result);

    }
}