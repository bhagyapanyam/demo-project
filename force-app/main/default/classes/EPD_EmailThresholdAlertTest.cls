/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EmailThresholdAlert
History:        jschn 2019-07-19 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EmailThresholdAlertTest {

    @IsTest
    static void testWithoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailThresholdAlert().sendEmails(null);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void testWithEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailThresholdAlert().sendEmails(new List<EPD_Engine_Performance_Data__c>());
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void testWithProperParam() {
        HOG_SObjectFactory.createSObject(new HOG_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, true);
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        EPD_Block_Information__c blockInfo = [SELECT Intake_Manifold_Pressure__c FROM EPD_Block_Information__c LIMIT 1];
        blockInfo.Intake_Manifold_Pressure__c = 99;
        update blockInfo;
        epd.Work_Order__r = new HOG_Maintenance_Servicing_Form__c(Planner_Group__c = '100', Main_Work_Centre__c = 'Test');
        //NOTE: No idea why it is failing on "INVALID_ID_FIELD, ID is invalid or you do not have access to the record."
        //I tried on dev console and it works like this.
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailThresholdAlert().sendEmails(
                    new List<EPD_Engine_Performance_Data__c> {
                            epd
                    }
            );
        } catch(Exception ex) {
            failFlag = true;
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    public class HPNTMock implements HOG_PlannerGroupNotificationTypeDAO {
        Contact contact = (Contact) HOG_SObjectFactory.createSObject(new Contact(), true);
        public List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes) {
            return new List<HOG_Planner_Group_Notification_Type__c> {
                    new HOG_Planner_Group_Notification_Type__c(
                            HOG_Planner_Group__r = new HOG_Planner_Group__c(Name = '100'),
                            Role_Type__c = 'Tradesman Lead',
                            Work_Center__c = 'Test',
                            Contact__c = contact.Id
                    )
            };
        }
    }

    @IsTest
    static void testWithRetrospectiveError() {
        HOG_SObjectFactory.createSObject(new HOG_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, true);
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPDWithRetrospectiveError();
        epd.Work_Order__r = new HOG_Maintenance_Servicing_Form__c(Planner_Group__c = '100', Main_Work_Centre__c = 'Test');
        //NOTE: No idea why it is failing on "INVALID_ID_FIELD, ID is invalid or you do not have access to the record."
        //I tried on dev console and it works like this.
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailThresholdAlert().sendEmails(
                    new List<EPD_Engine_Performance_Data__c> {
                            epd
                    }
            );
        } catch(Exception ex) {
            failFlag = true;
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}