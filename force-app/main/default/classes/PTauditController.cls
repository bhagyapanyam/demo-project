public class PTauditController {
    public String costCenterList {get;set;}
    public String dateList {get;set;}
    public String dateFromString {get;set;}
    public String dateToString {get;set;}
    public String sitesList {get;set;}
    public String recordtype {get;set;}
    public Boolean outputReport {get;set;}
    public Boolean albertaSelected {get;set;}
    public Boolean saskSelected {get;set;}
    public Boolean abActivate {get;set;}
    public Boolean skActivate {get;set;}
    public Boolean exceededSize{get;set;}
    public Field_Report_Record__c FRR {get;set;}
    public List<field_report_record__c> fFieldReportRecordList {get;set;}
    public List<String> listCostCenter = new List<String>();
    public List<String> listDate = new List<String>();
    public List<String> listSite = new List<String>();
    public Set<String> costCenterSet;
    public Set<Date> dateSet;
    public Set<String> siteSet;
    public Integer collectionSize = 200;
    public Integer collectionSizeDownload = 1000;
    public Integer finalSize {get;set;}
    
    public void GetAuditController(){
        outputReport = true;
        system.debug(outputReport);
        costCenterSet = new Set<String>();
        dateSet = new Set<Date>();
        siteSet = new Set<String>();
        fFieldReportRecordList = new List<field_report_record__c>();
        recordtype = null;
        List<field_report_record__c> initialFieldReportRecordList;
        
        Date fromDate = date.parse(dateFromString);
        Date toDate  = date.parse(dateToString);  
        
        if(albertaSelected == true){
            recordtype = 'AB_Field_Report_Record';
            initialFieldReportRecordList = new List<field_report_record__c>([select id,name,total_energy_dollars__c,File_Type__c,Consumption__c,Total_Dollars__c,PST_Dollars__c,Other_Dollars__c,Consumption_Charge_Dollars__c,Demand_Charge_Dollars__c,End_Date__c,Start_Date__c,Consumption_Month__c,account_group__c,amu__c,billing_demand__c,business_unit__c,cost_center__c,final__c,final_energy_dollars__c,final_volume__c,initial__c,initial_energy_dollars__c,initial_volume__c,interim__c,interim_energy_dollars__c,interim_volume__c,Metered_Demand__c,min_kw__c,operating_district__c,percentage__c,pfam_dollars__c,rate_code__c,retail_fee_total_dollars__c,site_id__c,status__c,total_cost__c,total_volume__c,uwi__c,wire_dollars__c from field_report_record__c where initial__c >= :fromDate and initial__c <= : toDate and recordtype.developername=: recordtype order by name limit  49999] );
        }
        else{
            recordtype = 'SK_Field_Report_Record';
            initialFieldReportRecordList = new List<field_report_record__c>([select id,name,total_energy_dollars__c,File_Type__c,Consumption__c,Total_Dollars__c,PST_Dollars__c,Other_Dollars__c,Consumption_Charge_Dollars__c,Demand_Charge_Dollars__c,End_Date__c,Start_Date__c,Consumption_Month__c,account_group__c,amu__c,billing_demand__c,business_unit__c,cost_center__c,final__c,final_energy_dollars__c,final_volume__c,initial__c,initial_energy_dollars__c,initial_volume__c,interim__c,interim_energy_dollars__c,interim_volume__c,Metered_Demand__c,min_kw__c,operating_district__c,percentage__c,pfam_dollars__c,rate_code__c,retail_fee_total_dollars__c,site_id__c,status__c,total_cost__c,total_volume__c,uwi__c,wire_dollars__c from field_report_record__c where consumption_month__c >= :fromDate and consumption_month__c<= : toDate and recordtype.developername=: recordtype order by name limit  49999] );
        }
        system.debug('This is the heap size at the beginning '+Limits.getHeapSize()); 
        
        List<field_report_record__c> refinedFieldReportRecordList = new List<field_report_record__c>();
        List<field_report_record__c> finalFieldReportRecordList = new List<field_report_record__c>();
        
        refinedFieldReportRecordList = initialFieldReportRecordList.clone();
                
        if (costCenterList != null && String.isNotEmpty(costCenterList) && refinedFieldReportRecordList.size() > 0){
        	listCostCenter = costCenterList.split('\n');
            for(String stringCostCenter : listCostCenter){
                costCenterSet.add(stringCostCenter.trim());
            }
            
			system.debug('This is the value of costCenterSet '+ costCenterSet);
            for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                if(costCenterSet.contains(fieldReportRecord.cost_center__c)){
                    finalFieldReportRecordList.add(fieldReportRecord);
                }
            }          
        }
        else{
            finalFieldReportRecordList = refinedFieldReportRecordList.clone();
            refinedFieldReportRecordList.clear();
        }
        
        if(dateList != null && String.isNotEmpty(dateList) && finalFieldReportRecordList.size() > 0){
            refinedFieldReportRecordList = finalFieldReportRecordList.clone();
            finalFieldReportRecordList.clear();
            listDate = dateList.split('\n');
            for(String stringDate : listDate){
            	dateSet.add(date.parse(stringDate));    
            }
            
            system.debug('This is the value of dateSet '+ dateSet);
            if(albertaSelected == true){
                for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                    if(dateSet.contains(fieldReportRecord.initial__c)){
                        finalFieldReportRecordList.add(fieldReportRecord);
                    }
                }
                refinedFieldReportRecordList.clear();
            }
            else{
                for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                    if(dateSet.contains(fieldReportRecord.consumption_month__c)){
                        finalFieldReportRecordList.add(fieldReportRecord);
                    }
                }
                refinedFieldReportRecordList.clear();                
            }
        }
        
        if (sitesList != null && String.isNotEmpty(sitesList) && finalFieldReportRecordList.size() > 0){
            refinedFieldReportRecordList = finalFieldReportRecordList.clone();
            finalFieldReportRecordList.clear();
            listSite = sitesList.split('\n');
            for(String stringSite : listSite){
                siteSet.add(stringSite.trim());
            }

            system.debug('This is the value of siteSet '+ siteSet);
            for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                if(siteSet.contains(fieldReportRecord.site_id__c)){
                    finalFieldReportRecordList.add(fieldReportRecord);
                }
            }
            refinedFieldReportRecordList.clear();
        }
        
        if(finalFieldReportRecordList.size() > 0){
            Integer collectionCount = 0;
            finalSize = finalFieldReportRecordList.size();
                if(finalSize > 1000){
                    exceededSize = true;
                }
                else{
                    exceededSize = false;
                }
            
            for(field_report_record__c fieldReportRecord : finalFieldReportRecordList){
                if(collectionCount < collectionSize){
                	fFieldReportRecordList.add(fieldReportRecord);    
                	collectionCount++;
                }
            }
            system.debug('This is the value of finalServiceList '+finalFieldReportRecordList);
        }
        else{
            finalSize = 0;
        }
        system.debug('This is the heap size at the end '+Limits.getHeapSize());
        
    }
    
    public Pagereference downloadReport(){
        List<field_report_record__c> initialFieldReportRecordList;
        fFieldReportRecordList = new List<field_report_record__c>();
        Date fromDate = date.parse(dateFromString);
        Date toDate  = date.parse(dateToString);
        
        if(albertaSelected == true){
            recordtype = 'AB_Field_Report_Record';
            initialFieldReportRecordList = new List<field_report_record__c>([select id,name,total_energy_dollars__c,File_Type__c,Consumption__c,Total_Dollars__c,PST_Dollars__c,Other_Dollars__c,Consumption_Charge_Dollars__c,Demand_Charge_Dollars__c,End_Date__c,Start_Date__c,Consumption_Month__c,account_group__c,amu__c,billing_demand__c,business_unit__c,cost_center__c,final__c,final_energy_dollars__c,final_volume__c,initial__c,initial_energy_dollars__c,initial_volume__c,interim__c,interim_energy_dollars__c,interim_volume__c,Metered_Demand__c,min_kw__c,operating_district__c,percentage__c,pfam_dollars__c,rate_code__c,retail_fee_total_dollars__c,site_id__c,status__c,total_cost__c,total_volume__c,uwi__c,wire_dollars__c from field_report_record__c where initial__c >= :fromDate and initial__c <= : toDate and recordtype.developername=: recordtype order by name limit  49999] );
        }
        else{
            recordtype = 'SK_Field_Report_Record';
            initialFieldReportRecordList = new List<field_report_record__c>([select id,name,total_energy_dollars__c,File_Type__c,Consumption__c,Total_Dollars__c,PST_Dollars__c,Other_Dollars__c,Consumption_Charge_Dollars__c,Demand_Charge_Dollars__c,End_Date__c,Start_Date__c,Consumption_Month__c,account_group__c,amu__c,billing_demand__c,business_unit__c,cost_center__c,final__c,final_energy_dollars__c,final_volume__c,initial__c,initial_energy_dollars__c,initial_volume__c,interim__c,interim_energy_dollars__c,interim_volume__c,Metered_Demand__c,min_kw__c,operating_district__c,percentage__c,pfam_dollars__c,rate_code__c,retail_fee_total_dollars__c,site_id__c,status__c,total_cost__c,total_volume__c,uwi__c,wire_dollars__c from field_report_record__c where consumption_month__c >= :fromDate and consumption_month__c<= : toDate and recordtype.developername=: recordtype order by name limit  49999] );
        }
        
        List<field_report_record__c> refinedFieldReportRecordList= new List<field_report_record__c>();
        List<field_report_record__c> finalFieldReportRecordList = new List<field_report_record__c>();
        
        refinedFieldReportRecordList = initialFieldReportRecordList.clone();
        
        if (costCenterList != null && String.isNotEmpty(costCenterList) && refinedFieldReportRecordList.size() > 0){
			system.debug('This is the value of costCenterSet '+ costCenterSet);
            for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                if(costCenterSet.contains(fieldReportRecord.cost_center__c)){
                    finalFieldReportRecordList.add(fieldReportRecord);
                }
            }            
        }
        else{
            finalFieldReportRecordList = refinedFieldReportRecordList.clone();
            refinedFieldReportRecordList.clear();
        }
        
        if(dateList != null && String.isNotEmpty(dateList) && finalFieldReportRecordList.size() > 0){
            refinedFieldReportRecordList = finalFieldReportRecordList.clone();
            finalFieldReportRecordList.clear();
            listDate = dateList.split('\n');
            for(String stringDate : listDate){
            	dateSet.add(date.parse(stringDate));    
            }
            
            system.debug('This is the value of dateSet '+ dateSet);
            if(albertaSelected == true){
                for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                    if(dateSet.contains(fieldReportRecord.initial__c)){
                        finalFieldReportRecordList.add(fieldReportRecord);
                    }
                }
                refinedFieldReportRecordList.clear();
            }
            else{
                for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                    if(dateSet.contains(fieldReportRecord.consumption_month__c)){
                        finalFieldReportRecordList.add(fieldReportRecord);
                    }
                }
                refinedFieldReportRecordList.clear();                
            }
        }
        
        if (sitesList != null && String.isNotEmpty(sitesList) && finalFieldReportRecordList.size() > 0){
            refinedFieldReportRecordList = finalFieldReportRecordList.clone();
            finalFieldReportRecordList.clear();
            listSite = sitesList.split('\n');
            for(String stringSite : listSite){
                siteSet.add(stringSite.trim());
            }

            system.debug('This is the value of siteSet '+ siteSet);
            for(field_report_record__c fieldReportRecord : refinedFieldReportRecordList){
                if(siteSet.contains(fieldReportRecord.site_id__c)){
                    finalFieldReportRecordList.add(fieldReportRecord);
                }
            }
            refinedFieldReportRecordList.clear();
        }
        
        if(finalFieldReportRecordList.size() > 0){
            Integer collectionCountDownload = 0;
            for(field_report_record__c fieldReportRecord : finalFieldReportRecordList){
                if(collectionCountDownload < collectionSizeDownload){
                	fFieldReportRecordList.add(fieldReportRecord);    
                	collectionCountDownload++;
                }
            }
            system.debug('This is the value of finalServiceList '+finalFieldReportRecordList);
        }
        
		return new Pagereference('/apex/PT_Audit_Excel_VF');        
    }    
    public void hideReport(){
        outputReport = false;
        saskSelected = false;
        skActivate = false;
        albertaSelected = true;
        abActivate = true;
        exceededSize = false;
        FRR = new field_report_record__c();
    }
    
    public void disableAlberta(){
		albertaSelected = false;
        abActivate = false;
        saskSelected = true;
        skActivate = true;
    }
    
    public void disableSask(){
        saskSelected = false;
        skActivate = false;
        albertaSelected = true;
        abActivate = true;
    }
    
    
}