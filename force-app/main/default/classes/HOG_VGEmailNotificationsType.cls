/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Enums for Vent Gas Email notification types
History:        jschn 25/10/2018 - Created. US W-001290, W-001293, W-001304
*************************************************************************************************/
public enum HOG_VGEmailNotificationsType {
    URGENT_INCOMPLETE_ALERTS,
    NON_URGENT_INCOMPLETE_ALERTS,
    PRODUCTION_ENGINEER_REASSIGNMENT,
    URGENT_INCOMPLETE_TASKS,
    NON_URGENT_INCOMPLETE_TASKS
}