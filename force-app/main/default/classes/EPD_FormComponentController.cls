/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller for classic version of the form.
Test Class:     EPD_FormComponentControllerTest
                EPD_FormComponentControllerTestNP
History:        jschn 2019-06-20 - Created. - EPD R1
*************************************************************************************************/
public without sharing class EPD_FormComponentController {

    private static final String CLASS_NAME = String.valueOf(EPD_FormComponentController.class);

    public EPD_FormStructure formStructure {get;set;}
    public String woaRecordId {
        get;
        set {
            woaRecordId = value;
            loadFormStructure(false);
        }
    }
    public Boolean hasInitLoadError {get;set;}
    public Boolean showForm {get;set;}

    public String engineBlockInputLabelToCheck {get;set;}

    public String engineBlockNameToCheck {get;set;}
    public String cylinderNumberToCheck {get;set;}

    public String warningRPM {get;set;}
    public String warningIMP {get;set;}
    public String warningValveSeatRecession {get;set;}
    public String warningCompression {get;set;}

    public Boolean intakeManifoldPressureError {get;set;}

    public EPD_FormComponentController() {
        resetModals();
        showForm = true;
    }

    /**
     * Loads form structure.
     * When ForceReload flag is enabled, it will reload even if record is already loaded.
     * Logic is using EPD_Endpoint to handle load.
     *
     * @param forceReload
     */
    private void loadFormStructure(Boolean forceReload) {
        hasInitLoadError = false;

        System.debug(CLASS_NAME + ' -> loadFormStructure. Force Reload: ' + forceReload);

        if(forceReload || formStructure == null ) {

            EPD_Response response = EPD_Endpoint.loadForm(woaRecordId);
            if (response.success) {

                formStructure = response.formStructure;
                setWarningMessages();

            } else {

                handleErrors(response);
                hasInitLoadError = true;

            }
            System.debug(CLASS_NAME + ' -> loadFormStructure. Load finished.');
        }
    }

    /**
     * Prepare warning messages for thresholds.
     */
    private void setWarningMessages() {
        warningRPM = String.format(
                EPD_Constants.THRESHOLD_WARNING_BASE,
                new List<String> {
                        'Min RPM: ',
                        String.valueOf(formStructure.engineThresholds.minRPM),
                        ', Max RPM: ',
                        String.valueOf(formStructure.engineThresholds.maxRPM)
                }
        );
        warningIMP = String.format(
                EPD_Constants.THRESHOLD_WARNING_BASE,
                new List<String> {
                        'Min Intake Manifold Pressure: ',
                        String.valueOf(formStructure.engineThresholds.minIntakeManifoldPressure),
                        ', Max Intake Manifold Pressure: ',
                        String.valueOf(formStructure.engineThresholds.maxIntakeManifoldPressure)
                }
        );
        warningValveSeatRecession = String.format(
                EPD_Constants.THRESHOLD_WARNING_BASE,
                new List<String> {
                        'Max Valve Seat Recession: ',
                        String.valueOf(formStructure.engineThresholds.maxValveSeatRecession),
                        '',
                        ''
                }
        );
        warningCompression = String.format(
                EPD_Constants.THRESHOLD_WARNING_BASE,
                new List<String> {
                        'Min Cylinder Compression: ',
                        String.valueOf(formStructure.engineThresholds.minCylinderCompression),
                        ', Max Cylinder Compression: ',
                        String.valueOf(formStructure.engineThresholds.maxCylinderCompression)
                }
        );
    }

    /**
     * Handle errors from EPD_Response. Adds error messages to Page Messages.
     *
     * @param response
     */
    private void handleErrors(EPD_Response response) {
        for (String errorMessage : response.errors) {
            HOG_GeneralUtilities.addPageError(errorMessage);
        }
    }

//    ACTIONS
    /**
     * Save form. Handles through EPD_Endpoint action
     */
    public void saveForm() {
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        EPD_Response response = EPD_Endpoint.saveForm(formStructure.toString());
        if(response.success) {
            formStructure = response.formStructure;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Form was successfully saved. You can close form if you want.'));
        } else {
            handleErrors(response);
        }
    }

    /**
     * Submit form. Handles through EPD_Endpoint action
     */
    public void submitForm() {
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        EPD_Response response = EPD_Endpoint.submitForm(formStructure.toString());
        if(response.success) {
            formStructure = response.formStructure;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Form was successfully submitted. You can close form if you want.'));
            showForm = false;
        } else {
            handleErrors(response);
        }
    }

    /**
     * Close form.
     */
    public PageReference closeForm() {
        PageReference pageRef = Page.VTT_ActivityDetailTimeLog;
        pageRef.getParameters().put('id', woaRecordId);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /**
     * Checks RPM Thresholds. If RPM values are outside of thresholds warning messages is displayed.
     */
    public void checkRPM() {
        formStructure.epd.EIRPM_Error = false;
        if(EPD_Utility.isOutsideOfRange(formStructure.epd.EIRPM, formStructure.engineThresholds.minRPM, formStructure.engineThresholds.maxRPM)) {

            System.debug(CLASS_NAME + ' -> checkRPM. Threshold reached. Displaying error message.');

            formStructure.epd.EIRPM_Error = true;
            HOG_GeneralUtilities.addPageError(warningRPM);
        }
    }

    /**
     * Checks Intake Manifold Pressure Thresholds.
     * If Intake Manifold Pressure values are outside of thresholds warning messages is displayed.
     */
    public void checkIMP() {
        EPD_FormStructureBlock engineBlockToCheck = getBlockByIMPLabel(engineBlockInputLabelToCheck);
        if(engineBlockToCheck != null) {
            intakeManifoldPressureError = false;

            if(EPD_Utility.isOutsideOfRange(engineBlockToCheck.intakeManifoldPressure,
                    formStructure.engineThresholds.minIntakeManifoldPressure,
                    formStructure.engineThresholds.maxIntakeManifoldPressure)) {

                System.debug(CLASS_NAME + ' -> checkIMP. Threshold reached. Displaying error message.');

                intakeManifoldPressureError = true;
                HOG_GeneralUtilities.addPageError(warningIMP);
            }
        }
    }

    /**
     * Checks Wear Intake Thresholds. If Wear Intake values are outside of thresholds warning messages is displayed.
     */
    public void checkIntake() {
        EPD_FormStructureCylinder focusedCylinder = getCylinderByNumber(getBlockByName(engineBlockNameToCheck), cylinderNumberToCheck);
        if(focusedCylinder != null) {
            focusedCylinder.wearIntakeError = false;

            if(EPD_Utility.isBigger(focusedCylinder.wearIntake, formStructure.engineThresholds.maxValveSeatRecession)) {

                System.debug(CLASS_NAME + ' -> checkIntake. Threshold reached. Displaying error message.');

                focusedCylinder.wearIntakeError = true;
                HOG_GeneralUtilities.addPageError(warningValveSeatRecession);
            }
        }
    }

    /**
     * Checks Compression Thresholds. If Compression values are outside of thresholds warning messages is displayed.
     */
    public void checkCompression() {
        EPD_FormStructureCylinder focusedCylinder = getCylinderByNumber(getBlockByName(engineBlockNameToCheck), cylinderNumberToCheck);
        if(focusedCylinder != null) {
            focusedCylinder.compressionError = false;
            if(EPD_Utility.isOutsideOfRange(focusedCylinder.compression,
                    formStructure.engineThresholds.minCylinderCompression,
                    formStructure.engineThresholds.maxCylinderCompression)) {

                System.debug(CLASS_NAME + ' -> checkCompression. Threshold reached. Displaying error message.');

                focusedCylinder.compressionError = true;
                HOG_GeneralUtilities.addPageError(warningCompression);
            }
        }
    }

    /**
     * Checks Wear Exhaust  Thresholds. If Wear Exhaust  values are outside of thresholds warning messages is displayed.
     */
    public void checkExhaust() {
        EPD_FormStructureCylinder focusedCylinder = getCylinderByNumber(getBlockByName(engineBlockNameToCheck), cylinderNumberToCheck);
        if(focusedCylinder != null) {
            focusedCylinder.wearExhaustError = false;
            if(EPD_Utility.isBigger(focusedCylinder.wearExhaust, formStructure.engineThresholds.maxValveSeatRecession)) {

                System.debug(CLASS_NAME + ' -> checkExhaust. Threshold reached. Displaying error message.');

                focusedCylinder.wearExhaustError = true;
                HOG_GeneralUtilities.addPageError(warningValveSeatRecession);
            }
        }
    }

    /**
     * Return Block structure based on selected block name.
     *
     * @param blockName
     *
     * @return EPD_FormStructureBlock
     */
    private EPD_FormStructureBlock getBlockByName(String blockName) {
        for(EPD_FormStructureBlock block : formStructure.engineBlocks) {
            if(block.blockName == blockName) {
                return block;
            }
        }
        System.debug(CLASS_NAME + ' -> getBlockByName. Block was not found. ' + blockName);
        return null;
    }

    /**
     * Return Cylinder structure based on selected cylinder number.
     *
     * @param focusedEngineBlock
     * @param cylinderNumberToCheck
     *
     * @return EPD_FormStructureCylinder
     */
    private EPD_FormStructureCylinder getCylinderByNumber(EPD_FormStructureBlock focusedEngineBlock, String cylinderNumberToCheck) {
        if(focusedEngineBlock != null) {
            for (EPD_FormStructureCylinder cylinder : focusedEngineBlock.cylinders) {
                if (String.valueOf(cylinder.cylinderNumber) == cylinderNumberToCheck) {
                    return cylinder;
                }
            }
        }
        System.debug(CLASS_NAME + ' -> getCylinderByNumber. Cylinder was not found. ' + cylinderNumberToCheck);
        return null;
    }

    /**
     * Return Block structure based on selected Intake Manifold Pressure input label.
     *
     * @param inputLabel
     *
     * @return EPD_FormStructureBlock
     */
    private EPD_FormStructureBlock getBlockByIMPLabel(String inputLabel) {
        for(EPD_FormStructureBlock block : formStructure.engineBlocks) {
            if(block.inputLabelIMP == inputLabel) {
                return block;
            }
        }
        System.debug(CLASS_NAME + ' -> getBlockByIMPLabel. Block was not found. ' + inputLabel);
        return null;
    }

//    MODAL ACTIONS
    /**
     * Reset modals display flag.
     */
    private void resetModals() {
        showThresholds = false;
    }
//    ENGINE THRESHOLDS MODAL
    public Boolean showThresholds {get;set;}
    /**
     * Change show thresholds flag to false
     */
    public void closeEngineThresholds() {
        showThresholds = false;
    }
    /**
     * Change show thresholds flag to true
     */
    public void openEngineThresholds() {
        showThresholds = true;
    }

}