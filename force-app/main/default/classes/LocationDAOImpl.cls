/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of DAO interface for Location__c
Test Class:     LocationDAOImplTest
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public inherited sharing class LocationDAOImpl implements LocationDAO {

    /**
     * Retrieves location record by Location Id
     *
     * @param wellId
     *
     * @return List<Location__c>
     */
    public List<Location__c> getLocationById(Id wellId) {
        return [
                SELECT Functional_Location_Category__c, Well_Type__c
                        , Operating_Field_AMU__r.Is_Thermal__c, Operating_Field_AMU__r.Production_Engineer_User__c
                FROM Location__c
                WHERE Id = :wellId
        ];
    }

}