/**
 * Created by shreyasdhond1 on 2018-10-03.
 */
@isTest
private class VTT_RetryConfirmationBatchTest {
    @isTest static void testSuccessResponse() {
        User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
            //Set Mock
            Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(false);

            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';
            workOrder1.Work_Order_Priority_Number__c  = '1';
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;
            update workOrder1;

            MaintenanceServicingUtilities.executeTriggerCode = true;

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Operation_Number__c = '0010';
            update woActivity;

            //Create Logs and Log Entries
            List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null,
                    null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_JOBONHOLD, null,
                    null, null, 'On Hold'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null,
                    null, null, 'Start Job Again'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    null, null, 'Start Job at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    null, null, 'Finish Job at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY, null,
                    null, null, 'Finish for the day'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null,
                    null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    null, null, 'Finish Job at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1,
                    woActivity, VTT_Utilities.LOGENTRY_JOBCOMPLETE, null,
                    null, null, 'Job Complete'));
            insert logEntryList;

            //Change status of confirmations to ERROR
            List<Work_Order_Activity_Confirmation__c> confirmationList = [Select Id, Confirmation_Text__c, Final_Confirmation__c, Start_Log_Entry__r.Status__c,
                    End_Log_Entry__r.Status__c, Status__c, Tradesman__c, Work_Order_Activity__c,
                    Confirmation_Number__c
            From Work_Order_Activity_Confirmation__c];
            for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
                confirmation.Status__c = VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR;
            }
            update confirmationList;

            //Setting for batch job
            HOG_Settings__c hogSettings = (HOG_Settings__c) HOG_SObjectFactory.createSObject(new HOG_Settings__c());
            hogSettings.VTT_Retry_Confirmations_Expiry_Days__c = 30;
            insert hogSettings;
        }


        //Test
        Test.startTest();
            Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl(false));
            Database.executeBatch(new VTT_RetryConfirmationBatch());
        Test.stopTest();

        //Post-test check
        List<Work_Order_Activity_Confirmation__c> confirmationList = [Select Id, Confirmation_Text__c, Final_Confirmation__c, Start_Log_Entry__r.Status__c,
                End_Log_Entry__r.Status__c, Status__c, Tradesman__c, Work_Order_Activity__c,
                Confirmation_Number__c
        From Work_Order_Activity_Confirmation__c];
        System.assertEquals(confirmationList.size(), 3);
        System.assertEquals(confirmationList[0].Status__c, VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_SENT);
        System.assertEquals(confirmationList[1].Status__c, VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_SENT);
        System.assertEquals(confirmationList[2].Status__c, VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_SENT);
    }
}