/*************************************************************************************************\
Author:         Martin Jurcek
Company:        Husky Energy
Description:    Controller Extension for Runsheet Functionality
History:        jschon  - 8.7.2018  - Added ability to retrieve filter options from LoadRequestRecreate
**************************************************************************************************/


public class FM_LoadRequestControllerX {

    public String sourceType                    {get; set;}
    public String selectedRouteId               {get; set;}
    public String selectedLocationId            {get; set;}
    public Location__c selectedLocation         {get; set;}
    public String selectedFacilityId            {get; set;}
    public Facility__c selectedFacility         {get; set;}
    public Boolean sourceH2S                    {get; set;}
    public List<SelectOption> locationOptions   {get; private set;}

    private static final String LOCATION = 'location';
    private static final String FACILITY = 'facility';

    private UrlParameters urlParams;

    public FM_LoadRequestControllerX(ApexPages.StandardController stdController) {
      getUrlParameters();
      sourceType = (urlParams.facilityId <> null) ? FACILITY : LOCATION;
      selectedRouteId = urlParams.routeId;
      if(sourceType.equals(LOCATION))
        BuildLocationLists();
      selectedFacilityId = urlParams.facilityId;
      selectedLocationId = urlParams.locationId;
    }

    //Getter and Setter functions
    public List<SelectOption> sourceTypeOptions {
      get {
        if(sourceTypeOptions == null) {
          sourceTypeOptions = new List<SelectOption>();
          for(String key : FM_LoadRequest_Utilities.LOADREQUEST_SOURCE_TYPE.keySet()) {
            sourceTypeOptions.add(new SelectOption(key, FM_LoadRequest_Utilities.LOADREQUEST_SOURCE_TYPE.get(key)));
          }
        }
        return sourceTypeOptions;
      }
      private set;
    }

    public List<SelectOption> facilityOptions {
        get{
            if(facilityOptions == null){
                facilityOptions = FM_Utilities.GetRunsheetFacilityList();
            }
            return facilityOptions;
        }
        private set;
    }

    public List<SelectOption> routeOptions {
      get {
        if(routeOptions == null) {
          List<Equipment_Tank__c> lAllRoutes = [SELECT Equipment__r.Location__r.Route__r.Id, Equipment__r.Location__r.Route__r.Name, Equipment__c
                                                FROM Equipment_Tank__c
                                                WHERE Equipment__r.Location__r.Route__r.Fluid_Management__c = true
                                                AND Equipment__r.Location__r.Functional_Location_Category__c IN :FM_Utilities.RUNSHEET_FUNCTIONAL_LOCATION_CATEGORIES
                                                AND Equipment__r.Location__r.Status__c IN :FM_Utilities.RUNSHEET_WELL_INCLUDED_STATUSES
                                                AND Equipment__r.Location__r.Well_Type__c IN :FM_Utilities.RUNSHEET_WELL_TYPES
                                                AND Tank_Settings__c <> 'Hidden - Hide this Tank from the selectable list.'];
          Set<Id> setRoutes = new Set<Id>();

          routeOptions = new List<SelectOption>();
          for(Equipment_Tank__c oOneRoute : lAllRoutes){
              if(!setRoutes.contains(oOneRoute.Equipment__r.Location__r.Route__r.Id)){
                  routeOptions.add(new SelectOption(oOneRoute.Equipment__r.Location__r.Route__r.Id, oOneRoute.Equipment__r.Location__r.Route__r.Name));
                  setRoutes.add(oOneRoute.Equipment__r.Location__r.Route__r.Id);
              }
          }

          // Sort the final Route List
          routeOptions = FM_Utilities.SortSelectOptions(routeOptions, FM_Utilities.SortSelectOptionBy.Label);
        }
        System.debug('routeOptions: ' + routeOptions);
        return routeOptions;
      }
      private set;
    }

    public static String getNotSelected() {
        return FM_Utilities.RUNSHEET_VALUE_NOT_SELECTED;
    }

    //Page Actions
    public PageReference RefreshLocationList(){
        InitializeLocationData();

        if(this.selectedRouteId != null) {
            BuildLocationLists();
        }

        return null;
    }

    // reset preselected variables and do nothing
    // this method is used when user changes sourceType picklist 
    public PageReference NoAction() {
      selectedRouteId = null;
      selectedLocationId = null;
      selectedFacilityId = null;
      selectedLocation = null;
      selectedFacility = null;
      if (this.locationOptions != null)
        this.locationOptions.clear();
      return null;
    }

    public PageReference getSourceData() {
      selectedLocation = null;
      selectedFacility = null;
      sourceH2S = false;

      if (selectedLocationId != null) {
          selectedLocation = [SELECT Hazardous_H2S__c FROM Location__c WHERE Id = :selectedLocationId LIMIT 1];
          sourceH2S = (selectedLocation.Hazardous_H2S__c == 'Y') ? true : false;
      }
      else if (selectedFacilityId != null) {
          // this condition runs SELECET on Location__c or Facility__c because some selectedFacilityId can be a valid location in fact
          // these other facilities are non standard locations having id concatenated with string: ###_WELL_###
          if (selectedFacilityId.contains(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)) {
              selectedLocation = [SELECT Hazardous_H2S__c FROM Location__c WHERE Id = :selectedFacilityId.replace(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER, '') LIMIT 1];
              sourceH2S = (selectedLocation.Hazardous_H2S__c == 'Y') ? true : false;
          } else {
            selectedFacility = [SELECT Hazardous_H2S__c FROM Facility__c WHERE Id = :selectedFacilityId LIMIT 1];
            sourceH2S = (selectedFacility.Hazardous_H2S__c == 'Y') ? true : false;
          }

      }
      System.debug('selectedLocationId: ' + selectedLocationId + ' selectedLocation: ' + selectedLocation);
      System.debug('selectedFacilityId: ' + selectedFacilityId + ' selectedFacility: ' + selectedFacility);
      return null;
    }

    //Utility Functions
    private void InitializeLocationData(){
      this.locationOptions = new List<SelectOption>();
      this.selectedLocationId = null;
      this.selectedFacilityId = null;
    }

    private void BuildLocationLists() {
      this.locationOptions = FM_LoadRequest_Utilities.GetLocationOptionsForLoadRequests(this.selectedRouteId);
    }

    private void getUrlParameters() {
      urlParams = new UrlParameters();
      urlParams.facilityId = ApexPages.currentPage().getParameters().get('facilityid');
      urlParams.routeId = ApexPages.currentPage().getParameters().get('routeid');
      urlParams.locationId = ApexPages.currentPage().getParameters().get('wellid');
      getUrlParamsFromLoadRequestRecreate();
    }

    private void getUrlParamsFromLoadRequestRecreate() {
      String sourceType = HOG_GeneralUtilities.getUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_TYPE);
      String sourceId = HOG_GeneralUtilities.getUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_ID);
      String routeId = HOG_GeneralUtilities.getUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ROUTE_ID);
      if(String.isNotBlank(sourceType)
      && String.isNotBlank(sourceId)
      && String.isNotBlank(routeId)) {
        if(sourceType.equals(LOCATION)) {
          urlParams.locationId = sourceId;
          urlParams.facilityId = null;
        } else {
          urlParams.locationId = null;
          urlParams.facilityId = sourceId;
        }
        urlParams.routeId = routeId;
      }
    }

    private class UrlParameters {
      public String facilityId {get; set;}
      public String routeId {get; set;}
      public String locationId {get; set;}
    }
}