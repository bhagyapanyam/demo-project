public class ATSSMSReportGeneratorControllerOpp{
    String[] categories= new String[]{};
        public String categoriesList {get;set;} // same content as categories but separated with , when used pdfPage.getParameters().put it accepts primitive types
    String[] customers= new String[]{};
        public String opportunityList {get;set;}
    public Boolean reportSuccess {get;set;}
    
   //  Map<String, String> categoriesMap = new Map<String, String>();
    Map<String, String> customersMap = new Map<String, String>();
    public List<SelectOption> CategoryOptions = new List<SelectOption>();
    List<SelectOption> CustomerOptions = new List<SelectOption>();
    //List<Opportunity> tax = new List<Opportunity>();
    //List<Opportunity> payout = new List<Opportunity>();

    
    
    
    private final Opportunity tender;
    
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public ATSSMSReportGeneratorControllerOpp(ApexPages.StandardController stdController) {
        reportSuccess = false;
        this.tender= (Opportunity)stdController.getRecord();
        
        for(Cpm_Customer_Opportunity__c opp: [SELECT id, name, cpm_OpportunityLookup__r.cpm_Product_Category__c FROM Cpm_Customer_Opportunity__c WHERE 
                                              cpm_OpportunityLookup__c = :tender.id AND cpm_Won__c = TRUE])
        {
            //String oppName = opp.name + ' (' + opp.RecordType.Name + ')';     
            
            List<String> parsedCategories = new List<String>();
            
            if (opp.cpm_OpportunityLookup__r.cpm_Product_Category__c != null)
            {
                parsedCategories = opp.cpm_OpportunityLookup__r.cpm_Product_Category__c.split(';');          
            }
            
            List<Integer> tmp = new Integer[]{64};
                categoriesList = '';
                for(string ctg : parsedCategories)
            {
                tmp[0]++;
                String convertedChar =  string.fromCharArray(tmp); 
                //CategoryOptions.add(new SelectOption(convertedChar, ctg));
                //CategoryOptions.add(new SelectOption(opp.id,oppName ));
                categories.add(ctg); //categories.add(opp.id);
                //categoriesList = ctg + ',';
                //categoriesMap.put(opp.id,opp.name);
            }            
            
        }
        
        // [From Picklist] - fill the checkboxes list directly with pick-list values 
        Schema.DescribeFieldResult fieldResult = Opportunity.cpm_Product_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            CategoryOptions.add(new SelectOption(f.getLabel(), f.getValue()));                        
        }        
        
        for(Cpm_Customer_Opportunity__c obj: [SELECT cpm_AccountLookup__r.id, name, cpm_AccountLookup__r.Name, cpm_AccountLookup__r.ATS_Customer_Type__c  FROM Cpm_Customer_Opportunity__c 
                                              WHERE cpm_OpportunityLookup__c =: tender.id and cpm_Won__c = TRUE])
        {
            SelectOption customerOption;
            
            System.debug('Customertype: ');
            System.debug(obj.cpm_AccountLookup__r.ATS_Customer_Type__c);
            
            if(obj.cpm_AccountLookup__r.ATS_Customer_Type__c == null)
            {
                customerOption = new SelectOption(obj.cpm_AccountLookup__c, obj.cpm_AccountLookup__r.name + ' (Unknown Customer Type)',true);
            }
            else
            {
                customerOption = new SelectOption(obj.cpm_AccountLookup__c, obj.cpm_AccountLookup__r.name + ' (' + obj.cpm_AccountLookup__r.ATS_Customer_Type__c +  ')');   
                customers.add(obj.cpm_AccountLookup__c);         
            }
            CustomerOptions.add(customerOption);
            customersMap.put(obj.cpm_AccountLookup__c,obj.cpm_AccountLookup__r.name ); 
            
        }     
    }
    
    public PageReference doSearch() {
      
      Opportunity opp =  [SELECT cpm_Tax_Exempt_picklist__c, cpm_Tax_Exempt_picklist2__c, Pay_out_Handling_Fees__c, Pay_out_Handling_Fees2__c, cpm_Opportunity_Results_Category__c, cpm_Opportunity_Results_Category2__c FROM Opportunity WHERE Id = :tender.Id];
        System.debug('opp' + opp);
        if (opp.cpm_Tax_Exempt_picklist__c == null && opp.cpm_Opportunity_Results_Category__c != null ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Tax exempt" on Opportunity record- Opportunity Results Section 1. Click on Close button to get back to Opportunity record.'));
        } 
        else if (opp.Pay_out_Handling_Fees__c == null && opp.cpm_Opportunity_Results_Category__c != null ) {	
         	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Pay out Handling Fees" on Opportunity record- Opportunity Results Section 1. Click on Close button to get back to Opportunity record.'));
        }
        else if (opp.Pay_out_Handling_Fees2__c == null && opp.cpm_Opportunity_Results_Category2__c != null ) {	
         	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Pay out Handling Fees" on Opportunity record- Opportunity Results Section 2. Click on Close button to get back to Opportunity record.'));
        }
        else if (opp.cpm_Tax_Exempt_picklist2__c == null && opp.cpm_Opportunity_Results_Category2__c != null ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Tax exempt" on Opportunity record- Opportunity Results Section 2. Click on Close button to get back to Opportunity record.'));
        } 
        
        else {
            
            GenerateSMSReports();
            
        /*    Pagereference pg =  new Pagereference('/'+tender.id);
        pg.setRedirect(true);
        return pg; */
        }
        return null;
    } 
    
  /*   public PageReference doSearch2() {
      Opportunity opp =  [SELECT cpm_Tax_Exempt_picklist2__c, Pay_out_Handling_Fees2__c, cpm_Opportunity_Results_Category__c FROM Opportunity WHERE Id = :tender.Id];
        if (opp.cpm_Tax_Exempt_picklist2__c == null && opp.cpm_Opportunity_Results_Category__c != null ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Tax exempt" on Opportunity record- Opportunity Results Section 2. Click on Close button to get back to Opportunity record.'));
        } 
        else if (opp.Pay_out_Handling_Fees2__c == null && opp.cpm_Opportunity_Results_Category2__c != null ) {	
         	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Pay out Handling Fees" on Opportunity record- Opportunity Results Section 2. Click on Close button to get back to Opportunity record.'));
        }
        else {
            GenerateSMSReports(); */
            
        /*    Pagereference pg =  new Pagereference('/'+tender.id);
        pg.setRedirect(true);
        return pg; */
     /*   }
        return null;
    } */
    
  /*  public PageReference doSearch2() {
      Opportunity opp =  [SELECT Pay_out_Handling_Fees__c FROM Opportunity WHERE Id = :tender.Id];
        if (opp.Pay_out_Handling_Fees__c == null || opp.Pay_out_Handling_Fees__c == '') {	
         	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter value for field "Pay out Handling Fees" on Opportunity record. Click on Close button to get back to Opportunity record.'));
         
        } 
    
        else {
            GenerateSMSReports();
            
            Pagereference pg =  new Pagereference('/'+tender.id);
        pg.setRedirect(true);
        return pg;
        } 
        return null;
    } */
    
   
    
   
    
    
    public PageReference GenerateSMSReports() {
        reportSuccess = false;
        for(String accountID: customers)
        {
            opportunityList = ''; 
            PageReference pdfPage = new PageReference( '/apex/ATSSMSReportOpporunity' );
            pdfPage.getParameters().put('tenderId',tender.id); 
            pdfPage.getParameters().put('accountId',accountID);
            pdfPage.getParameters().put('categoriesList',categoriesList);
            
            // pdfPage.setRedirect(false);
            // generate the pdf blob
            
            
            
            
            Blob pdfBlob;
            if (Test.IsRunningTest())
            {
                pdfBlob =Blob.valueOf('UNIT.TEST');
            }
            else
            {			
                
                try {
                    pdfBlob = pdfPage.getContent();
                } 
                catch (Exception e) 
                {
                    System.debug('This is Error'+ e);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 
                                                               'There was an error generating SMS report for customer \'' + customersMap.get(accountID) + '\'.' +
                                                               ' Please review Product and Freight data associated with the opportunity. Please ensure that Opportunity is in one of these stages: Bid Analysis, Sales Contract, Closed Won Partial or Closed Won'));
                    return null;		
                }               
                
                //pdfBlob = pdfPage.getContent();
            }
            
            
            //pdfBlob = pdfPage.getContent();            
            
            
            // create the attachment against the quote
            Attachment attachment = new Attachment(); 
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = tender.id; // the record the file is attached to
            attachment.name = 'SMS Report - ' +  customersMap.get(accountID) + '.pdf';
            attachment.IsPrivate = false;
            attachment.body = pdfBlob;
            
            //insert the attachment
            try {
                insert attachment;            
            } catch (DMLException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                return null;
            } 
            reportSuccess = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Attachment uploaded successfully. Click on Close button to get back to Opportunity record.'));
        }
        
        Pagereference pg = new PageReference('/'+tender.id+'#' + tender.id + '_RelatedNoteList_target');
        pg.setRedirect(true);
        reportSuccess = true;
        return null;
    }
    public pagereference BackMethod()
    {
        Pagereference pg =  new Pagereference('/'+tender.id);
        pg.setRedirect(true);
        return pg;
    }    
    
    
    public List<SelectOption> getCategoryItems() {
        return CategoryOptions ;
    }    
    
    public List<SelectOption> getCustomers() {
        return CustomerOptions ;
    }       
    
    
    public String[] getSelectedCategories() {
        System.debug('categories :' +categories);
        return categories;
    }    
    public void setSelectedCategories(String[] items) {
        this.categories = items;
        System.debug('items :' +items);
        for (string s : items) 
              {						                     	 
                  categoriesList += s + ',';   
              } 
        System.debug('categoriesList :' +categoriesList);
    }    
    
    public String[] getSelectedCustomers() {
        return customers;
    }    
    public void setSelectedCustomers(String[] items) {
        this.customers= items;
    }  
    
    
}