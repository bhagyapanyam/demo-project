/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EngineConfigService
History:        jschn 2019-07-24 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EngineConfigServiceTest {

    @IsTest
    static void getEngineConfig_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent = 'Engine Manufacturer';
        EPD_Engine__mdt result;

        try {
            result = new EPD_EngineConfigService().getEngineConfig(null, null);
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assert(errorMessage.contains(expectedErrorMessageContent));
    }

    @IsTest
    static void getEngineConfig_withManufacturerParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent = 'Engine Model';
        EPD_Engine__mdt result;

        try {
            result = new EPD_EngineConfigService().getEngineConfig('UnitTestData', null);
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assert(errorMessage.contains(expectedErrorMessageContent));
    }

    @IsTest
    static void getEngineConfig_withModelParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent = 'Engine Manufacturer';
        EPD_Engine__mdt result;

        try {
            result = new EPD_EngineConfigService().getEngineConfig(null, 'UnitTestData');
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assert(errorMessage.contains(expectedErrorMessageContent));
    }

    @IsTest
    static void getEngineConfig_withWrongParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent = 'Wrong';
        EPD_Engine__mdt result;

        try {
            result = new EPD_EngineConfigService().getEngineConfig('Wrong', 'Wrong');
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assert(errorMessage.contains(expectedErrorMessageContent));
    }

    @IsTest
    static void getEngineConfig_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent;
        EPD_Engine__mdt result;

        try {
            result = new EPD_EngineConfigService().getEngineConfig('UnitTestData', 'UnitTestData');
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(errorMessage, expectedErrorMessageContent);
    }

    @IsTest
    static void getConfigsMap() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String errorMessage;
        String expectedErrorMessageContent;
        Map<String, EPD_Engine__mdt> result;

        try {
            result = new EPD_EngineConfigService().getConfigsMap();
        } catch (HOG_Exception ex) {
            failFlag = true;
            errorMessage = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(errorMessage, expectedErrorMessageContent);
    }

    @IsTest
    static void getConfigMapKeyForEngine_withoutParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String result;

        try {
            result = new EPD_EngineConfigService().getConfigMapKeyForEngine(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getConfigMapKeyForEngine_withManufacturerParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String result;
        String model;
        String manufacturer = 'b';
        String expectedResult = manufacturer + '#' + model;

        try {
            result = new EPD_EngineConfigService().getConfigMapKeyForEngine(manufacturer, model);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getConfigMapKeyForEngine_withModelParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String result;
        String model = 'a';
        String manufacturer;
        String expectedResult = manufacturer + '#' + model;

        try {
            result = new EPD_EngineConfigService().getConfigMapKeyForEngine(manufacturer, model);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getConfigMapKeyForEngine_withBothParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        String result;
        String model = 'a';
        String manufacturer = 'b';
        String expectedResult = manufacturer + '#' + model;

        try {
            result = new EPD_EngineConfigService().getConfigMapKeyForEngine(manufacturer, model);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedResult, result);
    }

}