global virtual with sharing class DSP_CMS_ImageCard extends DSP_CMS_ImageCardController
{
    global override String getHTML()
    {
        return getImageCardHTML();
    }
}