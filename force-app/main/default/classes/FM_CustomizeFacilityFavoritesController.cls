/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Controller for Facility Favorites Customize Page
History:        ssd 07.15.2016 - Initial Revision
**************************************************************************************************/
public class FM_CustomizeFacilityFavoritesController {

	private FM_DestinationFavorites__c destinationFavorites;

	//Destination Facility List Variables
	public List<SelectOption> selectedDestinations {get; set;}
	public List<SelectOption> allDestinations {get; set;}
	public Set<String> savedFavoriteDestinationIdsList {get; set;}

	//Source Facility/Well Location
	private String sourceFunctionalLocationRecordId;
	public Location__c sourceWellLocation {get; private set;}
	public Facility__c sourceFacility {get; private set;}
	public String sourceFunctionalLocationName {get; private set;}

	public FM_CustomizeFacilityFavoritesController() {
		InitializeData();
		PopulateSourceFunctionalLocation();

		if(sourceFacility != null || sourceWellLocation != null) {
			PopulateDestinations();
		}
	}

	private void InitializeData() {
		this.selectedDestinations = new List<SelectOption>();
		this.allDestinations = new List<SelectOption>();
		this.savedFavoriteDestinationIdsList = new Set<String>();
	}

	private void PopulateSourceFunctionalLocation() {
		this.sourceFunctionalLocationRecordId = ApexPages.currentPage().getParameters().get('funclocId');
		List<Facility__c> facilityList = [Select Id, Name, Facility_Name_For_Fluid__c
							 			  From Facility__c 
							 			  Where Id =: sourceFunctionalLocationRecordId];
		this.sourceFacility = (facilityList != null && !facilityList.isEmpty()) ? facilityList[0] : null;

		if(this.sourceFacility == null) {
			List<Location__c> locationList = [Select Id, Name, Location_Name_for_Fluid__c
											  From Location__c
											  Where Id =: sourceFunctionalLocationRecordId];

			this.sourceWellLocation = (locationList != null && !locationList.isEmpty()) ? locationList[0] : null;
		}

		sourceFunctionalLocationName = (this.sourceFacility != null) ? 
			(this.sourceFacility.Facility_Name_For_Fluid__c != null) ? this.sourceFacility.Facility_Name_For_Fluid__c : this.sourceFacility.Name :
			(this.sourceWellLocation.Location_Name_for_Fluid__c != null) ? this.sourceWellLocation.Location_Name_for_Fluid__c : this.sourceWellLocation.Name;

	}

	private void PopulateDestinations() {
		List<FM_DestinationFavorites__c> destinationFavoritesList = (this.sourceFacility != null) ? 
															  		[Select Id, Name, DestinationIDList__c
																   	 From FM_DestinationFavorites__c
																     Where Facility__c =: this.sourceFacility.Id] :
																    (this.sourceWellLocation != null) ? 
																    [Select Id, Name, DestinationIDList__c
																     From FM_DestinationFavorites__c
																     Where Well_Location__c =: this.sourceWellLocation.Id] :
																    new List<FM_DestinationFavorites__c>();
		this.destinationFavorites = (destinationFavoritesList != null && !destinationFavoritesList.isEmpty()) ? 
								 destinationFavoritesList[0] : 
								 new FM_DestinationFavorites__c(
								 	Facility__c = (this.sourceFacility != null) ? this.sourceFacility.Id : null,
								 	Well_Location__c = (this.sourceWellLocation != null) ? this.sourceWellLocation.Id : null
								 );
		//this.savedFavoriteDestinationIdsList.addAll(((this.destinationFavorites.DestinationIDList__c != null) ? this.destinationFavorites.DestinationIDList__c : '').split(','));

		//Map<String, List<SelectOption>> destinationMap = FM_Utilities.BuildDispatchDestinationMap(this.savedFavoriteDestinationIdsList);
		Map<String, List<SelectOption>> destinationMap = (FM_Utilities.dispatchDestinationMap.get(this.sourceFunctionalLocationRecordId) != null) ? 
			FM_Utilities.dispatchDestinationMap.get(this.sourceFunctionalLocationRecordId) : 
			new Map<String, List<SelectOption>>{FM_Utilities.DESTINATION_INDEX_FAVORITE_DESTINATIONS => new List<SelectOption>(),
                FM_Utilities.DESTINATION_INDEX_ALL_DESTINATIONS => FM_Utilities.allDestinationOptions};
		selectedDestinations = RebuildDestionationList(destinationMap.get(FM_Utilities.DESTINATION_INDEX_FAVORITE_DESTINATIONS));
		allDestinations = RebuildDestionationList(destinationMap.get(FM_Utilities.DESTINATION_INDEX_ALL_DESTINATIONS));
	}

	public PageReference Save() {
		String savedFavoriteDestinationIdsText = '';
		for(SelectOption option : selectedDestinations) {
			savedFavoriteDestinationIdsText += option.getValue() + ',';
		}
		savedFavoriteDestinationIdsText.removeEnd(',');

		this.destinationFavorites.DestinationIDList__c = savedFavoriteDestinationIdsText;
		
		try {
			upsert destinationFavorites;
		} catch (DmlException ex) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, ex.getMessage()));
			return null;
		}

		return Exit();
	}

	public PageReference Exit() {
		PageReference pageRef = Page.FM_DispatchViewNew;
		return pageRef;
	}

	private List<SelectOption> RebuildDestionationList(List<SelectOption> pDestinationList) {
		List<SelectOption> rebuiltList = new List<SelectOption>();

		for(SelectOption option : pDestinationList) {
			rebuiltList.add(new SelectOption(option.getValue().split('\\|')[0], option.getLabel()));
		}

		return rebuiltList;
	}

}