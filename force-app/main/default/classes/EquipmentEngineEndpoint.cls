/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint for requests related to Equipment Engine SObject
Test Class:     EquipmentEngineEndpointTest
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
public with sharing class EquipmentEngineEndpoint {

    /**
     * Updates (or Creates) Engine record based on Equipment record.
     * Only supported Equipment Object Type can trigger this action.
     *
     * @param equipmentId
     *
     * @return HOG_SimpleResponse
     */
    @AuraEnabled
    public static HOG_SimpleResponse refreshEngineData(Id equipmentId) {
        return new EquipmentEngineEndpointHandler().refreshEngineData(equipmentId);
    }

}