@isTest
private class PCR_ViewPageControllerXTest {
    testmethod static void defaultMethod() {
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id, Contractor__c = uid,
                                                        Change_Status__c = 'Verification' );
        insert chq;
        
        Impact_Assessor__c impact = new Impact_Assessor__c( Change_Request__c = chq.Id, Name = 'Test',
                                        ChangeRequestImpactedFuntionAssessorKey__c = chq.Id, 
                                        Assessor__c = uid, SPA__c = uid ); 
        insert impact;
        
        Project__c projectVal = new Project__c( Name = 'Test', VP__c = uid, Project_Manager__c = uid );
        insert projectVal;
        
        ApexPages.StandardController sc = new ApexPages.StandardController( chq );
        test.startTest();
            PCR_ViewPageControllerX controller = new PCR_ViewPageControllerX( sc );
            controller.getPcrFieldSets();
            controller.getPcrEditFieldSets();
            controller.getImageName();
            controller.edit();
            controller.getGroups();
            controller.next();
            controller.assessment();
            controller.functional();
            controller.functional2();
            controller.impact();
            controller.spaReview();
            controller.addReviewer();
        test.stopTest();
    }
}