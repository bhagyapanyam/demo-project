/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO interface that is used for purpose of accessing data for Manage Assignments purposes
                MA stands for Manage Assignments
History:        jschn 20/11/2019 - Created.
*************************************************************************************************/
public interface VTT_WOA_MA_ActivityDAO {

    List<Work_Order_Activity__c> getRecordsForAssignments(List<Id> woaIds);

}