/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_PlannerGroupNotificationTypeSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class HOG_PlannerGroupNotificationTypeSlcrTest {

    @IsTest
    static void getNotificationTypesForRoleTypes_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<HOG_Planner_Group_Notification_Type__c> records;

        Test.startTest();
        try {
            records = new HOG_PlannerGroupNotificationTypeSelector()
                    .getNotificationTypesForRoleTypes(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(null, records);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getNotificationTypesForRoleTypes_withEmptyParam() {
        Integer expectedRecordCount = 0;
        Account vendor = [SELECT Id FROM Account LIMIT 1];
        Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id, UserInfo.getUserId());
        HOG_Planner_Group__c plannerGroup = VTT_TestData.createHogPlannerGroup('TestPlannerGroup');
        VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, 'TestOrderType', 'MECH*','Tradesman Lead', tradesman.Id);

        Test.startTest();
        List<HOG_Planner_Group_Notification_Type__c> records = new HOG_PlannerGroupNotificationTypeSelector()
                .getNotificationTypesForRoleTypes(new List<String>());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getNotificationTypesForRoleTypes_withWrongParam() {
        Integer expectedRecordCount = 0;
        Account vendor = [SELECT Id FROM Account LIMIT 1];
        Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id, UserInfo.getUserId());
        HOG_Planner_Group__c plannerGroup = VTT_TestData.createHogPlannerGroup('TestPlannerGroup');
        VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, 'TestOrderType', 'MECH*','Tradesman Lead', tradesman.Id);

        Test.startTest();
        List<HOG_Planner_Group_Notification_Type__c> records = new HOG_PlannerGroupNotificationTypeSelector()
                .getNotificationTypesForRoleTypes( new List<String> { 'Wrong Role Type' } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getNotificationTypesForRoleTypes_withProperParam() {
        Integer expectedRecordCount = 1;
        Account vendor = [SELECT Id FROM Account LIMIT 1];
        Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id, UserInfo.getUserId());
        HOG_Planner_Group__c plannerGroup = VTT_TestData.createHogPlannerGroup('TestPlannerGroup');
        HOG_Planner_Group_Notification_Type__c plannerGroupNotificationType = VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, 'TestOrderType', 'MECH*','Tradesman Lead', tradesman.Id);

        Test.startTest();
        List<HOG_Planner_Group_Notification_Type__c> records = new HOG_PlannerGroupNotificationTypeSelector()
                .getNotificationTypesForRoleTypes( new List<String> { plannerGroupNotificationType.Role_Type__c } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getNotificationTypesForRoleTypes_withProperParam2() {
        Integer expectedRecordCount = 2;
        Account vendor = [SELECT Id FROM Account LIMIT 1];
        Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id, UserInfo.getUserId());
        HOG_Planner_Group__c plannerGroup = VTT_TestData.createHogPlannerGroup('TestPlannerGroup');
        HOG_Planner_Group_Notification_Type__c plannerGroupNotificationType1 = VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, 'TestOrderType', 'MECH*','Tradesman Lead', tradesman.Id);
        HOG_Planner_Group_Notification_Type__c plannerGroupNotificationType2 = VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, 'TestOrderType', 'MECH*','Field Scheduler', tradesman.Id);

        Test.startTest();
        List<HOG_Planner_Group_Notification_Type__c> records = new HOG_PlannerGroupNotificationTypeSelector()
                .getNotificationTypesForRoleTypes( new List<String> { plannerGroupNotificationType1.Role_Type__c, plannerGroupNotificationType2.Role_Type__c } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @TestSetup
    static void prepareData() {
        VTT_TestData.createVendorAccount('TestVendor1');
    }

}