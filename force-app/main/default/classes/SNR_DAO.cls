/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for Service Request Notifications

History:        mbrimus 2020-01-08 - Created.
*************************************************************************************************/
public interface SNR_DAO {
    /**
     * Returns Work Order types for SNR picklist (Old SNR Controller logic - getServiceMainNames)
     *
     * @return
     */
    List<AggregateResult> getServiceCategories();

    /**
     * Returns HOG_Notification_Type__c for SNR picklist (Old SNR Controller logic - getServiceTypeNames)
     *
     * @return
     */
    List<AggregateResult> getServiceTypesNoServiceRigProgram(String serviceCategoryId, Boolean isCommunityUser);

    /**
     * Returns data for notification type by Id
     *
     * @param Id
     *
     * @return
     */
    List<HOG_Notification_Type__c> getNotificationTypeById(String Id);

    /**
    * Returns HOG_User_Status__c for SNR picklist (Old SNR Controller logic - getServiceActivityNames)
    *
    * @return
    */
    List<AggregateResult> getServiceActivityNames(String serviceCategoryId, String serviceType);

    /**
     * Returns Notification Type Priorities (Old SNR Controller logic - getServicePriorityNames)
     *
     * @param serviceType
     *
     * @return
     */
    List<HOG_Notification_Type_Priority__c> getServicePriorities(String serviceType);

    /**
     * Returns HOG Service Required options for given Service Category - (Old SNR Controller logic - getServiceRequiredNames)
     *
     * @param serviceCategoryId
     * @param serviceType
     * @param serviceActivity
     * @param wellLocationStatus
     * @return
     */
    List<AggregateResult> getServiceRequired(String serviceCategoryId, String serviceType, String serviceActivity, Boolean wellLocationStatus);

    /**
     * Return Service Required based on selected IDs
     *
     * @param serviceCategoryId
     * @param serviceTypeId
     * @param serviceActivityId
     * @param serviceRequiredId
     * @param wellLocationStatusProducing
     *
     * @return
     */
    List<AggregateResult> getServiceSpecifics(
            String serviceCategoryId,
            String serviceTypeId,
            String serviceActivityId,
            String serviceRequiredId,
            Boolean wellLocationStatusProducing);

    /**
     * Return Service Required Data
     * @param serviceRequiredId
     *
     * @return
     */
    List<HOG_Service_Required__c> getOneServiceRequired(String serviceRequiredId);

    /**
     * Return single well location
     *
     * @param locationId
     *
     * @return
     */
    List<Location__c> getOneWellLocation(String locationId);

    /**
     * Return single Facility__c
     *
     * @param facilityId
     *
     * @return
     */
    List<Facility__c> getOneFacility(String facilityId);

    /**
     * Return single Equipment__c
     *
     * @param equipmentId
     *
     * @return
     */
    List<Equipment__c> getOneEquipment(String equipmentId);

    /**
    * Return single Well_Event__c
    *
    * @param wellEventId
    *
    * @return
    */
    List<Well_Event__c> getOneWellEvent(String wellEventId);

    /**
    * Return single System__c
    *
    * @param systemId
    *
    * @return
    */
    List<System__c> getOneSystem(String systemId);

    /**
    * Return single Sub_System__c
    *
    * @param subSystemId
    *
    * @return
    */
    List<Sub_System__c> getOneSubSystem(String subSystemId);

    /**
    * Return single Yard__c
    *
    * @param yardId
    *
    * @return
    */
    List<Yard__c> getOneYard(String yardId);

    /**
    * Return single Functional_Equipment_Level__c
    *
    * @param felId
    *
    * @return
    */
    List<Functional_Equipment_Level__c> getFEL(String felId);
}