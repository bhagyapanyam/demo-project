/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VG_AlertEndpoint
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class VG_AlertEndpointTest {

    @IsTest
    static void getProductionEngineer() {
        Id result;
        Id expectedResult = null;

        result = VG_AlertEndpoint.getProductionEngineer(null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidLocation() {
        Boolean result;
        Boolean expectedResult;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        try {
            result = VG_AlertEndpoint.isValidLocation(null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidUser() {
        User usr = HOG_VentGas_TestData.createUser();
        Boolean result;
        Boolean expectedResult = false;

        System.runAs(usr) {
            result = VG_AlertEndpoint.isValidUser();
        }

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles() {
        Boolean result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        try {
            result = VG_AlertEndpoint.saveFiles(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
    }

}