public class ATSCustomEmailControllerOpp{
    public ID OpportunityID {get;set;} 
    List<String> categoriesList = new List<String>();
    List<String> categoriesSet = new List<String>();
    private ATSTenderWrapper ATSTender;
    
    public ATSTenderWrapper getATSTender()
    {       
       if(OpportunityID!= null)
       {
           System.debug('tu som');
           ATSTender = new ATSTenderWrapper(OpportunityID);
       }
       return ATSTender;
    
    }
    
 // Wrapper class
 
    public class ATSTenderWrapper 
    {
        public ID parentTenderID {get;set;}
        public List<ATSOpportunityWrapper> Opportunities {get; set;}
        public String Destination {get;set;}
                     
     
        public ATSTenderWrapper (ID OpportunityID)
        {
           
            Opportunities = new List<ATSOpportunityWrapper>();
            
            System.debug('opp' + OpportunityID);
            list<Opportunity>lstopp = new list<Opportunity>([SELECT id, Name, cpm_Destination_City_Province__c, RecordType.Name
                       FROM Opportunity
                       WHERE id= :OpportunityID ]);
        
            
	            for(Opportunity atsOpportunity : lstopp){
                     Destination = atsOpportunity.cpm_Destination_City_Province__c; 
	                 Opportunities.add( new ATSOpportunityWrapper(atsOpportunity.id, atsOpportunity.RecordType.Name ));
	            }     
            System.debug('Opportunity List'+ lstopp);
        }
    }   

    public class ATSProductWrapper   
    {
        public String Name {get;set;}
        public String Unit{get;set;}        
        public Decimal Qty{get;set;}  
        public String ProductFamily {get;set;}
        public ATSProductWrapper(String pName, String pUnit, Decimal pQty, String prdFamily)
        {
            Name=pName;
            Unit=pUnit;
            Qty=pQty;
            ProductFamily = prdFamily;
        }   
    
    }

        public class ATSSupplierWrapper   
    {
        public String supplierName {get;set;}
        public String productFamily {get;set;}
        public ATSSupplierWrapper(String suppName, String prdFamily)
        {
            supplierName=suppName;
            ProductFamily = prdFamily;
        }   
    
    }

        public class ATSFreightCommentWrapper   
    {
        public String productFamily {get;set;}
        public String freightComment {get;set;}
        public ATSFreightCommentWrapper(String prdFamily, String ftComment)
        {
            productFamily = prdFamily;
			freightComment = ftComment;
        }   
    
    }    
    
    public class ATSOpportunityWrapper    
    {
        public List<ATSProductWrapper> Products {get;set;}
        public List<ATSSupplierWrapper> Suppliers  {get;set;} 
        public List<ATSFreightCommentWrapper> freightCommentList  {get;set;} 
//        public List<String> ProductFamily  {get;set;}
        public List<String> ProductCategoriesList  {get;set;}  
        public String ProductCategory {get;set;}
        public String FreightComments {get; set;}
        public String OpportunityName {get; set;}
        public Date SalesContractDueDate {get; set;}
               
        public ATSOpportunityWrapper(ID pOpportunityID, String pProduct)
        {
       
             System.debug('in ATS opp Wrapper constructor');
             Products = new List<ATSProductWrapper>();
             Suppliers  = new List<ATSSupplierWrapper>(); 
             ProductCategoriesList = new List<String>();
             freightCommentList = new List<ATSFreightCommentWrapper>();
            
             List<OpportunityLineItem> opportunityLinkeItems = [Select 
                                         PricebookEntry.Product2.id, 
                                         PricebookEntry.Product2.Name, 
                                         cpm_Product_Family__c,                       
                                         Unit__c,
                                         Quantity
                                         From OpportunityLineItem WHERE Opportunity.ID=:pOpportunityID  order by cpm_Product_Family__c ];
            
             if (opportunityLinkeItems != null && opportunityLinkeItems.size() > 0)
             {
	             for(OpportunityLineItem lineItem : opportunityLinkeItems ){ 
	                 Products.add( 
	                                 new ATSProductWrapper(
	                                     lineItem.PricebookEntry.Product2.Name,
                                         lineItem.Unit__c,
	                                     lineItem.Quantity,
                                         lineItem.cpm_Product_Family__c
	                             )
	                             );
             		  ProductCategoriesList.add(lineItem.cpm_Product_Family__c);

	             }                         
             }            	 
			
             // remove duplicates from product categories list
             List<String> noDupes = new List<String>(new Set<String>(ProductCategoriesList));           
             ProductCategoriesList = noDupes;
            
             List<ATS_Freight__c> atsFreights = [select id,             
                                                 cpm_Product_Type__c,
                                                 cpm_HuskySupplier1_ats_pricing__r.Name, 
                                                 cpm_HuskySupplier2_ats_pricing__r.Name,
                                                 cpm_HuskySupplier3_ats_pricing__r.Name,
                                                 cpm_Competitor1_ats_pricing__r.Name,
                                                 cpm_Competitor2_ats_pricing__r.Name,
                                                 cpm_Competitor3_ats_pricing__r.Name,
                                                 cpm_Competitor4_ats_pricing__r.Name
                                                 from ATS_Freight__c 
                                                 where ATS_Freight__c  =:pOpportunityID ];
      
             List<ATS_Freight__c> atsFreightDetails = [select id,
                                            cpm_Product_Type__c,
             								Freight_Comments__c
             								from ATS_Freight__c
             								where ATS_Freight__c = :pOpportunityID];
            
             if (atsFreightDetails != null && atsFreightDetails.size() > 0)                        
            	for(ATS_Freight__c atsFreightDetail : atsFreightDetails ){  
					freightCommentList.add(new ATSFreightCommentWrapper( atsFreightDetail.cpm_Product_Type__c, atsFreightDetail.Freight_Comments__c));
                 }
            
             Opportunity opp = [select id, Name, Sales_Contract_Due_Date__c
             								from Opportunity
             								where Opportunity.Id = :pOpportunityID];
             				
             if (opp != null)
             {								
			     OpportunityName = opp.Name;  
                 SalesContractDueDate = opp.Sales_Contract_Due_Date__c;
             }
            
             if (atsFreights != null && atsFreights.size() > 0)
             {
                 System.debug('nas supplier'+ atsFreights);
	             for(ATS_Freight__c atsFreight : atsFreights ){           
	                
	                //lets add suppliers
	                if(atsFreight.cpm_HuskySupplier1_ats_pricing__c != null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_HuskySupplier1_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));
                    }
	                if(atsFreight.cpm_HuskySupplier2_ats_pricing__c != null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_HuskySupplier2_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));

                    }
                     if(atsFreight.cpm_HuskySupplier3_ats_pricing__c != null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_HuskySupplier3_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));
                                      
	                }                
	                if(atsFreight.cpm_Competitor1_ats_pricing__c!= null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_Competitor1_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));	                    
	                }  
	                if(atsFreight.cpm_Competitor2_ats_pricing__c!= null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_Competitor2_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));	                    	                    
	                }  
	                if(atsFreight.cpm_Competitor3_ats_pricing__c!= null)
	                {
                        Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_Competitor3_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));	                    	                    
	                }  
	                if(atsFreight.cpm_Competitor4_ats_pricing__c!= null)
	                {
	                    Suppliers.add(new ATSSupplierWrapper(atsFreight.cpm_Competitor4_ats_pricing__r.Name, atsFreight.cpm_Product_Type__c));	  
	                }                 
	             }
             }
                                 
        System.debug('Suppliers'+Suppliers);
        }
    }
}