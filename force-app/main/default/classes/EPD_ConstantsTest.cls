/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test to test methods in EPD_Constants
History:        jschn 2019-07-25 - Created.
*************************************************************************************************/
@IsTest
private class EPD_ConstantsTest {

    @IsTest
    static void SENDER_ID_withoutSettings() {

        Test.startTest();
        Id senderId = EPD_Constants.SENDER_ID;
        Test.stopTest();

        System.assert(String.isBlank(senderId));
    }

    @IsTest
    static void SENDER_ID_withWrongSettings() {
        HOG_SObjectFactory.createSObject(
                new HOG_Engine_Performance_Data__c(),
                EPD_FieldDefaultsWrong.CLASS_NAME,
                true
        );

        Test.startTest();
        Id senderId = EPD_Constants.SENDER_ID;
        Test.stopTest();

        System.assert(String.isBlank(senderId));
    }

    @IsTest
    static void SENDER_ID_withProperSettings() {
        HOG_SObjectFactory.createSObject(
                new HOG_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                true
        );

        Test.startTest();
        Id senderId = EPD_Constants.SENDER_ID;
        Test.stopTest();

        System.assert(String.isNotBlank(senderId));
    }

}