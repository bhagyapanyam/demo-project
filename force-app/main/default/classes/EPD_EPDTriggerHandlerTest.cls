/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EPDTriggerHandler
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EPDTriggerHandlerTest {

    @IsTest
    static void handleAfterInsert_withoutRecords() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<EPD_Engine_Performance_Data__c> epdRecords;

        try {
            new EPD_EPDTriggerHandler().handleAfterInsert(epdRecords);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void handleAfterInsert_withEmptyList() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Engine_Performance_Data__c> epdRecords = new List<EPD_Engine_Performance_Data__c>();

        try {
            new EPD_EPDTriggerHandler().handleAfterInsert(epdRecords);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void handleAfterInsert_updateSuccess() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_Engine_Performance_Data__c> epdRecords = new List<EPD_Engine_Performance_Data__c>{
                EPD_TestData.createSingleEPD()
        };
        EPD_EPDTriggerHandler.skipTrigger = false;
        Equipment_Engine__c engineBeforeAction = [SELECT Last_EPD_Check__c FROM Equipment_Engine__c LIMIT 1];

        try {
            new EPD_EPDTriggerHandler().handleAfterInsert(epdRecords);
        } catch (Exception ex) {
            failFlag = true;
        }

        Equipment_Engine__c engineAfterAction = [SELECT Last_EPD_Check__c FROM Equipment_Engine__c LIMIT 1];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, engineBeforeAction.Last_EPD_Check__c);
        System.assertNotEquals(engineBeforeAction.Last_EPD_Check__c, engineAfterAction.Last_EPD_Check__c);
    }

}