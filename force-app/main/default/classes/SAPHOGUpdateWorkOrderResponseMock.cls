@isTest
global with sharing class SAPHOGUpdateWorkOrderResponseMock implements WebServiceMock{
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {       
        
        SAPHOGWorkOrderServices.UpdateWorkOrderResponse responseElement = new SAPHOGWorkOrderServices.UpdateWorkOrderResponse();
        responseElement.Message = 'SAPHOGWorkOrderServices.UpdateWorkOrderResponse Test Message';
        responseElement.type_x = True;
        responseElement.WorkOrder = new SAPHOGWorkOrderServices.WorkOrder();
        responseElement.WorkOrder.WorkOrderNumber = '7521256';
        
        response.put('response_x', responseElement);
    }
}