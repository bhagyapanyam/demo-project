/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class which provides flag for escaping from recursion in triggers.
Test Class:     Location_TriggerHelperTest
History:        jschn 19. 9. 2018 - Created.
**************************************************************************************************/
global with sharing class HOG_TriggerRecursionPreventController {

    global static Boolean flag = true;

}