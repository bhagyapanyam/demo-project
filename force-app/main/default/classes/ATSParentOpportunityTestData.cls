public class ATSParentOpportunityTestData {
    
    public static List<ATS_Parent_Opportunity__c> createTender(Integer numberOfRecords) {
        
        List<ATS_Parent_Opportunity__c> tenderList = new List<ATS_Parent_Opportunity__c>();
        User tempUser;
        if(Test.isRunningTest()) {
            System.runAs(new User(Id = UserInfo.getUserId())) {
				tempUser = UserTestData.createTestUser();
        		insert tempUser;                
            }
        }
        
        for(Integer i = 0; i < numberOfRecords; i++) {
            ATS_Parent_Opportunity__c tender = new ATS_Parent_Opportunity__c();
            tender.Tender__c= 'Tender Name ' + i;
            tender.Bid_Due_Date_Time__c= Date.today();
            tender.Freight_Due_Date_Time__c = DateTime.now();
            tender.Marketer__c = tempUser.Id;
            tender.Destination_City_Province__c = 'Test Destination';
            tenderList.add(tender);
        }
        
        insert tenderList;
        return tenderList; 
    }
    
public static Opportunity createTender(Id accountId) {
        User tempUser;
        if(Test.isRunningTest()) {
            System.runAs(new User(Id = UserInfo.getUserId())) {
				tempUser = UserTestData.createTestUser();
        		insert tempUser;                
            }
        }
    	
    	    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
            Opportunity tender = new Opportunity();
            tender.Name = 'NAME' + 'ACCOUNT' + 'RECORD TYPE';    		
            tender.cpm_Bid_Due_Date_Time__c = Date.today();
            tender.cpm_Freight_Due_Date_Time__c = DateTime.now();
            tender.Marketer__c = tempUser.Id;
            tender.cpm_Destination_City_Province__c = 'Test Destination';
    		tender.AccountId =  accountId;
    		
System.debug('ATSParentOpportunityTestData - AccountId: ' + accountId);    
    		//-->		
    			tender.StageName = 'Closed Won';
				tender.CloseDate = Date.today()+2; 
    			tender.cpm_Product_Category__c = 'Emulsion';
    			tender.cpm_Start_Date__c =  Date.today();
    			tender.cpm_Price_Valid_From__c = Date.today();
                tender.cpm_Price_Valid_To__c = Date.today();
                tender.cpm_Acceptance_Deadline__c = Date.today();
    			tender.recordTypeId = recordTypeId;



        return tender; 
    }    
}