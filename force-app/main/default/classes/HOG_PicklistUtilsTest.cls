/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_PicklistUtils class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_PicklistUtilsTest {

    @IsTest
    static void simpleWrap_withoutParams() {
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = new HOG_PicklistUtils().simpleWrap(null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void simpleWrap_withEmptyList() {
        List<SObject> items = new List<SObject>();
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = new HOG_PicklistUtils().simpleWrap(items, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void simpleWrap_withItemInList() {
        List<SObject> items = new List<SObject>{
                new Account(Name = 'Test')
        };
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = new HOG_PicklistUtils().simpleWrap(items, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void simpleWrap_withValueFieldParam() {
        List<SObject> items = new List<SObject>{
                new Account(Name = 'Test')
        };
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = new HOG_PicklistUtils().simpleWrap(items, 'Name', null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void simpleWrap_withAllParams() {
        List<SObject> items = new List<SObject>{
                new Account(Name = 'Test')
        };
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = new HOG_PicklistUtils().simpleWrap(items, 'Name', 'Name');
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals('Test', result.get(0).label);
        System.assertEquals('Test', result.get(0).value);
    }

}