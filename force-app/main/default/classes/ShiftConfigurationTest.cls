@isTest
public with sharing class ShiftConfigurationTest {
	
    public static testMethod void testValidateShift(){
    	// list of shift configuration records
    	List<Shift_Configuration__c> shiftList = new List<Shift_Configuration__c>();
    	
    	shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 1', '600', '1800', true, false, false, false, false, false, true, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 2', '600', '1800', false, true, false, false, false, true, false, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 3', '600', '1800', false, false, true, false, true, false, false, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 4', '600', '1800', false, false, false, true, false, false, false, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 5', '1800', '600', true, false, false, false, false, false, true, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 6', '1900', '200', false, false, true, false, false, false, false, true));
                                                 
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 7', '1800', '2000', false, false, false, true, false, false, false, true)); 	
        
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 8', '700', '700', true, false, false, true, true, true, false, true));
                                                 
		shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 9', '300', '700', true, false, false, false, false, false, false, true));
                                                 
		shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 10', '1700', '2000', true, false, false, false, false, false, false, true));
		
		shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 11', '100', '100', true, false, false, false, false, false, false, true));
        
        shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 12', '1800', '1800', false, true, true, false, false, false, false, true));
        
		shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 13', '2400', '2400', true, false, true, false, false, false, false, true));
        
		shiftList.add(ShiftConfigurationTestData.createShiftConfiguration('Shift 14', '0000', '0000', true, false, true, false, false, false, false, true));                                                 
        
        Test.startTest();
        try{
            insert shiftList;
        } catch (Exception e) {
            System.assertEquals(e.getMessage().contains('This record has overlapping time with existing record.'), true);
        }

        try{
            shiftList = [SELECT ID FROM Shift_Configuration__c];
            System.assertEquals(0, shiftList.size());
        } catch (Exception e) {
            System.debug(LoggingLevel.Error, e.getMessage());
        }
        
        Test.stopTest();
    }
}