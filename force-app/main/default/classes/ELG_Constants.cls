/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    XXX
Test Class:     ELG_ListView_EndpointTest
History:        mbrim 2019-05-31 - Created.
*************************************************************************************************/
public with sharing class ELG_Constants {
	// Used in response for initial state
	public static final String USER_SETTINGS_KEY = 'USERSETTINGS';
	public static final String AVAILABLE_POSTS_KEY = 'AVAILABLEPOSTS';
	public static final String INCOMING_HANDOVER_KEY = 'INCOMINGHANDOVER';
	public static final String IS_SHIFTENGINEER_KEY = 'ISSHIFTENGINEER';
	public static final String PRIMARYONSHIFT_KEY = 'PRIMARYONSHIFT';

	// Status of shift
	public static final String SHIFT_NEW = 'New';
	public static final String SHIFT_HANDOVER_IN_PROGRESS = 'Handover in progress';
	public static final String SHIFT_HANDOVER_COMPLETE = 'Complete';

	// Handover status
	public static final String HANDOVER_NEW = 'New';
	public static final String HANDOVER_ACCEPTED = 'Accepted';
	public static final String HANDOVER_IN_PROGRESS = 'In Progress';

	// Task status
	public static final String NO_TASK_FOUND = 'There are no Tasks based on selected criteria.';
	public static final String TASK_ACTIVE = 'Active';
	public static final String TASK_COMPLETED = 'Completed';
	public static final String TASK_CANCELLED = 'Cancelled';

	//GENERIC RECORDTYPE FOR LOG
	public static final String GENERIC_LOG_ENTRY_RECORDTYPE = 'Generic';

	//Reviewer Permission Set
	public static final String REVIEWER_PERMISSION_SET = 'ELG_Review_Acknowledge';

	//Handover list view messages
	public static final String NO_HANDOVER_FOUND = 'There are no Handovers based on selected criteria.';
	public static final String SHIFT_ENGINEER_REVIEW = 'Shift Engineer';
	public static final String STEAM_CHIEF_REVIEW = 'Steam Chief';
	public static final String SHIFT_LEAD_REVIEW = 'Shift Lead';
	public static final String SUPERVISOR_REVIEW = 'Sr. Supervisor';

	public static String maximumQueryLimit(String labelName, Integer numberOfRecords) {
		String MAXIMUM_LIMIT_MSG = 'Maximum limit to show is 2050 {0} and based on selection there were found {1} Handover records. Please, narrow down your selection.';
		String msg = String.format(MAXIMUM_LIMIT_MSG, new List<String>{
				String.valueOf(labelName),
				String.valueOf(numberOfRecords)
		});
		return msg;
	}
}