/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentMissingFormEdit/EquipmentMissingFormView page

Test Class:    EquipmentMissingFormTest
History:       28-Feb-17 Miro Zelina - included Well Events into Equipment Missing Form Request
               03-July-17 Miro Zelina - included System & Sub-System into Equipment Missing Form Request
               04.09.17    Marcel Brimus - added FEL and Yard 
               25-Oct-2017 Miro Zelina - added check for user who can process/close equipment form
               03-Aug-2018 Miro Zelina - added check for not populated FLOC id (isEmptyFloc) [W-001180]
               16-Nov-18 	Maros Grajcar - Sprint - Equipment Transfer Release 1
							- added Rejected button to page
               				- added popup window on Rejected button
               				- added new field to the object
               				- added logic to get email from Planner Group
				08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude - Email Service Enhancement R
------------------------------------------------------------*/
public with sharing class EquipmentMissingFormControllerX {


	public Equipment_Missing_Form__c form { get; set; }
	private EquipmentFormService addEquipmentService;
	public Boolean displayPopup { get; set; }
	private String locId;
	private String facilityId;
	private String eventId;
	private String sysId;
	private String subSysId;
	private String felId;
	private String yardId;
	private ApexPages.StandardController controller;
	public String formId { get; set; }
	public Attachment attachment { get; set; }
	public Attachment serialPlate { get; set; }
	public ContentDocumentLink serialPlateFile { get; set; }
	public ContentDocumentLink attachmentFile { get; set; }
	public String attId { get; set; }
	public String attFileId { get; set; }
	public EquipmentUtilities.UserPermissions userPermissions;

	public EquipmentMissingFormControllerX(ApexPages.StandardController std) {
		if (!Test.isRunningTest()) {
			//jschn 08-Jul-2019 - added Function_Location__c
			std.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Comments__c', 'P_ID__c', 'Part_of_Package__c', 'Manufacturer__c', 'Model_Number__c', 'Manufacturer_Serial_No__c', 'Tag_Number__c', 'Location__c', 'Serial_plate_of_asset__c', 'Attachment__c', 'Location__r.Route__r.Route_Number__c', 'Location__r.Planner_Group__c', 'Location__r.Functional_Location__c', 'Well_Event__c', 'Well_Event__r.Route__r.Route_Number__c', 'Well_Event__r.Planner_Group__c', 'Well_Event__r.Functional_Location__c', 'System__c', 'System__r.Route__r.Route_Number__c', 'System__r.Planner_Group__c', 'System__r.Functional_Location__c', 'Sub_System__c', 'Sub_System__r.Route__r.Route_Number__c', 'Sub_System__r.Planner_Group__c', 'Sub_System__r.Functional_Location__c', 'Functional_Equipment_Level__c', 'Functional_Equipment_Level__r.Route__r.Route_Number__c', 'Functional_Equipment_Level__r.Planner_Group__c', 'Functional_Equipment_Level__r.Functional_Location__c', 'Yard__c', 'Yard__r.Route__r.Route_Number__c', 'Yard__r.Planner_Group__c', 'Yard__r.Functional_Location__c', 'CreatedById', 'CreatedDate', 'LastModifiedDate', 'LastModifiedById', 'Description__c', 'Facility__c', 'Facility__r.Plant_Section__r.Route_Number__c', 'Facility__r.Planner_Group__c', 'Reason__c', 'Facility__r.Functional_Location__c', 'Expiration_Date__c', 'Regulatory_Equipment__c', 'Safety_Critical_Equipment__c', 'Send_Email_Notification__c', 'CreatedById', 'Location_Label__c'});
		}
		controller = std;
		form = (Equipment_Missing_Form__c) std.getRecord();
		System.debug('equipment missing form : ' + form);
		attachment = new Attachment();
		serialPlate = new Attachment();
		serialPlateFile = new ContentDocumentLink();
		attachmentFile = new ContentDocumentLink();
		userPermissions = new EquipmentUtilities.UserPermissions();
		addEquipmentService = new EquipmentFormService();

		if (form.Id == null) {
			HOG_Equipment_Request_Configuration__c settings = HOG_Equipment_Request_Configuration__c.getInstance();
			Date dT = Date.today();
			Date expiredDate = (Date.newInstance(dT.year(), dT.month(), dT.day())) + Integer.valueOf(settings.Equipment_Request_Expiration_Date_Days__c);
			form.Expiration_Date__c = expiredDate;
		}
		//get location or facility Id from Url parameters
		locId = ApexPages.currentPage().getParameters().get('locId');
		facilityId = ApexPages.currentPage().getParameters().get('facId');
		eventId = ApexPages.currentPage().getParameters().get('eventId');
		sysId = ApexPages.currentPage().getParameters().get('sysId');
		subSysId = ApexPages.currentPage().getParameters().get('subSysId');
		felId = ApexPages.currentPage().getParameters().get('felId');
		yardId = ApexPages.currentPage().getParameters().get('yardId');

		//User have to new a record from Equipment detail view
		if (form.Id == null && (locId != null || facilityId != null || eventId != null || sysId != null || subSysId != null || felId != null || yardId != null)) //creating a new form
		{
			setDefault();
		} else {
			getAttachmentPhoto();
		}

	}
	private void getAttachmentPhoto() {
		List<Attachment> att = [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :form.Id];
		if (att.size() > 0) {
			for (Attachment a : att) {
				if (a.Id == form.Serial_plate_of_asset__c) {
					serialPlate = a;
				} else if (a.Id == form.Attachment__c) {
					attachment = a;
				}
			}
		}

		System.debug('Attachments ' + att);

		if (form.Id != null) {

			List<ContentDocumentLink> files = [SELECT Id, ContentDocument.Title, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :form.Id];

			if (files.size() > 0) {
				for (ContentDocumentLink cdl : files) {
					if (cdl.ContentDocumentId == form.Serial_plate_of_asset__c) {
						serialPlateFile = cdl;
					} else if (cdl.ContentDocumentId == form.Attachment__c) {
						attachmentFile = cdl;
					}
				}
			}
			System.debug('Files ' + files);
			System.debug('serilaplatefile ' + serialPlateFile);
			System.debug('serilaplatefile id ' + serialPlateFile.Id);
		}

	}

	private void setDefault() {
		//if location Id is in Url, then query location record
		if (locId != null) {
			Location__c location = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM Location__c
					WHERE Id = :locId
			];

			form.Location__c = locId;
			form.Location__r = location;
			form.Planner_Group__c = location.Planner_Group__c;
			form.AMU_Name__c = location.Operating_Field_AMU__r.Name;
		}
		//if facility Id is in Url, then query facility record
		else if (facilityId != null) {
			Facility__c facility = [
					SELECT Id, Name, Plant_Section__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM Facility__c
					WHERE Id = :facilityId
			];

			form.Facility__c = facilityId;
			form.Facility__r = facility;
			form.Planner_Group__c = facility.Planner_Group__c;
			form.AMU_Name__c = facility.Operating_Field_AMU__r.Name;
		}
		//if well event Id is in Url, then query well event record
		else if (eventId != null) {
			Well_Event__c wellEvent = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Well_ID__r.Operating_Field_AMU__r.Name
					FROM Well_Event__c
					WHERE Id = :eventId
			];

			form.Well_Event__c = eventId;
			form.Well_Event__r = wellEvent;
			form.Planner_Group__c = wellEvent.Planner_Group__c;
			form.AMU_Name__c = wellEvent.Well_ID__r.Operating_Field_AMU__r.Name;
		}
		//if System Id is in Url, then query System record
		else if (sysId != null) {
			System__c syst = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM System__c
					WHERE Id = :sysId
			];

			form.System__c = sysId;
			form.System__r = syst;
			form.Planner_Group__c = syst.Planner_Group__c;
			form.AMU_Name__c = syst.Operating_Field_AMU__r.Name;
		}
		//if Sub-System Id is in Url, then query Sub-System record
		else if (subSysId != null) {
			Sub_System__c subSyst = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM Sub_System__c
					WHERE Id = :subSysId
			];

			form.Sub_System__c = subSysId;
			form.Sub_System__r = subSyst;
			form.Planner_Group__c = subSyst.Planner_Group__c;
			form.AMU_Name__c = subSyst.Operating_Field_AMU__r.Name;
		}
		//if FEL Id is in Url, then query FEL record
		else if (felId != null) {
			Functional_Equipment_Level__c fel = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM Functional_Equipment_Level__c
					WHERE Id = :felId
			];

			form.Functional_Equipment_Level__c = felId;
			form.Functional_Equipment_Level__r = fel;
			form.Planner_Group__c = fel.Planner_Group__c;
			form.AMU_Name__c = fel.Operating_Field_AMU__r.Name;
		}
		//if Yard Id is in Url, then query Yard record
		else if (yardId != null) {
			Yard__c yard = [
					SELECT Id, Name, Route__r.Route_Number__c, Planner_Group__c, Operating_Field_AMU__r.Name
					FROM Yard__c
					WHERE Id = :yardId
			];

			form.Yard__c = yardId;
			form.Yard__r = yard;
			form.Planner_Group__c = yard.Planner_Group__c;
			form.AMU_Name__c = yard.Operating_Field_AMU__r.Name;
		}

		form.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
		form.Equipment_Status__c = EquipmentUtilities.REQUEST_STATUS_MISSING;

	}
	private void clearViewState() {
		if (attachment.Id == null) {
			attachment.Body = null;
			attachment = new Attachment();
		}
		if (serialPlate.Id == null) {
			serialPlate.Body = null;
			serialPlate = new Attachment();
		}

		if (attachmentFile.ContentDocumentId == null) {
			attachmentFile = new ContentDocumentLink();
		}
		if (serialPlateFile.ContentDocumentId == null) {
			serialPlateFile = new ContentDocumentLink();
		}
	}

	public PageReference save() {


		if (form.Description__c == null) {
			clearViewState();
			form.Description__c.addError('You must enter a value');
			return null;
		}

		if (form.Model_Number__c == null) {
			clearViewState();
			form.Model_Number__c.addError('You must enter a value');
			return null;
		}

		if (form.Manufacturer__c == null) {
			clearViewState();
			form.Manufacturer__c.addError('You must enter a value');
			return null;
		}

		if (form.Manufacturer_Serial_No__c == null) {
			clearViewState();
			form.Manufacturer_Serial_No__c.addError('You must enter a value');
			return null;
		}


		if (serialPlate.Body == null && serialPlateFile.ContentDocumentId == null) {
			clearViewState();
			form.Serial_plate_of_asset__c.addError('You must attach a file');
			return null;
		}

		if (form.Location__c == null && form.Facility__c == null && form.Well_Event__c == null && form.System__c == null && form.Sub_System__c == null
				&& form.Yard__c == null && form.Functional_Equipment_Level__c == null) {
			clearViewState();
			form.addError('Please go to a Well Location / Facility / Well Event / System or Sub-System / Yard / FEL detail page and click the "Add Equipment Form" button on the Equipment related list.');
			return null;
		}

		try {
			PageReference nextPage = controller.save();
			form = (Equipment_Missing_Form__c) controller.getRecord();
			if (attachment.Id == null && attachment.Body != null) {
				attachment.ParentId = controller.getId();
				insert attachment;
				System.debug('insert attachment');
				form.Attachment__c = attachment.Id;
				//clear body of uploaded file to remove from view state limit error
				attachment.Body = null;
				attachment = new Attachment();
			}
			if (serialPlate.Id == null && serialPlate.Body != null) {
				serialPlate.ParentId = controller.getId();
				insert serialPlate;
				System.debug('insert serial plate =' + serialPlate.Id);
				form.Serial_plate_of_asset__c = serialPlate.Id;
				//clear body of uploaded file to remove from view state limit error
				serialPlate.Body = null;
				serialPlate = new Attachment();
			}
			System.debug('update form =' + form);
			if (form.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {
				form.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
			}
			update form;
			if (ApexPages.currentPage().getParameters().get('retURL') != null) {
				System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
				nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
				nextPage.setRedirect(true);
			}
			return nextPage;
		} catch (Exception e) {
			System.debug('save error');

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
	}
	public void DeletePhoto() {
		try {
			attId = attId == '' ? null : attId;
			attFileId = attFileId == '' ? null : attFileId;
			if (attachment.Id == attId && attId != null) {
				delete attachment;
				form.Attachment__c = '';
				attachment.Body = null;
				attachment = new Attachment();
			} else if (serialPlate.Id == attId && attId != null) {
				delete serialPlate;
				form.Serial_plate_of_asset__c = '';
				serialPlate.Body = null;
				serialPlate = new Attachment();
			} else if (attachmentFile.ContentDocumentId == attFileId && attFileId != null) {
				ContentDocument document = [SELECT Id FROM ContentDocument WHERE Id = :attFileId];
				delete document;
				form.Attachment__c = '';
				attachmentFile = new ContentDocumentLink();
			} else if (serialPlateFile.ContentDocumentId == attFileId && attFileId != null) {
				ContentDocument document = [SELECT Id FROM ContentDocument WHERE Id = :attFileId];
				delete document;
				form.Serial_plate_of_asset__c = '';
				serialPlateFile = new ContentDocumentLink();
			}

			if (form.Id != null) {
				update form;
			}
		} catch (DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error deleting file'));

		}

		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'File deleted successfully'));

	}

	public PageReference CloseRequest() {
		try {
			if (!isUserAdmin) {

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
				return null;
			} else {

				form.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
				update form;
				EquipmentFormService.formProcessedTemplateEmail(form.Id, form.CreatedById, new Set<String>{
						addEquipmentService.addEquipmentPlannerGroup(form)
								+ EquipmentFormService.COMBINATION_DELIMITER
								+ addEquipmentService.getFunctionalLocationForDestination(form)
				});

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully PROCESSED and notification email has been sent.'));
				return null;
			}
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			return null;
		}
	}

	public Boolean isEmptyFloc {
		get {
			if (form.Location__c == null &&
					form.Facility__c == null &&
					form.Well_Event__c == null &&
					form.System__c == null &&
					form.Sub_System__c == null &&
					form.Functional_Equipment_Level__c == null &&
					form.Yard__c == null) {
				return true;
			} else {
				return false;
			}
		}
		set;
	}

	public Boolean isUserAdmin {
		get {
			return userPermissions.isSystemAdmin
					|| userPermissions.isSapSupportLead
					|| userPermissions.isHogAdmin;
		}
		private set;
	}

	/**
	* functions for popup window
	*/
	public void showPopup() {
		displayPopup = true;
		//closePopupReason = form.Reason__c;
	}
	public void closePopup() {
		displayPopup = false;
		//form.Reason__c = closePopupReason;
	}

	public Boolean isEditable {
		get {
			return form.Status__c != EquipmentUtilities.REQUEST_STATUS_PROCESSED
					&& form.Status__c != EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
					&& form.Status__c != EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED;
		}
		private set;
	}

	public Boolean isClosed {
		get {
			return form.Status__c == EquipmentUtilities.REQUEST_STATUS_PROCESSED
					|| form.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
					|| form.Status__c == EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED;
		}
	}


	public void saveOnPopup() {
		System.Savepoint sp = Database.setSavepoint();
		String originalStatus = form.Status__c;

		form.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;

		try {
			update form;
			//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
			EquipmentFormService.requestRejectionTemplateEmail(form.Id, form.CreatedById, new Set<String>{
					addEquipmentService.addEquipmentPlannerGroup(form)
							+ EquipmentFormService.COMBINATION_DELIMITER
							+ addEquipmentService.getFunctionalLocationForDestination(form)
			});

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully ' +
					'REJECTED and notification email ' +
					'has been sent.'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			Database.rollback(sp);
			form.Status__c = originalStatus;
		}
		closePopup();
	}

	public PageReference rejectAndNotify() {
		showPopup();
		return null;
	}


}