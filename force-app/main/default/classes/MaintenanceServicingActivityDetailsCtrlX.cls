/*----------------------------------------------------------------------------------------------------
Author     : Shreyas Dhond
Company    : Husky Energy
Description: A controller class for displaying Activity Log Details on a Maintenance Work Order Detail Page
Test Class : TestMaintenanceServicingDetailsCtrlXTest
History    :
            09.29.15 ssd Initial Revision
            29.03.18 mp added CompletedUsingRequestReschedule__c to the activityList query in getActivityList()                                        
------------------------------------------------------------------------------------------------------*/
public with sharing class MaintenanceServicingActivityDetailsCtrlX {

	private final HOG_Maintenance_Servicing_Form__c maintenanceWorkOrder;
    
    private Map<Id, Work_Order_Activity__c> activityList;
    private Map<String, List<Work_Order_Activity_Log__c>> activityLogMap;
    private Map<String, List<Work_Order_Activity_Log_Entry__c>> activityLogEntryMap;
    private List<Work_Order_Activity_Log__c> activityLogs;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public MaintenanceServicingActivityDetailsCtrlX(ApexPages.StandardController stdController) {
        maintenanceWorkOrder = (HOG_Maintenance_Servicing_Form__c) stdController.getRecord();
    }

    public List<Work_Order_Activity__c> getActivityList() {
        if(activityList == null) {
             //Get Work Order Activities and create map for Activity Logs
            activityList = new Map<Id, Work_Order_Activity__c>([Select Id, Name, CompletedUsingRequestReschedule__c
                                          From Work_Order_Activity__c
                                          Where Maintenance_Work_Order__c =: this.maintenanceWorkOrder.Id]);
        }

        System.debug('ActivityListValues: ' + activityList.values());
        return activityList.values();
    }

    public Map<String, List<Work_Order_Activity_Log__c>> getActivityLogMap() {
        if(activityLogMap == null) {
            activityLogMap = new Map<String, List<Work_Order_Activity_Log__c>>();
            for(Work_Order_Activity__c activity : activityList.values()) {
                activityLogMap.put(activity.Id, new List<Work_Order_Activity_Log__c>());
            }
            
            activityLogs = [Select  Id, Name, Work_Order_Activity__c,
                                    Tradesman__c, Started_New__c, Finished_New__c
                            From Work_Order_Activity_Log__c
                            Where Work_Order_Activity__c In :activityList.keySet()];
            for(Work_Order_Activity_Log__c log : activityLogs) {
                activityLogMap.get(log.Work_Order_Activity__c).add(log);
            }
        }

        System.debug('activityLogMap: ' + activityLogMap);
        return activityLogMap;
    }

    public Map<String, List<Work_Order_Activity_Log_Entry__c>> getActivityLogEntryMap() {
        if(activityLogEntryMap == null) {
            Set<Id> activityLogSet = new Set<Id>();
            activityLogEntryMap = new Map<String, List<Work_Order_Activity_Log_Entry__c>>();
            for(Work_Order_Activity_Log__c log : activityLogs) {
                activityLogSet.add(log.Id);
                activityLogEntryMap.put(log.Id, new List<Work_Order_Activity_Log_Entry__c>());
            }

            
            List<Work_Order_Activity_Log_Entry__c> activityLogEntries = [Select Id, Name, Work_Order_Activity_Log_Lookup__c,
                                                                                Status__c, TimeStamp__c, Duration_Text__c,
                                                                                Comments__c, MapUrl__c, User_Defined_Duration__c
                                                                         From Work_Order_Activity_Log_Entry__c
                                                                         Where Work_Order_Activity_Log_Lookup__c In :activityLogSet];
            for(Work_Order_Activity_Log_Entry__c logEntry : activityLogEntries) {
                activityLogEntryMap.get(logEntry.Work_Order_Activity_Log_Lookup__c).add(logEntry);
            }
        }

        System.debug('activityLogEntryMap: ' + activityLogEntryMap);
        return activityLogEntryMap;
    }
}