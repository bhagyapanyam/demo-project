/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Batch job that runs in two modes. It's job is to update and create engine records
                for queried Equipment records.
                Modes only determine on size of queried records. Default mode is NEW_RECORDS which means
                it will query records that were created or last modified yesterday or today.
                ALL_RECORDS mode, is for initial setup and it runs on all Equipment records.
Test Class:     EquipmentEngineCreatorBatchTest
History:        jschn 2019-06-13 - Created. - EPD R1
*************************************************************************************************/
global with sharing class EquipmentEngineCreatorBatch implements Database.Batchable<SObject> {

    public static final String CLASS_NAME = String.valueOf(EquipmentEngineCreatorBatch.class);
    private static final Set<String> SUPPORTED_EQUIPMENT_OBJECT_TYPE = EPD_Constants.SUPPORTED_EQUIPMENT_OBJECT_TYPE;

    private static final String QUERY_BASE = 'SELECT ' +
            'Name, Manufacturer__c, Model_Number__c, Tag_Number__c, Serial_Number__c, Manufacturer_Serial_No__c ' +
            'FROM Equipment__c ' +
            'WHERE Object_Type__c IN :SUPPORTED_EQUIPMENT_OBJECT_TYPE';

    private EquipmentEngineBatchMode mode;

    global EquipmentEngineCreatorBatch(EquipmentEngineBatchMode mode) {
        this.mode = mode;
    }

    /**
     * Prepares query and return query locator.
     *
     * @param BC
     *
     * @return Database.QueryLocator
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug(CLASS_NAME + ' -> STARTING BATCH JOB IN MODE: ' + String.valueOf(mode));
        String query = buildQuery();
        System.debug(CLASS_NAME + ' -> Query: ' + query);
        return Database.getQueryLocator(query);
    }

    /**
     * Runs Equipment Engine service to update and create Equipment Engine records.
     *
     * @param BC
     * @param scope
     */
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        System.debug(CLASS_NAME + ' -> EXECUTING JOB ON CHUNK OF SIZE: ' + scope.size());

        new EquipmentEngineService()
                .updateEngines((List<Equipment__c>) scope, EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED)
                .createEngines((List<Equipment__c>) scope, EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED);

        System.debug(CLASS_NAME + ' -> JOB ON CHUNK HAS BEEN FINISHED.');
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(CLASS_NAME + ' -> BATCH JOB FINISHED.');
    }

    /**
     * Builds query based on mode.
     * In NEW_RECORDS mode it restricts time range to yesterday and today records.
     *
     * @return String
     */
    private String buildQuery() {
        String query = QUERY_BASE;

        if(EquipmentEngineBatchMode.NEW_RECORDS.equals(mode)
                || mode == null) {
            query += ' AND (LastModifiedDate >= YESTERDAY OR CreatedDate >= YESTERDAY)';
        }

        return query;
    }

}