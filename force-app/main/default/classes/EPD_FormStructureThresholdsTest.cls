/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureThresholds
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureThresholdsTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine__mdt engineConfig;
        EPD_FormStructureThresholds result;

        Test.startTest();
        try {
            result = new EPD_FormStructureThresholds().setRecord(engineConfig);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);

    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt();
        EPD_FormStructureThresholds result;

        Test.startTest();
        try {
            result = new EPD_FormStructureThresholds().setRecord(engineConfig);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(engineConfig.Max_RPM__c, result.maxRPM);
        System.assertEquals(engineConfig.Min_RPM__c, result.minRPM);
        System.assertEquals(engineConfig.Max_Intake_Manifold_Pressure__c, result.maxIntakeManifoldPressure);
        System.assertEquals(engineConfig.Min_Intake_Manifold_Pressure__c, result.minIntakeManifoldPressure);
        System.assertEquals(engineConfig.Max_Cylinder_Compression__c, result.maxCylinderCompression);
        System.assertEquals(engineConfig.Min_Cylinder_Compression__c, result.minCylinderCompression);
        System.assertEquals(engineConfig.Recommended_Cranking_Speed__c, result.recommendedCrankingSpeed);
        System.assertEquals(engineConfig.Max_Valve_Seat_Recession__c, result.maxValveSeatRecession);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1,
                Min_RPM__c = 2,
                Max_Intake_Manifold_Pressure__c = 3,
                Min_Intake_Manifold_Pressure__c = 4,
                Max_Cylinder_Compression__c = 5,
                Min_Cylinder_Compression__c = 6,
                Recommended_Cranking_Speed__c = 7,
                Max_Valve_Seat_Recession__c = 8,
                Valve_Seat_Warning_Limit__c = 8
        );
        EPD_FormStructureThresholds result;

        Test.startTest();
        try {
            result = new EPD_FormStructureThresholds().setRecord(engineConfig);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(engineConfig.Max_RPM__c, result.maxRPM);
        System.assertEquals(engineConfig.Min_RPM__c, result.minRPM);
        System.assertEquals(engineConfig.Max_Intake_Manifold_Pressure__c, result.maxIntakeManifoldPressure);
        System.assertEquals(engineConfig.Min_Intake_Manifold_Pressure__c, result.minIntakeManifoldPressure);
        System.assertEquals(engineConfig.Max_Cylinder_Compression__c, result.maxCylinderCompression);
        System.assertEquals(engineConfig.Min_Cylinder_Compression__c, result.minCylinderCompression);
        System.assertEquals(engineConfig.Recommended_Cranking_Speed__c, result.recommendedCrankingSpeed);
        System.assertEquals(engineConfig.Max_Valve_Seat_Recession__c, result.maxValveSeatRecession);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureThresholds structure = new EPD_FormStructureThresholds();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}