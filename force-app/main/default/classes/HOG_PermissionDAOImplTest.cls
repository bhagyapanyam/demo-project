/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_PermissionDAOImpl
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class HOG_PermissionDAOImplTest {

    @IsTest
    static void getPermissionSetsByName_withoutParam() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Set<String> param;
        Integer expectedCount = 0;
        List<PermissionSet> result;

        Test.startTest();
        try {
            result = dao.getPermissionSetsByName(param);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPermissionSetsByName_withEmptyParam() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Set<String> param = new Set<String>();
        Integer expectedCount = 0;
        List<PermissionSet> result;

        Test.startTest();
        try {
            result = dao.getPermissionSetsByName(param);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPermissionSetsByName_withWrongParam() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Set<String> param = new Set<String>{
                'WrongPermissionName'
        };
        Integer expectedCount = 0;
        List<PermissionSet> result;

        Test.startTest();
        try {
            result = dao.getPermissionSetsByName(param);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPermissionSetsByName_withGoodParam() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Set<String> param = new Set<String>();
        for(PermissionSet ps : [SELECT Name FROM PermissionSet LIMIT 100]) {
            param.add(ps.Name);
        }
        Integer expectedCount = param.size();
        List<PermissionSet> result;

        Test.startTest();
        try {
            result = dao.getPermissionSetsByName(param);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignmentCountForPermissions_withoutParams() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id param1;
        Set<Id> param2;
        Integer expectedCount = 0;
        Integer result;

        Test.startTest();
        try {
            result = dao.getAssignmentCountForPermissions(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result);
    }

    @IsTest
    static void getAssignmentCountForPermissions_withoutParam1() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id param1;
        Set<Id> param2 = new Set<Id>{UserInfo.getUserId()};
        Integer expectedCount = 0;
        Integer result;

        Test.startTest();
        try {
            result = dao.getAssignmentCountForPermissions(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result);
    }

    @IsTest
    static void getAssignmentCountForPermissions_withoutParam2() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id param1 = UserInfo.getUserId();
        Set<Id> param2;
        Integer expectedCount = 0;
        Integer result;

        Test.startTest();
        try {
            result = dao.getAssignmentCountForPermissions(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result);
    }

    @IsTest
    static void getAssignmentCountForPermissions_withWrongParams() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id param1 = UserInfo.getUserId();
        Set<Id> param2 = new Set<Id>{UserInfo.getUserId()};
        Integer expectedCount = 0;
        Integer result;

        Test.startTest();
        try {
            result = dao.getAssignmentCountForPermissions(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result);
    }

    @IsTest
    static void getAssignmentCountForPermissions_withProperParams() {
        HOG_PermissionDAO dao = new HOG_PermissionDAOImpl();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id param1 = UserInfo.getUserId();
        List<PermissionSetAssignment> assignments = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: param1];
        Set<Id> param2 = HOG_GeneralUtilities.getSetOfParentIds(assignments, 'PermissionSetId');
        Integer expectedCount = assignments.size();
        Integer result;

        Test.startTest();
        try {
            result = dao.getAssignmentCountForPermissions(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result);
    }

}