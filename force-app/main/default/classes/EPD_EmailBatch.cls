/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Batch job that handles sending emails by 10. It's required bcs those emails are
                really heavy on SOQL queries per record. (bcs of usage of Email Template)
Test Class:     EPD_EmailBatchTest
History:        jschn 2019-07-16 - Created. - EPD R1
*************************************************************************************************/
public without sharing class EPD_EmailBatch implements Database.Batchable<EPD_Engine_Performance_Data__c> {

    private EPD_EmailServiceBase emailService;
    private List<EPD_Engine_Performance_Data__c> epdRecords;

    @TestVisible
    private Integer RECORD_COUNT_THRESHOLD = 10;

    public EPD_EmailBatch(EPD_EmailServiceBase emailService, List<EPD_Engine_Performance_Data__c> epdRecords) {
        this.emailService = emailService;
        this.epdRecords = epdRecords;
    }

    public Iterable<EPD_Engine_Performance_Data__c> start(Database.BatchableContext param1) {
        return new EPD_EPDIterable(epdRecords);
    }

    public void execute(Database.BatchableContext param1, List<EPD_Engine_Performance_Data__c> scope) {
        validateRecordCount(scope);
        emailService.sendEmails(scope);
    }

    public void finish(Database.BatchableContext param1) {}

    /**
     * Validate Record count on provided list of EPD records.
     * This is because this email service is heavy on SOQL calls. As we are using EmalTemplate and we need
     * to append some other data into that template we need to call Messaging.renderStoredEmailTemplate
     * which is considered as SOQL query.
     */
    @TestVisible
    private void validateRecordCount(List<EPD_Engine_Performance_Data__c> scope) {
        if(scope != null && scope.size() > RECORD_COUNT_THRESHOLD) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Threshold_Email_Record_Count_Error,
                    new List<String> {
                            String.valueOf(RECORD_COUNT_THRESHOLD),
                            String.valueOf(scope.size())
                    }
            );
        }
    }

}