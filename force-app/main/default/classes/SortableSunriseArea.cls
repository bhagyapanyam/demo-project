public with sharing class SortableSunriseArea implements Comparable {
	// Values must be in all lower case.
    private static final Map<String, Integer> KNOWN_AREA_NAMES_ORDINAL = new Map<String, Integer> {
            'control room 1a' => 1,
            'steam 1a' => 2,
            'process 1a' => 3,
            'water 1a' => 4,
            
            'control room 1b' => 5,
            'steam 1b' => 6,
            'process 1b' => 7,
            'water 1b' => 8,
            
            'field panel' => 9,
            'field' => 10,
            'swing operator' => 11        
    };
        
    public String areaName { get; set; }
    
    public SortableSunriseArea(String areaName) {
        this.areaName = areaName;
    }
    
    public Integer compareTo(Object obj) {
    	SortableSunriseArea other = (SortableSunriseArea) obj;
    	
    	if (this.areaName == null) {
    		return -1;
    	} else if (other.areaName == null) {
    		return 1;
    	} else {
    		Integer thisAreaNameOrdinal = KNOWN_AREA_NAMES_ORDINAL.get(this.areaName.toLowerCase());
    		Integer otherAreaNameOrdinal = KNOWN_AREA_NAMES_ORDINAL.get(other.areaName.toLowerCase());
    		
    		System.debug('\n*******************************************\n' 
    			+ 'METHOD: SortableSunriseArea.compareTo()'
    			+ '\nThis Area: [name="' + this.areaName + '", ordinal=' + thisAreaNameOrdinal + ']'
    			+ '\nOther Area: [name="' + other.areaName + '", ordinal=' + otherAreaNameOrdinal + ']'
    			+ '\n*******************************************\n');
    			    		
    		if (thisAreaNameOrdinal == null && otherAreaNameOrdinal == null) {
    			// None found. Use natural sort.
    			return this.areaName.compareTo(other.areaName);
    		} else if (thisAreaNameOrdinal == null) {
    			// Place unknowns at the bottom.
    			return 1;
    		} else if (otherAreaNameOrdinal == null) {
    			// Place unknowns at the bottom.
    			return -1;
    		} else {
    			return (thisAreaNameOrdinal - otherAreaNameOrdinal);
    		}
    	}
    }
}