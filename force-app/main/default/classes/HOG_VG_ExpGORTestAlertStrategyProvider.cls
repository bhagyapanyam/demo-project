/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertStrategyProvider for Expired GOR Test Alerts.
                This class contains logic for deciding whether Expired GOR Test Alert should be
                created.
Test Class:     HOG_VG_ExpGORTestAlertStrategyPrvdrTest
History:        jschn 19/10/2018 - Created. - W-001275 - W-001277
*************************************************************************************************/
public with sharing class HOG_VG_ExpGORTestAlertStrategyProvider extends HOG_VG_AlertStrategyProvider {

    public HOG_VG_ExpGORTestAlertStrategyProvider(List<Location__c> locations,
            Map<Id, Location__c> oldLocationsMap,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {
        super(locations, oldLocationsMap, ventGasExemptionsByLocationId, activeAlertsByLocationAndType);
    }

    /**
     * Implementation of decision logic for creating Expired GOR Test Alert.
     * Checks whether there is another active alert as well as if there is an active exemption on requested Location.
     * Last, it will check if there are all necessary conditions met.
     *
     * @param location
     *
     * @return
     */
    public override Boolean shouldCreate(Location__c location) {
        return !super.ventGasExemptionsByLocationId.containsKey(location.Id)
                && !super.activeAlertsByLocationAndType.containsKey(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST)
                && expiredGORTestTriggered(location);
    }

    /**
     * Checks if there are all necessary conditions met for creating requested Alert.
     *
     * @param location
     *
     * @return
     */
    private Boolean expiredGORTestTriggered(Location__c location) {
        return location.Expired_GOR_Test__c != oldLocationsMap.get(location.Id).Expired_GOR_Test__c
                && location.Expired_GOR_Test__c == 'YES'
                && super.isCorrectLocation(location)
                && correctDateToCreate();
    }

    /**
     * Return always true. Expired GOR Test doesn't care for date.
     *
     * @return
     */
    protected override Boolean correctDateToCreate() {
        return true;
    }


}