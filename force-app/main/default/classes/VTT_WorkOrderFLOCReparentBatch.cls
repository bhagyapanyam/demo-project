/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Job for re-parenting Work Order and Work Order Activities with FLOCs.
                Jobs are chained to avoid hitting Lookup Lock DML error. Even thou it might be not
                necessary, I didn't wanted to take risks. As we don't want to launch trigger during
                these updates I'm disabling trigger flags for both cases.
Test Class:     VTT_WorkOrderFLOCReparentBatchTest
History:        jschn 2018-12-12 - Created. (US - 001323)
*************************************************************************************************/
public without sharing class VTT_WorkOrderFLOCReparentBatch implements Database.Batchable<SObject> {

    private static final String WORK_ORDER_QUERY = 'SELECT' +
            ' Equipment__c, Functional_Location__c, Functional_Location_Category_SAP__c' +
            ' FROM HOG_Maintenance_Servicing_Form__c' +
            ' WHERE Functional_Location__c != NULL' +
            ' AND Functional_Location_Category_SAP__c != NULL' +
            ' AND Business_Unit_Lookup__c = NULL' +
            ' AND DLFL__c = false' +
            ' AND ALT_Confirmed__c = false';
    private static final String WORK_ORDER_ACTIVITY_QUERY = 'SELECT' +
            ' Equipment__c, Functional_Location__c, Functional_Location_Category_SAP__c' +
            ' FROM Work_Order_Activity__c' +
            ' WHERE Functional_Location__c != NULL' +
            ' AND Business_Unit__c = NULL' +
            ' AND Maintenance_Work_Order__r.DLFL__c = false' +
            ' AND Maintenance_Work_Order__r.ALT_Confirmed__c = false';
    private String query = '';

    private BatchType type;

    private Boolean rerunWithChild;

    public VTT_WorkOrderFLOCReparentBatch(BatchType type, Boolean rerunWithChild) {
        this.type = type;
        this.rerunWithChild = rerunWithChild;
    }

    /**
     * Before main logic kicks in, disable respective trigger, prepare query and Query Locator
     *
     * @param context
     *
     * @return
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        setQuery();
        return Database.getQueryLocator(query);
    }

    /**
     * Sets correct FLOCs on provided records and updates them.
     *
     * @param context
     * @param records
     */
    public void execute(Database.BatchableContext context, List<SObject> records) {
        if(records != null && records.size() > 0) {
            switchTriggerFlag(false);
            setFLOCOnRecords(records);
            update records;
            switchTriggerFlag(true);
        }
    }

    /**
     * After batch job finished, enable respective trigger and if required, run same job on Work Order Activities
     *
     * @param context
     */
    public void finish(Database.BatchableContext context) {
        if(rerunWithChild && isWorkOrderType(type)) {
            Database.executeBatch(new VTT_WorkOrderFLOCReparentBatch(BatchType.WORK_ORDER_ACTIVITY, false));
        }
    }

    /**
     * Main logic of this batch job. It runs records through parenting logic
     *
     * @param records
     */
    private void setFLOCOnRecords(List<SObject> records) {
        if(isWorkOrderType(type)) {
            MaintenanceServicingUtilities.SetCorrectFLOCoNWorkOrder(records);
        } else if(isWorkOrderActivityType(type)) {
            VTT_Utilities.SetCorrectFLOCoNWorkOrderActivity(records);
        }
    }

    /**
     * Switch flag on triggers based on batch type. (Disabling & enabling triggers)
     *
     * @param flag
     */
    private void switchTriggerFlag(Boolean flag) {
        MaintenanceServicingUtilities.executeTriggerCode = flag;
        if(isWorkOrderActivityType(type)) {
            VTT_Utilities.executeTriggerCode = flag;
        }
    }

    /**
     * Set query based on provided batch type.
     */
    private void setQuery() {
        if(isWorkOrderType(type)) {
            query = WORK_ORDER_QUERY;
        } else if(isWorkOrderActivityType(type)) {
            query = WORK_ORDER_ACTIVITY_QUERY;
        }
    }

    /**
     * Checks if provided type is Work Order batch type.
     *
     * @param type
     *
     * @return
     */
    private Boolean isWorkOrderType(BatchType type) {
        return type == BatchType.WORK_ORDER;
    }

    /**
     * Checks if provided type is Work Order Activity batch type.
     *
     * @param type
     *
     * @return
     */
    private Boolean isWorkOrderActivityType(BatchType type) {
        return type == BatchType.WORK_ORDER_ACTIVITY;
    }

    public enum BatchType {
        WORK_ORDER,
        WORK_ORDER_ACTIVITY
    }

}