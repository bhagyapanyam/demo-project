/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_EngineAction = action model for vtt ltng engine
Test Class:     VTT_LTNG_EngineActionTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public with sharing class VTT_LTNG_EngineAction implements Comparable{

    public String logEntryAction;

    @AuraEnabled
    public String buttonLabel;

    // for matching action with component on frontend
    @AuraEnabled
    public String actionName;

    @AuraEnabled
    public Integer buttonOrder;

    public VTT_LTNG_EngineAction(String logEntryAction, String buttonLabel, String actionName, Integer buttonOrder) {
        this.logEntryAction = logEntryAction;
        this.buttonLabel = buttonLabel;
        this.actionName = actionName;
        this.buttonOrder = buttonOrder;
    }

    public String getNewTradesmanStatus(){
        System.debug('ACTION logEntryAction' + logEntryAction);
        return VTT_LTNG_WorkFlowEngineConstants.TRADESMAN_AND_ACTIVITY_STATUS_MAP.get(logEntryAction)[0];
    }

    public String getNewActivityStatus(){
        return VTT_LTNG_WorkFlowEngineConstants.TRADESMAN_AND_ACTIVITY_STATUS_MAP.get(logEntryAction)[1];
    }

    public Integer compareTo(Object compareTo) {
        VTT_LTNG_EngineAction compare = (VTT_LTNG_EngineAction) compareTo;
        Integer returnValue = 0;
        if (buttonOrder > compare.buttonOrder) {
            returnValue = 1;
        } else if (buttonOrder < compare.buttonOrder) {
            returnValue = -1;
        }
        return returnValue;
    }
}