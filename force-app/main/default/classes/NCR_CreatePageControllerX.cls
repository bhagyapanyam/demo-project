public class NCR_CreatePageControllerX{
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
  
    private final ApexPages.StandardController stdcontroller;
    private final NCR_Case__c ncr_record;
    
    private Map<String, NCR_State__c> NCRStateMap {get; set;}
    public NCR_State__c CurrentNCRState {get; private set;}
    public integer ActionCount {get; private set;}
    
    public List<Attachment> attachmentList {get; private set;} 
    public List<Document> documentList {get; private set;} 
    public Attachment document {get; set;}
    public Document documentFile {get; set;}    
    
    public String businessUnitName {get;set;}
    public String ProjectName {get;set;}
    
    
    private String NewRecordState {get; set;}

    public String selectedNCROption {get;set;}

    //used to get a hold of the attachment record selected for deletion
    public String selectedAttachmentId { get; set; }
  
    public Boolean SubmitConfirmMode {get; private set;}  
    
    public string currentBU { get; set; }
    public string currentProj { get; set; }

    private void LoadStateMap()
    {
    
        Map<String, NCR_State__c> m = new Map<String, NCR_State__c>([
        select id, name, ActionCount__c,
            (select id, name, Edit_Screen_Required__c, 
                FieldSet__c, NCR_New_State__r.Name, NCR_Action__r.Name 
                from NCR_StateActions__r order by Order__c ) 
        from NCR_State__c
        ]);
        NCRStateMap = new Map<String, NCR_State__c>();
        for(String strid: m.keySet())
        {
            NCR_State__c state = (NCR_State__c)m.get(strid);
            NCRStateMap.put((String)state.Name, (NCR_State__c)state);
        }    
    }
    

       
    public NCR_CreatePageControllerX(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();

       List<string> fields = new List<String>{'Name','NCR_Case_State__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldName);

            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }



        

        this.stdcontroller = stdController;
        //LoadFieldSetFields();
        
        this.ncr_record = (NCR_Case__c)stdController.getRecord();  

        NewRecordState = '';
        
        LoadStateMap();
        LoadCurrentStateActions();
        
        ncr_record.NCR_Case_State__c = 'Created';
        ncr_record.Originator__c =  UserInfo.getUserId();  
        
        //check for record security
        
        selectedNCROption = 'Project';


        attachmentList = new List<Attachment>() ;  
        document = new Attachment();
        
        documentList = new List<Document>() ; 
        documentFile = new Document();
        
        SubmitConfirmMode = false;
    }

    
    private void LoadCurrentStateActions()
    {
        this.ActionCount = 0;
        this.CurrentNCRState = NCRStateMap.get(ncr_record.NCR_Case_State__c);
        if(currentNCRState == null)
        {
        //throw error
            return;
        }
        this.ActionCount  = (Integer)currentNCRState.ActionCount__c;
    }

    public PageReference NCR_Redirect()
    {
        //assign recordtype
        
        List<RecordType>  recTypes = [select id, name, sobjectType,developername from recordtype where sobjecttype = 'NCR_Case__c' and Name = :ncr_record.NCR_Type__c LIMIT 1];
        if(recTypes .size()>0)
        {
            ncr_record.RecordTypeID = recTypes[0].id; 
        }
    
        if(ncr_record.NCR_Type__c =='Project') return Page.NCR_CreateProjectPage;
        if(ncr_record.NCR_Type__c =='Asset Integrity') return Page.NCR_CreateAssetIntegrityPage;
        return null;
    }


    public PageReference NCR_Save()
    {
        ncr_record.NCR_Case_State__c = 'Created';
        ncr_record.Originator__c =  UserInfo.getUserId();        
        this.ncr_record.Business_Unit_Lookup__c = currentBU;
        this.ncr_record.Project__c = currentProj;
        try{       
        insert ncr_record;
        
        }
         catch(DmlException ex){
       // ApexPages.addMessages(ex);
        return null;
       }
        System.debug('Record created with id: '  + ncr_record.id );
        NCR_Utils.manualNCRCaseShareRead(ncr_record.id);
        SaveAttachments();
        this.stdcontroller.save();
        
        PageReference viewpage  = new ApexPages.StandardController(ncr_record).view(); 
        viewpage.setRedirect(true);
        return viewpage;
    }
    public PageReference NCR_SaveAndSubmit()
    {
        ncr_record.NCR_Case_State__c = 'Submitted';
        ncr_record.Originator__c =  UserInfo.getUserId();
        this.ncr_record.Business_Unit_Lookup__c = currentBU;
        this.ncr_record.Project__c = currentProj;
         try{       
        insert ncr_record;
        
        }
         catch(DmlException ex){
        //ApexPages.addMessages(ex);
        return null;
       
       }
        
        System.debug('Record created with id: '  + ncr_record.id );
        NCR_Utils.manualNCRCaseShareRead(ncr_record.id);
        SaveAttachments();
        this.stdcontroller.save();

       PageReference pg = new PageReference('/apex/NCR_ViewPage');
       pg.getParameters().put('id', ncr_record.id);
       pg.getParameters().put('message', 'NCR Record successfully submitted');
       pg.setRedirect(true);
       return pg;

        /*
        SubmitConfirmMode = true;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'NCR Record successfully submitted')); 
        
        this.stdcontroller.save();
        */
        
        //return Page.NCR_Case_SubmitConfirmation;
        
        //return this.stdcontroller.save();
    }

    public PageReference exit(){
        Schema.SObjectType objObjectType = ncr_record.getSObjectType();
        DescribeSObjectResult anySObjectSchema = objObjectType.getDescribe();

        String objectIdPrefix = anySObjectSchema.getKeyPrefix();
        PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
        pageReference.setRedirect(true);
        return pageReference;
    }    


    private void SaveAttachments()
    {
        
        List<Attachment> attachmentList = new List<Attachment>();
        
        documentList = [select id, Name, Body from Document where id in :documentList];
        for(Document doc: documentList)
        {
        
            Attachment newAtt = new Attachment();
             
            newAtt.ParentId = ncr_record.id;
            newAtt.OwnerId = UserInfo.getUserId();            
            newAtt.Body= doc.body;                        
            newAtt.Name= doc.Name;              
            
            attachmentList.add(newAtt); 
        }
        insert attachmentList;
        
        
        delete documentList;
    }

    
    public PageReference NCR_CancelEdit()
    {
        return this.stdcontroller.cancel();
    }
    
    public PageReference removeFile()
    {
        // if for any reason we are missing the reference
        if (selectedAttachmentId == null)
            return null;

        List<Document> selectedDocument = [Select Id, Name From Document Where Id = :selectedAttachmentId ];
         
        if (selectedDocument.size() > 0)
        {
                try
                {
                    delete selectedDocument ;
                    
                    
                    documentList = [select id, name from Document where id in :documentList];
                    /*                    
                    Integer i = 0;
                    for(Document doc: documentList)
                    {
                        if(doc.id == selectedAttachmentId)
                        {
                            documentList.remove(i);
                        }
                        i++;
                    }
                    */
                } 
                catch (DMLException e) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
                    return null;
                }
                finally 
                {
                    //maintenanceAttachments = getMaintenanceAttachments();
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File deleted successfully')); 
                }
        }
            
        return null;
    }    
    public PageReference uploadFile()  
    {  
    
        try 
        {    
        if(documentFile.Name == null || documentFile.Body == null)
        {
            return null;
        }
    
    /*
        Document doc = new Document();
        doc.folderid = UserInfo.getUserId();
        doc.name = document.Name;
        doc.body = document.body;
        insert doc;
      */
        documentFile.folderid = UserInfo.getUserId(); 
        insert documentFile;
        documentList.add(documentFile);                    
        
        //document.ParentId = UserInfo.getUserId();
        document.OwnerId = UserInfo.getUserId();

        System.debug('\n*****************************************\n'
            + 'METHOD: NCR_CreatePageControllerX.uploadFile'
            + '\NCR ID: ' + this.ncr_record.id
            + '\ndocument: ' + documentFile
            + '\ndocumentBody: ' + documentFile.Body
            + '\ndocumentName: ' + documentFile.Name
            + '\n************************************************\n');    

            //insert document;
            //attachmentList.add(document);
        } 
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        }
        finally 
        {
            //document.body = null; // clears the viewstate
            //document = new Attachment();
            
            
            documentFile.body = null;
            documentFile = new Document();
        }    

        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully')); 
        
       
  
                     
        return null; 
    }    
    
    /*populate business unit picklist*/
    public List<SelectOption> getBusinessUnits() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Business_Unit__c b:[SELECT id, name from Business_Unit__c where RecordType.name = 'SAP Profit Center Level 4']){
            options.add(new SelectOption(b.id,b.name));
        }
        
        return options;
    }
    
    /*populate project picklist based on business unit*/
    public List<SelectOption> getProjects() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Milestone1_Project__c p:[SELECT id, name from Milestone1_Project__c where RecordType.name = 'SAP Project' and Active__c = true and Business_Unit__c = :currentBU]){
            options.add(new SelectOption(p.id,p.name));
        }
        
        return options;
    }    
    

}