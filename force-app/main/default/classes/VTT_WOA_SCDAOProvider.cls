/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_SCDAOProvider {

    public static VTT_WOA_SearchCriteriaDAO searchCriteriaDAO = new VTT_WOA_SearchCriteriaSelector();
    public static VTT_SCAccountDAO accountDAO = new HOG_AccountDAOImpl();
    public static VTT_SCContactDAO contactDAO = new HOG_ContactDAOImpl();
    public static VTT_SCFieldDAO AMUDAO = new HOG_SO_FieldSelector();

}