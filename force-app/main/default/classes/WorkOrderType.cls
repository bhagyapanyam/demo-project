/*-------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A controller class for creating Work Order Type
Inputs     : ApexPages.StandardController
Test Class : WorkOrderTypeTest
History    :
---------------------------------------------------------------------------------------------------*/
public with sharing class WorkOrderType
{                
    @TestVisible private ApexPages.StandardController std;

    public HOG_Work_Order_Type__c workOrderType;
    
    private static final String WORK_ORDER_TYPE_NEW_PAGE_URI = '/apex/WorkOrderType';
    private Map<String, String> mapSelectListValues = new Map<String, String>();
            
    public WorkOrderType (ApexPages.StandardController controller)
    {
        std = controller;

        workOrderType = (HOG_Work_Order_Type__c)controller.getRecord();
 
        System.debug('\n***************************************\n'
            + 'workOrderType'
            + '\nworkOrderType: ' + workOrderType
            + '\n***************************************\n');  
                               
        getRecordTypes();
    }

    List<SelectOption> recordTypeOptions = new List<SelectOption>();

    //public void getRecordTypes()
    private void getRecordTypes()
    {
        Schema.DescribeSObjectResult objectResult = HOG_Maintenance_Servicing_Form__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = objectResult.getRecordTypeInfos();
                     
        for (Schema.RecordTypeInfo rt : recordTypeInfo)  
        {                        
            recordTypeOptions.add(new SelectOption(rt.getRecordTypeId(), rt.getName()));

            // prepare map of select options
            mapSelectListValues.put(rt.getRecordTypeId(), rt.getName());
        }        
    }

    public List<SelectOption> getRecordTypeOptions()
    {
        return recordTypeOptions;
    }

    //public PageReference newWorkOrderType()
    private PageReference newWorkOrderType() 
    {
        HOG_Work_Order_Type__c newWorkOrderType = new HOG_Work_Order_Type__c();
                
        Schema.DescribeSObjectResult describeResult = newWorkOrderType.getSObjectType().getDescribe();
        String safeReturnUrl = EncodingUtil.urlEncode(WORK_ORDER_TYPE_NEW_PAGE_URI, 'UTF-8');
        String addNewUrl = '/' 
            + describeResult.getKeyPrefix() + '/e?' 
            + 'retURL=' + safeReturnUrl;
                    
        PageReference pr = new PageReference(addNewUrl);
        pr.setRedirect(true);
        return pr;
    }
    
    public pageReference cancel() 
    {
       return std.cancel();
    }

    public pageReference save() 
    {        
        workOrderType.Record_Type_Name__c = mapSelectListValues.get(workOrderType.Record_Type_ID__c);

		System.debug('workOrderType.Record_Type_ID__c: ' + workOrderType.Record_Type_ID__c);
		System.debug('workOrderType.Record_Type_Name__c: ' +workOrderType.Record_Type_Name__c);
                
        return std.save();
    }

    public PageReference saveAndNew() 
    {     
        new ApexPages.StandardController(workOrderType).save();
        return newWorkOrderType();      
    }        
}