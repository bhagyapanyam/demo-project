public class USCP_HomePage_Controller {
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
       
    public USCP_HomePage_Controller(ApexPages.StandardController controller) {
        recordId = controller.getId();
        userID =   UserInfo.getUserid();
    }
    /*
        protal user will be redirected to the USCP_Customer with id = his accountid
        admin user will stay on this page        
    */
    public PageReference redirect() {

            Profile p = [select name from Profile where id = :UserInfo.getProfileId()];

                      if ('US Portal User Profile'.equals(p.name)) 
                         {
                           PageReference customPage =  Page.USCP_Customer;
                           customPage.setRedirect(true);
                           
                           contactId = [Select contactid from User where id =:userID].contactId;
                           AccID  = [Select AccountID from Contact where id =:contactId].AccountId;


                           customPage.getParameters().put('id', AccID);
                           return customPage;
                          
                          } else {
                              //return null; //otherwise stay on the same page  
                              //return null; //otherwise stay on the same page  
                              return new PageReference('/001'); //redirect to Accounts                              
                          }
   }
}