public with sharing class MaintenanceServicingBulkALTControllerX{

    ApexPages.StandardSetController setCon;
    

    public List<HOG_Maintenance_Servicing_Form__c> selectedList {get; private set;}
    
    public integer CurrentIndex {get; private set;}
    
    public HOG_Maintenance_Servicing_Form__c CurrentWorkOrder {get; private set;}    

    public MaintenanceServicingBulkALTControllerX(ApexPages.StandardSetController controller) {

         
       
       //controller.addFields('ALT_Confirmed__c');
       setCon = controller;       
       selectedList = new List<HOG_Maintenance_Servicing_Form__c>();

       
       if(this.setCon.getSelected().Size()==0)
       {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Work order'));
           return;
       }
       
       selectedList = [select id, name, ALT_Confirmed__c, Service_Status__c, Order_Description__c, RecordTypeId
       from HOG_Maintenance_Servicing_Form__c where id in :this.setCon.getSelected()];

       //filter list to only Maintenance WOs
       selectedList = MaintenanceServicingUtilities.GetMaintenanceWorkOrders(selectedList);

       if(selectedList == null || selectedList.isEmpty()) {
          CurrentIndex = 0;
          CurrentWorkOrder = null;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No maintenance Work Orders were selected'));
       } else {
         CurrentIndex = 1;       
         CurrentWorkOrder = selectedList[CurrentIndex-1];
       }
    }

   
    public boolean getHasPreviousWorkOrder()
    {
        return (CurrentIndex > 1);
    }   
   
    public boolean getHasNextWorkOrder()
    {
        return (CurrentIndex < selectedList.size());
    }
    
    public PageReference NextWorkOrder()
    {
        if(getHasNextWorkOrder())
        { 
            system.debug('NextWorkOrder');
            system.debug(CurrentIndex);
            CurrentIndex = CurrentIndex + 1 ;
            CurrentWorkOrder = selectedList[CurrentIndex-1];   
            system.debug(CurrentIndex); 
        }
       return null; 
    }
   
    public PageReference PreviousWorkOrder()
    { 
       if(getHasPreviousWorkOrder())
       { 
           system.debug('PreviousWorkOrder'); 
           system.debug(CurrentIndex);       
           CurrentIndex = CurrentIndex - 1 ;
           CurrentWorkOrder = selectedList[CurrentIndex-1];       
           system.debug(CurrentIndex);
       }
       return null;
    }    
    
    public PageReference ConfirmWorkOrder()
    {
           HOG_Maintenance_Servicing_Form__c obj = CurrentWorkOrder;  // (HOG_Maintenance_Servicing_Form__c)this.setCon.getRecords()[0];
    
           if(obj.ALT_Confirmed__c <> true)
           {
               String ProcessMessage;
               try
               {
                ProcessMessage  = MaintenanceServicingGlobal.updateALTConfirmed(obj.id);
                
                if(ProcessMessage.indexOf('has been successfully "ALT Confirmed".')>=0)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, ProcessMessage));
                    //this.setCon.next();                 
                    return null; 
                }   
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, ProcessMessage));
                    return null;
                }
               }
               catch(Exception ex)    
               {
                   ProcessMessage = ex.getMessage();
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ProcessMessage));
                   return null;
               }               
           }   
      
           return null;   
   
    }
}