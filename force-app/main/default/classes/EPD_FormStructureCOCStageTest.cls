/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureCOCStage
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureCOCStageTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Compressor_Operating_Condition_Stage__c cocStage;
        EPD_FormStructureCOCStage result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(cocStage);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Compressor_Operating_Condition_Stage__c cocStage = new EPD_Compressor_Operating_Condition_Stage__c();
        EPD_FormStructureCOCStage result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(cocStage);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStage.Stage_Number__c, result.stageNumber);
        System.assertEquals(cocStage.Discharge_Pressure__c, result.dischargePressure);
        System.assertEquals(cocStage.Discharge_Temperature__c, result.dischargeTemperature);
        System.assertEquals(cocStage.Suction_Pressure__c, result.suctionPressure);
        System.assertEquals(cocStage.Suction_Temperature__c, result.suctionTemperature);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Compressor_Operating_Condition_Stage__c cocStage = (EPD_Compressor_Operating_Condition_Stage__c) HOG_SObjectFactory.createSObject(
                new EPD_Compressor_Operating_Condition_Stage__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureCOCStage result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(cocStage);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStage.Stage_Number__c, result.stageNumber);
        System.assertEquals(cocStage.Discharge_Pressure__c, result.dischargePressure);
        System.assertEquals(cocStage.Discharge_Temperature__c, result.dischargeTemperature);
        System.assertEquals(cocStage.Suction_Pressure__c, result.suctionPressure);
        System.assertEquals(cocStage.Suction_Temperature__c, result.suctionTemperature);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Compressor_Operating_Condition_Stage__c cocStage = new EPD_Compressor_Operating_Condition_Stage__c();
        EPD_Compressor_Operating_Condition_Stage__c result;

        Test.startTest();
        try {
            result = (EPD_Compressor_Operating_Condition_Stage__c) new EPD_FormStructureCOCStage().setRecord(cocStage).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStage.Stage_Number__c, result.Stage_Number__c);
        System.assertEquals(cocStage.Discharge_Pressure__c, result.Discharge_Pressure__c);
        System.assertEquals(cocStage.Discharge_Temperature__c, result.Discharge_Temperature__c);
        System.assertEquals(cocStage.Suction_Pressure__c, result.Suction_Pressure__c);
        System.assertEquals(cocStage.Suction_Temperature__c, result.Suction_Temperature__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Compressor_Operating_Condition_Stage__c cocStage = (EPD_Compressor_Operating_Condition_Stage__c) HOG_SObjectFactory.createSObject(
                new EPD_Compressor_Operating_Condition_Stage__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Compressor_Operating_Condition_Stage__c result;

        Test.startTest();
        try {
            result = (EPD_Compressor_Operating_Condition_Stage__c) new EPD_FormStructureCOCStage().setRecord(cocStage).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cocStage.Stage_Number__c, result.Stage_Number__c);
        System.assertEquals(cocStage.Discharge_Pressure__c, result.Discharge_Pressure__c);
        System.assertEquals(cocStage.Discharge_Temperature__c, result.Discharge_Temperature__c);
        System.assertEquals(cocStage.Suction_Pressure__c, result.Suction_Pressure__c);
        System.assertEquals(cocStage.Suction_Temperature__c, result.Suction_Temperature__c);
    }

    @IsTest
    static void testClassicGettersAndSetters_emptyValues() {
        EPD_FormStructureCOCStage structure = new EPD_FormStructureCOCStage();
        structure.dischargePressureClassic = '';
        structure.dischargeTemperatureClassic = '';
        structure.suctionPressureClassic = '';
        structure.suctionTemperatureClassic = '';

        System.assertEquals(null, structure.dischargePressureClassic);
        System.assertEquals(null, structure.dischargeTemperatureClassic);
        System.assertEquals(null, structure.suctionPressureClassic);
        System.assertEquals(null, structure.suctionTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_properValues() {
        EPD_FormStructureCOCStage structure = new EPD_FormStructureCOCStage();
        structure.dischargePressureClassic = '1';
        structure.dischargeTemperatureClassic = '1';
        structure.suctionPressureClassic = '1';
        structure.suctionTemperatureClassic = '1';

        System.assertEquals('1', structure.dischargePressureClassic);
        System.assertEquals('1', structure.dischargeTemperatureClassic);
        System.assertEquals('1', structure.suctionPressureClassic);
        System.assertEquals('1', structure.suctionTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_wrongValues() {
        EPD_FormStructureCOCStage structure = new EPD_FormStructureCOCStage();

        Boolean dischargePressureClassicFail = false;
        try {
            structure.dischargePressureClassic = 'abc';
        } catch(Exception ex) {
            dischargePressureClassicFail = true;
        }

        Boolean dischargeTemperatureClassicFail = false;
        try {
            structure.dischargeTemperatureClassic = 'abc';
        } catch(Exception ex) {
            dischargeTemperatureClassicFail = true;
        }

        Boolean suctionPressureClassicFail = false;
        try {
            structure.suctionPressureClassic = 'abc';
        } catch(Exception ex) {
            suctionPressureClassicFail = true;
        }

        Boolean suctionTemperatureClassicFail = false;
        try {
            structure.suctionTemperatureClassic = 'abc';
        } catch(Exception ex) {
            suctionTemperatureClassicFail = true;
        }

        System.assertEquals(true, dischargePressureClassicFail);
        System.assertEquals(true, dischargeTemperatureClassicFail);
        System.assertEquals(true, suctionPressureClassicFail);
        System.assertEquals(true, suctionTemperatureClassicFail);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureCOCStage structure = new EPD_FormStructureCOCStage();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}