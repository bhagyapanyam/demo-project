public class PTValidateBillingRateCacheQuery {
    public static Map<id,Utility_Rate__c> rateMap = new Map<id,Utility_Rate__c>();
    public static boolean rateListQueried = false;
    
        public static Map<id,Utility_Rate__c> queryRateList(){
        if(rateListQueried == false){
            for(Utility_Rate__c rate : [select id,name,rate_minimum__c from utility_rate__c]){
                    rateMap.put(rate.id,rate);
            }
            
        }        
        rateListQueried = true;
       	return rateMap;
    }
    
}