@isTest
global class WeldingRequestReviewSheetWebServMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           String[] accountIdArray = new String[]{};
       WeldingRequestReviewSheetSOAP.GenerateReviewSheetAndSendResponse_element respElement = new WeldingRequestReviewSheetSOAP.GenerateReviewSheetAndSendResponse_element();
       WeldingRequestReviewSheetSOAP.AllowFieldTruncationHeader_element class1 = new WeldingRequestReviewSheetSOAP.AllowFieldTruncationHeader_element();
       WeldingRequestReviewSheetSOAP.DebuggingHeader_element class2 = new WeldingRequestReviewSheetSOAP.DebuggingHeader_element();
       WeldingRequestReviewSheetSOAP.CallOptions_element class3 = new WeldingRequestReviewSheetSOAP.CallOptions_element();
       WeldingRequestReviewSheetSOAP.DebuggingInfo_element  class4 = new WeldingRequestReviewSheetSOAP.DebuggingInfo_element ();
       WeldingRequestReviewSheetSOAP.SessionHeader_element class5 = new WeldingRequestReviewSheetSOAP.SessionHeader_element();
       response.put('response_x', respElement ); 
   }
}