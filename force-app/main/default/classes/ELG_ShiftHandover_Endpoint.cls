/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    ShiftHandover part of
Test Class:     ELG_ShiftHandover_EndpointTest
History:        mbrim 2019-05-27 - Created.
*************************************************************************************************/
public with sharing class ELG_ShiftHandover_Endpoint {
    private static final String CLASS_NAME = String.valueOf(ELG_ShiftHandover_Endpoint.class);

    /**
     * Returns data for shift handover tab
     * @return
     */
    @AuraEnabled
    public static HOG_CustomResponseImpl getBaseShiftHandoverData() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().getBaseShiftHandoverData();
            System.debug(CLASS_NAME + ' DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    /**
     * Returns handover documents in correct status that are going to be
     * displayed on Take Post dropdown
     *
     * @return
     */
    @AuraEnabled
    public static HOG_CustomResponseImpl findDataForTakePost() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().getHandoverForTakePost();
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }
        return response;
    }


    /**
     * Returns all log entries for provided shift
     *
     * @param post
     * @param facilityId
     *
     * @return
     */
    @AuraEnabled
    public static HOG_CustomResponseImpl getAllLogEntriesForPostInFacility(String post, String facilityId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().getAllLogsForActiveShiftAndPost(
                    post,
                    facilityId);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    /**
    * Returns all question categories
    *
    * @return List of
    */
    @AuraEnabled
    public static HOG_CustomResponseImpl getAllQuestionCategories() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().getAllQuestionCategories();
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

//    /**
//     * Endpoint for accepting handovers
//     *
//     * @param handover
//     * @param newShift
//     *
//     * @return
//     */
//    @AuraEnabled
//    public static HOG_RequestResult acceptHandover(ELG_Shift_Handover__c handover) {
//        System.debug(CLASS_NAME + ' accepting handover ' + handover);
//        HOG_RequestResult result;
//
//        Savepoint savePoint = Database.setSavepoint();
//
//        try {
//            // Update existing shift and Create new one
//            result = new ELG_ShiftHandoverService().acceptHandover(handover);
//        } catch (Exception ex) {
//            System.debug(CLASS_NAME + ' -> ' + ex.getMessage());
//            Database.rollback(savePoint);
//            result = new HOG_DMLResponseImpl(ex);
//            return result;
//        }
//        return result;
//    }

    /**
     * Endpoint for saving log entries
     *
     * @param log
     *
     * @return
     */
    @AuraEnabled
    public static HOG_RequestResult saveLogEntry(ELG_Log_Entry__c log) {
        HOG_RequestResult result;

        // Validation before save
        result = validateLogEntry(log, result);
        if (result != null) {
            if (!result.success) {
                return result;
            }
        }

        // Check if shift is active
        List<ELG_Shift_Assignement__c> currentShiftForPost = [
                SELECT
                        Id,
                        Primary__r.Name,
                        Shift_Cycle__c
                FROM ELG_Shift_Assignement__c
                WHERE Post__c = :log.Post__c
                AND Shift_Status__c = :ELG_Constants.SHIFT_NEW
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];

        // Logic for changing shift on added log entry. TODO do we trust frontend?
        if (currentShiftForPost.size() > 0) {
            // Change the post if this log is tied to the wrong one
            if (currentShiftForPost.get(0).Id != log.Shift_Assignement__c) {
                log.Shift_Assignement__c = currentShiftForPost.get(0).Id;
            }
        } else {
            result = new HOG_DMLResponseImpl(System.Label.ELG_Post_With_No_Active_Shift);
            return result;
        }

        try {

            Id recordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
                    .get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();

            log.RecordTypeId = recordType;
            log.User__c = UserInfo.getUserId();
            System.debug(CLASS_NAME + ' data ' + log);
            insert log;
            result = new HOG_DMLResponseImpl(log);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' -> ' + ex.getMessage());
            result = new HOG_DMLResponseImpl(ex);
        }
        return result;
    }

    @AuraEnabled
    public static HOG_RequestResult findHandoverDocumentByShift(String shiftId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().getHandoverDocumentByShift(
                    shiftId);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_RequestResult getSteamShiftWithMissingShiftEngineer(String amuId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new ELG_ShiftHandoverService().getShiftWithMissingShiftEngineer(amuId);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    /**
     * Validation for log entries
     *
     * @param log
     * @param result
     *
     * @return
     */
    private static HOG_RequestResult validateLogEntry(ELG_Log_Entry__c log, HOG_RequestResult result) {
        if (log.User_Log_Entry__c.length() > 5000) {
            result = new HOG_DMLResponseImpl(System.Label.ELG_User_Log_Entry);
            return result;
        }
        if (log.Post__c == null) {
            result = new HOG_DMLResponseImpl(System.Label.ELG_Log_Post);
            return result;
        }

        if (log.Operating_Field_AMU__c == null) {
            result = new HOG_DMLResponseImpl(System.Label.ELG_Log_Entry_AMU);
            return result;
        }

        return result;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl saveShiftEngineer(List<String> shiftIds, String currentUser) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new ELG_ShiftHandoverService().updateShiftEngineerOnMorePosts(shiftIds, currentUser);
            System.debug(CLASS_NAME + ' DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' ex ' + ex.getMessage());
            response.addError(ex);
        }
        return response;
    }
}