/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trampoline class that disables sharing settings for session.
Test Class:     EPD_EndpointTest
History:        jschn 2019-06-11 - Created. - EPD R1
*************************************************************************************************/
public without sharing class EPD_EndpointWithoutSharingTrampoline {

    /**
     * Disables sharing for loadForm action
     *
     * @param workOrderActivityId
     *
     * @return EPD_Response
     */
    public EPD_Response loadForm(Id workOrderActivityId) {
        return new EPD_EndpointHandler()
                .handleLoad(
                        workOrderActivityId,
                        EPD_LoadMode.INDIRECT_LOAD
        );
    }

    /**
     * Disables sharing for saveForm action
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    public EPD_Response saveForm(String formStructure) {
        return new EPD_EndpointHandler()
                .handleSave(
                        formStructure,
                        EPD_SaveMode.SAVE
                );
    }

    /**
     * Disables sharing for submitForm action
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    public EPD_Response submitForm(String formStructure) {
        return new EPD_EndpointHandler()
                .handleSave(
                        formStructure,
                        EPD_SaveMode.SUBMIT
                );
    }

    /**
     * Disables sharing for getEPDRecord action
     *
     * @param epdId
     *
     * @return EPD_Response
     */
    public EPD_Response getEPDRecord(Id epdId) {
        return new EPD_EndpointHandler()
                .handleLoad(
                        epdId,
                        EPD_LoadMode.DIRECT_LOAD
        );
    }

}