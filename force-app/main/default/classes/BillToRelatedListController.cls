public class BillToRelatedListController 
{     

    public List<Bill_To__c> billTos{get;set;} 
    public ATS_Freight__c freight{get;set;} 
    public Opportunity opp{get;set;} 
    
    //Constructor 
    public BillToRelatedListController (ApexPages.StandardController controller)
    {
        if (!Test.isRunningTest())
            controller.addFields(new List<String>{'ATS_Freight__r','ATS_Freight__r.Account'});
        freight = (ATS_Freight__c) controller.getRecord();        
        billTos = [ SELECT Name, Sequence__c, Account__c, Customer__c, 
                           //ATS_Freight__c, 
                           Bill_To_Address__c, Bill_To_ZIP_Code__c FROM Bill_To__c WHERE Account__c=:freight.ATS_Freight__r.Account.Id];
    } 

    /*public ContactRelatedListController(ApexPages.StandardController controller) 
    { 
        acc = (account)controller.getRecord();      
        accounts = [SELECT id FROM account WHERE id=: acc.id LIMIT 1]; 
        contacts = [SELECT id,Name, mobilephone, email FROM contact WHERE accountid = :accounts.id ORDER BY Name];     
    }*/ 

    //This method is to create a new BillTo while clicking on the New BillTo button 
    /*public pageReference newBillTo()
    {  
        Schema.DescribeSObjectResult result = Bill_To__c.sObjectType.getDescribe();
        String keyPrefix = result.getKeyPrefix();
        pageReference pageRef = new pageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/'+keyPrefix+'/e?&retURL=' + freight.id);  
        return pageRef; 
    }*/

    //This method is to edit the existing BillTo record while clicking the Edit link 
    public pageReference editBillTo()
    {  
        String billToId = Apexpages.currentpage().getParameters().get('billTo');  
        pageReference pageRef = new pageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + billToId + '/e?retURL='+freight.id); 
        return pageRef;  
    }    

    //This method is to delete the BillTo record while clicking the Del link 
    public pageReference deleteBillTo()
    {  
        String billToId = Apexpages.currentpage().getParameters().get('billTo');  
        List<Bill_To__c>  billToList = [SELECT Id FROM Bill_To__c WHERE id = : billToId LIMIT 1]; 
        delete billToList; 
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm(); 
        PageReference redirectPage = new PageReference(baseUrl+'/'+freight.id); 
        return redirectPage;  
    }   
}