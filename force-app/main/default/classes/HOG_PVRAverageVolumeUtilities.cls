/*-------------------------------------------------------------------------------------------------
Author     : Shreyas Dhond
Company    : Husky Energy
Description: Utilities for PVR Average Calculations and Updates
Inputs     : N/A
Test Class : HOG_PVRUpdateTest
History:   : mz: 20.3.2018 - update for vent gas 
---------------------------------------------------------------------------------------------------*/
public class HOG_PVRAverageVolumeUtilities {

    /**
     * Bulkified function to Update PVR Volument on Well Locations
     **/
    public static void updatePVRAverageVolume(List<Location__c> wellLocationList) {

        if(wellLocationList != null && !wellLocationList.isEmpty()) {
            for(Location__c wellLocation : wellLocationList) {

                wellLocation.PVR_AVGVOL_30D_COND__c = 0;
                wellLocation.PVR_AVGVOL_30D_GAS__c = 0;
                wellLocation.PVR_AVGVOL_30D_OIL__c= 0;
                wellLocation.PVR_AVGVOL_30D_SAND__c = 0;
                wellLocation.PVR_AVGVOL_30D_WATER__c = 0;
                wellLocation.PVR_AVGVOL_5D_COND__c = 0;
                wellLocation.PVR_AVGVOL_5D_GAS__c = 0;
                wellLocation.PVR_AVGVOL_5D_OIL__c = 0;
                wellLocation.PVR_AVGVOL_5D_SAND__c = 0;
                wellLocation.PVR_AVGVOL_5D_WATER__c = 0;
                wellLocation.PVR_Hours_On_Prod__c = 0;
                wellLocation.PVR_Produced_Cond__c = 0;
                wellLocation.PVR_Produced_Gas__c = 0;
                wellLocation.PVR_Produced_Oil__c = 0;
                wellLocation.PVR_Produced_Record_Count__c = 0;
                wellLocation.PVR_Produced_Sand__c = 0;
                wellLocation.PVR_Produced_Water__c = 0;
                wellLocation.PVR_GOR_Factor__c = 0;
                wellLocation.PVR_Fuel_Consumption__c = 0;
                wellLocation.Measured_Vent_Rate__c = 0;
                wellLocation.GOR_Test_Date__c = null;
                wellLocation.GOR_Effective_Date__c = null;
                wellLocation.Monthly_Trucked_Oil__c = 0;
                wellLocation.Single_Well_Battery__c = null;
                


                system.debug('reset well location '+wellLocation);
                List<Well_Event__c> wellEventList = wellLocation.Well_Events__r;

                system.debug('well event list size '+wellEventList.size());

                for(Well_Event__c wellEvent : wellEventList)
                {
                    if(wellEvent.Status__c == 'PROD') {
                        system.debug('Last updated well event '+wellEvent.PVR_AVGVOL_UPDATED__c);
                        if(wellEvent.PVR_AVGVOL_30D_COND__c != null)
                            wellLocation.PVR_AVGVOL_30D_COND__c += wellEvent.PVR_AVGVOL_30D_COND__c;
                        if(wellEvent.PVR_AVGVOL_30D_GAS__c != null)
                            wellLocation.PVR_AVGVOL_30D_GAS__c += wellEvent.PVR_AVGVOL_30D_GAS__c;
                        if(wellEvent.PVR_AVGVOL_30D_OIL__c != null)
                            wellLocation.PVR_AVGVOL_30D_OIL__c += wellEvent.PVR_AVGVOL_30D_OIL__c;
                        if(wellEvent.PVR_AVGVOL_30D_SAND__c != null)
                            wellLocation.PVR_AVGVOL_30D_SAND__c += wellEvent.PVR_AVGVOL_30D_SAND__c;
                        if(wellEvent.PVR_AVGVOL_30D_WATER__c != null)
                            wellLocation.PVR_AVGVOL_30D_WATER__c += wellEvent.PVR_AVGVOL_30D_WATER__c;
                        if(wellEvent.PVR_AVGVOL_5D_COND__c != null)
                            wellLocation.PVR_AVGVOL_5D_COND__c += wellEvent.PVR_AVGVOL_5D_COND__c;
                        if(wellEvent.PVR_AVGVOL_5D_GAS__c != null)
                            wellLocation.PVR_AVGVOL_5D_GAS__c += wellEvent.PVR_AVGVOL_5D_GAS__c;
                        if(wellEvent.PVR_AVGVOL_5D_OIL__c != null)
                            wellLocation.PVR_AVGVOL_5D_OIL__c += wellEvent.PVR_AVGVOL_5D_OIL__c;
                        if(wellEvent.PVR_AVGVOL_5D_SAND__c != null)
                            wellLocation.PVR_AVGVOL_5D_SAND__c += wellEvent.PVR_AVGVOL_5D_SAND__c;
                        if(wellEvent.PVR_AVGVOL_5D_WATER__c != null)
                            wellLocation.PVR_AVGVOL_5D_WATER__c += wellEvent.PVR_AVGVOL_5D_WATER__c;
                        if(wellEvent.PVR_Hours_On_Prod__c != null)
                            wellLocation.PVR_Hours_On_Prod__c += wellEvent.PVR_Hours_On_Prod__c;
                        if(wellEvent.PVR_Produced_Cond__c != null)
                            wellLocation.PVR_Produced_Cond__c += wellEvent.PVR_Produced_Cond__c;
                        if(wellEvent.PVR_Produced_Gas__c != null)
                            wellLocation.PVR_Produced_Gas__c += wellEvent.PVR_Produced_Gas__c;
                        if(wellEvent.PVR_Produced_Oil__c != null)
                            wellLocation.PVR_Produced_Oil__c += wellEvent.PVR_Produced_Oil__c;
                        if(wellEvent.PVR_Produced_Record_Count__c != null)
                            wellLocation.PVR_Produced_Record_Count__c += wellEvent.PVR_Produced_Record_Count__c;
                        if(wellEvent.PVR_Produced_Sand__c != null)
                            wellLocation.PVR_Produced_Sand__c += wellEvent.PVR_Produced_Sand__c;
                        if(wellEvent.PVR_Produced_Water__c != null)
                            wellLocation.PVR_Produced_Water__c += wellEvent.PVR_Produced_Water__c;
                        //mz: 20.3.2018 Gas Vent        
                        if(wellEvent.PVR_GOR_Factor__c != null)
                            wellLocation.PVR_GOR_Factor__c += wellEvent.PVR_GOR_Factor__c;
                        if(wellEvent.PVR_Fuel_Consumption__c != null)
                            wellLocation.PVR_Fuel_Consumption__c += wellEvent.PVR_Fuel_Consumption__c;
                        if(wellEvent.Measured_Vent_Rate__c != null)
                            wellLocation.Measured_Vent_Rate__c += wellEvent.Measured_Vent_Rate__c;  
                        if(wellEvent.GOR_Test_Date__c != null 
                            && ((wellEvent.GOR_Test_Date__c != wellLocation.GOR_Test_Date__c 
                            && wellEvent.GOR_Test_Date__c > wellLocation.GOR_Test_Date__c)
                                || wellLocation.GOR_Test_Date__c == null))
                            wellLocation.GOR_Test_Date__c = wellEvent.GOR_Test_Date__c;
                        if(wellEvent.GOR_Effective_Date__c != null 
                            && ((wellEvent.GOR_Effective_Date__c != wellLocation.GOR_Effective_Date__c 
                            && wellEvent.GOR_Effective_Date__c > wellLocation.GOR_Effective_Date__c)
                                || wellLocation.GOR_Effective_Date__c == null))
                            wellLocation.GOR_Effective_Date__c = wellEvent.GOR_Effective_Date__c;    
                        if(wellEvent.Monthly_Trucked_Oil__c != null)
                            wellLocation.Monthly_Trucked_Oil__c += wellEvent.Monthly_Trucked_Oil__c;
                        if(wellEvent.Single_Well_Battery__c != null && wellLocation.Single_Well_Battery__c != 'N/A') {
                            String wellEventStatus = wellEvent.Single_Well_Battery__c ? 'YES' : 'NO';
                            if(wellLocation.Single_Well_Battery__c == null || wellLocation.Single_Well_Battery__c == wellEventStatus)
                                wellLocation.Single_Well_Battery__c = wellEventStatus;
                            else wellLocation.Single_Well_Battery__c = 'N/A';
                        }    
                        
                        //last updated date of well location is set to the oldest update date on well events
                        //this is used to check if well location has the updated well events PVR information
                        wellLocation.PVR_AVGVOL_UPDATED__c = wellEvent.PVR_AVGVOL_UPDATED__c;
                        wellLocation.PVR_Month_To_Day_Generated__c = (wellEvent.PVR_Month_To_Day_Generated__c != null) ?
                            wellEvent.PVR_Month_To_Day_Generated__c : wellLocation.PVR_Month_To_Day_Generated__c;
                    }
                }

                //5 Days Production - Sum of 5 Days for Oil, Water and Sand
                wellLocation.PVR_AVGVOL_5D_PROD__c = wellLocation.PVR_AVGVOL_5D_OIL__c + wellLocation.PVR_AVGVOL_5D_WATER__c +
                                                     wellLocation.PVR_AVGVOL_5D_SAND__c;
                //30 Days Production - Sum of 30 Days for Oil, Water and Sand
                wellLocation.PVR_AVGVOL_30D_PROD__c = wellLocation.PVR_AVGVOL_30D_OIL__c + wellLocation.PVR_AVGVOL_30D_WATER__c +
                                                     wellLocation.PVR_AVGVOL_30D_SAND__c;
            }
        }
    }

    public static void updateProductStrategy(List<Location__c> wellLocationList) {
        for(Location__c wellLocation : wellLocationList){
            Boolean shutInFound = false;

            List<Well_Event__c> wellEventList = wellLocation.Well_Events__r;
            if(wellEventList != null && !wellEventList.isEmpty()) {
                //Need to go in reverse order of default sorting
                for(Integer i=wellEventList.size()-1; i >= 0; i--) {
                    Well_Event__c wellEvent = wellEventList[i];
                    if(wellEvent.Status__c == 'PROD') {
                        wellLocation.Product_Strategy__c = wellEvent.Product_Strategy__c;
                        break; //Producing is highest priority
                    }
                    if(wellEvent.Status__c == 'SHUT') {
                        wellLocation.Product_Strategy__c = wellEvent.Product_Strategy__c;
                        shutInFound = true;
                    }
                    if(wellEvent.Status__c == 'SUSP' && !shutInFound) //The sorting order will make sure the highest number event populates the product strategy
                        wellLocation.Product_Strategy__c = wellEvent.Product_Strategy__c;
                }
            }
        }
    }

    public static List<Database.SaveResult> commitToDB(List<Location__c> wellLocationList) {
        System.debug('commitToDB');
        List<Database.SaveResult> updateErrorResults = new List<Database.SaveResult>();
        
        //Update well locations
        List<Database.SaveResult> allResults = Database.update(wellLocationList, false);

        //Accumulate errors to send them back
        for(Database.SaveResult result : allResults) {
            if(!result.isSuccess())
                updateErrorResults.add(result);
        }

        return updateErrorResults;
    }
}