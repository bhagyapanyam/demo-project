/*------------------------------------------------------------------------------------------------------------
Author     : Marcel Brimus
Company    : Husky Energy
Description: HOG_ContentDocumentLinkTriggerHandler
Test Class : HOG_ContentDocumentLinkTriggHandlTest
History    :
			20.02.2019  mb  - created
-------------------------------------------------------------------------------------------------------------*/

public with sharing class HOG_ContentDocumentLinkTriggerHandler {
    /**
     * Trigger helper method for HOG Content Document Links.
     * Ran after insert.
     *
     * @param taskList
     */
    public static void handleBeforeInsert(List<ContentDocumentLink> contentDocumentLinks) {
        shareWorkOrderAndActivityFilesWithCommunityUsers(contentDocumentLinks);
    }

    /**
     * This will add access to files for Community users when files are attached by internal users
     *
     *
     * @param contentDocumentLinks
     */
    private static void shareWorkOrderAndActivityFilesWithCommunityUsers(List<ContentDocumentLink> contentDocumentLinks){
        Schema.DescribeSObjectResult workOrder = HOG_Maintenance_Servicing_Form__c.SObjectType.getDescribe();
        Schema.DescribeSObjectResult workOrderActivity = Work_Order_Activity__c.SObjectType.getDescribe();

        for(ContentDocumentLink cdl:contentDocumentLinks) {
            if(workOrder.getLocalName().equalsIgnoreCase(cdl.LinkedEntityId.getSobjectType().getDescribe().getLocalName())
                    || workOrderActivity.getLocalName().equalsIgnoreCase(cdl.LinkedEntityId.getSobjectType().getDescribe().getLocalName())) {
                cdl.ShareType = 'I';
                cdl.Visibility = 'AllUsers';
            }
        }
    }
}