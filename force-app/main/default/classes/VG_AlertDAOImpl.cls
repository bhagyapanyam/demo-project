/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of DAO layer
Test Class:     VG_AlertDAOImplTest
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
public inherited sharing class VG_AlertDAOImpl implements VG_AlertTriggerDAO {

    /**
     * Retrieves Alerts based on provided Ids and Set of Alert Types
     *
     * @param ids
     * @param types
     *
     * @return List<HOG_Vent_Gas_Alert__c>
     */
    public List<HOG_Vent_Gas_Alert__c> getAlertsForTriggerByType(Set<Id> ids, Set<String> types) {
        return [
                SELECT Name, Well_Location__c, Well_Location__r.Name, Type__c, Status__c, Comments__c
                        , Production_Engineer__c, Production_Engineer__r.Name, Priority__c, Start_Date__c
                FROM HOG_Vent_Gas_Alert__c
                WHERE Id IN :ids
                AND Type__c IN :types
        ];
    }

}