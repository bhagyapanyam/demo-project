/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Notification_Type_Priority__c
-------------------------------------------------------------------------------------------------*/
@isTest
public class NotificationTypePriorityTestData
{
    public static HOG_Notification_Type_Priority__c createNotificationTypePriority
        (Id notificationTypeId, Id servicePriorityId, Boolean workOrderPriority)
    {        

        HOG_Notification_Type_Priority__c results = new HOG_Notification_Type_Priority__c
            (
                HOG_Notification_Type__c = notificationTypeId,
                HOG_Service_Priority__c = servicePriorityId,
                Work_Order_Priority__c = workOrderPriority                
            );

        return results;
    }
}