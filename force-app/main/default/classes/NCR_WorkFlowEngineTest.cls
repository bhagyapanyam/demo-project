/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
public class NCR_WorkFlowEngineTest{

    static testMethod void testNCR_WorkFlowEngineConstructor(){
        User ncrAdminUser = NCRUserTestData.createNCRAdminUser();
                    
        System.runAs(ncrAdminUser){
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            //Business_Unit__c bu = NCR_CAR_TestData.createBusinessUnit('Business Unit');
            //Milestone1_Project__c p = NCR_CAR_TestData.createProject('Project Name',bu);
                    
            System.AssertNotEquals(ncrAdminUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null); 
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
            
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true);  
            
            //insert ncrCase;
            System.AssertNotEquals(ncrCase.Id, null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);
            System.AssertNotEquals((sObject)standardController.getRecord(), null);
            
            
            NCR_WorkFlowEngine workFlowEngine = new NCR_WorkFlowEngine(standardController);   
            
            List<String> viewScreenStateList = workFlowEngine.getViewScreenStateList();
            Map<String,String> viewScreenStateMap = workFlowEngine.getViewScreenStateMap();
             
            System.AssertNotEquals(viewScreenStateList, null);    
            System.AssertNotEquals(viewScreenStateMap, null); 
            
            PageReference cancelEdit = workFlowEngine.NCR_CancelEdit();
            PageReference ncrEdit = workFlowEngine.NCR_Edit();
            System.AssertEquals(cancelEdit, null);
            System.AssertEquals(ncrEdit, null);
            
            PageReference genericAction = workFlowEngine.NCR_GenericAction(0);
            System.AssertEquals(genericAction, null);
            
            PageReference ncrSave = workFlowEngine.NCR_Save();
            System.AssertNotEquals(ncrSave, null);   
            
            PageReference ncrExit = workFlowEngine.exit();
            System.AssertNotEquals(ncrExit, null);                      
                                                           
        }
    }
}