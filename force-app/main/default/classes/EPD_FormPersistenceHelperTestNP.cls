/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel Unit Test for EPD_FormPersistenceHelper
History:        jschn 2019-07-25 - Created.
*************************************************************************************************/
@IsTest
private class EPD_FormPersistenceHelperTestNP {

    @IsTest
    static void persistEPD_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEPD(epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistEPD_withEmptyParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEPD(epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistEPD_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD(false);

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEPD(epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(epd.Id));
    }

    @IsTest
    static void persistPartReplacements_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructurePart> parts;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistPartReplacements(parts, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistPartReplacements_withEmptyParam1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructurePart> parts = new List<EPD_FormStructurePart>();
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Part_Replacement__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistPartReplacements(parts, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistPartReplacements_withEmptyParam2() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        List<EPD_FormStructurePart> parts;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistPartReplacements(parts, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistPartReplacements_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Part_Replacement__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Part_Replacement__c part = (EPD_Part_Replacement__c) HOG_SObjectFactory.createSObject(
                new EPD_Part_Replacement__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        List<EPD_FormStructurePart> parts = new List<EPD_FormStructurePart> {
                (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(part)
        };

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistPartReplacements(parts, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(parts.get(0).recordId));
    }

    @IsTest
    static void persistEOCs_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureEOC> EOCs;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEOCs(EOCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistEOCs_withEmptyParam1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureEOC> EOCs = new List<EPD_FormStructureEOC>();
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEOCs(EOCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistEOCs_withEmptyParam2() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        List<EPD_FormStructureEOC> EOCs;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEOCs(EOCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void persistEOCs_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        List<EPD_FormStructureEOC> EOCs = new List<EPD_FormStructureEOC> {
                (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord)
        };

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistEOCs(EOCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(EOCs.get(0).recordId));
    }

    @IsTest
    static void persistCOCStages_withoutParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureCOCStage> COCs;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCOCStages(COCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCOCStages_withEmptyParam1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureCOCStage> COCs = new List<EPD_FormStructureCOCStage>();
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCOCStages(COCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCOCStages_withEmptyParam2() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        List<EPD_FormStructureCOCStage> COCs;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCOCStages(COCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCOCStages_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Compressor_Operating_Condition_Stage__c COCRecord = (EPD_Compressor_Operating_Condition_Stage__c) HOG_SObjectFactory.createSObject(
                new EPD_Compressor_Operating_Condition_Stage__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        List<EPD_FormStructureCOCStage> COCs = new List<EPD_FormStructureCOCStage> {
                (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(COCRecord)
        };

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCOCStages(COCs, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(COCs.get(0).recordId));
    }

    @IsTest
    static void persistBlockInfosWithChildRecs_withoutParams() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureBlock> blockInfos;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistBlockInfosWithChildRecs(blockInfos, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistBlockInfosWithChildRecs_withEmptyParam1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd;
        List<EPD_FormStructureBlock> blockInfos = new List<EPD_FormStructureBlock>();
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistBlockInfosWithChildRecs(blockInfos, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistBlockInfosWithChildRecs_withEmptyParam2() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        EPD_Engine_Performance_Data__c epd = new EPD_Engine_Performance_Data__c();
        List<EPD_FormStructureBlock> blockInfos;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistBlockInfosWithChildRecs(blockInfos, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistBlockInfosWithChildRecs_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock wrappedBlock = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        wrappedBlock.cylinders = new List<EPD_FormStructureCylinder>();
        List<EPD_FormStructureBlock> blockInfos = new List<EPD_FormStructureBlock> {
                wrappedBlock
        };

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistBlockInfosWithChildRecs(blockInfos, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(blockInfos.get(0).recordId));
    }

    @IsTest
    static void persistBlockInfosWithChildRecs_withProperParamWithChild() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c] + 1;
        Integer expectedCylinderRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c] + 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Cylinder_Information__c cylinderInfo = (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Cylinder_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock wrappedBlock = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        wrappedBlock.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinderInfo)
        };
        List<EPD_FormStructureBlock> blockInfos = new List<EPD_FormStructureBlock> {
                wrappedBlock
        };

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistBlockInfosWithChildRecs(blockInfos, epd);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCylinderRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(blockInfos.get(0).recordId));
        System.assertEquals(expectedCylinderRecordCount, finalCylinderRecordCount);
        System.assert(String.isNotBlank(blockInfos.get(0).cylinders.get(0).recordId));
    }

    @IsTest
    static void persistCylinderInfos_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureBlock> blocks;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCylinderInfos(blocks);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCylinderInfos_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureBlock> blocks;
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCylinderInfos(blocks);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCylinderInfos_withoutInnerParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInformation = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        blockInformation.Engine_Performance_Data__c = epd.Id;
        insert blockInformation;
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock> {
                (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInformation)
        };
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCylinderInfos(blocks);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCylinderInfos_withEmptyInnerParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInformation = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        blockInformation.Engine_Performance_Data__c = epd.Id;
        insert blockInformation;
        EPD_FormStructureBlock wrappedBlock = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInformation);
        wrappedBlock.cylinders = new List<EPD_FormStructureCylinder>();
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock> {
                wrappedBlock
        };
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCylinderInfos(blocks);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
    }

    @IsTest
    static void persistCylinderInfos_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInformation = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        blockInformation.Engine_Performance_Data__c = epd.Id;
        insert blockInformation;
        EPD_FormStructureBlock wrappedBlock = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInformation);
        EPD_Cylinder_Information__c cylinder = (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Cylinder_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        wrappedBlock.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder)
        };
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock> {
                wrappedBlock
        };
        Integer expectedRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c] + 1;

        Test.startTest();
        try {
            new EPD_FormPersistenceHelper().persistCylinderInfos(blocks);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        Integer finalRecordCount = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedRecordCount, finalRecordCount);
        System.assert(String.isNotBlank(blocks.get(0).cylinders.get(0).recordId));
        System.assertEquals(blocks.get(0).recordId, blocks.get(0).cylinders.get(0).blockInformationId);
    }

}