// this test tests the controller UploadAcctPkgController, called from apex page UploadAcctPkg.
// It also tests the trigger SetAcctDataUpload because that trigger fires when an attachment is inserted.
@isTest
public class UploadAcctPkgControllerTest
{
	// this one tests simply one file upload.  After that the MSO has a length of 1 in its
	// Accounting Data Upload related list.
	public static testMethod void test1Upload()
	{
		Account a = new Account(Name = 'Account1');
		insert a;
		
		Retail_Cluster__c rc = new Retail_Cluster__c(Name = 'RC1', Account__c = a.Id);
		insert rc;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rc);
		UploadAcctPkgController uapc = new UploadAcctPkgController(sc);
		
		Attachment att = uapc.attachment;
		att.Name = 'Test Attachment.csv';
		att.Body = Blob.valueOf('Test attachment body');
		att.Description = 'Test Description';
		uapc.upload();
		
		List<LOM_Accounting_Data_Upload__c> aduList = [SELECT Id, Success__c, File_Description__c, Filename__c 
			FROM LOM_Accounting_Data_Upload__c
			WHERE Multi_Site_Operator__c = :rc.Id];
		
		System.assertEquals(aduList.size(), 1);
		System.assertEquals(aduList[0].Success__c, true);
		System.assertEquals(1, [SELECT Count(Id) Counter FROM Attachment WHERE ParentId = :aduList[0].Id][0].get('Counter'));
		System.assertEquals(att.Description, aduList[0].File_Description__c);
		System.assertEquals(att.Name, aduList[0].Filename__c);
	}
	
	// this one tests upload of 30 files.  After that the MSO has a length of 100 in its
	// Accounting Data Upload related list.  Each of these Accounting Data Upload records has only
	// one Notes and Attachments record.
	public static testMethod void test30Uploads()
	{
		Account a = new Account(Name = 'Account2');
		insert a;
		
		Retail_Cluster__c rc = new Retail_Cluster__c(Name = 'RC2', Account__c = a.Id);
		insert rc;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rc);
		UploadAcctPkgController uapc = new UploadAcctPkgController(sc);
		
		for (Integer i = 0; i < 30; i++)
		{
			Attachment att = uapc.attachment;
			att.Name = 'Test Attachment ' + ('0' + i).right(2) + '.csv';
			att.Body = Blob.valueOf('Test attachment body ' + ('0' + i).right(2));
			att.Description = 'Test Description ' + ('0' + i).right(2);
			uapc.upload();
		}
		
		List<LOM_Accounting_Data_Upload__c> aduList = [SELECT Id, Success__c, File_Description__c, Filename__c 
			FROM LOM_Accounting_Data_Upload__c
			WHERE Multi_Site_Operator__c = :rc.Id
			ORDER BY Filename__c];
		
		System.assertEquals(aduList.size(), 30);
		for (Integer i = 0; i < 30; i++)
		{
			System.assertEquals(aduList[i].Success__c, true);
			System.assertEquals(1, [SELECT Count(Id) Counter FROM Attachment WHERE ParentId = :aduList[i].Id][0].get('Counter'));
			System.assertEquals('Test Description ' + ('0' + i).right(2), aduList[i].File_Description__c);
			System.assertEquals('Test Attachment ' + ('0' + i).right(2) + '.csv', aduList[i].Filename__c);
		}
	}
	
	// this one tests missing body or name, no upload was performed.
	public static testMethod void testEmptyFile()
	{
		Account a = new Account(Name = 'Account1');
		insert a;
		
		Retail_Cluster__c rc = new Retail_Cluster__c(Name = 'RC1', Account__c = a.Id);
		insert rc;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rc);
		UploadAcctPkgController uapc = new UploadAcctPkgController(sc);
		
		Attachment att = uapc.attachment;
		att.Description = 'Test Description';
		uapc.upload();
		
		List<LOM_Accounting_Data_Upload__c> aduList = [SELECT Id, Success__c FROM LOM_Accounting_Data_Upload__c
			WHERE Multi_Site_Operator__c = :rc.Id];
		
		System.assertEquals(aduList.size(), 0);
	}

	// this one tests file with wrong extension, so no upload was performed.
	public static testMethod void testWrongFileExtension()
	{
		Account a = new Account(Name = 'Account1');
		insert a;
		
		Retail_Cluster__c rc = new Retail_Cluster__c(Name = 'RC1', Account__c = a.Id);
		insert rc;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rc);
		UploadAcctPkgController uapc = new UploadAcctPkgController(sc);
		
		Attachment att = uapc.attachment;
		att.Name = 'Test Attachment.txt';
		att.Body = Blob.valueOf('Test attachment body but not csv file');
		att.Description = 'Test Description for a text not csv file';
		uapc.upload();
		
		List<LOM_Accounting_Data_Upload__c> aduList = [SELECT Id, Success__c, File_Description__c, Filename__c 
			FROM LOM_Accounting_Data_Upload__c
			WHERE Multi_Site_Operator__c = :rc.Id];
		
		System.assertEquals(aduList.size(), 0);
	}

}