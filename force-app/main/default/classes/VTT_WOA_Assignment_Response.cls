/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Enum that is used as response for some of the VTT_WOA_Assignment_Endpoint actions
                Currently there are 2 supported responses:
                    - SUCCESS - Success is returned if everything is done correctly without error
                    - INVALID_CASE - Invalid case is returned when action is not supported as
                                    there are more then one cases that can be run for single action.
                                    It's also use as indication for logic that action was invalid and
                                    it should try to run another part of logic instead.
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public enum VTT_WOA_Assignment_Response {
    SUCCESS,
    INVALID_CASE
}