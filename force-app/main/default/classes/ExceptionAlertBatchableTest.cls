@isTest
public with sharing class ExceptionAlertBatchableTest {

    @testSetup
    static void setup() {
        Test.startTest();
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message', 'INFO');
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message 1');
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message 2');
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message 3');
        Test.stopTest();
    }

    @isTest
    static void testAlertEmail() {
        String query = 'SELECT Id, Name, Status__c, Apex_Class_Trigger__c, Description__c, Log_Date__c FROM Error_Log__c WHERE (Status__c = null OR Status__c = \'Created\') AND Severity__c = \'ERROR\' AND Apex_Class_Trigger__c != null ORDER  BY CreatedDate DESC';

        Test.startTest();
        Database.executeBatch(new ExceptionAlertBatchable(query), 100);
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(4, errors.size());
    }
}