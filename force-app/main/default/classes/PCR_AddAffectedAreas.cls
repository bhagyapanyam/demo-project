global class PCR_AddAffectedAreas{
    
    @InvocableMethod
    public static void addAffectedAreas(List<String> options) {
        Map<String, Id> affectedMap = new Map<String, Id>();
        List<CR_Affected_Program_Area__c> cra = new List<CR_Affected_Program_Area__c>();
        
        for(Project__c p : [select id,name from Project__c]){
            affectedMap.put(p.Name, p.id);
        }
        
        String selectedOptions = options.get(0);
        String[] optionArray = selectedOptions.split(';');
        
        for(String s : optionArray) {
            CR_Affected_Program_Area__c a = new CR_Affected_Program_Area__c();
            a.Name = s;
            a.Change_Request__c = ApexPages.currentPage().getParameters().get('id');
            a.Project__c = affectedMap.get(s.trim());
            a.ChangeRequestAndProgramAreaKey__c = ApexPages.currentPage().getParameters().get('id') + affectedMap.get(s.trim());
            
            cra.add(a);

        }
       
        try{           
            upsert cra;       
        } catch(Exception exp){
            
        }    
    }
}