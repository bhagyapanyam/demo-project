@isTest
private class AccountLocatorControllerTest {
    @isTest private static void testAccountProcessor(){    
        AccountLocatorController controller = new AccountLocatorController();
        controller.Name = 'test';
        controller.City = 'test City';
        controller.Zip = 'test Zip'; 
    	controller.State = 'test State'; 
        test.startTest();
        Account account = new Account (
            Name= 'Unit test',
            BillingCity = 'test City',
            Billinglatitude = 1.0,
            Billinglongitude = 1.0,
            BillingStreet = 'test street',
            BillingCountry = 'test Country'
        );
        insert account;
        test.stopTest();

        List<SelectOption> cities = controller.Cities;
		List<SelectOption> states = controller.States;
        system.assertEquals('test',  controller.Name);
        system.assertEquals('test City',  controller.City);
        system.assertEquals('test Zip',  controller.Zip);
        system.assertEquals('test State',  controller.State);
    }
}