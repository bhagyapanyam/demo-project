@isTest
private class TestServiceRequestNotificationJob
{
    @isTest
    static void ServiceRequestNotificationJob_Test()
    {
        Test.startTest();
    
        ServiceRequestNotificationJob srnJob = new ServiceRequestNotificationJob();

        String chron = '0 0 23 * * ?';        
        System.schedule('Test Sched', chron, srnJob);
        
        Test.stopTest();        
    }
}