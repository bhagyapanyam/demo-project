/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentTransferItemSelection page which is to display available Equipments on Equipment Transfer Request

Test Class:    EquipmentTransferFormTest
History: 	   
			   27-Mar-15 Gangha Kaliyan  To include facilities in Equipment Transfer Request
			   27-Feb-17 Miro Zelina - included Well Events into Equipment Transfer Request
			   29-Jun-17 Miro Zelina - included System & Sub-System into Equipment Transfer Request
------------------------------------------------------------*/
public with sharing class EquipmentTransferItemSelectionController {
    
    public String searchString {get;set;}
    
    public Equipment__c[] shoppingCart {get;set;}
    public Equipment__c[] AvailableEquips {get;set;}
  
    public String toSelect {get; set;}
    
    public Boolean overLimit {get;set;}
  
    public String selectedEquipIds {get;set;} 
    
    private String fromLocId;
    private String toLocId;
    
    private String fromFacId;
    private String toFacId;
    
    private String fromEventId;
    private String toEventId;
    
    private String fromSysId;
    private String toSysId;
    
    private String fromSubSysId;
    private String toSubSysId;

    private String fromYardId;
    private String toYardId;

    private String fromFELId;
    private String toFELId;
    
    public EquipmentTransferItemSelectionController()
    {

        shoppingCart = new List<Equipment__c>();
        selectedEquipIds = ApexPages.currentPage().getParameters().get('equipId');
        
        //either location or facility or well event can be passed as parameters
        fromLocId = ApexPages.currentPage().getParameters().get('fromLocId'); 
        toLocId = ApexPages.currentPage().getParameters().get('toLocId'); 
        
  		fromFacId = ApexPages.currentPage().getParameters().get('fromFacId');
  		toFacId = ApexPages.currentPage().getParameters().get('toFacId');
  		
  		fromEventId = ApexPages.currentPage().getParameters().get('fromEventId');
  		toEventId = ApexPages.currentPage().getParameters().get('toEventId');
  		
  		fromSysId = ApexPages.currentPage().getParameters().get('fromSystemId');
  		toSysId = ApexPages.currentPage().getParameters().get('toSystemId'); 
  		
  		fromSubSysId = ApexPages.currentPage().getParameters().get('fromSubSysId'); 
  		toSubSysId = ApexPages.currentPage().getParameters().get('toSubSysId'); 

        fromYardId = ApexPages.currentPage().getParameters().get('fromYardId'); 
        toYardId = ApexPages.currentPage().getParameters().get('toYardId'); 

        fromFELId = ApexPages.currentPage().getParameters().get('fromFELId'); 
        toFELId = ApexPages.currentPage().getParameters().get('toFELId'); 
  		
  		system.debug('location Ids '+fromLocId +' : '+toLocId);
  		system.debug('facility Ids '+fromFacId +' : '+toFacId);
  		system.debug('well event Ids '+fromEventId +' : '+toEventId);
  		system.debug('system Ids '+fromSysId +' : '+toSysId);
  		system.debug('sub-system Ids '+fromSubSysId +' : '+toSubSysId);
        
        updateAvailableList();
    }
    
    public void updateAvailableList() 
    {    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Name, Equipment_Number__c, Description_of_Equipment__c, Location__r.Name, Well_Event__r.Name, ' +
                         'Manufacturer__c, Model_Number__c, Manufacturer_Serial_No__c, Tag_Number__c, Facility__r.Name, ' +
                         'System__r.Name, Sub_System__r.Name, ' +
                         'Functional_Equipment_Level__r.Name, Yard__r.Name ' +
                         'from Equipment__c where Name != null ';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (Description_of_Equipment__c like \'%' + searchString 
                + '%\' or Manufacturer_Serial_No__c like \'%' + searchString 
                + '%\' or Manufacturer__c like  \'%' + searchString 
                + '%\' or Equipment_Number__c like  \'%' + searchString 
                + '%\' or Model_Number__c like \'%' + searchString;
                
            //set search string for both location and facility as both the columns are present in the list
            //if((fromLocId != null && fromLocId != '') || (toLocId != null && toLocId != ''))    
                qString += '%\' or Location__r.Name like \'%' + searchString;
                
            //if((fromFacId != null && fromFacId != '') || (toFacId != null && toFacId != ''))    
                qString += '%\' or Facility__r.Name like \'%' + searchString;
                
                qString += '%\' or Well_Event__r.Name like \'%' + searchString;
                
                qString += '%\' or System__r.Name like \'%' + searchString;   
                
                qString += '%\' or Sub_System__r.Name like \'%' + searchString;

                qString += '%\' or Functional_Equipment_Level__r.Name like \'%' + searchString; 

                qString += '%\' or Yard__r.Name like \'%' + searchString;        
                
            qString += '%\')'; 
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        if (shoppingCart != null)
        {
            for(Equipment__c d : shoppingCart){
                selectedEntries.add(d.Id);
            }
            
            if(selectedEntries.size() > 0){
                String tempFilter = ' and Id not in (';
                for(Id i : selectedEntries){
                    tempFilter+= '\'' + (String)i + '\',';
                }
                String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
                extraFilter+= ')';
                
                qString+= extraFilter;
            }
        }
        //exclude selected Equipments
        if (selectedEquipIds != null)
        {
            List<String> ids = selectedEquipIds.split(',');
            System.debug('ids =' + ids);
            qString += ' and Id not in: ids ';
        }
        
        //if from location is present, filter records using location
        if (fromLocId != null && fromLocId != '')
            qString += ' and Location__c =: fromLocId';
            
        //if from facility is present, filter records using facility
        if (fromFacId != null && fromFacId != '')
        	qString += ' and Facility__c =: fromFacId';
        	
        //if from well event is present, filter records using well event
        if (fromEventId != null && fromEventId != '')
        	qString += ' and Well_Event__c =: fromEventId';
        	
        //if from system is present, filter records using system
        if (fromSysId != null && fromSysId != '')
        	qString += ' and System__c =: fromSysId';	
        	
        //if from sub-system is present, filter records using sub-system
        if (fromSubSysId != null && fromSubSysId != '')
        	qString += ' and Sub_System__c =: fromSubSysId';

        //if from FEL is present, filter records using sub-system
        if (fromFELId != null && fromFELId != '')
            qString += ' and Functional_Equipment_Level__c =: fromFELId';

        //if from Yard is present, filter records using sub-system
        if (fromYardId != null && fromYardId != '')
            qString += ' and Yard__c =: fromYardId';   	
       
        qString += ' ORDER BY ' + String.escapeSingleQuotes(SortField) + ' ' + String.escapeSingleQuotes(SortDirection) ;
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableEquips = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableEquips.size()==101){
            AvailableEquips.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
        
        system.debug('qString '+ qString);
    }
    
    public void addToShoppingCart() {    
        // This function runs when a user hits "select" button next to a product  
      
              
        for(Equipment__c d : AvailableEquips){
            if((String)d.Id == toSelect){
                shoppingCart.add(d);
            //    selectedEquipIds += d.Id + ',';
                break;
            }
        }
        System.debug('addToShoppingCart selectedEquipIds =' + selectedEquipIds);
        updateAvailableList();  
    }    
    
    public PageReference onSave()
    {
        selectedEquipIds = '';
        for(Equipment__c d : shoppingCart)
            selectedEquipIds += d.Id + ',';
    
        System.debug('onSave EquipmentTransferToFormCtrl.equipmentList = ' + selectedEquipIds);
    
        return null;
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    /***
    * SortDirection - return sort direction. Default ascending(asc)
    ***/
    public String SortDirection {
        get { if (SortDirection == null) {  SortDirection = 'asc'; } return SortDirection;  }
        set;
    }
 
    /***
    * SortField - return sort by field. Default to Name
    ***/
    public String SortField {
        get { 
            if (SortField == null) 
            {
                SortField = 'Name'; 
            } 
            return SortField;  
        }
        set; 
    }
 
    /***
    * SortToggle - toggles the sorting of query from asc<-->desc
    ***/
    public void SortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
     
      
        updateAvailableList();
    }
}