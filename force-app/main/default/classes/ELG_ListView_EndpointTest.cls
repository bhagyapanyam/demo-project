/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/7/2019   
 */

@IsTest
public class ELG_ListView_EndpointTest {

	@IsTest
	static void getAllThermalAMUsTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c];
		amu.Planner_Group__c = '520';
		update amu;

		Test.startTest();
		ELG_ListView_Service.DataPayload result = ELG_ListView_Endpoint.getThermalAMUs(usr.Id);
		System.assert(!result.amus.isEmpty(), 'Creating object or query failure. List is Empty');
		System.assert(String.isBlank(result.defaultAMU), 'Value was passed to Object, should not be');

		insert new ELG_User_Setting__c(Operating_Field_AMU__c = amu.Id, User__c = usr.Id);
		result = ELG_ListView_Endpoint.getThermalAMUs(usr.Id);
		System.assert(String.isNotBlank(result.defaultAMU), 'Value was not passed to Object, parameter should have a value');
		Test.stopTest();

	}

	@IsTest
	static void getAMUPostsTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);

		Test.startTest();
		List<ELG_ListView_Service.TemporaryClassToBuildObject> result = ELG_ListView_Endpoint.getAMUsPosts(post.Operating_Field_AMU__c);
		System.assert(!result.isEmpty(), 'Creating object or query failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(post.Id, result[0].value, 'Value pass to Object failed (constructor)');
		System.assertEquals(post.Post_Name__c, result[0].label, 'Value pass to Object failed (constructor)');
		Test.stopTest();
	}

	@IsTest
	static void searchOnlyByAMUTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Shift_Assignement__c,
						Shift_Assignement__r.Post__r.Operating_Field_AMU__c
				FROM ELG_Shift_Handover__c
		];
		Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c];
		amu.Planner_Group__c = '520';
		update amu;

		Test.startTest();
		ELG_ListView_Service.FilterWrapper handoverForm = new ELG_ListView_Service.FilterWrapper();
		handoverForm.amuId = createdHandover.Shift_Assignement__r.Post__r.Operating_Field_AMU__c;

		List<ELG_ListView_Service.DataTableWrapper> result = ELG_ListView_Endpoint.getSearchResults(handoverForm,
				0,
				'CreatedDate',
				'DESC',
				false,
				false,
				false,
				false);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(createdHandover.Id, result[0].handId, 'Value pass to object failure');
		Test.stopTest();
	}

	@IsTest
	static void searchByMoreParametersTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id, Name FROM User WHERE Alias = 'theOne'];
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Outgoing_Operator__c,
						Shift_Assignement__c,
						Shift_Assignement__r.Post__c,
						Shift_Assignement__r.Post__r.Operating_Field_AMU__c
				FROM ELG_Shift_Handover__c
		];

		Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c];
		amu.Planner_Group__c = '520';
		update amu;
		shift.Primary__c = usr.Id;
		shift.Shift_Cycle__c = 'Day';
		shift.Shift_Takeover_Reason__c = 'Something';
		update shift;
		createdHandover.Status__c = ELG_Constants.HANDOVER_ACCEPTED;
		update createdHandover;


		Test.startTest();
		ELG_ListView_Service.FilterWrapper handoverForm = new ELG_ListView_Service.FilterWrapper();
		String startDate = String.valueOf(Date.today() - 10);
		String endDate = String.valueOf(Date.today());

		handoverForm.amuId = createdHandover.Shift_Assignement__r.Post__r.Operating_Field_AMU__c;
		handoverForm.primaryId = usr.Id;
		handoverForm.handoverStatus = ELG_Constants.HANDOVER_ACCEPTED;
		handoverForm.postId = createdHandover.Shift_Assignement__r.Post__c;
		handoverForm.shiftCycle = 'Day';
		handoverForm.startDate = startDate;
		handoverForm.endDate = endDate;

		List<ELG_ListView_Service.DataTableWrapper> result = ELG_ListView_Endpoint.getSearchResults(
				handoverForm,
				0,
				'CreatedDate',
				'DESC',
				false,
				false,
				false,
				false);

		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(createdHandover.Id, result[0].handId, 'Value pass to object failure');
		System.assertEquals(usr.Name, result[0].outgoingOperator, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.HANDOVER_ACCEPTED, result[0].handoverStatus, 'Value pass to object failure');
		System.assertEquals(post.Post_Name__c, result[0].postName, 'Value pass to object failure');
		System.assertEquals('Day', result[0].shiftCycle, 'Value pass to object failure');
		Test.stopTest();

	}

	@IsTest
	static void searchByMoreParametersWithEngineerTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Steam', true);
		post.Shift_Engineer_Review_Required__c = true;
		update post;
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id, Name FROM User WHERE Alias = 'theOne'];
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Outgoing_Operator__c,
						Shift_Assignement__c,
						Shift_Assignement__r.Post__c,
						Shift_Assignement__r.Post__r.Operating_Field_AMU__c
				FROM ELG_Shift_Handover__c
		];

		Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c];
		amu.Planner_Group__c = '520';
		update amu;
		shift.Primary__c = usr.Id;
		shift.Shift_Engineer__c = usr.Id;
		shift.Shift_Cycle__c = 'Day';
		shift.Shift_Takeover_Reason__c = 'Something';
		update shift;
		createdHandover.Status__c = ELG_Constants.HANDOVER_ACCEPTED;
		update createdHandover;


		Test.startTest();
		ELG_ListView_Service.FilterWrapper handoverForm = new ELG_ListView_Service.FilterWrapper();
		handoverForm.amuId = createdHandover.Shift_Assignement__r.Post__r.Operating_Field_AMU__c;
		handoverForm.primaryId = usr.Id;
		handoverForm.handoverStatus = ELG_Constants.HANDOVER_ACCEPTED;
		handoverForm.postId = createdHandover.Shift_Assignement__r.Post__c;
		handoverForm.shiftCycle = 'Day';
		handoverForm.shiftEngineerId = usr.Id;

		List<ELG_ListView_Service.DataTableWrapper> result = ELG_ListView_Endpoint.getSearchResults(handoverForm,
				0,
				'CreatedDate',
				'DESC',
				true,
				true,
				true,
				true);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(createdHandover.Id, result[0].handId, 'Value pass to object failure');
		System.assertEquals(usr.Name, result[0].outgoingOperator, 'Value pass to object failure');
		System.assertEquals(usr.Name, result[0].shiftEngineer, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.HANDOVER_ACCEPTED, result[0].handoverStatus, 'Value pass to object failure');
		System.assertEquals(post.Post_Name__c, result[0].postName, 'Value pass to object failure');
		System.assertEquals('Day', result[0].shiftCycle, 'Value pass to object failure');
		//System.assertEquals(ELG_Constants.AWAITING_SHIFT_ENGINEER_ACCEPTANCE, result[0].awaitingReviewText, 'Value pass to object failure');

		createdHandover.Shift_Engineer__c = usr.Id;
		update createdHandover;
		result = ELG_ListView_Endpoint.getSearchResults(handoverForm,
				0,
				'CreatedDate',
				'DESC',
				true,
				false,
				false,
				false);
		System.assertEquals(true, result[0].hasError, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.NO_HANDOVER_FOUND, result[0].errorMsg, 'Value pass to object failure');
		Test.stopTest();

	}

	@IsTest
	static void tooManyRecordsForOffsetTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		List<ELG_Shift_Handover__c> createdHandover = [
				SELECT Id,
						Shift_Assignement__c,
						Shift_Assignement__r.Post__r.Operating_Field_AMU__c
				FROM ELG_Shift_Handover__c
		];

		Test.startTest();
		Integer numberOfHandovers = 2500;
		List<ELG_ListView_Service.DataTableWrapper> result = ELG_ListView_Service.objectBuilder(createdHandover, numberOfHandovers);
		System.assertEquals(true, result[0].hasError, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.maximumQueryLimit('Handovers', numberOfHandovers), result[0].errorMsg, 'Value pass to object failure');
		Test.stopTest();
	}

	@IsTest
	static void massHandoverReviewersUpdateTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Steam', true);
		post.Shift_Engineer_Review_Required__c = true;
		update post;

		ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		List<ELG_Shift_Handover__c> handovers = [
				SELECT Id,
						Sr_Supervisor__c,
						Reviewed_by_Sr_Supervisor_Timestamp__c,
						Steam_Chief__c,
						Reviewed_by_Steam_Chief_Timestamp__c,
						Shift_Lead__c,
						Reviewed_by_Shift_Lead_Timestamp__c,
						Shift_Engineer__c,
						Reviewed_by_Shift_Engineer_Timestamp__c,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c
				FROM ELG_Shift_Handover__c
		];

		List<ELG_Shift_Handover__c> toUpdate = new List<ELG_Shift_Handover__c>();
		for (ELG_Shift_Handover__c handover : handovers) {
			handover.Status__c = ELG_Constants.HANDOVER_ACCEPTED;
			toUpdate.add(handover);
		}

		update toUpdate;

		Test.startTest();
		List<String> handoverIds = new List<String>();

		for (ELG_Shift_Handover__c handover : handovers) {
			handoverIds.add(handover.Id);
		}

		result = ELG_ListView_Endpoint.massHandoverReviewersUpdate(
				handoverIds,
				usr.Id,
				true,
				true,
				true,
				true);

		System.assertNotEquals(null, result.resultObjects[0], '');
		System.assertEquals('2 Handovers has been successfully updated.', result.resultObjects[0], '');

		Test.stopTest();
	}

	@IsTest
	static void massHandoverReviewersUpdateNothingToUpdateTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];


		Test.startTest();
		List<String> handoverIds = new List<String>();

		result = ELG_ListView_Endpoint.massHandoverReviewersUpdate(
				handoverIds,
				usr.Id,
				true,
				true,
				true,
				true);

		System.assertNotEquals(null, result.resultObjects[0], 'Value was no passed correctly.');
		System.assertEquals('There are no Handovers to updated based on the selections.', result.resultObjects[0], 'Value was not passed correctly.');

		Test.stopTest();
	}

	@IsTest
	static void massHandoverReviewersUpdateNegativeTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];


		Test.startTest();
		result = ELG_ListView_Endpoint.massHandoverReviewersUpdate(
				null,
				usr.Id,
				true,
				true,
				true,
				true);

		System.assertEquals(null, result.resultObjects, 'Value was passed to object event when no value was queried.');
		System.assertEquals(false, result.errors.isEmpty(), 'Attempt to de-reference a null object from Exception is missing in errors');

		Test.stopTest();
	}

	/*Test Data*/
	@TestSetup
	static void testData() {
		ELG_TestDataFactory.createReviewerAndLogEntryUsers();
	}

}