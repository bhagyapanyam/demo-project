/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for EPD_BlockInformationSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class EPD_BlockInformationSelectorTest {

    @IsTest
    static void getBlockInformationByEPDs_withoutParam() {
        List<EPD_Block_Information__c> result;
        Integer expectedSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationSelector().getBlockInformationByEPDs(null);
        Test.stopTest();

        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDs_withEmptyParam() {
        List<EPD_Block_Information__c> result;
        Integer expectedSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationSelector().getBlockInformationByEPDs(new Set<Id>());
        Test.stopTest();

        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDs_withWrongParam() {
        List<EPD_Block_Information__c> result;
        Integer expectedSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationSelector().getBlockInformationByEPDs(new Set<Id> { UserInfo.getUserId() } );
        Test.stopTest();

        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDs_withProperParam() {
        List<EPD_Block_Information__c> result;
        Id epdId = EPD_TestData.createEPD().Id;
        Integer expectedSize = 1;

        Test.startTest();
        result = new EPD_BlockInformationSelector().getBlockInformationByEPDs(new Set<Id> { epdId } );
        Test.stopTest();

        System.assertEquals(expectedSize, result.size());
    }

}