public with sharing class TeamAllocationUtilities {
    public static final String ACTIVE_STATUS_NAME = 'Active';
    public static final String ACTIVE_EXPIRED_STATUS_NAME = 'Active-Expired';
    public static final String INACTIVE_STATUS_NAME = 'Inactive';
    
    public static final Long ACTIVE_EXPIRED_DURATION_IN_HRS = 1;
    public static final Long ACTIVE_EXPIRED_DURATION_IN_MINS = ACTIVE_EXPIRED_DURATION_IN_HRS * 60;
    public static final Long ACTIVE_EXPIRED_DURATION_IN_SECS = ACTIVE_EXPIRED_DURATION_IN_MINS * 60;
    public static final Long ACTIVE_EXPIRED_DURATION_IN_MILLIS = ACTIVE_EXPIRED_DURATION_IN_SECS * 1000;
    
    private static final String START_TIME_INDICATOR = 'START';
    private static final String END_TIME_INDICATOR = 'END'; 
    
    /*********************************************************
     * Returns the start time for the specified team allocation
     * shift.
     * @param ta - The team allocation.
     * @param sc - The shift for the allocation.
     ********************************************************/
    public static Datetime getStartDatetime(Team_Allocation__c ta, Shift_Configuration__c sc) {
        return getDatetime(ta, sc, START_TIME_INDICATOR);
    }
    
    /*********************************************************
     * Returns the end time for the specified team allocation
     * shift.
     * @param ta - The team allocation.
     * @param sc - The shift for the allocation.
     ********************************************************/
    public static Datetime getEndDatetime(Team_Allocation__c ta, Shift_Configuration__c sc) {
        return getDatetime(ta, sc, END_TIME_INDICATOR);
    }
    
    /*****************************************************
     * Returns the start or end time for the specified 
     * team allocation and shift.
     * @param ta - The team allocation.
     * @param sc - The shift for the allocation.
     * @param StartOrEndIndicator - 'START' or 'END' 
     * indicating whether to compute the start or end time.
     *****************************************************/
    private static Datetime getDatetime(Team_Allocation__c ta, Shift_Configuration__c sc, String StartOrEndIndicator) {
        Integer startTime = Integer.valueof(sc.Start_Time__c);
        Integer endTime = Integer.valueof(sc.End_Time__c);
        
        Integer hr = 0;
        Integer min = 0;
        Boolean isNextDay = False;
        if ('START' == StartOrEndIndicator) {
            hr = startTime / 100;
            min = startTime - (hr * 100);
        } else if ('END' == StartOrEndIndicator) {
            hr = endTime / 100;
            min = endTime - (hr * 100);
            isNextDay = (startTime > endTime);
        }
        
        Datetime result = Datetime.newInstance(ta.Date__c.year(), ta.Date__c.month(), ta.Date__c.day(), hr, min, 0);
        if (isNextDay) {
            result = result.addDays(1);
        }
        
        System.debug('\n*********************************************\n'
            + 'StartOrEndIndicator = ' + StartOrEndIndicator
            + '\n'
            + 'startTime = ' + startTime
            + '\n'
            + 'endTime = ' + endTime
            + '\n'
            + 'isNextDay = ' + isNextDay
            + '\n'
            + 'ta.Date__c.year() = ' + ta.Date__c.year() 
            + '\n'
            + 'ta.Date__c.month()= ' + ta.Date__c.month() 
            + '\n'
            + 'ta.Date__c.day() = ' + ta.Date__c.day()
            + '\n'
            + 'hr = ' + hr
            + '\n'
            + 'min = ' + min
            + '\n'
            + 'Computed Date = ' + result
            + '\n*********************************************\n');
        
        return result;
    }
    
    /**********************************************************
     * Returns the first active team allocation.
     **********************************************************/
    public static Team_Allocation__c getActiveTeamAllocation() {
        List<Team_Allocation__c> tas = getActiveTeamAllocations();
        return ((tas != null) && (tas.size() > 0)) ? tas.get(0) : null;
    }
    
    /**********************************************************
     * Returns the first active expired team allocation.
     **********************************************************/
    public static Team_Allocation__c getActiveExpiredTeamAllocation() {
        List<Team_Allocation__c> tas = getActiveExpiredTeamAllocations();
        return ((tas != null) && (tas.size() > 0)) ? tas.get(0) : null;
    }
    
    /**********************************************************
     * Returns all the active team allocations.
     **********************************************************/
    public static List<Team_Allocation__c> getActiveTeamAllocations() {
        return getTeamAllocations(ACTIVE_STATUS_NAME);
    }
    
    /**********************************************************
     * Returns all the active expired team allocations.
     **********************************************************/
    public static List<Team_Allocation__c> getActiveExpiredTeamAllocations() {
        return getTeamAllocations(ACTIVE_EXPIRED_STATUS_NAME);
    }
    
    /**********************************************************
     * Returns all the in-active expired team allocations.
     **********************************************************/
    public static List<Team_Allocation__c> getInActiveTeamAllocations() {
        return getTeamAllocations(INACTIVE_STATUS_NAME);
    }
    
    public static List<Team_Allocation__c> getActiveTeamAllocationsByType(string rtName)
    {
    	return [
            SELECT 
                id,
                Status__c, 
                Date__c,
                Team__c, Team__r.Name,
                Shift__c, Shift__r.Name,
                Operating_District__c, Operating_District__r.Name,
                Team_Allocation_Name__c,
                Team_Allocation_DetailPage_HLink__c, 
                LastModifiedDate,
                LastModifiedById, LastModifiedBy.Name,
                Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
            FROM Team_Allocation__c
            WHERE Status__c = :ACTIVE_STATUS_NAME
                and RecordType.Name =: rtName
        ];
    }
    /**********************************************************
     * Returns all the team allocations with the specified status.
     * @param status - The team allocation status of interest.
     **********************************************************/
    public static List<Team_Allocation__c> getTeamAllocations(String status) {
        return [
            SELECT 
                id,
                Status__c,
                Date__c,
                Team__c, Team__r.Name,
                Shift__c, Shift__r.Name,
                Operating_District__c, Operating_District__r.Name,
                Team_Allocation_Name__c,
                Team_Allocation_DetailPage_HLink__c, 
                LastModifiedDate,
                LastModifiedById, LastModifiedBy.Name,
                Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
            FROM Team_Allocation__c
            WHERE Status__c = :status
            ORDER BY Date__c DESC, Team_Allocation_Name__c DESC
           limit 3000
        ];
    }

    
    /**********************************************************
     * Returns the team allocations with the specified Id.
     * @param teamAllocationId - The team allocation of interest.
     **********************************************************/
    public static Team_Allocation__c getTeamAllocation(Id teamAllocationId) {
        return [
            SELECT
                id,
                Status__c,
                Date__c,
                Team__c, Team__r.Name,
                Shift__c, Shift__r.Name,
                Operating_District__c, Operating_District__r.Name,
                Team_Allocation_Name__c,
                Team_Allocation_DetailPage_HLink__c, 
                LastModifiedDate,
                LastModifiedById, LastModifiedBy.Name,
                Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
            FROM Team_Allocation__c
            WHERE Id = :teamAllocationId
        ];
    }
        
    /**********************************************************
     * Returns all the operational events for the specified team 
     * allocation.
     * @param teamAllocationId - The team allocation of interest.
     **********************************************************/
    public static List<Operational_Event__c> getOperationalEvents(Id teamAllocationId) {
        return [
            SELECT
                Id,
                Name,
                Description__c,
                Operating_District__c,
                Operational_Event_DetailPage_HLink__c,
                Equipment__c,
                Operator_Contact__c,
                Team_Allocation__c,
                Time__c,
                Unit__c,
                Area__c,
                LastModifiedDate,
                LastModifiedById               
            FROM Operational_Event__c
            WHERE Team_Allocation__c = :teamAllocationId            
        ];
    }
     
    /***************************************************************
     * Returns all the team allocations that have not been approved.
     **********************************************************/
    public static List<Team_Allocation__c> getUnapprovedTeamAllocations() {
        return [
            SELECT 
                id,
                Status__c,
                Date__c,
                Team__c, Team__r.Name,
                Shift__c, Shift__r.Name,
                Operating_District__c, Operating_District__r.Name,
                Team_Allocation_Name__c,
                Team_Allocation_DetailPage_HLink__c, 
                LastModifiedDate,
                LastModifiedById, LastModifiedBy.Name,
                Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
            FROM Team_Allocation__c
            WHERE Approver__c = null 
            ORDER BY Date__c DESC, Team_Allocation_Name__c DESC
            limit 3000
        ];
    }    
}