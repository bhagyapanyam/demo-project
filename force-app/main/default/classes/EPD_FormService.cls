/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding form structure building.
Test Class:     EPD_FormServiceTest
                EPD_FormServiceTestNP
History:        jschn 2019-05-27 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormService {

    private static final String CLASS_NAME = String.valueOf(EPD_FormService.class);

    /**
     * Handles Loading for Existing EPD record.
     * If record is submitted and it's not a direct load, it will throw an error.
     * If record is not yet submitted, it will load form based on the provided record.
     *
     * @param epdRecord
     * @param loadMode
     *
     * @return EPD_Response
     */
    public EPD_Response handleLoadForExistingEPD(EPD_Engine_Performance_Data__c epdRecord, EPD_LoadMode loadMode) {
        EPD_Response response;

        if(epdRecord.Submitted__c && !EPD_LoadMode.DIRECT_LOAD.equals(loadMode)) {
            System.debug(CLASS_NAME + ' -> handleLoadForExistingEPD. Submitted record: ' + epdRecord.Submitted__c);

            response = new EPD_Response(Label.EPD_Response_Already_Submitted);

        } else {
            System.debug(CLASS_NAME + ' -> handleLoadForExistingEPD. Create structure.');

            response = createFormStructureFromRecord(epdRecord);
        }

        System.debug(CLASS_NAME + ' -> handleLoadForExistingEPD. Response: ' + JSON.serialize(response));
        return response;
    }

    /**
     * This method handles creating response with structure for request.
     * This method also sets hasEditAccess flag for EPD record.
     * This is due most of the users has access to view form but only few have ability to edit it.
     * As we are not using SObjects, front end is purely dependent on this flag to restrict access to edit.
     * If we don't set this flag, it will allow users to go to edit mode, but it will fail on save action which is not
     * desired behaviour.
     *
     * @param epdRecord
     *
     * @return EPD_Response
     */
    private EPD_Response createFormStructureFromRecord(EPD_Engine_Performance_Data__c epdRecord) {

        System.debug(CLASS_NAME + ' -> createFormStructureFromRecord. EPD: ' + JSON.serialize(epdRecord));

        List<EPD_FormStructure> formStructures = new EPD_EPDService().getFormStructuresForEPDs(
                new List<EPD_Engine_Performance_Data__c> { epdRecord }
        );

        EPD_Response response;
        if(EPD_Utility.isSingleRecord(formStructures)) {

            applyEditAccessFlagForStructure(formStructures.get(0));

            response = new EPD_Response(formStructures.get(0));

        } else {
            response = new EPD_Response(
                    String.format(
                            Label.EPD_Unexpected_Record_Count,
                            new List<String> {
                                    'Engine Performance Data',
                                    epdRecord.Id
                            }
                    )
            );
        }

        System.debug(CLASS_NAME + ' -> createFormStructureFromRecord. Form structure: ' + JSON.serialize(formStructures));
        System.debug(CLASS_NAME + ' -> createFormStructureFromRecord. Response: ' + JSON.serialize(response));

        return response;
    }

    /**
     * Sets hasEditAccess for EPD structure.
     *
     * @param formStructure
     */
    private void applyEditAccessFlagForStructure(EPD_FormStructure formStructure) {
        formStructure.epd.hasEditAccess = 0 < new HOG_UserRecordAccessSelector().countOfUserEditAccessPerSObject(
                        formStructure.epd.recordId,
                        UserInfo.getUserId()
                );
    }

    /**
     * Handles load request with creating new EPD form structure based on related records.
     * This method uses Structure Builder to do that.
     * First it needs to load related records of course.
     *
     * @param workOrderActivityId
     *
     * @return EPD_Response
     */
    public EPD_Response handleLoadForNewEPD(Id workOrderActivityId) {
        EPD_Response response;
        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. Querying records.');

        Work_Order_Activity__c woa = new VTT_WorkOrderActivityService().getRecord(workOrderActivityId);
        Equipment_Engine__c engine;
        try{
            engine = new EquipmentEngineService().getEngineByEquipmentId(woa.Equipment__c);
        } catch (HOG_Exception ex) {
            engine = new EquipmentEngineService().getEngineByEquipmentId(woa.Maintenance_Work_Order__r.Equipment__c);
        }

        EPD_Engine__mdt engineConfig = new EPD_EngineConfigService().getEngineConfig(
                engine.Manufacturer__c,
                engine.Model__c
        );

        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. WOA: ' + JSON.serialize(woa));
        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. WO: ' + JSON.serialize(woa.Maintenance_Work_Order__r));
        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. Company: ' + JSON.serialize(woa.Assigned_Vendor__r));
        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. Engine: ' + JSON.serialize(engine));
        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. Engine Config: ' + JSON.serialize(engineConfig));

        response = new EPD_Response(
                new EPD_FormStructureBuilder()
                        .setConfig(engineConfig)
                        .setWorkOrderActivity(woa)
                        .setWorkOrder(woa.Maintenance_Work_Order__r)
                        .setCompany(woa.Assigned_Vendor__r)
                        .setEngine(engine)
                        .build()
        );

        System.debug(CLASS_NAME + ' -> handleLoadForNewEPD. Response: ' + JSON.serialize(response));
        return response;
    }

    /**
     * This method works in bulk. It creates list of structures based on provided list of EPD records and block Information
     * mapped by EPD Id.
     * This method uses setters from every Structure class to map values correctly.
     *
     * @param epdRecords
     * @param blockInformationByEPDId
     *
     * @return List<EPD_FormStructure>
     */
    public List<EPD_FormStructure> generateFormStructures(List<EPD_Engine_Performance_Data__c> epdRecords,
            Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId) {

        System.debug(CLASS_NAME + ' -> generateFormStructures. START.');

        List<EPD_FormStructure> structures = new List<EPD_FormStructure>();
        Map<String, EPD_Engine__mdt> engineConfigsByManufacturerAndModel = new EPD_EngineConfigService().getConfigsMap();

        for(EPD_Engine_Performance_Data__c epd : epdRecords) {
            System.debug(CLASS_NAME + ' -> generateFormStructures. Generating for:' + JSON.serialize(epd));

            EPD_Engine__mdt engineConfig = engineConfigsByManufacturerAndModel.get(
                    new EPD_EngineConfigService().getConfigMapKeyForEngine(
                            epd.Equipment_Engine__r.Manufacturer__c,
                            epd.Equipment_Engine__r.Model__c
                    )
            );

            System.debug(CLASS_NAME + ' -> generateFormStructures. Engine Config:' + JSON.serialize(engineConfig));

            EPD_FormStructure formStructure = new EPD_FormStructure()
                    .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                    .setParts(getPartsWrapped(epd.Part_Replacements__r))
                    .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                    .setCOCStages(getCOCStagesWrapped(epd.Compressor_Operating_Condition_Stages__r))
                    .setEOCs(getEOCWrapped(epd.Engine_Operating_Conditions__r, engineConfig))
                    .setEngineBlocks(
                            new EPD_BlockInformationService().buildEngineBlocksForEPD(
                                    blockInformationByEPDId.get(epd.Id),
                                    engineConfig
                            )
                    )
                    .setFlags(engineConfig);

            System.debug(CLASS_NAME + ' -> generateFormStructures. Final Structure:' + JSON.serialize(formStructure));

            structures.add(formStructure);
        }

        System.debug(CLASS_NAME + ' -> generateFormStructures. END. Structures: ' + JSON.serialize(structures));
        return structures;
    }

    /**
     * Wraps Compressor Operating Condition stage records into structure class using setter to map values correctly.
     *
     * @param cocStages
     *
     * @return List<EPD_FormStructureCOCStage>
     */
    public List<EPD_FormStructureCOCStage> getCOCStagesWrapped(List<EPD_Compressor_Operating_Condition_Stage__c> cocStages) {
        List<EPD_FormStructureCOCStage> cocStagesWrapped = new List<EPD_FormStructureCOCStage>();

        for(EPD_Compressor_Operating_Condition_Stage__c stage : cocStages) {
            cocStagesWrapped.add(
                    (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(stage)
            );
        }

        return cocStagesWrapped;
    }

    /**
     * Wraps Engine Operating Condition records into structure class using setter to map values correctly.
     *
     * @param EOCs
     * @param config
     *
     * @return List<EPD_FormStructureEOC>
     */
    public List<EPD_FormStructureEOC> getEOCWrapped(List<EPD_Engine_Operating_Condition__c> EOCs, EPD_Engine__mdt config) {
        List<EPD_FormStructureEOC> EOCsWrapped = new List<EPD_FormStructureEOC>();

        for(EPD_Engine_Operating_Condition__c eoc : EOCs) {
            EOCsWrapped.add(
                    ((EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(eoc)).setFlag(config)
            );
        }

        return EOCsWrapped;
    }

    /**
     * Wraps Part Replacement records into structure class using setter to map values correctly.
     *
     * @param parts
     *
     * @return List<EPD_FormStructurePart>
     */
    public List<EPD_FormStructurePart> getPartsWrapped(List<EPD_Part_Replacement__c> parts) {
        List<EPD_FormStructurePart> partsWrapped = new List<EPD_FormStructurePart>();

        for(EPD_Part_Replacement__c part : parts) {
            partsWrapped.add(
                    (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(part)
            );
        }

        return partsWrapped;
    }

    /**
     * Handle save action for Form Structure.
     * It will go down the structure and try to persist every record.
     * If it fails and it was insert action, it will clean ids that were already inserted (as rollback is going to happen)
     * and rethrow the error.
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    public EPD_Response handleFormSave(EPD_FormStructure formStructure) {
        System.debug(CLASS_NAME + ' -> handleFormSave. START.');

        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) formStructure.epd.getRecord();

        Boolean isInsert = String.isBlank(epd.Id);
        if(isInsert) {
             epd.Previous_EPD__c = new EPD_EPDService().getIdForLastEPDOnEngine(epd.Equipment_Engine__c);
        }

        EPD_FormPersistenceHelper persistenceHelper = new EPD_FormPersistenceHelper();

        try {
            persistenceHelper.persistEPD(epd);
            persistenceHelper.persistPartReplacements(formStructure.parts, epd);
            persistenceHelper.persistEOCs(formStructure.EOCs, epd);
            persistenceHelper.persistCOCStages(formStructure.cocStages, epd);
            persistenceHelper.persistBlockInfosWithChildRecs(formStructure.engineBlocks, epd);
        } catch(Exception ex) {
            if(isInsert) { persistenceHelper.cleanIDsFromFormStructure(formStructure); }
            throw ex;
        }

        System.debug(CLASS_NAME + ' -> handleFormSave. END.');
        return createFormStructureFromRecord(epd);
    }

}