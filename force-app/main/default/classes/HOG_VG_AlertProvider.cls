/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Base class for creating alerts from trigger.
Test Class:     XXX
History:        jschn 19/10/2018 - Created. - W-001275 - W-001277
                jschn 23/10/2018 - Added prepopulate General Data (Location snapshot) - W-001302
*************************************************************************************************/
public abstract class HOG_VG_AlertProvider {

    /**
     * Creates alert based on location.
     *
     * @param location
     *
     * @return
     */
    public abstract HOG_Vent_Gas_Alert__c createAlert(Location__c location);

    /**
     * Prepopulate all general data for new Alert.
     * Location Vent Gas data snapshot, status, location ID and Start Date.
     *
     * @param alert
     * @param location
     *
     * @return
     */
    protected HOG_Vent_Gas_Alert__c prepopulateGeneralData(HOG_Vent_Gas_Alert__c alert, Location__c location){
        alert.PVR_GOR_Factor__c = location.PVR_GOR_Factor__c;
        alert.GOR_Test_Date__c = location.GOR_Test_Date__c;
        alert.PVR_Fuel_Consumption__c = location.PVR_Fuel_Consumption__c;
        alert.GOR_Effective_Date__c = location.GOR_Effective_Date__c;
        alert.PVR_Fuel_Consumption_Rate__c = location.PVR_Fuel_Consumption_Rate__c;
        alert.Measured_Vent_Rate__c = location.Measured_Vent_Rate__c;
        alert.Monthly_Trucked_Oil__c = location.Monthly_Trucked_Oil__c;
        alert.Total_Gas_Production__c = location.Total_Gas_Production__c;
        alert.Predicted_Reported_Vent_Rate__c = location.Predicted_Reported_Vent_Rate__c;
        alert.Production_Engineer__c = location.Operating_Field_AMU__r.Production_Engineer_User__c;
        alert.Start_Date__c = Datetime.now();
        alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED;
        alert.Well_Location__c = location.Id;
        return alert;
    }

}