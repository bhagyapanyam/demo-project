/*
 * Scheduled to run every hour, this class fetches articles from Alberta Oil Magazine and Yahoo weather and stores the result in the "DSP_Settings" custom setting.
 * This cached content is displayed on the Home page of the Downstream Portal CMS.
 *
 * See also: custom setting "DSP_Settings"
 * See also: Visualforce page "DSP_CMS_Index"
 * See also: scheduled job "DSP_FetchWebContent"
 */
global class DSP_CSM_ScheduledUpdate implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        updateAOM();
        updateWeather();
    }
    
    @future(callout=true)
    public static void updateWeather()
    {
        // 
        
        DateTime now = DateTime.now();
        
        String year = String.valueOf(now.year());
        String month = String.valueOf(now.month()-1);
        String day = String.valueOf(Integer.valueOf(now.format('u'))-1);
        String hours = String.valueOf(now.hour());
        
        DSP_Settings__c customSettings = DSP_Settings__c.getInstance();
        String location = customSettings.Weather_Location__c;
        
        String URL = 'https://weather.gc.ca/wxlink/site_js/s0000047_e.js';
        
        // get JSON from that URL
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(URL);
        HttpResponse res = h.send(req);
        
        String weatherData = res.getbody();
        
        // Fetch settings
        
        DSP_Settings__c weatherSettings = [SELECT Id,
                                                  Weather_Code_Forecast__c,
                                                  Weather_Temp_Forecast__c,
                                                  
                                                  Weather_Code_Today__c,
                                                  Weather_Temp_Today__c,
                                                  
                                                  Weather_Code_Tonight__c,
                                                  Weather_Temp_Tonight__c,
                                                  
                                                  Weather_Code_Tommorow__c,
                                                  Weather_Day_Tommorow__c,
                                                  Weather_Temp_Tommorow__c,
                                                  
                                                  Weather_Wind__c,
                                                  
                                                  Weather_Last_Updated__c
                                                  
                                           FROM DSP_Settings__c];
        
        // Parse XML
        
        String[] weatherItems = weatherData.split('\n');
        
        String viewing = ''; // Today or tonight.
        
        for(String weatherItem : weatherItems)
        {
            weatherItem = weatherItem.trim();

            if(weatherItem.startswith('var obTemperature'))
            {
                Matcher pm = Pattern.compile('.*"([^"]+)".*').matcher(weatherItem);
                if(pm.matches())
                {
                    weatherSettings.Weather_Temp_Today__c = pm.group(1) + '&#176;C';
                }
            }
            
            else if(weatherItem.startswith('var obIconCode'))
            {
                Matcher pm = Pattern.compile('.*"([^"]+)".*').matcher(weatherItem);
                if(pm.matches())
                    weatherSettings.Weather_Code_Today__c = pm.group(1);
            }
            
            else if(weatherItem.startswith('var obWindDir'))
            {
                Matcher pm = Pattern.compile('.*"([^"]+)".*').matcher(weatherItem);
                if(pm.matches())
                    weatherSettings.Weather_Wind__c = 'Wind ' + pm.group(1) + ' ';
            }
            
            else if(weatherItem.startswith('var obWindSpeed'))
            {
                Matcher pm = Pattern.compile('.*"([^"]+)".*').matcher(weatherItem);
                if(pm.matches())
                    weatherSettings.Weather_Wind__c += pm.group(1) + ' km/h';
            }
            
            else if(weatherItem.startswith('var forecastPeriods'))
            {
                Matcher pm = Pattern.compile('.*"([^"]*)"[^"]*"([^"]*)"[^"]*"([^"]*)".*').matcher(weatherItem);
                if(pm.matches())
                {
                    if(pm.group(1) == 'Today')
                    {
                        viewing = 'Today';
                        weatherSettings.Weather_Day_Tommorow__c = pm.group(3);
                    }
                    else if(pm.group(1) == 'Tonight') viewing = 'Tonight';
                }
            }
            
            else if(weatherItem.startswith('var forecastIconCodes'))
            {
                Matcher pm = Pattern.compile('.*"([^"]*)"[^"]*"([^"]*)"[^"]*"([^"]*)".*').matcher(weatherItem);
                if(pm.matches())
                {
                    if(viewing == 'Today')
                    {
                        weatherSettings.Weather_Code_Forecast__c = pm.group(1);
                        weatherSettings.Weather_Code_Tonight__c = pm.group(2);
                        weatherSettings.Weather_Code_Tommorow__c = pm.group(3);
                    }
                    else
                    {
                        weatherSettings.Weather_Code_Tonight__c = pm.group(1);
                        weatherSettings.Weather_Code_Tommorow__c = pm.group(2);
                    }
                }
            }
            
            else if(weatherItem.startswith('var forecastHighs'))
            {
                Matcher pm = Pattern.compile('.*"([^"]*)"[^"]*"([^"]*)"[^"]*"([^"]*)".*').matcher(weatherItem);
                if(pm.matches())
                {
                    if(viewing == 'Today')
                    {
                        // Forecast for the day
                        weatherSettings.Weather_Temp_Forecast__c = pm.group(1) + '&#176;C';
                        weatherSettings.Weather_Temp_Tommorow__c = pm.group(3) + '&#176;C';
                    }
                    else
                    {
                        // Forecast for the night
                        weatherSettings.Weather_Temp_Tommorow__c = pm.group(2) + '&#176;C';
                    }
                }
            }
            
            else if(weatherItem.startswith('var forecastLows'))
            {
                Matcher pm = Pattern.compile('.*"([^"]*)"[^"]*"([^"]*)"[^"]*"([^"]*)".*').matcher(weatherItem);
                if(pm.matches())
                {
                    if(viewing == 'Today')
                    {
                        weatherSettings.Weather_Temp_Tonight__c = pm.group(2) + '&#176;C';
                    }
                    else
                    {
                        weatherSettings.Weather_Temp_Tonight__c = pm.group(1) + '&#176;C';
                    }
                }
            }
        }
        
        
        /*
        
        Dom.Document doc = new Dom.Document();
        doc.load(weatherData);
        
        Dom.XMLNode feedRoot = doc.getRootElement();
        
        String seeking = 'Current Conditions';
        
        for(Dom.XMLNode child : feedRoot.getChildElements())
        {
            if(child.getName() == 'entry')
            {
                if(seeking == 'Current Conditions')
                {
                    for(Dom.XMLNode entryChild : feedRoot.getChildElements())
                    {
                        if(entryChild.getName() == 'title')
                        {
                            String text = entryChild.getText();
                            
                            if(text.startsWithIgnoreCase('Current Conditions'))
                            {
                                
                            }
                        }
                    }
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                weatherSettings.Weather_Wind__c = 'Wind ';
                
                Decimal direction = Decimal.valueOf(child.getAttribute('direction',''));
                
                if     (direction >= 348.75 || direction < 11.25 ) weatherSettings.Weather_Wind__c += 'N';
                else if(direction >= 11.25  && direction < 33.75 ) weatherSettings.Weather_Wind__c += 'NNE';
                else if(direction >= 33.75  && direction < 56.25 ) weatherSettings.Weather_Wind__c += 'NE';
                else if(direction >= 56.25  && direction < 78.75 ) weatherSettings.Weather_Wind__c += 'ENE';
                else if(direction >= 78.75  && direction < 101.25) weatherSettings.Weather_Wind__c += 'E';
                else if(direction >= 101.25 && direction < 123.75) weatherSettings.Weather_Wind__c += 'ENE';
                else if(direction >= 123.75 && direction < 146.25) weatherSettings.Weather_Wind__c += 'SE';
                else if(direction >= 146.25 && direction < 168.75) weatherSettings.Weather_Wind__c += 'SSE';
                else if(direction >= 168.75 && direction < 191.25) weatherSettings.Weather_Wind__c += 'S';
                else if(direction >= 191.25 && direction < 213.75) weatherSettings.Weather_Wind__c += 'SSW';
                else if(direction >= 213.75 && direction < 236.25) weatherSettings.Weather_Wind__c += 'SW';
                else if(direction >= 236.25 && direction < 258.75) weatherSettings.Weather_Wind__c += 'WSW';
                else if(direction >= 258.75 && direction < 281.25) weatherSettings.Weather_Wind__c += 'W';
                else if(direction >= 281.25 && direction < 303.75) weatherSettings.Weather_Wind__c += 'WNW';
                else if(direction >= 303.75 && direction < 326.25) weatherSettings.Weather_Wind__c += 'NW';
                else if(direction >= 326.25 && direction < 348.75) weatherSettings.Weather_Wind__c += 'NNW';
                
                weatherSettings.Weather_Wind__c += ' '+child.getAttribute('speed','')+' km/h';
            }
        }
        
        String parsing = 'Tonight';
        
        for(Dom.XMLNode child : feedRoot.getChildElement('results',null).getChildElement('channel',null).getChildElement('item',null).getChildElements())
        {
            if(child.getName() == 'condition')
            {
                weatherSettings.Weather_Code_Today__c = child.getAttribute('code','');
                weatherSettings.Weather_Temp_Today__c = child.getAttribute('temp','') + '&#176;C';
            }
            
            if(child.getName() == 'forecast')
            {
                if(parsing == 'Tonight')
                {
                    weatherSettings.Weather_Code_Tonight__c = child.getAttribute('code','');
                    weatherSettings.Weather_Temp_Tonight__c = child.getAttribute('low','') + '&#176;C';
                    weatherSettings.Weather_Code_Forecast__c = child.getAttribute('code','');
                    weatherSettings.Weather_Temp_Forecast__c = child.getAttribute('high','') + '&#176;C';
                    
                    parsing = 'Tomorrow';
                }
                else if(parsing == 'Tomorrow')
                {
                    weatherSettings.Weather_Code_Tommorow__c = child.getAttribute('code','');
                    weatherSettings.Weather_Day_Tommorow__c = child.getAttribute('day','');
                    weatherSettings.Weather_Temp_Tommorow__c = child.getAttribute('high','') + '&#176;C';
                    
                    break;
                }
            }
        }
        */
        weatherSettings.Weather_Last_Updated__c = DateTime.now();
        
        update weatherSettings;
    }
      
    @future(callout=true)
    public static void updateAOM()
    {
        // Read the xml feed
        
        String url = 'http://www.albertaoilmagazine.com/feed/';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        HttpResponse res = h.send(req);
        
        String newsFeed = res.getbody();
        
        // Parse the xml
        
        Dom.Document doc = new Dom.Document();
        doc.load(newsFeed);
        
        Dom.XMLNode feedRoot = doc.getRootElement();
        
        List<List<String>> albertaOilMagCaches = new List<List<String>>();
        
        Integer itemCounter = 0;
        for(Dom.XMLNode child : feedRoot.getChildElement('channel',null).getChildElements())
        {
           if(child.getName() == 'item')
           {
               String link     = child.getChildElement('link',    null).getText();
               String title    = child.getChildElement('title',   null).getText();
               String newsdate = child.getChildElement('pubDate', null).getText();

               String[] newsdateArray = newsdate.split(' ');
               String formattedNewsDate = newsdateArray[3]+'-';
               if(newsdateArray[2] == 'Jan') formattedNewsDate += '1-';
               if(newsdateArray[2] == 'Feb') formattedNewsDate += '2-';
               if(newsdateArray[2] == 'Mar') formattedNewsDate += '3-';
               if(newsdateArray[2] == 'Apr') formattedNewsDate += '4-';
               if(newsdateArray[2] == 'May') formattedNewsDate += '5-';
               if(newsdateArray[2] == 'Jun') formattedNewsDate += '6-';
               if(newsdateArray[2] == 'Jul') formattedNewsDate += '7-';
               if(newsdateArray[2] == 'Aug') formattedNewsDate += '8-';
               if(newsdateArray[2] == 'Sep') formattedNewsDate += '9-';
               if(newsdateArray[2] == 'Oct') formattedNewsDate += '10-';
               if(newsdateArray[2] == 'Nov') formattedNewsDate += '11-';
               if(newsdateArray[2] == 'Dec') formattedNewsDate += '12-';
               formattedNewsDate += newsdateArray[1]+' '+newsdateArray[4];
               DateTime newsdate2 = DateTime.valueOfGMT(formattedNewsDate);
               
               String newformat = newsdate2.format('MMMM d, YYYY HH:MM');
               
               List<String> AOMitem = new List<String>();
               
               AOMitem.add(link);
               AOMitem.add(title);
               AOMitem.add(newformat);
               
               albertaOilMagCaches.add(AOMitem);
               
               itemCounter++;
               if(itemCounter == 4) break;
           }
        }
        
        while(albertaOilMagCaches.size() < 4)
        {
            List<String> AOMitem = new List<String>();
               
           AOMitem.add('');
           AOMitem.add('');
           AOMitem.add('');
           
            albertaOilMagCaches.add(AOMitem);
        }
        
        // Update the custom settings
        
        DSP_Settings__c articleSettings = [SELECT Id,
                                                  Alberta_Oil_Magazine_Article_1__c,
                                                  Alberta_Oil_Magazine_Article_1_Date__c,
                                                  Alberta_Oil_Magazine_Article_1_URL__c,
                                                  Alberta_Oil_Magazine_Article_2__c,
                                                  Alberta_Oil_Magazine_Article_2_Date__c,
                                                  Alberta_Oil_Magazine_Article_2_URL__c,
                                                  Alberta_Oil_Magazine_Article_3__c,
                                                  Alberta_Oil_Magazine_Article_3_Date__c,
                                                  Alberta_Oil_Magazine_Article_3_URL__c,
                                                  Alberta_Oil_Magazine_Article_4__c,
                                                  Alberta_Oil_Magazine_Article_4_Date__c,
                                                  Alberta_Oil_Magazine_Article_4_URL__c,
                                                  Alberta_Oil_Mag_Last_Updated__c
                                           FROM DSP_Settings__c];
        
        articleSettings.Alberta_Oil_Mag_Last_Updated__c = DateTime.now();
        
        articleSettings.Alberta_Oil_Magazine_Article_1_URL__c  = albertaOilMagCaches[0][0];
        articleSettings.Alberta_Oil_Magazine_Article_1__c      = albertaOilMagCaches[0][1];
        articleSettings.Alberta_Oil_Magazine_Article_1_Date__c = albertaOilMagCaches[0][2];
        
        articleSettings.Alberta_Oil_Magazine_Article_2_URL__c  = albertaOilMagCaches[1][0];
        articleSettings.Alberta_Oil_Magazine_Article_2__c      = albertaOilMagCaches[1][1];
        articleSettings.Alberta_Oil_Magazine_Article_2_Date__c = albertaOilMagCaches[1][2];
        
        articleSettings.Alberta_Oil_Magazine_Article_3_URL__c  = albertaOilMagCaches[2][0];
        articleSettings.Alberta_Oil_Magazine_Article_3__c      = albertaOilMagCaches[2][1];
        articleSettings.Alberta_Oil_Magazine_Article_3_Date__c = albertaOilMagCaches[2][2];
        
        articleSettings.Alberta_Oil_Magazine_Article_4_URL__c  = albertaOilMagCaches[3][0];
        articleSettings.Alberta_Oil_Magazine_Article_4__c      = albertaOilMagCaches[3][1];
        articleSettings.Alberta_Oil_Magazine_Article_4_Date__c = albertaOilMagCaches[3][2];
        
        update articleSettings;
    }
}