/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class used for transferring files from Lightning Component to Endpoint.
Test Class:     VG_AlertFileServiceTest
History:        jschn 22/01/2020 - Created.
*************************************************************************************************/
public with sharing class VG_File {

    @AuraEnabled
    public String name;

    @AuraEnabled
    public String fileContents;

}