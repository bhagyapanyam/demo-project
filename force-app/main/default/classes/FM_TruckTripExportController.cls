/*----------------------------------------------------------------------------------------------------
Author     : Jakub Schon
Company    : Husky Energy
Description: A controller to Export Truck Trips
             Used for FM_TruckTripExportCsv Page
Test Class : FM_TruckTripExportControllerTest
History    : 07.18.17 js Initial Revision
             07.19.17 js Rebuild in way as the runSheet export is done
             11.20.17 js LoadType column added, sort fixed, added Additional TKs in header and rework for additional TKs.
------------------------------------------------------------------------------------------------------*/
public with sharing class FM_TruckTripExportController {

    public string 					     header				    {get; private set;}
	public List<TruckTripWrapper> 	     wrappedTruckTrips 	    {get; private set;}
	public String					     carrier 			    {get; private set;}
    public Map<Id, List<String>>         additionalTKs          {get;set;}

    private Map<Id, Id>                  truckTripToRunsheetMap {get; set;}
    private Set<Id>                      facilityIds            {get; set;}
    private Set<Id>                      locationIds            {get; set;}
    private Date                         startDate              {get; set;}
    private Date                         endDate                {get; set;}

    //TODO JS: rework which values are displayed (should be other tanks info)
    
	public class TruckTripWrapper implements Comparable {
        public String id                {get;set;}
		public String truckTrip 		{get;set;}
		public String loadRequest 		{get;set;}
        public String tankName          {get;set;}
        public String route             {get;set;}
        public String createdDate       {get;set;}
        public String wellName          {get;set;}
        public String tankSize          {get;set;}
        public String tankLowLevel      {get;set;}
        public String actTankLevel      {get;set;}
        public String actFlowRate       {get;set;}
        public String shift             {get;set;}
        public String product           {get;set;}
        public String loadType          {get;set;}
        public String standingComments  {get;set;}
        public String unitConfig        {get;set;}
        public String sour              {get;set;}
        public String surfLocation      {get;set;}
        public String sourceId          {get;private set;}

		public TruckTripWrapper(FM_Truck_Trip__c truckTrip) {
            this.id = truckTrip.Id;
			this.truckTrip = truckTrip.Name;
			this.loadRequest = truckTrip.Load_Request__r.Name;
            this.tankName = truckTrip.Load_Request__r.Tank_Label__c;
            this.route = truckTrip.Route__c;
            this.createdDate = String.valueOf(truckTrip.Load_Request__r.CreatedDate.date());
            this.wellName = FM_TruckTrip_Utilities.getWellName(truckTrip);
            this.tankSize = String.valueOf(truckTrip.Load_Request__r.Tank_Size__c);
            this.tankLowLevel = String.valueOf(truckTrip.Load_Request__r.Tank_Low_Level__c);
            this.actTankLevel = String.valueOf(truckTrip.Load_Request__r.Act_Tank_Level__c);
            this.actFlowRate = String.valueOf(truckTrip.Load_Request__r.Act_Flow_Rate__c);
            this.shift = truckTrip.Shift__c + ' - ' + truckTrip.Shift_Day__c;
            this.product = truckTrip.Product__c;
            this.loadType = truckTrip.Load_Type__c != null ? truckTrip.Load_Type__c : truckTrip.Load_Request__r.Load_Type__c;
            this.standingComments = truckTrip.Standing_Comments__c;
            this.unitConfig = truckTrip.Unit_Configuration__c + ' / ' + truckTrip.Load_Weight__c;
            this.sour = truckTrip.Sour__c ? 'YES' : 'NO';
            this.surfLocation = getSurfLocation(truckTrip);
            this.sourceId = truckTrip.Load_Request__r.Source_Location__c != null ? truckTrip.Load_Request__r.Source_Location__c : truckTrip.Load_Request__r.Source_Facility__c;
            if (String.isNotBlank(standingComments)){
                this.standingComments = standingComments.replace('\n', ' ');
                this.standingComments = standingComments.replace(',', ' ');
            }
		}

        private String getSurfLocation(FM_Truck_Trip__c truckTrip) {
            if (truckTrip.Load_Request__r.Source_Location__c != null)
                return truckTrip.Load_Request__r.Source_Location__r.Surface_Location__c;
            else if (truckTrip.Load_Request__r.Source_Facility__c != null)
                return truckTrip.Load_Request__r.Source_Facility__r.Name;
            return '';
        }

        public Integer compareTo(Object obj) {
            TruckTripWrapper ttw = (TruckTripWrapper) obj;
            String current = wellName + ' ' + shift;
            String compared = ttw.wellName + ' ' + ttw.shift;
            if (current > compared) return 1;
            if (current < compared) return -1;
            return 0; //preserve existing order
        }

	}

	public FM_TruckTripExportController() {
		wrappedTruckTrips = new List<TruckTripWrapper>();
        truckTripToRunsheetMap = new Map<Id, Id>();
        additionalTks = new Map<Id, List<String>>();
        locationIds = new Set<Id>();
        facilityIds = new Set<Id>();
        startDate = Date.today();
        endDate = Date.today();

		header = FM_TruckTrip_Utilities.EXPORT_HEADER;
	}

	public void exportTruckTripsToCSV() {
		String idString = System.currentPageReference().getParameters().get('trucktrips');
		if (String.isNotBlank(idString)) {
            try {
    			Set<String> truckTripIds = new Set<String>();
                if (idString.contains(','))
    	       		truckTripIds.addAll(idString.split(','));
                else truckTripIds = new Set<String>{idString};
    			wrapTruckTrips(getTruckTrips(truckTripIds));

                populateAdditionalTksFromLoadRequest();
                populateAdditionalTksFromRunsheet();
                populateAdditionalTksFromEquipmentTanks();
                makeAdditionalTKsSafe();
                System.debug('additionalTks: ' + additionalTks);
                FM_TruckTrip_Utilities.markTruckTripsAsExported(idString);
                wrappedTruckTrips.sort();
            } catch (Exception ex) {
                header = 'Error: ' + ex.getMessage();
            }
		} else
            header = 'No data to export!';
	}

	private void wrapTruckTrips(List<FM_Truck_Trip__c> truckTrips) {
        System.debug('Truck Trips size: ' + truckTrips.size());
		if (truckTrips != null && truckTrips.size() > 0) {
			// if (truckTrips.size() > 0) truckTrips.clear();
			carrier = truckTrips.get(0).Carrier__r.Carrier_Name_For_Fluid__c;
			for (FM_Truck_Trip__c tt : truckTrips) {
				wrappedTruckTrips.add(new TruckTripWrapper(tt));
                addSource(tt);
                Date createdDate = Date.newinstance(tt.Load_Request__r.CreatedDate.year(), tt.Load_Request__r.CreatedDate.month(), 
                    tt.Load_Request__r.CreatedDate.day());
                startDate = (createdDate < startDate) ? createdDate : startDate;
                endDate = (createdDate > endDate) ? createdDate : endDate;

                //Add entry to runsheet ma 
                if(tt.Load_Request__r.Run_Sheet_Lookup__c != null)
                    truckTripToRunsheetMap.put(tt.Id, tt.Load_Request__r.Run_Sheet_Lookup__c);
            }
            endDate = endDate.addDays(1);
        }
    }

    private void addSource(FM_Truck_Trip__c tt) {
        if (tt.Load_Request__r.Source_Location__c != null && !locationIds.contains(tt.Load_Request__r.Source_Location__c))
            locationIds.add(tt.Load_Request__r.Source_Location__c);
        if (tt.Load_Request__r.Source_Facility__c != null && !facilityIds.contains(tt.Load_Request__r.Source_Facility__c))
            facilityIds.add(tt.Load_Request__r.Source_Facility__c);
    }

	private List<FM_Truck_Trip__c> getTruckTrips(Set<String> truckTripIds) {
        return [SELECT Id, Name, Route__c, Well__c, Tank__c, Shift__c, Shift_Day__c, Product__c, Load_Type__c
                    , Standing_Comments__c, Unit_Configuration__c, Load_Weight__c, Sour__c
                    , Load_Request__c, Load_Request__r.Name, Load_Request__r.CreatedDate
                    , Load_Request__r.Load_Type__c
                    , Load_Request__r.Tank_Size__c, Load_Request__r.Tank_Low_Level__c
                    , Load_Request__r.Act_Tank_Level__c, Load_Request__r.Act_Flow_Rate__c
                    , Load_Request__r.Source_Location__c, Load_Request__r.Source_Facility__c
                    , Load_Request__r.Source_Location__r.Name
                    , Load_Request__r.Source_Facility__r.Name
                    , Load_Request__r.Source_Location__r.Surface_Location__c
                    , Load_Request__r.Tank_Label__c, Load_Request__r.Run_Sheet_Lookup__c
                    , Carrier__r.Carrier_Name_For_Fluid__c
                FROM FM_Truck_Trip__c
                WHERE Id In :truckTripIds
                ORDER BY CreatedDate desc];
	}

    /**
    *   Generate additional tanks based on runsheet information.
    */
    private void populateAdditionalTksFromRunsheet() {
        for (FM_Runsheet_Tank_Information__c rsti : getRunSheetTankInfo()) {
            String tankLabel = String.isNotBlank(rsti.Label__c) ? rsti.Label__c : 'Unknown Tank';
            Id sourceId = rsti.Runsheet__r.Well__c != null ? rsti.Runsheet__r.Well__c : rsti.Runsheet__r.Facility__c;
            String value = tankLabel + ' (RS): ' + rsti.Level__c;
            addValueToAdditionalTksMap(sourceId, tankLabel, value);
        }
    }

    /**
    *   Generate additional tanks based on Load Request information.
    */
    private void populateAdditionalTksFromLoadRequest() {
        Map<String, List<FM_Load_Request__c>> sourceToLoadRequestMap = getSourceToLoadRequestMap();
        for(TruckTripWrapper tt : wrappedTruckTrips) {
            if(sourceToLoadRequestMap.containsKey(tt.sourceId)) {
                for(FM_Load_Request__c lr : sourceToLoadRequestMap.get(tt.sourceId)) {
                    String tankLabel = String.isNotBlank(lr.Tank_Label__c) ? lr.Tank_Label__c : 'Unknown Tank';
                    String value = tankLabel + ' (LR): ' + lr.Act_Tank_Level__c;
                    addValueToAdditionalTksMap(tt.sourceId, tankLabel, value);
                }
            }
        }
    }

    /**
    *   Generate addtional tanks based on Equipment Tank Information.
    */
    private void populateAdditionalTksFromEquipmentTanks() {
        Map<String, List<Equipment_Tank__c>> sourceToTanksMap = getSourceToTanksMap();
        for(TruckTripWrapper tt : wrappedTruckTrips) {
            if(sourceToTanksMap.get(tt.sourceId) != null) {
                for(Equipment_Tank__c equipmentTank : sourceToTanksMap.get(tt.sourceId)) {
                    if(equipmentTank.SCADA_Tank_Level__c != null) {
                        String tankLabel = String.isNotBlank(equipmentTank.Tank_Label__c) ? equipmentTank.Tank_Label__c : 'Unknown Tank';
                        Decimal level = equipmentTank.SCADA_Tank_Level__c;
                        String value = tankLabel + ' (SCADA): ' + level.setScale(0);
                        addValueToAdditionalTksMap(tt.sourceId, tankLabel, value);
                    }
                }
            }
        }
    }

    //Get Tankinfo for load requests with runsheets 
    private List<FM_Runsheet_Tank_Information__c> getRunSheetTankInfo(){
        return [SELECT Id, Name, Label__c, Size__c, Level__c, Low_Level__c, Runsheet__c, 
                        Runsheet__r.Well__c, Runsheet__r.Facility__c, Runsheet__r.Date__c
                  FROM FM_Runsheet_Tank_Information__c
                  WHERE Runsheet__c IN :truckTripToRunsheetMap.values()
                  ORDER BY Label__c];
    }

    //Get TankInfo for load requests with other load requests
    private Map<String, List<FM_Load_Request__c>> getSourceToLoadRequestMap() {
        Map<String, List<FM_Load_Request__c>> sourceToLoadRequestMap = new Map<String, List<FM_Load_Request__c>>();
        for(FM_Load_Request__c lr : [SELECT Id, Name, CreatedDate, Act_Tank_Level__c, Tank_Label__c, Source_Location__c, Source_Facility__c
                                  FROM FM_Load_Request__c
                                  WHERE (Source_Location__c In :locationIds OR Source_Facility__c In :facilityIds)
                                  AND Run_Sheet_Lookup__c = null
                                  AND CreatedDate >= :startDate
                                  AND CreatedDate <= :endDate
                                  ORDER BY CreatedDate DESC]) {
            Id sourceId = lr.Source_Location__c != null ? 
                lr.Source_Location__c :
                lr.Source_Facility__c;
            if(sourceToLoadRequestMap.containsKey(sourceId)) sourceToLoadRequestMap.get(sourceId).add(lr);
            else sourceToLoadRequestMap.put(sourceId, new List<FM_Load_Request__c>{lr});
        }
        return sourceToLoadRequestMap;
    }

    //Get TankInfo for load requests with no runsheets
    private Map<String, List<Equipment_Tank__c>> getSourceToTanksMap() {
        Map<String, List<Equipment_Tank__c>> sourceToTanksMap = new Map<String, List<Equipment_Tank__c>>();
        for (Equipment_Tank__c equipmentTank : getEquipmentTanks()) {
            Id sourceId = equipmentTank.Equipment__r.Location__c != null ? 
                equipmentTank.Equipment__r.Location__c :
                equipmentTank.Equipment__r.Facility__c;

            if(sourceId != null) {
                if(sourceToTanksMap.containsKey(sourceId)) 
                    sourceToTanksMap.get(sourceId).add(equipmentTank);
                else sourceToTanksMap.put(sourceId, new List<Equipment_Tank__c>{equipmentTank});
            }
        }
        return sourceToTanksMap;
    }

    private List<Equipment_Tank__c> getEquipmentTanks() {
        //To make the query more selective
        Set<String> tankLabels = new Set<String>();
        for(SelectOption tankOption : FM_Utilities.GetTankLabels('all')) {
            tankLabels.add(tankOption.getLabel());
        }

        return [SELECT Id, Name, Tank_Size_m3__c, Tank_Label__c, SCADA_Tank_Level__c,
                     Equipment__r.Location__c, Equipment__r.Facility__c, Latest_Tank_Reading_Date__c
              FROM Equipment_Tank__c
              WHERE (Equipment__r.Location__c In :locationIds OR Equipment__r.Facility__c In :facilityIds)
                  AND Tank_Label__c In :tankLabels
                  AND Tank_Settings__c <> 'Hidden - Hide this Tank from the selectable list.'
              ORDER BY Tank_Label__c];
    }

    private void makeAdditionalTKsSafe(){ 
        if (additionalTKs!= null)
            for(TruckTripWrapper ttw : wrappedTruckTrips)
                if (!additionalTKs.containsKey(ttw.sourceId))
                    additionalTKs.put(ttw.sourceId, new List<String>());
    }

    private void addValueToAdditionalTksMap(Id sourceId, String tankLabel, String value) {
        if (additionalTks.containsKey(sourceId)) {
            Boolean isThere = false;
            for (String s : additionalTks.get(sourceId)) {
                if (s.contains(tankLabel)) {
                    isThere = true;
                    break;
                }
            }
            if (!isThere) additionalTKs.get(sourceId).add(value);
        } else additionalTKs.put(sourceId, new List<String>{value});
    }

}