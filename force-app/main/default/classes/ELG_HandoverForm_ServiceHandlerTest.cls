/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 7/17/2019   
 */


@IsTest(IsParallel = true)
public class ELG_HandoverForm_ServiceHandlerTest {

	/*@IsTest
	static void getHandoverFormTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

		Test.startTest();
		ELG_DAOProvider.handoverDAO = new ELG_HandoverFormSelectorMock();
		result = new ELG_HandoverForm_ServiceHandler().getHandoverForm(null);
		ELG_Shift_Handover__c document = (ELG_Shift_Handover__c) result.resultObjects.get(0);
		System.assertNotEquals(null, result.resultObjects, 'Handover form was not found.');
		System.assertEquals(1, result.resultObjects.size(), 'Should be just one Handover form.');
		System.assertNotEquals(null, document, 'Handover form was not found.');
		Test.stopTest();
	}

	@IsTest
	static void preparedLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

		Test.startTest();
		ELG_DAOProvider.handoverDAO = new ELG_HandoverFormSelectorMock();
		result = new ELG_HandoverForm_ServiceHandler().preparedLogEntries(null);
		HOG_RequestResult requestResult = result;
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Handover_Category_Name
		};
		System.assertEquals(null, result.resultObjects, 'There should not be any log entries.');
		System.assertEquals(errorMsg, requestResult.errors, 'Error msg is not the same as label.');
		Test.stopTest();
	}

	@IsTest
	static void prepareLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_TestDataFactory.createQuestions('Test', 2, true);
		Id recordTypeId = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();
		List<ELG_Handover_Category_Question__c> listOfQuestions = [
				SELECT Id,
						Question__c
				FROM ELG_Handover_Category_Question__c
				ORDER BY Order__c ASC
		];


		Test.startTest();
		result = new ELG_HandoverForm_ServiceHandler().preparedLogEntries('Test Cat');
		System.assertNotEquals(null, result.resultObjects, 'No Log Entries returned.');

		List<ELG_Log_Entry__c> listOfPreparedLogEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);
		System.assertEquals(2, listOfPreparedLogEntries.size(), 'Number of Log Entries is different.');

		System.assertEquals(listOfQuestions[0].Question__c, listOfPreparedLogEntries[0].Question__c, 'Question is different form Log Entry questions.');
		System.assertEquals(listOfQuestions[0].Id, listOfPreparedLogEntries[0].Handover_Category_Question__c, 'Id is different form Log Entry questions.');
		System.assertEquals(recordTypeId, listOfPreparedLogEntries[0].RecordTypeId, 'Record Type Id is different form Log Entry questions.');

		System.assertEquals(listOfQuestions[1].Question__c, listOfPreparedLogEntries[1].Question__c, 'Question is different form Log Entry questions.');
		System.assertEquals(listOfQuestions[1].Id, listOfPreparedLogEntries[1].Handover_Category_Question__c, 'Id is different form Log Entry questions.');
		System.assertEquals(recordTypeId, listOfPreparedLogEntries[1].RecordTypeId, 'Record Type Id is different form Log Entry questions.');
		Test.stopTest();
	}

	@IsTest
	static void logEntriesToInsertTest() {
	//cant do will try to insert...will be tested in Endpoint
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

		Test.startTest();
		ELG_DAOProvider.handoverDAO = new ELG_HandoverFormSelectorMock();
		List<ELG_Log_Entry__c> listOfLogEntries = ELG_DAOProvider.handoverDAO.getLogEntriesByCategories(null);
		System.debug('Show me list '+listOfLogEntries);
		List<ELG_Shift_Assignement__c> shift =  ELG_DAOProvider.handoverDAO.getUnavailableIncomingOperatorFromShifts(null);
		ELG_Shift_Assignement__c shift2 = shift[0];
		System.debug('Show me shift '+shift);
		System.debug('Show me shift2 '+shift2);
		result = new ELG_HandoverForm_ServiceHandler().logEntriesToInsert(listOfLogEntries, shift2);
		System.debug('Show me result '+result);

		Test.stopTest();
	}



	// Mockup for Selector
	public class ELG_HandoverFormSelectorMock implements ELG_HandoverForm_DAO {
		public ELG_Shift_Handover__c getHandoverForm(String handoverFormId) {
			return (ELG_Shift_Handover__c) HOG_SObjectFactory.createSObject(new ELG_Shift_Handover__c(),
					ELG_FieldDefaults.CLASS_NAME, false);
		}

		public List<ELG_Handover_Category__c> getQuestionsBasedOnCategory(String categoryName) {
			return (List<ELG_Handover_Category__c>) HOG_SObjectFactory.createSObjectList(new ELG_Handover_Category__c(),
					2,
					ELG_FieldDefaults.CLASS_NAME,
					false);
		}

		public List<ELG_Log_Entry__c> getLogEntriesByCategories(String shiftAssignementId) {
			return (List<ELG_Log_Entry__c>) HOG_SObjectFactory.createSObjectList(new ELG_Log_Entry__c(),
					10,
					ELG_FieldDefaults.CLASS_NAME,
					false);
		}

		public List<ELG_Shift_Handover__c> getUnavailableIncomingOperatorFromHandovers(String incomingOperatorId) {
			return (List<ELG_Shift_Handover__c>) HOG_SObjectFactory.createSObjectList(new ELG_Shift_Handover__c(),
					2,
					ELG_FieldDefaults.CLASS_NAME,
					false);
		}

		public List<ELG_Shift_Assignement__c> getUnavailableIncomingOperatorFromShifts(String incomingOperatorId) {
			return (List<ELG_Shift_Assignement__c>) HOG_SObjectFactory.createSObjectList(new ELG_Shift_Assignement__c(),
					2,
					ELG_FieldDefaults.CLASS_NAME,
					false);
		}

		public List<ELG_Log_Entry__c> getGenericLogEntries(String shiftAssignmentId, String recordTypeId) {
			return (List<ELG_Log_Entry__c>) HOG_SObjectFactory.createSObjectList(new ELG_Log_Entry__c(),
					10,
					ELG_FieldDefaults.CLASS_NAME,
					false);
		}

	}*/
}