/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Code_Group__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class ServiceCodeGroupTestData
{
    public static HOG_Service_Code_Group__c createServiceCodeGroup(String name, Boolean active)
    {                
        HOG_Service_Code_Group__c results = new HOG_Service_Code_Group__c
            (
                Name = name,
                Active__c = active
            );
                            
        return results;
    }
}