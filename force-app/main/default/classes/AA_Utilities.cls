/**
 * Utility class for Agile initiative object
 *
 * Test: AA_UtilitiesTest
 * Created: 2018/04/16
 * Author: MBrimus
 *
 * Changelog:
 * 2018/04/16 - Created
 */
//Test specfic components deployment 
public without sharing class AA_Utilities {
	
	/**
	 * After insert trigger handler
	 * This will insert x number of stage records for each inserted AA
	 * number of records is determined based on values from AA_Initiative__c.Chevron_Phase__c
	 * @param aList [list of inserted records
	 */
	public static void TriggerAfterUpdateAgileInitiative(List<AA_Initiative__c> aList){
		List<Agile_Initiative_Stage__c> stagesToInsert = new List<Agile_Initiative_Stage__c>();
		List<String> stages = getPhasePickListValues();
		
		for(AA_Initiative__c record : aList){
			
			for(String stage: stages){

				Agile_Initiative_Stage__c newStage = new Agile_Initiative_Stage__c();
				newStage.Name = stage;
				newStage.Phase__c = stage;
				newStage.Agile_Initiative__c = record.Id;
				stagesToInsert.add(newStage);

			}

		}

		if(!stagesToInsert.isEmpty()){
			try {
				upsert stagesToInsert;
			} catch(Exception e){
				System.debug('ERROR IN TriggerAfterUpdateAgileInitiative ' + e.getMessage());
			}
		}
	}

	/**
	 * This will return picklist values from AA_Initiative__c.Chevron_Phase__c in a list
	 * @return list of strings
	 */
	public static List<String> getPhasePickListValues(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = AA_Initiative__c.Chevron_Phase__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}     
		return pickListValuesList;
    }
}