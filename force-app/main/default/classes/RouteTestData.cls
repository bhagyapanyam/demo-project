/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Route__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class RouteTestData
{
    public static Route__c createRoute(String name)
    {                
        Route__c results = new Route__c
            (           
                Name = name,
                Route_Number__c = name
            );            

        return results;
    }
}