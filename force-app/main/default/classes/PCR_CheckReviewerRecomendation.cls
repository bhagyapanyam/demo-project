global class PCR_CheckReviewerRecomendation{
    @InvocableMethod
    public static void validateReviews(List<ID> changeIds) {
        Boolean recomended = true;
        ID changeid = changeIds.get(0);
        
        for(CR_Review__c r : [select id, Recommendation__c from CR_Review__c where Change_Request__c =: changeid]){
            //if(r.Recommendation__c != 'Recommended'){
            if(string.ISBLANK(r.Recommendation__c)){
                recomended = false;
                break;
            }
        }
        
        //if all CR reviews in recomended state then auto approve the CR
        if(recomended){
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');  
            req1.setObjectId(changeid);
            req1.setProcessDefinitionNameOrId('SPA_Review');
            req1.setSkipEntryCriteria(true);

            Approval.ProcessResult result = Approval.process(req1);
                                              
        }
    }
}