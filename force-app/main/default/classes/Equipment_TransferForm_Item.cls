/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_TransferForm_Item {
	@AuraEnabled
	public Equipment_Transfer_Item__c item;
	@AuraEnabled
	public List<ContentDocumentLink> files;
	@AuraEnabled
	public List<Attachment> attachments;
	@AuraEnabled
	public Boolean hasAttachment = false;
	@AuraEnabled
	public String fileName;

	public Equipment_TransferForm_Item() {
		this.item = new Equipment_Transfer_Item__c();
		this.files = new List<ContentDocumentLink>();
		this.attachments = new List<Attachment>();
	}
}