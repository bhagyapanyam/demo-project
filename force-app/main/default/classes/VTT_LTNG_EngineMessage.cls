/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_EngineMessage message model for vtt ltng engine
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public class VTT_LTNG_EngineMessage {
    @AuraEnabled
    public String message;

    // Determines what class will be used on frontend
    @AuraEnabled
    public String severity;

    // For linking to record
    // Name of object to navigate to
    @AuraEnabled
    public String entityObjectName;

    // Name field on object
    @AuraEnabled
    public String entityName;

    // Id to link to
    @AuraEnabled
    public String entityId;

    public VTT_LTNG_EngineMessage(String message, String severity) {
        this.message = message;
        this.severity = severity;
    }

    public VTT_LTNG_EngineMessage(String message, String severity, String entityObjectName,
            String entityName, String entityId) {
        this.message = message;
        this.severity = severity;
        this.entityObjectName = entityObjectName;
        this.entityName = entityName;
        this.entityId = entityId;
    }
}