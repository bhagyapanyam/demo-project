/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_PDFPageController
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest
private class EPD_PDFPageControllerTest {

    @IsTest
    static void constructorTest_fail1_listOfOutBounds() {
        EPD_DAOProvider.EPDDAO = new MockWithoutRecord();
        ApexPages.StandardController stdController = new ApexPages.StandardController(new EPD_Engine_Performance_Data__c());
        EPD_PDFPageController ext;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try{
            ext = new  EPD_PDFPageController(stdController);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, ext);
    }

    @IsTest
    static void constructorTest_fail2_unableToConstructStructure() {
        EPD_DAOProvider.EPDDAO = new MockWithRecord();
        ApexPages.StandardController stdController = new ApexPages.StandardController(new EPD_Engine_Performance_Data__c());
        EPD_PDFPageController ext;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try{
            ext = new  EPD_PDFPageController(stdController);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, ext);
    }

    @IsTest
    static void constructorTest_success() {
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        ApexPages.StandardController stdController = new ApexPages.StandardController(epd);
        EPD_PDFPageController ext;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try{
            ext = new  EPD_PDFPageController(stdController);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, ext);
        System.assertNotEquals(null, ext.formStructure);
    }

    public class MockWithoutRecord implements EPD_EPDDAO {

        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
            return new List<EPD_Engine_Performance_Data__c>();
        }

        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {return null;}
    }

    public class MockWithRecord implements EPD_EPDDAO {

        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
            return new List<EPD_Engine_Performance_Data__c>{
                    (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(new EPD_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }

        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {return null;}
    }

}