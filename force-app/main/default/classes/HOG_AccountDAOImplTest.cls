/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_AccountDAOImpl class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class HOG_AccountDAOImplTest {

    @IsTest
    static void getHuskyAccountsByRecordTypeId_withoutParam() {
        HOG_AccountDAO selector = new HOG_AccountDAOImpl();
        List<Account> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getHuskyAccountsByRecordTypeId(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getHuskyAccountsByRecordTypeId_withParam() {
        HOG_AccountDAO selector = new HOG_AccountDAOImpl();
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), false);
        acc.Husky_AD_Companyname__c = HOG_Constants.HUSKY_AD_COMPANY_NAME;
        insert acc;
        List<Account> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = selector.getHuskyAccountsByRecordTypeId(acc.RecordTypeId);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(acc.Id, result.get(0).Id);
    }


    @IsTest
    static void getAccountsWithContactsByRecordType_withoutParam() {
        VTT_SCAccountDAO selector = new HOG_AccountDAOImpl();
        List<Account> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getAccountsWithContactsByRecordType(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAccountsWithContactsByRecordType_withParam() {
        VTT_SCAccountDAO selector = new HOG_AccountDAOImpl();
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), false);
        acc.Husky_AD_Companyname__c = HOG_Constants.HUSKY_AD_COMPANY_NAME;
        insert acc;
        Contact con = VTT_TestData.createTradesmanContact('Natasha', 'Romanov', acc.Id, UserInfo.getUserId());
        List<Account> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = selector.getAccountsWithContactsByRecordType(acc.RecordTypeId);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(acc.Id, result.get(0).Id);
        System.assertEquals(expectedCount, result.get(0).Contacts.size());
        System.assertEquals(con.Id, result.get(0).Contacts.get(0).Id);
    }

}