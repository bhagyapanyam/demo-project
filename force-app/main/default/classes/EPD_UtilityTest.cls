/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_Utility
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_UtilityTest {

    @IsTest
    static void isSingleRecord_missingParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isSingleRecord(null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isSingleRecord_emptyList() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isSingleRecord(new List<SObject>());

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isSingleRecord_singleItem() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isSingleRecord( new List<SObject> { new Account() } );

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isSingleRecord_multiItem() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isSingleRecord(new List<SObject> { new Account(), new Account() } );

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEmptyList_missingParam() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isEmptyList(null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEmptyList_emptyList() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isEmptyList(new List<SObject>());

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEmptyList_singleItem() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isEmptyList( new List<SObject> { new Account() } );

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEmptyList_multiItem() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isEmptyList(new List<SObject> { new Account(), new Account() } );

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isBigger_withoutParams() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isBigger(null, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isBigger_withValueParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isBigger(0, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isBigger_withTopRangeParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isBigger(null, 0);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isBigger_false() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isBigger(0, 0);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isBigger_true() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isBigger(1, 0);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withoutParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(null, null, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withValueParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(0, null, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withBottomRangeParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(null, 0, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withTopRangeParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(null, null, 0);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withoutValueParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(null, 0, 2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withoutBottomRangeParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(1, null, 2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_withoutTopRangeParam() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(1, 0, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_outOfTopRange() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isOutsideOfRange(1, -1, 0);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_outOfBottomRange() {
        Boolean expectedResult = true;

        Boolean result = EPD_Utility.isOutsideOfRange(1, 2, 3);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_inRange1() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(1, 0, 2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void iisOutsideOfRange_inRange2() {
        Boolean expectedResult = false;

        Boolean result = EPD_Utility.isOutsideOfRange(1, 1, 1);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_withoutParams() {
        Boolean expectedResult;
        Boolean result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(null, null, null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_withoutParam1() {
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(null, 1, 100);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_withoutParam2() {
        Boolean expectedResult;
        Boolean result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(1, null, 100);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_withoutParam3() {
        Boolean expectedResult;
        Boolean result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(1, 1, null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_true() {
        Boolean expectedResult = true;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(2, 1, 0);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_false1() {
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(1, 1, 100);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void isLessThenPreviousWithCoefficient_false2() {
        Boolean expectedResult = false;
        Boolean result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.isLessThenPreviousWithCoefficient(1, 2, 100);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getCoefficientFromPercentage_withoutParam() {
        Decimal expectedResult;
        Decimal result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.getCoefficientFromPercentage(null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getCoefficientFromPercentage_withParam1() {
        Decimal expectedResult = 1.10;
        Decimal result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.getCoefficientFromPercentage(10);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getCoefficientFromPercentage_withParam2() {
        Decimal expectedResult = 1.20;
        Decimal result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        try {
            result = EPD_Utility.getCoefficientFromPercentage(20);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedResult, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

}