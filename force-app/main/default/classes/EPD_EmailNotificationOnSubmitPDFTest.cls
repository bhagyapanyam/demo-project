/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EmailNotificationOnSubmitPDF
History:        jschn 2019-07-19 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EmailNotificationOnSubmitPDFTest {

    @IsTest
    static void testWithoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailNotificationOnSubmitPDF().sendEmails(null);
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void testWithEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailNotificationOnSubmitPDF().sendEmails(new List<EPD_Engine_Performance_Data__c>());
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void testWithProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EmailNotificationOnSubmitPDF().sendEmails(
                    new List<EPD_Engine_Performance_Data__c> {
                            (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Engine_Performance_Data__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    }
            );
        } catch(Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}