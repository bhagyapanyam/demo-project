@isTest
private class OliAsphaltTableEditControllerTest {
    static list<Opportunity> lstOpp = new list<Opportunity>(); 
    static list<ATS_Freight__c> lstFreight = new list<ATS_Freight__c>();
    static list<cpm_Supplier__c> lstSupplier = new list<cpm_Supplier__c>(); 
    static list<OpportunityLineItem> lstOli = new list<OpportunityLineItem>(); 

    static void createData() {   

        //create account
        Account acc= new Account(
            Name = 'testAcc',
            BillingStreet = 'testStreet',
            BillingCity = 'tectcity',
            BillingState = 'testState',
            BillingPostalCode = '123',
            BillingCountry = 'testcountry',
            Description = 'testdesc'
        );       	
        insert acc;    
        
        //create opportunity                
		Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
        
        Opportunity opp = new Opportunity(
            AccountId = acc.id,
            Name = 'testOpp',
            CloseDate = System.Today(),
            StageName = 'Bid Initiation',
            cpm_Bid_Due_Date_Time__c = System.Today(),
            cpm_Country__c = 'Canada',
            cpm_For_Season__c = '2018',
            cpm_Freight_Due_Date_Time__c = System.Today(),
            cpm_Trade_Class__c = 'Emulsion',
             recordTypeId = recordTypeId
        ); 
        lstOpp.add(opp);
        
        insert lstOpp;
        
        // Insert a test product.
        Product2 prod = new Product2(
            Name = 'Test Product', 
            Family = 'Asphalt'
        );
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug('pricebookId' + pricebookId);
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook', 
            isActive=true
        );
        insert customPB;
        system.debug('customPB' + customPB);
        
        // 2. Insert a price book entry with a custom price        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;  
        system.debug('customPrice' + customPrice);
        
        // create opportunityLineItem
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opp.Id;
        oli.PricebookEntryId = customPrice.Id;
        oli.Quantity = 1;
      
        oli.UnitPrice = customPrice.UnitPrice; 
        oli.ServiceDate = System.today();
        insert oli;
        system.debug('oli' + oli);
        
		lstOli.add(oli);
        
        
    	// create husky supplier 1
        cpm_Supplier__c huskysupp1 = new cpm_Supplier__c();
        huskysupp1.Name = 'Husky supplier 1';
        insert huskysupp1;
        system.debug('huskysupp1' + huskysupp1);
        
        // create husky supplier 2
        cpm_Supplier__c huskysupp2 = new cpm_Supplier__c();
        huskysupp2.Name = 'Husky supplier 2';
        insert huskysupp2;
        
        // create husky supplier 3
        cpm_Supplier__c huskysupp3 = new cpm_Supplier__c();
        huskysupp3.Name = 'Husky supplier 3';
        insert huskysupp3;
        
        // create competitor 1
        cpm_Supplier__c comp1 = new cpm_Supplier__c();
        comp1.Name = 'Competitor 1';
        insert comp1;
        
        // create competitor 2
        cpm_Supplier__c comp2 = new cpm_Supplier__c();
        comp2.Name = 'Competitor 2';
        insert comp2;
        
        // create competitor 3
        cpm_Supplier__c comp3 = new cpm_Supplier__c();
        comp3.Name = 'Competitor 3';
        insert comp3;
        
        // create competitor 4
        cpm_Supplier__c comp4 = new cpm_Supplier__c();
        comp4.Name = 'Competitor 4';
        insert comp4;
        
        
        lstSupplier.add(huskysupp1);
        lstSupplier.add(huskysupp2);
        lstSupplier.add(huskysupp3);
        lstSupplier.add(comp1);
        lstSupplier.add(comp2);
        lstSupplier.add(comp3);
        lstSupplier.add(comp4);
        system.debug(lstSupplier);
        
        // create ATS_Freight__c
        ATS_Freight__c freight = new ATS_Freight__c();
        freight.ATS_Freight__c = opp.Id;
        freight.cpm_HuskySupplier1_ats_pricing__c = huskysupp1.Id;
        freight.cpm_Product_Type__c = 'Asphalt';
        freight.Local_Freight_Via__c = 'Truck';
        freight.Prices_F_O_B__c = 'Origin';
        // not required:
        freight.cpm_HuskySupplier2_ats_pricing__c = huskysupp2.Id;
        freight.cpm_HuskySupplier3_ats_pricing__c = huskysupp3.Id;
        freight.cpm_Competitor1_ats_pricing__c = comp1.Id;
        freight.cpm_Competitor2_ats_pricing__c = comp2.Id;
        freight.cpm_Competitor3_ats_pricing__c = comp3.Id;
        freight.cpm_Competitor4_ats_pricing__c = comp4.Id;
        
        freight.Supplier_1_Rate__c = 1111;
        freight.Emulsion_Rate5_Supplier1__c = 1555;
        freight.Emulsion_Rate6_Supplier_1__c = 1666;
        freight.Emulsion_Rate7_Supplier1__c = 1777;
        freight.Emulsion_Rate8_Supplier1__c = 1888;
        
        freight.Supplier_2_Rate__c = 2222;
        freight.Emulsion_Rate5_Supplier2__c = 2555;
        freight.Emulsion_Rate6_Supplier2__c = 2666;
        freight.Emulsion_Rate7_Supplier2__c = 2777;
        freight.Emulsion_Rate8_Supplier2__c = 2888;
        
        freight.Supplier_3_Rate__c = 3333;
        freight.Emulsion_Rate5_Supplier3__c = 3555;
        freight.Emulsion_Rate6_Supplier3__c = 3666;
        freight.Emulsion_Rate7_Supplier3__c = 3777;
        freight.Emulsion_Rate8_Supplier3__c = 3888;
        
        freight.Competitor_Supplier_1_Rate__c = 11;
        freight.Emulsion_Rate5_Competitor1__c = 15;
        freight.Emulsion_Rate6_Competitor1__c = 16;
        freight.Emulsion_Rate7_Competitor1__c = 17;
        freight.Emulsion_Rate8_Competitor1__c = 18;
        
        freight.Competitor_Supplier_2_Rate__c = 22;
        freight.Emulsion_Rate5_Competitor2__c = 25;
        freight.Emulsion_Rate6_Competitor2__c = 26;
        freight.Emulsion_Rate7_Competitor2__c = 27;
        freight.Emulsion_Rate8_Competitor2__c = 28;
        
        freight.Competitor_Supplier_3_Rate__c = 33;
        freight.Emulsion_Axle5_Rate_Competitor3__c = 35;
        freight.Emulsion_Axle6_Rate_Competitor3__c = 36;
        freight.Emulsion_Rate7_Competitor3__c = 37;
        freight.Emulsion_Axle8_Rate_Competitor3__c = 38;
        
        freight.Competitor_Supplier_4_Rate__c = 44;
        freight.Emulsion_Axle5_Rate_Competitor4__c = 45;
        freight.Emulsion_Axle6_Rate_Competitor4__c = 46;
        freight.Emulsion_Rate7_Competitor4__c = 47;
        freight.Emulsion_Axle8_Rate_Competitor4__c = 48;
        
        insert freight;
        
		lstFreight.add(freight);
     }
    
    static testmethod void test1(){    
        
        createData();
        
        system.Test.StartTest();
        
        OpportunityLineItem oli = lstOli[0];
        
        Apexpages.StandardController sc = new Apexpages.StandardController(oli);
        
        OliAsphaltTableEditController controller = new OliAsphaltTableEditController(sc);
        ATS_Freight__c objATS_Freight = [select id, cpm_HuskySupplier1_ats_pricing__c, cpm_HuskySupplier1_ats_pricing__r.Name from ATS_Freight__c where id =: lstFreight[0].id];

        system.assertEquals(objATS_Freight.cpm_HuskySupplier1_ats_pricing__r.Name,controller.ats_Huskysupp1Name); 
        
        system.assertEquals(lstFreight[0].Local_Freight_Via__c,controller.ats_freight_method);
        system.assertEquals(lstFreight[0].Emulsion_Rate5_Supplier1__c,controller.ats_A5huskysupp1);
        system.assertEquals(lstFreight[0].Emulsion_Rate5_Supplier2__c,controller.ats_A5huskysupp2);
        system.assertEquals(lstFreight[0].Emulsion_Rate5_Supplier3__c,controller.ats_A5huskysupp3);
        
        
        system.Test.StopTest();

    
}
    
}