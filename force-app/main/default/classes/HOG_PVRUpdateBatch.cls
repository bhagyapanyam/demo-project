/*-------------------------------------------------------------------------------------------------
Author     : Shreyas Dhond
Company    : Husky Energy
Description: This class batches all surface locations and Updates PVR Average Volumes from well events.
Inputs     : N/A
Test Class : HOG_PVRUpdateTest
History:   : mz: 20.3.2018 - update for vent gas         
---------------------------------------------------------------------------------------------------*/
global class HOG_PVRUpdateBatch implements Database.Batchable<sObject> {
	
	String allSurfaceLocationsQuery = 'Select Id, Name, RecordTypeId, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c,' 
                   					  + ' PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c,' 
                   					  + ' PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,'
                   					  + ' PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,'
                   					  + ' PVR_AVGVOL_5D_PROD__c, PVR_AVGVOL_30D_PROD__c, PVR_Hours_On_Prod__c, PVR_Month_To_Day_Generated__c,'
                   					  + ' PVR_Produced_Cond__c, PVR_Produced_Gas__c, PVR_Produced_Oil__c, PVR_Produced_Record_Count__c,'
                   					  + ' PVR_Produced_Sand__c, PVR_Produced_Water__c, Product_Strategy__c,'
                   					  + ' (Select Id, Name, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c,'
	                                  + ' PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c,' 
	                                  + ' PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,'
	                                  + ' PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,'
	                                  + ' PVR_Hours_On_Prod__c, PVR_Month_To_Day_Generated__c,'
	       					  		  + ' PVR_Produced_Cond__c, PVR_Produced_Gas__c, PVR_Produced_Oil__c, PVR_Produced_Record_Count__c,'
	       					  		  +	' PVR_Produced_Sand__c, PVR_Produced_Water__c, Status__c, Product_Strategy__c,'
	       					  		  + ' PVR_GOR_Factor__c, PVR_Fuel_Consumption__c, Measured_Vent_Rate__c,'
	       					  		  + ' GOR_Test_Date__c, GOR_Effective_Date__c, Monthly_Trucked_Oil__c, Single_Well_Battery__c'
	                                  + ' From Well_Events__r '
                                	  + ' ORDER BY Name DESC NULLS LAST)'
                   					  + ' From Location__c'
                   					  + ' Where RecordType.DeveloperName = \'Well_ID\'';
    List<Database.SaveResult> updateErrorResults;
	
	global HOG_PVRUpdateBatch() {
		System.debug('HOG_PVRUpdateBatch initiating with query: ' + allSurfaceLocationsQuery);
		updateErrorResults = new List<Database.SaveResult>();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('HOG_PVRUpdateBatch starting\nJob Id: ' + BC.getJobId() + '\nChunk Job Id: ' + BC.getChildJobId());
		return Database.getQueryLocator(allSurfaceLocationsQuery);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('HOG_PVRUpdateBatch executing ... ');

		List<Location__c> surfaceLocations = (List<Location__c>) scope;
		HOG_PVRAverageVolumeUtilities.updatePVRAverageVolume(surfaceLocations);
		HOG_PVRAverageVolumeUtilities.updateProductStrategy(surfaceLocations);
		updateErrorResults.addAll(HOG_PVRAverageVolumeUtilities.commitToDB(surfaceLocations));
	}
	
	global void finish(Database.BatchableContext BC){
	   // Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	      TotalJobItems, CreatedBy.Email, CreatedBy.Id
	      FROM AsyncApexJob WHERE Id =:BC.getJobId()];
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   mail.setTargetObjectId(a.CreatedBy.Id);
	   mail.setSaveAsActivity(false);
	   mail.setSubject('HOG PVR Average Volume Update ' + a.Status);
	   mail.setPlainTextBody
	   ('The batch Apex job processed ' + a.TotalJobItems +
	   ' batches with '+ a.NumberOfErrors + ' failures.');
	   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}