/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureCylinder
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureCylinderTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder;
        EPD_FormStructureCylinder result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = new EPD_Cylinder_Information__c();
        EPD_FormStructureCylinder result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cylinder.Block_Information__c, result.blockInformationId);
        System.assertEquals(cylinder.RecordTypeId, result.recordTypeId);
        System.assertEquals(cylinder.Cylinder_Number__c, result.cylinderNumber);
        System.assertEquals(cylinder.Head_Replacement__c, result.headReplacement);
        System.assertEquals(cylinder.Wear_Exhaust__c, result.wearExhaust);
        System.assertEquals(cylinder.Wear_Intake__c, result.wearIntake);
        System.assertEquals(cylinder.Compression__c, result.compression);
        System.assertEquals(cylinder.Exhaust_Temperature__c, result.exhaustTemperature);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureCylinder result;

        Test.startTest();
        try {
            result = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cylinder.Block_Information__c, result.blockInformationId);
        System.assertEquals(cylinder.RecordTypeId, result.recordTypeId);
        System.assertEquals(cylinder.Cylinder_Number__c, result.cylinderNumber);
        System.assertEquals(cylinder.Head_Replacement__c, result.headReplacement);
        System.assertEquals(cylinder.Wear_Exhaust__c, result.wearExhaust);
        System.assertEquals(cylinder.Wear_Intake__c, result.wearIntake);
        System.assertEquals(cylinder.Compression__c, result.compression);
        System.assertEquals(cylinder.Exhaust_Temperature__c, result.exhaustTemperature);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = new EPD_Cylinder_Information__c();
        EPD_Cylinder_Information__c result;

        Test.startTest();
        try {
            result = (EPD_Cylinder_Information__c) new EPD_FormStructureCylinder().setRecord(cylinder).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cylinder.Block_Information__c, result.Block_Information__c);
        System.assertEquals(cylinder.RecordTypeId, result.RecordTypeId);
        System.assertEquals(cylinder.Cylinder_Number__c, result.Cylinder_Number__c);
        System.assertEquals(cylinder.Head_Replacement__c, result.Head_Replacement__c);
        System.assertEquals(cylinder.Wear_Exhaust__c, result.Wear_Exhaust__c);
        System.assertEquals(cylinder.Wear_Intake__c, result.Wear_Intake__c);
        System.assertEquals(cylinder.Compression__c, result.Compression__c);
        System.assertEquals(cylinder.Exhaust_Temperature__c, result.Exhaust_Temperature__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Cylinder_Information__c result;

        Test.startTest();
        try {
            result = (EPD_Cylinder_Information__c) new EPD_FormStructureCylinder().setRecord(cylinder).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(cylinder.Block_Information__c, result.Block_Information__c);
        System.assertEquals(cylinder.RecordTypeId, result.RecordTypeId);
        System.assertEquals(cylinder.Cylinder_Number__c, result.Cylinder_Number__c);
        System.assertEquals(cylinder.Head_Replacement__c, result.Head_Replacement__c);
        System.assertEquals(cylinder.Wear_Exhaust__c, result.Wear_Exhaust__c);
        System.assertEquals(cylinder.Wear_Intake__c, result.Wear_Intake__c);
        System.assertEquals(cylinder.Compression__c, result.Compression__c);
        System.assertEquals(cylinder.Exhaust_Temperature__c, result.Exhaust_Temperature__c);
    }

    @IsTest
    static void getThresholdWarnings_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder)).getThresholdWarnings(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getThresholdWarnings_noWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals('', result);
    }

    @IsTest
    static void getThresholdWarnings_recessionWarningOnIntake() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        cylinder.Wear_Intake__c = 0.12;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
        System.assert(result.contains('Intake'));
    }

    @IsTest
    static void getThresholdWarnings_recessionWarningOnExhaust() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        cylinder.Wear_Exhaust__c = 0.12;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
        System.assert(result.contains('Exhaust'));
    }

    @IsTest
    static void getThresholdWarnings_warningOnCompression() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 240,
                Max_Cylinder_Compression__c = 250
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
        System.assert(result.contains('Compression'));
    }

    @IsTest
    static void getThresholdWarnings_allWarnings() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.05,
                Valve_Seat_Warning_Limit__c = 0.05,
                Min_Cylinder_Compression__c = 240,
                Max_Cylinder_Compression__c = 250
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
        System.assert(result.contains('Intake'));
        System.assert(result.contains('Exhaust'));
        System.assert(result.contains('Compression'));
    }

    @IsTest
    static void hasReachedThreshold_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder)).hasReachedThreshold(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void hasReachedThreshold_noWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        Boolean expectedResult = false;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_onIntake() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        cylinder.Wear_Intake__c = 0.12;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_onExhaust() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        cylinder.Wear_Exhaust__c = 0.12;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );
        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_onCompression() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 240,
                Max_Cylinder_Compression__c = 250
        );
        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);

        Boolean expectedResult;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedRetrospectiveThreshold(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Cylinder_Information__c cylinder2 = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        Boolean expectedResult = false;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedRetrospectiveThreshold(
                            (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder2)
                    );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Cylinder_Information__c cylinder2 = prepareCylinder(EPD_FieldDefaultsWrong.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .hasReachedRetrospectiveThreshold(
                            (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder2)
                    );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);

        String expectedResult;
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getRetrospectiveThresholdWarnings(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Cylinder_Information__c cylinder2 = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        String expectedResult = '';
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getRetrospectiveThresholdWarnings(
                            (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder2)
                    );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Cylinder_Information__c cylinder = prepareCylinder(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Cylinder_Information__c cylinder2 = prepareCylinder(EPD_FieldDefaultsWrong.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder))
                    .getRetrospectiveThresholdWarnings(
                            (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder2)
                    );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
    }

    private static EPD_Cylinder_Information__c prepareCylinder(String className) {
        return (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Cylinder_Information__c(),
                className,
                false
        );
    }

    @IsTest
    static void testClassicGettersAndSetters_emptyValues() {
        EPD_FormStructureCylinder structure = new EPD_FormStructureCylinder();
        structure.wearExhaustClassic = '';
        structure.wearIntakeClassic = '';
        structure.compressionClassic = '';
        structure.exhaustTemperatureClassic = '';

        System.assertEquals(null, structure.wearExhaustClassic);
        System.assertEquals(null, structure.wearIntakeClassic);
        System.assertEquals(null, structure.compressionClassic);
        System.assertEquals(null, structure.exhaustTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_properValues() {
        EPD_FormStructureCylinder structure = new EPD_FormStructureCylinder();
        structure.wearExhaustClassic = '1';
        structure.wearIntakeClassic = '1';
        structure.compressionClassic = '1';
        structure.exhaustTemperatureClassic = '1';

        System.assertEquals('1', structure.wearExhaustClassic);
        System.assertEquals('1', structure.wearIntakeClassic);
        System.assertEquals('1', structure.compressionClassic);
        System.assertEquals('1', structure.exhaustTemperatureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_wrongValues() {
        EPD_FormStructureCylinder structure = new EPD_FormStructureCylinder();

        Boolean wearExhaustClassicFail = false;
        try {
            structure.wearExhaustClassic = 'abc';
        } catch(Exception ex) {
            wearExhaustClassicFail = true;
        }

        Boolean wearIntakeClassicFail = false;
        try {
            structure.wearIntakeClassic = 'abc';
        } catch(Exception ex) {
            wearIntakeClassicFail = true;
        }

        Boolean compressionClassicFail = false;
        try {
            structure.compressionClassic = 'abc';
        } catch(Exception ex) {
            compressionClassicFail = true;
        }

        Boolean exhaustTemperatureClassicFail = false;
        try {
            structure.exhaustTemperatureClassic = 'abc';
        } catch(Exception ex) {
            exhaustTemperatureClassicFail = true;
        }

        System.assertEquals(true, wearExhaustClassicFail);
        System.assertEquals(true, wearIntakeClassicFail);
        System.assertEquals(true, compressionClassicFail);
        System.assertEquals(true, exhaustTemperatureClassicFail);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureCylinder structure = new EPD_FormStructureCylinder();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}