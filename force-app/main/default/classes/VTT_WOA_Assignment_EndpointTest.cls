/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WOA_Assignment_Endpoint and VTT_WOA_Assignment_EndpointHandler
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_Assignment_EndpointTest {

    @IsTest
    static void getVendorsPicklistValues_withAccount() {
        Account acc = VTT_TestData.createVendorAccount('Shield');
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 1;

        Test.startTest();
        result = VTT_WOA_Assignment_Endpoint.getVendors();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(acc.Id ,result.get(0).value);
        System.assertEquals(acc.Name ,result.get(0).label);
    }

    @IsTest
    static void getTradesmenByAccountId_withoutParam() {
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Account acc = VTT_TestData.createVendorAccount('Shield');
            VTT_TestData.createTradesmanContact('Nick', 'Fury', acc.Id);

            Test.startTest();
            result = VTT_WOA_Assignment_Endpoint.getTradesmen(null);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmenByAccountId_withAccount() {
        Account acc = VTT_TestData.createVendorAccount('Shield');
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = VTT_WOA_Assignment_Endpoint.getTradesmen(acc.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmenByAccountId_withRecords() {
        List<HOG_PicklistItem_Simple> result;
        Integer expectedCount = 1;
        Contact con;

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Account acc = VTT_TestData.createVendorAccount('Shield');
            con = VTT_TestData.createTradesmanContact('Nick', 'Fury', acc.Id);

            Test.startTest();
            result = VTT_WOA_Assignment_Endpoint.getTradesmen(acc.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(con.Id ,result.get(0).value);
        System.assertEquals(con.FirstName + ' ' + con.LastName ,result.get(0).label);
    }

    @IsTest
    static void getAssignedTradesmenForActivity_withoutRecord() {
        List<String> result;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            result = VTT_WOA_Assignment_Endpoint.getAssignedTradesmenForActivity(activity.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignedTradesmenForActivity_withRecord() {
        List<String> result;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            result = VTT_WOA_Assignment_Endpoint.getAssignedTradesmenForActivity(activity.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void assign_withoutTradesman() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        String response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            vendorAccountId = vendor.Id;
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = VTT_WOA_Assignment_Endpoint.assign(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(String.valueOf(VTT_WOA_Assignment_Response.SUCCESS), response);
        System.assertEquals(vendorAccountId, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
    }

    @IsTest
    static void assign_withTradesman() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        String response;
        Integer assignmentsCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            vendorAccountId = vendor.Id;
            tradesmenIds.add(tradesman.Id);
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = VTT_WOA_Assignment_Endpoint.assign(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(String.valueOf(VTT_WOA_Assignment_Response.SUCCESS), response);
        System.assertEquals(vendorAccountId, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
    }

    @IsTest
    static void unAssign_withoutTradesman() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        String response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            vendorAccountId = vendor.Id;
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = VTT_WOA_Assignment_Endpoint.unassign(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(String.valueOf(VTT_WOA_Assignment_Response.SUCCESS), response);
        System.assertEquals(null, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, assignmentsCount);
    }

    @IsTest
    static void unAssign_withTradesman() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        String response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;
        Id vendorAccountId;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            tradesmenIds.add(tradesman.Id);
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = VTT_WOA_Assignment_Endpoint.unassign(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(String.valueOf(VTT_WOA_Assignment_Response.SUCCESS), response);
        System.assertEquals(null, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, assignmentsCount);
    }

}