/*************************************************************************************************\
Author:         Martin Panak
Company:        Husky Energy
Description:    Controller for preventing edit of Work Order by Vendor Community Tradesman 
Test class      VTT_PreventEditOfWorkOrderTest 
History:        mp 06.07.2018 - Created for W-001165 Vendor is able to edit work order information
**************************************************************************************************/
public with sharing class VTT_PreventEditOfWorkOrder {
    public static Id userId {get;set;}
    public static String retUrl {get;set;}
    public static String orderId {get;set;}

    public VTT_PreventEditOfWorkOrder(ApexPages.StandardController stdController) {
        userId = userinfo.getuserId();
        orderId = ApexPages.currentPage().getParameters().get('id');
        retUrl = ApexPages.currentPage().getParameters().get('retURL');
        retUrl = retUrl.replace('/a', '');
    }

    /**
     * [Redirect to standard/community edit page for Work Order]
     * - For Vendor Community Tradesmen and Supervisors show error Page
     * - For all other users redirect to standard edit Page for Work Order
     * @return [PageReference]
     */
    public PageReference redirect(){
        if(isPortalUser()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, System.Label.VTT_PreventEditOfOrder_ErrorMessage));
            return null;
        } else {
            PageReference editPage = new PageReference('/'+ orderId+'/e?retURL=%2Fa'+retUrl+ '&nooverride=1');   
            editPage.setRedirect(true);
            return editPage;
        }
    }

    /**
     * [Check if the current user is a portal user]
     * @return [Boolean]
     */
    public static Boolean isPortalUser(){
        String userType = UserInfo.getUserType();
        if(userType == 'PowerPartner'){
            return true;
        }
        return false;
    }
}