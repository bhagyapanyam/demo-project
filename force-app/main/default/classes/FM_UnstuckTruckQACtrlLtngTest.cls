/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for class FM_UnstuckTruckQACtrlLtng
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
@IsTest
private class FM_UnstuckTruckQACtrlLtngTest {

    private static final String UNSTUCK = 'Unstuck';
    private static final String STUCK = 'Stuck';

    @IsTest
    static void getActiveStuckTruckId_withCorrectId_WrongStuckTruckStatus() {
        FM_Truck_Trip__c truckTrip = createTestTruckTrip(UNSTUCK);
        HOG_CustomResponse response;

        Test.startTest();
        response = FM_UnstuckTruckQACtrlLtng.getActiveStuckTruckId(truckTrip.Id);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(2, response.errors.size());
    }

    @IsTest
    static void getActiveStuckTruckId_withCorrectId_CorrectStuckTruckStatus() {
        FM_Truck_Trip__c truckTrip = createTestTruckTrip(STUCK);
        HOG_CustomResponse response;

        Test.startTest();
        response = FM_UnstuckTruckQACtrlLtng.getActiveStuckTruckId(truckTrip.Id);
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assert(response.errors == null);
        System.assert(response.resultObjects != null);
        System.assertEquals(1, response.resultObjects.size());
    }

    @IsTest
    static void getActiveStuckTruckId_withWrongId() {
        HOG_CustomResponse response;

        Test.startTest();
        response = FM_UnstuckTruckQACtrlLtng.getActiveStuckTruckId('123456789abcdef');
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(2, response.errors.size());
    }

    @IsTest
    static void getActiveStuckTruckId_withoutId() {
        HOG_CustomResponse response;

        Test.startTest();
        response = FM_UnstuckTruckQACtrlLtng.getActiveStuckTruckId(null);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(2, response.errors.size());
    }

    private static FM_Truck_Trip__c createTestTruckTrip(String stuckTruckStatus) {
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);

        Field__c field = HOG_TestDataFactory.createAMU(true);

        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c unit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);

        Route__c route = HOG_TestDataFactory.createRoute(true);

        Location__c location = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);

        Facility__c facility = HOG_TestDataFactory.createFacility(route.Id, field.Id, true);

        FM_Load_Request__c lr = FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'O', null,
                'Night',
                location.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                true);


        List<FM_Truck_Trip__c> truckTrips = FM_TestDataFactory.createTruckTrips(new List<FM_Load_Request__c>{lr},
                'Today',
                FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED,
                unit.Id,
                true);

        if(STUCK.equals(stuckTruckStatus)) {
            List<FM_Stuck_Truck__c> stuckTrucks = FM_TestDataFactory.createStuckTrucks(truckTrips, false);
            for(FM_Stuck_Truck__c stuckTruck : stuckTrucks) {
                stuckTruck.Location__c = location.Id;
            }
            insert stuckTrucks;
        }

        return truckTrips.get(0);
    }

}