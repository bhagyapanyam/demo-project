/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class to eas manipulating with exceptions.
Test Class:     HOG_ExceptionUtilsTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public with sharing class HOG_ExceptionUtils {

    /**
     * Throws formatted exception of type HOG_Exception.
     * Does pre-validation on provided params first.
     *
     * @param errorMsg
     * @param values
     */
    public static void throwError(String errorMsg, List<String> values) {
        validateThrowError(errorMsg, values);
        throw new HOG_Exception(
                String.format(
                        errorMsg,
                        values
                )
        );
    }

    /**
     * Throws exception of type HOG_Exception based on provided error message.
     * Error message is run through Error Prettify Service.
     *
     * @param errorMsg
     */
    public static void throwError(String errorMsg) {
        throw new HOG_Exception(
                HOG_ErrorPrettifyService.prettifyErrorMessage(errorMsg)
        );
    }

    /**
     * Validates inputs for throwError method.
     * If inappropriate params are provided, error is thrown.
     *
     * @param errorMsg
     * @param values
     */
    private static void validateThrowError(String errorMsg, List<String> values) {
        if(String.isBlank(errorMsg)) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Missing_Param,
                    new List<String> {'Error Message'}
            );
        }
        if(values == null || values.size() == 0) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Missing_Param,
                    new List<String> {'Formatted values'}
            );
        }
    }

}