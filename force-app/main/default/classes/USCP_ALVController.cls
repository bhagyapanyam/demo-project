//DTN Allocation Viewer Controller
public class USCP_ALVController{


    public Boolean IsAdmin {get;set;}
    public Boolean isDTNReady{get;set;}
    public String SoldTo {get;set;}
    public String SecurityToken {get;set;}
    public DateTime TokenCreated {get; set;}    


    public USCP_ALVController()
    {
    
      
       try {
       
            string accountid = USCP_Utils.UserAccountID();
            if(accountid==null)
            {
                IsAdmin = true;
                isDTNReady = false;
            }
            else
            {                        

                SoldTo  = [select id, Name, SMS_Number__c  from Account  where id = :accountid limit 1].SMS_Number__c;
                Refresh(); //SoldTo ='121355'; // '122896';

            }            
        } 
        catch (Exception e) 
        {  
            USCP_Utils.logError('Unexpected Allocation Viewer Error: ' + e );      
        }            
    }
    
    public PageReference Refresh()
    {
        if(SoldTo!='')
        {    
            SecurityToken  = USCP_DTN_API.GetSecurityToken(SoldTo); 
            /*
            if (USCP_DTN_API.Ping()==false)
            {
                return null;
            }
            */
            //SecurityToken  ='';
            isDTNReady = (SecurityToken != null);
            TokenCreated  = datetime.now();    
    
            PageReference pageRef = null;
            return pageRef;
        }  
        return null;      
    }

    @RemoteAction
    public static List<Account > searchAccount(String searchAccount) {
        System.debug('Account Name is: '+searchAccount);
        List<Account> result = Database.query('Select id, Name, SMS_Number__c from Account where RecordType.DeveloperName = \'USCP_Account_Record\'   and (name like \'%' + String.escapeSingleQuotes(searchAccount) + '%\' or Customer_Number__c  like \'%'  + String.escapeSingleQuotes(searchAccount) + '%\' )  order by Name ');
        //RecordType.DeveloperName = \'USCP_Account_Record\'         
        return result ;
    }   
    


}