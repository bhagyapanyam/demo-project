public with sharing class SPCC_VAAControllerX {

    private String projectId;

    public SPCC_Vendor_Account_Assignment__c vaa {get; set;}
    public String projectName {get; private set;}
    public Boolean duplicateFound {get; private set;}
    public Boolean isError {get {return SPCC_Utilities.errorDisplayed;} private set;}


    public SPCC_VAAControllerX(ApexPages.StandardController stdController) {
        this.vaa = (SPCC_Vendor_Account_Assignment__c)stdController.getRecord();
        this.projectId = ApexPages.CurrentPage().getParameters().get('EWR_Id'); 
        vaa.Engineering_Work_Request__c = projectId;
        List<SPCC_Engineering_Work_Request__c> lEWR = [SELECT Id, Project_Name__c FROM SPCC_Engineering_Work_Request__c WHERE Id =:  projectId];
        this.projectName = lEWR[0].Project_Name__c;
        this.duplicateFound = false;
    }



    public PageReference saveVAA() {
        SPCC_Utilities.checkForDuplicate = false; //Avoid  duplcation check trigger from fire
        List<SPCC_Vendor_Account_Assignment__c> vaaList = new List<SPCC_Vendor_Account_Assignment__c>();

        //duplication check
        vaaList =  [SELECT Engineering_Work_Request__c, Engineering_Work_Request__r.Project_Name__c, 
                           Engineering_Work_Request__r.Name, 
                           Vendor_Account__c,Vendor_Account__r.Name
                    FROM SPCC_Vendor_Account_Assignment__c
                    WHERE Engineering_Work_Request__c = :vaa.Engineering_Work_Request__c
                    AND Vendor_Account__c = :vaa.Vendor_Account__c];

        //Disaply Error message if duplicate found
        if (!vaaList.isEmpty()){

            this.duplicateFound = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                    'Engineering Work Request is already assigned with this Vendor Account. Please choose different Vendor for this EWR.' ));
        } 

        //No duplicate found, Insert PO
        else { 

            this.duplicateFound = false;   

            try {
                insert vaa;                     
            } catch (DmlException ex) {

                SPCC_Utilities.logErrorOnPage(ex);
            }
        } 

         return null;
    }
}