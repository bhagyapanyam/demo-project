@isTest

public class cpmbdcpereturner_test {
    static list<cpm_BD_PCE__c> myBdpceList= new list<cpm_BD_PCE__c>();
    static cpm_BD_PCE__c bdpce;
    static integer numOfBD;
    static Opportunity opp = new Opportunity();
    static map<id,cpm_BD_PCE__c> MapOfBDpceById = new map <id,cpm_BD_PCE__c>();
    
    static void createData() {
	
        Account acc = new Account();
            acc.Name='Test Account';
            insert acc;
            
        Contact cont = new Contact();
            cont.FirstName ='Test';
            cont.LastName ='Test';
            cont.accountid =acc.id;
            insert cont;
        
       
            opp.Name = 'Test Opportunity';
            opp.CloseDate = date.today();
            opp.AccountId = acc.id;
            opp.LeadSource = 'Association';
            opp.StageName = 'Prospecting';
        //	opp.cpm_Data_Collection_Status__c = '';
        	insert opp;
        
        // vytvorenie BD objektu a insertnutie (my to chceme vytvarat v setup metode)
        for(Integer i = 0; i < numOfBD; i++){ //integer od do
        myBdpceList.add(new cpm_BD_PCE__c(
        	Opportunity__c = opp.id,
        	Name = 'Testing BD PCE record',
        	cpm_Category__c = 'BD Key Costs',
        	cpm_Quantity__c = 2,
        	cpm_Unit__c = 'Each',
        	cpm_Rate__c = 20,
        	cpm_Comments__c = 'testing comment'));
       }
        MapOfBDpceById.putAll(myBdpceList);
        
        
        insert myBdpceList;
     }  
    
    static testMethod void testOneRecord(){
        numOfBD=1;
        createData();
        
        list<cpm_BD_PCE__c> lstBDbeforeAssert = [select id, Opportunity__c from cpm_BD_PCE__c where id IN: myBdpceList ]; 
        system.debug(logginglevel.error, 'lstBDbeforeAssert' + lstBDbeforeAssert.size());
        
        Apexpages.StandardController sc = new Apexpages.standardController(opp);						
        
        cpmbdcpereturner ext = new cpmbdcpereturner(sc);
        
        // checknutie hodnot z databazy pre record testOu a z page ext.lstPointVersion[0]
        										
  		System.assertEquals(myBdpceList[0].Name, ext.lstBDPCE[0].Name );
        System.assertEquals(myBdpceList[0].cpm_Category__c, ext.lstBDPCE[0].cpm_Category__c); 
        System.assertEquals(myBdpceList[0].cpm_Quantity__c, ext.lstBDPCE[0].cpm_Quantity__c); 
        System.assertEquals(myBdpceList[0].cpm_Unit__c, ext.lstBDPCE[0].cpm_Unit__c); 
        System.assertEquals(myBdpceList[0].cpm_Rate__c, ext.lstBDPCE[0].cpm_Rate__c );
  		System.assertEquals(myBdpceList[0].cpm_Comments__c, ext.lstBDPCE[0].cpm_Comments__c );
        
       
        ext.lstBDPCE[0].Name = 'test1';
        ext.lstBDPCE[0].cpm_Category__c = 'BD Key Costs';
       	ext.lstBDPCE[0].cpm_Comments__c = 'test comment';
		ext.lstBDPCE[0].cpm_Unit__c = 'Each';
       	ext.lstBDPCE[0].cpm_Quantity__c = 2;
        ext.lstBDPCE[0].cpm_Rate__c = 2;
  		
        Test.startTest();

        
  		ext.save();
   
  		Test.stopTest();   

        
  		list<cpm_BD_PCE__c> lstBDtoAssert = new list<cpm_BD_PCE__c> ([select id, Name, cpm_Category__c, cpm_Comments__c , cpm_Unit__c, cpm_Quantity__c, cpm_Rate__c
                                                                        from cpm_BD_PCE__c
                                                                        where id IN: myBdpceList
                                                                        ]);
        
        // assertnem hodnoty
  			   
  			System.assertEquals(lstBDtoAssert[0].Name , ext.lstBDPCE[0].Name );
  			System.assertEquals(lstBDtoAssert[0].cpm_Category__c , ext.lstBDPCE[0].cpm_Category__c );
  			System.assertEquals(lstBDtoAssert[0].cpm_Comments__c , ext.lstBDPCE[0].cpm_Comments__c );
  			System.assertEquals(lstBDtoAssert[0].cpm_Unit__c , ext.lstBDPCE[0].cpm_Unit__c );
  			System.assertEquals(lstBDtoAssert[0].cpm_Quantity__c , ext.lstBDPCE[0].cpm_Quantity__c );
  			System.assertEquals(lstBDtoAssert[0].cpm_Rate__c , ext.lstBDPCE[0].cpm_Rate__c );    
        }
    
    
    @isTest static void testMethodForPageReference(){
		numOfBD=1;
        createData();
        
		ApexPages.StandardController sController = new ApexPages.StandardController(opp);
    	Test.startTest();
		PageReference pageRef = new cpmbdcpereturner(sController).save();
        Test.stopTest();
        
        System.assert(pageRef.getRedirect()==true, 'Get reference has not been set to true');
             
        
    }

    static testMethod void testbulkRecords(){
        numOfBD = 20;
        createData();
        
        //map<id,cpm_BD_PCE__c> mapBDbeforeAssert = new map<id,cpm_BD_PCE__c>([select id, Opportunity__c from cpm_BD_PCE__c where id IN: myBdpceList ]);
        
        Apexpages.StandardController sc = new Apexpages.standardController(opp);
        cpmbdcpereturner ext = new cpmbdcpereturner(sc);
        
        /*for(Integer i = 0; i < numOfBD; i++){
			System.assertEquals(myBdpceList[i].Name, ext.lstBDPCE[i].Name );
            System.assertEquals(myBdpceList[i].cpm_Category__c, ext.lstBDPCE[i].cpm_Category__c); 
            System.assertEquals(myBdpceList[i].cpm_Quantity__c, ext.lstBDPCE[i].cpm_Quantity__c); 
            System.assertEquals(myBdpceList[i].cpm_Unit__c, ext.lstBDPCE[i].cpm_Unit__c); 
            System.assertEquals(myBdpceList[i].cpm_Rate__c, ext.lstBDPCE[i].cpm_Rate__c );
            System.assertEquals(myBdpceList[i].cpm_Comments__c, ext.lstBDPCE[i].cpm_Comments__c );    
        }*/
        for(Integer j = 0; j < numOfBD; j++){
            ext.lstBDPCE[j].Name = 'test'+j;
            ext.lstBDPCE[j].cpm_Category__c = 'BD Key Costs';
            ext.lstBDPCE[j].cpm_Comments__c = 'test comment';
            ext.lstBDPCE[j].cpm_Unit__c = 'Each';
            ext.lstBDPCE[j].cpm_Quantity__c = 2;
            ext.lstBDPCE[j].cpm_Rate__c = 2;            
        }
        Test.startTest();
  		ext.save();
  		Test.stopTest();   
        
        map<id,cpm_BD_PCE__c> mapBDtoAssert = new map<id,cpm_BD_PCE__c>([
            select id, Name, cpm_Category__c, cpm_Comments__c , cpm_Unit__c, cpm_Quantity__c, cpm_Rate__c
            from cpm_BD_PCE__c 
            where id IN: myBdpceList ]);
        
        for(cpm_BD_PCE__c bdpce : ext.lstBDPCE){
            System.assertEquals(mapBDtoAssert.get(bdpce.Id).Name,bdpce.Name,'Names after save are different.');
            System.assertEquals(mapBDtoAssert.get(bdpce.Id).cpm_Category__c,bdpce.cpm_Category__c,'Names after save are different.');
			System.assertEquals(mapBDtoAssert.get(bdpce.Id).cpm_Comments__c,bdpce.cpm_Comments__c,'Names after save are different.');
			System.assertEquals(mapBDtoAssert.get(bdpce.Id).cpm_Unit__c,bdpce.cpm_Unit__c,'Names after save are different.');
			System.assertEquals(mapBDtoAssert.get(bdpce.Id).cpm_Quantity__c,bdpce.cpm_Quantity__c,'Names after save are different.');
			System.assertEquals(mapBDtoAssert.get(bdpce.Id).cpm_Rate__c,bdpce.cpm_Rate__c,'Names after save are different.');
            
        }
        
    }
        
	
}