/*-------------------------------------------------------------------------------------------------
Author     : Marcel Brimus
Company    : Husky Energy
Description: Controller for Vendor Portal Management Page used by Vendor SuperVisors in Vendor Community
Inputs     : N/A
Test Class : VendorPortalManagementControllerTest
Changes    : 
---------------------------------------------------------------------------------------------------*/ 
public without sharing class VendorPortalManagementController {
	
	public Profile vendorProfile {get; private set;}
    public Account vendorAccount {get; private set;}
    //public List<Contact> portalEnabledVendorContacts {get; private set;}
    //public List<Contact> portalDisabledVendorContacts {get; private set;}
    
    //Used for conditional rendering
    public Boolean isPortalEnabled {get;private set;}
    public String contactIdToEnablePortal {get; set;}
    public String contactIdToDisablePortal {get; set;}
    public String userType {get; set;}

    // used for paginantion
    public Integer sizePortalEnabled {get;set;} 
    public Integer sizePortalDisabled {get;set;} 
    public Integer noOfRecordsPortalEnabled {get; set;}
    public Integer noOfRecordsPortalDisabled {get; set;}
    public List<SelectOption> paginationSizeOptionsPortalEnabled {get; set;}
    public List<SelectOption> paginationSizeOptionsPortalDisabled {get; set;}
    
    //Account of logged in user
    public List<Account> userAccount {get; private set;}
    //Contact of logged in user
	public List<Contact> userContact {get; private set;}

    
	public VendorPortalManagementController() {
		this.vendorProfile = [Select id,name from Profile where name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE limit 1];
        populateVendorPortalUsers();
        // default pagination list size, values
        this.sizePortalEnabled=10;
        this.paginationSizeOptionsPortalEnabled = new List<SelectOption>();
        this.paginationSizeOptionsPortalEnabled.add(new SelectOption('10','10'));
        this.paginationSizeOptionsPortalEnabled.add(new SelectOption('25','25'));
        this.paginationSizeOptionsPortalEnabled.add(new SelectOption('50','50'));
        this.paginationSizeOptionsPortalEnabled.add(new SelectOption('100','100'));
        this.sizePortalDisabled=10;
        this.paginationSizeOptionsPortalDisabled = new List<SelectOption>();
        this.paginationSizeOptionsPortalDisabled.add(new SelectOption('10','10'));
        this.paginationSizeOptionsPortalDisabled.add(new SelectOption('25','25'));
        this.paginationSizeOptionsPortalDisabled.add(new SelectOption('50','50'));
        this.paginationSizeOptionsPortalDisabled.add(new SelectOption('100','100'));
	}

    public void populateVendorPortalUsers(){
        //Find contact that has User__C field assigned to logged in user
        this.userContact = [SELECT id, AccountId, Account.Name 
                            FROM Contact 
                            WHERE User__c =: UserInfo.getUserId() 
                            AND AccountId IN (SELECT Id FROM Account WHERE IsPartner = true)];
        
        //Current User has no refence to contact that is under portal enabled Account
        if (this.userContact.isEmpty()) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: User has no contact assigned or this contact is not under portal enabled Account...'));
            this.isPortalEnabled = false;
        
        } else if (this.userContact.size() > 1){
            
            //This means that current user has multiple contacts (referencing him) that are under portal enabled account
            String message = 'Error: Your user is referenced in multiple contacts that are under portal enabled account, please remove reference from one of them: ';
            for(Contact c : this.userContact){
                message += '<a href="/'+c.id +'">Referenced in '+c.Account.Name + '</a>  ';
            }
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,message));
        
        } else {
            
            //We should have only one Account at this point.. 
            this.userAccount = [SELECT id, Total_Licenses__c, Remaining_Licenses__c, Used_Licenses__c, Name FROM Account WHERE id =: this.userContact[0].AccountId AND IsPartner = true Limit 1];
            System.debug('ACCOUNT ' + this.userContact[0].AccountId);
            if(!this.userAccount.isEmpty()) {
            	this.isPortalEnabled = true;
            	this.vendorAccount = this.userAccount[0];

	        } else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Account has not been enabled for vendor portal...'));
            	this.isPortalEnabled = false;
			}
           
        }
    }
    
    // pagination controller for portal DISABLED users
    public ApexPages.StandardSetController setConPortalDisabled {
        get {
            if(setConPortalDisabled == null) {                
                setConPortalDisabled = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT 
                           id, Name, firstName, lastName, is_Vendor_Portal_User__c, email, 
                           AccountId 
                           FROM Contact 
                           WHERE AccountId =: this.userAccount[0].id 
                           AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
                           AND is_Vendor_Portal_User__c = false
                           ORDER BY Name]));
                setConPortalDisabled.setPageSize(this.sizePortalDisabled);  
                this.noOfRecordsPortalDisabled = setConPortalDisabled.getResultSize();
            }            
            return setConPortalDisabled;
        }
        set;
    }
    // pagination controller for portal ENABLED users
    public ApexPages.StandardSetController setConPortalEnabled {
        get {
            if(setConPortalEnabled == null) {                
                setConPortalEnabled = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT id, 
                               Name, firstName, lastName, is_Vendor_Portal_User__c, 
                               email, User_Name__c, AccountId 
                               FROM Contact 
                               WHERE AccountId =: this.userAccount[0].id 
                               AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
                               AND is_Vendor_Portal_User__c = true
                               AND User__c <> null
                               ORDER BY Name]));
                setConPortalEnabled.setPageSize(this.sizePortalEnabled);  
                this.noOfRecordsPortalEnabled = setConPortalEnabled.getResultSize();
            }            
            return setConPortalEnabled;
        }
        set;
    }   
    public List<Contact> getPortalEnabledVendorSet() {
        return (List<Contact>) setConPortalEnabled.getRecords();
    }
    public List<Contact> getPortalDisabledVendorSet() {
        return (List<Contact>) setConPortalDisabled.getRecords();
    }   
    
    //Changes the size of pagination
    public PageReference refreshPageSizePortalEnabled() {
         setConPortalEnabled.setPageSize(this.sizePortalEnabled);
         return null;
    }        
    public PageReference refreshPageSizePortalDisabled() {
         setConPortalDisabled.setPageSize(this.sizePortalDisabled);
         return null;
    }        

    //This method creates Portal user under this contact
    //Firing trigger on User object that will add this user to Public Group (for sharing) and assign him PS
    //Using this on previously activated user will re-activate him, removes and adds correct permission set
    public PageReference enablePortalUser(){
    	 System.debug('Enabling this contact as user: '+ contactIdToEnablePortal);
    	 Contact c = [SELECT id, Name, firstName, lastName, is_Vendor_Portal_User__c, email, AccountId FROM Contact WHERE id =: contactIdToEnablePortal];
    	 
        if(c.firstName == null || c.lastName == null || c.email == null ){
    	 	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: First Name, Last Name and Email cannot be empty on contact...'));
    	 } else if(this.vendorAccount.Remaining_Licenses__c < 1){
    	 	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: You cannot create any more users because your Account doesnt have any more remaining licenses'));
    	 } else {
    	 	VendorportalUtility.createOrReactivatePortalUser(contactIdToEnablePortal, c.email, c.firstName, c.lastName, vendorProfile.id, userType);
    	 }
    	 return null;
    }

    //If Vendor portal admin clicks this it will send and email to HOG Admins notifing them that this user needs to be disabled
    public PageReference disablePortalUser(){
    	System.debug('Sending email to disable this contact: ' + contactIdToDisablePortal);
        List<User> portalUserToDisable = [SELECT Id, Name, ContactId, Contact.Name FROM User WHERE ContactId =:contactIdToDisablePortal];
    	
        //Hog admin profile
        Profile HogAdmin = [Select id,name from Profile where name = 'Standard HOG - Administrator' limit 1];

        //Get all HOG Admins emails
        List<User> admins = [SELECT id, email FROM User WHERE profileId =:HogAdmin.id AND IsActive = true];

        //If its null then it means that this user is already disabled or doesnt exist
        if(portalUserToDisable.size() > 0){
            
            String adminPageUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/VendorPortalManagement_Admin';

            //Prepare message for admin
            String adminEmailText = UserInfo.getName() + ' has requested to disable this contact: ' 
                                + portalUserToDisable[0].Contact.Name + ' for vendor: ' 
                                + this.vendorAccount.name 
                                +'</br>'
                                + '<a href="'+adminPageUrl+'">Click here to go to Vendor Portal Management Page</a>';
            String adminEmailSubjectLine = 'Disable user for ' + this.vendorAccount.name;
            
            //HOG Admins emails
            List<String> toAddresses = new List<String> {'HOGLloydSF@huskyenergy.com'};
            //if(admins.size() > 0){
            //   for (User u: admins){
            //       toAddresses.add(u.email);
            //   }
            //}

            Messaging.SingleEmailMessage emailToAdmin = new Messaging.SingleEmailMessage();
            emailToAdmin.setSubject(adminEmailSubjectLine);
            emailToAdmin.setToAddresses(toAddresses);
            emailToAdmin.setHtmlBody(adminEmailText);

            //Prepare message for requester
            List<String> requesterEmail = new List<String>{UserInfo.getUserEmail()};
            String requesterEmailText = 'You have requested disabling of this user: ' + portalUserToDisable[0].Contact.Name 
                                        + ' if this was done by mistake please contact HOGLloydSF@huskyenergy.com as soon as possible';
            String requesterEmailSubjectLine = 'Requested disabling of this user: ' + portalUserToDisable[0].Contact.Name;

            Messaging.SingleEmailMessage emailToRequester = new Messaging.SingleEmailMessage();
            emailToRequester.setSubject(requesterEmailSubjectLine);
            emailToRequester.setToAddresses(requesterEmail);
            emailToRequester.setHtmlBody(requesterEmailText);

            System.debug('Sending email to disable ' + portalUserToDisable[0] + ' to these users: ' + toAddresses);

            try {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailToAdmin,emailToRequester});
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Administrator has been notified and this user will be disabled'));
                return null;
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Failed to Send Email. Please try again. Please contact your Administrator if the error persists'));
                System.debug('Error sending email: ' + e.getMessage());
                return null;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: This user has already been disabled'));
        }
        return null;
    }

    
    
}