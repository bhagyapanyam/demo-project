@isTest
private class TestHOGTriggers 
{
    @isTest static void ServiceRequest_Test()
    {
        test.startTest();
        
        /*
        //--- Setup Route
        Route__c route = 
          new Route__c
              (
                  Name = '999'
              );              
        insert route;  
                              
        //--- Setup Location
        Location__c location = 
            new Location__c
                (
                    Name = 'Test Location'        
                );
        insert location;
        */
        
        Business_Unit__c businessUnit = new Business_Unit__c
            (           
                Name = 'Test Business Unit'
            );            
        insert businessUnit;

        Business_Department__c businessDepartment = new Business_Department__c
            (           
                Name = 'Test Business Department'
            );            
        insert businessDepartment;

        Operating_District__c operatingDistrict = new Operating_District__c
            (           
                Name = 'Test Field',
                Business_Department__c = businessDepartment.Id,
                Business_Unit__c = businessUnit.Id
            );            
        insert operatingDistrict;

        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        Field__c field = new Field__c
            (           
                Name = 'Test Field Control Centre',
                Operating_District__c = operatingDistrict.Id,
                RecordTypeId = recordTypeId
            );            
        insert field;
        
        Route__c route = new Route__c
              (
                  Name = '999'
              );     
        insert route;                  
                              
        Location__c location = 
            new Location__c
                (
                    Name = 'Test Location' ,
                    Route__c = route.id, 
                    Operating_Field_AMU__c = field.id
                );
        insert location;        
        //
                
        //-- Test createNewRiglessServicingForms and UpdateOperatorRoute triggers
        Service_Request_Form__c recordServiceRequest = 
            new Service_Request_Form__c
                (
                    Name = 'Test SR',
                    Location__c = location.Id,
                    Route_if_different__c = '001',
                    Service_Required__c = 'Rod Servicing',
                    Service_Specifics__c = 'Insert Pump Change',
                    Priority__c = 'Low',
                    M3_Oil_Day__c = 0.50,
                    Well_Down_on__c = Date.today(),
                    Work_Details__c = 'This is a test'
                );    
        insert recordServiceRequest;
        
        //-- Test updateWorkOrder trigger
        update recordServiceRequest;
        
        //--- Test deleteRiglessServicingForm trigger
        delete recordServiceRequest;
        test.stopTest();

    }

    //@isTest(SeeAllData=true) static void RiglessServicing_Test()
    @isTest static void RiglessServicing_Test()
    {

        Rigless_Servicing_Form__c recordRiglessServicing = 
            new Rigless_Servicing_Form__c
                (
                    Name = 'Test Rigless', 
                    Service_Status__c = 'New Request'
                );

        Service_Time__c recordServiceTime = new Service_Time__c();
        Vendor_Personnel__c recordVendorPersonnel = new Vendor_Personnel__c();

        test.startTest();

        //--- Insert Rigless_Servicing_Form__c record, testing RiglessServicingFormRecord trigger        
        insert recordRiglessServicing;
        
        //--- Insert Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        recordServiceTime.Rigless_Servicing_Form__c = recordRiglessServicing.Id;
        recordServiceTime.Day__c = 'Day 1';
        recordServiceTime.Rig_On_Location__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordServiceTime.Rig_Off_Location__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordServiceTime;
        //--
        
        //--- Update Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        update recordServiceTime;
           
        //--- Insert Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        // Set up a test Account
        Account account = new Account(Name = 'Test Account');
        insert account;

        recordVendorPersonnel.Rigless_Servicing_Form__c = recordRiglessServicing.Id;
        recordVendorPersonnel.Invoice_Category__c = 'Communication';
        recordVendorPersonnel.Vendor__c = account.Id;
        recordVendorPersonnel.On_Location_Time__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordVendorPersonnel.Off_Location_Time__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordVendorPersonnel;
        //--

        //--- Update Rigless_Servicing_Form__c record, testing RiglessServicingFormRecord and RiglessServicingFormUpdated triggers        
        recordRiglessServicing.Service_Status__c = 'Complete'; 
        update recordRiglessServicing;

        //--- Update Rigless_Servicing_Form__c record, testing RiglessServicingFormRecord and RiglessServicingFormUpdated triggers        
        recordRiglessServicing.Service_Status__c = 'New Request'; 
        update recordRiglessServicing;

        //--- Update  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        update recordVendorPersonnel;

        //--- Update Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        update recordServiceTime;

        //--- Delete Service_Time__c record, testing ServiceTimeRecord trigger
        delete recordServiceTime;

        //--- Delete  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        delete recordVendorPersonnel;
        
        test.stopTest();
        
        Rigless_Servicing_Form__c insertedRecord = 
            [SELECT Name, Service_Status__c, Id FROM Rigless_Servicing_Form__c WHERE Id = :recordRiglessServicing.Id];
                
        System.assertEquals(recordRiglessServicing.Id, insertedRecord.Id);              
                
    }
}