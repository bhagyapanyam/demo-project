public with sharing class LOM_DashboardPieChartController {

    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}    

    public String MultiSiteNumber {get;set;}  

    public Boolean DepartmentSalesHasData {get;set;}   
    public Boolean CarWashHasData {get;set;}       
    public Boolean FuelSalesHasData {get;set;}   
    
    public List<StatementData> DepartmentSalesData{get; set;}    
    public List<StatementData> DepartmentSalesDataBySite{get; set;} 
    public List<StatementData> DepartmentSalesDataByDepartment{get; set;}    
         
    public List<StatementData> CarWashDataBySite{get; set;}
    public List<StatementData> CarWashDataByDepartment{get; set;}    
            
    public List<StatementData> FuelSalesData{get; set;}        
    public List<StatementData> FuelSalesDataBySite{get; set;}            
    public List<StatementData> FuelSalesDataByGrade {get; set;}                
    
    public String DepartmentSalesType {get;set;}    
    public String FuelSalesType {get;set;}        
    
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            return d.format() ;
        }
        set
        {
            dateFrom = convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            return d.format() ;
        }
        set
        {
            dateTo =  convertDate(value); //Date.valueOf(value);
        }
    }    
    
    

    public LOM_DashboardPieChartController (){
    
        dateFrom = Date.Today()-1;
        dateTo = Date.Today()-1;
        
        DepartmentSalesType  = 'dealer';
        FuelSalesType  = 'dealer';
        
        
        retrieveMultiSiteNumber();
    }
    
    public PageReference RefreshData()
    {
    system.debug('RefreshData');
        retrieveDepartmentSales();
        retrieveFuelSales();        
        retrieveCarWashData();        
        return null;
    }   
    public PageReference RefreshDepartmentSalesData()
    {
    system.debug('RefreshData');
        retrieveDepartmentSales();
        return null;
    }      
    public PageReference RefreshFuelSalesData()
    {
    system.debug('RefreshData');
        retrieveFuelSales();
        return null;
    }      
    public PageReference RefreshCarWashData()
    {
    system.debug('RefreshData');
        retrieveCarWashData();
        return null;
    }      
    
    public void retrieveMultiSiteNumber() {
    List<String> strSiteNumbers = new List<String>();
    AggregateResult[] AgR;
    
    AgR = [select Retail_Cluster__r.Name Name from Retail_Location__c Group by Retail_Cluster__r.Name order by Retail_Cluster__r.Name];
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            strSiteNumbers.add(String.valueOf(s.get('Name')));
         }
         MultiSiteNumber =  String.join( strSiteNumbers, ', ' );     
    }


    public void retrieveDepartmentSales() {
    
    //exclude motor oil and car wash codes
    List<String> excludedCodes = new List<String> {'100','88','92','93','94'};
    
        List<StatementData> tempData;
        AggregateResult[] AgR;


        tempData= new List<StatementData>();

        AgR = [SELECT Retail_Location__r.Name Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c not in :excludedCodes  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY Retail_Location__r.Name]; 
          
        DepartmentSalesHasData = (AgR.size() > 0);
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         DepartmentSalesHasData = !tempData.isEmpty();
         DepartmentSalesDataBySite = tempData;

        tempData = new List<StatementData>();
          AgR = [SELECT Department__c Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c not in :excludedCodes  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY  Department__c ORDER BY SUM(Sales__c) DESC LIMIT 10];        
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         DepartmentSalesDataByDepartment = tempData;

    }




//retrieveCarWashData
    public void retrieveCarWashData() {
    
    //only include car wash codes
    List<String> excludedCodes = new List<String> {'88','92','93','94'};
    
        List<StatementData> tempData;
        AggregateResult[] AgR;

        tempData = new List<StatementData>();

          AgR = [SELECT Retail_Location__r.Name Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c in :excludedCodes  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY Retail_Location__r.Name]; 

        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         CarWashDataBySite= tempData;
         CarWashHasData = (tempData.size()>0);


        tempData = new List<StatementData>();

          AgR = [SELECT Department__c Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c in :excludedCodes  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY  Department__c ORDER BY SUM(Sales__c) DESC LIMIT 10];        
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         CarWashDataByDepartment= tempData;
    }


    

//FuelSales
    public void retrieveFuelSales() {
        List<StatementData> data;
        AggregateResult[] AgR;
        
        data = new List<StatementData>();
         AgR = [SELECT Retail_Location__r.Name Name, SUM(Total_Vol__c) Total FROM LOM_Daily_Fuel_Sale__c
          WHERE Total_Vol__c > 0  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY Retail_Location__r.Name]; 
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            data.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         FuelSalesHasData = (data.size()>0);
         FuelSalesDataBySite =  data;         
         

        data = new List<StatementData>();
         AgR = [SELECT  Grade__c Name, SUM(Total_Vol__c) Total FROM LOM_Daily_Fuel_Sale__c
          WHERE Total_Vol__c > 0  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          GROUP BY  Grade__c];         
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            data.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         FuelSalesDataByGrade =  data;    

    }

    public List<StatementData> getUnpaidStatementsByDealer() {
        List<StatementData> data = new List<StatementData>();
        
          AggregateResult[] AgR = [SELECT Retail_Location__r.Name Location, SUM(Amount__c) Total FROM LOM_Invoice__c
          WHERE Amount__c > 0 AND Paid_Of_Date__c = NULL
          GROUP BY Retail_Location__r.Name]; 
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            data.add(new StatementData( String.valueOf(s.get('Location')), Integer.valueof(s.get('Total'))));
         }
         
        return data;
    }    
    

    // Wrapper class
    public class StatementData{

        public String name { get; set; }
        public Integer data { get; set; }
        public String dataCurrency { get; set; }        
        public String dataDecimal { get; set; }          

        public StatementData(String name,  Integer data) {
            this.name = name;
            this.data = data;


            //this.dataCurrency  = '$' + string.valueOf(data);
            List<String> args = new String[]{'0','number','###,###,###,##0.00'};
            String s = String.format(data.format(), args);
            System.debug(s);
            this.dataCurrency  ='$' + s;
            this.dataDecimal = s;

            
        }
    }
    
  // --------------------------------------------------------------------------
  
    
    //convert string dates in dd/mm/yyyy format into Date
    private Date convertDate(String inDate) {
           String[] dateParts = inDate.split('/');
           Integer day = Integer.valueOf(dateParts[0]);
           Integer month= Integer.valueOf(dateParts[1]);           
           Integer year = Integer.valueOf(dateParts[2]);                      
          

           Date dt = Date.newInstance(year , month, day);
              
           return dt ;    
  }    
  
    
}