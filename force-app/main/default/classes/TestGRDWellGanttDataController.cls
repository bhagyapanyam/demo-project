@isTest
private class TestGRDWellGanttDataController
{
    @isTest (SeeAllData = false)
    public static void doTest()
    {
        GRD_Borehole__c borehole = new GRD_Borehole__c(Name = 'test',
                                                       RTD_Initiated__c = Date.newInstance(2014,10,1),
                                                       Go_No_Go_Decision__c = Date.newInstance(2014,10,31),
                                                       Lease_Construction_Start__c = Date.newInstance(2014,10,1),
                                                       Lease_Construction_End__c = Date.newInstance(2014,10,31),
                                                       Drilling_Start__c = Date.newInstance(2014,10,1),
                                                       Drilling_End__c = Date.newInstance(2014,10,31),
                                                       Frac_Start__c = Date.newInstance(2014,10,1),
                                                       Frac_End__c = Date.newInstance(2014,10,31),
                                                       Test_Start__c = Date.newInstance(2014,10,1),
                                                       Test_End__c = Date.newInstance(2014,10,31),
                                                       Extended_Test_Start__c = Date.newInstance(2014,10,1),
                                                       Extended_Test_End__c = Date.newInstance(2014,10,31),
                                                       Production_Tubing_Start__c = Date.newInstance(2014,10,1),
                                                       Production_Tubing_End__c = Date.newInstance(2014,10,31),
                                                       Early_Production_Facility_Start__c = Date.newInstance(2014,10,1),
                                                       Early_Production_Facility_End__c = Date.newInstance(2014,10,31),
                                                       Tie_In_Start__c = Date.newInstance(2014,10,1),
                                                       Tie_In_End__c = Date.newInstance(2014,10,31),
                                                       Tank_Farms_Start__c = Date.newInstance(2014,10,1),
                                                       Tank_Farms_End__c = Date.newInstance(2014,10,31),
                                                       Production_Start__c = Date.newInstance(2014,10,1),
                                                       Production_End__c = Date.newInstance(2014,10,31));
        Database.saveResult boreResult = Database.insert(borehole);
        
        Test.startTest();
        
        PageReference GRDWellGanttJSON = Page.GRDWellGanttJSON;
        GRDWellGanttJSON.getParameters().put('Id',boreResult.getId());
        Test.setCurrentPage(GRDWellGanttJSON);
        
        GRDWellGanttDataController controller = new GRDWellGanttDataController();
        
        controller.getjsonMilestones();
        
        Test.stopTest();
    }
}