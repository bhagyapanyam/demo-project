/*************************************************************************************************
Author:         Martin Bilicka
Company:        Husky Energy
Related Class:  WS_TestDataFactory
Description:    Well Services's SObjects field defaults for testing purposes.
History:        mbilicka 25.3.2019 - Created.
***************************************************************************************************
WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
***************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but don't change any values these. If for any reason
you have to then rerun following tests:
WS_WellOptimizationControllerXTest
**************************************************************************************************/
@isTest
public with sharing class WS_FieldDefaults {

    public static final String CLASS_NAME = 'WS_FieldDefaults';

    public class Well_OptimizationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    Well_Optimization__c.Initial_Speed__c => 0
            };
        }
    }
}