@isTest(SeeAllData=true)
public class PTauditControllerTestClass {
    
    private static testmethod void CreateData(){
        PTTestData.createPTauditControllerRecords();
    }
    
    private static testmethod void albertaSiteListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = true;
        ptAudit.skActivate = false;
        ptAudit.albertaSelected = true;
        ptAudit.saskSelected = false;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.sitesList = '0010007660014';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();
    }
    
    private static testmethod void albertaCostCenterListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = true;
        ptAudit.skActivate = false;
        ptAudit.albertaSelected = true;
        ptAudit.saskSelected = false;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.costCenterList = '10063019';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();      
    }
    
    private static testmethod void albertaDateListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = true;
        ptAudit.skActivate = false;
        ptAudit.albertaSelected = true;
        ptAudit.saskSelected = false;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.dateList = '01/01/2016';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();        
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();    
    }
    
    private static testmethod void saskSiteListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = false;
        ptAudit.skActivate = true;
        ptAudit.albertaSelected = false;
        ptAudit.saskSelected = true;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.sitesList = '500003794368';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();        
    }
    
    private static testmethod void saskCostCenterListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = false;
        ptAudit.skActivate = true;
        ptAudit.albertaSelected = false;
        ptAudit.saskSelected = true;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.costCenterList = '11064152';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();        
    }
    
    private static testmethod void saskDateListInfo(){
        PTTestData.createPTauditControllerRecords();
        PTauditController ptAudit = new PTauditController();
        ptAudit.abActivate = false;
        ptAudit.skActivate = true;
        ptAudit.albertaSelected = false;
        ptAudit.saskSelected = true;
        ptAudit.dateFromString = '01/01/2016';
        ptAudit.dateToString = '01/01/2016';
        ptAudit.dateList = '01/01/2016';
        ptAudit.GetAuditController();
        ptAudit.downloadReport();
        ptAudit.hideReport();
        ptAudit.disableAlberta();
        ptAudit.disableSask();        
    }
    
}