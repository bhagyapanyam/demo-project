/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Handler class that handles basic logic from SNR_Endpoint requests.
Test Class:     SNR_EndpointTest
History:        mbrimus 2020-01-20 - Created.
*************************************************************************************************/
public inherited sharing class SNR_EndpointHandler {
    private static final String CLASS_NAME = String.valueOf(SNR_EndpointHandler.class);

    /**
     * Handles save requests from Endpoint. Deserialize payload
     *
     * @param snrRequest
     * @param flocModel
     *
     * @return SNR_Response
     */
    public SNR_Response handleSave(String objectType, String snrRequest, String flocModel) {
        SNR_Response response = new SNR_Response();
        HOG_Service_Request_Notification_Form__c serviceRequestForm = new HOG_Service_Request_Notification_Form__c();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType =
                new SNR_Utilities.LightningNotificationRecordType();

        // First check to see if value of object api is supported - it has to match the old mapping so that
        // record type is properly set
        // This is validated on Init in loadInitialValues so we can access the map
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(objectType);

        if (String.isNotBlank(snrRequest) && String.isNotBlank(flocModel)) {
            SNR_FLOCModel flocModelData = (SNR_FLOCModel) JSON.deserialize(flocModel.trim(), SNR_FLOCModel.class);
            SNR_Request snrRequestData = (SNR_Request) JSON.deserialize(snrRequest.trim(), SNR_Request.class);

            serviceRequestForm = createNotificationForm(flocModelData, snrRequestData, ServiceRequestNotificationUtilities.readObjectRecordTypeId(
                    notificationRecordType.NOTIFICATIONOBJECTNAME, recordTypeName));

            if (flocModelData.generateNumbersFromSAP) {
                if (flocModelData.generateNotificationNumber) {
                    try {
                        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
                        SAPHOGNotificationServices.CreateNotificationResponse sapResponse = createNotificationAndGetNumber(
                                notification,
                                flocModelData,
                                serviceRequestForm,
                                snrRequestData);
                        if (sapResponse.type_x) {
                            serviceRequestForm.Notification_Number__c = sapResponse.Notification.NotificationNumber;
                            serviceRequestForm.SAP_Generated_Notification_Number__c = true;
                            serviceRequestForm.SAP_Changed_Date__c = sapResponse.Notification.ChangedDate;
                            serviceRequestForm.SAP_Changed_By__c = sapResponse.Notification.ChangedBy;
                        } else {
                            return new SNR_Response(
                                    'SAP Response: ' + sapResponse.Message
                            );
                        }
                    } catch (Exception e) {
                        return new SNR_Response(
                                'Error when calling SAP: ' + e.getMessage()
                        );
                    }
                    // TODO ADD Case for generating wo numbers here
                }

                if (flocModelData.showPVRAvgInfo) {
                    populatePVR(serviceRequestForm, flocModelData);
                }
            }

            Savepoint sp = Database.setSavepoint();

            try {
                upsert serviceRequestForm;
            } catch (Exception ex) {
                System.debug(CLASS_NAME + ' handleSave exception line: ' + ex.getLineNumber());
                System.debug(CLASS_NAME + ' handleSave exception trace: ' + ex.getStackTraceString());
                System.debug(CLASS_NAME + ' handleSave exception message: ' + ex.getMessage());
                System.debug(CLASS_NAME + ' handleSave exception cause: ' + ex.getCause());
                response = new SNR_Response(ex);
                Database.rollback(sp);
            }

        } else {
            response = new SNR_Response(
                    'Invalid request - missing snrRequest & flocModel'
            );
        }
        return response;
    }

    private void populatePVR(HOG_Service_Request_Notification_Form__c serviceRequestForm, SNR_FLOCModel flocModelData) {
        serviceRequestForm.PVR_AVGVOL_5D_GAS__c = flocModelData.avgVol5DGas;
        serviceRequestForm.PVR_AVGVOL_5D_OIL__c = flocModelData.avgVol5DOil;
        serviceRequestForm.PVR_AVGVOL_5D_COND__c = flocModelData.avgVol5DCond;
        serviceRequestForm.PVR_AVGVOL_5D_SAND__c = flocModelData.avgVol5DSand;
        serviceRequestForm.PVR_AVGVOL_5D_WATER__c = flocModelData.avgVol5DWater;
        serviceRequestForm.PVR_AVGVOL_UPDATED__c = flocModelData.pvrAvgVolUpdated;
        serviceRequestForm.PVR_AVGVOL_5D_LAST_MEASURED__c = flocModelData.pvrAvgVol5DLastMeasured;
        serviceRequestForm.PVR_AVGVOL_5D_NO_OF_MEASURES__c = flocModelData.pvrAvgVol5DNoOfMeasures;
    }

    private HOG_Service_Request_Notification_Form__c createNotificationForm(
            SNR_FLOCModel flocModelData,
            SNR_Request snrRequestData,
            Id recordTypeId) {

        HOG_Service_Request_Notification_Form__c form = new HOG_Service_Request_Notification_Form__c();
        form.HOG_Notification_Type_Priority__c = snrRequestData.servicePriorityId;
        //form.HOG_Notification_Type_Priority_WO__c = generateWorkOrderNumber && generateNumbersFromSAP ? servicePriorityWorkOrder : null;
        form.HOG_Work_Order_Type__c = snrRequestData.serviceSpecificsId;
        form.Vendor_Company__c = snrRequestData.vendorCompanyId;
        form.RecordTypeId = recordTypeId;
        form.Supervised__c = flocModelData.serviceSupervised;
        form.Title__c = snrRequestData.title;
        form.Work_Details__c = snrRequestData.workDetails;
        form.Equipment_Down__c = snrRequestData.equipmentDown;
        form.Production_Impacted__c = snrRequestData.productionImpacted;
        form.Malfunction_Start_Date__c = snrRequestData.malfunctionStartDate;

        form.Well_Event_Selected__c = flocModelData.wellEventSelected;
        form.SAP_Object_ID__c = flocModelData.functionalLocationObjectId;
        form.Maintenance_Plant__c = flocModelData.maintenancePlanningPlant;
        form.Main_Work_Centre__c = flocModelData.mainWorkCentre;

        // Set proper floc
        form.Business_Unit_Lookup__c = flocModelData.operatingBusiness;
        form.Operating_District_Lookup__c = flocModelData.operatingDistrict;
        form.Operator_Route_Lookup__c = flocModelData.operatingRoute;
        form.Operating_Field_AMU_Lookup__c = flocModelData.operatingField;
        form.Facility_Lookup__c = flocModelData.operatingFacility;
        form.Location_Lookup__c = flocModelData.operatingLocation;
        form.Well_Event__c = flocModelData.operatingWellEvent;
        form.System__c = flocModelData.operatingSystem;
        form.Sub_System__c = flocModelData.operatingSubSystem;
        form.Functional_Equipment_Level__c = flocModelData.operatingFel;
        form.Yard__c = flocModelData.operatingYard;
        form.Equipment__c = flocModelData.operatingEquipment;

// TODO THIS when migrating for internal users
//        serviceRequestForm.Generated_Work_Order_Record__c =
//                insertRecord ? generateWorkOrderRecord : serviceRequestFormRecord.size() > 0 ?
//                        serviceRequestFormRecord[0].Generated_Work_Order_Record__c : false;
//        serviceRequestForm.Generated_Rig_Program_Record__c =
//                insertRecord ? generateRigProgramRecord : serviceRequestFormRecord.size() > 0 ?
//                        serviceRequestFormRecord[0].Generated_Rig_Program_Record__c : false;
        form.Functional_Location_Status__c = flocModelData.functionalLocationStatus;

        return form;
    }

    /**
     * Calls SAP PI to create SNR and returns Notification Number
     *
     * @param notification
     * @param flocModelData
     * @param serviceRequestForm
     * @param snrRequestData
     *
     * @return
     */
    private SAPHOGNotificationServices.CreateNotificationResponse createNotificationAndGetNumber(
            SAPHOGNotificationServices.NotificationPort notification,
            SNR_FLOCModel flocModelData,
            HOG_Service_Request_Notification_Form__c serviceRequestForm,
            SNR_Request snrRequestData) {
        SAPHOGNotificationServices.CreateNotificationResponse sapResponse = new SAPHOGNotificationServices.CreateNotificationResponse();

        List<AggregateResult> serviceSpecifics = SNR_DAOProvider.snrDao.getServiceSpecifics(
                snrRequestData.serviceCategoryId,
                snrRequestData.serviceTypeId,
                snrRequestData.serviceActivityId,
                snrRequestData.serviceRequiredId,
                flocModelData.wellLocationStatus == 'Producing');

        List<HOG_Notification_Type_Priority__c> priorities = SNR_DAOProvider.snrDao.getServicePriorities(snrRequestData.serviceTypeId);
        Map<String, String> matCodeMap = SNR_Utilities.getServiceMATCodesMap(serviceSpecifics);
        Map<String, String> priorityMap = SNR_Utilities.getServicePriorityMap(priorities);

        String maintenanceActivityType = matCodeMap.get(snrRequestData.serviceSpecificsId);
        String priority = priorityMap.get(snrRequestData.servicePriorityId);

        String userName = String.isEmpty(UserInfo.getFirstName()) ? '' : UserInfo.getFirstName().toUpperCase();
        String reportedBy = UserInfo.getLastName().toUpperCase() + ' ' + userName;

        String equipmentDownString = snrRequestData.equipmentDown ? 'X' : '';

        if (Test.isRunningTest()) {
            sapResponse.type_x = true;
            sapResponse.Notification = new SAPHOGNotificationServices.Notification();
        } else {
            sapResponse = notification.CreateNotification(
                    String.isBlank(flocModelData.equipmentNumber) ? flocModelData.functionalLocationObjectId : null, // do not pass FLOC ID if notification is for Equipment
                    flocModelData.maintenancePlanningPlant,
                    serviceRequestForm.Title__c,
                    serviceRequestForm.Work_Details__c,
                    priority,
                    flocModelData.notificationTypeCode,
                    null,
                    maintenanceActivityType,
                    flocModelData.notificationCodeGroup,
                    reportedBy,
                    flocModelData.equipmentNumber,
                    serviceRequestForm.Malfunction_Start_Date__c,
                    null,
                    equipmentDownString
            );
        }

        return sapResponse;
    }
}