/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Vendor_Personnel__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class VendorPersonnelTestData
{
    public static HOG_Vendor_Personnel__c createVendorPersonnel
    (
		Id workOrderId,
		Id vendorId,
		String invoiceCategory,
		DateTime onLocation,
		DateTime offLocation
   	)
    {                
        HOG_Vendor_Personnel__c results = new HOG_Vendor_Personnel__c
            (
                Work_Order__c = workOrderId,
                Vendor__c = vendorId,
		        Invoice_Category__c = invoiceCategory,
		        On_Location_Time__c = onLocation,
		        Off_Location_Time__c = offLocation
            );
            
        return results;
    }
}