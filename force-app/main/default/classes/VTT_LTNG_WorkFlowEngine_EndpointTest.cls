/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_WorkFlowEngine_Endpoint class
History:        mbrimus 21/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_WorkFlowEngine_EndpointTest {

    @IsTest
    static void loadEngineState_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_WorkFlowEngine_Endpoint.loadEngineState(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result.resultObjects);
    }

    @IsTest
    static void loadEngineState_wrongID() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = VTT_LTNG_WorkFlowEngine_Endpoint.loadEngineState(Id.valueOf('asdasdasdasdasd'));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void loadEngineState_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = VTT_LTNG_WorkFlowEngine_Endpoint.loadEngineState(activity.Id);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result.resultObjects);
    }

    @IsTest
    static void suppressEPD_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_WorkFlowEngine_Endpoint.suppressEPD(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void suppressEPD_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = VTT_LTNG_WorkFlowEngine_Endpoint.suppressEPD(true, activity.Id);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void suppressEPD_withOutData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_WorkFlowEngine_Endpoint.suppressEPD(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getPermitHolderData_withData() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                handler.handleSaveActionWithCheckList(jobOnHoldAction, activity, tradesman, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesman.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            result = VTT_LTNG_WorkFlowEngine_Endpoint.getPermitHolderData(activity, tradesman);

            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);
        String checklistId = (String) resultsMap.get('checkListId');
        Boolean isPermitHolder = (Boolean) resultsMap.get('isPermitHolder');

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, checklistId);
        System.assertEquals(true, isPermitHolder);
    }

    @IsTest
    static void getPermitHolderData_withOutData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.getPermitHolderData(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getDataForJobComplete_withOutData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.getDataForJobComplete(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getDamage_withOutData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.getDamage(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getDamage_withData() {
        HOG_CustomResponseImpl result;

        HOG_Damage__c equipmentPartDamage = HOGDamageTestData.createHOGDamage('Test Damage', 'DMGECODE', 'Test Damage', 'H1', true);
        insert equipmentPartDamage;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.getDamage('H1');
        Test.stopTest();

        System.assertNotEquals(null, result);
        List<HOG_Damage__c> results = (List<HOG_Damage__c>) result.resultObjects.get(0);
        System.assertEquals(1, results.size());

    }

    @IsTest
    static void canActivityBePutOnHold_withData() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        Work_Order_Activity__c activity;
        Contact tradesman;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                handler.handleSaveActionWithCheckList(jobOnHoldAction, activity, tradesman, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesman.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            Test.stopTest();
        }

        result = VTT_LTNG_WorkFlowEngine_Endpoint.canActivityBePutOnHold(activity, tradesman);

        System.assertNotEquals(null, result);
        Boolean canBePutOnHold = (Boolean) result.resultObjects.get(0);
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, canBePutOnHold);
    }

    @IsTest
    static void canActivityBePutOnHold_withOutData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.canActivityBePutOnHold(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void hasActivityChanged_withOutData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.hasActivityChanged(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void hasActivityChanged_withData() {
        HOG_CustomResponseImpl result;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            result = VTT_LTNG_WorkFlowEngine_Endpoint.hasActivityChanged(activity);
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void saveAction_noDataInRequest() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_WorkFlowEngine_Endpoint.saveAction(null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void saveAction_withoutCheckList_ActivityChanged() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        Work_Order_Activity__c activity;
        Contact tradesman;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;
                result = VTT_LTNG_WorkFlowEngine_Endpoint.saveAction(jobOnHoldAction, activity, tradesman, null);
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void saveAction_withoutCheckList_ActivityHasNotChanged() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        Work_Order_Activity__c activity;
        Contact tradesman;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

                result = VTT_LTNG_WorkFlowEngine_Endpoint.saveAction(jobOnHoldAction, activity, tradesman, null);
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void saveAction_withCheckList_ButActivityChanged() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        Work_Order_Activity__c activity;
        Contact tradesman;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;
                result = VTT_LTNG_WorkFlowEngine_Endpoint.saveAction(jobOnHoldAction, activity, tradesman, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesman.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void saveAction_withCheckList_ButActivityHasNotChanged() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action);
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        Work_Order_Activity__c activity;
        Contact tradesman;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                handler.handleSaveAction(startAtEq, activity, tradesman);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

                result = VTT_LTNG_WorkFlowEngine_Endpoint.saveAction(jobOnHoldAction, activity, tradesman, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesman.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                expectedFailFlag = true;
            }
            Test.stopTest();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }
}