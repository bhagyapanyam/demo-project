/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class MaintenanceServicingBulkALTTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
       
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        
        PageReference pageRef = Page.MaintenanceServicingBulkALT;
        Test.setCurrentPage(pageRef);        
        
        
        System.runAs(runningUser) {

            setup();  
            
            Test.startTest();
        
            List<HOG_Maintenance_Servicing_Form__c> workorders = [select id, name from HOG_Maintenance_Servicing_Form__c limit 3];
            
            ApexPages.StandardSetController stdcon = new ApexPages.StandardSetController(workorders);
            stdcon.SetSelected(workorders);    


            MaintenanceServicingBulkALTControllerX setCtrl  = new MaintenanceServicingBulkALTControllerX(stdcon);

                                
            
            
            system.assertEquals(1, setCtrl.CurrentIndex); 
            system.assertNotEquals(null, setCtrl.CurrentWorkOrder); 
            system.assertEquals(3, setCtrl.selectedList.Size()); 
            
            
            HOG_Maintenance_Servicing_Form__c CurrentWorkOrder = setCtrl.CurrentWorkOrder;
            
            HOG_Maintenance_Servicing_Form__c WorkOrder2 = setCtrl.selectedList[1];
            HOG_Maintenance_Servicing_Form__c WorkOrder3 = setCtrl.selectedList[2];
            
            system.assertEquals(false, setCtrl.getHasPreviousWorkOrder()); 
            system.assertEquals(true, setCtrl.getHasNextWorkOrder());            

            setCtrl.NextWorkOrder();
            system.assertEquals(2, setCtrl.CurrentIndex); 
            system.assertEquals(WorkOrder2 , setCtrl.CurrentWorkOrder); 
            system.assertEquals(true, setCtrl.getHasPreviousWorkOrder()); 
            system.assertEquals(true, setCtrl.getHasNextWorkOrder()); 

            setCtrl.NextWorkOrder();
            system.assertEquals(3, setCtrl.CurrentIndex);             
            system.assertEquals(WorkOrder3 , setCtrl.CurrentWorkOrder); 
            system.assertEquals(true, setCtrl.getHasPreviousWorkOrder()); 
            system.assertEquals(false, setCtrl.getHasNextWorkOrder());                        

            //lets try to go over the last work order
            setCtrl.NextWorkOrder();
            system.assertEquals(3, setCtrl.CurrentIndex); //still 3            
            system.assertEquals(WorkOrder3 , setCtrl.CurrentWorkOrder); 
            system.assertEquals(true, setCtrl.getHasPreviousWorkOrder()); 
            system.assertEquals(false, setCtrl.getHasNextWorkOrder());  

            setCtrl.PreviousWorkOrder();
            system.assertEquals(2, setCtrl.CurrentIndex); 
            system.assertEquals(WorkOrder2 , setCtrl.CurrentWorkOrder); 
            system.assertEquals(true, setCtrl.getHasPreviousWorkOrder()); 
            system.assertEquals(true, setCtrl.getHasNextWorkOrder()); 
            
            system.assertEquals(null,setCtrl.ConfirmWorkOrder());
                                                              
            Test.stopTest();   
        }
    }

    public static void setup() {
        String serviceCategoryCode = 'MNT';  // Maintenance service category code
        String notificationTypeCode = 'WP';
        String orderTypeCode = 'WP01';
        String matCode = 'TXN';

        Id recordTypeId;

        HOG_Service_Category__c serviceCategory = new HOG_Service_Category__c
            (
                Name = 'Test Category',
                Service_Category_Code__c = serviceCategoryCode                                
            );
        insert serviceCategory;

        HOG_Notification_Type__c notificationType = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategory.Id,
                Notification_Type__c = notificationTypeCode,
                Order_Type__c = orderTypeCode
            );
        insert notificationType;

        MaintenanceServicingUtilities.executeTriggerCode = false;   // do not to execute trigger      

        List<HOG_Maintenance_Servicing_Form__c> maintenanceWORecords = new List<HOG_Maintenance_Servicing_Form__c>();
        HOG_Maintenance_Servicing_Form__c record1 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = '97654321',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            HOG_Notification_Type__c = notificationType.Id
        );
        maintenanceWORecords.add(record1);

        HOG_Maintenance_Servicing_Form__c record2 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = '97654322',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            HOG_Notification_Type__c = notificationType.Id            
        );
        maintenanceWORecords.add(record2);

        HOG_Maintenance_Servicing_Form__c record3 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = '97654323',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = true,
            HOG_Notification_Type__c = notificationType.Id
        );
        maintenanceWORecords.add(record3);

        insert maintenanceWORecords;
    }    
}