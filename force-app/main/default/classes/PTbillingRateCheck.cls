public with sharing class PTbillingRateCheck {
    public List<Utility_Billing__c> utilityBillingList {get;set;}
    public utility_billing__c utilityBilling {get;set;}
    public String dateFromString {get;set;}
    public String dateToString {get;set;}
    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}
    public Boolean checkedFlag {get;set;}
    
    public void checkBillingRate(){
        Map<id,Utility_Billing__c> uBillingMap = PTBillingRateCheckCacheQuery.queryInfo();
        List<Utility_Billing__c> temporaryBillingList = new List<Utility_Billing__c>();
        utilityBillingList = new List<Utility_Billing__c>();
        dateFrom = null;
        dateTo = null;
        //We grab the values of the inputField dates if there are any values
        if(dateFromString != null && String.isNotEmpty(dateFromString)){
            dateFrom = date.parse(dateFromString);}
        if(dateToString != null && String.isNotEmpty(dateToString)){
            dateTo = date.parse(dateToString);}
        //-------------End
        //This section is to query the information to populate the report
        //Next lines are to transfer the values from the map to a list
        for(Utility_Billing__c billing: uBillingMap.values()){
            temporaryBillingList.add(billing);
        }
		//This section evaluates the different fields of the report        
        if(dateFrom != null && dateTo == null){
            for(Utility_Billing__c utilityBilling: temporaryBillingList){
                if(utilityBilling.Billing_Date__c < dateFrom){
                    uBillingMap.remove(utilityBilling.Id);
                }
            }    
        }        
        else if(dateFrom == null && dateTo !=null){
            for(Utility_Billing__c utilityBilling: temporaryBillingList){
                if(utilityBilling.Billing_Date__c > dateTo){
                    uBillingMap.remove(utilityBilling.Id);
                }
            }              
        }
        else if(dateFrom != null && dateTo !=null){
            for(Utility_Billing__c utilityBilling: temporaryBillingList){
                if(utilityBilling.Billing_Date__c > dateTo || utilityBilling.Billing_Date__c < dateFrom){
                    uBillingMap.remove(utilityBilling.Id);
                }
            }             
        }
        
        if(checkedFlag == true){
            for(Utility_Billing__c utilityBilling: temporaryBillingList){
                if(utilityBilling.Billing_Checked__c == true){
                    uBillingMap.remove(utilityBilling.id);
                }
            }
        }
        
        //-------------End
        //This nex section is to insert the values in the final list if the service rate and the billing rate are different
        integer finalCounter = 0;
        for(Utility_Billing__C utilityBilling : uBillingMap.values()){
            if(utilityBilling.service_rate__c != null && utilityBilling.utility_rate__c != null && finalCounter < 1000){
                if(utilityBilling.service_rate__c != utilityBilling.utility_rate__C){
                    utilityBillingList.add(utilityBilling);
                    finalCounter++;
                }    
            }		  
        }
        //-------------End
    }
    
    public void initializeReport(){ //This method initializes the report 
        utilityBilling = new utility_billing__c();
        dateFromString = null;
        dateToString = null;
        checkBillingRate();
    }
}