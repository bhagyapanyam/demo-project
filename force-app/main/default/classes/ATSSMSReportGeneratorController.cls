public class ATSSMSReportGeneratorController{
    String[] categories= new String[]{};
    String[] customers= new String[]{};
    public String opportunityList {get;set;}
    
    Map<String, String> categoriesMap = new Map<String, String>();
    Map<String, String> customersMap = new Map<String, String>();
    List<SelectOption> OpportunityOptions = new List<SelectOption>();
    List<SelectOption> CustomerOptions = new List<SelectOption>();
                
    private final ATS_Parent_Opportunity__c tender;
   
    
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public ATSSMSReportGeneratorController(ApexPages.StandardController stdController) {
        this.tender= (ATS_Parent_Opportunity__c)stdController.getRecord();

        for(Opportunity opp: [select id, name, Opportunity_ATS_Product_Category__r.name, RecordType.Name from Opportunity where 
        Opportunity_ATS_Product_Category__c = :tender.id and  StageName = 'Won'])
        {
            String oppName = opp.name + ' (' + opp.RecordType.Name + ')';
            OpportunityOptions.add(new SelectOption(opp.id,oppName ));
            categoriesMap.put(opp.id,opp.name);
            categories.add(opp.id);
            
        }

        for(ATS_Tender_Customers__c obj: [select Account__r.id, name, Account__r.name, Account__r.ATS_Customer_Type__c  from ATS_Tender_Customers__c 
                                        where ATS_Parent_Opportunity__c = :tender.id
                                        and Won__c=true])
        {
            SelectOption customerOption;
            
            if(obj.Account__r.ATS_Customer_Type__c ==null)
            {
                customerOption = new SelectOption(obj.Account__c,obj.Account__r.name + ' (Unknown Customer Type)',true);
            }
            else
            {
                customerOption = new SelectOption(obj.Account__c,obj.Account__r.name + ' (' + obj.Account__r.ATS_Customer_Type__c +  ')');   
                customers.add(obj.Account__c);         
            }
            CustomerOptions.add(customerOption);
            customersMap.put(obj.Account__c,obj.Account__r.name ); 
            
        }

         
    }

    
    public PageReference GenerateSMSReports() {

        for(String accountID: customers)
        {
            opportunityList = ''; 
            PageReference pdfPage = new PageReference( '/apex/ATSSMSReport' );
            pdfPage.getParameters().put('tenderId',tender.id); 
            pdfPage.getParameters().put('accountId',accountID); 
           
            
            
            // pdfPage.setRedirect(false);
            // generate the pdf blob
           Blob pdfBlob;
           if (Test.IsRunningTest())
           {
                pdfBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
                pdfBlob = pdfPage.getContent();
           }

            
            // create the attachment against the quote
            Attachment attachment = new Attachment(); 
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = tender.id; // the record the file is attached to
            attachment.name = 'SMS Report - ' +  customersMap.get(accountID) + '.pdf';
            attachment.IsPrivate = false;
            attachment.body = pdfBlob;
            
            //insert the attachment
            try {
                    insert attachment;            
               } catch (DMLException e) {
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
              return null;
            } 
        
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        }

        Pagereference pg = new PageReference('/'+tender.id+'#' + tender.id + '_RelatedNoteList_target');
        pg.setRedirect(true);
        return pg;
    }
    public pagereference BackMethod()
    {
        Pagereference pg =  new Pagereference('/'+tender.id);
        pg.setRedirect(true);
        return pg;
        }    
        
    
    public List<SelectOption> getOpportunityItems() {
            return OpportunityOptions ;
    }    
    
    public List<SelectOption> getCustomers() {
            return CustomerOptions ;
    }       
    

    public String[] getSelectedCategories() {
            return categories;
    }    
    public void setSelectedCategories(String[] items) {
            this.categories = items;
    }    

    public String[] getSelectedCustomers() {
            return customers;
    }    
    public void setSelectedCustomers(String[] items) {
            this.customers= items;
    }  

    
}