/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Builder class for building EPD Form Structure based on all different kind of records
                then actual EPD records. (Parents like work order activity, work order, engine, etc.
Test Class:     EPD_FormStructureBuilderTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureBuilder {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureBuilder.class);

    private HOG_Maintenance_Servicing_Form__c workOrder;
    private Work_Order_Activity__c workOrderActivity;
    private Account company;
    private Equipment_Engine__c engine;
    private EPD_Engine__mdt engineConfig;

    /**
     * Setter for workOrder record. This record is required for builder to properly build final form structure.
     *
     * @param workOrder
     *
     * @return EPD_FormStructureBuilder
     */
    public EPD_FormStructureBuilder setWorkOrder(HOG_Maintenance_Servicing_Form__c workOrder) {
        System.debug(CLASS_NAME + ' -> setWorkOrder. Work Order: ' + JSON.serialize(workOrder));
        this.workOrder = workOrder;
        return this;
    }

    /**
     * Setter for activity record. This record is required for builder to properly build final form structure.
     *
     * @param activity
     *
     * @return EPD_FormStructureBuilder
     */
    public EPD_FormStructureBuilder setWorkOrderActivity(Work_Order_Activity__c activity) {
        System.debug(CLASS_NAME + ' -> setWorkOrderActivity. Work Order Activity: ' + JSON.serialize(activity));
        workOrderActivity = activity;
        return this;
    }

    /**
     * Setter for engine record. This record is required for builder to properly build final form structure.
     *
     * @param engine
     *
     * @return EPD_FormStructureBuilder
     */
    public EPD_FormStructureBuilder setEngine(Equipment_Engine__c engine) {
        System.debug(CLASS_NAME + ' -> setEngine. Engine: ' + JSON.serialize(engine));
        this.engine = engine;
        return this;
    }

    /**
     * Setter for company record. This record is required for builder to properly build final form structure.
     *
     * @param company
     *
     * @return EPD_FormStructureBuilder
     */
    public EPD_FormStructureBuilder setCompany(Account company) {
        System.debug(CLASS_NAME + ' -> setCompany. Company: ' + JSON.serialize(company));
        this.company = company;
        return this;
    }

    /**
     * Setter for engineConfig record. This record is required for builder to properly build final form structure.
     *
     * @param engineConfig
     *
     * @return EPD_FormStructureBuilder
     */
    public EPD_FormStructureBuilder setConfig(EPD_Engine__mdt engineConfig) {
        System.debug(CLASS_NAME + ' -> setConfig. config: ' + JSON.serialize(engineConfig));
        this.engineConfig = engineConfig;
        return this;
    }

    /**
     * This method will build form structure from provided parent records.
     * It uses setters defined in EPD_FormStructure class.
     *
     * @return EPD_FormStructure
     */
    public EPD_FormStructure build() {
        System.debug(CLASS_NAME + ' -> build. START.');

        EPD_FormStructure epdFormStructure = new EPD_FormStructure()
                .setEPDStructure(prepareEPDFormStructureEPD())
                .setParts(preparePartStructures())
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                .setCOCStages(prepareCOCStageStructures())
                .setEOCs(prepareEOCRecords())
                .setEngineBlocks(prepareBlockStructures())
                .setFlags(engineConfig);

        System.debug(CLASS_NAME + ' -> build. epdFormStructure: ' + JSON.serialize(epdFormStructure));
        return epdFormStructure;
    }

    /**
     * Builds EPD_FormStructureEPD from configs and basically all related records.
     * First it take flag for Required Exhaust Measurements from engine config,
     * then it sets default values like current user, ate and so on.
     * Then it sets related records to lookup fields.
     * Then it sets location related data like LSD and so on.
     * Last step is utilizing setRecord override from EPD_FormStructureEPD class that translates those values
     * to variables located inside this class.
     *
     * @return EPD_FormStructureEPD
     */
    private EPD_FormStructureEPD prepareEPDFormStructureEPD() {
        EPD_Engine_Performance_Data__c epdRecord = new EPD_Engine_Performance_Data__c(
                Required_Exhaust_Measurements__c = engineConfig.Required_Exhaust_Measurements__c
        );

        setDefaultValues(epdRecord);
        setRelatedRecord(epdRecord);
        populateLocationData(epdRecord);
        return (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epdRecord);
    }

    /**
     * Sets default values for Engine Performance Data record, like current date and current user.
     *
     * @param epdRecord
     */
    private void setDefaultValues(EPD_Engine_Performance_Data__c epdRecord) {
        epdRecord.Data_Collected__c = Date.today();
        epdRecord.Data_Collected_By__c = UserInfo.getUserId();
        epdRecord.Data_Collected_By__r = new User(
                FirstName = UserInfo.getFirstName(),
                LastName = UserInfo.getLastName()
        );
    }

    /**
     * Sets all related records for Engine Performance Data record.
     *
     * @param epdRecord
     */
    private void setRelatedRecord(EPD_Engine_Performance_Data__c epdRecord) {
        epdRecord.Work_Order__c = workOrder.Id;
        epdRecord.Work_Order__r = workOrder;
        epdRecord.Work_Order_Activity__c = workOrderActivity.Id;
        epdRecord.Work_Order_Activity__r = workOrderActivity;
        epdRecord.Company__c = company.Id;
        epdRecord.Company__r = company;
        epdRecord.Equipment_Engine__c = engine.Id;
        epdRecord.Equipment_Engine__r = engine;
    }

    /**
     * Populates location related data for Engine Performance Data record.
     *
     * @param epdRecord
     */
    private void populateLocationData(EPD_Engine_Performance_Data__c epdRecord) {
        epdRecord.Planner_Group__c = workOrder.Planner_Group__c;
        epdRecord.LSD__c = extractLSD();
        epdRecord.Operation_Group__c = extractProductStrategy();
    }

    /**
     * It tries to extract product strategy from Location and Facility. If this information is not available, it will
     * return N/A.
     *
     * @return String
     */
    private String extractProductStrategy() {
        if(workOrder.Location__r != null &&  String.isNotBlank(workOrder.Location__r.Product_Strategy__c)) {
            return workOrder.Location__r.Product_Strategy__c;
        } else if (workOrder.Facility__r != null &&  String.isNotBlank(workOrder.Facility__r.Product_Strategy__c)) {
            return workOrder.Facility__r.Product_Strategy__c;
        }
        return 'N/A';
    }

    /**
     * It tries to extract LSD from Work Order and Facility. If this information is not available, it will
     * return N/A.
     *
     * @return String
     */
    private String extractLSD() {
        if(String.isNotBlank(workOrder.Surface_Location__c)) {
            return workOrder.Surface_Location__c;
        } else if (String.isNotBlank(workOrder.Facility__c)) {
            return workOrder.Facility__r.Name;
        }
        return 'N/A';
    }

    /**
     * Builds List<EPD_FormStructurePart> from configs.
     * As this builder is utilized only in initial load, it is good enough to create new records and just put part label
     * as a part name and send resulted list to form service to get it all wrapped into wrapper.
     *
     * @return List<EPD_FormStructurePart>
     */
    private List<EPD_FormStructurePart> preparePartStructures() {
        List<EPD_Part_Replacement__c> parts = new List<EPD_Part_Replacement__c>();

        for(EPD_Engine_Part__mdt enginePart : engineConfig.Engine_Parts__r) {
            System.debug(CLASS_NAME + ' -> createPartReplacementRecords: ' + enginePart.Display_Order__c + ' ' + enginePart.Part__r.Label);
            parts.add(
                    new EPD_Part_Replacement__c(
                            Name = enginePart.Part__r.Label,
                            Form_Order__c = enginePart.Display_Order__c
                    )
            );
        }

        System.debug(CLASS_NAME + ' -> createPartReplacementRecords: ' + JSON.serialize(parts));
        return new EPD_FormService().getPartsWrapped(parts);
    }

    /**
     * Builds List<EPD_FormStructureCOCStage> from configs.
     * As this builder is utilized only in initial load, it is good enough to create new records and just put index number
     * as a Stage Number and send resulted list to form service to get it all wrapped into wrapper.
     *
     * @return List<EPD_FormStructureCOCStage>
     */
    private List<EPD_FormStructureCOCStage> prepareCOCStageStructures() {
        List<EPD_Compressor_Operating_Condition_Stage__c>
                cocStages = new List<EPD_Compressor_Operating_Condition_Stage__c>();

        for(Integer i = 0; i < engineConfig.Number_of_COC_Stages__c; i++) {

            cocStages.add(
                    new EPD_Compressor_Operating_Condition_Stage__c(
                            Stage_Number__c = i + 1
                    )
            );

        }

        return new EPD_FormService().getCOCStagesWrapped(cocStages);
    }

    /**
     * Creates one or two block structures based on engine config.
     * Then build Cylinder information structures for them.
     *
     * @return List<EPD_FormStructureBlock>
     */
    private List<EPD_FormStructureBlock> prepareBlockStructures() {
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock>();

        if(isInlineEngine()) {
            blocks.add(createBlockStructure(EPD_Constants.BLOCK_TYPE_STRAIGHT, engineConfig));
        }
        if (isVShapedEngine()) {
            blocks.add(createBlockStructure(EPD_Constants.BLOCK_TYPE_V_SHAPED_LEFT, engineConfig));
            blocks.add(createBlockStructure(EPD_Constants.BLOCK_TYPE_V_SHAPED_RIGHT, engineConfig));
        }

        prepareCylinderStructures(blocks);

        return blocks;
    }

    /**
     * Builds single Block structure based on config and blockType.
     *
     * @param blockType
     * @param config
     *
     * @return EPD_FormStructureBlock
     */
    private EPD_FormStructureBlock createBlockStructure(String blockType, EPD_Engine__mdt config) {
        EPD_Block_Information__c block = new EPD_Block_Information__c(
                Block_Type__c = blockType
        );

        EPD_FormStructureBlock blockStruct = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(block)).setConfig(config);

        return blockStruct;
    }

    /**
     * Prepare cylinder structures for engine blocks.
     * First it will take recordType name based on map. It will then get Id for requested Record Type.
     * Then it will build cylinder structures for blocks.
     *
     * @param blocks
     */
    private void prepareCylinderStructures(List<EPD_FormStructureBlock> blocks) {
        String recordTypeName = EPD_Constants.MEASUREMENT_TYPE_TO_CYLINDER_RECORD_TYPE_NAME.get(
                engineConfig.Measurement_Type__c
        );

        Id cylinderRecordType = Schema.SObjectType.EPD_Cylinder_Information__c.getRecordTypeInfosByName()
                .get(recordTypeName).getRecordTypeId();

        for(EPD_FormStructureBlock block : blocks) {

            block.cylinders = getCylindersForBlock(engineConfig, cylinderRecordType, blocks.size());

        }
    }

    /**
     * Builds list of Cylinder Structures based on config.
     *
     * @param config
     * @param cylinderRecordType
     * @param numOfBlocks
     *
     * @return List<EPD_FormStructureCylinder>
     */
    private List<EPD_FormStructureCylinder> getCylindersForBlock(EPD_Engine__mdt config, Id cylinderRecordType, Integer numOfBlocks) {
        List<EPD_FormStructureCylinder> cylinders = new List<EPD_FormStructureCylinder>();

        for(Integer i = 1; i <= config.Number_of_Cylinders__c / numOfBlocks; i++) {

            cylinders.add(
                    (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                            new EPD_Cylinder_Information__c(
                                    Cylinder_Number__c = i,
                                    RecordTypeId = cylinderRecordType
                            )
                    )
            );

        }

        return cylinders;
    }

    /**
     * Prepare EOC Structures for engine blocks.
     * Based on engine config it will create one or two EOC records (VShaped and EOC Fuel Pressure per Block) with
     * corresponding EOC Block Name and record type.
     *
     * @return List<EPD_FormStructureEOC>
     */
    private List<EPD_FormStructureEOC> prepareEOCRecords() {
        List<EPD_FormStructureEOC> EOCRecords = new List<EPD_FormStructureEOC>();

        String recordTypeName = EPD_Constants.ADVANCED_EOC_FLAG_TO_RECORD_TYPE.get(
                engineConfig.Advanced_EOC__c
        );
        Id EOCRecordTypeId = Schema.SObjectType.EPD_Engine_Operating_Condition__c.getRecordTypeInfosByName()
                .get(recordTypeName).getRecordTypeId();

        if (isVShapedEngine() && engineConfig.EOC_Fuel_Pressure_Per_Block__c) {
            EOCRecords.add(createEOCRecord(EPD_Constants.EOC_BLOCK_NAME_LEFT, EOCRecordTypeId));
            EOCRecords.add(createEOCRecord(EPD_Constants.EOC_BLOCK_NAME_RIGHT, EOCRecordTypeId));
        } else {
            EOCRecords.add(createEOCRecord(EPD_Constants.EOC_BLOCK_NAME_EMPTY, EOCRecordTypeId));
        }

        return EOCRecords;
    }

    /**
     * Creates EOC Structure record based on provided params.
     *
     * @param blockName
     * @param recordTypeId
     *
     * @return EPD_FormStructureEOC
     */
    private EPD_FormStructureEOC createEOCRecord(String blockName, Id recordTypeId) {
        return ((EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(
                new EPD_Engine_Operating_Condition__c(
                        Block_Name__c = blockName,
                        RecordTypeId = recordTypeId
                )
        )).setFlag(engineConfig);
    }

    /**
     * Checks if engine is straight / inline
     *
     * @return Boolean
     */
    private Boolean isInlineEngine() {
        return EPD_Constants.ENGINE_CONFIG_STRAIGHT.equals(engineConfig.Engine_Configuration__c);
    }

    /**
     * Checks if engine is V-Shaped
     *
     * @return Boolean
     */
    private Boolean isVShapedEngine() {
        return EPD_Constants.ENGINE_CONFIG_V_SHAPED.equals(engineConfig.Engine_Configuration__c);
    }

}