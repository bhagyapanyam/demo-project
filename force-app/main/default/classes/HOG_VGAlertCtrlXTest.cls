/*************************************************************************************************\
Author:         Miro Zelina
Company:        Husky Energy
Description:    Controller Extension Unit Test for HOG_VGAlertCtrlX
History:        mz 04.27.2018 - Created.
**************************************************************************************************/
@isTest
private class HOG_VGAlertCtrlXTest {

    @isTest
	private static void checkOpenAndClosedRelatedLists(){
	    
		List<HOG_Vent_Gas_Alert_Task__c> vgOpenTasks = [SELECT Id, Is_Closed__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Is_Closed__c = false];
		List<HOG_Vent_Gas_Alert_Task__c> vgClosedTasks = [SELECT Id, Is_Closed__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Is_Closed__c = true];
		HOG_Vent_Gas_Alert__c vgAlert = [SELECT Id FROM HOG_Vent_Gas_Alert__c].get(0);
		ApexPages.StandardController stdVGAlert = new ApexPages.StandardController(vgAlert);
		
		PageReference pageRef = Page.HOG_VGAlertView;
        Test.setCurrentPage(pageRef);     
		
        HOG_VGAlertCtrlX controller = new HOG_VGAlertCtrlX(stdVGAlert);
        
        System.assertEquals(2, vgOpenTasks.Size());
        System.assertEquals(2, vgClosedTasks.Size());
        System.assertEquals(vgOpenTasks.Size(), controller.getVGAOpenTasks().Size());
        System.assertEquals(vgClosedTasks.Size(), controller.getVGAClosedTasks().Size());
        
	}
	
	@testSetup 
    static void createTestData() {
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User usr = HOG_VentGas_TestData.createUser();
		Operator_On_Call__c operatorOnCall = HOG_VentGas_TestData.createOperatorOnCall(loc.Route__c, usr.Id);
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST,'TestDescription', 
																		loc.Id,
																		usr.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_Vent_Gas_Alert_Task__c task1 = HOG_VentGas_TestData.createTask(alert.Id, 
																	       usr.Id, 
																	       null, 
																	       'NotStartedTask', 
																	       alert.Priority__c, 
																	       true, 
																	       HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	       HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		
	    HOG_Vent_Gas_Alert_Task__c task2 = HOG_VentGas_TestData.createTask(alert.Id, 
																	       usr.Id, 
																	       null, 
																	       'InProgressTask', 
																	       alert.Priority__c, 
																	       true, 
																	       HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	       HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);	
		task2.Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;
		update task2;															       
																	
	    HOG_Vent_Gas_Alert_Task__c task3 = HOG_VentGas_TestData.createTask(alert.Id, 
																	       usr.Id, 
																	       null, 
																	       'ComplettedTask', 
																	       alert.Priority__c, 
																	       true, 
																	       HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	       HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);	
		task3.Status__c = HOG_VentGas_Utilities.TASK_STATUS_COMPLETED;
		update task3;																       
																	       

	    HOG_Vent_Gas_Alert_Task__c task4 = HOG_VentGas_TestData.createTask(alert.Id, 
																	       usr.Id, 
																	       null, 
																	       'CancelledTask', 
																	       alert.Priority__c, 
																	       true, 
																	       HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	       HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);	
																	       
	    task4.Status__c = HOG_VentGas_Utilities.TASK_STATUS_CANCELLED;
		update task4;																       
	}
}