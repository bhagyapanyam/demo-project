/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure for Engine Performance Data SObject
Test Class:     EPD_FormStructureEPDTest
History:        jschn 2019-06-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureEPD extends EPD_FormStructureBase {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureEPD.class);

    @AuraEnabled
    public Boolean hasEditAccess;

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id companyId;

    @AuraEnabled
    public Id dataCollectedById;

    @AuraEnabled
    public Id equipmentEngineId;

    @AuraEnabled
    public Id workOrderId;

    @AuraEnabled
    public Id workOrderActivityId;

    @AuraEnabled
    public String vendorWorkOrderNumber {get;set;}

    @AuraEnabled
    public String engineManufacturer {get;set;}

    @AuraEnabled
    public String engineModel {get;set;}

    @AuraEnabled
    public String engineSerialNumber {get;set;}

    @AuraEnabled
    public String workOrderNumber {get;set;}

    @AuraEnabled
    public String operationGroup {get;set;}

    @AuraEnabled
    public String plannerGroup {get;set;}

    @AuraEnabled
    public String LSD {get;set;}

    @AuraEnabled
    public String equipmentTag {get;set;}

    @AuraEnabled
    public Date dataCollected {get;set;}

    @AuraEnabled
    public String userName {get;set;}

    @AuraEnabled
    public String companyName {get;set;}

    @AuraEnabled
    public Decimal hours;
    //TODO remove after LTNG migration
    public String hoursClassic {
        get {
            return String.valueOf(hours);
        }
        set {
            hoursClassic = value;
            if(String.isNotBlank(value)) {
                hours = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal COCOilPressure;
    //TODO remove after LTNG migration
    public String COCOilPressureClassic {
        get {
            return String.valueOf(COCOilPressure);
        }
        set {
            COCOilPressureClassic = value;
            if(String.isNotBlank(value)) {
                COCOilPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal COCOilTemperature;
    //TODO remove after LTNG migration
    public String COCOilTemperatureClassic {
        get {
            return String.valueOf(COCOilTemperature);
        }
        set {
            COCOilTemperatureClassic = value;
            if(String.isNotBlank(value)) {
                COCOilTemperature = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EIIgnitionTiming;
    //TODO remove after LTNG migration
    public String EIIgnitionTimingClassic {
        get {
            return String.valueOf(EIIgnitionTiming);
        }
        set {
            EIIgnitionTimingClassic = value;
            if(String.isNotBlank(value)) {
                EIIgnitionTiming = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EIRPM;
    //TODO remove after LTNG migration
    public String EIRPMClassic {
        get {
            return String.valueOf(EIRPM);
        }
        set {
            EIRPMClassic = value;
            if(String.isNotBlank(value)) {
                EIRPM = Decimal.valueOf(value);
            }
        }
    }
    public Boolean EIRPM_Error {get;set;}

    @AuraEnabled
    public Decimal EJWTemperatureIN;
    //TODO remove after LTNG migration
    public String EJWTemperatureINClassic {
        get {
            return String.valueOf(EJWTemperatureIN);
        }
        set {
            EJWTemperatureINClassic = value;
            if(String.isNotBlank(value)) {
                EJWTemperatureIN = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EJWTemperatureOUT;
    //TODO remove after LTNG migration
    public String EJWTemperatureOUTClassic {
        get {
            return String.valueOf(EJWTemperatureOUT);
        }
        set {
            EJWTemperatureOUTClassic = value;
            if(String.isNotBlank(value)) {
                EJWTemperatureOUT = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EOCCrankcasePressure;
    //TODO remove after LTNG migration
    public String EOCCrankcasePressureClassic {
        get {
            return String.valueOf(EOCCrankcasePressure);
        }
        set {
            EOCCrankcasePressureClassic = value;
            if(String.isNotBlank(value)) {
                EOCCrankcasePressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EOCOilPressure;
    //TODO remove after LTNG migration
    public String EOCOilPressureClassic {
        get {
            return String.valueOf(EOCOilPressure);
        }
        set {
            EOCOilPressureClassic = value;
            if(String.isNotBlank(value)) {
                EOCOilPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal EOCOilTemperature;
    //TODO remove after LTNG migration
    public String EOCOilTemperatureClassic {
        get {
            return String.valueOf(EOCOilTemperature);
        }
        set {
            EOCOilTemperatureClassic = value;
            if(String.isNotBlank(value)) {
                EOCOilTemperature = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public String notes {get;set;}

    @AuraEnabled
    public Boolean submitted {get;set;}

    @AuraEnabled
    public Boolean requiredExhaustMeasurements {get;set;}

    @AuraEnabled
    public Id previousEPD {get;set;}

    /**
     * Maps local variables into Engine Performance Data record.
     *
     * @return SObject (EPD_Engine_Performance_Data__c)
     */
    public override SObject getRecord() {
        System.debug(CLASS_NAME + ' -> getRecord. EPD Structure: ' + JSON.serialize(this));
        return new EPD_Engine_Performance_Data__c(
                Id = recordId,
                Company__c = companyId,
                Data_Collected_By__c = dataCollectedById,
                Equipment_Engine__c = equipmentEngineId,
                Work_Order__c = workOrderId,
                Work_Order_Activity__c = workOrderActivityId,
                Vendor_Work_Order_Number__c = vendorWorkOrderNumber,
                Operation_Group__c = operationGroup,
                Planner_Group__c = plannerGroup,
                LSD__c = LSD,
                Data_Collected__c = dataCollected,
                Hours__c = hours,
                COC_Oil_Pressure__c = COCOilPressure,
                COC_Oil_Temperature__c = COCOilTemperature,
                EI_Ignition_Timing__c = EIIgnitionTiming,
                EI_RPM__c = EIRPM,
                Previous_EPD__c = previousEPD,
                EJW_Temperature_IN__c = EJWTemperatureIN,
                EJW_Temperature_OUT__c = EJWTemperatureOUT,
                EOC_Crankcase_Pressure__c = EOCCrankcasePressure,
                EOC_Oil_Pressure__c = EOCOilPressure,
                EOC_Oil_Temperature__c = EOCOilTemperature,
                Notes__c = notes,
                Submitted__c = submitted,
                Required_Exhaust_Measurements__c = requiredExhaustMeasurements
        );
    }

    /**
     * Maps Engine Performance Data record into local variables.
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Object param: ' + JSON.serialize(obj));

        EIRPM_Error = false;
        hasEditAccess = false;

        EPD_Engine_Performance_Data__c epdRecord = (EPD_Engine_Performance_Data__c) obj;
        recordId = epdRecord.Id;
        companyId = epdRecord.Company__c;
        dataCollectedById = epdRecord.Data_Collected_By__c;
        equipmentEngineId = epdRecord.Equipment_Engine__c;
        workOrderId = epdRecord.Work_Order__c;
        workOrderActivityId = epdRecord.Work_Order_Activity__c;
        vendorWorkOrderNumber = epdRecord.Vendor_Work_Order_Number__c;
        workOrderNumber = epdRecord.Work_Order__r.Work_Order_Number__c;
        operationGroup = epdRecord.Operation_Group__c;
        plannerGroup = epdRecord.Planner_Group__c;
        LSD = epdRecord.LSD__c;
        engineManufacturer = epdRecord.Equipment_Engine__r.Manufacturer__c;
        engineModel = epdRecord.Equipment_Engine__r.Model__c;
        engineSerialNumber = epdRecord.Equipment_Engine__r.Serial_Number__c;
        equipmentTag = epdRecord.Equipment_Engine__r.Tag__c;
        dataCollected = epdRecord.Data_Collected__c;
        userName = epdRecord.Data_Collected_By__r.FirstName + ' ' + epdRecord.Data_Collected_By__r.LastName;
        companyName = epdRecord.Company__r.Name;
        hours = epdRecord.Hours__c;
        previousEPD = epdRecord.Previous_EPD__c;
        COCOilPressure = epdRecord.COC_Oil_Pressure__c;
        COCOilTemperature = epdRecord.COC_Oil_Temperature__c;
        EIIgnitionTiming = epdRecord.EI_Ignition_Timing__c;
        EIRPM = epdRecord.EI_RPM__c;
        EJWTemperatureIN = epdRecord.EJW_Temperature_IN__c;
        EJWTemperatureOUT = epdRecord.EJW_Temperature_OUT__c;
        EOCCrankcasePressure = epdRecord.EOC_Crankcase_Pressure__c;
        EOCOilPressure = epdRecord.EOC_Oil_Pressure__c;
        EOCOilTemperature = epdRecord.EOC_Oil_Temperature__c;
        notes = epdRecord.Notes__c;
        submitted = epdRecord.Submitted__c;
        requiredExhaustMeasurements = epdRecord.Required_Exhaust_Measurements__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. EPD Structure: ' + JSON.serialize(this));
        return this;
    }

    /**
     * Checks required values against thresholds and if thresholds are reached it will build string with warning.
     * Currently used only for Threshold Alert email.
     *
     * @param thresholds
     *
     * @return String
     */
    public String getThresholdWarnings(EPD_FormStructureThresholds thresholds){
        String epdThresholdWarnings = '';

        if(hasReachedThreshold(thresholds)) {
            epdThresholdWarnings += getRPMThresholdWarning();
        }

        return epdThresholdWarnings;
    }

    /**
     * Builds Threshold warning string for Engine RPM.
     *
     * @return String
     */
    private String getRPMThresholdWarning() {
        return String.format(
                EPD_Constants.THRESHOLD_EMAIL_WARNING_BASE,
                new List<String> {
                        'Engine',
                        'RPM',
                        String.valueOf(EIRPM)
                }
        );
    }

    /**
     * Checks if EPD measurements reached engine thresholds.
     *
     * @return Boolean
     */
    public Boolean hasReachedThreshold(EPD_FormStructureThresholds thresholds) {
        return EPD_Utility.isOutsideOfRange(EIRPM, thresholds.minRPM, thresholds.maxRPM);
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"hasEditAccess":' + hasEditAccess + ',';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"'+recordId+'"'):'null') + ',';
        contentBuildingString += '"companyId":' + (String.isNotBlank(companyId) ? ('"'+companyId+'"'):'null') + ',';
        contentBuildingString += '"dataCollectedById":' + (String.isNotBlank(dataCollectedById) ? ('"'+dataCollectedById+'"'):'null') + ',';
        contentBuildingString += '"equipmentEngineId":' + (String.isNotBlank(equipmentEngineId) ? ('"'+equipmentEngineId+'"'):'null') + ',';
        contentBuildingString += '"workOrderId":' + (String.isNotBlank(workOrderId) ? ('"'+workOrderId+'"'):'null') + ',';
        contentBuildingString += '"workOrderActivityId":' + (String.isNotBlank(workOrderActivityId) ? ('"'+workOrderActivityId+'"'):'null') + ',';
        contentBuildingString += '"vendorWorkOrderNumber":' + (String.isNotBlank(vendorWorkOrderNumber) ? ('"'+vendorWorkOrderNumber+'"'):'null') + ',';
        contentBuildingString += '"engineManufacturer":' + (String.isNotBlank(engineManufacturer) ? ('"'+engineManufacturer+'"'):'null') + ',';
        contentBuildingString += '"engineModel":' + (String.isNotBlank(engineModel) ? ('"'+engineModel+'"'):'null') + ',';
        contentBuildingString += '"engineSerialNumber":' + (String.isNotBlank(engineSerialNumber) ? ('"'+engineSerialNumber+'"'):'null') + ',';
        contentBuildingString += '"workOrderNumber":' + (String.isNotBlank(workOrderNumber) ? ('"'+workOrderNumber+'"'):'null') + ',';
        contentBuildingString += '"operationGroup":' + (String.isNotBlank(operationGroup) ? ('"'+operationGroup+'"'):'null') + ',';
        contentBuildingString += '"plannerGroup":' + (String.isNotBlank(plannerGroup) ? ('"'+plannerGroup+'"'):'null') + ',';
        contentBuildingString += '"LSD":' + (String.isNotBlank(LSD) ? ('"'+LSD+'"'):'null') + ',';
        contentBuildingString += '"equipmentTag":' + (String.isNotBlank(equipmentTag) ? ('"'+equipmentTag+'"'):'null') + ',';
        contentBuildingString += '"dataCollected":' + (dataCollected != null ? '"' + dataCollected.year()+'-'+dataCollected.month()+'-'+dataCollected.day() + '"' : null) + ',';
        contentBuildingString += '"userName":' + (String.isNotBlank(userName) ? ('"'+userName+'"'):'null') + ',';
        contentBuildingString += '"companyName":' + (String.isNotBlank(companyName) ? ('"'+companyName+'"'):'null') + ',';
        contentBuildingString += '"hours":' + hours + ',';
        contentBuildingString += '"COCOilPressure":' + COCOilPressure + ',';
        contentBuildingString += '"COCOilTemperature":' + COCOilTemperature + ',';
        contentBuildingString += '"EIIgnitionTiming":' + EIIgnitionTiming + ',';
        contentBuildingString += '"EIRPM":' + EIRPM + ',';
        contentBuildingString += '"EJWTemperatureIN":' + EJWTemperatureIN + ',';
        contentBuildingString += '"EJWTemperatureOUT":' + EJWTemperatureOUT + ',';
        contentBuildingString += '"EOCCrankcasePressure":' + EOCCrankcasePressure + ',';
        contentBuildingString += '"EOCOilPressure":' + EOCOilPressure + ',';
        contentBuildingString += '"EOCOilTemperature":' + EOCOilTemperature + ',';
        contentBuildingString += '"notes":' + (String.isNotBlank(notes) ? ('"'+notes+'"'):'null') + ',';
        contentBuildingString += '"submitted":' + submitted + ',';
        contentBuildingString += '"requiredExhaustMeasurements":' + requiredExhaustMeasurements + ',';
        contentBuildingString += '"previousEPD":' + (String.isNotBlank(previousEPD) ? ('"'+previousEPD+'"'):'null');
        return '"epd":{' + contentBuildingString + '}';
    }

}