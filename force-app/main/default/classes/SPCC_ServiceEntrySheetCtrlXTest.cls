@isTest
private class SPCC_ServiceEntrySheetCtrlXTest {
	
	@testSetup static void setup() {
		SPCC_Utilities.executeTriggerCode = false;

		SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
		insert ewr;

		SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K2', 'A1-F7GH56-4D-J7-K2', 'Test AFE', ewr.Id);
		insert afe;

		SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, 'Test Category', 
													 'Test Description', 
													 '6000000',
													 'Test Cose Element',
													 5000,
													 0);
		insert costElement;

		Account vendorAccount = SPCC_TestData.createSPCCVendor('Test Account');
		insert vendorAccount;

		SPCC_Vendor_Account_Assignment__c vaa = SPCC_TestData.createVAA(ewr.Id, vendorAccount.Id);

		List<SPCC_Purchase_Order__c> purchaseOrders = new List<SPCC_Purchase_Order__c>();
		for(Integer i=0; i<10; i++) {
			purchaseOrders.add(SPCC_TestData.createPO('012345678' + i,
												  	ewr.Id,
												  	afe.Id,
												  	vendorAccount.Id,
												  	'Test Purchase Order',
												  	null));
		}
		insert purchaseOrders;

		List<SPCC_Line_Item__c> lineItems = new List<SPCC_Line_Item__c>();
		for(SPCC_Purchase_Order__c po : purchaseOrders) {
			lineItems.add(SPCC_TestData.createLineItem(po.Id, costElement.Id, 100, '0010', 'Test Line Item for ' + po.Name));
		}
		insert lineItems;
	}


	@isTest static void testController() {
		SPCC_Utilities.executeTriggerCode = false;

		//Set Page
		PageReference pref = Page.SPCC_ServiceEntrySheetCreate;
		pref.getParameters().put('retUrl', Page.SPCC_ServiceEntrySheetList.getUrl());
		Test.setCurrentPage(pref);

		//Create Controller
		SPCC_Service_Entry_Sheet__c serviceEntrySheet = SPCC_TestData.createSES(null, null, 800, Date.today(),'12345');
		ApexPages.StandardController stdController = new ApexPages.StandardController(serviceEntrySheet);
		SPCC_ServiceEntrySheetCtrlX extCtrl = new SPCC_ServiceEntrySheetCtrlX(stdController);

		//Query needed setup data
		List<SPCC_Purchase_Order__c> purchaseOrders = [Select Id, Name, Purchase_Order_Number__c
													   From SPCC_Purchase_Order__c];
		Map<Id, SPCC_Line_Item__c> poToLineItemMap = new Map<Id, SPCC_Line_Item__c>();//Since we set up the test data to have only one Line Item per PO, this is okay
		for(SPCC_Line_Item__c lineItem : [Select Id, Name, Purchase_Order__c
										  From SPCC_Line_Item__c]) {
			poToLineItemMap.put(lineItem.Purchase_Order__c, lineItem);
		}

		Test.startTest();
			//Test PO Autopopulate functionality
			List<SPCC_Purchase_Order__c> posMatched = SPCC_ServiceEntrySheetCtrlX.searchPurchaseOrder('012345678');
			System.debug('posMatched: ' + posMatched + ' purchaseOrders: ' + purchaseOrders);
			System.assertNotEquals(posMatched, null);
			System.assertEquals(posMatched.size(), purchaseOrders.size());

			posMatched = SPCC_ServiceEntrySheetCtrlX.searchPurchaseOrder('0123456789');
			System.assertEquals(posMatched.size(), 1);

			posMatched = SPCC_ServiceEntrySheetCtrlX.searchPurchaseOrder('test');
			System.assert(posMatched == null || posMatched.size() == 0);

			//Test Refresh Purchase Order
			extCtrl.purchaseOrderId = purchaseOrders[0].Id;
			extCtrl.serviceEntrySheet.Line_Item__c = extCtrl.purchaseOrderLineItemOptions[0].getValue();
			extCtrl.refreshPurchaseOrder();
			System.assertEquals(extCtrl.purchaseOrder.Id, purchaseOrders[0].Id);

			//Test Submit 
			PageReference submitPref = extCtrl.submit();
			System.assertEquals(Page.SPCC_ServiceEntrySheetList.getUrl(), submitPref.getUrl());
			SPCC_Service_Entry_Sheet__c submittedSES = [Select Id, Name, Amount__c, Date_Entered__c, Ticket_Number__c
														From SPCC_Service_Entry_Sheet__c
														Where Purchase_Order__c =: purchaseOrders[0].Id];
			System.assertNotEquals(submittedSES, null);
			System.assertEquals(submittedSES.Amount__c, serviceEntrySheet.Amount__c);
			System.assertEquals(submittedSES.Date_Entered__c, serviceEntrySheet.Date_Entered__c);

			//Test Cancel
			PageReference cancelPref = extCtrl.cancel();
			System.assertEquals(Page.SPCC_ServiceEntrySheetList.getUrl(), cancelPref.getUrl());
		Test.stopTest();
	}
}