/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Error message prettify service that extracts actual message from Exception messages.
                Currently there are only 3 supported exception type messages.
Test Class:     HOG_ErrorPrettifyServiceTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class HOG_ErrorPrettifyService {

    private static final String CLASS_NAME = String.valueOf(HOG_ErrorPrettifyService.class);

    @TestVisible
    private static final String CUSTOM_VALIDATION_ERROR = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, ';
    @TestVisible
    private static final String FIELD_MISSING = 'REQUIRED_FIELD_MISSING, ';
    @TestVisible
    private static final String SOBJECT_EXCEPTION = 'System.SObjectException: ';

    /**
     * Takes standard error message (as String) and removes part which User shouldn't see.
     *
     * @param errorMsg
     *
     * @return
     */
    public static String prettifyErrorMessage(String errorMsg) {
        return prettifyErrorMessage(errorMsg, Label.HOG_Generic_Error_Message);
    }

    /**
     * Takes standard error message (as String) and removes part which User shouldn't see.
     *
     * @param errorMsg
     * @param genericMessage
     *
     * @return
     */
    public static String prettifyErrorMessage(String errorMsg, String genericMessage) {
        if(String.isNotBlank(errorMsg)) {

            System.debug(CLASS_NAME + ' -> prettifyErrorMessage. Original message: ' + errorMsg);

            if (errorMsg.contains(CUSTOM_VALIDATION_ERROR)) {
                List<String> splitMessage = errorMsg.split(CUSTOM_VALIDATION_ERROR);
                errorMsg = splitMessage.get(1);
            }
            if (errorMsg.contains(FIELD_MISSING)) {
                List<String> splitMessage = errorMsg.split(FIELD_MISSING);
                errorMsg = splitMessage.get(1);
            }
            if (errorMsg.contains(SOBJECT_EXCEPTION)) {
                List<String> splitMessage = errorMsg.split(SOBJECT_EXCEPTION);
                errorMsg = splitMessage.get(1);
            }

            errorMsg = String.format(
                    genericMessage,
                    new List<String> {errorMsg}
            );

        }
        return errorMsg;
    }

}