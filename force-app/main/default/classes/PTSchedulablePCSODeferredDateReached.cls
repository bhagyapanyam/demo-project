global class PTSchedulablePCSODeferredDateReached implements Schedulable{   
    global void execute(SchedulableContext ctx){
        List <Messaging.SingleEmailMessage> MailList = new List<Messaging.SingleEmailMessage>();
        set<PCSO__c> PCSOList = new set<PCSO__c>([select id,name,site_id__c,lsd__c,site_name__c,amu__C,deferred_date__c,owner.email from PCSO__c where PCSO_Status__c ='Deferred' and System_Review__c='Valid' and Deferred_date__c<=today]);
        String salesforceURL = system.URL.getSalesforceBaseUrl().toExternalForm() ;
        
        if (!PCSOList.isempty()){
            for(PCSO__c pcso: PCSOList){
                Messaging.SingleEmailMessage mail = new messaging.SingleEmailMessage();
                String[] toAddress = new String[]{pcso.Owner.email};
                mail.setSubject('IMPORTANT:'+pcso.name+' has reached its deferred date');
                string emailMessage = pcso.name+' has reached its deferred date '+pcso.deferred_date__c+
                '.\r\n\r\nIf it still needs to be in deferred status, change the deferred date. If no action, weekly reminder will be sent.'+
                '\r\n\r\nPCSO Information:\r\n\r\nSite Id: '+pcso.site_id__c+
                '\r\nLSD: '+pcso.LSD__c+
                '\r\nSite Name: '+pcso.Site_Name__c+
                '\r\nAMU : '+pcso.AMU__c+
                '\r\n\r\n You can access the record by clicking the following link:'+
                '\r\n\r\n'+salesforceURL+'/'+pcso.id;
                mail.setPlainTextBody(emailMessage);
                mail.setToAddresses(toAddress);
                mail.setReplyTo('Electrical.Utilities@huskyenergy.com');
                MailList.add(mail);
            }

            if(!Test.isRunningTest()) {
                Messaging.sendEmail(MailList);
            }
            
        }
    }
}