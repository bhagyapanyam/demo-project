/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormPersistenceHelperTest
History:        jschn 2019-07-25 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormPersistenceHelperTest {

    @IsTest
    static void cleanIDsFromFormStructure_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag= false;
        EPD_FormStructure formStructure;

        try {
            new EPD_FormPersistenceHelper().cleanIDsFromFormStructure(formStructure);
        } catch(Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void cleanIDsFromFormStructure_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag= false;
        EPD_FormStructure formStructure = new EPD_FormStructure();

        try {
            new EPD_FormPersistenceHelper().cleanIDsFromFormStructure(formStructure);
        } catch(Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void cleanIDsFromFormStructure_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag= false;
        Id expectedId = null;
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure(prepareEPD())
                .setEOCs(prepareEOCs())
                .setParts(prepareParts())
                .setCOCStages(prepareCOCStages())
                .setEngineBlocks(prepareBlocks());

        try {
            new EPD_FormPersistenceHelper().cleanIDsFromFormStructure(formStructure);
        } catch(Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, formStructure);
        System.assertEquals(expectedId, formStructure.epd.recordId);
        for(EPD_FormStructureEOC eoc : formStructure.EOCs) {
            System.assertEquals(expectedId, eoc.recordId);
            System.assertEquals(expectedId, eoc.epdId);
        }
        for(EPD_FormStructurePart part : formStructure.parts) {
            System.assertEquals(expectedId, part.recordId);
            System.assertEquals(expectedId, part.epdId);
        }
        for(EPD_FormStructureCOCStage cocStage : formStructure.cocStages) {
            System.assertEquals(expectedId, cocStage.recordId);
            System.assertEquals(expectedId, cocStage.enginePerformanceDataId);
        }
        for(EPD_FormStructureBlock block : formStructure.engineBlocks) {
            System.assertEquals(expectedId, block.recordId);
            System.assertEquals(expectedId, block.epdId);
            for(EPD_FormStructureCylinder cylinder : block.cylinders) {
                System.assertEquals(expectedId, cylinder.recordId);
                System.assertEquals(expectedId, cylinder.blockInformationId);
            }
        }
    }

    private static EPD_FormStructureEPD prepareEPD() {
        EPD_FormStructureEPD structure = (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Engine_Performance_Data__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        return structure;
    }

    private static List<EPD_FormStructureEOC> prepareEOCs() {
        List<EPD_FormStructureEOC> structures = new List<EPD_FormStructureEOC>();
        EPD_FormStructureEOC structure = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Engine_Operating_Condition__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        structure.epdId = UserInfo.getUserId();
        structures.add(structure);
        return structures;
    }

    private static List<EPD_FormStructurePart> prepareParts() {
        List<EPD_FormStructurePart> structures = new List<EPD_FormStructurePart>();
        EPD_FormStructurePart structure = (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Part_Replacement__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        structure.epdId = UserInfo.getUserId();
        structures.add(structure);
        return structures;
    }

    private static List<EPD_FormStructureCOCStage> prepareCOCStages() {
        List<EPD_FormStructureCOCStage> structures = new List<EPD_FormStructureCOCStage>();
        EPD_FormStructureCOCStage structure = (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Compressor_Operating_Condition_Stage__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        structure.enginePerformanceDataId = UserInfo.getUserId();
        structures.add(structure);
        return structures;
    }

    private static List<EPD_FormStructureBlock> prepareBlocks() {
        List<EPD_FormStructureBlock> structures = new List<EPD_FormStructureBlock>();
        EPD_FormStructureBlock structure = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        structure.epdId = UserInfo.getUserId();
        structure.cylinders = prepareCylinders();
        structures.add(structure);
        return structures;
    }

    private static List<EPD_FormStructureCylinder> prepareCylinders() {
        List<EPD_FormStructureCylinder> structures = new List<EPD_FormStructureCylinder>();
        EPD_FormStructureCylinder structure = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        structure.recordId = UserInfo.getUserId();
        structure.blockInformationId = UserInfo.getUserId();
        structures.add(structure);
        return structures;
    }

}