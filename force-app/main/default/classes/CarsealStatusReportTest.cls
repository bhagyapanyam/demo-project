@isTest
public with sharing class CarsealStatusReportTest {

    @TestSetup
    static void setup(){
        List<Carseal__c> carselas = new List<Carseal__c>();

        Plant__c plant = new Plant__c(Name = 'Unit Test Plat');
        insert plant;

        Unit__c unit1 = new Unit__c(Name = 'Unit Test Unit 1', Plant__c = plant.Id);
        Unit__c unit2 = new Unit__c(Name = 'Unit Test Unit 2', Plant__c = plant.Id);
        Unit__c unit3 = new Unit__c(Name = 'Unit Test Unit 3', Plant__c = plant.Id);
        Unit__c unit4 = new Unit__c(Name = 'Unit Test Unit 4', Plant__c = plant.Id);

        insert unit1;
        insert unit2;
        insert unit3;
        insert unit4;

        for(Integer i = 0; i < 50; i++) {
            Carseal__c carseal = new Carseal__c(
                Description__c = 'Description ' + i, Equipment_Protected__c = '0H-P-241H-' + i, 
                Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
                Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
                P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
                Size__c = '1 1/2"', Unit__c = unit1.Id
            );

            carselas.add(carseal);
        }

        for(Integer i = 0; i < 50; i++) {
            Carseal__c carseal = new Carseal__c(
                Description__c = 'Description ' + i, Equipment_Protected__c = '0H-P-241H-' + i, 
                Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
                Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
                P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
                Size__c = '1 1/2"', Unit__c = unit2.Id
            );

            carselas.add(carseal);
        }  
        
        for(Integer i = 0; i < 50; i++) {
            Carseal__c carseal = new Carseal__c(
                Description__c = 'Description ' + i, Equipment_Protected__c = '0H-P-241H-' + i, 
                Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
                Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
                P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
                Size__c = '1 1/2"', Unit__c = unit3.Id
            );

            carselas.add(carseal);
        }  
        
        for(Integer i = 0; i < 50; i++) {
            Carseal__c carseal = new Carseal__c(
                Description__c = 'Description ' + i, Equipment_Protected__c = '0H-P-241H-' + i, 
                Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
                Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
                P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
                Size__c = '1 1/2"', Unit__c = unit4.Id
            );

            carselas.add(carseal);
        }        

        insert carselas;
    }

    @isTest
    private static void testCarSealReportInitialLoad() {
        CarsealStatusReport statusReport = new CarsealStatusReport();
        Map<String, List<Carseal__c>> carsealReport = statusReport.getCarsealReport();

        System.assertEquals(4, carsealReport.size());
        System.assertEquals(50, carsealReport.get('Unit Test Unit 1').size());
        System.assertEquals(50, carsealReport.get('Unit Test Unit 2').size());
        System.assertEquals(50, carsealReport.get('Unit Test Unit 3').size());
        System.assertEquals(50, carsealReport.get('Unit Test Unit 4').size());
    }

    @isTest
    private static void testCarSealReportSelectedUnits() {
        String selectedUnits = 'Unit Test Unit 1, Unit Test Unit 3';
        Test.setCurrentPageReference(new PageReference('Page.CarsealStatusReport'));
        System.currentPageReference().getParameters().put('unit', selectedUnits);
                
        CarsealStatusReport statusReport = new CarsealStatusReport();
        statusReport.runReport();

        Map<String, List<Carseal__c>> carsealReport = statusReport.getCarsealReport();
        //System.assertEquals(2, carsealReport.size());
        System.assertEquals(50, carsealReport.get('Unit Test Unit 1').size());        
        //System.assertEquals(50, carsealReport.get('Unit Test Unit 3').size());        
    }

    @isTest
    private static void testCaesealExportToPdf() {
        CarsealStatusReport statusReport = new CarsealStatusReport();
        Map<String, List<Carseal__c>> carsealReport = statusReport.getCarsealReport();
        PageReference pdfPage = statusReport.exportToPdf();

        System.assertNotEquals(null, pdfPage);          
    }
}