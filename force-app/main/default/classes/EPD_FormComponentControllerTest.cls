/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormComponentControllerTest
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormComponentControllerTest {

    @IsTest
    static void EPD_FormComponentController_init() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller;

        Test.startTest();
        try {
            controller = new EPD_FormComponentController();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, controller);
        System.assertEquals(false, controller.showThresholds);
    }

    @IsTest
    static void EPD_FormComponentController_openThresholds() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller;

        Test.startTest();
        try {
            controller = new EPD_FormComponentController();
            controller.openEngineThresholds();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, controller);
        System.assertEquals(true, controller.showThresholds);
    }

    @IsTest
    static void EPD_FormComponentController_closeThresholds() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller;

        Test.startTest();
        try {
            controller = new EPD_FormComponentController();
            controller.openEngineThresholds();
            controller.closeEngineThresholds();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, controller);
        System.assertEquals(false, controller.showThresholds);
    }

    @IsTest
    static void EPD_FormComponentController_loadError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller;

        Test.startTest();
        try {
            controller = new EPD_FormComponentController();
            controller.woaRecordId = null;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, controller);
        System.assertEquals(true, controller.hasInitLoadError);
        System.assert(ApexPages.hasMessages());
        System.assertEquals(1, ApexPages.getMessages().size());
    }

    @IsTest //mocked
    static void EPD_FormComponentController_success() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_DAOProvider.workOrderActivityDAO = new WOAMock();
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMock();
        EPD_FormComponentController controller;

        Test.startTest();
        try {
            controller = new EPD_FormComponentController();
            controller.woaRecordId = UserInfo.getUserId();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, controller);
        System.assertNotEquals(null, controller.formStructure);
        System.assertEquals(false, controller.hasInitLoadError);
        System.assert(!ApexPages.hasMessages());
        System.assertEquals(0, ApexPages.getMessages().size());
        System.assert(String.isNotBlank(controller.warningRPM));
        System.assert(String.isNotBlank(controller.warningIMP));
        System.assert(String.isNotBlank(controller.warningValveSeatRecession));
        System.assert(String.isNotBlank(controller.warningCompression));
    }


    public class WOAMock implements VTT_WorkOrderActivityDAO {
        public List<Work_Order_Activity__c> getRecord(Id workOrderActivityId) {
            Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName', null, null, false);
            woa.Maintenance_Work_Order__r = VTT_TestDataFactory.createWorkOrder(null, null, false);
            woa.Assigned_Vendor__r = (Account) HOG_SObjectFactory.createSObject(new Account(), false);
            return new List<Work_Order_Activity__c> {
                    woa
            };
        }
    }
    public class EquipmentEngineMock implements EquipmentEngineDAO {
        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(
                            new Equipment_Engine__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }
        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }

    @IsTest
    static void checkRPM_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_RPM__c = 100,
                Max_RPM__c = 500
        );
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningRPM = 'error';

        Test.startTest();
        try {
            controller.checkRPM();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, controller.formStructure.epd.EIRPM_Error);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void checkRPM_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_RPM__c = 1000,
                Max_RPM__c = 1500
        );
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.checkRPM();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.epd.EIRPM_Error);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkIMP_withoutLabel() {
        String impLabel = 'engineBlockInputLabelToCheck';
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 0
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.inputLabelIMP = impLabel;
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningIMP = 'Error';

        Test.startTest();
        try {
            controller.checkIMP();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, controller.intakeManifoldPressureError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkIMP_withError() {
        String impLabel = 'engineBlockInputLabelToCheck';
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 0
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.inputLabelIMP = impLabel;
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningIMP = 'Error';

        Test.startTest();
        try {
            controller.engineBlockInputLabelToCheck = impLabel;
            controller.checkIMP();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, controller.intakeManifoldPressureError);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void checkIMP_withoutError() {
        String impLabel = 'engineBlockInputLabelToCheck';
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 25
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.inputLabelIMP = impLabel;
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningIMP = 'Error';

        Test.startTest();
        try {
            controller.engineBlockInputLabelToCheck = impLabel;
            controller.checkIMP();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.intakeManifoldPressureError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkIntake_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.190,
                Valve_Seat_Warning_Limit__c = 0.190
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningValveSeatRecession = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkIntake();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkIntake_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.050,
                Valve_Seat_Warning_Limit__c = 0.050
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningValveSeatRecession = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkIntake();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void checkExhaust_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.190,
                Valve_Seat_Warning_Limit__c = 0.190
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningValveSeatRecession = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkExhaust();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkExhaust_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_Valve_Seat_Recession__c = 0.050,
                Valve_Seat_Warning_Limit__c = 0.050
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningValveSeatRecession = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkExhaust();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearExhaustError);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void checkCompression_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 260
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningCompression = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkCompression();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void checkCompression_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Cylinder_Compression__c = 250,
                Max_Cylinder_Compression__c = 260
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);

        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;
        controller.warningCompression = 'Error';

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkCompression();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, controller.formStructure.engineBlocks.get(0).cylinders.get(0).compressionError);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void getBlockByName_returnNull() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Cylinder_Compression__c = 250,
                Max_Cylinder_Compression__c = 260
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = String.valueOf(cylinder.cylinderNumber);
        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkCompression();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void getCylinderByNumber_returnNull() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Cylinder_Compression__c = 250,
                Max_Cylinder_Compression__c = 260
        );
        String blockName = 'blockName';
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.blockName = blockName;
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        String cylinderNumber = null;
        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig));
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.engineBlockNameToCheck = blockName;
            controller.cylinderNumberToCheck = cylinderNumber;
            controller.checkCompression();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.formStructure.engineBlocks.get(0).cylinders.get(0).wearIntakeError);
        System.assert(!ApexPages.hasMessages());
    }

    @IsTest
    static void saveForm_fail() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block});
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.saveForm();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(ApexPages.hasMessages());
    }

    @IsTest
    static void submitForm_fail() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        EPD_FormStructureCylinder cylinder = (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.cylinders = new List<EPD_FormStructureCylinder>{cylinder};
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block});
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.submitForm();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(ApexPages.hasMessages());
        System.assertEquals(true, controller.showForm);
    }

    @IsTest
    static void closeForm() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();
        PageReference pageRef;

        Test.startTest();
        try {
            pageRef = controller.closeForm();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(!ApexPages.hasMessages());
        System.assertNotEquals(null, pageRef);
        System.assertEquals(controller.woaRecordId, pageRef.getParameters().get('id'));
    }

}