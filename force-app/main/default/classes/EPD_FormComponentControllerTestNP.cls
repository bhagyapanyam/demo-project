/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non Parallel unit test for EPD_FormComponentController
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest
private class EPD_FormComponentControllerTestNP {

    @IsTest
    static void saveForm_success() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();

        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        block.cylinders = new List<EPD_FormStructureCylinder>{
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                )
        };

        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setParts(new List<EPD_FormStructurePart>())
                .setCOCStages(new List<EPD_FormStructureCOCStage>())
                .setEOCs(new List<EPD_FormStructureEOC>());
        controller.formStructure = formStructure;

        Test.startTest();
        try {
            controller.saveForm();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Engine_Performance_Data__c]);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Block_Information__c]);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Cylinder_Information__c]);
    }

    @IsTest
    static void submitForm_success() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormComponentController controller = new EPD_FormComponentController();

        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        block.cylinders = new List<EPD_FormStructureCylinder>{
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                )
        };

        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                .setEngineBlocks(new List<EPD_FormStructureBlock>{block})
                .setParts(new List<EPD_FormStructurePart>())
                .setCOCStages(new List<EPD_FormStructureCOCStage>())
                .setEOCs(new List<EPD_FormStructureEOC>());
        controller.formStructure = formStructure;
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();

        Test.startTest();
        try {
            controller.submitForm();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, controller.showForm);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Engine_Performance_Data__c]);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Block_Information__c]);
        System.assertEquals(1, [SELECT COUNT() FROM EPD_Cylinder_Information__c]);
    }

    public class HPNTMock implements HOG_PlannerGroupNotificationTypeDAO {
        Contact contact = (Contact) HOG_SObjectFactory.createSObject(new Contact(), true);
        public List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes) {
            return new List<HOG_Planner_Group_Notification_Type__c> {
                    new HOG_Planner_Group_Notification_Type__c(
                            HOG_Planner_Group__r = new HOG_Planner_Group__c(Name = '100'),
                            Role_Type__c = 'Tradesman Lead',
                            Work_Center__c = 'Test',
                            Contact__c = contact.Id
                    )
            };
        }
    }

}