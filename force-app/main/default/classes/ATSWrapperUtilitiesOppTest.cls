@isTest (SeeAllData = True)

public class ATSWrapperUtilitiesOppTest {
    private static Opportunity opp;
    private static OpportunityLineItem oli;
    private static ATS_Freight__c freight;
    private static ATS_Freight__c freight2;
    private static ATS_Freight__c freight3;
    private static void setup (String recordTypeName) {
        Map<String, Id> recordTypeMap = new Map<String, Id>();
        
        for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                  WHERE DeveloperName like 'CPM_Asphalt' AND 
                                  SObjectType = 'Opportunity']) {
                                      recordTypeMap.put(recType.DeveloperName, recType.Id);            
                                  }
        System.debug('recordTypeMap = ' + recordTypeMap);
        List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
        
        opp = OpportunityTestData.createOpportunity(recordTypeMap.get(recordTypeName));
        //opp.Opportunity_ATS_Product_Category__c = tender.Id;
        opp.StageName = 'Closed Won'; //<--
        opp.Confirmation_Method__c = 'PO';
        opp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
        opp.PO__c = 'PO Test';
        opp.cpm_Bid_Due_Date_Time__c = date.today();
        System.debug('opp = ' + opp);
        insert opp;
        
        System.debug('opp with record type name = '+opp);
        System.debug('opp with record type NAME = '+opp.RecordType.Name);
        
        opp.Pricebook2Id = standardPriceBook[0].Id;
        opp.priceBook2 = standardPriceBook[0];
        update opp;
        opp = [
            SELECT RecordType.Name, Name,AccountId, CloseDate, StageName, RecordTypeId, Confirmation_Method__c, Carrier_Winner_Multi__c, PO__c			
            FROM Opportunity
            WHERE Id = :opp.Id
        ];
        List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
        priceBookEntryList[0].IsActive = true;
        update priceBookEntryList[0];
        oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
        insert oli;
        
        cpm_Supplier__c sup1 = new cpm_Supplier__c();
        sup1.name = 'a9s290000008OKYAA2';
        insert sup1;
        
        cpm_Supplier__c sup2 = new cpm_Supplier__c();
        sup2.name = 'a9s290000008OJlAAM';
        insert sup2;
        
        cpm_Supplier__c sup3 = new cpm_Supplier__c();
        sup3.name = 'a9s290000008OJ5AAM';
        insert sup3;
        
        freight = new ATS_Freight__c();
        freight.cpm_Product_Type__c='Asphalt';
        freight.cpm_HuskySupplier1_ats_pricing__c = sup1.Id;
        freight.cpm_HuskySupplier2_ats_pricing__c = sup2.Id;
        freight.cpm_HuskySupplier3_ats_pricing__c = sup3.Id;
        freight.Husky_Supplier_1__c= 'Husky_Supplier_1';
        freight.Husky_Supplier_2__c= 'Husky_Supplier_2';          
        freight.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
        freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
        freight.ATS_Freight__c  =   opp.Id;
        freight.Husky_Supplier_1_Selected__c = true;
        freight.Prices_F_O_B__c = 'Origin + Freight'; 
        
        insert freight;
        
        freight2 = new ATS_Freight__c();
        freight2.cpm_Product_Type__c='Emulsion';
        freight2.cpm_HuskySupplier1_ats_pricing__c = sup1.Id;
        freight2.cpm_HuskySupplier2_ats_pricing__c = sup2.Id;
        freight2.cpm_HuskySupplier3_ats_pricing__c = sup3.Id;
        freight2.Husky_Supplier_1__c= 'Husky_Supplier_1';
        freight2.Husky_Supplier_2__c= 'Husky_Supplier_2';          
        freight2.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
        freight2.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
        freight2.ATS_Freight__c  =   opp.Id;
        freight2.Husky_Supplier_1_Selected__c = true;
        freight2.Prices_F_O_B__c = 'Origin + Freight'; 
        
        insert freight2; 
        
        freight3 = new ATS_Freight__c();
        freight3.cpm_Product_Type__c='Residual';
        freight3.cpm_HuskySupplier1_ats_pricing__c = sup1.Id;
        freight3.cpm_HuskySupplier2_ats_pricing__c = sup2.Id;
        freight3.cpm_HuskySupplier3_ats_pricing__c = sup3.Id;
        freight3.Husky_Supplier_1__c= 'Husky_Supplier_1';
        freight3.Husky_Supplier_2__c= 'Husky_Supplier_2';          
        freight3.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
        freight3.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
        freight3.ATS_Freight__c  =   opp.Id;
        freight3.Husky_Supplier_1_Selected__c = true;
        freight3.Prices_F_O_B__c = 'Origin + Freight'; 
        
        insert freight3;  
        
        
    }
   
    @isTest static void constructorTest  () {
        setup('CPM_Asphalt'); 
        Test.startTest();
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight, opp);
        
        freight.Husky_Supplier_1_Selected__c = false;
        freight.Husky_Supplier_2_Selected__c = true;
        update freight;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight, opp);
        freight.Husky_Supplier_2_Selected__c = false;
        freight.Husky_Supplier_3_Selected__c = true;
        update freight;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight, opp);
        Test.stopTest();
    }
   
    
    
    
     @isTest static void constructorTestFreight2  () {
        setup('CPM_Asphalt');
        Test.startTest(); 
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight2, opp);
        
        freight2.Husky_Supplier_1_Selected__c = false;
        freight2.Husky_Supplier_2_Selected__c = true;
        update freight2;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight2, opp);
        
        freight2.Husky_Supplier_2_Selected__c = false;
        freight2.Husky_Supplier_3_Selected__c = true;
        update freight2;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight2, opp);
        Test.stopTest();
    }
    
    @isTest static void constructorTestFreight3  () {
        setup('CPM_Asphalt');
		Test.startTest();        
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight3, opp);
        
        freight2.Husky_Supplier_1_Selected__c = false;
        freight2.Husky_Supplier_2_Selected__c = true;
        update freight3;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight3, opp);
        
        freight2.Husky_Supplier_2_Selected__c = false;
        freight2.Husky_Supplier_3_Selected__c = true;
        update freight3;      
        new ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper(oli, freight3, opp);
        Test.stopTest();
    }
    
    

   
    
    
    @isTest static void QuoteTest () {
        setup('CPM_Asphalt');
        test.startTest();
        List<Opportunity> listOpp = new List<Opportunity>();
        listOpp.add(opp);
        ATSWrapperUtilitiesOpp.ATSQuoteWrapper wrapper = new ATSWrapperUtilitiesOpp.ATSQuoteWrapper();
        wrapper.setOpportunities(listOpp, null);
        wrapper.setOpportunities(listOpp, 'Asphalt');
        
        Id accId = wrapper.accountId;
        String accName = wrapper.accountName;
        String accNumber = wrapper.accountNumber;
        String accCity = wrapper.accountAddressCity;
        String accPostal = wrapper.accountAddressPostal;
        String accState = wrapper.accountAddressState;
        String accStreet = wrapper.accountAddressStreet;
        String custName = wrapper.customerName;
        String custComm = wrapper.CustomerComments;
        Opportunity opp = wrapper.tender;
        String proFamilly = wrapper.productFamily;
        String oppMark = wrapper.opportunityMarketer;
        String FobShip = wrapper.FobShipping;
        String oppComm = wrapper.oppComments;
        String oppType = wrapper.opportunityType;
        Decimal FreiCre = wrapper.FreightCredits;
        Id conId = wrapper.contactId;
        String conName = wrapper.contactName;
        String conFax = wrapper.contactFaxNumber;
        String conComm = wrapper.contactComments;
        String conEmail = wrapper.contactEmailAddress;
        String tenHea = wrapper.tenderHeader;
        String autoNum = wrapper.autoNumber;
        Id tenId = wrapper.tenderId;
        String tenNum = wrapper.tenderNumber;
        String tenName = wrapper.tenderName;
        Decimal FreiCredit = wrapper.freightCredit;
        String po = wrapper.PO;
        String pro = wrapper.project;
        String fob = wrapper.fob;
        String priceValid = wrapper.priceValid;
        String priceValidTo = wrapper.priceValidto;
        String offerDate = wrapper.offerEndDateString;
        DateTime accept = wrapper.acceptanceDeadline;
        DateTime offer = wrapper.offerEndDate;
        String addComment = wrapper.additionalComments;
        String broker = wrapper.Broker;
        String tenderNumber = wrapper.TenderNum;
        String taxExe = wrapper.TaxExempt;
        String ForSeason = wrapper.ForSeason;
        String TradeClass = wrapper.TradeClass;
        String Dest = wrapper.Destination;
        String signName = wrapper.signatureName;
        String signImage = wrapper.signatureImageURL;
        String marketName = wrapper.marketerName;
        String marketFax = wrapper.marketerFax;
        String marketEmail = wrapper.Broker2;
        String OpResuCat = wrapper.OppResultCat;
        String OpResuCat2 = wrapper.OppResultCat2;
        String TaxExe2 = wrapper.TaxExempt2;
        String PayO = wrapper.PayOut;
        String PayO2 = wrapper.PayOut2;
        String SMSCon = wrapper.SMSCon;
        String SMSCon2 = wrapper.SMSCon2;
        String POquote = wrapper.POquote;
        String POquote2 = wrapper.PO2quote;
        String SalesComments = wrapper.SalesComments;
        String SalesComments2 = wrapper.SalesComments2;
        String TaxExemptPick = wrapper.TaxExemptPick;
        String TaxExemptPick2 = wrapper.TaxExemptPick2;
        String ForSeasons = wrapper.ForSeason;
       test.stopTest();  
        
    }
    
 
   /* @isTest static void OliTest () {
      setup('ATS_Residual_Product_Category');
        opp.OpportunityLineItems = new List<OpportunityLineItem>();
		opp.OpportunityLineItems.add(oli);
        new ATSWrapperUtilitiesOpp.ATSOpportunityWrapper(opp);
        
    } */
    
    
}