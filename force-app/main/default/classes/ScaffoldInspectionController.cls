public with sharing class ScaffoldInspectionController {
    // The record
    public Scaffold_Inspection__c record {get; set;}
    
    public Scaffold__c test {get; set;}
    
    public ScaffoldInspectionController(ApexPages.StandardController controller) {
    
    
        
        
        this.record = (Scaffold_Inspection__c) controller.getRecord();
        
        this.record = [ SELECT 
                            Toe_board_surface__c, Toe_board_point_load__c, Toe_board_above_platform__c, Tags_updated__c, Tags_accessess__c, Stairs_wide__c, Stairs_vertical__c, 
                            Stairs_height__c, Stairs_depth__c, Stairs_angle__c, Sills_Secured_from_movement__c, Sills_Load_Distribution__c, Sills_Hard_Surfaces__c, 
                            Sills_Base_plates_upright__c, Sills_Base_plates_mudsills__c, Safe_Distance_walkway__c, Safe_Distance_impeed_acess__c, Safe_Distance_conductors__c, 
                            Ramps_resistance__c, Plum_And_Level_height__c, Plum_And_Level_distance__c, Platforms_span__c, Platforms_painted__c, Platforms_mud_sills__c, 
                            Platforms_movement__c, Platforms_minimum_wide__c, Platforms_light_duty__c, Platforms_Scafford_plank__c, Platforms_Planks__c, Platforms_No_gaps__c, 
                            Ladders_deck_height__c, Ladders_cage_from_ladder__c, Ladders_cage_extends__c, Ladders_cage_begin__c, Ladders_access_point__c, Scaffold__c,
                            Ladder_rest_platform__c, Ladder_clear_span__c, Ladder_cage_verticals__c, Id, Hangers_main_clamp__c, Hangers_joiners__c, Guardrails_vertical_members__c, 
                            Guardrails_point_load__c, Guardrails_over_height__c, Guardrails_grade_lumber__c, Guardrails_Top_Guard_Rails__c, Guardrails_Midway__c, Guardrails_Gap__c, 
                            Engineering__c, Connections_wedges__c, Connections_clamps__c, Connections_Transoms__c, Connections_Node__c, Connections_Ledgers__c, Connections_Joints__c, 
                            Connections_Bottom__c, Bracing_wind_force__c, Bracing_head_room__c, Bracing_guy_wired__c, Bracing_free_standing__c, Bracing_csa__c, Access_scafford__c, 
                            Access_opening__c, isLocked__c
                        FROM 
                            Scaffold_Inspection__c
                        WHERE
                            Id =:record.Id];
                            
                            
               test = [Select name from Scaffold__c where id =: record.Scaffold__c ]  ;           
    }
    
    public PageReference save() {
        if(!record.isLocked__c) {
            update record;
            //return null;
        }
        
        PageReference pageRef =  new ApexPages.StandardController(this.record).view();
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    public PageReference submit() {
        
        if(!record.isLocked__c) {
            record.isLocked__c = True;
            
            update record;
        }
        
        PageReference pageRef =  new ApexPages.StandardController(this.record).view();
        pageRef.setRedirect(true);
            
        return pageRef;
    }
}