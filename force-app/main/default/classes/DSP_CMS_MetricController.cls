global virtual with sharing class DSP_CMS_MetricController extends cms.ContentTemplateController
{
    // Edit page constructor
    global DSP_CMS_MetricController(cms.CreateContentController cc)
    {
        super(cc);
    }
    
    global DSP_CMS_MetricController(cms.GenerateContent cc){
        super(cc);
    }
    
    // Constructor
    global DSP_CMS_MetricController()
    {}
    
    global virtual override String getHTML()
    {
        return '';
    }
    
    public String name
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('name')) ?  getProperty('name') : '';
            return returnValue;
        }
        set;
    }
    
    public String value
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('value')) ?  getProperty('value') : '';
            return returnValue;
        }
        set;
    }
    
    public String colour
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('colour')) ?  getProperty('colour') : '';
            return returnValue;
        }
        set;
    }
    
    public String getMetric()
    {        
        String html = '';
        
        html += '<div class="center-align col s12 m12 l12 dashboardlist">';
        html +=     '<div class="center-align col s5 m5 l5">'+name+'</div>';
        html +=     '<div class="center-align col s4 m4 l4">'+value+'</div>';
        
        if(colour == 'Red')
        {
            html +=     '<div class="center-align col s3 m3 l3"><div class="circle red"></div></div>';
        }
        else if(colour == 'Yellow')
        {
            html +=     '<div class="center-align col s3 m3 l3"><div class="circle yellow"></div></div>';
        }
        else
        {
            html +=     '<div class="center-align col s3 m3 l3"><div class="circle green"></div></div>';
        }
        html += '</div>';
        
        return html;
    }
}