/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form Structure class holding information about current engine thresholds.
                I guess that's one of two structure classes that doesn't extend Base class.
                That is bcs mdt records cannot be cast to SObject and back.
Test Class:     EPD_FormStructureThresholdsTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureThresholds {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureThresholds.class);

    @AuraEnabled
    public Decimal maxRPM {get;set;}

    @AuraEnabled
    public Decimal minRPM {get;set;}

    @AuraEnabled
    public Decimal maxIntakeManifoldPressure {get;set;}

    @AuraEnabled
    public Decimal minIntakeManifoldPressure {get;set;}

    @AuraEnabled
    public Decimal maxCylinderCompression {get;set;}

    @AuraEnabled
    public Decimal minCylinderCompression {get;set;}

    @AuraEnabled
    public Decimal recommendedCrankingSpeed {get;set;}

    @AuraEnabled
    public Decimal maxValveSeatRecessionHardLimit {get;set;}

    @AuraEnabled
    public Decimal maxValveSeatRecession {get;set;}

    /**
     * This method maps values from Engine Config custom metadata type into local variables.
     *
     * @param engine
     *
     * @return EPD_FormStructureThresholds
     */
    public EPD_FormStructureThresholds setRecord(EPD_Engine__mdt engine) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Engine config: ' + JSON.serialize(engine));

        maxRPM = engine.Max_RPM__c;
        minRPM = engine.Min_RPM__c;
        maxIntakeManifoldPressure = engine.Max_Intake_Manifold_Pressure__c;
        minIntakeManifoldPressure = engine.Min_Intake_Manifold_Pressure__c;
        maxCylinderCompression = engine.Max_Cylinder_Compression__c;
        minCylinderCompression = engine.Min_Cylinder_Compression__c;
        recommendedCrankingSpeed = engine.Recommended_Cranking_Speed__c;
        maxValveSeatRecessionHardLimit = engine.Max_Valve_Seat_Recession__c;
        maxValveSeatRecession = engine.Valve_Seat_Warning_Limit__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. Engine Threshold Structure: ' + JSON.serialize(this));
        return this;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"maxRPM":' + maxRPM + ',';
        contentBuildingString += '"minRPM":' + minRPM + ',';
        contentBuildingString += '"maxIntakeManifoldPressure":' + maxIntakeManifoldPressure + ',';
        contentBuildingString += '"minIntakeManifoldPressure":' + minIntakeManifoldPressure + ',';
        contentBuildingString += '"maxCylinderCompression":' + maxCylinderCompression + ',';
        contentBuildingString += '"minCylinderCompression":' + minCylinderCompression + ',';
        contentBuildingString += '"recommendedCrankingSpeed":' + recommendedCrankingSpeed + ',';
        contentBuildingString += '"maxValveSeatRecessionHardLimit":' + maxValveSeatRecessionHardLimit + ',';
        contentBuildingString += '"maxValveSeatRecession":' + maxValveSeatRecession;
        return '"engineThresholds":{' + contentBuildingString + '}';
    }

}