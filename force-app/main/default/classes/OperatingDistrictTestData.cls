/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Operating_District__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class OperatingDistrictTestData
{
    public static Operating_District__c createOperatingDistrict(String name, Id businessDepartmentId, Id businessUnitId)
    {                
        Operating_District__c results = new Operating_District__c
            (           
                Name = name,
                Business_Department__c = businessDepartmentId,
                Business_Unit__c = businessUnitId
            );            

        return results;
    }
}