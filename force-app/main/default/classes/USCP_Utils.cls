public class USCP_Utils{
    //convert string dates in dd/mm/yyyy format into Date
    Public Static Date convertDate(String inDate) {
           if(inDate==null || inDate=='') 
           {
               return null;
            }   
           String[] dateParts = inDate.split('/');
           Integer day = Integer.valueOf(dateParts[1]);
           Integer month= Integer.valueOf(dateParts[0]);           
           Integer year = Integer.valueOf(dateParts[2]);                      
          

           Date dt = Date.newInstance(year , month, day);
              
           return dt ;    
      }
   Public Static void LogPageVisit()
   {
     String strurl = ApexPages.currentPage().getUrl();
     System.debug(strurl);
     String[] urlParts = strurl.split('(?i)apex/USCP_'); //case insensitive split
     String strPageName = strurl;
     if(urlParts.size()>1)
     {
         strPageName = urlParts[1];
     }
     strPageName = strPageName.split('\\?')[0]; //get rid of params     
     LogPageVisit(strPageName);
   }
      
   Public Static void LogPageVisit(String pageName)
   {
    USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();   
    system.debug(pageName);
    
    string accountid =  UserAccountID();  

    //this is internal user and we do not track visits for internal users
    if(accountid == null && mc.Track_Page_Visits_internal_Users__c == false)
    {
        return;
    }
    //this is portal user and we do not track visits for portal users
    if(accountid != null && mc.Track_Page_Visits_Portal_Users__c == false)
    {
        return;
    }    
   
    try
    {
        //
    
        USCP_PageTracking__c newTracking = new USCP_PageTracking__c(User__c=UserInfo.getUserId(),Page_Visited__c=pageName,Datetime_Visited__c=datetime.now());
        insert newTracking; 
    }
    catch(Exception ex)
    {
        logError(ex);
    }
    }
    
    Public Static String UserAccountID()
    {
       string accountid =   [select accountid from User where id = :UserInfo.getUserId() limit 1].accountid; 
       return accountid;        
    }

    Public Static Boolean IsAdminUser()
    {    
    
        Profile p = [Select Name from Profile where Id =: userinfo.getProfileid() LIMIT 1];
        String pname = p.name;
        if(pname == 'Standard USCP Admin' || pname == 'System Administrator' || pname == 'HMSC Sales Rep' || pname == 'HMSC Support Rep')
        {
            return true;
        }
        return false;
        //return (USCP_Utils.UserAccountID()==null);
    }
    
    public static void logError(Exception ex)
    {
    
        string message = ex.getMessage();
        logError('Unexpected error:' +message );
    }
    public static void logError(string errorMessage)
    {
        USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
        String csEmail  = mc.Customer_Support_Email__c;    
        String csPhone  = mc.Customer_Support_Phone__c;  
        String csMsg  =  mc.Customer_Support_Message__c;
        if(csMsg != null)
        {
            csMsg = csMsg.replace('{PHONE}', csPhone);
            csMsg = csMsg.replace('{EMAIL}', '<a href= mailto:'+ csEmail + '>' + csEmail + '</a>' );          
        }
        else
        {
           //if customer support message is not set up in customsettings lets use something generic 
           csMsg  = 'Please contact customer support';
        }
    
    
        String message = errorMessage +  '<br/>' +  csMsg;
        system.debug(message);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, message));
    }    
    
    
  }