public with sharing class DateUtilities {
    public static final String REQUEST_DATE_FORMAT = 'M/d/yyyy';    
    public static final String DDMMYYYY_DATE_FORMAT = 'dd/MM/yyyy';
    public static DateTime toDateTime(Date sourceDate) {
        return Datetime.newInstance(sourceDate.year(), sourceDate.month(), sourceDate.day());
    }
    
   /**********************************************************************
     * Converts the supplied date string to a Date instance. 
     * @param dateString The date string to convert. Ex: "2/9/2013", "12/28/2012".
     * @param separatorChar The character between the date parts. Ex: "/"
     * @param defaultToReturnIfError A value to be returned if there as an error.
     **********************************************************************/        
    public static Date toMDYYYDate(String dateString, String separatorChar, Date defaultToReturnIfError) {
        Date result = defaultToReturnIfError;
        
        try {
            String[] values = dateString.split(separatorChar, 3);
            Integer month = Integer.valueOf(values[0]);
            Integer day = Integer.valueOf(values[1]);
            Integer year = Integer.valueOf(values[2]);
            
            result = Date.newInstance(year, month, day);
        } catch (Exception ex) { /** Ignore*/ }
        
        return result;
    }
    public static Date toDMYYYDate(String dateString, String separatorChar, Date defaultToReturnIfError) {
        Date result = defaultToReturnIfError;
        
        try {
            String[] values = dateString.split(separatorChar, 3);
            Integer month = Integer.valueOf(values[1]);
            Integer day = Integer.valueOf(values[0]);
            Integer year = Integer.valueOf(values[2]);
            
            result = Date.newInstance(year, month, day);
        } catch (Exception ex) { /** Ignore*/ }
        
        return result;
    }
}