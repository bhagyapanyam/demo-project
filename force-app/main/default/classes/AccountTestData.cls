public class AccountTestData {

	public static Account createAccount(String accountName, Id parentAccountId) {
	
		Account account = new Account();
		account.Name = accountName;
		account.ParentId = (parentAccountId != Null) ? parentAccountId : Null;
		
		return account;
	}
    
	public static Account createAccountOpp(String accountName, Id parentAccountId) {
	
		Account account = new Account();
		account.Name = accountName;
		account.ParentId = (parentAccountId != Null) ? parentAccountId : Null;
		
		return account;
	}    
}