/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_DataCleanupScheduleTest
{
    public static String CRON_EXP = '0 0 0 3 9 ? 2022';

    static testmethod void test() {

        Integer recordcount;
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {


                USCP_Settings__c  mc = new USCP_Settings__c(); //USCP_Settings__c.getOrgDefaults();
                mc.Delete_Data_After_N_Days__c  = 15; 
                insert mc;
    
                /*        
                //lets prepare some data
                //lets create some test terminal
                USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
                System.AssertNotEquals(terminal.Id, Null);
        
                //create test product
                Product2 product = USCP_TestData.createProduct('product1',true);
                
                Date cutoffday =  Date.today()-30;
                
                // Create a Parent Account with the name 100
                Account account = AccountTestData.createAccount('100', Null);
                insert account;
                System.AssertNotEquals(account.Id, Null);            
                
                
                
                USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100',  null,  cutoffday , cutoffday , cutoffday , 1000.0,  true);
                USCP_TestData.createInvoice(account.Id, 'INV200', null, cutoffday , cutoffday , cutoffday , 2000.0,  true);
                
                recordcount = [SELECT COUNT() FROM USCP_Invoice__c];
                System.assertEquals(recordcount, 2);            
                
                
                USCP_TestData.createBOLAccTransaction(account.Id, 'BOL1', invoice.id,  'Carrier 1', terminal.id,   cutoffday , product.id ,  true);
                USCP_TestData.createBOLAccTransaction(account.Id, 'BOL2', invoice.id,  'Carrier 1', terminal.id,   cutoffday , product.id ,  true);
                recordcount = [SELECT COUNT() FROM USCP_BOL_AccTransaction__c];
                System.assertEquals(recordcount, 2);                
                    */
         System.debug('Start Test');
         Test.startTest();

            
                String jobId = System.schedule('testBasicScheduledApex',
                          CRON_EXP, 
                             new USCP_DataCleanupSchedule());
                
                // Get the information from the CronTrigger API object
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                 NextFireTime
                 FROM CronTrigger WHERE id = :jobId];

              // Verify the expressions are the same
              System.assertEquals(CRON_EXP, 
                 ct.CronExpression);

              // Verify the job has not run
              System.assertEquals(0, ct.TimesTriggered);

              // Verify the next time the job will run
              System.assertEquals('2022-09-03 00:00:00', 
                 String.valueOf(ct.NextFireTime));


            Test.stopTest();
            System.debug('Stop Test');            
            
            // Verify records got deleted 
            /* for whatever reasons records do not get deleted
            recordcount = [SELECT COUNT() FROM USCP_Invoice__c];
            System.assertEquals(0,recordcount);
            recordcount = [SELECT COUNT() FROM USCP_BOL_AccTransaction__c];
            System.assertEquals(0,recordcount); 
            recordcount = [SELECT COUNT() FROM USCP_EFT__c];
            System.assertEquals(0,recordcount);                            
            */
        }            
            
    }

}