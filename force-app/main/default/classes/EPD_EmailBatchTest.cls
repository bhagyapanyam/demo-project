/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EmailBatch
History:        jschn 2019-07-19 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EmailBatchTest {

    @IsTest
    static void testBatch_withEmptyList() {
        EPD_EmailBatch job;

        Test.startTest();
        job = new EPD_EmailBatch(null, new List<EPD_Engine_Performance_Data__c>());
        Database.executeBatch(job, 1);
        Test.stopTest();

        //No idea what to test assert
        //this should fail on emailService.sendEmails call.
    }

    @IsTest
    static void testBatch_properParams() {
        EPD_EmailBatch job;

        Test.startTest();
        job = new EPD_EmailBatch(
                new EPD_EmailNotificationOnSubmitPDF(),
                new List<EPD_Engine_Performance_Data__c> {
                        (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                new EPD_Engine_Performance_Data__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                }
        );
        Database.executeBatch(job, 1);
        Test.stopTest();

        //No idea what to test assert
        //This should pass even with empty scope
    }

    @IsTest
    static void testBatch_invalidRecordCount() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_EmailBatch batch = new EPD_EmailBatch(null, null);

        Test.startTest();
        try {
            batch.validateRecordCount(
                    HOG_SObjectFactory.createSObjectList(
                            new EPD_Engine_Performance_Data__c(),
                            batch.RECORD_COUNT_THRESHOLD + 1,
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}