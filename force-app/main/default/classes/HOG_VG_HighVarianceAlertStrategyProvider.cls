/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertStrategyProvider for High Variance Alerts.
                This class contains logic for deciding whether High Variance Alert should be
                created.
Test Class:     HOG_VG_HighVarianceAlertStrategyPrvdTest
History:        jschn 19/10/2018 - Created. - W-001276
*************************************************************************************************/
public with sharing class HOG_VG_HighVarianceAlertStrategyProvider extends HOG_VG_AlertStrategyProvider {

    public HOG_VG_HighVarianceAlertStrategyProvider(List<Location__c> locations,
            Map<Id, Location__c> oldLocationsMap,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {
        super(locations, oldLocationsMap, ventGasExemptionsByLocationId, activeAlertsByLocationAndType);
    }

    /**
     * Implementation of decision logic for creating High Variance Alert.
     * Checks whether there is another active alert as well as if there is an active exemption on requested Location.
     * Last, it will check if there are all necessary conditions met.
     *
     * @param location
     *
     * @return
     */
    public override Boolean shouldCreate(Location__c location) {
        return !super.ventGasExemptionsByLocationId.containsKey(location.Id)
                && !super.activeAlertsByLocationAndType.containsKey(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VARIANCE)
                && largeHighVarianceTriggered(location);
    }

    /**
     * Checks if there are all necessary conditions met for creating requested Alert.
     *
     * @param location
     *
     * @return
     */
    private Boolean largeHighVarianceTriggered(Location__c location) {
        return location.High_Variance__c == 'YES'
                && super.isCorrectLocation(location)
                && correctDateToCreate();
    }

    /**
     * Compares today's day number with value from Vent Gas settings.
     *
     * @return
     */
    protected override Boolean correctDateToCreate() {
        Date today = Date.today();
        return today.day() == super.settings.Alerts_Create_Day_of_month__c;
    }


}