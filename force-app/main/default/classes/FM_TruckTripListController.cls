/**
 * Test class 	FM_TruckTripListControllerTest
 */
public with sharing class FM_TruckTripListController {

	public List<TruckTripWrapper> 			truckTrips		{get; private set;}
	public FilterWrapper					filter			{get; private set;}
	public String							linkForExport	{get; private set;}
	public SortWrapper						sortWrapper		{get; set;}

	//NOTE if user not vendor - able to see husky logistic records (I don't know if anybody would like to see husky logistic records)
	public Boolean							isVendor		{get; private set;}

	public Boolean							isSelection		{get {
		if (truckTrips != null)
			for (TruckTripWrapper tt : truckTrips)
				if (tt.marked) return true;
		return false;
	}}

	public FM_TruckTripListController() {
		isVendor = getVendorUserInfo();
		filter = new FilterWrapper(isVendor);
		truckTrips = new List<TruckTripWrapper>();
		linkForExport = '';
		sortWrapper = new SortWrapper();
	}

	private Boolean getVendorUserInfo() {
		List<PermissionSetAssignment> pss = [SELECT Id, PermissionSet.Name
		FROM PermissionSetAssignment
		WHERE AssigneeId =: UserInfo.getUserId()];
		for (PermissionSetAssignment ps : pss) {
			if (ps.PermissionSet.Name == 'HOG_FM_Dispatcher_Vendor') return true;
		}
		return false;
	}

	public Boolean getIsLightningUser(){
		return HOG_GeneralUtilities.isUserUsingLightning();
	}

	public PageReference export() {
		if (getAbleToExport()) {
			PageReference pageRef = new PageReference(linkForExport);
			pageRef.setRedirect(false);
			return pageRef;
		}
		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Not able to export.'));
		return null;
	}

	public PageReference exportLtng() {
		return null;
	}

	public PageReference exportSelection() {
		if (getAbleToExport() && isSelection) {
			PageReference pageRef = new PageReference(getLinkForExportSelection());
			pageRef.setRedirect(false);
			return pageRef;
		}
		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Not able to export.'));
		return null;
	}

	public PageReference exportSelectionLtng() {
		return null;
	}

	public String getLinkForExportSelection() {
		String linkForExportSelection = '';
		if (truckTrips != null && truckTrips.size() > 0) {
			linkForExportSelection = FM_Utilities.TRUCKTRIP_EXPORT_LINK_BASE;
			for (TruckTripWrapper tt : truckTrips)
				if (tt.marked) {
					if(linkForExport != FM_Utilities.TRUCKTRIP_EXPORT_LINK_BASE) linkForExportSelection += ',';
					linkForExportSelection += tt.truckTrip.Id;
				}
		}
		return linkForExportSelection;
	}

	private void prepareLinkForExport() {
		if (truckTrips != null && truckTrips.size() > 0) {
			linkForExport = FM_Utilities.TRUCKTRIP_EXPORT_LINK_BASE;
			for (TruckTripWrapper tt : truckTrips) {
				if(linkForExport != FM_Utilities.TRUCKTRIP_EXPORT_LINK_BASE) linkForExport += ',';
				linkForExport += tt.truckTrip.Id;
			}
		}
	}

	public Boolean getAbleToExport() {
		return (truckTrips != null && truckTrips.size() > 0 && linkForExport != FM_Utilities.TRUCKTRIP_EXPORT_LINK_BASE);
	}

	public Boolean getTableHasRecords() {
		return truckTrips != null && truckTrips.size() > 0;
	}

	public PageReference search() {
		if (truckTrips == null) truckTrips = new List<TruckTripWrapper>();
		if (trucktrips.size() > 0) truckTrips.clear();
		if (isFilterValid()) {
			for(FM_Truck_Trip__c tt : Database.query(getQueryString()))
				truckTrips.add(new TruckTripWrapper(tt, sortWrapper));
			prepareLinkForExport();
		}
		return null;
	}

	private Boolean isFilterValid() {
		Boolean valid = true;
		if (filter.routes == null || filter.routes.size() == 0) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No route was selected.'));
			valid = false;
		}
		if (String.isBlank(filter.carrier)) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No carrier was selected.'));
			valid = false;
		}
		return valid;
	}

	private String getQueryString() {
		String queryString = FM_Utilities.QUERY_TRUCKTRIP_EXPORT_IMPORT
				+ ' WHERE Route__c IN (' + filter.getSelectedRoutesForQuery() + ')'
				+ ' AND Carrier__c = \'' + filter.carrier + '\''
				+ ' AND (NOT Load_request__r.Standing_Comments__c LIKE \'Created during Project Release Migration%\')';
		if(filter.showExported) queryString += ' AND (Truck_Trip_Status__c = \'Exported\' OR Truck_Trip_Status__c = \'New\')';
		else queryString += ' AND Truck_Trip_Status__c = \'New\'';
		queryString += ' ORDER BY CreatedDate desc LIMIT 650';
		return queryString;
	}

	//SORTING
	public void SortToggle() {
		// if the sort column has changed, then change it back to asc
		sortWrapper.SORT_DIRECTION = (sortWrapper.SORT_DIRECTION.equals(sortWrapper.SORT_ORDER_ASC) ?
				sortWrapper.SORT_ORDER_DSC : sortWrapper.SORT_ORDER_ASC);
		if (sortWrapper.LAST_SORT_BY != sortWrapper.SORT_BY) {
			sortWrapper.SORT_DIRECTION = sortWrapper.SORT_ORDER_ASC;
			sortWrapper.LAST_SORT_BY = sortWrapper.SORT_BY;
		}
		truckTrips.sort();
	}

	public class TruckTripWrapper implements Comparable {
		public FM_Truck_Trip__c	truckTrip	{get;set;}
		public String 			wellName {get; private set;}
		public Boolean			marked		{get;set;}
		private SortWrapper		sortWrapper {get;set;}

		public TruckTripWrapper(FM_Truck_Trip__c truckTrip, SortWrapper sortWrapper) {
			this.truckTrip = truckTrip;
			this.wellName = FM_TruckTrip_Utilities.getWellName(truckTrip);
			marked = false;
			this.sortWrapper = sortWrapper;
		}

		public Integer compareTo(Object obj) {
			TruckTripWrapper ttw = (TruckTripWrapper) obj;
			if (sortWrapper.SORT_BY == sortWrapper.SORT_BY_ROUTE) return sortByRoute(ttw);
			if (sortWrapper.SORT_BY == sortWrapper.SORT_BY_CREATED_DATE) return sortByCreatedDate(ttw);
			if (sortWrapper.SORT_BY == sortWrapper.SORT_BY_WELL_NAME) return sortByWellName(ttw);
			if (sortWrapper.SORT_BY == sortWrapper.SORT_BY_STATUS) return sortByStatus(ttw);
			return 0; //preserve existing order
		}

		private Integer sortByRoute(TruckTripWrapper ttw) {
			String thisRoute = this.truckTrip.Route__c;
			String comparedRoute = ttw.truckTrip.Route__c;
			if (thisRoute > comparedRoute) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? 1 : -1;
			if (thisRoute < comparedRoute) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? -1 : 1;
			return 0;
		}

		private Integer sortByCreatedDate(TruckTripWrapper ttw) {
			String thisDate = String.valueOf(this.truckTrip.Load_Request__r.CreatedDate);
			String comparedDate = String.valueOf(ttw.truckTrip.Load_Request__r.CreatedDate);
			if (thisDate > comparedDate) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? 1 : -1;
			if (thisDate < comparedDate) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? -1 : 1;
			return 0;
		}

		private Integer sortByWellName(TruckTripWrapper ttw) {
			String thisWellName = (this.truckTrip.Load_request__r.Source_Location__c != null ?
					this.truckTrip.Load_request__r.Source_Location__r.Name :
					this.truckTrip.Load_request__r.Source_Facility__r.Name)
					+ ' / ' + this.truckTrip.Tank__c;
			String comparedWellName = (ttw.truckTrip.Load_request__r.Source_Location__c != null ?
					ttw.truckTrip.Load_request__r.Source_Location__r.Name :
					ttw.truckTrip.Load_request__r.Source_Facility__r.Name)
					+ ' / ' + ttw.truckTrip.Tank__c;
			if (thisWellName > comparedWellName) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? 1 : -1;
			if (thisWellName < comparedWellName) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? -1 : 1;
			return 0;
		}

		private Integer sortByStatus(TruckTripWrapper ttw) {
			String thisStatus = this.truckTrip.Truck_Trip_Status__c;
			String comparedStatus = ttw.truckTrip.Truck_Trip_Status__c;
			if (thisStatus > comparedStatus) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? 1 : -1;
			if (thisStatus < comparedStatus) return (sortWrapper.SORT_DIRECTION == sortWrapper.SORT_ORDER_ASC) ? -1 : 1;
			return 0;
		}

	}

	public class FilterWrapper {
		public List<String>			routes			{get;set;}
		public String				carrier		 	{get;set;}
		public Boolean				showExported	{get;set;}

		public List<SelectOption> 	routeOptions	{get;set;}
		public List<SelectOption> 	carrierOptions	{get;set;}

		public FilterWrapper(Boolean isVendor) {
			routes = new List<String>();
			showExported = false;
			getRouteOptions();
			getCarrierOptions(isVendor);
			setCarrier();
		}

		private void getRouteOptions() {
			routeOptions = new List<SelectOption>();
			for (Route__c route : [SELECT Name FROM Route__c WHERE Fluid_Management__c = true ORDER BY Name])
				routeOptions.add(new SelectOption(route.Name, route.Name));
		}

		private void getCarrierOptions(Boolean isVendor) {
			carrierOptions = new List<SelectOption>();
			for (Carrier__c carrier : [SELECT Id, Carrier_Name_For_Fluid__c, Carrier_Name__c
			FROM Carrier__c WHERE Enabled__c = true])
				if (!isVendor || (!carrier.Carrier_Name__c.contains('Husky')
						&& !carrier.Carrier_Name_For_Fluid__c.contains('Husky')))
					carrierOptions.add(new SelectOption(carrier.Id, carrier.Carrier_Name__c));
		}

		private void setCarrier() {
			if (carrierOptions != null && carrierOptions.size() > 0)
				carrier = carrierOptions.get(0).getValue();
		}

		public String getSelectedRoutesForQuery() {
			return FM_Utilities.getStringQueryFromStringList(routes);
		}

	}

	public class SortWrapper {
		public String		SORT_BY_ROUTE 			{get; private set;}
		public String		SORT_BY_CREATED_DATE 	{get; private set;}
		public String		SORT_BY_WELL_NAME 		{get; private set;}
		public String		SORT_BY_STATUS 			{get; private set;}
		public String 		SORT_ORDER_ASC 			{get; private set;}
		public String 		SORT_ORDER_DSC 			{get; private set;}

		public String 		SORT_BY 				{get; set;}
		public String 		SORT_DIRECTION 			{get; set;}
		public String 		LAST_SORT_BY 			{get; set;}

		public SortWrapper() {
			SORT_BY_ROUTE = 'Route';
			SORT_BY_CREATED_DATE = 'CreatedDate';
			SORT_BY_WELL_NAME = 'WellName';
			SORT_BY_STATUS = 'Status';
			SORT_ORDER_ASC = 'ASC';
			SORT_ORDER_DSC = 'DSC';
			reset();
		}

		public void reset() {
			SORT_BY = SORT_BY_CREATED_DATE;
			SORT_DIRECTION = SORT_ORDER_ASC;
			LAST_SORT_BY = SORT_BY;
		}

	}

}