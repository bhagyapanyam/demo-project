/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_WorkFlowEngine_Endpoint, VTT_Rule_AssignedButNotWorking
                VTT_Rule_TradesmanWorkStatusValidation, VTT_Rule_AssignedAndWorking class
History:        mbrimus 21/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_WorkFlowEngineTest {

    @IsTest
    static void assignedButNotWorking() {
        VTT_Rule_AssignedButNotWorking result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            Test.startTest();
            result = new VTT_Rule_AssignedButNotWorking(activity, tradesman, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(4, result.getAvailableActions().size());

    }

    @IsTest
    static void assignedAndWorking_StartJOB() {
        VTT_Rule_AssignedAndWorking result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_Job_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            result = new VTT_Rule_AssignedAndWorking(activity, tradesman, lastEntry, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(3, result.getAvailableActions().size());

    }

    @IsTest
    static void assignedAndWorking_StartAtEquipment() {
        VTT_Rule_AssignedAndWorking result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            result = new VTT_Rule_AssignedAndWorking(activity, tradesman, lastEntry, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(3, result.getAvailableActions().size());

    }

    @IsTest
    static void assignedAndWorking_FinishedAtEquipment() {
        VTT_Rule_AssignedAndWorking result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            result = new VTT_Rule_AssignedAndWorking(activity, tradesman, lastEntry, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(4, result.getAvailableActions().size());

    }

    @IsTest
    static void assignedAndWorking_FinishedForTheDay() {
        VTT_Rule_AssignedAndWorking result;
        Boolean expectedResult = false;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
            entry.TimeStamp__c = System.now().addHours(-5);
            update entry;
            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
            VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Finished_for_the_Day_Action),
            activity,
            tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    AND Status__c = :VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            result = new VTT_Rule_AssignedAndWorking(activity, tradesman, lastEntry, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(4, result.getAvailableActions().size());

    }

    @IsTest
    static void assignedAndWorking_JobOnHold() {
        VTT_Rule_AssignedAndWorking result;
        Boolean expectedResult = false;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
            entry.TimeStamp__c = System.now().addHours(-5);
            update entry;
            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Job_On_Hold_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    AND Status__c = :VTT_Utilities.LOGENTRY_JOBONHOLD
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            result = new VTT_Rule_AssignedAndWorking(activity, tradesman, lastEntry, false, true);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(0, result.getMessages().size());
        System.assertEquals(3, result.getAvailableActions().size());

    }

    @IsTest
    static void tradesmanWorkStatusValidation_NotAssigned() {
        VTT_Rule_TradesmanWorkStatusValidation result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            //VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);


            Test.startTest();
            result = new VTT_Rule_TradesmanWorkStatusValidation(activity, tradesman);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(1, result.getMessages().size());
        System.assertEquals(0, result.getAvailableActions().size());

    }

    @IsTest
    static void tradesmanWorkStatusValidation_AssignedToOther() {
        VTT_Rule_TradesmanWorkStatusValidation result;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activityAssigned = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            Work_Order_Activity__c activityNOTAssigned = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activityAssigned.Id, tradesman.Id);

            activityAssigned = VTT_TestData.reloadWorkOrderActivity(activityAssigned.Id);
            tradesman.Current_Work_Order_Activity__c = activityNOTAssigned.Id;
            update tradesman;

            Test.startTest();
            result = new VTT_Rule_TradesmanWorkStatusValidation(activityAssigned, tradesman);
            result.processRule();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result.passedRule());
        System.assertEquals(1, result.getMessages().size());
        System.assertEquals(1, result.getAvailableActions().size());

    }

    @IsTest
    static void workFlowEngine_TradesmanIsWorking() {
        VTT_LTNG_WorkFlowEngine engine;
        Boolean expectedResult = false;

        System.runAs(VTT_TestData.createVTTUser()) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
            Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
            Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);

            Work_Order_Activity__c activity = VTT_TestDataFactory.createWorkOrderActivity(
                    'TEST',
                    'THERMAL',
                    workOrder.Id,
                    thermalField.Id,
                    true);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);

            new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                    VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action),
                    activity,
                    tradesman);

            Work_Order_Activity_Log_Entry__c lastEntry = [
                    SELECT Status__c
                    FROM Work_Order_Activity_Log_Entry__c
                    WHERE Work_Order_Activity__c = :activity.Id
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];

            Test.startTest();
            VTT_TradesmanFlags tradesmanInfo = new VTT_Utility_EndpointHandler().getTradesmanInfo();
            engine = new VTT_LTNG_WorkFlowEngine.EngineBuilder()
                    .addTradesman(tradesman)
                    .addTradesmanInfo(tradesmanInfo)
                    .addActivity(activity)
                    .addLastEntry(lastEntry)
                    .checkForEPD()
                    .build();
            Test.stopTest();
        }
        System.assertEquals(0, engine.messages.size());
        System.assertEquals(3, engine.availableActions.size());

    }
}