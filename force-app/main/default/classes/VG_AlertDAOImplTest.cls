/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VG_AlertDAOImpl
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class VG_AlertDAOImplTest {

    @IsTest
    static void getAlertsForTriggerByType_withoutParams_withoutParams() {
        VG_AlertTriggerDAO dao = new VG_AlertDAOImpl();
        List<HOG_Vent_Gas_Alert__c> result;
        List<HOG_Vent_Gas_Alert__c> expectedResult = new List<HOG_Vent_Gas_Alert__c>();

        result = dao.getAlertsForTriggerByType(null, null);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getAlertsForTriggerByType_withoutParams_withoutParam1() {
        VG_AlertTriggerDAO dao = new VG_AlertDAOImpl();
        List<HOG_Vent_Gas_Alert__c> result;
        List<HOG_Vent_Gas_Alert__c> expectedResult = new List<HOG_Vent_Gas_Alert__c>();
        Set<Id> param1;
        Set<String> param2 = new Set<String> {
                HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF
        };

        result = dao.getAlertsForTriggerByType(param1, param2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getAlertsForTriggerByType_withoutParams_withoutParam2() {
        VG_AlertTriggerDAO dao = new VG_AlertDAOImpl();
        List<HOG_Vent_Gas_Alert__c> result;
        List<HOG_Vent_Gas_Alert__c> expectedResult = new List<HOG_Vent_Gas_Alert__c>();
        Set<Id> param1 = HOG_GeneralUtilities.getSetOfIds([SELECT Id FROM HOG_Vent_Gas_Alert__c]);
        Set<String> param2;

        result = dao.getAlertsForTriggerByType(param1, param2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getAlertsForTriggerByType_withoutParams_withWrongParams() {
        VG_AlertTriggerDAO dao = new VG_AlertDAOImpl();
        List<HOG_Vent_Gas_Alert__c> result;
        List<HOG_Vent_Gas_Alert__c> expectedResult = new List<HOG_Vent_Gas_Alert__c>();
        Set<Id> param1 = new Set<Id> {UserInfo.getUserId()};
        Set<String> param2 = new Set<String> {
                HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VARIANCE
        };

        result = dao.getAlertsForTriggerByType(param1, param2);

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getAlertsForTriggerByType_withoutParams_withPropperParams() {
        VG_AlertTriggerDAO dao = new VG_AlertDAOImpl();
        List<HOG_Vent_Gas_Alert__c> result;
        List<HOG_Vent_Gas_Alert__c> currentAlerts = [SELECT Id FROM HOG_Vent_Gas_Alert__c];
        Integer expectedCount = currentAlerts.size();
        Set<Id> param1 = HOG_GeneralUtilities.getSetOfIds(currentAlerts);
        Set<String> param2 = new Set<String> {
                HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF,
                HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST
        };

        result = dao.getAlertsForTriggerByType(param1, param2);

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @TestSetup
    static void prepareData() {
        HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c();
        insert config;

        Location__c location = HOG_VentGas_TestData.createLocation();
        User usr = HOG_VentGas_TestData.createUser();

        HOG_VentGas_TestData.createAlert('TestAlert1',
                'TestAlertDescription1',
                location.Id,
                usr.Id,
                HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
                HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF);

        HOG_VentGas_TestData.createAlert('TestAlert2',
                'TestAlertDescription2',
                location.Id,
                usr.Id,
                HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
                HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);
    }

}