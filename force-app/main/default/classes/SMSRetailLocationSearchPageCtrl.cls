// ********************************************************************
// 
public without sharing class SMSRetailLocationSearchPageCtrl {
 
    ApexPages.StandardSetController resultList{get; private set;}
    public Retail_Location__c filterLocation{get; set;}

    public SMSRetailLocationSearchPageCtrl() {
        InitializePage();
    }

    public void InitializePage() {
        String query = 'SELECT Id';

        for (Schema.FieldSetMember resultField : getResultFields()) {
            query += ', ' + resultField.getFieldPath();
        }

        query += ' FROM Retail_Location__c Where Active__c = \'YES\'';

        resultList = 
            new ApexPages.StandardSetController(
                Database.getQueryLocator(query));

        Retail_Location__c dummyLocation = 
        [SELECT Id FROM Retail_Location__c LIMIT 1];

        filterLocation = new Retail_Location__c(Id = dummyLocation.Id); 

        for (Schema.FieldSetMember filterField : this.getFilters()) {
            if (filterField.getFieldPath() == 'Active__c') {
                filterLocation.put('Active__c', 'YES');
            }
            else {
                filterLocation.put(filterField.getFieldPath(), null);
            }
        }
    }


    public List<Schema.FieldSetMember> getFilters() {
        return SobjectType.Retail_Location__c.FieldSets.SMS_Search_Filter.getFields();
    }

    public List<Schema.FieldSetMember> getResultFields() {
        return SobjectType.Retail_Location__c.FieldSets.SMS_Results.getFields();
    }

    public PageReference Reset() {
        InitializePage();
        return null;
    }

    public PageReference search() {
        
        List<GroupMember> CPMDatabaseMembers = [ Select Group.name, Group.DeveloperName from GroupMember
                                                    where UserOrGroupId = :UserInfo.getUserId()
                                                    And Group.DeveloperName = 'CPM_Contract_Database' ];
        String query = 'SELECT Id';

        for (Schema.FieldSetMember resultField : getResultFields()) {
            query += ', ' + resultField.getFieldPath();
        }

        query += ' FROM Retail_Location__c';

        Boolean hasPrevCondition = false;
        
        List<User> users = [ Select Id, Name from User where Name = 'Retail DataLoader' ];
        if( CPMDatabaseMembers.size() == 0 )
        {
            if( users.size() > 0 )
            {
                query += ' Where CreatedById = \'' + users[0].Id + '\'';
                hasPrevCondition = true;
            }
        }

        for (Schema.FieldSetMember filterField : this.getFilters()) {
            Object queryVal = filterLocation.get(filterField.getFieldPath());
            if (queryVal != null &&
                (filterField.getType() != Schema.DisplayType.Boolean ||
                (Boolean)queryVal == true)) {

                if (hasPrevCondition) {
                    query += ' AND ';
                } else {
                    query += ' WHERE ';
                    hasPrevCondition = true;
                }

                if (filterField.getFieldPath() == 'Address__c') {
                    query += '(Address__c LIKE \'%' +
                             String.escapeSingleQuotes((String)queryVal) + 
                             '%\')';
                    continue;
                }

                if (filterField.getFieldPath() == 'Cardlock__c') {
                     Schema.DescribeFieldResult fieldResult = Retail_Location__c.Cardlock_Type__c.getDescribe();

                     String subQuery = '';

                     for (Schema.PicklistEntry picklistVal : fieldResult.getPicklistValues()) {
                        if (picklistVal.getValue().toLowerCase() == 'no') {
                            continue;
                        }
                        if (subQuery != '') {
                            subQuery += ' OR ';
                        }
                        subQuery += 'Cardlock_Type__c = \'' + 
                        picklistVal.getValue() + '\' ';

                     }

                     if (subQuery != '') {
                        query += '(' + subQuery + ')';
                     }
                        
                    continue;
                }

                query += filterField.getFieldPath();





                if (filterField.getType() == Schema.DisplayType.String) {

                    query += ' LIKE \'%' + 
                             String.escapeSingleQuotes((String)queryVal) + 
                             '%\''; 

                }
                else if (filterField.getType() == Schema.DisplayType.Boolean) {
                    if ((Boolean)queryVal == true) {
                        query += ' = ' + (Boolean)queryVal;
                    }
                }
                else if (filterField.getType() == Schema.DisplayType.MultiPicklist) {
                    query += ' INCLUDES (\'' +
                    String.escapeSingleQuotes((String)queryVal).replaceAll(';', '\',\'') +
                    '\')';  
                    System.debug(query);

                }
                else {
                    query += ' = \'' + 
                    String.escapeSingleQuotes((String)queryVal) + '\'';
                }
            }
        }
        query += ' LIMIT 10000';


        System.debug(query);

        if (!Test.isRunningTest()) {
            resultList = 
            new ApexPages.StandardSetController(
                Database.getQueryLocator(query));

        }
        else {
            System.assert(!String.isEmpty(query));
        }

        return null;
    }

    public PageReference next() {
        resultList.next();
        return null;
    }

    public PageReference previous() {
        resultList.previous();
        return null;
    }

    public Boolean getHasNext() {

        return resultList.getHasNext();

    }

    public Boolean getHasPrevious() {
        return resultList.getHasPrevious();
    }

    public List<Retail_Location__c>getLocations() {
        return (List<Retail_Location__c>)resultList.getRecords();
    }
}