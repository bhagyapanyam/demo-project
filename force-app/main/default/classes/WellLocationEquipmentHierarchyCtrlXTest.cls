@isTest
private class WellLocationEquipmentHierarchyCtrlXTest {
	
	static Testmethod void testEquipmentHierarchy() {
		Location__c testLocation = LocationTestData.createLocation();
		List<Equipment__c> testEquipmentHierarchy = EquipmentTestData.createEquipmentHierarchy(testLocation);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(testLocation));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}
	
	
	static Testmethod void testEquipmentHierarchyEvent() {
		Well_Event__c testEvent = LocationTestData.createEvent();
		List<Equipment__c> testEquipmentHierarchyEvent = EquipmentTestData.createEquipmentHierarchyEvent(testEvent);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(testEvent));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment Event: ' +testEquipmentHierarchyEvent[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchyEvent[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchyEvent[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}
	
	static Testmethod void testEquipmentHierarchySystem() {
		System__c testSystem = LocationTestData.createSystem('test System');
		List<Equipment__c> testEquipmentHierarchySystem = EquipmentTestData.createEquipmentHierarchySystem(testSystem);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(testSystem));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment System: ' +testEquipmentHierarchySystem[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchySystem[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchySystem[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}
	
	static Testmethod void testEquipmentHierarchySubSystem() {
		Sub_System__c testSubSystem = LocationTestData.createSubSystem('test Sub System');
		List<Equipment__c> testEquipmentHierarchySubSystem = EquipmentTestData.createEquipmentHierarchySubSystem(testSubSystem);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(testSubSystem));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment Sub-System: ' +testEquipmentHierarchySubSystem[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchySubSystem[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchySubSystem[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}

	static Testmethod void testEquipmentHierarchyFEL() {
		Functional_Equipment_Level__c fel = LocationTestData.createFEL('test FEL');
		List<Equipment__c> testEquipmentHierarchySubSystem = EquipmentTestData.createEquipmentHierarchyFEL(fel);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(fel));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment Sub-System: ' +testEquipmentHierarchySubSystem[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchySubSystem[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchySubSystem[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}

	static Testmethod void testEquipmentHierarchyYard() {
		Yard__c yard = LocationTestData.createYard('test Yard');
		List<Equipment__c> testEquipmentHierarchySubSystem = EquipmentTestData.createEquipmentHierarchyYard(yard);

		WellLocationequipmentHierarchyCtrlX wellLocationHierarchyCtrl = new WellLocationequipmentHierarchyCtrlX(
			new ApexPages.StandardController(yard));

		List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();

		//Test Hierarchy Structure
		System.assertEquals(equipmentHierarchyResult.size(), 5);
		for(Integer i=0; i < 5; i++) {
			System.debug('Superior Equipment Sub-System: ' +testEquipmentHierarchySubSystem[i].Superior_Equipment__c);
			System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchySubSystem[i].Id);
			System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
			if(i == 0) {
				//Top Level tests
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			} else if (i == 4) {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
			} else {
				System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
				System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
				System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
			}
		}

		//Test Hierarchy Behaviour
		ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchySubSystem[0].Id);
		wellLocationHierarchyCtrl.toggleNodeState();
		equipmentHierarchyResult = wellLocationHierarchyCtrl.getEquipmentHierarchy();
		System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
	}
}