/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Field Defaults for SObjects related to EPD. Contains wrong values.
History:        jschn 2019-07-25 - Created.
*************************************************************************************************/
@IsTest
@SuppressWarnings('ApexUnusedDeclaration')
public class EPD_FieldDefaultsWrong {

    public static final String CLASS_NAME = String.valueOf(EPD_FieldDefaultsWrong.class);

    public class HOG_Engine_Performance_DataDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    HOG_Engine_Performance_Data__c.Equipment_Engine_Persist_Fail_Addresses__c => 'invalidEmailAddress',
                    HOG_Engine_Performance_Data__c.PDF_Senders_Name__c => 'WrongSendersName',
                    HOG_Engine_Performance_Data__c.Threshold_Alert_Cc_Addresses__c => 'WrongSendersName',
                    HOG_Engine_Performance_Data__c.Compression_Minor_Threshold__c => 0.0
            };
        }
    }

    public class HOG_Maintenance_Servicing_FormDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    HOG_Maintenance_Servicing_Form__c.Work_Order_Number__c => 'Mark 85',
                    HOG_Maintenance_Servicing_Form__c.Order_Type__c => 'Wrong'
            };
        }
    }

    public class EPD_Cylinder_InformationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        String recordTypeName = EPD_Constants.MEASUREMENT_TYPE_TO_CYLINDER_RECORD_TYPE_NAME.get(
                'Wear'
        );
        Id cylinderRecordType = Schema.SObjectType.EPD_Cylinder_Information__c.getRecordTypeInfosByName()
                .get(recordTypeName).getRecordTypeId();
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Cylinder_Information__c.Cylinder_Number__c=> 1,
                    EPD_Cylinder_Information__c.Exhaust_Temperature__c => 20,
                    EPD_Cylinder_Information__c.Wear_Intake__c => 0.090,
                    EPD_Cylinder_Information__c.Wear_Exhaust__c => 0.090,
                    EPD_Cylinder_Information__c.Compression__c => 500,
                    EPD_Cylinder_Information__c.RecordTypeId => cylinderRecordType
            };
        }
    }

    public class UserDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User'];
        Double random = Math.random();

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    User.FirstName => 'Bruce',
                    User.LastName => 'Banner',
                    User.ProfileId => p.Id,
                    User.Alias => 'Hulk',
                    User.Email => 'bruce' +  random + '@banner.com.test',
                    User.UserName => 'bruce' + random + '@banner.com.unittest',
                    User.EmailEncodingKey => 'UTF-8',
                    User.LanguageLocaleKey => 'en_US',
                    User.LocaleSidKey => 'en_US',
                    User.TimeZoneSidKey => 'America/Denver'
            };
        }
    }

}