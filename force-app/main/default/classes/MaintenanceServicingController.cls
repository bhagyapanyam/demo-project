/*----------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A controller class for Log Work Order
Test Class : TestMaintenanceServicingController
History    :
            03.24.15 rbo Added a validation for Cause making it mandatory when the Part is defined
            04.24.15 rbo Added a control to only allow creation of one log per vendor per work order
            04.27.15 rbo Technical Debt fixes
                     deleted all unnecessary commentations
                     declared a constant NONE_ITEM and replaced '--None--' with NONE_ITEM
                     fixed repeating literal values
                     apply optimizations that generally for best practices
            08.19.15 rbo Added code to accommodate Activity Tracking
            08.24.15 rbo Added rollup fields in query of Maintenance WO record to fix the null issue
            09.01.15 rbo Removed references to rollup fields referencing Well Site Construction
            09.18.15 ssd Added flag to show Parts, Damages and Causes section based on Catalogue Code for Equipment
            01.27.16 ssd Made modifications necessary to move Skip Parts Damage cause logic away from SNR                                            
------------------------------------------------------------------------------------------------------*/
public with sharing class MaintenanceServicingController
{                                       
    private final static String VENDOR_COMPLETE_ACTIVITY_STATUS = 'Complete';
    private final static String NONE_ITEM = '--None--';
    private DateTime vendorActivityModifiedStamp;
    private MaintenanceServicingUtilities.WorkOrderServiceStatus workOrderServiceStatus = new MaintenanceServicingUtilities.WorkOrderServiceStatus(); 
    
    // rbo 04.27.15 added private scoping         
    @TestVisible private Id maintenanceServicingFormId;
    @TestVisible private String searchWorkOrder;
    //
    
    private Map<String, String> mapPartCode = new Map<String, String>();
    private Map<String, String> mapUsers = new Map<String, String>();
    private String vendorComments;
    private String pageURL;
    
    // rbo 04.27.15 added private scoping         
    private Contact[] userContactAccountInfo;
    private Attachment[] maintenanceAttachments;
    
    private List<SelectOption> partsOptions = new List<SelectOption>();
    private List<SelectOption> damagesOptions = new List<SelectOption>();
    private List<SelectOption> causesOptions = new List<SelectOption>();
    private List<SelectOption> usersOptions = new List<SelectOption>();
    //
    
    public HOG_Maintenance_Servicing_Form__c[] maintenanceWorkOrders {get; set;}
    public HOG_Maintenance_Servicing_Vendor__c[] maintenanceVendors {get; set;}
    public String partItem {get; set;}
    public String damageItem {get; set;}
    public String causeItem {get; set;}    
    public String userItem {get; set;}    
    public String vendorName {get; set;}
    public Attachment document {get; set;}
    public String selectedAttachmentId { get; set; }   
    public Boolean recordIsLocked {get; set;}
    public Boolean searchInvalid {get; set;}
    public Boolean showPartsDamageCause {get; set;}

                            
    public MaintenanceServicingController()
    {
                           
        maintenanceWorkOrders = getMaintenanceWorkOrders();   
        
        // rbo 04.27.15 changed from userContactAccountInfo() to getUserContactAccountInfo()
        userContactAccountInfo = getUserContactAccountInfo();
                
        vendorName = 
            userContactAccountInfo != null && !userContactAccountInfo.isEmpty()
            ? userContactAccountInfo[0].Account.Name
            : null;

        pageURL = Apexpages.currentPage().getUrl();
                            
        usersSelection();                  
    }
    
    // public Getter to list attachments
    public Attachment[] getMaintenanceAttachments()
    {            
        // only show attachments that are owned by the vendor
        final Set<String> ownerIds = new Set<String>();

        for (String fieldId : mapUsers.keySet())
            ownerIds.add(fieldId);    
                
        return
            [
                Select 
                    Id,
                    Name,
                    ContentType,
                    OwnerId,
                    ParentId,
                    CreatedDate,
                    CreatedById
                From Attachment
                Where ParentId = :maintenanceServicingFormId
                    And OwnerId In :ownerIds
                Order By CreatedDate Desc];
    }
        
    public PageReference uploadFile()  
    {                      
        document.ParentId = maintenanceServicingFormId;
        document.OwnerId = UserInfo.getUserId();

        try 
        {
            insert document;
        } 
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        }
        finally 
        {
            document.body = null; // clears the viewstate
            document = new Attachment();
        }    

        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully')); 
                             
        return null; 
    }

    public PageReference removeFile()
    {
        // if for any reason we are missing the reference
        if (selectedAttachmentId == null)
        {
            return null;
        }
        
        final List<Attachment> selectedAttachment = [Select Id, OwnerId From Attachment Where Id = :selectedAttachmentId];
         
        //if (selectedAttachment.size() > 0)
        if (!selectedAttachment.isEmpty())
        {
            if (mapUsers.get(selectedAttachment[0].OwnerId) == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'You do not have access to delete this file'));
            }
            else
            {
                try
                {
                    delete selectedAttachment;
                } 
                catch (DMLException e) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
                    return null;
                }
                finally 
                {
                    maintenanceAttachments = getMaintenanceAttachments();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File deleted successfully')); 
                }
            }    
        }
            
        return null;
    }
                            
    public String getSearchWorkOrder() 
    {
        return searchWorkOrder;
    }
    
    public void setSearchWorkOrder(String workOrder) 
    {
        searchWorkOrder = workOrder;
    }
    
    public List<SelectOption> getPartsOptions()
    {
        return partsOptions;
    }

    public List<SelectOption> getDamagesOptions()
    {
        return damagesOptions;
    }

    public List<SelectOption> getCausesOptions()
    {
        return causesOptions;
    }

    public List<SelectOption> getUsersOptions()
    {
        return usersOptions;
    }
        
    // Builds picklist of parts   
    public void partsSelection()
    {
        partsOptions.clear();

        String catalogueCode = '';
        String partCode = NONE_ITEM;
        String partDescription = NONE_ITEM;
        
        if (maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty())
        {
            catalogueCode = maintenanceWorkOrders[0].Equipment__r.Catalogue_Code__c;            
            partCode = maintenanceWorkOrders[0].Part__r.Part_Code__c;
            partDescription = maintenanceWorkOrders[0].Part__r.Part_Description__c;
        }
                    
        final HOG_Part__c[] results = 
            [Select
                    Id,
                    Part_Code__c,
                    Part_Description__c
                From HOG_Part__c 
                Where Catalogue_Code__c = :catalogueCode
            ];
                
        mapPartCode.clear();

        final Map<String, String> mapResults = new Map<String, String>();
        
        for (HOG_Part__c r : results)
        {
            mapResults.put(r.Part_Code__c, r.Part_Description__c);
            mapPartCode.put(r.Part_Code__c, r.Id);           
        }
                                          
        partItem = 
            selectionOption
            (
                partsOptions, 
                mapResults, 
                partCode,
                partDescription,
                true              
            );
                
        damagesSelection();      
    }

    // Builds picklist of damages   
    public void damagesSelection()
    {
        damagesOptions.clear();
                    
        final HOG_Damage__c[] results = 
            [Select
                    Id,
                    Damage_Code__c,
                    Damage_Description__c
                From HOG_Damage__c 
                Where Part_Code__c = :partItem
            ];
                
        final Map<String, String> mapResults = new Map<String, String>();
        
        for (HOG_Damage__c r : results)
        {
            mapResults.put(r.Id, r.Damage_Description__c);
        }
                    
        damageItem = 
            selectionOption
            (
                damagesOptions, 
                mapResults, 
                maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty() 
                    ?  maintenanceWorkOrders[0].Damage__c 
                    : NONE_ITEM,
                maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty() 
                    ?  maintenanceWorkOrders[0].Damage__r.Damage_Description__c
                    : NONE_ITEM,
                true
            );                
    }

    // Builds picklist of causes
    public void causesSelection()
    {
        causesOptions.clear();
                    
        final HOG_Cause__c[] results = 
            [Select
                    Id,
                    Cause_Code__c,
                    Cause_Description__c
                From HOG_Cause__c 
            ];
        
        final Map<String, String> mapResults = new Map<String, String>();
        
        for (HOG_Cause__c r : results)
        {
            mapResults.put(r.Id, r.Cause_Description__c);
        }           
                                                 
        causeItem = 
            selectionOption
            (
                causesOptions, 
                mapResults, 
                maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty() 
                    ?  maintenanceWorkOrders[0].Cause__c 
                    : NONE_ITEM,
                maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty() 
                    ?  maintenanceWorkOrders[0].Cause__r.Cause_Description__c
                    : NONE_ITEM,
                true              
            );                                       
    }

    // Builds picklist of users
    public void usersSelection()
    {
        usersOptions.clear();
                            
        final Id userAccountId =
            userContactAccountInfo != null && !userContactAccountInfo.isEmpty()
            ? userContactAccountInfo[0].AccountId
            : null;
            
        final Account[] results =
            [
                Select 
                    (Select Id, Name, User__r.Id From Contacts Where User__c <> null Order By Name Limit 990)
                From Account
                Where Id = :userAccountId
            ];
            
        mapUsers.clear();
               
        String value = NONE_ITEM;
        String label = NONE_ITEM;

        final Map<String, String> mapContacts = new Map<String, String>();

        for (Account r : results)
        {
            for (Contact c : r.Contacts)
            {            
                if (c.Id != null) mapContacts.put(c.Id, c.Name);
                if (c.User__r.Id != null) mapUsers.put(c.User__r.Id, c.Name);
            }                
        }
                    
        if (maintenanceVendors != null && !maintenanceVendors.isEmpty() && maintenanceVendors[0].Completed_By_Contact__c != null)
        {
            value = maintenanceVendors[0].Completed_By_Contact__c;
            label = maintenanceVendors[0].Completed_By_Contact__r.Name;
        }        
                            
        userItem = 
            selectionOption
            (
                usersOptions, 
                mapContacts, 
                value,
                label,
                true
            );  
    }

    // Create a picklist selection of items
    // Can be moved to the Utilities class
    private String selectionOption
    (
        List<SelectOption> optionList, // the picklist
        Map<String, String> mapItems, // the items in picklist
        String selectedId, // the selected item value
        String selectedName, // the selected item label
        Boolean selectPickNone // to select the "--None--" in Add mode
    )
    {                
        final String pickNone = NONE_ITEM;
        String selectedValue = selectedId;
        
        optionList.add(new SelectOption(pickNone, pickNone));

        for(String key : mapItems.keySet())
        {
            selectedValue = key;
            optionList.add(new SelectOption(selectedValue, mapItems.get(key)));            
        }
                    
        if (selectedId != pickNone)
        {
            selectedValue = selectedId;
        }
                           
        // sort the items
        sortSelectOption(optionList);
                      
        return selectPickNone ? selectedId : selectedValue;
    }

    // Sorts items/labels in SelectOption
    private void sortSelectOption(List<Selectoption> opts) 
    {        
        final Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        
        // Suffix to avoid duplicate values like same labels or values are in inbound list 
        Integer suffix = 1;
        for (Selectoption opt : opts)
        { 
            mapping.put(opt.getLabel() + suffix++, opt);  
        }
                                    
        final List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        
        // clear the original collection to rebuild it
        opts.clear();
        
        for (String key : sortKeys)
        { 
            opts.add(mapping.get(key));
        }
    }
    
    // public Getter to list maintenance work orders
    public HOG_Maintenance_Servicing_Form__c[] getMaintenanceWorkOrders() 
    {                         
        return
          [
                Select 
                    Id,
                    Service_Status__c,
                    Equipment__c,
                    Equipment__r.Name,
                    Equipment__r.Catalogue_Code__c,
                    Part__c,
                    Part__r.Part_Code__c,
                    Part__r.Part_Description__c,
                    Damage__c,
                    Damage__r.Damage_Code__c,
                    Damage__r.Damage_Description__c,
                    Damage_Text__c,
                    Cause__c,
                    Cause__r.Cause_Code__c,
                    Cause__r.Cause_Description__c,
                    Cause_Text__c,
                    Work_Details__c,
                    ALT_Confirmed__c,
                    TECO__c,
                    DLFL__c,
                    CLSD__c,
                    Requested_Start_Time__c,
                    Requested_Stop_Time__c,
                    Main_Work_Centre__c,
                    Operating_Field_AMU_Lookup__c,
                    Operator_Route__c,
                    Facility__r.Name,
                    Location__r.name,
                    Planner_Group__c,
                    Priority_Number__c,
                    Notification_Priority_Number__c,
                    Work_Order_Priority_Number__c,
                    Required_End_Date__c,
                    SystemModstamp,
                    Maintenance_Servicing_Vendors_Count__c,
                    // WSC_Count__c, rbo 09.01.15
                    // WSC_Incomplete_Service_Status_Count__c, rbo 09.01.15
                    VTT_Activities_Count__c,
                    VTT_Activities_Completed_Count__c, // rbo 08.24.15
                    VTT_Activities_New_Count__c,       // rbo 08.24.15
                    Incomplete_Activity_Status_Count__c,
                    MAT_Code__c,//ssd 01.27.16
                    Order_Type__c,//ssd 01.27.16
                    Gas_Permit__c,//mz 5.11.16
                    Electrical_Permit__c//mz 5.11.16                                       
                From HOG_Maintenance_Servicing_Form__c
                Where Work_Order_Number__c = :searchWorkOrder And Work_Order_Number__c != null
                Limit 1
            ];                                              
    }

    // rbo 04.27.15 changed from userContactAccountInfo() to getUserContactAccountInfo()
    private Contact[] getUserContactAccountInfo() 
    { 
        // only returns the first account that the logged in user is associated with
        // ordered by Account.Name ascending 
        return 
            [
                Select 
                    AccountId,
                    Account.Name                     
                From Contact                
                Where User__c = :Userinfo.getUserId()
                Order By Account.Name
                Limit 1
            ];   
    }

    // public Getter to list maintenance vendors
    public HOG_Maintenance_Servicing_Vendor__c[] getMaintenanceVendors() 
    {             
        final Id vendorId = 
            userContactAccountInfo != null && !userContactAccountInfo.isEmpty()
            ? userContactAccountInfo[0].AccountId
            : null;
                            
        final HOG_Maintenance_Servicing_Vendor__c[] vendorRecord =
            [
                Select 
                    Id,
                    Name,
                    Maintenance_Servicing_Form__c,
                    Vendor__c,
                    Completed_By_Contact__c,
                    Completed_By_Contact__r.Name,
                    Start_Time__c,
                    Stop_Time__c,
                    Total_Personnel__c,
                    Total_Travel_Times_Hrs__c,
                    Comments__c,
                    Activity_Status__c,
                    Activity_Status_Reason__c,
                    SystemModstamp
                From HOG_Maintenance_Servicing_Vendor__c                
                Where Maintenance_Servicing_Form__c = :maintenanceServicingFormId 
                    And Vendor__c = :vendorId
                Limit 1
            ];                  

        if (vendorRecord.isEmpty())
        {
            vendorRecord.add(new HOG_Maintenance_Servicing_Vendor__c());
        }
        
        vendorComments = vendorRecord[0].Comments__c;

        return vendorRecord;                            
    }
    
    public PageReference doSearch() 
    {              
        if (searchWorkOrder == null)
        {
            return null;
        }
        
        maintenanceWorkOrders = getMaintenanceWorkOrders();
        
        maintenanceServicingFormId = 
            maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty() 
            ? maintenanceWorkOrders[0].Id
            : null;
                    
        recordIsLocked = true;
        searchInvalid = true;
        showPartsDamageCause = true;
        
        if (maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty())
        {                        
            recordIsLocked = maintenanceWorkOrders[0].ALT_Confirmed__c || maintenanceWorkOrders[0].TECO__c || 
                maintenanceWorkOrders[0].DLFL__c || maintenanceWorkOrders[0].CLSD__c;
            
            searchInvalid = false;

            showPartsDamageCause = !MaintenanceServicingUtilities.SkipPartsDamageCause(maintenanceWorkOrders[0]) &&
                !MaintenanceServicingUtilities.SkipPartsDamageCauseSysAdmin(maintenanceWorkOrders[0]);
            
            maintenanceVendors = getMaintenanceVendors(); 
            vendorActivityModifiedStamp = maintenanceVendors[0].SystemModstamp;
                                                 
            maintenanceAttachments = getMaintenanceAttachments();
        
            partsSelection();
            causesSelection();
            usersSelection();
    
            document = new Attachment();
        }
            
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please fill in mandatory fields'));
        
        return null;
    }
        
    public pageReference save() 
    {       
        if (maintenanceWorkOrders != null && !maintenanceWorkOrders.isEmpty())
        {
            //if (vendorName == null)
            if (String.isBlank(vendorName))
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'You do not have an account to assign a work order'));
                return null;
            }

            if (maintenanceVendors[0].Total_Personnel__c <= 0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Total Personnel: Invalid value'));
                return null;
            }

            if (maintenanceVendors[0].Total_Travel_Times_Hrs__c < 0 || maintenanceVendors[0].Total_Travel_Times_Hrs__c > 12)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Total Travel Times (hrs): Invalid value'));
                return null;
            }

            If (maintenanceVendors[0].Start_Time__c != null && maintenanceVendors[0].Start_Time__c > System.now())
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'On Site Time: Value must not be in the future'));   
                return null;
            }

            If (maintenanceVendors[0].Stop_Time__c != null && maintenanceVendors[0].Stop_Time__c > System.now())
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Off Site Time: Value must not be in the future'));
                return null;
            }
                   
            if (maintenanceVendors[0].Stop_Time__c != null && maintenanceVendors[0].Start_Time__c == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'On Site Time: Value must not be empty'));
                return null;
            }

            if (maintenanceVendors[0].Stop_Time__c != null && maintenanceVendors[0].Stop_Time__c < maintenanceVendors[0].Start_Time__c)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Off Site Time: Value must be greater than or equal to On Site Time'));
                return null;
            }

            if (maintenanceVendors[0].Start_Time__c > System.now())
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'On Site Time: You must have a date that is not in the future'));
                return null;
            }

            if (maintenanceVendors[0].Stop_Time__c > System.now())
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Off Site Time: You must have a date that is not in the future'));
                return null;
            }

            if (userItem == NONE_ITEM)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Completed By: Invalid value'));
                return null;
            }
            
            if (partItem == NONE_ITEM && maintenanceWorkOrders[0].Equipment__c != null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Part: You must select valid equipment parts'));
                return null;
            }
                                    
            if (damageItem == NONE_ITEM && maintenanceWorkOrders[0].Equipment__c != null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Damage: You must select a valid damage on parts'));
                return null;
            }

            if (causeItem == NONE_ITEM && maintenanceWorkOrders[0].Equipment__c != null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Cause: You must select a valid cause of damage'));
                return null;
            }

            maintenanceWorkOrders[0].Part__c = partItem == NONE_ITEM ? null : mapPartCode.get(partItem);
            maintenanceWorkOrders[0].Damage__c = damageItem == NONE_ITEM ? null : damageItem;
            maintenanceWorkOrders[0].Cause__c = causeItem == NONE_ITEM ? null : causeItem;
                                    
            if (vendorComments != maintenanceVendors[0].Comments__c)            
                maintenanceWorkOrders[0].Work_Details__c = 
                    maintenanceVendors[0].Comments__c != null 
                    ? (maintenanceWorkOrders[0].Work_Details__c != null 
                        ? maintenanceWorkOrders[0].Work_Details__c : '')
                        + '\n\n' 
                        + String.valueOf(System.now())
                        + ' ' 
                        + Userinfo.getName()
                        + ' - '
                        + userContactAccountInfo[0].Account.Name 
                        + '\n' 
                        + maintenanceVendors[0].Comments__c 
                    : maintenanceWorkOrders[0].Work_Details__c;
                            
            maintenanceVendors[0].Maintenance_Servicing_Form__c = maintenanceServicingFormId;
            maintenanceVendors[0].Vendor__c = userContactAccountInfo[0].AccountId;
            maintenanceVendors[0].Name = userContactAccountInfo[0].Account.Name;
            maintenanceVendors[0].Completed_By_Contact__c = userItem == NONE_ITEM ? null : userItem;

            if (maintenanceVendors[0].Activity_Status__c != VENDOR_COMPLETE_ACTIVITY_STATUS)
            {
                maintenanceWorkOrders[0].Service_Status__c = workOrderServiceStatus.AWAITING_ON_VENDORS;
            }
            else
            {
                // validate completetion against other vendor records if any                
                final Id vendorId = 
                    userContactAccountInfo != null && !userContactAccountInfo.isEmpty()
                    ? userContactAccountInfo[0].AccountId
                    : null;                
        
                final HOG_Maintenance_Servicing_Vendor__c[] vendorRecords =
                    [
                        Select 
                            Activity_Status__c
                        From HOG_Maintenance_Servicing_Vendor__c                
                        Where Maintenance_Servicing_Form__c = :maintenanceServicingFormId
                            And Vendor__c != :vendorId
                            And Activity_Status__c != :VENDOR_COMPLETE_ACTIVITY_STATUS
                    ];

                /*
                maintenanceWorkOrders[0].Service_Status__c = 
                    vendorRecords != null && !vendorRecords.isEmpty()
                    ? workOrderServiceStatus.AWAITING_ON_VENDORS
                    : workOrderServiceStatus.VENDORS_COMPLETE;
                */
                      
                /* SM 8/25/2015
                maintenanceWorkOrders[0].Service_Status__c = 
                    (vendorRecords != null && !vendorRecords.isEmpty())
                    || MaintenanceServicingUtilities.IsActivityStatusInComplete(maintenanceWorkOrders[0])                    
                    ? workOrderServiceStatus.AWAITING_ON_VENDORS
                    : workOrderServiceStatus.VENDORS_COMPLETE;     
                */
//----------------------SM---------------------------------------
                // convert null values to 0s
                Integer incompleteActivityStatusCount = 
                    maintenanceWorkOrders[0].Incomplete_Activity_Status_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].Incomplete_Activity_Status_Count__c;

                // rbo 09.01.15
                //Integer wscIncompleteServiceStatusCount = 
                //    maintenanceWorkOrders[0].WSC_Incomplete_Service_Status_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].WSC_Incomplete_Service_Status_Count__c;
                // rbo 09.01.15

                Integer vttActivitiesNewCount = 
                    maintenanceWorkOrders[0].VTT_Activities_New_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].VTT_Activities_New_Count__c;
                Integer vttActivitiesCompletedCount = 
                    maintenanceWorkOrders[0].VTT_Activities_Completed_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].VTT_Activities_Completed_Count__c;
                Integer vttActivitiesCount = 
                    maintenanceWorkOrders[0].VTT_Activities_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].VTT_Activities_Count__c;
                Integer maintenanceServicingVendorsCount = 
                    maintenanceWorkOrders[0].Maintenance_Servicing_Vendors_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].Maintenance_Servicing_Vendors_Count__c;

                // rbo 09.01.15
                //Integer wscCount = 
                //    maintenanceWorkOrders[0].WSC_Count__c == null ? 0 : (Integer)maintenanceWorkOrders[0].WSC_Count__c;
                // rbo 09.01.15

                System.debug('incompleteActivityStatusCount: ' + incompleteActivityStatusCount);
                System.debug('vttActivitiesNewCount: ' + vttActivitiesNewCount);
                System.debug('record.VTT_Activities_New_Count__c: ' + vttActivitiesNewCount);
                System.debug('vttActivitiesCompletedCount: ' + vttActivitiesCompletedCount);
                System.debug('vttActivitiesCount: ' + vttActivitiesCount);
                System.debug('maintenanceServicingVendorsCount: ' + maintenanceServicingVendorsCount);

                Boolean workOrderIncomplete = false;
                
                //in case if we have well site constraction incomplete service records
                // rbo 09.01.15
                /*
                if(wscIncompleteServiceStatusCount > 0)
                {
                    workOrderIncomplete = true;
                }
                */
                
                //in case if there are activities with status 'New' and some activities with different status
                if(vttActivitiesNewCount > 0 && vttActivitiesNewCount <> vttActivitiesCount)
                {
                    workOrderIncomplete = true;
                }
                //in case if there are no activities with status 'New' and not all activities are completed
                if(vttActivitiesNewCount == 0 && vttActivitiesCount > vttActivitiesCompletedCount)
                {
                    workOrderIncomplete = true;
                }
                maintenanceWorkOrders[0].Service_Status__c = 
                    (vendorRecords != null && !vendorRecords.isEmpty())
                    || workOrderIncomplete                    
                    ? workOrderServiceStatus.AWAITING_ON_VENDORS
                    : workOrderServiceStatus.VENDORS_COMPLETE;   
//----------------------SM---------------------------------------
            }
            
            // validate against the latest work order status, ALT_CONFIRMED/TECO
            final HOG_Maintenance_Servicing_Form__c[] workOrderRecordStatus = getMaintenanceWorkOrders();
            if (workOrderRecordStatus[0].ALT_Confirmed__c)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Save: This work order has already been locked (ALT Confirmed)'));
                return null;            
            }

            if (workOrderRecordStatus[0].TECO__c)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Save: This work order has already been locked (TECO)'));
                return null;            
            }

            if (workOrderRecordStatus[0].DLFL__c)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Save: This work order has already been locked (Deleted)'));
                return null;            
            }

            if (workOrderRecordStatus[0].CLSD__c)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Save: This work order has already been locked (Closed)'));
                return null;            
            }

            // rbo 04.24.15
            // make sure that there is only one vendor activity per work order
            final HOG_Maintenance_Servicing_Vendor__c[] vendorLogRecord = getMaintenanceVendors();
            if (vendorLogRecord[0].SystemModStamp != vendorActivityModifiedStamp)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Save: Unable to save record, this log has recently been modified by someone, click "Search" to refresh'));
                return null;                
            }
            //
            
            final Savepoint savePoint = Database.setSavepoint();
                                                    
            try
            {                    
                // do not execute triggers
                MaintenanceServicingUtilities.executeTriggerCode = false;
                
                upsert maintenanceWorkOrders;
                upsert maintenanceVendors;
            }
            catch (Exception e)
            {
                Database.rollback(savePoint);                        
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, e.getMessage()));                                    
                return null;
            }
    
            return vendorMaintenancePage();
        }
        
        return null;    
    }
    
    public pageReference cancel() 
    {
        return vendorMaintenancePage();
    }
    
    private pageReference vendorMaintenancePage()
    {        
        final String sitePrefix = Site.getPrefix();        
        final String hostVal = ApexPages.currentPage().getHeaders().get('Host') + (sitePrefix == null ? '' : sitePrefix);
        
        final PageReference pageRef = new PageReference('https://' + hostVal + pageURL);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
}