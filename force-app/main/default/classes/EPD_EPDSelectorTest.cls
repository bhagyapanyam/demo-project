/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EPDSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EPDSelectorTest {

    @IsTest
    static void getEnginePerformanceDataByWOAId_withoutParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEnginePerformanceDataByWOAId(null);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEnginePerformanceDataByWOAId_withWrongParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEnginePerformanceDataByWOAId(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEnginePerformanceDataByWOAId_withProperParam() {
        Integer expectedCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEnginePerformanceDataByWOAId(epd.Work_Order_Activity__c);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecords_withoutParam() {
        Boolean failed = false;
        Boolean expectedFailedFlag = true;
        List<EPD_Engine_Performance_Data__c> records;

        Test.startTest();
        try{
            records = new EPD_EPDSelector().getEPDRecords(null);
        } catch (Exception ex) {
            failed = true;
        }
        Test.stopTest();

        System.assertEquals(null, records);
        System.assertEquals(expectedFailedFlag, failed);
    }

    @IsTest
    static void getEPDRecords_withEmptyParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecords(new Set<Id>());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecords_withWrongParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecords( new Set<Id> { UserInfo.getUserId() } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecords_withProperParam() {
        Integer expectedCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecords( new Set<Id> { epd.Id } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecordsByWorkOrderIds_withoutParam() {
        Boolean failed = false;
        Boolean expectedFailedFlag = true;
        List<EPD_Engine_Performance_Data__c> records;

        Test.startTest();
        try {
            records = new EPD_EPDSelector().getEPDRecordsByWorkOrderIds(null);
        } catch (Exception ex) {
            failed = true;
        }
        Test.stopTest();

        System.assertEquals(null, records);
        System.assertEquals(expectedFailedFlag, failed);
    }

    @IsTest
    static void getEPDRecordsByWorkOrderIds_withEmptyParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecordsByWorkOrderIds(new Set<Id>());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecordsByWorkOrderIds_withWrongParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecordsByWorkOrderIds( new Set<Id> { UserInfo.getUserId() } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getEPDRecordsByWorkOrderIds_withProperParam() {
        Integer expectedCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getEPDRecordsByWorkOrderIds( new Set<Id> { epd.Work_Order__c } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getCountOfSubmittedRecords_withoutParam() {
        Integer expectedCount = 0;

        Test.startTest();
        Integer submittedRecords = new EPD_EPDSelector().getCountOfSubmittedRecords(null);
        Test.stopTest();

        System.assertEquals(expectedCount, submittedRecords);
    }

    @IsTest
    static void getCountOfSubmittedRecords_withWrongParam() {
        Integer expectedCount = 0;

        Test.startTest();
        Integer submittedRecords = new EPD_EPDSelector().getCountOfSubmittedRecords(UserInfo.getUserId());
        Test.stopTest();

        System.assertEquals(expectedCount, submittedRecords);
    }

    @IsTest
    static void getCountOfSubmittedRecords_withProperParam_0() {
        Integer expectedCount = 0;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        epd.Submitted__c = false;
        update epd;

        Test.startTest();
        Integer submittedRecords = new EPD_EPDSelector().getCountOfSubmittedRecords(epd.Work_Order_Activity__c);
        Test.stopTest();

        System.assertEquals(expectedCount, submittedRecords);
    }

    @IsTest
    static void getCountOfSubmittedRecords_withProperParam_1() {
        Integer expectedCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        Integer submittedRecords = new EPD_EPDSelector().getCountOfSubmittedRecords(epd.Work_Order_Activity__c);
        Test.stopTest();

        System.assertEquals(expectedCount, submittedRecords);
    }

    @IsTest
    static void getPreviousEPDByEngineId_withoutParam() {
        Boolean failed = false;
        Boolean expectedFailedFlag = false;
        List<EPD_Engine_Performance_Data__c> records;

        Test.startTest();
        try {
            records = new EPD_EPDSelector().getPreviousEPDByEngineId(null);
        } catch (Exception ex) {
            failed = true;
        }
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(new List<EPD_Engine_Performance_Data__c>(), records);
        System.assertEquals(expectedFailedFlag, failed);
    }

    @IsTest
    static void getPreviousEPDByEngineId_withWrongParam() {
        Integer expectedCount = 0;

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getPreviousEPDByEngineId(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

    @IsTest
    static void getPreviousEPDByEngineId_withProperParam() {
        Integer expectedCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<EPD_Engine_Performance_Data__c> records = new EPD_EPDSelector().getPreviousEPDByEngineId(epd.Equipment_Engine__c);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedCount, records.size());
    }

}