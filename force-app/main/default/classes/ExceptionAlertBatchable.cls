global with sharing class ExceptionAlertBatchable implements Database.Batchable<sObject> {
    private String query;

    public ExceptionAlertBatchable(String query) {
        this.query = query;
    }

    global Database.Querylocator start(Database.BatchableContext context) {
        return Database.getQuerylocator(this.query);
    }

    global void execute(Database.BatchableContext context, List<sObject> errorLogs) {
        alertFailures(errorLogs);
    }

    global void finish(Database.BatchableContext context){

    }

    private void alertFailures(List<sObject> errorLogs) {
        Map<String, List<Error_Log__c>> errorLogMap = new Map<String, List<Error_Log__c>>();
        
        if(errorLogs.isEmpty()) {
            return;
        }

        for(sObject errorLog : errorLogs) {
            Error_Log__c uscpError = (Error_Log__c )errorLog;

            if(errorLogMap.containsKey(uscpError.Apex_Class_Trigger__c)) {
                List<Error_Log__c> errorList = errorLogMap.get(uscpError.Apex_Class_Trigger__c);
                errorList.add(uscpError);
                errorLogMap.put(uscpError.Apex_Class_Trigger__c, errorList);

            } else {
                List<Error_Log__c> errorList = new List<Error_Log__c>();
                errorList.add(uscpError);
                errorLogMap.put(uscpError.Apex_Class_Trigger__c, errorList);                
            }
        }

        composeFailureEmails(errorLogMap);
    }

    private void composeFailureEmails(Map<String, List<Error_Log__c>> errorLogMap) {
        for(String interfaceName : errorLogMap.keySet()) {
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'LIST-IS-ACN-Salesforce-Support@huskyenergy.com'}; 

            OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'list-is-acn-salesforce-support@huskyenergy.com'];
            if (owea.size() > 0) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }                

            mail.setToAddresses(toAddresses);
            mail.setSubject(interfaceName + ' Errors on ' + System.today().format());
            mail.setHtmlBody(getErrorsAsTable(errorLogMap.get(interfaceName)));

            if(!Test.isRunningTest()) {
                List<Messaging.SendEmailResult> sendResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

                if(sendResults[0].success) {
                    updateErrorLogEntries(errorLogMap.get(interfaceName));
                }
            }
        }
    }

    private void updateErrorLogEntries(List<Error_Log__c> errorLogEntries) {
        for(Error_Log__c errorLogEntry : errorLogEntries) {
            errorLogEntry.Status__c = 'Emailed';
        }

        Database.update(errorLogEntries);
    }

    private String getErrorsAsTable(List<Error_Log__c> errorLogEntries) {
        String htmlTable = '<table width="80%" border="1"><tbody>';

        //Table header 
        htmlTable += '<tr>';
        htmlTable += '<th width="10%">Error Log Name</th>';
        htmlTable += '<th width="10%">Source Interface</th>';
        htmlTable += '<th width="15%">Log Date</th>';
        htmlTable += '<th width="65%">Error Message</th>';
        htmlTable += '</tr>';

        //Table rows
        for(Error_Log__c errorLogEntry : errorLogEntries) {
            htmlTable += '<tr>';
            htmlTable += '<td>' + errorLogEntry.Name + '</td>';
            htmlTable += '<td>' + errorLogEntry.Apex_Class_Trigger__c + '</td>';
            htmlTable += '<td>' + errorLogEntry.Log_Date__c + '</td>';
            htmlTable += '<td>' + errorLogEntry.Description__c + '</td>';
            htmlTable += '</tr>';
        }

        htmlTable += '</tbody></table>';

        return htmlTable;
    }

}