@IsTest 
public with sharing class WeldingTrackerForgotPasswordTest {     
	
    @IsTest(SeeAllData=true) 
    public static void testForgotPasswordController() {
       
        // Instantiate a new controller with all parameters in the page
        WeldingTrackerForgotPasswordController controller = new WeldingTrackerForgotPasswordController();
        controller.username = 'test@salesforce.com';

        ApexPages.CurrentPage().getParameters().put('hasError','true');  

        System.assertEquals(controller.forgotPassword(),null); 
    }
}