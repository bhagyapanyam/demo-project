/*------------------------------------------------------------------________-------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for ServiceRequestNotificationUtilities
History:     10.10.2017 - m.z. added test for pre-population of operator details (TestOperatorDetailsPopulation)
             10-april-18  mp:  added Title__c field to be included in the createServiceRequestNotificationForm method (as 'Test Title',)
             12-Jul-2018 mz: Updated test for 2Z FIX, ++ coverage for getFLOCs
----------------------------------------------------------------------------------------------------------*/ 
@isTest
private class ServiceRequestNotificationUtilitiesTest
{
    static List<HOG_Service_Code_MAT__c> serviceCodeMAT;
    static List<HOG_Work_Order_Type__c> workOrderType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
	static List<Well_Tracker__c> wellTracker;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Notification_Type__c notificationType;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static Business_Unit__c businessUnit;
	static Business_Department__c businessDepartment;
	static Operating_District__c operatingDistrict;
	static Field__c field;
	static Route__c route;
    static Route__c route2;
	static Location__c location;
	static Location__c location2;
	static Facility__c facility;		
	static Account account;	
	static User operator1;
    static User operator2;
	static User operator3;
    static User operator4;
    static User operator5;
    static User operator6;
	static Contact contact1;
	static Contact contact2;
	static Contact contact3;
    static Contact contact4;
    static Contact contact5;
    static Contact contact6;
	static HOG_Service_Request_Notification_Form__c serviceRequest1;
	static HOG_Service_Request_Notification_Form__c serviceRequest2;
	static HOG_Service_Request_Notification_Form__c serviceRequest3;
	static HOG_Settings__c settings;
	static Id recordTypeId;

	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationUtilities_Test()
    {                                        
        //-- Begin setup of data needed to test the Service Request Notification Utilities --//

		SetupTestData();
                                                                                                   
		String mainWorkCentre;
		Date workOrderSAPChangedDate;
		String workOrderSAPChangedBy;
		String workOrderSAPSystemStatus;
		String workOrderSAPUserStatus;
		Id operatingRoute;
		Id operatingField;	
		Id controlCentre;	


        //--- Begin test Location
        Test.startTest();
        
        List<HOG_Service_Request_Notification_Form__c> serviceNotification = new List<HOG_Service_Request_Notification_Form__c>();
        serviceNotification.add(serviceRequest1);

		try
		{       	        
	    	ServiceRequestNotificationUtilities.TriggerBeforeInsertServiceRequestNotificationForm(serviceNotification);
		}
		catch(Exception e)
		{
			System.Assert(e.getMessage().contains('You can not create a notification order record.'));			
		}

		try
		{  
		    ServiceRequestNotificationUtilities.TriggerBeforeUpdateServiceRequestNotificationForm(serviceNotification, new Map<Id, HOG_Service_Request_Notification_Form__c>(serviceNotification)); 
		}
		catch(Exception e)
		{
			System.Assert(e.getMessage().contains('You can not update a notification order record.'));			
		}

		try
		{       	        
		    ServiceRequestNotificationUtilities.TriggerBeforeDeleteServiceRequestNotificationForm(serviceNotification);
		}
		catch(Exception e)
		{
			System.Assert(e.getMessage().contains('You can not delete a notification order record.'));			
		}

    	ServiceRequestNotificationUtilities.Recurrence recurrence = new ServiceRequestNotificationUtilities.Recurrence();
		ServiceRequestNotificationUtilities.NotificationRecordType notificationRecordType = new ServiceRequestNotificationUtilities.NotificationRecordType();
        
		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

		request.record = serviceRequest1;
    	request.workOrderName = 'Test Location';		
    	request.workOrderCount = 1;

		request.notificationType = notificationType.Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;		
		        					
        HOG_Maintenance_Servicing_Form__c riglessServicingForm = ServiceRequestNotificationUtilities.createServiceWorkOrder(request);
        System.AssertEquals(true, riglessServicingForm != null);

    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;
        insert riglessServicingForm;
                              
        //--- Begin test Location
        System.AssertEquals(true, ServiceRequestNotificationUtilities.createServiceWorkOrder(request) != null);
                            
        ServiceRequestNotificationUtilities.readWellTrackerWorkOrderCount(wellTracker[0].Id, true);
        ServiceRequestNotificationUtilities.readWellTrackerWorkOrderCount(wellTracker[0].Id, false);  
        ServiceRequestNotificationUtilities.readWellTrackerServiceCompletedAndRunning(wellTracker[0].Id);
        ServiceRequestNotificationUtilities.readWellTrackerRecent(wellTracker[0].Id, location.Id);
        ServiceRequestNotificationUtilities.readWellTrackerModified(wellTracker[0].Id, DateTime.now());
        ServiceRequestNotificationUtilities.readWorkOrderRecent(wellTracker[0].Id, location.Id);
        ServiceRequestNotificationUtilities.readWorkOrderModified(riglessServicingForm.Id, DateTime.now());
        ServiceRequestNotificationUtilities.readLastControlCentre(location.Id);
        ServiceRequestNotificationUtilities.readServiceRequestModified(serviceRequest1.Id, DateTime.now());
        ServiceRequestNotificationUtilities.readFacilityTrackerRecent(wellTracker[0].Id, facility.Id);
        ServiceRequestNotificationUtilities.readLastControlCentreFacility(facility.Id);
        ServiceRequestNotificationUtilities.readWellTrackerCount(location.Id);
        ServiceRequestNotificationUtilities.readFacilityTrackerCount(facility.Id);
        ServiceRequestNotificationUtilities.readWellTrackerAverageOilProduction(location.Id);
        ServiceRequestNotificationUtilities.readDLFL('Location__c', location.Id);
        ServiceRequestNotificationUtilities.readDLFL('TestError', location.Id);
		ServiceRequestNotificationUtilities.readServiceNotificationRequests(null);
		ServiceRequestNotificationUtilities.readObjectRecordTypeId(notificationRecordType.NOTIFICATIONOBJECTNAME, 'Well ID');        
                                           
        //--- Begin test Facility
		request.record = serviceRequest2;
    	request.workOrderName = 'Test Facility';		
        System.AssertEquals(true, ServiceRequestNotificationUtilities.createServiceWorkOrder(request) != null);
        //--- End test Facility

        Test.stopTest();
        
        System.AssertEquals(ServiceRequestNotificationUtilities.getEquipment('00000E'),null);
        System.AssertEquals(ServiceRequestNotificationUtilities.getFacility('00000F').Id,facility.Id);
        System.AssertEquals(ServiceRequestNotificationUtilities.getLocation('00000L').Id,location.Id);
        System.AssertEquals(ServiceRequestNotificationUtilities.getWellEvent('0000WE'),null);
        System.AssertEquals(ServiceRequestNotificationUtilities.getSystem('00000S'),null);
        System.AssertEquals(ServiceRequestNotificationUtilities.getSubSystem('0000SS'),null);
        System.AssertEquals(ServiceRequestNotificationUtilities.getFunctionalEquipmentLevel('000FEL'),null);
        System.AssertEquals(ServiceRequestNotificationUtilities.getYard('00000Y'),null);
    } 
    
    @isTest (SeeAllData = false) 
    static void TestOperatorDetailsPopulation(){
    
        
        SetupTestData();    

        Test.startTest();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            Profile cro_profile = [SELECT Id FROM Profile WHERE Name='Standard HOG - General User'];
            UserRole cro_role = [SELECT Id FROM UserRole WHERE Name ='Control Room Operator'  Limit 1];
            List<Contact> contacts = [Select Id, Name, User__c
                                      From Contact
                                      Order By Name];
            
            operator1 = UserTestData.createTestUser(cro_profile, cro_role);
            operator1.FirstName = 'Operator';
            operator1.LastName = '1';
            operator1.MobilePhone = '111-111-111';
            insert operator1;
            contacts[0].User__c = operator1.Id;
            
            operator2 = UserTestData.createTestUser(cro_profile, cro_role);
            operator2.FirstName = 'Operator';
            operator2.LastName = '2';
            operator2.MobilePhone = '222-222-222';
            insert operator2;
            contacts[1].User__c = operator2.Id;
            
            operator3 = UserTestData.createTestUser(cro_profile, cro_role);
            operator3.FirstName = 'Operator';
            operator3.LastName = '3';
            insert operator3;
            contacts[2].User__c = operator3.Id;
            
            operator4 = UserTestData.createTestUser(cro_profile, cro_role);
            operator4.FirstName = 'Operator';
            operator4.LastName = '4';
            operator4.MobilePhone = '444-444-444';
            insert operator4;
            contacts[3].User__c = operator4.Id;
            
            operator5 = UserTestData.createTestUser(cro_profile, cro_role);
            operator5.FirstName = 'Operator';
            operator5.LastName = '5';
            operator5.MobilePhone = '555-555-555';
            insert operator5;
            contacts[4].User__c = operator5.Id;
            
            operator6 = UserTestData.createTestUser(cro_profile, cro_role);
            operator6.FirstName = 'Operator';
            operator6.LastName = '6';
            operator6.MobilePhone = '666-666-666';
            insert operator6;

            update contacts;
        }
  
        
        List<HOG_Service_Request_Notification_Form__c> serviceNotification = new List<HOG_Service_Request_Notification_Form__c>();
        serviceNotification.add(serviceRequest3);
        ServiceRequestNotificationUtilities.Recurrence recurrence = new ServiceRequestNotificationUtilities.Recurrence();
        ServiceRequestNotificationUtilities.NotificationRecordType notificationRecordType = new ServiceRequestNotificationUtilities.NotificationRecordType();
        ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 
        
		request.record = serviceRequest3;
    	request.workOrderName = 'Test Location';		
    	request.workOrderCount = 1;
    	request.notificationType = notificationType.Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;	
		
		route2.Operator_1_User__c = operator1.Id;
		route2.Operator_2_User__c = operator2.Id;
		route2.Operator_3_User__c = operator3.Id;
		route2.Operator_4_User__c = operator4.Id;
		route2.Operator_5_User__c = operator5.Id;
		//route2.Operator_6_User__c = operator6.Id;

    	update route2;
		        					
        HOG_Maintenance_Servicing_Form__c riglessServicingForm = ServiceRequestNotificationUtilities.createServiceWorkOrder(request);
        System.AssertEquals(true, riglessServicingForm != null);

    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;

        insert riglessServicingForm;
    
        Test.stopTest(); 
    	
    	System.AssertEquals('Operator 1 | 2Z-11111 | 111-111-111',riglessServicingForm.Operator_1_detail__c);
    	System.AssertEquals('Operator 2 | 2Z-22222 | 222-222-222',riglessServicingForm.Operator_2_detail__c);
    	System.AssertEquals('Operator 3 | 2Z-33333 | N/A',riglessServicingForm.Operator_3_detail__c);
    	System.AssertEquals('Operator 4 | 2Z-44444 | 444-444-444',riglessServicingForm.Operator_4_detail__c);
        System.AssertEquals('Operator 5 | N/A | 555-555-555',riglessServicingForm.Operator_5_detail__c);
        System.AssertEquals('N/A | N/A | N/A',riglessServicingForm.Operator_6_detail__c);
    }
    
    
	private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
        notificationType = NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, false, false, true);
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = new List<HOG_Service_Code_MAT__c>();
        serviceCodeMAT.add((HOG_Service_Code_MAT__c)ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]));
        serviceCodeMAT.add((HOG_Service_Code_MAT__c)ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TS2', WORKORDERRECORDTYPE[1])); 
        insert serviceCodeMAT;        
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
        notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriorityList.Id, false));
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriorityList.Id, true));
        insert notificationTypePriority;        
        //--
                
 	    workOrderType = new List<HOG_Work_Order_Type__c>();
        workOrderType.add((HOG_Work_Order_Type__c)WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[0].Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));        
        workOrderType.add((HOG_Work_Order_Type__c)WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[1].Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));                                
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;   
        
        route2 = RouteTestData.createRoute('111');
        insert route2;     

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);
		location.SAP_Object_ID__c = '00000L';
        insert location;
        
        location2 = LocationTestData.createLocation('Test Location 2', route2.Id, field.Id);		
        insert location2;

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
		facility.SAP_Object_ID__c = '00000F';
        insert facility;        

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;
        
        contact1 = ContactTestData.createContact(account.Id, 'Operator', '1');
        contact2 = ContactTestData.createContact(account.Id, 'Operator', '2');
        contact3 = ContactTestData.createContact(account.Id, 'Operator', '3');
        contact4 = ContactTestData.createContact(account.Id, 'Operator', '4');
        contact5 = ContactTestData.createContact(account.Id, 'Operator', '5');
        contact6 = ContactTestData.createContact(account.Id, 'Operator', '6');
        
        contact1.X2Z_Employee_ID__c = '2Z-11111';
        contact2.X2Z_Employee_ID__c = '2Z-22222';
        contact3.X2Z_Employee_ID__c = '2Z-33333';
        contact4.X2Z_Employee_ID__c = '2Z-44444';
        contact6.X2Z_Employee_ID__c = '2Z-66666';
        
        List<Contact> contactList = new List<Contact>{contact1, contact2, contact3, contact4, contact5, contact6};
        insert contactList;

		wellTracker = new List<Well_Tracker__c>();
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
        insert wellTracker;
        
        
        //--

		ServiceRequestNotificationUtilities.executeTriggerCode = false; 

		serviceRequest1 = ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[0].Id,
				null,
				account.Id,
				'7521256',
				'98765430',
                'Test Title',
				'Test Detail',
				null,
				null
			);
		insert serviceRequest1;	

		serviceRequest2 = ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[1].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[0].Id,
				null,
				account.Id,
				'7521257',
				'98765431',
                'Test Title',
				'Test Detail',
				null,
				null
			);
		insert serviceRequest2;
		
	    serviceRequest3 = ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				location2.Id,
				null,
				notificationTypePriority[0].Id,
				null,
				account.Id,
				'7521258',
				'98765432',
                'Test Title',
				'Test Detail',
				null,
				null
			);
		serviceRequest3.Operator_Route_Lookup__c = route2.Id;
		insert serviceRequest3;	

        //-- End setup of data needed to test the Service Request Notification controller --//

		settings = HOGSettingsTestData.createHOGSettings();
		insert settings;
	}
}