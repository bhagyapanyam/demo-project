/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_TransferForm {
	@AuraEnabled
	public Equipment_Transfer_Form__c transferForm;
	@AuraEnabled
	public Equipment_TransferForm_FLOC_Info flocInfo;
}