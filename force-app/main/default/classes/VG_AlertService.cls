/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class that handles business logic for manipulation with VG Alerts
Test Class:     VG_AlertServiceTest
                VG_AlertServiceTestNP
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public inherited sharing class VG_AlertService {

    private static final String CLASS_NAME = String.valueOf(VG_AlertFileService.class);

    public static LocationDAO locationSelector = new LocationDAOImpl();
    public static HOG_PermissionDAO permissionSelector = new HOG_PermissionDAOImpl();

    /**
     * Checks if User is valid to be able to create Vent Gas Alert with Type of Engineering Notification.
     * Only System Admin user or user with correct permission is allowed to create Alerts.
     *
     * @return Boolean
     */
    public Boolean isValidUserForEngNotifAlert(){
        return HOG_VentGas_Utilities.getIsAdmin()
                || hasCorrectAlertCreatePermissions();
    }

    /**
     * Runs validation on Location to check if it is valid location to create Vent Gas Alert with Type of Engineering
     * Notification.
     * If not, HOG_Exception is thrown
     *
     * @param wellId
     *
     * @return Boolean
     */
    public Boolean isValidLocation(Id wellId) {

        Boolean isValid = new HOG_VG_EngNotifStrategyProvider()
                .shouldCreate(
                        extractLocationFromList(
                                locationSelector.getLocationById(wellId)
                        )
                );

        if(!isValid) {
            throw new HOG_Exception(Label.VG_Invalid_Location);
        }

        return isValid;
    }

    /**
     * Retrieves ID of Production Engineer based on Location.
     * Production Engineer is located on AMU.
     *
     * @param wellId
     *
     * @return Id
     */
    public Id getProductionEngineer(Id wellId) {
        Id productionEngineerId;

        Location__c loc = extractLocationFromList(
                locationSelector.getLocationById(wellId)
        );

        if(loc.Operating_Field_AMU__r != null) {
            productionEngineerId = loc.Operating_Field_AMU__r.Production_Engineer_User__c;
        }

        return productionEngineerId;
    }

    /**
     * Checks if current user has correct Permissions to be able to create Alert with type of Engineering Notification.
     * Allowed Permissions are located in HOG_VentGas_Utilities in constant VENT_GAS_ALLOW_CREATE_PS.
     * If at least one of those permission sets is assigned to the user, he is considered as Valid user.
     *
     * @return Boolean
     */
    private Boolean hasCorrectAlertCreatePermissions() {
        try {
            String userId = UserInfo.getUserId();

            Set<Id> PSIds = HOG_GeneralUtilities.getSetOfIds(
                    permissionSelector.getPermissionSetsByName(HOG_VentGas_Utilities.VENT_GAS_ALLOW_CREATE_PS)
            );

            Integer assignments = permissionSelector.getAssignmentCountForPermissions(userId, PSIds);

            return assignments >= 1;
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' -> hasCorrectAlertCreatePermissions. Exception: ' + ex.getMessage());
        }

        return false;
    }

    /**
     * Method that extracts first Location from List. Only works with list containing single Location record.
     * If there are more or less records in provided list, it will return empty Location record.
     *
     * @param wells
     *
     * @return Location__c
     */
    private Location__c extractLocationFromList(List<Location__c> wells) {
        if(wells != null && wells.size() == 1) {
            return wells.get(0);
        } else {
            return new Location__c();
        }
    }

}