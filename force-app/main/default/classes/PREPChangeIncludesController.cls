public with sharing class PREPChangeIncludesController {
    public PREP_Initiative__c preInitiative { get; set; }
    public PREP_Material_Service_Group__c taxonomy { get; set; }
    public List<PREPTaxonomyWrapper> taxonomyWrapperList { get; set; }
    public String requestTitle { get; set; }
    public String valuationClassDesc { get; set; }
    Map<Id, PREP_Material_Service_Group__c> taxonomyMap;
    
    public PREPChangeIncludesController() {
        preInitiative = new PREP_Initiative__c();
        taxonomy = new PREP_Material_Service_Group__c();
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();
        valuationClassDesc = '';
    }
    
    public void searchRecords() {
        Boolean whereCondition = false;
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();

        String taxonomyQuery = 'SELECT Id, Name, Owner.Name, Material_Service_Group_Name__c, SAP_Short_Text_Name__c, '
                                + 'Type__c, Category_Manager__r.Name, Category_Specialist__r.Name, '
                                + 'Category_Link__c, Discipline_Link__c, Includes__c, Does_Not_Include__c, '
                                + 'Sub_Category__c FROM PREP_Material_Service_Group__c';                       
        
        if( preInitiative.Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Link__c = \'' + preInitiative.Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Link__c = \'' + preInitiative.Category__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Sub_Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Discipline__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Discipline_Link__c = \'' + preInitiative.Discipline__c + '\'';
            } else {
                taxonomyQuery += ' AND Discipline_Link__c = \'' + preInitiative.Discipline__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Manager__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Specialist__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Name != null && taxonomy.Name != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            else
                taxonomyQuery += ' AND Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            whereCondition = true;
        }
                
        taxonomyQuery += ' LIMIT 1000';
        
        List<PREP_Material_Service_Group__c> taxonomyList = Database.query( taxonomyQuery );
                
        taxonomyMap = new Map<Id, PREP_Material_Service_Group__c>();
        
        for( PREP_Material_Service_Group__c t : taxonomyList ){
            taxonomyWrapperList.add( new PREPTaxonomyWrapper( t.clone( true, true ) ));
            taxonomyMap.put( t.Id, t );
        }
        
        taxonomyList = new List<PREP_Material_Service_Group__c>();
    }
    
    public PageReference saveChanges() {
                
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
            }        
        }
        
        if( taxWrapper.size() > 0 ){
            saveTaxonomy( taxWrapper, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
        }
    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference saveAndSubmitChanges(){                
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
            }        
        }
        
        if( taxWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveTaxonomy( taxWrapper, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process(request);
            }
        }

        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    private List<PREP_Change_Header__c> saveTaxonomy( List<PREPTaxonomyWrapper> taxWrapper, String objectName, String reasonVal ){
        Boolean includeFlag = false;
        Boolean notIncludeFlag = false;
        //Set<Id> taxonomyIds = new Set<Id>();
        
        for( PREPTaxonomyWrapper w : taxWrapper ){
            if( !w.taxonomy.Includes__c.equalsIgnoreCase( taxonomyMap.get( w.taxonomy.Id ).Includes__c )){
                includeFlag = true;
                //taxonomyIds.add( w.taxonomy.Id );
            }
            
            if( !w.taxonomy.Does_Not_Include__c.equalsIgnoreCase( taxonomyMap.get( w.taxonomy.Id ).Does_Not_Include__c )){
                notIncludeFlag = true;
                //taxonomyIds.add( w.taxonomy.Id );
            }
        }
        
        List<PREP_Change_Header__c> headerList = new List<PREP_Change_Header__c>();
        PREP_Change_Header__c headerInclude;
        PREP_Change_Header__c headerNotInclude;
        /*Set<Id> taxonomyNotIds = new Set<Id>();
        
        if( taxonomyIds.size() > 0 )
            taxonomyNotIds = cancelPreviousRequests( taxonomyIds );*/
        
        if( includeFlag ){
            headerInclude = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Include',
                                                        Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                        Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                        Object_Name__c = objectName );
        
            headerList.add( headerInclude );
        }
        
        if( notIncludeFlag ){
            headerNotInclude = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Does not Include',
                                                            Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                            Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                            Object_Name__c = objectName );
        
            headerList.add( headerNotInclude );
        }
        
        if( headerList.size() > 0 ){
            insert headerList;
        
            List<PREP_Change_Detail__c> detailList = new List<PREP_Change_Detail__c>();
            for( PREPTaxonomyWrapper w : taxWrapper ){
                if( !w.taxonomy.Includes__c.equalsIgnoreCase( taxonomyMap.get( w.taxonomy.Id ).Includes__c )){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerInclude.id, Field_Name__c = 'Include',
                                                                                    From_Field__c = taxonomyMap.get( w.taxonomy.Id ).Includes__c,
                                                                                    Record_Id__c = w.taxonomy.Id,
                                                                                    To_Field__c = w.taxonomy.Includes__c,
                                                                                    //To_Field_Id__c = w.taxonomy.Id,
                                                                                    //From_Field_Id__c = w.taxonomy.Id,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = w.taxonomy.Name,
                                                                                    Update_Record_Description__c = w.taxonomy.Material_Service_Group_Name__c );
                    detailList.add( detailRec );
                }
                
                if( !w.taxonomy.Does_Not_Include__c.equalsIgnoreCase( taxonomyMap.get( w.taxonomy.Id ).Does_Not_Include__c )){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerNotInclude.id, Field_Name__c = 'Does not Include',
                                                                                    From_Field__c = taxonomyMap.get( w.taxonomy.Id ).Does_Not_Include__c,
                                                                                    Record_Id__c = w.taxonomy.Id,
                                                                                    To_Field__c = w.taxonomy.Does_Not_Include__c,
                                                                                    //To_Field_Id__c = w.taxonomy.Id,
                                                                                    //From_Field_Id__c = w.taxonomy.Id,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = w.taxonomy.Name,
                                                                                    Update_Record_Description__c = w.taxonomy.Material_Service_Group_Name__c );
                    detailList.add( detailRec );
                }
            }
            
            if( detailList.size() > 0 )    
                insert detailList;
            
            if( detailList.size() == 0 )    
                delete headerList;
        }
        
        return headerList;
    }
        
    public class PREPTaxonomyWrapper {
        public Boolean selected { get; set; }
        public PREP_Material_Service_Group__c taxonomy { get; set; }
        
        public PREPTaxonomyWrapper( PREP_Material_Service_Group__c taxonomy ){
            this.taxonomy = taxonomy;
            this.selected = false;
        }
    }
}