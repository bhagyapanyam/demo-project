/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_ExceptionUtils
History:        jschn 2019-07-24 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_ExceptionUtilsTest {

    @IsTest
    static void throwError2Param_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;

        try {
            HOG_ExceptionUtils.throwError(null, null);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assert(errorMessage.contains('Error Message'));
    }

    @IsTest
    static void throwError2Param_withErrorMessageParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message = '{0}';

        try {
            HOG_ExceptionUtils.throwError(message, null);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assert(errorMessage.contains('Formatted values'));
    }

    @IsTest
    static void throwError2Param_withEmptyListParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message;
        List<String> values = new List<String> {};

        try {
            HOG_ExceptionUtils.throwError(message, values);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assert(errorMessage.contains('Error Message'));
    }

    @IsTest
    static void throwError2Param_withBothParams_EmptyList() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message = '{0}';
        List<String> values = new List<String> {};

        try {
            HOG_ExceptionUtils.throwError(message, values);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assert(errorMessage.contains('Formatted values'));
    }

    @IsTest
    static void throwError2Param_withBothParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message = '{0}';
        String value = 'error';
        List<String> values = new List<String> {value};

        try {
            HOG_ExceptionUtils.throwError(message, values);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assertEquals(value, errorMessage);
    }

    @IsTest
    static void throwError1Param_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message;

        try {
            HOG_ExceptionUtils.throwError(message);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, errorMessage);
    }

    @IsTest
    static void throwError1Param_withEmptyParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message = '';

        try {
            HOG_ExceptionUtils.throwError(message);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
    }

    @IsTest
    static void throwError1Param_withProperParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        String errorMessage;
        String message = 'error';

        try {
            HOG_ExceptionUtils.throwError(message);
        } catch(HOG_Exception ex) {
            errorMessage = ex.getMessage();
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, errorMessage);
        System.assert(errorMessage.contains(message));
    }

}