/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that carries information for form related actions.
Test Class:     EPD_ResponseTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_Response extends HOG_RequestResult {

    @AuraEnabled
    public EPD_FormStructure formStructure;

    public EPD_Response(EPD_FormStructure formStructure) {
        this.formStructure = formStructure;
        this.success = true;
    }

    public EPD_Response(Exception ex) {
        setErrorMessage(HOG_ErrorPrettifyService.prettifyErrorMessage(ex.getMessage()));
    }

    public EPD_Response(HOG_Exception ex) {
        setErrorMessage(ex.getMessage());
    }

    public EPD_Response(String errorMessage) {
        setErrorMessage(errorMessage);
    }

    private void setErrorMessage(String errorMessage) {
        if(errors == null) {
            errors = new List<String>();
        }
        errors.add(errorMessage);
        this.success = false;
    }

    public List<String> addError(HOG_Exception ex) {
        setErrorMessage(ex.getMessage());
        return errors;
    }

    public List<String> addError(Exception ex) {
        setErrorMessage(HOG_ErrorPrettifyService.prettifyErrorMessage(ex.getMessage()));
        return errors;
    }

    public List<String> addError(String errorMsg) {
        setErrorMessage(errorMsg);
        return errors;
    }

}