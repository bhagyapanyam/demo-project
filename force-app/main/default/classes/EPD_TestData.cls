/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Test Data factory for EPD Unit Tests
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
public class EPD_TestData {

    public static EPD_Engine_Performance_Data__c createEPD() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();

        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(new EPD_Block_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        blockInfo.Engine_Performance_Data__c = epd.Id;
        insert blockInfo;

        return epd;
    }

    public static EPD_Engine_Performance_Data__c createSingleEPD() {
        return createSingleEPD(true);
    }

    public static EPD_Engine_Performance_Data__c createEPDWithRetrospectiveError() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();

        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(new EPD_Block_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        blockInfo.Engine_Performance_Data__c = epd.Id;
        insert blockInfo;

        EPD_Cylinder_Information__c cylinderInformation = (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(new EPD_Cylinder_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        cylinderInformation.Block_Information__c = blockInfo.Id;
        insert cylinderInformation;


        EPD_Engine_Performance_Data__c epd2 = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(new EPD_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        epd2.Work_Order__c = epd.Work_Order__c;
        epd2.Work_Order_Activity__c = epd.Work_Order_Activity__c;
        epd2.Company__c = epd.Company__c;
        epd2.Equipment_Engine__c = epd.Equipment_Engine__c;
        insert epd2;

        blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(new EPD_Block_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        blockInfo.Engine_Performance_Data__c = epd2.Id;
        insert blockInfo;
        cylinderInformation = (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(new EPD_Cylinder_Information__c(), EPD_FieldDefaultsWrong.CLASS_NAME, false);
        cylinderInformation.Block_Information__c = blockInfo.Id;
        insert cylinderInformation;

        epd.Previous_EPD__c = epd2.Id;
        update epd;

        return epd;
    }

    public static EPD_Block_Information__c createBlockInformation() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(new EPD_Block_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        blockInfo.Engine_Performance_Data__c = epd.Id;
        insert blockInfo;
        return blockInfo;
    }

    public static EPD_Cylinder_Information__c createCylinderInformation() {
        EPD_Block_Information__c blockInfo = createBlockInformation();
        EPD_Cylinder_Information__c cylinderInformation = (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(new EPD_Cylinder_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        cylinderInformation.Block_Information__c = blockInfo.Id;
        insert cylinderInformation;
        return cylinderInformation;
    }

    public static EPD_Part_Replacement__c createPartReplacement() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();
        EPD_Part_Replacement__c partReplacement = (EPD_Part_Replacement__c) HOG_SObjectFactory.createSObject(new EPD_Part_Replacement__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        partReplacement.Engine_Performance_Data__c = epd.Id;
        insert partReplacement;
        return partReplacement;
    }

    public static EPD_Compressor_Operating_Condition_Stage__c createCOCStage() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();
        EPD_Compressor_Operating_Condition_Stage__c cocStage = (EPD_Compressor_Operating_Condition_Stage__c) HOG_SObjectFactory.createSObject(new EPD_Compressor_Operating_Condition_Stage__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        cocStage.Engine_Performance_Data__c = epd.Id;
        insert cocStage;
        return cocStage;
    }

    public static EPD_Engine_Operating_Condition__c createEOC() {
        EPD_Engine_Performance_Data__c epd = createSingleEPD();
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(new EPD_Engine_Operating_Condition__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        EOCRecord.Engine_Performance_Data__c = epd.Id;
        insert EOCRecord;
        return EOCRecord;
    }

    public static EPD_Engine_Performance_Data__c createSingleEPD(Boolean doInsert) {
        EPD_EPDTriggerHandler.skipTrigger = true;
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        HOG_Service_Category__c serviceCategory = VTT_TestDataFactory.createServiceCategory(true);
        HOG_Notification_Type__c notificationType = VTT_TestDataFactory.createNotificationType(serviceCategory.Id, true);
        HOG_Service_Code_MAT__c serviceCodeMat = VTT_TestDataFactory.createServiceCodeMat(true);
        HOG_Service_Required__c serviceRequired = VTT_TestDataFactory.createServiceRequired(true);
        HOG_User_Status__c userStatus = VTT_TestDataFactory.createUserStatus(true);
        HOG_Work_Order_Type__c workOrderType =  VTT_TestDataFactory.createWorkOrderType(notificationType.Id,
                serviceCodeMat.Id,
                serviceRequired.Id,
                userStatus.Id,
                true);

        Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);
        HOG_Service_Request_Notification_Form__c snr = VTT_TestDataFactory.createServiceRequestNotification(workOrderType.Id, true);

        Route__c route = RouteTestData.createRoute('999');
        insert route;
        Location__c loc = LocationTestData.createLocation('Location Source', route.Id, thermalField.Id);
        insert loc;
        Equipment__c eq = EquipmentTestData.createEquipment(loc);

        HOG_Maintenance_Servicing_Form__c wo = VTT_TestDataFactory.createWorkOrder(snr.Id, notificationType.Id, false);
        wo.Planner_Group__c = '100';
        wo.Main_Work_Centre__c = 'Test';
        insert wo;
        Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName', wo.Id, thermalField.Id, false);
        woa.Equipment__c = eq.Id;
        woa.Assigned_Vendor__c = acc.Id;
        insert woa;


        Equipment_Engine__c eqEngine = (Equipment_Engine__c) HOG_SObjectFactory.createSObject(new Equipment_Engine__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        eqEngine.Equipment__c = eq.Id;
        insert eqEngine;

        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(new EPD_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        epd.Work_Order__c = wo.Id;
        epd.Work_Order_Activity__c = woa.Id;
        epd.Company__c = acc.Id;
        epd.Equipment_Engine__c = eqEngine.Id;
        if(doInsert) insert epd;

        return epd;
    }

    public static Id createEquipmentForBatch() {
        Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Field__c thermalField = HOG_TestDataFactory.createAMU('634', district.Id, recordTypeId, true);
        Route__c route = RouteTestData.createRoute('999');
        insert route;
        Location__c loc = LocationTestData.createLocation('Location Source', route.Id, thermalField.Id);
        insert loc;

        Equipment__c eq = (Equipment__c) HOG_SObjectFactory.createSObject(new Equipment__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
        eq.Location__c = loc.Id;
        insert eq;

        return eq.Id;
    }

    public static EPD_FormStructure prepareStructure() {
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                )
        };
        EPD_FormStructure formStructure = new EPD_FormStructure()
                .setEPDStructure(
                        (EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(
                                createSingleEPD()
                        )
                )
                .setEngineBlocks(
                        new List<EPD_FormStructureBlock>{
                                block
                        }
                )
                .setParts(
                        new List<EPD_FormStructurePart>{
                                (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(
                                        HOG_SObjectFactory.createSObject(
                                                new EPD_Part_Replacement__c(),
                                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                                false)
                                )
                        }
                )
                .setCOCStages(
                        new List<EPD_FormStructureCOCStage>{
                                (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(
                                        HOG_SObjectFactory.createSObject(
                                                new EPD_Compressor_Operating_Condition_Stage__c(),
                                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                                false)
                                )
                        }
                )
                .setEOCs(
                        new List<EPD_FormStructureEOC>{
                                (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(
                                        HOG_SObjectFactory.createSObject(
                                                new EPD_Engine_Operating_Condition__c(),
                                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                                false)
                                )
                        }
                );

        return formStructure;
    }

    public static void assignPermission(User user) {
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'HOG_Reliability_Engineer'];

        PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
        permissionSetAssignment.PermissionSetId = permissionSet.Id;
        permissionSetAssignment.AssigneeId = user.Id;

        insert permissionSetAssignment;
    }

}