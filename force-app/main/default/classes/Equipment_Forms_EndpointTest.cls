/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/6/2020   
 */

@IsTest
private class Equipment_Forms_EndpointTest {

	@TestSetup
	static void prepareData() {
		Business_Department__c department = HOG_TestDataFactory.createBusinessDepartment(true);
		Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict(department.Id, true);
		Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
		HOG_TestDataFactory.createAMU('100', district.Id, recordTypeId, true);
		insert RouteTestData.createRoute('999');
	}

	@IsTest
	static void insertAddEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;

		Test.startTest();
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c();

		Equipment_Forms_Endpoint.insertAddEquipmentForm(
				loc.Id,
				addForm,
				'123',
				'Test file',
				'321',
				'Test Attachment');

		Equipment_Missing_Form__c createdForm = [SELECT Id, Status__c, Serial_plate_of_asset__c, Attachment__c, Location__c FROM Equipment_Missing_Form__c LIMIT 1];
		System.assertEquals(EquipmentUtilities.REQUEST_STATUS_OPEN, createdForm.Status__c);
		System.assertEquals(loc.Id, createdForm.Location__c);

		List<ContentDocumentLink> createdFiles = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :addForm.Id];
		System.assertEquals(2, createdFiles.size());

		Integer fieldsAreFilled = 0;
		for (ContentDocumentLink file : createdFiles) {
			if (file.ContentDocumentId == createdForm.Serial_plate_of_asset__c || file.ContentDocumentId == createdForm.Attachment__c) {
				fieldsAreFilled++;
			}
		}
		System.assertEquals(2, fieldsAreFilled);

		Test.stopTest();
	}

	@IsTest
	static void insertAddEquipmentFormNegativeTest() {
		Test.startTest();
		try {
			Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c();

			Equipment_Forms_Endpoint.insertAddEquipmentForm(
					'wrongLocationId',
					addForm,
					'123',
					'Test file',
					'321',
					'Test Attachment');
		} catch (Exception ex) {
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void updateAddEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;
		String serialPlateFileId = Equipment_Forms_FileHandler.insertFile('123', 'Test File Serial Plate', addForm.Id);
		String attachmentFileId = Equipment_Forms_FileHandler.insertFile('321', 'Test File Attachment', addForm.Id);
		Attachment attachmentSerialPlate = new Attachment(Name = 'Test Attachment Serial Plate', Body = Blob.valueOf('123'), ParentId = addForm.Id);
		insert attachmentSerialPlate;
		Attachment attachmentAttachment = new Attachment(Name = 'Test Attachment Attachment', Body = Blob.valueOf('321'), ParentId = addForm.Id);
		insert attachmentAttachment;

		Test.startTest();

		Equipment_AddForm_File wrapper = new Equipment_AddForm_File();
		wrapper.originalSerialPlateFileId = serialPlateFileId;
		wrapper.serialPlateBase64Data = '123';
		wrapper.serialPlateName = 'New Test Serial Plate File';
		wrapper.attachmentBase64Data = '123';
		wrapper.originalAttachmentFileId = attachmentFileId;
		wrapper.attachmentFileName = 'New Test Attachment File';
		wrapper.insertSerialPlate = true;
		wrapper.insertAttachment = true;

		//positive test, delete previous files and insert new files
		List<ContentDocumentLink> files = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :addForm.Id];
		System.assertEquals(2, files.size());

		addForm.Description__c = 'Test description';
		Equipment_Forms_Endpoint.updateAddEquipmentForm(addForm, wrapper);
		Equipment_Missing_Form__c updatedForm = [
				SELECT Serial_plate_of_asset__c,
						Attachment__c,
						Description__c
				FROM Equipment_Missing_Form__c
				WHERE Id = :addForm.Id
		];
		System.assertNotEquals(null, updatedForm.Description__c);
		files = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :addForm.Id];
		System.assertEquals(2, files.size());

		Integer filesHasBeenCreated = 0;
		for (ContentDocumentLink file : files) {
			if (file.ContentDocumentId == updatedForm.Serial_plate_of_asset__c || file.ContentDocumentId == updatedForm.Attachment__c) {
				filesHasBeenCreated++;
			}
		}
		System.assertEquals(2, filesHasBeenCreated);

		//positive test, delete previous attachments
		wrapper.originalSerialPlateFileId = null;
		wrapper.serialPlateBase64Data = null;
		wrapper.serialPlateName = null;
		wrapper.attachmentBase64Data = null;
		wrapper.originalAttachmentFileId = null;
		wrapper.originalSerialPlateAttachmentId = attachmentSerialPlate.Id;
		wrapper.attachmentFileName = null;
		wrapper.originalAttachmentId = attachmentAttachment.Id;
		wrapper.insertSerialPlate = false;
		wrapper.insertAttachment = false;
		Equipment_Forms_Endpoint.updateAddEquipmentForm(addForm, wrapper);
		updatedForm = [
				SELECT Serial_plate_of_asset__c,
						Attachment__c
				FROM Equipment_Missing_Form__c
				WHERE Id = :addForm.Id
		];

		System.assertEquals(null, updatedForm.Serial_plate_of_asset__c);
		System.assertEquals(null, updatedForm.Attachment__c);

		Integer deletedAttachments = [SELECT count() FROM Attachment WHERE ParentId = :addForm.Id];
		System.assertEquals(0, deletedAttachments);

		//rejected form test
		wrapper.originalSerialPlateAttachmentId = null;
		wrapper.originalAttachmentId = null;
		addForm.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		update addForm;
		Equipment_Forms_Endpoint.updateAddEquipmentForm(addForm, wrapper);
		updatedForm = [
				SELECT Id,
						Status__c
				FROM Equipment_Missing_Form__c
				WHERE Id = :addForm.Id
		];

		System.assertEquals(EquipmentUtilities.REQUEST_STATUS_OPEN, updatedForm.Status__c);
		Test.stopTest();
	}

	@IsTest
	static void cannotUpdateAddEquipmentFormTest() {
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED);
		insert addForm;
		Test.startTest();
		try {
			//closed form test
			Equipment_Forms_Endpoint.updateAddEquipmentForm(addForm, new Equipment_AddForm_File());
		} catch (Exception ex) {
			System.assertEquals(EquipmentUtilities.FORM_IS_CLOSED_MESSAGE, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void loadAddEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;
		Equipment_Missing_Form__c addFormNoFiles = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addFormNoFiles;
		String serialPlateId = Equipment_Forms_FileHandler.insertFile('123', 'Test File Serial Plate', addForm.Id);
		Attachment attachmentAttachment = new Attachment(Name = 'Test Attachment Attachment', Body = Blob.valueOf('321'), ParentId = addForm.Id);
		insert attachmentAttachment;

		Test.startTest();
		//load form with file and attachment
		Equipment_AddForm addFormWrapper = Equipment_Forms_Endpoint.loadAddEquipmentForm(addForm.Id);
		System.assertEquals(addForm.Id, addFormWrapper.addForm.Id);
		System.assertEquals(serialPlateId, addFormWrapper.addFormFiles[0].ContentDocumentId);
		System.assertEquals(attachmentAttachment.Id, addFormWrapper.addFormAttachments[0].Id);

		//no file or attachment on the form
		addFormWrapper = Equipment_Forms_Endpoint.loadAddEquipmentForm(addFormNoFiles.Id);
		System.assertEquals(addFormNoFiles.Id, addFormWrapper.addForm.Id);
		System.assertEquals(null, addFormWrapper.addFormFiles);
		System.assertEquals(null, addFormWrapper.addFormAttachments);

		Test.stopTest();

	}

	@IsTest
	static void loadAddEquipmentFormNegativeTest() {
		Test.startTest();
		try {
			Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c();
			Equipment_Forms_Endpoint.loadAddEquipmentForm(addForm.Id);
		} catch (Exception ex) {
			System.assertEquals(EquipmentUtilities.ERROR_ON_LOAD_MESSAGE, ex.getMessage());
		}
		Test.stopTest();

	}

	@IsTest
	static void getFlocTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;

		Test.startTest();
		HOG_SObject_Information floc = Equipment_Forms_Endpoint.getFloc(loc.Id);
		System.assertEquals(floc.floc.Id, loc.Id);
		System.assertEquals(floc.flocAPI, 'Location__c');
		Test.stopTest();
	}

	@IsTest
	static void getFlocNegativeTest() {
		Test.startTest();
		try {
			Equipment_Forms_Endpoint.getFloc('wrongLocationId');
		} catch (Exception ex) {
			System.assertEquals(EquipmentUtilities.ERROR_ON_LOAD_MESSAGE, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void rejectAddEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;

		Test.startTest();
		String rejectionMessage = 'Test Rejection';
		Equipment_Forms_Endpoint.rejectAddEquipmentForm(addForm.Id, rejectionMessage);
		addForm = [SELECT Status__c, Reason__c FROM Equipment_Missing_Form__c WHERE Id = :addForm.Id];
		System.assertEquals(EquipmentUtilities.REQUEST_STATUS_REJECTED, addForm.Status__c);
		System.assertEquals(rejectionMessage, addForm.Reason__c);
		Test.stopTest();
	}

	@IsTest
	static void rejectAddEquipmentFormNegativeTest() {
		Test.startTest();
		try {
			Equipment_Forms_Endpoint.rejectAddEquipmentForm('wrongFormId', 'Test Rejection');
		} catch (Exception ex) {
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void processAddEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;

		Test.startTest();
		Equipment_Forms_Endpoint.processAddEquipmentForm(addForm.Id);
		addForm = [SELECT Status__c FROM Equipment_Missing_Form__c WHERE Id = :addForm.Id];
		System.assertEquals(EquipmentUtilities.REQUEST_STATUS_PROCESSED, addForm.Status__c);
		Test.stopTest();

	}

	@IsTest
	static void processAddEquipmentFormNegativeTest() {
		Test.startTest();
		try {
			Equipment_Forms_Endpoint.processAddEquipmentForm('wrongFormId');
		} catch (Exception ex) {
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();

	}

	@IsTest
	static void getFLOCKEquipmentTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;
		Equipment__c masterEquipment = EquipmentTestData.createEquipment(loc);
		Equipment__c childEquipment = EquipmentTestData.createEquipment(loc);
		childEquipment.Superior_Equipment__c = masterEquipment.Id;
		update childEquipment;

		List<Equipment__c> equipmentList = new List<Equipment__c>();
		equipmentList.add(masterEquipment);
		equipmentList.add(childEquipment);

		Test.startTest();
		try {

			//positive test
			Equipment_Forms_Endpoint.getFLOCKEquipment(loc.Id);
			Equipment_Forms_Endpoint.something(equipmentList);
			Equipment_Forms_Endpoint.somethingMore(masterEquipment.Id, loc.Id);


		} catch (Exception ex) {
			System.debug('Show exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void activeAddEquipmentFormsTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Location', route.Id, amu.Id);
		insert loc;
		Equipment_Missing_Form__c addForm = new Equipment_Missing_Form__c(Location__c = loc.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert addForm;

		Test.startTest();
		try {

			//positive test
			//Equipment_Forms_Endpoint.activeAddEquipmentForms(loc.Id);


		} catch (Exception ex) {
			System.debug('Show exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void insertTransferFormEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;
		Equipment__c equipment = EquipmentTestData.createEquipment(source);
		Equipment__c equipmentSecond = EquipmentTestData.createEquipment(destination);

		Test.startTest();
		try {
			Equipment_Transfer_Form__c transferForm = new Equipment_Transfer_Form__c();
			HOG_File file = new HOG_File();
			file.fileName = 'Test File';
			file.fileContent = '123';
			List<Equipment_TransferForm_ItemForInsert> wrapper = new List<Equipment_TransferForm_ItemForInsert>();
			Equipment_TransferForm_ItemForInsert firstItem = new Equipment_TransferForm_ItemForInsert();
			firstItem.equipment = equipment;
			firstItem.tagColour = 'Green';
			firstItem.itemFile = file;
			wrapper.add(firstItem);


			Equipment_TransferForm_ItemForInsert secondItem = new Equipment_TransferForm_ItemForInsert();
			secondItem.equipment = equipmentSecond;
			secondItem.tagColour = 'Black';
			secondItem.itemFile = file;
			wrapper.add(secondItem);

			System.debug('Wrapper ' + wrapper);

			//positive test from source to destination
			Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, destination.Id, transferForm, wrapper);


			//positive test to source only
			Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(null, null, null, null);

		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}


	@IsTest
	static void updateTransferEquipmentForm() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;
		Equipment__c equipment = EquipmentTestData.createEquipment(source);
		Equipment__c equipmentSecond = EquipmentTestData.createEquipment(destination);
		Equipment_Transfer_Form__c transferForm = new Equipment_Transfer_Form__c(From_Location__c = source.Id, To_Location__c = destination.Id);
		insert transferForm;

		List<Equipment_Transfer_Item__c> itemList = new List<Equipment_Transfer_Item__c>();
		//item to update
		itemList.add(new Equipment_Transfer_Item__c(Equipment_Transfer_Form__c = transferForm.Id, Equipment__c = equipment.Id));
		//item to remove
		itemList.add(new Equipment_Transfer_Item__c(Equipment_Transfer_Form__c = transferForm.Id, Equipment__c = equipmentSecond.Id));
		insert itemList;

		List<String> filesToRemove = new List<String>();
		List<String> attachmentsToRemove = new List<String>();
		Equipment_Forms_FileHandler.insertFile('123', 'Test File', itemList[0].Id);
		filesToRemove.add([SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :itemList[0].Id].ContentDocumentId);


		List<String> itemsToRemove = new List<String>();
		itemsToRemove.add(itemList[1].Id);


		Test.startTest();
		try {
			List<Equipment_TransferForm_Item> itemsToUpdate = new List<Equipment_TransferForm_Item>();
			List<Equipment_TransferForm_ItemFile> filesToInsertOnItem = new List<Equipment_TransferForm_ItemFile>();
			List<Equipment_TransferForm_ItemForInsert> itemsToInsert = new List<Equipment_TransferForm_ItemForInsert>();


			//Equipment Transfer Item to Update
			Equipment_TransferForm_Item createdItem = new Equipment_TransferForm_Item();
			createdItem.item = itemList[0];
			itemsToUpdate.add(createdItem);

			//firstItem.files.add([SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :itemList[0].Id]);


			//File To Insert after removing the previous one
			HOG_File file = new HOG_File();
			file.fileName = 'Test File';
			file.fileContent = '123';

			//File To Insert On Existing Item
			Equipment_TransferForm_ItemFile firstFile = new Equipment_TransferForm_ItemFile();
			firstFile.equipment = itemsToUpdate[0].item;
			firstFile.itemFile = file;
			filesToInsertOnItem.add(firstFile);


			//Item to Insert, inserting the same file
			Equipment_TransferForm_ItemForInsert firstItem = new Equipment_TransferForm_ItemForInsert();
			firstItem.equipment = equipmentSecond;
			firstItem.tagColour = 'Black';
			firstItem.itemFile = file;
			itemsToInsert.add(firstItem);


			System.debug('Wrapper ' + itemsToInsert);

			//positive test from source to destination
			Equipment_Forms_Endpoint.updateTransferEquipmentForm(
					transferForm,
					true,
					destination.Id,
					itemsToRemove,
					itemsToUpdate,
					filesToRemove,
					attachmentsToRemove,
					itemsToInsert,
					filesToInsertOnItem);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			Equipment_Forms_Endpoint.updateTransferEquipmentForm(
					null,
					true,
					null,
					null,
					null,
					null,
					null,
					null,
					null);

		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadTransferFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;

		Equipment_Transfer_Form__c transferForm = new Equipment_Transfer_Form__c(From_Location__c = source.Id, To_Location__c = destination.Id);
		insert transferForm;

		Test.startTest();
		try {


			//positive test from source to destination
			Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadEquipmentTransferItemsTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;

		Equipment__c equipment = EquipmentTestData.createEquipment(source);
		Equipment__c equipmentSecond = EquipmentTestData.createEquipment(destination);
		Equipment__c equipmentThird = EquipmentTestData.createEquipment(destination);

		Equipment_Transfer_Form__c transferForm = new Equipment_Transfer_Form__c(From_Location__c = source.Id, To_Location__c = destination.Id);
		insert transferForm;

		List<Equipment_Transfer_Item__c> itemList = new List<Equipment_Transfer_Item__c>();
		itemList.add(new Equipment_Transfer_Item__c(Equipment_Transfer_Form__c = transferForm.Id, Equipment__c = equipment.Id));
		itemList.add(new Equipment_Transfer_Item__c(Equipment_Transfer_Form__c = transferForm.Id, Equipment__c = equipmentSecond.Id));
		itemList.add(new Equipment_Transfer_Item__c(Equipment_Transfer_Form__c = transferForm.Id, Equipment__c = equipmentThird.Id));
		insert itemList;


		Equipment_Forms_FileHandler.insertFile('123', 'Test File', itemList[0].Id);
		insert new Attachment(Name = 'Test Attachment', Body = Blob.valueOf('123'), ParentId = itemList[1].Id);

		Test.startTest();
		try {


			//positive test from source to destination
			Equipment_Forms_Endpoint.loadEquipmentTransferItems(transferForm.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadSelectedEquipmentsTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;

		Equipment__c equipment = EquipmentTestData.createEquipment(source);
		Equipment__c equipmentSecond = EquipmentTestData.createEquipment(destination);
		Equipment__c equipmentThird = EquipmentTestData.createEquipment(destination);

		List<String> equipmentIds = new List<String>();
		equipmentIds.add(equipment.Id);
		equipmentIds.add(equipmentSecond.Id);
		equipmentIds.add(equipmentThird.Id);

		Test.startTest();
		try {


			//positive test from source to destination
			Equipment_Forms_Endpoint.loadSelectedEquipments(equipmentIds);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadEquipmentsTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;

		Equipment__c equipment = EquipmentTestData.createEquipment(source);
		Equipment__c equipmentSecond = EquipmentTestData.createEquipment(destination);
		Equipment__c equipmentThird = EquipmentTestData.createEquipment(destination);

		List<String> selectedTransferItems = new List<String>();

		Test.startTest();
		try {


			//positive test from source to destination
			Equipment_Forms_Endpoint.loadEquipments('100', false, source.Id, true, selectedTransferItems);
			Equipment_Forms_Endpoint.loadEquipments('100', true, source.Id, false, selectedTransferItems);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadEquipmentForNewDataUpdateFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;

		Equipment__c equipment = EquipmentTestData.createEquipment(loc);

		Test.startTest();
		try {


			//positive test from source to destination
			Equipment_Forms_Endpoint.loadEquipmentForNewDataUpdateForm(equipment.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void insertNewDataUpdateFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;

		Equipment__c equipment = EquipmentTestData.createEquipment(loc);

		Test.startTest();
		try {
			Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c(Equipment__c = equipment.Id);


			//positive test from source to destination


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			Equipment_Forms_Endpoint.insertNewDataUpdateForm(null, null, null, null);
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void updateDataUpdateFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;
		Equipment__c equipment = EquipmentTestData.createEquipment(loc);
		Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c(Equipment__c = equipment.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert correctionForm;
		String fileToRemove = Equipment_Forms_FileHandler.insertFile('123', 'Test File To Delete', correctionForm.Id);


		Test.startTest();
		try {

			Equipment_CorrectionForm_File wrapper = new Equipment_CorrectionForm_File();
			wrapper.insertAttachment = true;
			wrapper.originalAttachmentFileId = fileToRemove;
			wrapper.attachmentFileName = 'Test File To Insert';
			wrapper.attachmentBase64Data = '321';

			//positive test from source to destination
			Equipment_Forms_Endpoint.updateDataUpdateForm(correctionForm, wrapper);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			Equipment_Forms_Endpoint.updateDataUpdateForm(null, null);
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadCorrectionFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;
		Equipment__c equipment = EquipmentTestData.createEquipment(loc);
		Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c(Equipment__c = equipment.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert correctionForm;


		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.loadCorrectionForm(correctionForm.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void rejectCorrectionEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;
		Equipment__c equipment = EquipmentTestData.createEquipment(loc);
		Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c(Equipment__c = equipment.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert correctionForm;


		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.rejectCorrectionEquipmentForm(correctionForm.Id, 'Reject test');


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void processCorrectionEquipmentFormTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;
		Equipment__c equipment = EquipmentTestData.createEquipment(loc);
		Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c(Equipment__c = equipment.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert correctionForm;


		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.processCorrectionEquipmentForm(correctionForm.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void getUserPermissionTest() {
		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.getUserPermission();


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void getExpirationDateTest() {
		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.getExpirationDate();

			HOG_Equipment_Request_Configuration__c settings = new HOG_Equipment_Request_Configuration__c(Equipment_Request_Expiration_Date_Days__c = 60,
					Days_Before_Expiration_Notification__c = 10);
			insert settings;

			Equipment_Forms_Endpoint.getExpirationDate();

			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	@IsTest
	static void loadFLOCTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c loc = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert loc;
		Facility__c fac = FacilityTestData.createFacility('Source Location', route.Id, amu.Id);
		insert fac;

		Test.startTest();
		try {

			//positive test from source to destination
			Equipment_Forms_Endpoint.loadFLOC(loc.Id);
			Equipment_Forms_Endpoint.loadFLOC(fac.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}

	static List<HOG_Notification_Type__c> notificationType;
	static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
	static HOG_Work_Order_Type__c workOrderType;
	static HOG_Service_Code_MAT__c serviceCodeMAT;
	static HOG_Service_Category__c serviceCategory;
	static HOG_Service_Code_Group__c serviceCodeGroup;
	static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static List<String> WORKORDERRECORDTYPE = new List<String>{
			'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'
	};

	@IsTest
	static void equipmentFormsForWorkOrderTest() {
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		Route__c route = [SELECT Id FROM Route__c LIMIT 1];
		Location__c source = LocationTestData.createLocation('Source Location', route.Id, amu.Id);
		insert source;
		Location__c destination = LocationTestData.createLocation('Destination Location', route.Id, amu.Id);
		insert destination;
		insert new Equipment_Missing_Form__c(Location__c = source.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert new Equipment_Transfer_Form__c(From_Location__c = source.Id, To_Location__c = destination.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);
		insert new Equipment_Transfer_Form__c(To_Location__c = source.Id, Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN);


		Test.startTest();
		try {

			//positive test from source to destination
			/*HOG_Service_Category__c category = new HOG_Service_Category__c();
			insert category;

			HOG_Notification_Type__c type = new HOG_Notification_Type__c();
			type.HOG_Service_Category__c = category.Id;
			insert type;

			HOG_Maintenance_Servicing_Form__c workOrder = new HOG_Maintenance_Servicing_Form__c();
			workOrder.Location__c = loc.Id;
			workOrder.HOG_Notification_Type__c = type.Id;
			insert workOrder;*/

			Equipment_Forms_Endpoint.equipmentFormsForWorkOrder(source.Id);


			//positive test to source only
			//Equipment_Forms_Endpoint.insertTransferFormEquipmentForm(source.Id, null, transferForm, wrapper);

			//negative test
			//Equipment_Forms_Endpoint.loadTransferForm(transferForm.Id);


		} catch (Exception ex) {
			System.debug('Show me exception: ' + ex.getMessage());
			//System.assertNotEquals(null, ex.getMessage());
		}


		Test.stopTest();

	}


}