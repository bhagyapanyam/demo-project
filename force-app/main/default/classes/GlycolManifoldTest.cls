@isTest
private class GlycolManifoldTest {

    private static void InitializeData()
    {
        List<Plant__c> pList = new List<Plant__c>();
        List<Unit__c> uList;
        List<Glycol_Manifold__c> gmList = new List<Glycol_Manifold__c>();
        Integer plantNum = 10;
        
        // Map from plant to List of Units
        Map<Plant__c, List<Unit__c>> puMap = new Map<Plant__c, List<Unit__c>>();            
        
        for (Integer i = 0 ; i < plantNum ; i++)
        {
            Plant__c p = new Plant__c(Name = 'Plant ' + i);
            pList.add(p);
        }
        insert pList;
        for (Integer i = 0 ; i < plantNum ; i++)
        {
            // Need to refresh the uList variable
            uList = new List<Unit__c>();
            for (Integer j = 0 ; j < (i + 1) * 3 ; j++)
            {
                Unit__c u = new Unit__c(Name = 'Unit ' + j, Plant__c = pList[i].Id);
                uList.add(u);
            }
            insert uList;
            puMap.put(pList[i], uList);
        }

        Integer randomPlantNumber = Integer.valueOf(Math.random() * 10);
        Integer randomUnitNumber = Integer.valueOf(Math.random() * randomPlantNumber * 3);        

        for (Integer i = 0; i<6; i++)
        {
            if (i<3)
            {    
                Glycol_Manifold__c gm = new Glycol_Manifold__c(Name = '80-SM-00' +i, Manifold_Name__c = '80-SM-00' +i,Manifold_Type__c = 'Supply',Plant__c = pList[randomPlantNumber].Id,Unit__c = puMap.get(pList[randomPlantNumber])[randomUnitNumber].Id);
                gmList.add(gm);
            }
            else
            { 
                Glycol_Manifold__c gm = new Glycol_Manifold__c(Name = '80-RM-00' +i, Manifold_Name__c = '80-RM-00' +i,Manifold_Type__c = 'Return',Plant__c = pList[randomPlantNumber].Id,Unit__c = puMap.get(pList[randomPlantNumber])[randomUnitNumber].Id);           
                gmList.add(gm);
            }
        }
        insert gmList;

        List<Glycol_Manifold__c> gmanifolds = [select id, Name, Manifold_Name__c, Manifold_Type__c from Glycol_Manifold__c];
     
        Glycol_Manifold_Branch__c gmb1 = new Glycol_Manifold_Branch__c ();
        gmb1.Supply_Manifold__c = gmanifolds[0].id;
        gmb1.Supply_Branch_Num__c = 1;
        gmb1.Return_Manifold__c = gmanifolds[3].id;
        gmb1.Return_Branch_Num__c = 1;
   
        // Insert record with value of Supply Manifold 80-SM-000 and Supply Branch Number 1
        insert gmb1;

        Glycol_Manifold_Branch__c gmb2 = new Glycol_Manifold_Branch__c ();
        gmb2.Supply_Manifold__c = gmanifolds[1].id;
        gmb2.Supply_Branch_Num__c = 1;
        gmb2.Return_Manifold__c = gmanifolds[4].id;
        gmb2.Return_Branch_Num__c = 1;
    
        // Insert record with value of Return Manifold 80-RM-004 and Return Branch Number 1
        insert gmb2;

        List<Glycol_Piping_Line_Equipment__c> gpleList= new List<Glycol_Piping_Line_Equipment__c>();
    
        Glycol_Piping_Line_Equipment__c gple1 = new Glycol_Piping_Line_Equipment__c ();
        gple1.Name = 'Equip1';
        gple1.Line_Size__c = '1';
        gpleList.add(gple1);
    
        Glycol_Piping_Line_Equipment__c gple2 = new Glycol_Piping_Line_Equipment__c ();
        gple2.Name = 'Equip2';
        gple2.Line_Size__c = '2';
        gpleList.add(gple2);   
    
        Glycol_Piping_Line_Equipment__c gple3 = new Glycol_Piping_Line_Equipment__c ();
        gple3.Name = 'Equip3';
        gple3.Line_Size__c = '3';
        gpleList.add(gple3);   
    
        insert gpleList;
 
        Glycol_Branch_Equipment_Association__c gbea = new Glycol_Branch_Equipment_Association__c ();
        List <Glycol_Manifold_Branch__c> gmbranch = [SELECT id From Glycol_Manifold_Branch__c];
        List <Glycol_Piping_Line_Equipment__c> gplequip = [SELECT id From Glycol_Piping_Line_Equipment__c];
        
        gbea.Manifold_Branch__c = gmbranch[0].id;
        gbea.Piping_Line_Equipment__c = gplequip[0].id;
    
        insert gbea;
    }

////////////////////////////////////////////////////////////
/*  Test for Trigger: Glycol_ValidateUniqueManifoldBranch */
////////////////////////////////////////////////////////////

    static testMethod void testNewUpdateBranch()
    {
        InitializeData();
        
        List<Glycol_Manifold__c> gmanifolds = [select id, Name, Manifold_Name__c, Manifold_Type__c from Glycol_Manifold__c];
     
        Glycol_Manifold_Branch__c gmb3 = new Glycol_Manifold_Branch__c ();
        gmb3.Supply_Manifold__c = gmanifolds[0].id;
        gmb3.Supply_Branch_Num__c = 1;
        gmb3.Return_Manifold__c = gmanifolds[5].id;
        gmb3.Return_Branch_Num__c = 1;

        try  {
            // try to insert record with Supply Manifold 80-SM-000 and Supply Branch Number 1 that already exists
            insert gmb3;
        } catch (DmlException e) {
            // Assert Error Message
            System.assert(e.getMessage().contains('Supply Manifold 80-SM-000 and Supply Branch Number 1 already existed'),e.getMessage());
        } 

        Glycol_Manifold_Branch__c gmToUpdate;
        gmToUpdate = 
            [SELECT Return_Manifold__c FROM Glycol_Manifold_Branch__c
             WHERE Return_Manifold__c =: gmanifolds[3].id
             LIMIT 1];
        // Update the Return Manifold.
        gmToUpdate.Return_Manifold__c = gmanifolds[4].id;

        try {
            // try to update record with Return Manifold 80-RM-004 and Return Branch Number 1 that already exists
            update gmToUpdate;
        } catch (DmlException e) {
            //Assert Error Message
            System.assert(e.getMessage().contains('Return Manifold 80-RM-004 and Return Branch Number 1 already existed'),e.getMessage() );
        } 
            
        System.assertEquals(2,[select count() from  Glycol_Manifold_Branch__c]);   
    }

///////////////////////////////////////////////////////
/*  Test for Apex Class: Glycol_GlycolManifoldSearch */
///////////////////////////////////////////////////////

    static testMethod void testGlycolManifoldSearch()
    {
        InitializeData();
        List<Plant__c> plantsearch = [select id from Plant__c];
        List<Unit__c> unitsearch = [select id from Unit__c];    
         
        // set the page
        PageReference pageRef = Page.Glycol_Manifold_Search;       
        Test.setCurrentPage(pageRef);
        
        GlycolManifoldSearchCtrl GMSearchCtrl = new GlycolManifoldSearchCtrl();

        ApexPages.currentPage().getParameters().put('searchOption','Unit');
        ApexPages.currentPage().getParameters().put('plant',plantsearch[0].id);
        ApexPages.currentPage().getParameters().put('unit',unitsearch[0].id);
        ApexPages.currentPage().getParameters().put('type','Supply');

        ApexPages.currentPage().getParameters().put('searchOption','Return'); 
        ApexPages.currentPage().getParameters().put('searchOption','Equip');                   
        ApexPages.currentPage().getParameters().put('searchString','80');         

        ApexPages.currentPage().getParameters().put('searchOption','Supply');                    
        // System.debug('URL: ' + ApexPages.currentPage().getUrl());
                        
        GMSearchCtrl = new GlycolManifoldSearchCtrl();

        System.AssertNotEquals(GMSearchCtrl.getPlantItems(), null);
        System.AssertNotEquals(GMSearchCtrl.getUnitItems(), null);
        System.AssertNotEquals(GMSearchCtrl.getManifoldTypes(), null);
        GMSearchCtrl.RefreshUnitList();                  
        System.AssertNotEquals(GMSearchCtrl.GoToManifoldDetail(), null);
        System.AssertNotEquals(GMSearchCtrl.GoToNewManifold(), null);  
        System.assert(GMSearchCtrl.isUserHasAdminPermission == true);
        
        GMSearchCtrl.SearchManifold();            
        System.assertEquals(GMSearchCtrl.searchResults.size(), 0);

        ApexPages.currentPage().getParameters().put('searchOption','Unit');     
        ApexPages.currentPage().getParameters().put('type','Return');            
        GMSearchCtrl = new GlycolManifoldSearchCtrl();
        GMSearchCtrl.SearchByUnit();
        PageReference gmExportDetail = GMSearchCtrl.ExportDetail();            
        System.AssertNotEquals(gmExportDetail, null);        
        System.assertEquals(GMSearchCtrl.selectedMType, 'Return');         

        ApexPages.currentPage().getParameters().put('searchOption','Unit');     
        ApexPages.currentPage().getParameters().put('type','Supply'); 
        GMSearchCtrl = new GlycolManifoldSearchCtrl();
        GMSearchCtrl.SearchByUnit();      
        System.assertEquals(GMSearchCtrl.selectedMType, 'Supply');        
       
        ApexPages.currentPage().getParameters().put('searchOption','Equip'); 
        ApexPages.currentPage().getParameters().put('searchString','80');        
        GMSearchCtrl = new GlycolManifoldSearchCtrl();
        GMSearchCtrl.SearchByText();         
        System.AssertNotEquals(GMSearchCtrl.ExportDetail(), null); 
        System.AssertNotEquals(GMSearchCtrl.displayRecordCountInfo, null); 

        ApexPages.currentPage().getParameters().put('searchOption','Return'); 
        ApexPages.currentPage().getParameters().put('searchString','80');        
        GMSearchCtrl = new GlycolManifoldSearchCtrl();
        GMSearchCtrl.SearchByText();               

        ApexPages.currentPage().getParameters().put('searchOption','Supply'); 
        ApexPages.currentPage().getParameters().put('searchString','80');        
        GMSearchCtrl = new GlycolManifoldSearchCtrl();
        GMSearchCtrl.SearchByText(); 
        // System.debug('searchOption: ' + GMSearchCtrl.searchOption);  
        // System.debug('searchString: ' + GMSearchCtrl.searchString);     
    } 

////////////////////////////////////////////////////////////
/*  Test for Apex Class: GlycolAddMultiBranchEquipCtrl  */
////////////////////////////////////////////////////////////

    static testMethod void testGlycolAddMultiBranchEquipCtrl ()
    {
        InitializeData();

        List <Glycol_Manifold_Branch__c> gmbranch = [SELECT id From Glycol_Manifold_Branch__c];
        List <Glycol_Piping_Line_Equipment__c> gplequip = [SELECT id From Glycol_Piping_Line_Equipment__c];
  
        // set the page
        PageReference pageRef = Page.Glycol_AddMultiBranchEquipment;       
        Test.setCurrentPage(pageRef);
        Glycol_AddMultiBranchEquipCtrl GMAddMultiBranchEquipCtrl  = new Glycol_AddMultiBranchEquipCtrl();
        ApexPages.currentPage().getParameters().put('branchId',gmbranch[0].id);
        
        // Parameter to add equipment
        ApexPages.currentPage().getParameters().put('equipId',gplequip[1].id);
        // Parameter to remove equipment
        ApexPages.currentPage().getParameters().put('associateId',gplequip[1].id);
        // System.debug(ApexPages.currentPage().getUrl());
        GMAddMultiBranchEquipCtrl = new Glycol_AddMultiBranchEquipCtrl();
        GMAddMultiBranchEquipCtrl.loadSelectedEquipmentList();
       
        GMAddMultiBranchEquipCtrl.AddEquipment();
        GMAddMultiBranchEquipCtrl.RemoveEquipment();
        GMAddMultiBranchEquipCtrl.SaveEquipment(); 
        GMAddMultiBranchEquipCtrl.clearMsg();
        GMAddMultiBranchEquipCtrl.first();
        GMAddMultiBranchEquipCtrl.last();
        GMAddMultiBranchEquipCtrl.previous();
        GMAddMultiBranchEquipCtrl.next();
        System.AssertNotEquals(GMAddMultiBranchEquipCtrl.GoBackManifoldBranch(), null);         
        System.AssertNotEquals(GMAddMultiBranchEquipCtrl.getPaginationInfo(), null);  
        System.AssertNotEquals(GMAddMultiBranchEquipCtrl.pageNumber, null);  
        System.AssertNotEquals(GMAddMultiBranchEquipCtrl.resultSize, null); 
        System.AssertNotEquals(GMAddMultiBranchEquipCtrl.pageSize, null);              
        System.assert(GMAddMultiBranchEquipCtrl.hasNext == false);
        System.assert(GMAddMultiBranchEquipCtrl.hasPrevious == false);
    } 

///////////////////////////////////////////////////
/*  Test for Apex Class: GlycolManifoldEditCtrl  */
///////////////////////////////////////////////////

    static testMethod void testGlycolManifoldEdit()
    {
        InitializeData();

        Glycol_Manifold__c myTestGM = [SELECT id, Name, Manifold_Name__c, Manifold_Type__c, Plant__c, Unit__c  From Glycol_Manifold__c LIMIT 1];     
 
        // set the page
        PageReference pageRef = Page.GlycolManifoldEdit;       
        Test.setCurrentPage(pageRef);
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(myTestGM);
        GlycolManifoldEditCtrl GMEditCtrl  = new GlycolManifoldEditCtrl(sc);
        GMEditCtrl.RefreshUnitList();
        
        System.assert(GMEditCtrl.getPlantItems().size()>0);    
        System.assert(GMEditCtrl.getUnitItems().size()>0);     
       
        String nextPage = GMEditCtrl.saveAndNew().getUrl();
        System.assert(nextPage != null);
        //System.assertEquals('/a2h/e', nextPage);
    }  
   
}