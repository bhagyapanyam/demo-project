public class NCR_ViewPageControllerX_v0{
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
  
    private final ApexPages.StandardController stdcontroller;
    private final NCR_Case__c ncr_record;
    
    public Boolean ViewMode {get; private set;}
    
    
    /* Busston flags*/
    public Boolean btnEditEnabled {get; private set;}
    
    public Boolean btnSubmitEnabled {get; private set;}
    public Boolean btnAcceptEnabled {get; private set;}  
      
    public Boolean btnAssignEnabled {get; private set;} 
 
    
    public Boolean btnDeclineEnabled {get; private set;}      

    
    public Boolean btnChangeStateClicked   {get; private set;}   
    
    
    public Boolean btnSubmitForDispositionReviewEnabled {get; private set;}       
    public Boolean btnAcceptDispositionEnabled {get; private set;}   
    public Boolean btnRejectDispositionEnabled {get; private set;}      
    public Boolean btnImplementedEnabled {get; private set;}   
    public Boolean btnCloseEnabled {get; private set;}   
        

    public String FieldSetName {
            get; 
            private set
            {
                FieldSetName = value;
                try
                {
                    EditSectionName = SObjectType.NCR_Case__c.FieldSets.getMap().get(value).getLabel();                  
                }
                catch(Exception ex)
                {}
            }
    }
            
    public String EditSectionName {get; private set;}   

    private Map<String, NCR_State__c> NCRStateMap {get; set;}
    public NCR_State__c CurrentNCRState {get; private set;}
    public integer ActionCount {get; private set;}

    private String NewRecordState {get; set;}

    private void LoadStateMap()
    {
    
        Map<String, NCR_State__c> m = new Map<String, NCR_State__c>([
        select id, name, ActionCount__c,
            (select id, name, Edit_Screen_Required__c, 
                FieldSet__c, NCR_New_State__r.Name, NCR_Action__r.Name 
                from NCR_StateActions__r ) 
        from NCR_State__c
        ]);
        NCRStateMap = new Map<String, NCR_State__c>();
        for(String strid: m.keySet())
        {
            NCR_State__c state = (NCR_State__c)m.get(strid);
            NCRStateMap.put((String)state.Name, (NCR_State__c)state);
        }    
    }
    private void LoadFieldSetFields()
    {
       List<string> fields = new List<String>{'Name','NCR_Case_State__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldName);

            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }    
    }
       
    public NCR_ViewPageControllerX_v0(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();

       List<string> fields = new List<String>{'Name','NCR_Case_State__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldName);

            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }



        

        this.stdcontroller = stdController;
        //LoadFieldSetFields();
        
        this.ncr_record = (NCR_Case__c)stdController.getRecord();  

        ViewMode  = true;
        
        NewRecordState = '';
        
        LoadStateMap();

        
        LoadCurrentStateActions();
        


        SetButtonFlags();
    }
    
    
    private void LoadCurrentStateActions()
    {
        this.ActionCount = 0;
        this.CurrentNCRState = NCRStateMap.get(ncr_record.NCR_Case_State__c);
        if(currentNCRState == null)
        {
        //throw error
            return;
        }
        this.ActionCount  = (Integer)currentNCRState.ActionCount__c;
    }
    

    public PageReference NCR_GenericAction(Integer actionNumber)
    {
        NCR_StateAction__c stateAction = this.CurrentNCRState.NCR_StateActions__r[actionNumber-1];
    
        if(stateAction.NCR_New_State__r.Name != null)
        {
          NewRecordState  = stateAction.NCR_New_State__r.Name;
        }
        
        if(stateAction.FieldSet__c != null)
        {
          FieldSetName = stateAction.FieldSet__c;
        }        
        if(stateAction.Edit_Screen_Required__c)
        {
             ViewMode = false;
             btnChangeStateClicked = true;
        }        
         return null;    
    }
    public PageReference NCR_Action1()
    {
        return NCR_GenericAction(1);
    }    
    public PageReference NCR_Action2()
    {
        return NCR_GenericAction(2);
    }
    public PageReference NCR_Action3()
    {
        return NCR_GenericAction(3);
    }


    public PageReference NCR_Edit()
    {
        ViewMode = false;
        return null;
    }
    public PageReference NCR_Save()
    {
        ViewMode = true;
        ncr_record.NCR_Case_State__c = NewRecordState;
        NewRecordState = '';
        this.stdcontroller.save();
        SetButtonFlags();
        return null;
    }
    public PageReference NCR_CancelEdit()
    {
        ViewMode = true;
        SetButtonFlags();
        this.stdcontroller.cancel();
        return null;
    }


    private void SetButtonFlags()
    {

        btnEditEnabled = false;

        btnSubmitEnabled = false;
        btnAcceptEnabled = false; 
        btnAcceptEnabled = false; 
        btnAssignEnabled = false;
        btnDeclineEnabled = false;
        btnSubmitForDispositionReviewEnabled = false;
        btnAcceptDispositionEnabled = false;
        btnRejectDispositionEnabled = false;  
        btnImplementedEnabled = false;  
        btnCloseEnabled = false;  
        
        
        btnChangeStateClicked = false;
        
        
        if(ncr_record.NCR_Case_State__c == 'Created') // and user is originator
        {
            btnEditEnabled = true;
            btnSubmitEnabled = true;
            return;
        }    
        if(ncr_record.NCR_Case_State__c == 'Submitted') // and user is QA lead
        {
            btnAcceptEnabled = true;
            btnDeclineEnabled = true; // do we need 2 different declines? one for complete decline, one for request more information?
            
            return;            
        }  
        if(ncr_record.NCR_Case_State__c == 'Accepted') // and user is QA lead
        {
            btnAssignEnabled = true;
            return;            
        }  
        //Submit for Disposition Review
        if(ncr_record.NCR_Case_State__c == 'Assigned') // and user is Assignee
        {
        
            btnEditEnabled = true;
            btnSubmitForDispositionReviewEnabled = true;
            
            FieldSetName = 'DevelopDispositionPlan';
            EditSectionName = SObjectType.NCR_Case__c.FieldSets.getMap().get(FieldSetName).getLabel();            

            return;            
        }
        //Review Disposition  
        if(ncr_record.NCR_Case_State__c == 'Review Disposition') // and user is QA lead
        {
            btnAcceptDispositionEnabled = true;
            btnRejectDispositionEnabled = true;  //btw after Disposition is rejected does it go back to 'Assigned' state?         
            return;            
        }                  
        
        //Ready to Implement
        if(ncr_record.NCR_Case_State__c == 'Ready to Implement') // and user is QA lead
        {
            btnImplementedEnabled = true;
            return;            
        }          
        //Implemented
        if(ncr_record.NCR_Case_State__c == 'Implemented') // and user is Assignee
        {
            btnCloseEnabled = true;
            return;            
        }          
    }

     public PageReference NCR_Submit() {
        ncr_record.NCR_Case_State__c = 'Submitted';
        NCR_Save();
        return null;
     }

     public PageReference NCR_Accept() {
        NewRecordState  = 'Accepted';
        NCR_Save();   
        return null;          
     }
     public PageReference NCR_Decline() {
         ViewMode = false;
         btnChangeStateClicked = true;
         
         
         NewRecordState  = 'Declined';
         FieldSetName = 'RejectNCR';
         
         return null;
     }

     public PageReference NCR_Assign() {
        ViewMode = false;
        btnChangeStateClicked = true;
        NewRecordState  = 'Assigned';
        FieldSetName   = 'AssignNCR';
        return null;     
     }
     public PageReference NCR_SubmitDispositionReview() {
        ViewMode = false;
        btnChangeStateClicked = true;
        NewRecordState   = 'Review Disposition';
        FieldSetName   = 'DevelopDispositionPlan';
        return null;
     }
     public PageReference NCR_AcceptDisposition() {
        NewRecordState  = 'Ready to Implement';
        NCR_Save();   
        return null;        
     }
     public PageReference NCR_RejectDisposition() {
        ViewMode = false;
        btnChangeStateClicked = true;
        NewRecordState   = 'Assigned';
        FieldSetName   = 'RejectDispositionNCR';
        return null;     
     }
     public PageReference NCR_Implemented() {
        ViewMode = false;
        btnChangeStateClicked = true;
        NewRecordState   = 'Implemented';
        FieldSetName   = 'ImplementDispositionPlan';
        return null;  
     }
     public PageReference NCR_Close() {
        NewRecordState  = 'Closed';
        NCR_Save();   
        return null;    
     }



    /*
    public PageReference redirect() {

            Profile p = [select name from Profile where id = :UserInfo.getProfileId()];

                      if ('US Portal User Profile'.equals(p.name)) 
                         {
                           PageReference customPage =  Page.USCP_Customer;
                           customPage.setRedirect(true);
                           
                           contactId = [Select contactid from User where id =:userID].contactId;
                           AccID  = [Select AccountID from Contact where id =:contactId].AccountId;


                           customPage.getParameters().put('id', AccID);
                           return customPage;
                          
                          } else {
                              return null; //otherwise stay on the same page  
                          }
   }
   */
}