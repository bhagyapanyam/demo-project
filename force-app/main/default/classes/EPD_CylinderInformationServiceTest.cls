/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_CylinderInformationService
History:        jschn 2019-07-10 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_CylinderInformationServiceTest {

    @IsTest
    static void getCylindersWrapped_withoutParam() {
        List<EPD_FormStructureCylinder> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_CylinderInformationService().getCylindersWrapped(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(null, result);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getCylindersWrapped_withEmptyParam() {
        List<EPD_Cylinder_Information__c> initialList = new List<EPD_Cylinder_Information__c>();
        Integer expectedRecordCount = initialList.size();
        List<EPD_FormStructureCylinder> result;

        Test.startTest();
        result = new EPD_CylinderInformationService().getCylindersWrapped(initialList);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedRecordCount, result.size());
    }

    @IsTest
    static void getCylindersWrapped_withProperParam() {
        List<EPD_Cylinder_Information__c> initialList = new List<EPD_Cylinder_Information__c> {
                (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                        new EPD_Cylinder_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        };
        Integer expectedRecordCount = initialList.size();
        List<EPD_FormStructureCylinder> result;

        Test.startTest();
        result = new EPD_CylinderInformationService().getCylindersWrapped(initialList);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedRecordCount, result.size());
    }

}