global virtual with sharing class DSP_CMS_Metric extends DSP_CMS_MetricController
{
    global override String getHTML()
    {
        return getMetric();
    }
}