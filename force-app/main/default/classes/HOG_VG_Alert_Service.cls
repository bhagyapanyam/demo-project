/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service for Vent Gas Alert object and processes. Supporting Alert creation process.
Test Class:     HOG_VG_Alert_ServiceTest
History:        jschn 19. 9. 2018 - Created.
                jschn 19.10. 2018 - W-001275 - W-001277 -> Refactored createVentGasAlerts method
                                                        to create all types of alerts required
                                                        within USs.
**************************************************************************************************/
public with sharing class HOG_VG_Alert_Service {

    public static List<HOG_Vent_Gas_Alert__c> generateAlerts(List<Location__c> locations,
            HOG_VG_AlertStrategyProvider alertStrategyProvider,
            HOG_VG_AlertProvider alertProvider) {

        List<HOG_Vent_Gas_Alert__c> alerts = new List<HOG_Vent_Gas_Alert__c>();

        for(Location__c location : locations) {
            if(alertStrategyProvider.shouldCreate(location)) {
                alerts.add(alertProvider.createAlert(location));
            }
        }

        return alerts;
    }

    public static List<HOG_Vent_Gas_Alert__c> getActiveAlerts(Set<Id> locationIds) {
        return [SELECT Status__c, Well_Location__c, Type__c
            FROM HOG_Vent_Gas_Alert__c
            WHERE Well_Location__c IN : locationIds
                AND Status__c IN ('Not Started', 'In Progress')];
    }

}