/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service for Vent Gas Exemption Request object and processes.
Test Class:     HOG_VG_Alert_Exemption_TriggerHelperTest
History:        jschn 19. 9. 2018 - Created.
**************************************************************************************************/
public with sharing class HOG_VG_Alert_Exemption_Request_Service {

    public static List<HOG_Vent_Gas_Alert_Exemption_Request__c> getActiveExemptionsByLocationId(Set<Id> locationIds) {
        return [SELECT Name, Well_Location__c, Status__c, Expiration_Date__c, Alert_Type__c
                FROM HOG_Vent_Gas_Alert_Exemption_Request__c
                WHERE Well_Location__c IN : locationIds
                    AND ((Status__c = 'Approved'
                            AND Expiration_Date__c != NULL
                            AND Expiration_Date__c > TODAY)
                        OR Status__c = 'Submitted'
                        OR Status__c = 'Revoked')];
    }

}