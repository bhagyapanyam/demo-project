/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Enum that is used as Mode for Assignment logic.
                Currently it has 2 items
                    - SKIP_NEW_ASSIGNMENTS - which skips logic that creates new assignments
                    - BUILD_NEW_ASSIGNMENTS - which runs logic that creates new assignments
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public enum VTT_WOA_Assignment_Action {
    SKIP_NEW_ASSIGNMENTS,
    BUILD_NEW_ASSIGNMENTS
}