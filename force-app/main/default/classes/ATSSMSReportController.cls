public class ATSSMSReportController{
    public ATSWrapperUtilities.ATSQuoteWrapper quote{get; set;}
    
    public Id tenderId {get; set;}
    public Id accountId {get; set;}
    public List<ATSWrapperUtilities.ATSOpportunityWrapper> o{get;set;}
    public Decimal sequenceNumber{get;set;}
    public String CustomerType {get;set;}

 /* --- move to ATSWrapperUtilities.ATSOpportunityWrapper
    public Boolean IncludeAxle5{get; set;}
    public Boolean IncludeAxle6{get; set;}
    public Boolean IncludeAxle7{get; set;}
    public Boolean IncludeAxle8{get; set;} */
    
    List<String> opportunityList;
    
    List<ATSWrapperUtilities.ATSQuoteWrapper> quoteWrapperList{get; set;}
        
    
    public ATSSMSReportController() {
        tenderId = ApexPages.currentPage().getParameters().get('tenderId');
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        //opportunityList =  ApexPages.currentPage().getParameters().get('opportunityList').split(',');
        initializeQuoteWrapper();   
    }
    
    public void initializeQuoteWrapper() {
        quote = new ATSWrapperUtilities.ATSQuoteWrapper();
        
        ATS_Parent_Opportunity__c tender = [SELECT 
                                                Id, 
                                                Name,
                                                Tender__c, 
                                                Price_Valid_From__c, 
                                                Price_Valid_To__c, 
                                                Bid_Due_Date_Time__c, 
                                                Bid_Description__c, 
                                                Marketer__r.Name, 
                                                Marketer__c,
                                                For_Season__c,
                                                Trade_Class__c,
                                                Destination_City_Province__c,
                                                Broker__c
                                            FROM 
                                                ATS_Parent_Opportunity__c 
                                            WHERE 
                                                Id =:tenderId];
        
        
        ATS_Tender_Customers__c customer = [SELECT 
                                                Id, 
                                                Comments__c,
                                                Account__c, 
                                                Account__r.Name, 
                                                Account__r.AccountNumber,
                                                ATS_Parent_Opportunity__c, 
                                                Contact__c, 
                                                Contact__r.Name, 
                                                Contact__r.Fax,
                                                Account__r.ATS_Customer_Type__c 
                                            FROM 
                                                ATS_Tender_Customers__c 
                                            WHERE 
                                                Account__c =:accountId AND ATS_Parent_Opportunity__c =:tenderId][0];
        
        CustomerType  = customer.Account__r.ATS_Customer_Type__c; 
        if(CustomerType == null) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unknown Customer Type for account ' + customer.Account__r.Name);
            ApexPages.addMessage(myMsg);
            return;
        }
        
        quote.tender = tender ;
        quote.accountId = this.accountId;
        quote.accountName = customer.Account__r.Name;
        quote.accountNumber = customer.Account__r.AccountNumber;
        quote.CustomerComments  = customer.Comments__c;        
        
        quote.offerEndDate = tender.Bid_Due_Date_Time__c;
        // need to do this to include proper time zone into the output
        quote.offerEndDateString = tender.Bid_Due_Date_Time__c.format('h:mm a (z) MM/dd/yyyy ');
        
        quote.contactId = customer.Contact__c;
        quote.contactName = customer.Contact__r.Name;
        quote.contactFaxNumber = customer.Contact__r.Fax;
        
        quote.tenderId = this.tenderId;
        quote.tenderNumber = tender.Tender__c;
        quote.tenderName = tender.Name;
        
        quote.priceValid = tender.Price_Valid_From__c.format() + ' - ' + tender.Price_Valid_To__c.format();
        quote.project = tender.Bid_Description__c;
        quote.additionalComments = tender.Bid_Description__c; // need to change this value to customer specific comments (from junction)
        quote.signatureName = tender.Marketer__r.Name;
        quote.ForSeason = tender.For_Season__c;
        quote.TradeClass= tender.Trade_Class__c;
        quote.Destination= tender.Destination_City_Province__c;
        quote.Broker = tender.Broker__c;
        
        // now lets get all opportunity and figure out what we need to filter out based on customer type
        // Account__r.ATS_Customer_Type__c 
        quote.setOpportunities([SELECT Id, 
                                    Opportunity_ATS_Product_Category__c, 
                                    RecordType.Name,
                                    PO__c,
                                    SMS_Contract__c,
                                    Pay_out_Handling_Fees__c,
                                    Sales_Comments__c,
                                    (SELECT 
                                        Id, 
                                        PricebookEntry.Name, 
                                        PricebookEntry.Id, 
                                        Price_Type__c, 
                                        UnitPrice, 
                                        Unit__c, 
                                        Quantity, 
                                        TotalPrice, 
                                        PricebookEntryId, 
                                        OpportunityId, 
                                        ListPrice,
                                        Description, 
                                        Anti_Strip__c, 
                                        Additive__c,
                                        Currency__c,
                                        Sales_Price__c,
                                        Axle_5_Price__c,
                                        Axle_6_Price__c,
                                        Axle_7_Price__c,
                                        Axle_8_Price__c,
                                        Sales_Price_Formula__c,
                                        Axle_5_Price_Formula__c,
                                        Axle_6_Price_Formula__c,
                                        Axle_7_Price_Formula__c,
                                        Axle_8_Price_Formula__c,
                                        Include_Sales_Price__c,
                                        Include_Axle_5__c,
                                        Include_Axle_6__c,
                                        Include_Axle_7__c,
                                        Include_Axle_8__c,
                                        PricebookEntry.Product2.Density__c,
                                        PricebookEntry.Product2.ProductCode,
                                        PricebookEntry.Product2.Product_AKA_SMS_Name__c
                                     FROM 
                                        OpportunityLineItems
                                     Order By PricebookEntry.Name),
                                    (SELECT  
                                        Id,
                                        Husky_Supplier_1_Selected__c,
                                        Husky_Supplier_2_Selected__c,                                  
                                        Husky_Supplier_1__c,
                                        Supplier_1_Carrier__c,
                                        Supplier_1_Min_Load__c,
                                        Supplier_1_Rate__c,
                                        Supplier_1_Unit__c,
                                        Husky_Supplier_2__c,
                                        Supplier_2_Carrier__c,
                                        Supplier_2_Min_Load__c,
                                        Supplier_2_Rate__c,
                                        Supplier_2_Unit__c,
                                        Emulsion_Rate5_Supplier1__c,
                                        Emulsion_Rate5_Supplier2__c,
                                        Emulsion_Rate6_Supplier_1__c,
                                        Emulsion_Rate6_Supplier2__c,
                                        Emulsion_Rate8_Supplier1__c,
                                        Emulsion_Rate8_Supplier2__c,
                                        Emulsion_Min_Load5_Supplier1__c,
                                        Emulsion_Min_Load5_Supplier2__c,
                                        Emulsion_Min_Load6_Supplier1__c,    
                                        Emulsion_Min_Load6_Supplier2__c,
                                        Emulsion_Min_Load8_Supplier1__c,
                                        Emulsion_Min_Load8_Supplier2__c,
                                        Emulsions_Carrier_Supplier1__c,
                                        Emulsions_Carrier_Supplier2__c,
                                        Prices_F_O_B__c,
                                        Fleet_ID__c,
                                        Rail_Acknowledgment__c,
                                        RR_City__r.Name,
                                        RR_Address__r.Name,
                                        Invoicing_Bill_To__r.Sequence__c,
                                        Freight_to_COLLECT__c,
                                        Rail_Car_Routing__c,
                                        Freight_to_Pay__c,
                                        Freight_Pay_Method__c,
                                        Emulsion_Rate7_Supplier1__c,
                                        Emulsion_Rate7_Supplier2__c,
                                        Emulsion_Rate7_Competitor1__c,
                                        Emulsion_Rate7_Competitor2__c,
                                        Emulsion_Rate7_Competitor3__c,
                                        Emulsion_Rate7_Competitor4__c,
                                        Emulsion_Min_Load7_Supplier1__c,
                                        Emulsion_Min_Load7_Supplier2__c                            
                                    FROM 
                                        Freight_ATS__r)
                                FROM 
                                    Opportunity 
                                WHERE 
                                    StageName = 'Won' AND Opportunity_ATS_Product_Category__c = :tenderId], CustomerType);
                                //FROM Opportunity WHERE id in :opportunityList];    

        o = quote.getOpportunities();
        ATS_Freight__c  f;
        if(o!=null)
        f= quote.getOpportunities()[0].Freight ;
        if(f!=null) sequenceNumber = [Select Invoicing_Bill_To__r.Sequence__c from ATS_Freight__c   where Id = :f.Id limit 1][0].Invoicing_Bill_To__r.Sequence__c;
        
   /*   --- move to ATSWrapperUtilities.ATSOpportunityWrapper ----
        IncludeAxle5 = False;
        IncludeAxle6 = False;
        IncludeAxle7 = False;
        IncludeAxle8 = False;
        
        for(Opportunity opp: opportunities) {
            // lets extract opportunity type without 'ATS' or 'Product Category'
            String opportynityType = opp.RecordType.Name;
            opportynityType = opportynityType.Replace('ATS:','');
            opportynityType = opportynityType.Replace('Product Category','').trim();   
            
            if(CustomerType.containsIgnoreCase(opportynityType)) {
                oppWrapperList.add(new ATSWrapperUtilities.ATSOpportunityWrapper(opp));
            }
            
  //          if(opp.RecordType.Name == 'ATS: Emulsion Product Category') {
                for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                    IncludeAxle5 = IncludeAxle5 || oli.Include_Axle_5__c;
                    IncludeAxle6 = IncludeAxle6 || oli.Include_Axle_6__c;
                    IncludeAxle7 = IncludeAxle7 || oli.Include_Axle_7__c;
                    IncludeAxle8 = IncludeAxle8 || oli.Include_Axle_8__c;
                }
    //        }
        } */
        
    }
    /*
    private static String GetResourceURL(String resourceName) {
    
        List<StaticResource> resourceList = [SELECT 
                                                Name, 
                                                NamespacePrefix, 
                                                SystemModStamp 
                                             FROM 
                                                StaticResource 
                                             WHERE 
                                                Name = :resourceName];
                        
        if (resourceList.size() == 1) {
            String namespace = resourceList[0].NamespacePrefix;
            return '/resource/' 
                    + resourceList[0].SystemModStamp.getTime() + '/' 
                    + (namespace != null && namespace != '' ? namespace + '__' : '') 
                    + resourceName; 
        } 
        else 
            return '';
    }*/
}