@isTest
public with sharing class CarsealAuditReportTest {

    @TestSetup
    static void setup(){
        Plant__c plant = new Plant__c(Name = 'Unit Test Plat');
        insert plant;

        Unit__c unit1 = new Unit__c(Name = 'Unit Test Unit 1', Plant__c = plant.Id);
        Unit__c unit2 = new Unit__c(Name = 'Unit Test Unit 2', Plant__c = plant.Id);
        Unit__c unit3 = new Unit__c(Name = 'Unit Test Unit 3', Plant__c = plant.Id);

        insert unit1;
        insert unit2;
        insert unit3;

        Carseal__c carseal1 = new Carseal__c(
            Description__c = 'Description 1', Equipment_Protected__c = '0H-P-241H-1', 
            Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
            Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
            P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
            Size__c = '1 1/2"', Unit__c = unit1.Id
        );  
        
        Carseal__c carseal2 = new Carseal__c(
            Description__c = 'Description 2', Equipment_Protected__c = '0H-P-241H-2', 
            Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
            Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
            P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
            Size__c = '1 1/2"', Unit__c = unit2.Id
        );  
        
        Carseal__c carseal3 = new Carseal__c(
            Description__c = 'Description 3', Equipment_Protected__c = '0H-P-241H-3', 
            Justification__c = 'Exchanger Protection',  P_ID_Status__c = 'CSO', 
            Special_Consideration__c = 'Impairment Permit; vessel outage', Line__c = '0H-RF-AZ-1"-608', 
            P_ID__c = '0L-PI-001-1', Plant__c = plant.Id, Reference_Equipment__c = '0L-E-145', 
            Size__c = '1 1/2"', Unit__c = unit3.Id
        );             

        insert carseal1; 
        insert carseal2;  
        insert carseal3; 

        Carseal_Status__c carsealStatus1 = new Carseal_Status__c(Carseal__c = carseal1.Id, Date__c = System.today() - 10);
        Carseal_Status__c carsealStatus2 = new Carseal_Status__c(Carseal__c = carseal1.Id, Date__c = System.today() - 30);
        Carseal_Status__c carsealStatus3 = new Carseal_Status__c(Carseal__c = carseal2.Id, Date__c = System.today() - 90);
        Carseal_Status__c carsealStatus4 = new Carseal_Status__c(Carseal__c = carseal3.Id, Date__c = System.today() - 150);

        insert carsealStatus1;
        insert carsealStatus2;
        insert carsealStatus3;
        insert carsealStatus4;

    }

    @isTest
    private static void testCarSealReportInitialLoad() {
        CarsealAuditReport statusReport = new CarsealAuditReport();
        Map<String, Map<String, List<Carseal_Status__c>>> carsealReport = statusReport.getCarsealReport();

        System.assertEquals(2, carsealReport.size());
    }  
    
    @isTest
    private static void testCarSealReportSelectedUnits() {
        String selectedUnits = 'Unit Test Unit 1, Unit Test Unit 3';
        Test.setCurrentPageReference(new PageReference('Page.CarsealAuditReport'));
        System.currentPageReference().getParameters().put('unit', selectedUnits);
                
        CarsealAuditReport auditReport = new CarsealAuditReport();
        auditReport.runReport();

        Map<String, Map<String, List<Carseal_Status__c>>> carsealReport = auditReport.getCarsealReport();
        System.assertEquals(1, carsealReport.size());   
    }

    @isTest
    private static void testCaesealExportToPdf() {
        CarsealAuditReport auditReport = new CarsealAuditReport();
        Map<String, Map<String, List<Carseal_Status__c>>> carsealReport = auditReport.getCarsealReport();
        PageReference pdfPage = auditReport.exportToPdf();

        System.assertNotEquals(null, pdfPage);          
    }    

}