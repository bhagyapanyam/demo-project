@isTest
private class VTT_UpdateActivitySchedulableTest {
	@isTest static void Test_Schedule() {
    
	    Test.startTest();
	      List<String> jobNames = new List<String> {
				  VTT_UpdateActivitySchedulable.UPDATE_ACTIVITY_STATUS_JOB_NAME,
				  VTT_UpdateActivitySchedulable.REVERT_ACTIVITY_JOB_NAME,
				  VTT_UpdateActivitySchedulable.RETRY_ERROR_CONFIRMATION_JOB_NAME};

	      VTT_UpdateActivitySchedulable.scheduleJob();

	      List<CronTrigger> cts = [SELECT Id, CronJobDetail.Name
	                        FROM CronTrigger
	                        Where CronJobDetail.Name In :jobNames];
	      System.assertEquals(jobNames.size(), cts.size());
	    Test.stopTest();
  	}
	
}