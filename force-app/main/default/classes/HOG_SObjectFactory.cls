/*************************************************************************************************\
Author:         Marcel Brimus & Jakub Schon
Created from:   https://github.com/dhoechst/Salesforce-Test-Factory/blob/master/src/classes/TestFactory.cls
Company:        Husky Energy
Description:    SObject factory for Testing purposes. 
History:        jschn 05.07.2018 - Created.
                jschn 06.13.2018 - Added HOG_Settings field defaults 
                                 - FIX for autoNumber name field in createSObjectList
                                 - Major refactor
                                 - Added ability to define specific defaultClass
                mbrimus 06.18.2018  - Added business unit fd
                                    - Added well event fd
                mpanak 25.07.2018   - Added FLOCs: Yard, FEL, System, Sub-System
**************************************************************************************************/
@SuppressWarnings('ApexUnusedDeclaration')
@IsTest
public class HOG_SObjectFactory {
	public static final String CLASS_NAME = 'HOG_SObjectFactory';

	private static final String CLASS_NAME_ERROR = 'Invalid defaults class';

	private static final String UNSUPPORTED_CHARS = '__(c|C)$|__';

	/**
	 * Create record with default field values.
	 * Usable only for objects which has Field Defaults defined in this class.
	 * Doesn't perform insert.
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account());
	 * @param  obj Object for which you want to create new record.
	 * @return     SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj) {
		return createSObject(obj, CLASS_NAME);
	}

	/**
	 * Create record with default field values.
	 * Usable only for objects which has Field Defaults defined in this class.
	 * Insert performed on demand.
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
	 * @param  obj      Object for which you want to create new record.
	 * @param  doInsert Indication if insert will be performed.
	 * @return          SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj,
			Boolean doInsert) {
		return createSObject(obj, CLASS_NAME, doInsert);
	}

	/**
	 * Create record with default field values.
	 * Doesn't perform insert.
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), CLASS_NAME);
	 * @param  obj              Object for which you want to create new record.
	 * @param  defaultClassName Class name which contains field defaults for requested object.
	 * @return                  SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj,
			String defaultClassName) {
		String objectName = String.valueOf(obj.getSObjectType());
		String fieldDefaultClassImplementationName = objectName.replaceAll(UNSUPPORTED_CHARS, '')
				+ 'Defaults';
		return createSObject(obj, defaultClassName, fieldDefaultClassImplementationName);
	}

	/**
	 * Create record with default field values
	 * Doesn't perform insert
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), CLASS_NAME, 'AccountDefaults');
	 * @param  obj                                 Object for which you want to create new record.
	 * @param  defaultClassName                    Class name which contains class with field defaults for requested object.
	 * @param  fieldDefaultClassImplementationName Class name which contains field defaults for requested object.
	 * @return                                     SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj,
			String defaultClassName,
			String fieldDefaultClassImplementationName) {
		String fieldDefaultClassName = defaultClassName
				+ '.'
				+ fieldDefaultClassImplementationName.replaceAll(UNSUPPORTED_CHARS, '');
		Type t = Type.forName(fieldDefaultClassName);
		if (t == null) throw new SObjectFactoryException(CLASS_NAME_ERROR);
		SObjectFieldDefaults defaults = (SObjectFieldDefaults) t.newInstance();
		addFieldDefaults(obj, defaults.getFieldDefaults());
		return obj;
	}

	/**
	 * Create record with default field values
	 * Insert performed on demand
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), CLASS_NAME, 'AccountDefaults', true);
	 * @param  obj                                 Object for which you want to create new record.
	 * @param  defaultClassName                    Class name which contains class with field defaults for requested object.
	 * @param  fieldDefaultClassImplementationName Class name which contains field defaults for requested object.
	 * @param  doInsert                            Indication if insert will be performed.
	 * @return                                     SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj,
			String defaultClassName,
			String fieldDefaultClassImplementationName,
			Boolean doInsert) {
		obj = createSObject(obj, defaultClassName, fieldDefaultClassImplementationName);
		if (doInsert) insert obj;
		return obj;
	}

	/**
	 * Create record with default field values.
	 * Insert performed on demand.
	 * Usage: Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), CLASS_NAME, true);
	 * @param  obj              Object for which you want to create new record.
	 * @param  defaultClassName Class name which contains field defaults for requested object.
	 * @param  doInsert         Indication if insert will be performed.
	 * @return                  SObject record with field defaults.
	 */
	public static SObject createSObject(SObject obj,
			String defaultClassName,
			Boolean doInsert) {
		SObject retObject = createSObject(obj, defaultClassName);
		if (doInsert) insert retObject;
		return retObject;
	}

/**
CREATE LIST 
*/
	/**
	 * Create list of records with default values and iterated name field.
	 * Usable only for objects which has Field Defaults defined in this class.
	 * Doesn't perform insert.
	 * Usage: List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 10);
	 * @param  obj          Object for which you want to create new record.
	 * @param  numOfObjects Indicates number of records which will be created.
	 * @return              List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects) {
		return createSObjectList(obj, numOfObjects, (String) null);
	}

	/**
	 * Create list of records with default values and iterated name field.
	 * Usable only for objects which has Field Defaults defined in this class.
	 * Insert performed on demand.
	 * Usage: List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 10, true);
	 * @param  obj          Object for which you want to create new record.
	 * @param  numOfObjects Indicates number of records which will be created.
	 * @param  doInsert     Indication if insert will be performed.
	 * @return              List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects,
			Boolean doInsert) {
		return createSObjectList(obj, numOfObjects, (String) null, false);
	}

	/**
	 * Create list of records with default values and iterated name field.
	 * Doesn't perform insert.
	 * Usage: List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 10, CLASS_NAME);
	 * @param  obj              Object for which you want to create new record.
	 * @param  numOfObjects     Indicates number of records which will be created.
	 * @param  defaultClassName Class name which contains class with field defaults for requested object.
	 * @return                  List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects,
			String defaultClassName) {
		return createSObjectList(obj, numOfObjects, defaultClassName, (String) null);
	}

	/**
	 * Create list of records with default values and iterated name field.
	 * Insert performed on demand.
	 * Usage: List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 10, CLASS_NAME, true);
	 * @param  obj              Object for which you want to create new record.
	 * @param  numOfObjects     Indicates number of records which will be created.
	 * @param  defaultClassName Class name which contains field defaults for requested object.
	 * @param  doInsert         Indication if insert will be performed.
	 * @return                  List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects,
			String defaultClassName,
			Boolean doInsert) {
		List<SObject> retList = createSObjectList(obj, numOfObjects, defaultClassName);
		if (doInsert) insert retList;
		return retList;
	}

	/**
	 * Create list of records with default values and iterated name field.
	 * Insert performed on demand.
	 * Usage: List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 10, CLASS_NAME, 'AccountDefaults', true);
	 * @param  obj                                 Object for which you want to create new record.
	 * @param  numOfObjects                        Indicates number of records which will be created.
	 * @param  defaultClassName                    Class name which contains class with field defaults for requested object.
	 * @param  fieldDefaultClassImplementationName Class name which contains field defaults for requested object.
	 * @param  doInsert                            Indication if insert will be performed.
	 * @return                                     List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects,
			String defaultClassName,
			String fieldDefaultClassImplementationName,
			Boolean doInsert) {
		List<SObject> retList = createSObjectList(obj, numOfObjects, defaultClassName, fieldDefaultClassImplementationName);
		if (doInsert) insert retList;
		return retList;
	}

	/**
	 * Create list of records with default values and iterated name field.
	 * Doesn't perform insert.
	 * Usage: List<Account> accounts = (List<Account>) SObjectFactory.createSObjectList(new Account(), 10, CLASS_NAME, 'AccountDefaults');
	 * @param  obj                                 Object for which you want to create new record.
	 * @param  numOfObjects                        Indicates number of records which will be created.
	 * @param  defaultClassName                    Class name which contains class with field defaults for requested object.
	 * @param  fieldDefaultClassImplementationName Class name which contains field defaults for requested object.
	 * @return                                     List of SObjects record with field defaults.
	 */
	public static List<SObject> createSObjectList(SObject obj,
			Integer numOfObjects,
			String defaultClassName,
			String fieldDefaultClassImplementationName) {
		SObject newObj = createTemplateObject(obj, defaultClassName, fieldDefaultClassImplementationName);

		String objectName = String.valueOf(obj.getSObjectType());
		String nameField = getNameField(objectName);
		Double rnd = Math.random();

		List<SObject> objects = new List<SObject>();
		for (Integer i = 0; i < numOfObjects; i++) {
			SObject clonedObj = newObj.clone(false, true);

			if (!isFieldAutoNumber(objectName, nameField)) {
				clonedObj.put(nameField, (String) clonedObj.get(nameField) + ' ' + String.valueOf(rnd) + i);
			}

			objects.add(clonedObj);
		}
		return objects;
	}

	/**
	 * Create Template record with field defaults based on parameters.
	 * If no fieldDefaultClassImplementationName is provided use SObject API Name to get FieldDefaults class
	 * If no defaultClassName is provided use SObjectFactory class as place to look for field defaults
	 * @param  obj                                 Which will be used as template
	 * @param  defaultClassName                    Class name which would be used to look for field defaults
	 * @param  fieldDefaultClassImplementationName Class containing field defaults for SObject
	 * @return                                     Template SObject with field defaults
	 */
	private static SObject createTemplateObject(SObject obj,
			String defaultClassName,
			String fieldDefaultClassImplementationName) {
		SObject newObj;
		if (String.isBlank(fieldDefaultClassImplementationName)) {
			if (String.isBlank(defaultClassName)) {
				newObj = createSObject(obj);
			} else {
				newObj = createSObject(obj, defaultClassName);
			}
		} else {
			newObj = createSObject(obj, defaultClassName, fieldDefaultClassImplementationName);
		}
		return newObj;
	}

	/**
	 * Get API name for Name field.
	 * @param  objectName SObject API Name for which you want to get Name field name
	 * @return            Name field API name
	 */
	private static String getNameField(String objectName) {
		String nameField = nameFieldMap.get(objectName);
		if (String.isBlank(nameField)) nameField = 'Name';
		return nameField;
	}

	/**
	 * Adds default values to record.
	 * @param obj      Record for which fields should be populated.
	 * @param defaults Field Defaults map.
	 */
	private static void addFieldDefaults(SObject obj, Map<Schema.SObjectField, Object> defaults) {
		Map<String, Object> populatedFields = obj.getPopulatedFieldsAsMap();
		for (Schema.SObjectField field : defaults.keySet()) {
			if (!populatedFields.containsKey(String.valueOf(field))) {
				obj.put(field, defaults.get(field));
			}
		}
	}

	/**
	 * Checks if Field on SObject is Autonumber
	 * @param  objectName Object API Name to use
	 * @param  nameField  Field API Name to check
	 * @return            if applies.
	 */
	private static Boolean isFieldAutoNumber(String objectName, String nameField) {
		return Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(nameField).getDescribe().isAutoNumber();
	}

	/**
	 * Contains SObjects with different API names for Name fields.
	 */
	private static Map<String, String> nameFieldMap = new Map<String, String>{
			'Contact' => 'LastName',
			'Case' => 'Subject'
	};

	/**
	 * Exception class.
	 */
	public class SObjectFactoryException extends Exception {
	}

/**
INTERFACE FOR DEFAULTS
*/
	/**
	 * Interface for Field Defaults.
	 * Contains getter which return Map<Schema.SObjectField, Object> with defaults.
	 */
	public interface SObjectFieldDefaults {
		Map<Schema.SObjectField, Object> getFieldDefaults();
	}

/**
STANDARD SOBJECT DEFAULTS
*/
	/**
	 * Fields defaults for Account Standard SObject
	 */
	public class AccountDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Account.Name => 'Test Account'
			};
		}
	}

	/**
	 * Fields defaults for Contact Standard SObject
	 */
	public class ContactDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Contact.FirstName => 'First',
					Contact.LastName => 'Last'
			};
		}
	}

	/**
	 * Fields defaults for Opportunity Standard SObject
	 */
	public class OpportunityDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Opportunity.Name => 'Test Opportunity',
					Opportunity.StageName => 'Closed Won',
					Opportunity.CloseDate => System.today()
			};
		}
	}

	/**
	 * Fields defaults for Case Standard SObject
	 */
	public class CaseDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Case.Subject => 'Test Case'
			};
		}
	}

	/**
	 * Fields defaults for User Standard SObject (using Standard HOG - General User profile)
	 */
	public class UserDefaults implements SObjectFieldDefaults {

		Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User'];

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					User.EmailEncodingKey => 'UTF-8',
					User.LanguageLocaleKey => 'en_US',
					User.LocaleSidKey => 'en_US',
					User.TimeZoneSidKey => 'America/Denver',
					User.ProfileId => p.Id
			};
		}
	}

/**
FLOC DEFAULTS
*/
	/**
	 * Fields defaults for Route__c Custom SObject
	 */
	public class HOG_SettingsDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					HOG_Settings__c.Fluid_Day_Dispatch_Cutoff_Hour__c => '18:00',
					HOG_Settings__c.Fluid_Night_Dispatch_Cutoff_Hour__c => '9:00'
			};
		}
	}

	/**
	 * Fields defaults for Route__c Custom SObject
	 */
	public class RouteDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Route__c.Name => 'Test Route',
					Route__c.Route_Number__c => 'Test Route',
					Route__c.Fluid_Management__c => true
			};
		}
	}

	/**
	 * Fields defaults for Dispatch_Desk__c custom SObject
	 */
	public class Dispatch_DeskDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Dispatch_Desk__c.Name => 'Desk 1',
					Dispatch_Desk__c.Phone__c => '403-555-1234'
			};
		}
	}

	/**
	 * Fields defaults for Business_Unit__c Custom SObject
	 */
	public class Business_UnitDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Business_Unit__c.Name => 'Test Business Unit'
			};
		}
	}

	/**
	 * Fields defaults for Business_Department__c Custom SObject
	 */
	public class Business_DepartmentDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Business_Department__c.Name => 'Test Department'
			};
		}
	}

	/**
	 * Fields defaults for Operating_District__c Custom SObject
	 */
	public class Operating_DistrictDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Operating_District__c.Name => 'Test District'
			};
		}
	}

	/**
	 * Fields defaults for Field__c Custom SObject
	 */
	public class FieldDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Field__c.Name => 'Test Field'
			};
		}
	}

	/**
	 * Fields defaults for Location__c Custom SObject
	 */
	public class LocationDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Location__c.Name => 'Test Location',
					Location__c.Fluid_Location_Ind__c => true,
					Location__c.Location_Name_for_Fluid__c => 'Test Fluid Location',
					Location__c.Functional_Location_Category__c => 4
			};
		}
	}

	/**
	 * Fields defaults for Facility__c Custom SObject
	 */
	public class FacilityDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Facility__c.Name => 'Test Facility',
					Facility__c.Facility_Name_for_Fluid__c => 'Test Fluid Facility',
					Facility__c.Fluid_Facility_Ind__c => true,
					Facility__c.Fluid_Facility_Ind_Origin__c => true,
					Facility__c.Functional_Location_Category__c => 3
			};
		}
	}

	/**
	 * Fields defaults for Yard__c Custom SObject
	 */
	public class YardDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Yard__c.Name => 'Test Yard'
			};
		}
	}

	/**
	 * Fields defaults for Functional_Equipment_Level__c Custom SObject
	 */
	public class Functional_Equipment_LevelDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Functional_Equipment_Level__c.Name => 'Test FEL'
			};
		}
	}

	/**
	 * Fields defaults for System__c Custom SObject
	 */
	public class SystemDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					System__c.Name => 'System'
			};
		}
	}

	/**
	 * Fields defaults for SubSystem__c Custom SObject
	 */
	public class Sub_SystemDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Sub_System__c.Name => 'Test Sub System'
			};
		}
	}

	/**
	 * Fields defaults for Well_Event__c Custom SObject
	 */
	public class Well_EventDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Well_Event__c.Name => 'testEventName'
			};
		}
	}

	/**
	 * Fields defaults for Equipment__c Custom SObject
	 */
	public class EquipmentDefaults implements SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Equipment__c.Equipment_Category__c => 'D',
					Equipment__c.Object_Type__c => 'VESS_ATMOS'
			};
		}
	}

	/**
	 * Fields defaults for Equipment_Tank__c Custom SObject
	 */
	public class Equipment_TankDefaults implements SObjectFieldDefaults {

		String defaultLabel = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(Equipment_Tank__c.Tank_Label__c.getDescribe().getSobjectField());

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
					Equipment_Tank__c.SCADA_Tank_Level__c => 87.654,
					Equipment_Tank__c.Low_Level__c => 50,
					Equipment_Tank__c.Tank_Size_m3__c => 160,
					Equipment_Tank__c.Tank_Label__c => defaultLabel
			};
		}
	}

}