/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Facility__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class FacilityTestData
{
    public static Facility__c createFacility(String name, Id routeId, Id fieldId)
    {                
        Facility__c results = new Facility__c
            (           
                Name = name,
                Plant_Section__c = routeId, 
                Operating_Field_AMU__c = fieldId
            );            

        return results;
    }
}