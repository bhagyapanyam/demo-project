global class WCPRoles
{
    public static Set<String> roleDeveloperName2schemaNames(String roleDeveloperName)
    {
        if(roleDeveloperName.startsWith('Western_Canada_Production'))   return new Set<String>{'NONE',
                                                                                               'WCP_SL',
                                                                                               'GRD',
                                                                                               'WCP_GP', 
                                                                                               'WCP_PV',                                                                                              
                                                                                               'WCP_RB',
                                                                                               'WCP_RM',                                                                                           
                                                                                               
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        if(roleDeveloperName.startsWith('Western_Canada_Production_Director'))   return new Set<String>{'NONE',
                                                                                               'WCP_SL',
                                                                                               'GRD',
                                                                                               'WCP_GP', 
                                                                                               'WCP_PV',                                                                                              
                                                                                               'WCP_RB',
                                                                                               'WCP_RM',                                                                                           
                                                                                               
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        
                                                                                               
            //if(roleDeveloperName.startsWith('Edson '))                  return new Set<String>{'GRD'};
           // if(roleDeveloperName.startsWith('Grande Prairie'))          return new Set<String>{'WCP_GP'};
            //if(roleDeveloperName.startsWith('Rocky Mountain House'))    return new Set<String>{'WCP_RM'}; 
            if(roleDeveloperName.startsWith('Foothills'))                           return new Set<String>{'NONE',
                                                                                               'WCP_SL',
                                                                                               'GRD',
                                                                                               'WCP_GP', 
                                                                                               'WCP_PV',                                                                                              
                                                                                               'WCP_RB',
                                                                                               'WCP_RM',                                                                                           
                                                                                               
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};                                                                            
        if(roleDeveloperName.startsWith('Plains'))                   return new Set<String>{'NONE',
                                                                                               'WCP_SL',
                                                                                               'GRD',
                                                                                               'WCP_GP', 
                                                                                               'WCP_PV',                                                                                              
                                                                                               'WCP_RB',
                                                                                               'WCP_RM',                                                                                           
                                                                                               
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
            if(roleDeveloperName.startsWith('Athabasca / Slave Lake'))   return new Set<String>{'WCP_SL'};
            if(roleDeveloperName.startsWith('Provost / Dodsland'))          return new Set<String>{'WCP_PV'};
            if(roleDeveloperName.startsWith('Swift Current / Estevan'))     return new Set<String>{'WCP_SC'};
            if(roleDeveloperName.startsWith('Taber Hussar'))                return new Set<String>{'WCP_TB'};                                                                              
        if(roleDeveloperName.startsWith('GRD'))                         return new Set<String>{'GRD'};
        if(roleDeveloperName.startsWith('WCP_RB'))                      return new Set<String>{'NONE',
                                                                                               'WCP_SL',
                                                                                               'GRD',
                                                                                               'WCP_GP', 
                                                                                               'WCP_PV',                                                                                              
                                                                                               'WCP_RB',
                                                                                               'WCP_RM',                                                                                           
                                                                                               
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        if(roleDeveloperName.startsWith('Western_Canada_Conventional')) return new Set<String>{'NONE','WCP_GP',
                                                                                               
                                                                                               'WCP_SL',
                                                                                               'WCP_PV',
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        if(roleDeveloperName.startsWith('Northwest'))                   return new Set<String>{'NONE','WCP_GP',
                                                                                             
                                                                                               'WCP_SL'};
        if(roleDeveloperName.startsWith('WCP_GP'))                      return new Set<String>{'WCP_GP'};
      
        if(roleDeveloperName.startsWith('WCP_SL'))                      return new Set<String>{'WCP_SL'};
        if(roleDeveloperName.startsWith('Southeast'))                   return new Set<String>{'NONE','WCP_PV',
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        if(roleDeveloperName.startsWith('WCP_PV'))                      return new Set<String>{'WCP_PV'};
        if(roleDeveloperName.startsWith('WCP_SC'))                      return new Set<String>{'WCP_SC'};
        if(roleDeveloperName.startsWith('WCP_TB'))                      return new Set<String>{'WCP_TB'};
        
        return null;
    }
    
    public static Set<String> roleLabel2schemaNames(String roleLabel)
    {
        if(roleLabel.startsWith('Western Canada Production'))   return new Set<String>{'NONE',
                                                                                               'GRD',
                                                                                               'WCP_GP',
                                                                                               'WCP_RM',
                                                                                               'WCP_RB', 
                                                                                               'WCP_SL',
                                                                                               'WCP_PV',
                                                                                               'WCP_SC',
                                                                                               'WCP_TB'};
        if(roleLabel.startsWith('Foothills'))        return new Set<String>{'NONE',
                                                                                      'GRD',
                                                                                      'WCP_GP',
                                                                                      'WCP_RM'};
            if(roleLabel.startsWith('Edson '))                  return new Set<String>{'GRD'};
            if(roleLabel.startsWith('Grande Prairie'))          return new Set<String>{'WCP_GP'};
            if(roleLabel.startsWith('Rocky Mountain House'))    return new Set<String>{'WCP_RM'};           
        if(roleLabel.startsWith('Plains'))                      return new Set<String>{'NONE',                                                                                               
                                                                                            'WCP_SL',
                                                                                               'WCP_PV',
                                                                                               'WCP_SC',
                                                                                           'WCP_TB'};   
            if(roleLabel.startsWith('Athabasca / Slave Lake'))   return new Set<String>{'WCP_SL'};
            if(roleLabel.startsWith('Provost / Dodsland'))          return new Set<String>{'WCP_PV'};
            if(roleLabel.startsWith('Swift Current / Estevan'))     return new Set<String>{'WCP_SC'};
            if(roleLabel.startsWith('Taber Hussar'))                return new Set<String>{'WCP_TB'};                                                                                                  
        if(roleLabel.startsWith('Gas Resource Development'))    return new Set<String>{'GRD'};
        if(roleLabel.startsWith('Rainbow Lake'))                return new Set<String>{'WCP_RB'};
        if(roleLabel.startsWith('Western Canada Conventional')) return new Set<String>{'WCP_GP',
                                                                                      
                                                                                       'WCP_SL',
                                                                                       'WCP_PV',
                                                                                       'WCP_SC',
                                                                                       'WCP_TB'};
        if(roleLabel.startsWith('Northwest'))                   return new Set<String>{'WCP_GP',
                                                                                       'WCP_RM',
                                                                                       'WCP_SL'};
        if(roleLabel.startsWith('Grande Prairie'))              return new Set<String>{'WCP_GP'};
        if(roleLabel.startsWith('Rocky Mountain'))              return new Set<String>{'WCP_RM'};
        if(roleLabel.startsWith('Slave Lake'))                  return new Set<String>{'WCP_SL'};
        if(roleLabel.startsWith('Southeast'))                   return new Set<String>{'WCP_PV',
                                                                                       'WCP_SC',
                                                                                       'WCP_TB'};
        if(roleLabel.startsWith('Provost'))                     return new Set<String>{'WCP_PV'};
        if(roleLabel.startsWith('Swift Current'))               return new Set<String>{'WCP_SC'};
        if(roleLabel.startsWith('Taber/Hussar'))                return new Set<String>{'WCP_TB'};
        
        return null;
    }
    
    public static Set<String> roleId2schemaNames(Id roleId)
    {
        UserRole role = [SELECT Id, DeveloperName FROM UserRole WHERE Id =: roleId];
        
        return roleDeveloperName2schemaNames(role.DeveloperName);
        
        return null;
    }
    
    public static String schemaName2spotfirePrefix(String schemaName)
    {
        if(schemaName == 'GRD')    return 'GRD';
        if(schemaName == 'WCP_RB') return 'RainbowLake';
        if(schemaName == 'WCP_GP') return 'GrandePrairie';
        if(schemaName == 'WCP_RM') return 'RockyMountainHouse';
        if(schemaName == 'WCP_SL') return 'SlaveLakeAth';
        if(schemaName == 'WCP_PV') return 'Provost';
        if(schemaName == 'WCP_SC') return 'SwiftCurrent';
        if(schemaName == 'WCP_TB') return 'TaberHussar';
        
        return null;
    }
    
    public static String schemaName2RoleName(String schemaName)
    {
        /*if(schemaName == 'GRD')    return 'Gas Resource Development';
        if(schemaName == 'WCP_RB') return 'Rainbow Lake';
        if(schemaName == 'WCP_GP') return 'Grande Prairie';
        if(schemaName == 'WCP_SL') return 'Slave Lake/Athabasca';
        if(schemaName == 'WCP_PV') return 'Provost';
        if(schemaName == 'WCP_SC') return 'Swift Current';
        if(schemaName == 'WCP_TB') return 'Taber/Hussar';
        if(schemaName == 'WCP') return 'Western Canada Production';*/
        
        if(schemaName == 'NONE')    return 'None';
        if(schemaName == 'GRD')    return 'Edson';
        if(schemaName == 'WCP_GP') return 'Grande Prairie';
        if(schemaName == 'WCP_RM') return 'Rocky Mountain House';
        if(schemaName == 'WCP_RB') return 'Rainbow Lake';
        if(schemaName == 'WCP_SL') return 'Athabasca / Slave Lake';
        if(schemaName == 'WCP_PV') return 'Provost / Dodsland';
        if(schemaName == 'WCP_SC') return 'Swift Current / Estevan';
        if(schemaName == 'WCP_TB') return 'Taber/Hussar';
        if(schemaName == 'WCP') return 'Western Canada Production';
        
        return null;
    }

}