/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    DAO Provider. Holds all DAO Implementation providing required functionality for
                Service Notification Requests
History:        mbrimus 2020-01-08 - Created.
*************************************************************************************************/
public inherited sharing class SNR_DAOProvider {
    public static SNR_DAO snrDao = new SNR_Selector();
}