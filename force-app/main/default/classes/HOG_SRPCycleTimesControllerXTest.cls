/*------------------------------------------------------------------------------------------------------------
Author     : Miroslav Zelina
Company    : IBM
Description: Unit Test Class for HOG_SRPCycleTimesControllerX
History    : 04.28.17    Class Created            
-------------------------------------------------------------------------------------------------------------*/
@isTest
private class HOG_SRPCycleTimesControllerXTest {
    
    private static HOG_Service_Rig_Program__c serviceRigProgram;
    private static HOG_EOJ__c endOfJob;
    private static HOG_EOJ__c endOfJob2;
    private static HOG_Service_Rig_Program__c testSRP;
    private static HOG_EOJ__c testEOJ;
    
    @isTest
    static void testCycleTimesCalculations() {
        
        CreateTestData();
        
        Test.startTest();
        
            HOG_SRPCycleTimesControllerX SRPCycleTimesCtrlX = new HOG_SRPCycleTimesControllerX(
                new ApexPages.StandardController(serviceRigProgram));
        
        
            testSRP = [SELECT Id, SRP_Submitted_to_Fully_Approved__c, Well_Down_to_SRP_Submitted__c,
                              SRP_Submitted_date__c, Well_Down_Date__c
                       FROM  HOG_Service_Rig_Program__c 
                       WHERE Id =: SRPCycleTimesCtrlX.srpId];
            testEOJ = [SELECT Id, Service_Completed__c
                       FROM HOG_EOJ__c
                       Where Id = :SRPCycleTimesCtrlX.eoj.Id];
        
            System.assertEquals(5, testSRP.SRP_Submitted_to_Fully_Approved__c);
            System.assertEquals(Math.abs(testSRP.SRP_Submitted_date__c.date().daysBetween(testSRP.Well_Down_Date__c))+1,
                    testSRP.Well_Down_to_SRP_Submitted__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.eoj.Fully_Approved_to_Service_Started__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.eoj.Serv_Started_to_Serv_Completed__c);
            System.assertEquals(Math.abs(testEOJ.Service_Completed__c.daysBetween(testSRP.Well_Down_Date__c))+1,
                    SRPCycleTimesCtrlX.eoj.Well_Down_to_Service_Completed__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.ed.Serv_Completed_to_ED_Completed__c);
            
            Set<String> errMsgSet = new Set<String>();
            
            for(ApexPages.Message msg :  ApexPages.getMessages()) {

                errMsgSet.add(msg.getSummary());
            }
            
            System.assertEquals(true, errMsgSet.contains('This SRP has more than one End of Job, Cycle Times are calculated based on the oldest one.'));
            System.assertEquals(true, errMsgSet.contains('This SRP has more than one Engineering Diagnosis form, Cycle Times are calculated based on the oldest one.'));

        Test.stopTest(); 
    }
    
    
    public static void CreateTestData(){
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
        Approval.ProcessResult result;
      
              
        System.runAs(runningUser) {
           
          //SRP  + SNR and all required related objects
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
          serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
         
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            
         //Approve SRP as Service Rig Coordinator
         System.runAs(serviceRigPlanner) {
                
            List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req2);
        }
        
        //Approve SRP as Production Coordinator
        System.runAs(serviceRigPlanner) {
                
            List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req2);
        
        }


        System.runAs(runningUser) {
            Date todayDate = date.today();
            DateTime todayDateTime = DateTime.now();
            //EOJ
            endOfJob = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');
            endOfJob.Service_Started__c = todayDate.addDays(20);
            endOfJob.Service_Completed__c = todayDate.addDays(25);
            update endOfJob;

            //Create another EOJ for the same SRP - test odd case - disaply info message on vf page
            endOfJob2 = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');
            endOfJob2.Service_Started__c = todayDate.addDays(21);
            endOfJob2.Service_Completed__c = todayDate.addDays(26);
            update endOfJob2;

            //ED
            HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();
            engDiag.Service_Rig_Program__c = serviceRigProgram.Id;
            engDiag.End_Of_Job__c = endOfJob.Id;
            engDiag.Diagnosis_Completed_Date__c = todayDate.addDays(30);
            engDiag.Diagnosis_Comments__c = 'Diagnosis Comments Required';
            insert engDiag;

            //Create another ED for the same SRP - test odd case - disaply info message on vf page
            HOG_Engineering_Diagnosis__c engDiag2 = new HOG_Engineering_Diagnosis__c();
            engDiag2.Service_Rig_Program__c = serviceRigProgram.Id;
            engDiag2.End_Of_Job__c = endOfJob.Id;
            engDiag2.Diagnosis_Completed_Date__c = todayDate.addDays(31);
            engDiag2.Diagnosis_Comments__c = 'Diagnosis Comments Required';
            insert engDiag2;

            //Update SRP fields used in calculations
            serviceRigProgram.Financial_Approved_Date__c = todayDateTime.addDays(15);
            serviceRigProgram.Technical_Approved_Date__c = todayDateTime.addDays(15);
            serviceRigProgram.SRP_Submitted_date__c = todayDateTime.addDays(10);
            update serviceRigProgram;
        }
          
    } 

}