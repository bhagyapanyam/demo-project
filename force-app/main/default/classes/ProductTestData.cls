public class ProductTestData {
    public static List<Product2> createProducts(Integer numberOfProducts) {
        List<Product2> productList = new List<Product2>();
        
        for(Integer i = 0; i < numberOfProducts; i++) {
            Product2 product = new Product2();
            product.Name = 'Product Test Name ' + i;
            product.Density__c = 1.00;
            productList.add(product);
        }
        
        return productList;
    }

    public static List<Product2> createProducts(Integer numberOfProducts, Id productRecordType) {
        List<Product2> productList = new List<Product2>();
        
        for(Integer i = 0; i < numberOfProducts; i++) {
            Product2 product = new Product2();
            product.Name = 'Product Test Name ' + i;
            product.Density__c = 1.00;
            product.RecordTypeId = productRecordType;
            productList.add(product);
        }
        
        return productList;
    }
}