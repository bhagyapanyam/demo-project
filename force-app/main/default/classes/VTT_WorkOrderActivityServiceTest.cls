/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WorkOrderActivityService
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VTT_WorkOrderActivityServiceTest {

    @IsTest
    static void getRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Work_Order_Activity__c result;
        Exception thrownException;

        Test.startTest();
        try {
            result = new VTT_WorkOrderActivityService().getRecord(null);
        } catch (Exception ex) {
            failFlag = true;
            thrownException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag,
                'Expected fail flag is true as this method validates provided param and throws error if wrong. (No Record)');
        System.assertEquals(null, result,
                'Since action failed, result should not be present: ' + result);
        System.assertEquals(new HOG_Exception().getTypeName(), thrownException.getTypeName(),
                'As exception which is thrown on validation error is custom it should be HOG_Exception. ' + thrownException.getTypeName());
    }

    @IsTest
    static void getRecord_withWrongParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Work_Order_Activity__c result;
        Exception thrownException;

        Test.startTest();
        try {
            result = new VTT_WorkOrderActivityService().getRecord(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            thrownException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag,
                'Expected fail flag is true as this method validates provided param and throws error if wrong. (No Record)');
        System.assertEquals(null, result,
                'Since action failed, result should not be present: ' + result);
        System.assertEquals(new HOG_Exception().getTypeName(), thrownException.getTypeName(),
                'As exception which is thrown on validation error is custom it should be HOG_Exception. ' + thrownException.getTypeName());
    }

    @IsTest
    static void getRecord_mockedSelector_fail() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Work_Order_Activity__c result;
        Exception thrownException;

        Test.startTest();
        try {
            EPD_DAOProvider.workOrderActivityDAO = new SelectorMock_MultiRecord();
            result = new VTT_WorkOrderActivityService().getRecord(null);
        } catch (Exception ex) {
            failFlag = true;
            thrownException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag,
                'Expected fail flag is true as this method validates provided param and throws error if wrong. (multiple records)');
        System.assertEquals(null, result,
                'Since action failed, result should not be present: ' + result);
        System.assertEquals(new HOG_Exception().getTypeName(), thrownException.getTypeName(),
                'As exception which is thrown on validation error is custom it should be HOG_Exception. ' + thrownException.getTypeName());
    }

    @IsTest
    static void getRecord_mockedSelector_success() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Work_Order_Activity__c result;
        Exception thrownException;

        Test.startTest();
        try {
            EPD_DAOProvider.workOrderActivityDAO = new SelectorMock_SingleRecord();
            result = new VTT_WorkOrderActivityService().getRecord(null);
        } catch (Exception ex) {
            failFlag = true;
            thrownException = ex;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag,
                'Expected fail flag is false as this mock will return single record as expected.');
        System.assertEquals(null, thrownException, 'No exception should be thrown.');
        System.assertNotEquals(null, result, 'Result should not be empty');
    }

    public class SelectorMock_SingleRecord implements VTT_WorkOrderActivityDAO {
        public List<Work_Order_Activity__c> getRecord(Id workOrderActivityId) {
            return new List<Work_Order_Activity__c> {
                    VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName', null, null, false)
            };
        }
    }

    public class SelectorMock_MultiRecord implements VTT_WorkOrderActivityDAO {
        public List<Work_Order_Activity__c> getRecord(Id workOrderActivityId) {
            return new List<Work_Order_Activity__c> {
                    VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName1', null, null, false),
                    VTT_TestDataFactory.createWorkOrderActivity('RTS', 'TestName2', null, null, false)
            };
        }
    }

}