public  class OpsLog_Operating_Report_Ctrl {
     public String dateRange {get;set;}
     public String selectedAreas{get;set;}
     public String dFrom { get; set; }
     public String dTo { get; set; }
     public String selectedUnits {get;set;}
     public List<String> selectedPriority {get;set;}
     public String selectedOperators {get;set;}
     public String selectedDepartment {get;set;}
     public List<SelectOption> departmentList {get;set;}
     public String selectedTeams {get;set;}
     public String selectedRoles {get;set;}
     public integer resultNo {get;set;}
     public transient Map<String, List<DOI__c>> resultMap {get;set;}
     public transient Map<String, integer> areaMap {get;set;}
     public transient Map<Id, String> teamRoleMap {get;set;}
     public String selectedCompletedFlag {get;set;}
     public List<SelectOption> completedFlagList {get;set;}
     
     
    public OpsLog_Operating_Report_Ctrl(){
        if (ApexPages.currentPage().getParameters().get('startDate') != null 
                && ApexPages.currentPage().getParameters().get('endDate') != null)   
        {
            dFrom = ApexPages.currentPage().getParameters().get('startDate');
            dTo = ApexPages.currentPage().getParameters().get('endDate');  
            if (ApexPages.currentPage().getParameters().get('priority') != null)
            {
               String priority = ApexPages.currentPage().getParameters().get('priority');
               System.debug('priority =' + priority);
               selectedPriority = priority.split(',');
            }
            if (ApexPages.currentPage().getParameters().get('area') != null)
                selectedAreas = ApexPages.currentPage().getParameters().get('area');
            if (ApexPages.currentPage().getParameters().get('unit') != null)   
                selectedUnits = ApexPages.currentPage().getParameters().get('unit'); 
            
            if (ApexPages.currentPage().getParameters().get('operator') != null)   
                selectedOperators = ApexPages.currentPage().getParameters().get('operator'); 
            if (ApexPages.currentPage().getParameters().get('team') != null)   
                selectedTeams = ApexPages.currentPage().getParameters().get('team');    
            
            if (ApexPages.currentPage().getParameters().get('role') != null)   
                selectedRoles = ApexPages.currentPage().getParameters().get('role');             
        }
        else
        {
            //set default search criteria
          dateRange = 'LAST_N_DAYS:7';
          dFrom = datetime.now().addDays(-7).format(DateUtilities.DDMMYYYY_DATE_FORMAT);
          dTo = datetime.now().format(DateUtilities.DDMMYYYY_DATE_FORMAT);
          
          selectedPriority = new List<String>{'Important','Critical','Routine'};
        }
        getAllDepartments();
        getCompletedFlag();
        System.debug('From =' + dFrom + ' to=' + dTo);
        RunReport();    
    }
     
    public List<SelectOption> getPriorityList()  
    {
      List<SelectOption> options = new List<SelectOption>();
        
      Schema.DescribeFieldResult fieldResult = Operational_Event__c.Priority__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
          
      for( Schema.PicklistEntry f : ple)
      {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
      }       
      return options;
    }
    
    
    //run report and show the result
    public void RunReport()
    {
        resetResult();
        
        Date selectedDFrom = DateUtilities.toDMYYYDate(dFrom, '/', null);
        Date selectedDTo = DateUtilities.toDMYYYDate(dTo, '/', null);
        
        if (selectedDFrom == null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When does the search start ?'));
           return ;
        }
        
        if (dTo == null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When does the search end ?'));   
           return ;
        }
         
        try
        {
            /*build dynamic query*/
            String soql = 'SELECT Area__c, Area__r.Name, Date__c, Completed__c, Completion_Date__c, Instructions__c, Owner.Name,'
                       + 'Plant__c, Plant__r.Name, Unit__c, Unit__r.Name, Equipment__r.Name, RecordType.Name, DOI_Owner__c, DOI_Owner__r.Name, '
                       + 'Team_Allocation__r.Team__r.Name, Team_Allocation__r.Role__r.Name '
                       + 'From DOI__c '
                       //+ 'Where Date__c >= :selectedDFrom and Date__c <= :selectedDTo ';
                       + 'Where (Completion_Date__c >= :selectedDFrom OR Completion_Date__c = null) and Date__c <= :selectedDTo ';
                
            if (selectedDepartment != '' && selectedDepartment != null)
                soql += ' and RecordType.Name =: selectedDepartment';  
                
            if (selectedCompletedFlag != '' && selectedCompletedFlag != null) 
                soql += ' and Completed__c = ' + Boolean.valueOf(selectedCompletedFlag);  
                
            if (selectedAreas != null && selectedAreas != '') {
                String[] areaList = selectedAreas.split(',');
                soql += ' and Area__r.Name in :areaList';
            } 
            
            if (selectedUnits != null && selectedUnits != '') {
                String[] unitList = selectedUnits.split(',');
                soql += ' and Unit__r.Name in :unitList';
            } 
            
            if (selectedOperators != null && selectedOperators != '') {
                String[] ownerList = selectedOperators.split(',');
                soql += ' and DOI_Owner__r.Name in :ownerList';
            }
            
            if (selectedRoles != null && selectedRoles != '') {
                String[] roleList = selectedRoles.split(',');
                soql += ' and Team_Allocation__r.Role__r.Name in :roleList';
            } 
            
            if (selectedTeams != null && selectedTeams != '') {
                String[] teamList = selectedTeams.split(',');
                soql += ' and Team_Allocation__r.Team__r.Name in :teamList';
            }           
            
            soql += ' ORDER BY Area__r.Name, Date__c ASC';                                              
                       
            /*output results*/                     
            List<DOI__c> oEvents = Database.query(soql);
            
            if (oEvents != null && oEvents.size() > 0) {
                for(DOI__c oEvent : oEvents) {       
                    String curAreaName = oEvent.Area__r.Name;
                    List<DOI__c> curEventList = new List<DOI__c>();
                    
                    if (resultMap.containsKey(curAreaName)) {
                        curEventList = resultMap.get(curAreaName);
                        curEventList.add(oEvent);
                    }
                    else {
                        curEventList.add(oEvent);
                        resultMap.put(curAreaName, curEventList);
                    }                
                }
                
              for(String areaName : resultMap.keySet()) {
                  areaMap.put(areaName, resultMap.get(areaName).size());   
              }                     
            } 
            
            resultNo = oEvents.size();              
        }
        catch(QueryException ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));   
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));   
        }
        
    }     
    
    public PageReference ExportDetail()
    {
        string url = '/apex/OpsLog_Operating_Report_Export_Detail?startDate=' + dFrom + '&endDate=' + dTo;
        if (selectedAreas != null && selectedAreas != '')
            url += '&area=' + selectedAreas;
        if(selectedUnits != null && selectedUnits != '')
            url += '&unit=' + selectedUnits;    
        if(selectedOperators != null && selectedOperators != '')
            url += '&operator=' + selectedOperators;
        if(selectedTeams != null && selectedTeams != '')
            url += '&team=' + selectedTeams;            
        if (selectedRoles != null && selectedRoles != '')
            url += '&role=' + selectedRoles; 
               
        PageReference detailPage = new PageReference(url);
        
        return detailPage;
    } 
    
    private void getAllDepartments()
    {
      departmentList = new List<SelectOption>();
      departmentList.add(new SelectOption('','All'));
      departmentList.add(new SelectOption('Operations', 'Operations'));
      departmentList.add(new SelectOption('Maintenance', 'Maintenance'));
      
      UserRole role = [select DeveloperName from UserRole where Id =: UserInfo.getUserRoleId()];
      if (role.DeveloperName.contains('Operations'))
        selectedDepartment = 'Operations';
      else if (role.DeveloperName.contains('Maintenance'))   
          selectedDepartment = 'Maintenance';
      else if (role.DeveloperName == 'Sunrise' || role.DeveloperName == 'Oil_Sands')
          selectedDepartment = '';
      
    }         
    
    private void resetResult() 
    {
         resultMap = new Map<String, List<DOI__c>>();
         areaMap = new Map<String, integer>();
         teamRoleMap = new Map<Id, String>();
         resultNo = 0;
    } 
    
    private void getCompletedFlag(){
        completedFlagList = new List<SelectOption>();
        completedFlagList.add(new SelectOption('','All'));
        completedFlagList.add(new SelectOption('Yes','Yes'));
        completedFlagList.add(new SelectOption('No','No'));
    }    
         
}