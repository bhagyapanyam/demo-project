/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:	Batch job for Add equipment
 *  Test class:		EquipmentCorrectionFormTest
 *  History:        Created on 10/18/2018
 */

global class EquipmentUpdateExpirationBatchJob implements
				Database.Batchable<sObject>, Database.Stateful {

	global Date dateOfToday = Date.today();
	// using custom setting to get number of days to send notification email to SAP team.
	global Date notificationDate = Date.today() + Integer.valueOf(HOG_Equipment_Request_Configuration__c.getInstance().Days_Before_Expiration_Notification__c);
	public static final String expirationOfRejected = 'statusRejected';
	public static final String expirationOfOpen = 'statusOpen';
	public static final String beforeExpirationNotification = 'sendNotification';
	private final String checkStatusAndDateForEmail;
	private String query;

	global EquipmentUpdateExpirationBatchJob(String checkStatusAndDateForEmail) {
		this.checkStatusAndDateForEmail = checkStatusAndDateForEmail;
		if (checkStatusAndDateForEmail == expirationOfRejected) {
			query = 'SELECT Expiration_Date__c, Status__c FROM Equipment_Correction_Form__c WHERE Expiration_Date__c <=: dateOfToday AND Status__c = \'Rejected\' ';
		} else if (checkStatusAndDateForEmail == expirationOfOpen) {
			query = 'SELECT Expiration_Date__c, Status__c FROM Equipment_Correction_Form__c WHERE Expiration_Date__c <=: dateOfToday AND Status__c = \'Open\'';
		} else if (checkStatusAndDateForEmail == beforeExpirationNotification) {
			query = 'SELECT Expiration_Date__c, Status__c, Send_Email_Notification__c FROM Equipment_Correction_Form__c WHERE Expiration_Date__c =: notificationDate AND (Status__c = \'Rejected\' OR Status__c = \'Open\')';
		}
	}

	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<Equipment_Correction_Form__c> equipments) {

		List<Equipment_Correction_Form__c> equipmentsToUpdate = new List<Equipment_Correction_Form__c>();

		if (checkStatusAndDateForEmail == expirationOfRejected) {
			for (Equipment_Correction_Form__c eq : equipments) {
				eq.Status__c = 'Rejected Closed';
				equipmentsToUpdate.add(eq);
			}
		} else if (checkStatusAndDateForEmail == expirationOfOpen) {
			for (Equipment_Correction_Form__c eq : equipments) {
				eq.Status__c = 'Auto Closed';
				equipmentsToUpdate.add(eq);
			}
		} else if (checkStatusAndDateForEmail == beforeExpirationNotification) {
			for (Equipment_Correction_Form__c eq : equipments) {
				eq.Send_Email_Notification__c = true;
				equipmentsToUpdate.add(eq);
			}
		}
		update equipmentsToUpdate;
	}

	global void finish(Database.BatchableContext bc) {
	}
}